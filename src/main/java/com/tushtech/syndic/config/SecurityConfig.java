package com.tushtech.syndic.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.tushtech.syndic.security.ProvedorAutenticacao;
import com.tushtech.syndic.service.MyLogoutSuccessHandlerOne;


@Configuration
@EnableWebSecurity
public class SecurityConfig  extends WebSecurityConfigurerAdapter  {




	@Autowired
	private ProvedorAutenticacao provedorAutenticacao;
	
	
	@Autowired
	private MyLogoutSuccessHandlerOne myLogoutSuccessHandlerOne;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(provedorAutenticacao);
        

	}


	@Override
	public void configure(WebSecurity web) throws Exception {

		web.ignoring()
		.antMatchers("/assets/**")
		.antMatchers("/css/**")
		.antMatchers("/img/**")
		.antMatchers("/esquecisenha")
		.antMatchers("/site/**")
		.antMatchers("/js/**")
		.antMatchers("/syndic/condominio/listar")
		.antMatchers("/atendimento/pormestipo")
		.antMatchers("/javascripts/**")
		.antMatchers("/site");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.headers().addHeaderWriter(new XFrameOptionsHeaderWriter(XFrameOptionsHeaderWriter.XFrameOptionsMode.SAMEORIGIN));
		http
		.authorizeRequests()
			.anyRequest().authenticated()
			.and()
			.formLogin()
			.loginPage("/login")
			.permitAll()
			.successForwardUrl("/")
		.and()
			.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/sair"))
			.logoutSuccessHandler(myLogoutSuccessHandlerOne)
		.and()
		.exceptionHandling()
		.accessDeniedPage("/403")
		.and()
		.sessionManagement()
		.invalidSessionUrl("/login")
		.and()
		.csrf().disable();
	}


	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}



}