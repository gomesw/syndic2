package com.tushtech.syndic.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.tushtech.syndic.storage.ArquivoStorage;
import com.tushtech.syndic.storage.FotoStorage;
import com.tushtech.syndic.storage.local.ArquivoStorageLocal;
import com.tushtech.syndic.storage.local.FotoStorageLocal;


@Configuration
public class ServiceConfig {
	
	@Bean
	public FotoStorage fotoStorage() {
		return new FotoStorageLocal();
	}
	
	@Bean
	public ArquivoStorage ArquivoStorage() {
		return new ArquivoStorageLocal();
	}
}
