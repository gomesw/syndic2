package com.tushtech.syndic.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.tushtech.syndic.thymeleaf.SyndicDialect;


@Configuration
@EnableAsync
public class WebConfig extends WebMvcConfigurerAdapter{
	@Bean
	public SyndicDialect syndicDialect() {
		return new SyndicDialect();
	}
	
//	@Bean
//	public MultipartResolver multipartResolver() {
//		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
//		multipartResolver.setMaxUploadSize(10485760/2); // 5MB
//		multipartResolver.setMaxUploadSizePerFile(1048576); // 1MB
//		return multipartResolver;
//	}

	
	
//	@Bean(name = "multipartResolver")
//	public CommonsMultipartResolver createMultipartResolver() {
//	    CommonsMultipartResolver resolver=new CommonsMultipartResolver();
//	    resolver.setDefaultEncoding("utf-8");
//	    return resolver;
//	}
}
