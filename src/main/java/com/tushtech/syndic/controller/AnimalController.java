package com.tushtech.syndic.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Animal;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.CorAnimal;
import com.tushtech.syndic.entity.FotoAnimal;
import com.tushtech.syndic.entity.Raca;
import com.tushtech.syndic.entity.TipoAnimal;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.AnimalRepository;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.CorAnimalRepository;
import com.tushtech.syndic.repository.RacaRepository;
import com.tushtech.syndic.repository.TipoAnimalRepository;
import com.tushtech.syndic.repository.UnidadeRepository;
import com.tushtech.syndic.service.AnimalService;

@Controller
@RequestMapping()
public class AnimalController {
	
	
	@Autowired
	private UnidadeRepository unidadeRepository; 

	@Autowired
	private CondominioRepository condominioRepository; 


	@Autowired
	private AnimalRepository animalRepository; 

	@Autowired
	private TipoAnimalRepository tipoAnimalRepository; 

	@Autowired
	private RacaRepository racaRepository; 


	@Autowired
	private AnimalService animalService;	


	@Autowired
	private CorAnimalRepository corAnimalRepository;


	@RequestMapping("/unidade/animal/listar/{id}")
	public ModelAndView getAnimaisUnidades(@PathVariable Integer id) {
		ModelAndView mv = new ModelAndView("/syndic/unidades/animal/listar");
		Unidade unidade = unidadeRepository.findById(id);
		List<Animal> listAnimal =  animalRepository.findByUnidadeId(id);
		mv.addObject("listAnimal",listAnimal);
		mv.addObject("unidade",unidade);
		mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId())); 
		return mv;

	}


	/**
	 * TELA PARA ADICIONAR UM Aniaml A UMA UNIDADE
	 * LEMBRANDO QUE O DOCUMENTO É VINCULADO A PESSOAFISICAUNIDADE
	 * @param id DA UNIDADE
	 * @return
	 */
	@RequestMapping("/unidade/animal/adicionar/{id}")
	public ModelAndView novoAnimalUnidade(@PathVariable Integer id) {

		ModelAndView mv = new ModelAndView("/syndic/unidades/animal/formincluir"); 
		Unidade unidade = unidadeRepository.buscarUnidadeCompleta(id);
		Animal animal = new Animal(); 
		animal.setUnidade(unidade);

		List<FotoAnimal> listFotoAnimal = new ArrayList<>();
		animal.setListFotoAnimal(listFotoAnimal);

		List<TipoAnimal> tipoAnimal = tipoAnimalRepository.findAll();

		List<CorAnimal> listCorAnimal = corAnimalRepository.findAll();

		mv.addObject("listTipoAnimal", tipoAnimal); 
		mv.addObject("listCorAnimal", listCorAnimal); 
		mv.addObject("animal", animal); 
		mv.addObject("unidade", unidade); 
		mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());


		return mv;
	}


	@RequestMapping(value = "/unidade/animal/editar/{id}", method = RequestMethod.GET)
	public ModelAndView editarAnimal(@PathVariable Integer id,Animal animal) 
	{
		ModelAndView mv = new ModelAndView("/syndic/unidades/animal/formincluir"); 

		animal = animalRepository.findOne(id); 


		List<TipoAnimal> tipoAnimal = tipoAnimalRepository.findAll();
		List<CorAnimal> listCorAnimal = corAnimalRepository.findAll();
		List<Raca> listRaca = racaRepository.findByTipoAnimalIdOrderByNomeAsc(animal.getRaca()!=null &&animal.getRaca().getTipoAnimal()!=null? animal.getRaca().getTipoAnimal().getId():0);

		mv.addObject("listCorAnimal", listCorAnimal); 
		mv.addObject("listRaca", listRaca); 
		mv.addObject("listTipoAnimal", tipoAnimal); 
		mv.addObject("animal", animal); 
		mv.addObject("condominio", animal.getUnidade().getEnderecoUnidade().getCondominio());
		return mv; 
	}


	/**
	 * SALVAR ANIMAL  NO BANCO DE DADOS
	 * @param unidade
	 * @param result
	 * @param attributes
	 * @return
	 */
	@PostMapping(value = {"/unidade/animal/novo", "/animal/novo/{\\d+}"})
	public ModelAndView salvarAnimal(@Valid Animal animal, BindingResult result, RedirectAttributes attributes) {


		if(result.hasErrors()){
			return novoAnimalUnidadeErro(animal);
		}



		List<FotoAnimal> listFotoAnimal =  animal.getListFotoAnimal();

		animal = animalService.salvarAnimal(animal);

		animalService.salvarFotos(animal,listFotoAnimal);

		if(animal.isNovo()) {
			attributes.addFlashAttribute("mensagem"," Animal cadastrado com sucesso!");
		}
		else {
			attributes.addFlashAttribute("mensagem"," Animal editado com sucesso!");
		}

		return new ModelAndView("redirect:/unidade/editar/" + animal.getUnidade().getId());
	}



	private ModelAndView novoAnimalUnidadeErro(Animal animal) {

		ModelAndView mv = new ModelAndView("/syndic/unidades/animal/formincluir"); 


		List<TipoAnimal> tipoAnimal = tipoAnimalRepository.findAll();

		List<CorAnimal> listCorAnimal = corAnimalRepository.findAll();

		List<Raca> listRaca = racaRepository.findByTipoAnimalIdOrderByNomeAsc(animal.getRaca()!=null &&animal.getRaca().getTipoAnimal()!=null? animal.getRaca().getTipoAnimal().getId():0);

		mv.addObject("listCorAnimal", listCorAnimal); 
		mv.addObject("listRaca", listRaca); 
		mv.addObject("listTipoAnimal", tipoAnimal); 
		mv.addObject("animal", animal); 
		mv.addObject("condominio", animal.getUnidade().getEnderecoUnidade().getCondominio());


		return mv;
	}
	
	
	
	@RequestMapping("/condominio/animal/{id}")
	public ModelAndView formAnimal(@PathVariable Integer id) { 

		ModelAndView mv = new ModelAndView("/syndic/condominios/animal/formincluir");
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);

		Animal animal = new Animal(); 
		
		List<FotoAnimal>  listFotoAnimal = new ArrayList<>();
		
		animal.setListFotoAnimal(listFotoAnimal);
		
		List<TipoAnimal> tipoAnimal = tipoAnimalRepository.findAll();
		List<CorAnimal> listCorAnimal = corAnimalRepository.findAll();
		
		mv.addObject("condominio", condominio); 
		mv.addObject("unidades", condominio.getUnidades()); 
		mv.addObject("listTipoAnimal", tipoAnimal); 
		mv.addObject("listCorAnimal", listCorAnimal); 
		mv.addObject("animal", animal); 
		
		return mv;

	}
	
	
	@GetMapping("/condominio/animal/editar/{id}")
	public ModelAndView editarAnimalCondominio(@PathVariable("id") Integer id,Animal animal) 
	{

		animal = animalRepository.findById(id); 
		
		ModelAndView mv = formularioAnimal(animal);
		return mv; 
	}
	
	
	
	
	
	private ModelAndView formularioAnimal(Animal animal) {

		ModelAndView mv = new ModelAndView("/syndic/condominios/animal/formincluir");
		
		Unidade unidade = animal.getUnidade();
		unidade = unidadeRepository.buscarUnidadeCompleta(unidade.getId());
		
		List<TipoAnimal> tipoAnimal = tipoAnimalRepository.findAll();
		List<CorAnimal> listCorAnimal = corAnimalRepository.findAll();
		List<Raca> listRaca = racaRepository.findByTipoAnimalIdOrderByNomeAsc(animal.getRaca()!=null &&animal.getRaca().getTipoAnimal()!=null? animal.getRaca().getTipoAnimal().getId():0);
		
		
		mv.addObject("condominio", unidade.getCondominio()); 
		mv.addObject("unidades", unidade.getCondominio().getUnidades()); 
		mv.addObject("animal", animal); 
		mv.addObject("listTipoAnimal", tipoAnimal); 
		mv.addObject("listCorAnimal", listCorAnimal); 
		mv.addObject("listRaca", listRaca); 
		mv.addObject("unidade",unidade);


		return mv;
	}
	
	
	/**
	 * SALVAR UM NOVO ANIMAL NO BANCO DE DADOS
	 * @param pessoaFisicaUnidade
	 * @param recursos
	 * @param result
	 * @param attributes
	 * @return
	 */
	@PostMapping(value = {"/condominio/animal/novo", "/condominio/animal/novo/{\\d+}"})
	public ModelAndView salvarAnimalUnidade(@Valid Animal animal,
			BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

		boolean isNovo = animal.isNovo();
		
		
		
		if(result.hasErrors()){ 
			return formularioAnimal(animal);
		}		 

	
		List<FotoAnimal> listFotoAnimal =  animal.getListFotoAnimal();
		
		animal = animalService.salvarAnimal(animal);
		
		animalService.salvarFotos(animal,listFotoAnimal);


		if(isNovo){
			attributes.addFlashAttribute("mensagem","Animal cadastrado com sucesso!");
		}
		else{
			attributes.addFlashAttribute("mensagem","Animal editado com sucesso!");
		}
		return new ModelAndView("redirect:/sindico/condominio/editar/"+animal.getUnidade().getCondominio().getId());
	
	
	}
	
	
	
	
	
}