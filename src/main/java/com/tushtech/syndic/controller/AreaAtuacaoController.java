package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.AreaAtuacao;
import com.tushtech.syndic.repository.AreaAtuacaoRepository;
import com.tushtech.syndic.service.AreaAtuacaoService;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;


@Controller
@RequestMapping("/areaatuacao")
public class AreaAtuacaoController {
	
	@Autowired
	private AreaAtuacaoRepository areaAtuacaoRepository; 
	
	@Autowired
	private AreaAtuacaoService areaAtuacaoService;

	@RequestMapping
	public ModelAndView inicio(AreaAtuacao areaAtuacao) {
		
		return formulario(areaAtuacao);
	}
	
	@RequestMapping("/{id}")
	public ModelAndView editar(AreaAtuacao areaAtuacao, RedirectAttributes attributes){
		
		areaAtuacao = areaAtuacaoRepository.findOne(areaAtuacao.getId());
		
		if(areaAtuacao != null && areaAtuacao.getAtivo() != 1 && !areaAtuacao.isNovo()){
			attributes.addFlashAttribute("proibido", " Área de atuação está desativada! <a href=\"/areaatuacao/ativar/" + areaAtuacao.getId() + "\">Deseja Ativá-la ?</a>");
			return new ModelAndView("redirect:/areaatuacao/gerenciar");
		}
		
		if(areaAtuacao==null || areaAtuacao.isNovo()){
			areaAtuacao = new AreaAtuacao();
		}else{			
			areaAtuacao = areaAtuacaoRepository.findOne(areaAtuacao.getId());
		}
		ModelAndView mv = formulario(areaAtuacao);
		return mv;
	}
	
	@PostMapping("/{id}")
	public ModelAndView salvar(@Valid AreaAtuacao areaAtuacao, BindingResult result, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return formulario(areaAtuacao);
		}
		
		try {
			areaAtuacaoService.salvar(areaAtuacao);
		} catch (NomeDuplicadoException e) { 
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return formulario(areaAtuacao);
		}
		
		attributes.addFlashAttribute("mensagem", "Registro salvo com sucesso!");
		return new ModelAndView("redirect:/areaatuacao");
	}
	
	
	@GetMapping("/ativar/{id}")
	public ModelAndView ativar(AreaAtuacao areaAtuacao,RedirectAttributes attributes)
	{
		areaAtuacao = areaAtuacaoService.ativar(areaAtuacao);
		attributes.addFlashAttribute("mensagem", "Registro ativado com sucesso!");
		return new ModelAndView("redirect:/areaatuacao");
	}
	
	@DeleteMapping("/{id}")
	public @ResponseBody Integer desativar(AreaAtuacao areaAtuacao)
	{
		System.err.println("Excluir te teste teste"); 
		areaAtuacaoService.desativar(areaAtuacao);
		return areaAtuacao.getId();
	}	

	//FORMULARIO PARA E EDITAR E SALVAR
	private ModelAndView formulario(AreaAtuacao areaAtuacao) { 
		
		ModelAndView mv = new ModelAndView("/syndic/sistema/areaatuacao/formulario");
		List<AreaAtuacao> listAreaAtuacao =  areaAtuacaoRepository.findByAtivo(1);
		mv.addObject("listAreaAtuacao", listAreaAtuacao);		
		mv.addObject("areaAtuacao",areaAtuacao);
		return mv;
	}
}