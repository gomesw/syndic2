package com.tushtech.syndic.controller;

import java.io.File;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import com.tushtech.syndic.dto.ArquivoDTO;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.Documento;
import com.tushtech.syndic.entity.FotoOrdemServico;
import com.tushtech.syndic.entity.HistoricoOrdemServico;
import com.tushtech.syndic.entity.Orcamento;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.StatusDocumento;
import com.tushtech.syndic.entity.TipoDocumento;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.enumeration.TipoFotoOrdemServicoEnum;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.DocumentoRepository;
import com.tushtech.syndic.repository.FotoOrdemServicoRepository;
import com.tushtech.syndic.repository.HistoricoOrdemServicoRepository;
import com.tushtech.syndic.repository.OrcamentoRepository;
import com.tushtech.syndic.repository.OrdemServicoRepository;
import com.tushtech.syndic.repository.OrdemServicoStatusRepository;
import com.tushtech.syndic.repository.StatusDocumentoRepository;
import com.tushtech.syndic.repository.TipoDocumentoRepository;
import com.tushtech.syndic.service.DocumentoService;
import com.tushtech.syndic.service.MensagemService;
import com.tushtech.syndic.service.OrdemServicoService;
import com.tushtech.syndic.storage.ArquivoStorage;
import com.tushtech.syndic.storage.ArquivoStorageRunnable;

@RestController
@RequestMapping("/arquivo")
public class ArquivoController {

	@Autowired
	private ArquivoStorage arquivoStorage;



	@Autowired
	private CondominioRepository condominioRepository;

	
	@Autowired
	private OrdemServicoStatusRepository ordemServicoStatusRepository;

	@Autowired
	private DocumentoRepository documentoRepository;

	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;

	@Autowired
	private StatusDocumentoRepository  statusDocumentoRepository;

	

	@Autowired
	private OrdemServicoRepository ordemServicoRepository;

	
	@Autowired
	private HistoricoOrdemServicoRepository historicoOrdemServicoRepository;
	
	
	@Autowired
	private DocumentoService  documentoService;

	@Autowired
	private FotoOrdemServicoRepository fotoOrdemServicoRepository;
	
	@Autowired
	private OrcamentoRepository orcamentoRepository;

	@Autowired
	private OrdemServicoService ordemServicoService;
	
	@Autowired
	private MensagemService mensagemService;

	@PostMapping()
	public DeferredResult<ArquivoDTO> upload(@RequestParam("file") MultipartFile[] files,@RequestParam("diretorio") String diretorio,@RequestParam("id") String id) {

		DeferredResult<ArquivoDTO> resultado = new DeferredResult<>();
		String ids[] = id.split(",");

		//Unidade unidade = unidadeRepository.findById(Integer.valueOf(ids[1]));

		//Condominio condominio = unidade.getEnderecoUnidade().getCondominio(); 

		Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, arquivoStorage,File.separator+"ARQUIVOS"+File.separator+diretorio));
		thread.start();

		try { Thread.sleep (1000); } catch (InterruptedException ex) {}

		if(diretorio.equals("ArquivosUnidades")){
			documentoService.adicionarArquivos(ids[0],resultado);
		}


		return resultado;
	}


	@PostMapping("/condominio/{id}")
	public DeferredResult<ArquivoDTO> uploadBacth(
			@RequestParam(name="tipo",defaultValue="0") String tipo, 
			@RequestParam(name="tipodocumento",defaultValue="0") String tipodocumento,
			@RequestParam(name="endereco",defaultValue="0") String endereco,
			@RequestParam(name="subendereco",defaultValue="0") String subendereco,
			@RequestParam(name="unidadehabitacional",defaultValue="0") String unidadehabitacional,
			@RequestParam(name="obs",defaultValue="") String obs,
			@RequestParam("file") MultipartFile[] files,
			@PathVariable("id") Integer id,@AuthenticationPrincipal UsuarioSistema usuario) {



		DeferredResult<ArquivoDTO> resultado = new DeferredResult<>();

		String diretorio = "ArquivosUnidades";

		Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);


		if(Integer.valueOf(tipo)==2) {//enviar um ou vários arquivos para unidades escolhidas

			Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, arquivoStorage,File.separator+"ARQUIVOS"+File.separator+diretorio));
			thread.start();
			try { Thread.sleep (500); } catch (InterruptedException ex) {} 

			documentoService.enviarParaUnidadesEscolidas(tipo, endereco, subendereco, unidadehabitacional, obs, usuario, resultado,
					condominio);
		}
		else {//ENVIAR EM BATCH O NOME DO ARQUIVO TEM QUE SER IGUAL AO NOME DA UNIDADE


			//VERIFICAR SE EXISTE ALGUMA UNIDADE PARA QUE O ARQUIVO SEJA VICULADOS
			Optional<Unidade> uni = documentoService.enviarParaUnidadeComNomeEquivalente(tipo, endereco, subendereco, unidadehabitacional, obs, usuario,
					condominio,files); 


			//SE TIVER ALGUMA UNIDADE CORREPONDENTE AO NOMES SALVA NA PASTA
			if(uni.isPresent()) {
				Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, arquivoStorage,File.separator+"ARQUIVOS"+File.separator+diretorio));
				thread.start();

				try { Thread.sleep (500); } catch (InterruptedException ex) {} 


				TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(Integer.parseInt(tipo));
				StatusDocumento statusDocumento = statusDocumentoRepository.findById(1);

				ArquivoDTO arquivo = (ArquivoDTO)resultado.getResult();


				for (PessoaFisicaUnidade pfunidade :  uni.get().getListaProprietarioUnidade()) {
					documentoService.criarDocumentoInserir(obs, usuario, tipoDocumento, statusDocumento, arquivo, uni.get(),
							pfunidade);
				}


			}
			else {
				resultado.setResult(new ArquivoDTO("0", "0","0"));
			}


		}



		return resultado;
	}


	@PostMapping("/os")
	public DeferredResult<ArquivoDTO> uploadFotoAbertura(@RequestParam("file") MultipartFile[] files,@RequestParam("diretorio") String diretorio,@RequestParam("id") String id) {
		DeferredResult<ArquivoDTO> resultado = new DeferredResult<>();


		Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, arquivoStorage,File.separator+"ARQUIVOS"+File.separator+diretorio));
		thread.start();

		try { Thread.sleep (1000); } catch (InterruptedException ex) {}
		adicionarArquivosOs(id,resultado);


		return resultado;
	}
	
	
	@PostMapping("/andamento/os")
	public DeferredResult<ArquivoDTO> uploadFotoAndamento(@RequestParam("file") MultipartFile[] files,@RequestParam("diretorio") String diretorio,@RequestParam("id") String id) {
		DeferredResult<ArquivoDTO> resultado = new DeferredResult<>();
		
		

		Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, arquivoStorage,File.separator+"ARQUIVOS"+File.separator+"ArquivosOs"));
		thread.start();
		try { Thread.sleep (1000); } catch (InterruptedException ex) {}
		adicionarArquivosAndamentoOs(id,resultado);

		return resultado;
	}
	
	
	
	@PostMapping("/orcamento")
	public DeferredResult<ArquivoDTO> uploadOrcamento(@RequestParam("file") MultipartFile[] files,@RequestParam("diretorio") String diretorio,@RequestParam("id") String id,
			@AuthenticationPrincipal UsuarioSistema usuario) {
		DeferredResult<ArquivoDTO> resultado = new DeferredResult<>();

		if(usuario.getPessoaFisica().isSindico()){
			Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, arquivoStorage,File.separator+"ARQUIVOS"+File.separator+diretorio));
			thread.start();
	
			try { Thread.sleep (1000); } catch (InterruptedException ex) {}
			adicionarArquivosOrcamentoSindico(id,resultado,usuario.getPessoaFisica());
		}
		else{
			Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, arquivoStorage,File.separator+"ARQUIVOS"+File.separator+diretorio));
			thread.start();

			try { Thread.sleep (1000); } catch (InterruptedException ex) {}
			adicionarArquivosOrcamento(id,resultado);
		}


		return resultado;
	}
	
	
	
	
	
	private void adicionarArquivosOs(String id,DeferredResult<ArquivoDTO> arquivoDTO){


		OrdemServico ordemServico = ordemServicoRepository.findOne(Integer.valueOf(id));

		FotoOrdemServico fotoOrdemServico =  new FotoOrdemServico();

		if(ordemServico!=null){

			ArquivoDTO arquivo = (ArquivoDTO)arquivoDTO.getResult();

			fotoOrdemServico.setNome(arquivo.getNome());
			fotoOrdemServico.setContentType(arquivo.getContentType());
			fotoOrdemServico.setOrdemServico(ordemServico);
			fotoOrdemServico.setAtivo(1);
			fotoOrdemServico.setTipoAnexo(TipoFotoOrdemServicoEnum.DURANTEABERTURA.getId());

			fotoOrdemServicoRepository.save(fotoOrdemServico);
		}

	}
	
	private void adicionarArquivosAndamentoOs(String id,DeferredResult<ArquivoDTO> arquivoDTO){


		OrdemServico ordemServico = ordemServicoRepository.findOne(Integer.valueOf(id));

		FotoOrdemServico fotoOrdemServico =  new FotoOrdemServico();

		if(ordemServico!=null){

			ArquivoDTO arquivo = (ArquivoDTO)arquivoDTO.getResult();

			fotoOrdemServico.setNome(arquivo.getNome());
			fotoOrdemServico.setContentType(arquivo.getContentType());
			fotoOrdemServico.setOrdemServico(ordemServico);
			fotoOrdemServico.setAtivo(1);
			fotoOrdemServico.setTipoAnexo(TipoFotoOrdemServicoEnum.DURANTEANDAMENTO.getId());

			fotoOrdemServicoRepository.save(fotoOrdemServico);
		}

	}
	
	private void adicionarArquivosOrcamento(String id,DeferredResult<ArquivoDTO> arquivoDTO){


		OrdemServico ordemServico = ordemServicoRepository.findOne(Integer.valueOf(id));

		Orcamento orcamento =  new Orcamento();

		if(ordemServico!=null){
			
			ArquivoDTO arquivo = (ArquivoDTO)arquivoDTO.getResult();

			orcamento.setNome(arquivo.getNome());
			orcamento.setContentType(arquivo.getContentType());
			orcamento.setOrdemServico(ordemServico);
			orcamento.setAtivo(1);
			orcamento.setSindicoUpload(0);
			orcamentoRepository.save(orcamento);
		}

	}
	
	
	
	private void adicionarArquivosOrcamentoSindico(String id,DeferredResult<ArquivoDTO> arquivoDTO,PessoaFisica pessoa){

		OrdemServico ordemServico = ordemServicoRepository.findOne(Integer.valueOf(id));

		Orcamento orcamento =  new Orcamento();

		if(ordemServico!=null){
			
			for (Orcamento orcamento1 : ordemServico.getListOrcamento()) {
				orcamento1.setAceite(0);
				orcamentoRepository.saveAndFlush(orcamento1);
			}
			
			boolean isjaescolhido = false;
			for (HistoricoOrdemServico hist : ordemServico.getListHistoricoOrdemServico()) {
				
				if(hist.getOrdemServicoStatus().getId()==4 && hist.getDataFim()==null){
					hist.setDataInicio(Timestamp.valueOf(LocalDateTime.now()));
					historicoOrdemServicoRepository.save(hist);
					isjaescolhido = true;
				}
			}
			
			
			
			ArquivoDTO arquivo = (ArquivoDTO)arquivoDTO.getResult();

			orcamento.setNome(arquivo.getNome());
			orcamento.setContentType(arquivo.getContentType());
			orcamento.setOrdemServico(ordemServico);
			orcamento.setAtivo(1);
			orcamento.setAceite(1);
			orcamento.setSindicoUpload(1);

			orcamentoRepository.saveAndFlush(orcamento);
			
			
			orcamento = orcamentoRepository.saveAndFlush(orcamento);
			
			
			//SE O UPLOAD FOR REALIZADO PELO SINDICO JÁ SETA O ORÇAMENTO COMO O ESCOLIDO
			if(!isjaescolhido){
				HistoricoOrdemServico historicoOrdemServico = new HistoricoOrdemServico();
				historicoOrdemServico.setOrdemServico(ordemServico);
				historicoOrdemServico.setOrdemServicoStatus(ordemServicoStatusRepository.getOne(4));
				
				ordemServicoService.salvarNovoHistoricoOS(historicoOrdemServico);
			}
			
			mensagemService.gerarMensagemOrcamentoEscolhido(ordemServico,pessoa);
			
			
		}

	}
	


	@GetMapping("/{id}") 
	public HttpEntity<byte[]> recuperar(@PathVariable Integer id) {

		Documento documento = documentoRepository.findById(id);

		//Condominio condominio = documento.getUnidade().getEnderecoUnidade().getCondominio();



		byte[] arquivo = arquivoStorage.recuperar(documento.getNome(),File.separator+"ARQUIVOS"+File.separator+"ArquivosUnidades");
		HttpHeaders httpHeaders = new HttpHeaders();

		httpHeaders.add("Content-Disposition", "attachment;filename=\""+documento.getTipoDocumento().getNome()+"."+documento.getTipoReduzido()+"\"");

		HttpEntity<byte[]> entity = new HttpEntity<byte[]>(arquivo,httpHeaders); 

		return entity;
	}

	@GetMapping("/arquivoos/{id}") 
	public HttpEntity<byte[]> recuperarAnexoFinanceiro(@PathVariable Integer id) {

		
		FotoOrdemServico fotoOrdemServico = fotoOrdemServicoRepository.getOne(id);
		
		String nome = fotoOrdemServico.getTipoReduzido();
		
		byte[] arquivo = arquivoStorage.recuperar(fotoOrdemServico.getNome(),File.separator+"ARQUIVOS"+File.separator+"ArquivosOs");

		return converteTipoRetorno(nome, arquivo);
	}
	
	
	@GetMapping("/arquivoos/nome/{nome:.*}") 
	public HttpEntity<byte[]> recuperarAnexoFinanceiroPorNome(@PathVariable String nome) {

		
		String[] tipo = nome.split("\\.");
		
		byte[] arquivo = arquivoStorage.recuperar(nome,File.separator+"ARQUIVOS"+File.separator+"ArquivosOs");

		return converteTipoRetorno(tipo[1], arquivo);
	}
	
	
	
	@GetMapping("/arquivoorcamento/{id}") 
	public HttpEntity<byte[]> recuperarAnexoOrcamento(@PathVariable Integer id) {

		
		Orcamento orcamento = orcamentoRepository.getOne(id);
		
		String nome = orcamento.getTipoReduzido();
		
		byte[] arquivo = arquivoStorage.recuperar(orcamento.getNome(),File.separator+"ARQUIVOS"+File.separator+"ArquivosOsOrcamento");

		return converteTipoRetorno(nome, arquivo);
	}
	
	
	@GetMapping("/arquivoorcamento/nome/{nome:.*}") 
	public HttpEntity<byte[]> recuperarAnexoOrcamentoPorNome(@PathVariable String nome) {

		
		String[] tipo = nome.split("\\.");
		
		byte[] arquivo = arquivoStorage.recuperar(nome,File.separator+"ARQUIVOS"+File.separator+"ArquivosOsOrcamento");

		return converteTipoRetorno(tipo[1], arquivo);
	}
	
	
	
	
	public ResponseEntity<byte[]> converteTipoRetorno(String tipo, byte[] doc) {
		if(tipo.equals("pdf")){
			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE) 
					.body(doc);
		}
		else{
			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE) 
					.body(doc);
		}
	}

	@DeleteMapping("/{id}") 
	public void deletar(@PathVariable Integer id) {

		Documento documento = documentoRepository.findById(id);
		arquivoStorage.remover(documento.getNome(),File.separator+"ARQUIVOS"+File.separator+"ArquivosUnidades");
		documento.setNome(null);
		documento.setContentType(null);
		documentoRepository.save(documento); 


	}

	@PutMapping("/os/{nome:.*}") 
	public void deletarPorNomeOs(@PathVariable String nome) {
		arquivoStorage.remover(nome,"ArquivosOs");

	}
	
	
	@DeleteMapping("/os/{id}") 
	public void deletarOs(@PathVariable Integer id) {

		Documento documento = documentoRepository.findById(id);

		arquivoStorage.remover(documento.getNome(),File.separator+"ARQUIVOS"+File.separator+"ArquivosOs");
		documento.setNome(null);
		documento.setContentType(null);
		documentoRepository.save(documento); 


	}

	@PutMapping("/{nome:.*}") 
	public void deletarPorNome(@PathVariable String nome) {
		arquivoStorage.remover(nome,File.separator+"ARQUIVOS"+File.separator+"ArquivosUnidades");

	}
	
	@PutMapping("/orcamento/{nome:.*}") 
	public void deletarPorNomeOrcamento(@PathVariable String nome) {
		
		Orcamento orcamento = orcamentoRepository.findByNome(nome);
		orcamentoRepository.delete(orcamento);
		
		arquivoStorage.remover(nome,File.separator+"ARQUIVOS"+File.separator+"ArquivosOsOrcamento");
		
	}
	@PutMapping("/foto/andamento/{nome:.*}") 
	public void deletarPorNomeFoto(@PathVariable String nome) {
		
		
		FotoOrdemServico foto = fotoOrdemServicoRepository.findByNome(nome);
		fotoOrdemServicoRepository.delete(foto);
		arquivoStorage.remover(nome,File.separator+"ARQUIVOS"+File.separator+"ArquivosOs");
		
	}


	@RequestMapping( method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public int removerArquivo(@RequestBody String parametro) {


		String arr[] = parametro.split("&");

		String id = arr[0].split("=")[1];
		String ids[] = id.split("%2C");


		String diretorio = arr[1].split("=")[1];
		String nome = arr[2].split("=")[1];


		if(diretorio.equals("ArquivosUnidades")){
			documentoService.removerArquivosUnidade(ids[0]);
		}



		return arquivoStorage.remover(nome,id+""+File.separator+diretorio);
	} 
	
	@GetMapping("/girar/anexo/{id}") 
	public HttpEntity<byte[]> girarPorCodigo(@PathVariable Integer id) { 

		FotoOrdemServico documento = fotoOrdemServicoRepository.findById(id);
		
		byte[] arquivo = arquivoStorage.girar90Recuperar(documento.getNome(),File.separator+"ARQUIVOS"+File.separator+"ArquivosOs");

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE) 
				.body(arquivo);
	}
	
	@GetMapping("/giraranti/anexo/{id}") 
	public HttpEntity<byte[]> girarAntiPorCodigo(@PathVariable Integer id) { 

		FotoOrdemServico documento = fotoOrdemServicoRepository.findById(id);
		
		byte[] arquivo = arquivoStorage.girarAnti90Recuperar(documento.getNome(),File.separator+"ARQUIVOS"+File.separator+"ArquivosOs");

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE) 
				.body(arquivo);
	}
	
	
	@GetMapping("/girar/anexoorcamento/{id}") 
	public HttpEntity<byte[]> girarPorCodigoOrcamento(@PathVariable Integer id) { 

		FotoOrdemServico documento = fotoOrdemServicoRepository.findById(id);
		
		
		byte[] arquivo = arquivoStorage.girar90Recuperar(documento.getNome(),File.separator+"ARQUIVOS"+File.separator+"ArquivosOs");

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE) 
				.body(arquivo);
	}
	
	@GetMapping("/giraranti/anexoorcamento/{id}") 
	public HttpEntity<byte[]> girarAntiPorCodigoOrcamento(@PathVariable Integer id) { 

		FotoOrdemServico documento = fotoOrdemServicoRepository.findById(id);
		
		byte[] arquivo = arquivoStorage.girarAnti90Recuperar(documento.getNome(),File.separator+"ARQUIVOS"+File.separator+"ArquivosOs");

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.IMAGE_JPEG_VALUE) 
				.body(arquivo);
	}
	
	

	
	



}
