package com.tushtech.syndic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tushtech.syndic.entity.Login;
import com.tushtech.syndic.repository.LoginRepository;

@Controller
@RequestMapping()
public class AuditoriaController {
	
	
	@Autowired
	LoginRepository loginRepository;
	
	@RequestMapping("/administrador/acessos")
	public ModelAndView getAuditoriaAcessos(){
		
		ModelAndView mv = new ModelAndView("/syndic/auditoria/auditoriaacessos");
		
		
		List<Login> listLogins = loginRepository.findAll();
		
		
		mv.addObject("listLogins",listLogins);
		

		return mv;
		
		
		
	}
	

}
