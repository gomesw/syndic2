package com.tushtech.syndic.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.AvaliacaoOSPerguntaAvaliacao;
import com.tushtech.syndic.entity.AvaliacaoOrdemServico;
import com.tushtech.syndic.entity.HistoricoOrdemServico;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.AvaliacaoOSPerguntaAvaliacaoRepository;
import com.tushtech.syndic.repository.AvaliacaoOrdemServicoRepository;
import com.tushtech.syndic.repository.OrdemServicoStatusRepository;
import com.tushtech.syndic.service.OrdemServicoService;


@Controller
@RequestMapping("/avaliacao")
public class AvaliacaoController {

	@Autowired
	private OrdemServicoStatusRepository ordemServicoStatusRepository;

	@Autowired
	private AvaliacaoOrdemServicoRepository avaliacaoOrdemServicoRepository;
	
	@Autowired
	private AvaliacaoOSPerguntaAvaliacaoRepository avaliacaoOSPerguntaAvaliacaoRepository;
	
	@Autowired
	private OrdemServicoService ordemServicoService;

	@PostMapping("/salvar")
	public ModelAndView salvar(@Valid AvaliacaoOrdemServico avaliacaoOrdemServico, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {

		List<AvaliacaoOSPerguntaAvaliacao> listAvaliacaoOSPerguntaAvaliacao = avaliacaoOrdemServico.getListAvaliacaoOSPerguntaAvaliacao();
		
		avaliacaoOrdemServico.setAvaliador(usuario.getPessoaFisica());
		avaliacaoOrdemServico.setDataAvaliacao(new Timestamp(System.currentTimeMillis()));
		
		avaliacaoOrdemServico = avaliacaoOrdemServicoRepository.save(avaliacaoOrdemServico);
		
		
		for (AvaliacaoOSPerguntaAvaliacao avaliacaoOSPerguntaAvaliacao : listAvaliacaoOSPerguntaAvaliacao) {
			avaliacaoOSPerguntaAvaliacao.setAvaliacaoOrdemServico(avaliacaoOrdemServico);
			
			avaliacaoOSPerguntaAvaliacaoRepository.save(avaliacaoOSPerguntaAvaliacao);
		}
		
		//salvar novo historico e enviar msg
		HistoricoOrdemServico historicoOrdemServico = new HistoricoOrdemServico();

		historicoOrdemServico.setOrdemServico(avaliacaoOrdemServico.getOrdemServico());
		historicoOrdemServico.setOrdemServicoStatus(ordemServicoStatusRepository.findById(11));

		ordemServicoService.salvarNovoHistoricoOS(historicoOrdemServico);
		ordemServicoService.enviarMensagensAoRealizarPesquisaSatisfacao(avaliacaoOrdemServico.getOrdemServico(), usuario);
		//salvar novo historico e enviar msg
		
		attributes.addFlashAttribute("mensagem", "Avaliação realizada com sucesso!");
		return new ModelAndView("redirect:/ordemservico/ficha/"+avaliacaoOrdemServico.getOrdemServico().getId());
	} 

}