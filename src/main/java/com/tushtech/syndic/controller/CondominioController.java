package com.tushtech.syndic.controller;

import static java.nio.file.FileSystems.getDefault;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.util.StringUtils;

import com.tushtech.syndic.dto.DocumentoFiltro;
import com.tushtech.syndic.entity.Atendimento;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.CondominioPacoteServico;
import com.tushtech.syndic.entity.Cor;
import com.tushtech.syndic.entity.Documento;
import com.tushtech.syndic.entity.EnderecoUnidade;
import com.tushtech.syndic.entity.Marca;
import com.tushtech.syndic.entity.Modelo;
import com.tushtech.syndic.entity.PacoteServico;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.PessoaFisicaVeiculo;
import com.tushtech.syndic.entity.PessoaJuridica;
import com.tushtech.syndic.entity.PessoaJuridicaCondominio;
import com.tushtech.syndic.entity.StatusDocumento;
import com.tushtech.syndic.entity.TipoDocumento;
import com.tushtech.syndic.entity.TipoVeiculo;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.Veiculo;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaCondominioEnum;
import com.tushtech.syndic.repository.AtendimentoRepository;
import com.tushtech.syndic.repository.CondominioPacoteServicoRepository;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.CorRepository;
import com.tushtech.syndic.repository.DocumentoRepository;
import com.tushtech.syndic.repository.EnderecoUnidadeRepository;
import com.tushtech.syndic.repository.MarcaRepository;
import com.tushtech.syndic.repository.ModeloRepository;
import com.tushtech.syndic.repository.PacoteServicoRepository;
import com.tushtech.syndic.repository.PessoaFisicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaUnidadeRepository;
import com.tushtech.syndic.repository.PessoaFisicaVeiculoRepository;
import com.tushtech.syndic.repository.PessoaJuridicaCondominioRepository;
import com.tushtech.syndic.repository.StatusDocumentoRepository;
import com.tushtech.syndic.repository.TipoDocumentoRepository;
import com.tushtech.syndic.repository.TipoVeiculoRepository;
import com.tushtech.syndic.repository.UnidadeRepository;
import com.tushtech.syndic.service.CondominioPacoteServicosService;
import com.tushtech.syndic.service.CondominioService;
import com.tushtech.syndic.service.DocumentoService;
import com.tushtech.syndic.service.PessoaFisicaService;
import com.tushtech.syndic.service.PessoaFisicaVeiculoService;
import com.tushtech.syndic.service.VeiculoService;
import com.tushtech.syndic.storage.local.FotoStorageLocal;


@Controller
@RequestMapping
public class CondominioController { 

	@Autowired
	private CondominioRepository condominioRepository;	


	@Autowired
	private CondominioService condominioService;
	
	

	@Autowired
	private UnidadeRepository unidadeRepository;

	@Autowired
	private MarcaRepository marcaRepository;	

	

	@Autowired
	private PessoaFisicaService pessoaFisicaService;



	@Autowired
	private PessoaFisicaCondominioRepository pessoaFisicaCondominioRepository;

	

	@Autowired
	private FotoStorageLocal fotoStorageLocal;

	@Autowired
	private PessoaFisicaUnidadeRepository pessoaFisicaUnidadeRepository;
	
	@Autowired
	private CorRepository corRepository;  

	@Autowired
	private ModeloRepository modeloRepository; 	

	@Autowired
	private TipoVeiculoRepository tipoVeiculoRepository; 	

	
	
	@Autowired
	private VeiculoService veiculoService;
	
	@Autowired
	private PessoaFisicaVeiculoService pessoaFisicaVeiculoService;
	
	
	
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;
	
	
	@Autowired
	private StatusDocumentoRepository statusDocumentoRepository;
	
	@Autowired
	private PessoaFisicaVeiculoRepository pessoaFisicaVeiculoRepository;
	
	
	@Autowired
	private AtendimentoRepository atendimentoRepository;
	
	
	@Autowired
	private EnderecoUnidadeRepository enderecoUnidadeRepository;
	
	
	@Autowired
	private DocumentoService documentoService;
	
	
	
	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Autowired
	private PacoteServicoRepository pacoteServicoRepository;
	
	@Autowired
	private CondominioPacoteServicoRepository condominioPacoteServicoRepository;
	
	@Autowired
	private CondominioPacoteServicosService condominioPacoteServicosService;
	
	@Autowired
	private PessoaJuridicaCondominioRepository pessoaJuridicaCondominioRepository;
	/**
	 * 
	 *  FUNCIONALIDADES PARA REALIZAR CRUD DE CONDOMINIO
	 *
	 */	

	@RequestMapping("/condominio/listar")
	public ModelAndView listarCondominio(@AuthenticationPrincipal UsuarioSistema usuario) {

		ModelAndView mv = new ModelAndView();
		
		
		if(usuario.getPessoaFisica().isAdministradorSistema() || usuario.getPessoaFisica().isAdministradorEmpresa()) {//Administrador do sistema
			List<Condominio> listCondominio = new ArrayList<>();
			
			if(usuario.getPessoaFisica().isAdministradorSistema()){
				listCondominio = condominioRepository.condominioAtivos();
			}else{
				List<PessoaJuridicaCondominio> listPessoaJuridicaCondominio = pessoaJuridicaCondominioRepository.findByPessoaJuridicaId(usuario.getPessoaJuridica().getId());
				for (PessoaJuridicaCondominio pessoaJuridicaCondominio : listPessoaJuridicaCondominio) {
					listCondominio.add(pessoaJuridicaCondominio.getCondominio());
				}
			}
			
			mv = new ModelAndView("/syndic/condominios/listarcondominio");
			mv.addObject("condominios", listCondominio); 
			return mv;
		}
		
		
		
		if(usuario.getPessoaFisica().isSindico()) {//sindico
			List<PessoaFisicaCondominio> listCondominio = new ArrayList<>();
			listCondominio = condominioRepository.condominiosPorSindico(usuario.getPessoaFisica().getId());
			mv = new ModelAndView("/syndic/condominios/listarcondominioporsindico");
			mv.addObject("condominios", listCondominio); 
			return mv;
		}
		
		return mv;

	}
	
	
	
	
	@RequestMapping("condominio/gerenciar")
	public ModelAndView novoCondominio(Condominio condominio,@AuthenticationPrincipal UsuarioSistema usuario) {
		
		condominio = new Condominio();
		condominio.setPessoaJuridica(new PessoaJuridica());
		
		return formularioCondominio(condominio, usuario);
	}
	
	
	public ModelAndView erroCondominio(Condominio condominio,@AuthenticationPrincipal UsuarioSistema usuario) {
		
		return formularioCondominio(condominio, usuario);
	}
	
	
	public ModelAndView formularioCondominio(Condominio condominio,@AuthenticationPrincipal UsuarioSistema usuario) {
		
		ModelAndView mv = new ModelAndView("/syndic/condominios/formulario");
		
		List<Atendimento> listAtendimentos = new ArrayList<>();
		List<PessoaFisicaCondominio> listpessoaFisicaCondominio = new ArrayList<>();
		List<Atendimento> listAtendimentoAberto = new ArrayList<>();
		
				
		List<CondominioPacoteServico> listPacotesCondominio = condominioPacoteServicoRepository.findByCondominioIdAndAtivo(condominio.getId(),1); 
		
		listAtendimentos = atendimentoRepository.findByCondominioIdOrderByTipoAtendimentoPrioridade(condominio.getId());
		 listpessoaFisicaCondominio = pessoaFisicaCondominioRepository.findByCondominioIdAndTipoPessoaFisicaCondominioId(condominio.getId(),TipoPessoaFisicaCondominioEnum.SINDICO.getId());
		
		
		for (Atendimento atendimento : listAtendimentos) {
			if(atendimento.getUltimoStatusAtendimento().getStatusChamado().getId() == 1){
				listAtendimentoAberto.add(atendimento);
			}
		}	
		
		condominio.setPessoaFisicaCondominio(listpessoaFisicaCondominio);
		
		condominio.setListCondominioPacoteServico(listPacotesCondominio);
		
		mv.addObject("condominio",condominio);
		mv.addObject("quantSindicos",listpessoaFisicaCondominio.size());
		mv.addObject("quantAtendimentos", listAtendimentoAberto.size());
		mv.addObject("listAtendimentoAberto", listAtendimentoAberto);

		return mv;
	}
	
	
	@RequestMapping("/condominio/editar/{id}") 
	public ModelAndView editarCondominio(@PathVariable Integer id, Condominio condominio,@AuthenticationPrincipal UsuarioSistema usuario) 
	{
		condominio = condominioRepository.findById(condominio.getId());
		return formularioCondominio(condominio, usuario);
	}
	
	
	
	
	@RequestMapping(value = {"/condominio/novo", "/condominio/novo/{\\d+}"}, method = RequestMethod.POST)
	public ModelAndView salvarCondominio(@Valid Condominio condominio, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {
		
		condominioService.atribuirErros(condominio, result);

		
		
		if(result.hasErrors()){
			return erroCondominio(condominio,usuario);
		}
 
		boolean isnovo = condominio.isNovo();

		Condominio com =  condominioService.salvarCondominio(condominio);
		
		if(isnovo && usuario.getPessoaFisica().isAdministradorEmpresa()){
			PessoaJuridicaCondominio pessoaJuridicaCondominio = new PessoaJuridicaCondominio();
			pessoaJuridicaCondominio.setPessoaJuridica(usuario.getPessoaJuridica());
			pessoaJuridicaCondominio.setCondominio(condominio);
			pessoaJuridicaCondominio.setInicio(Timestamp.valueOf(LocalDateTime.now()));
			pessoaJuridicaCondominio.setPessoaFisicaResponsavel(usuario.getPessoaFisica());
			pessoaJuridicaCondominioRepository.saveAndFlush(pessoaJuridicaCondominio);
		}
		
		
//		if(isnovo){
//			movimentarFoto(com.getNomeFoto(),com.getId());
//		}

		attributes.addFlashAttribute("mensagem"," Condominio cadastrado com sucesso!");
		return new ModelAndView("redirect:/condominio/editar/"+com.getId());
	}
	
	
	
	@RequestMapping("/condominio")
	public ModelAndView dashboardCondominio(@AuthenticationPrincipal UsuarioSistema usuario) {

		ModelAndView mv = new ModelAndView("/syndic/condominios/formulario");
		
		List<PessoaFisicaCondominio> listCondominio = condominioRepository.condominiosPorSindico(usuario.getPessoaFisica().getId());
		int id = listCondominio.get(0).getCondominio().getId();
		
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);
		
		int quantSindicos = pessoaFisicaCondominioRepository.countByCondominioIdAndTipoPessoaFisicaCondominioId(id,TipoPessoaFisicaCondominioEnum.SINDICO.getId());
		
		int quantAtendimentos = atendimentoRepository.countByCondominioIdOrderByTipoAtendimentoPrioridade(id);
		
		
		List<Atendimento> listAtendimentoAberto = new ArrayList<>();
		
		
		for (Atendimento atendimento : condominio.getAtendimentos()) {
			if(atendimento.getUltimoStatusAtendimento().getStatusChamado().getId() == 1){
				listAtendimentoAberto.add(atendimento);
			}
		}		
		
		mv.addObject("condominio",condominio);
		mv.addObject("id",condominio.getId());
		mv.addObject("quantSindicos",quantSindicos);
		mv.addObject("quantUnidades",condominio.getUnidades().size());
		mv.addObject("quantMoradores",condominio.moradoresCondominio().size());
		mv.addObject("quantVeiculos", condominio.veiculosCondominio().size()); 
		mv.addObject("quantAnimais", condominio.animaisCondominio().size()); 
		mv.addObject("quantAtendimentos", quantAtendimentos);
		mv.addObject("listAtendimentoAberto", listAtendimentoAberto);
		
		return mv;
	}	
	
	@RequestMapping("/condominio/pacoteservico/gerenciar/{idCondominio}")
	public ModelAndView pacoteServicoCondominio(@PathVariable("idCondominio") Integer idCondominio, CondominioPacoteServico condominioPacoteServico) {
		
		ModelAndView mv = new ModelAndView("/syndic/condominios/pacoteservico/formularioinclusao");
		
		Condominio condominio = condominioRepository.findById(idCondominio);
		List<PacoteServico> listPacoteServico = pacoteServicoRepository.findByAtivo(1);
		condominioPacoteServico.setCondominio(condominio);
		
		
		List<CondominioPacoteServico> listCondominioPacoteServico = condominioPacoteServicoRepository.findByCondominioIdAndAtivo(condominio.getId(),1);
		
		mv.addObject("listPacoteServico", listPacoteServico);
		mv.addObject("condominioPacoteServico", condominioPacoteServico);
		mv.addObject("listCondominioPacoteServico", listCondominioPacoteServico);

		return mv;
	}
	
	@PostMapping("/condominio/pacoteservico/gerenciar")
	public ModelAndView salvar(@Valid CondominioPacoteServico condominioPacoteServico, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {
		
		
		
		
		if (result.hasErrors()) {
			return pacoteServicoCondominio(condominioPacoteServico.getCondominio().getId(), condominioPacoteServico);
		}	
		
		System.err.println("entroua qui");
		
//		condominioPacoteServico.setInicio(Timestamp.valueOf(LocalDateTime.now()));
		condominioPacoteServico.setAtivo(1);

		//setar a data fim quando for adicionar um novo pactote
		List<CondominioPacoteServico> listCondominioPacoteServico = condominioPacoteServicoRepository.findByCondominioId(condominioPacoteServico.getCondominio().getId());
		for (CondominioPacoteServico condominioPacoteServico2 : listCondominioPacoteServico) {
			if(condominioPacoteServico2.getDataFim() == null){
				condominioPacoteServico2.setDataFim(Timestamp.valueOf(LocalDateTime.now()));
				condominioPacoteServicoRepository.saveAndFlush(condominioPacoteServico2);
			}
		}
		
		condominioPacoteServicoRepository.saveAndFlush(condominioPacoteServico);
		
		attributes.addFlashAttribute("mensagem", "Registro salvo com sucesso!");
		return new ModelAndView("redirect:/condominio/pacoteservico/gerenciar/"+condominioPacoteServico.getCondominio().getId());
	}
	
	@RequestMapping("/condominio/pacoteservico/editar/{id}")
	public ModelAndView pacoteServicoCondominioEditar(@PathVariable Integer id) {
		
		ModelAndView mv = new ModelAndView("/syndic/condominios/pacoteservico/formularioinclusao");
		
		List<PacoteServico> listPacoteServico = pacoteServicoRepository.findByAtivo(1);
		CondominioPacoteServico condominioPacoteServico = condominioPacoteServicoRepository.getOne(id);
		List<CondominioPacoteServico> listCondominioPacoteServico = condominioPacoteServicoRepository.findByCondominioId(condominioPacoteServico.getCondominio().getId());
		
		mv.addObject("listPacoteServico", listPacoteServico);
		mv.addObject("condominioPacoteServico", condominioPacoteServico);
		mv.addObject("listCondominioPacoteServico", listCondominioPacoteServico);

		return mv;
	}
	
	@DeleteMapping("/condominio/pacoteservico/excluir/{id}")
	public @ResponseBody Integer desativar(CondominioPacoteServico condominioPacoteServico)
	{
		condominioPacoteServicosService.desativar(condominioPacoteServico);
		return condominioPacoteServico.getId();
	}
	
	@RequestMapping("administrador/configurar")
	public ModelAndView configurarSistema(@AuthenticationPrincipal UsuarioSistema usuario) {

		ModelAndView mv = new ModelAndView();
			
		mv = new ModelAndView("/syndic/sistema/configuracao");

		return mv;
	}	
	
	@RequestMapping("administrador/mail")
	public ModelAndView teste(@AuthenticationPrincipal UsuarioSistema usuario) {

		ModelAndView mv = new ModelAndView();
			
		mv = new ModelAndView("mail/MessagemSenha");
		return mv;


	}	

	
	
	
	
	@RequestMapping("/condominio/listarveiculos/{id}")
	public ModelAndView getVeiculosUnidades(@PathVariable Integer id) {
		ModelAndView mv = new ModelAndView("/syndic/condominios/veiculo/listar");
		
		Unidade unidade = unidadeRepository.buscarUnidadeCompleta(id);
		
		mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId())); 
		return mv;

	}
	

	@RequestMapping("/condominio/adicionarveiculo/{id}")
	public ModelAndView formVeiculo(@PathVariable Integer id) { 

		ModelAndView mv = new ModelAndView("/syndic/condominios/veiculo/formincluir");
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);

		PessoaFisicaVeiculo pessoaFisicaVeiculo = new PessoaFisicaVeiculo(); 
		List<Modelo> modeloVeiculo = new ArrayList<>();
		
		List<Marca> marcaVeiculo = marcaRepository.findByTipoVeiculoIdOrderByNomeAsc(0);
		List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
		
		List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
		
		mv.addObject("condominio", condominio); 
		mv.addObject("unidades", condominio.getUnidades()); 
		mv.addObject("modeloVeiculo", modeloVeiculo); 
		mv.addObject("marcaVeiculo", marcaVeiculo); 
		mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
		mv.addObject("tipoVeiculo", tipoVeiculo); 
		mv.addObject("cores", cores); 
		
		return mv;

	}
	
	
	@GetMapping("/condominio/editarveiculos/{id}")
	public ModelAndView editarVeiculo(@PathVariable("id") Integer id,PessoaFisicaVeiculo pessoaFisicaVeiculo) 
	{
		

		pessoaFisicaVeiculo = pessoaFisicaVeiculoRepository.findById(id); 
		
		
		
		
		ModelAndView mv = formularioVeiculo(pessoaFisicaVeiculo);
		return mv; 
	}
	
	private ModelAndView formularioVeiculo(PessoaFisicaVeiculo pessoaFisicaVeiculo) {

		
		
		ModelAndView mv = new ModelAndView("/syndic/condominios/veiculo/formincluir");
		
		Unidade unidade = pessoaFisicaVeiculo.getPessoaFisicaUnidade().getUnidade();
		unidade = unidadeRepository.buscarUnidadeCompleta(unidade.getId());
		List<PessoaFisicaUnidade> listPessoaFisicaUnidade = unidade.getListaPessoasAtivasUnidade();
		
		List<Modelo> modeloVeiculo = modeloRepository.findByMarcaIdOrderByNomeAsc(pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getId());
		List<Marca> marcaVeiculo = marcaRepository.findByTipoVeiculoIdOrderByNomeAsc(pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getTipoVeiculo()==null?0:pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getTipoVeiculo().getId());
		List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
		
		List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
		
		mv.addObject("modeloVeiculo", modeloVeiculo); 
		mv.addObject("condominio", unidade.getCondominio()); 
		mv.addObject("unidades", unidade.getCondominio().getUnidades()); 
		mv.addObject("marcaVeiculo", marcaVeiculo); 
		mv.addObject("listPessoaFisicaUnidade", listPessoaFisicaUnidade); 
		mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
		mv.addObject("tipoVeiculo", tipoVeiculo); 
		mv.addObject("cores", cores); 
		mv.addObject("unidade",unidade);


		return mv;
	}
	
	
	/**
	 * SALVAR UM NOVO VEICULO NO BANCO DE DADOS
	 * @param pessoaFisicaUnidade
	 * @param recursos
	 * @param result
	 * @param attributes
	 * @return
	 */
	@PostMapping(value = {"/condominio/veiculo/novo", "/condominio/veiculo/novo/{\\d+}"})
	public ModelAndView salvarMoradorUnidade(@Valid PessoaFisicaVeiculo pessoaFisicaVeiculo,
			BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

		boolean isNovo = pessoaFisicaVeiculo.isNovo();
		
		pessoaFisicaService.atribuirErrosPessoaFisicaVeiculo(pessoaFisicaVeiculo, result); 
		
		
		PessoaFisicaUnidade pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.findById(pessoaFisicaVeiculo.getPessoaFisicaUnidade().getId());
		
		
		if(result.hasErrors()){ 
			return formularioVeiculo(pessoaFisicaVeiculo);
		}		 

		
		if(isNovo){
			pessoaFisicaVeiculo.getVeiculo().setCadastrante(usuario.getPessoaFisica());
		}
		
		pessoaFisicaVeiculo.setTipoPessoaFisicaUnidade(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade());
		
		pessoaFisicaVeiculo.setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());
		
		pessoaFisicaVeiculo.getVeiculo().setCadastrante(usuario.getPessoaFisica());
		
		Veiculo veiculo = veiculoService.salvarVeiculo(pessoaFisicaVeiculo.getVeiculo());

		pessoaFisicaVeiculo.setVeiculo(veiculo);
		
		pessoaFisicaVeiculoService.salvar(pessoaFisicaVeiculo);


		if(isNovo){
			attributes.addFlashAttribute("mensagem","Veículo cadastrado com sucesso!");
		}
		else{
			attributes.addFlashAttribute("mensagem","Veículo editado com sucesso!");
		}
		return new ModelAndView("redirect:/condominio/editar/"+pessoaFisicaVeiculo.getPessoaFisicaUnidade().getUnidade().getCondominio().getId());
	}
	
	
	
	
	//--------------------------------------------------------------------
	
	
	@RequestMapping("/condominio/listaranimais/{id}")
	public ModelAndView getAnimaisUnidades(@PathVariable Integer id) {
		ModelAndView mv = new ModelAndView("/syndic/condominios/animal/listar");
		
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);
		
		mv.addObject("condominio", condominio); 
		return mv;

	}
	

	
	
		
	
	
	//----------------------------------------------------------
	
	
	
	
	
	


//	@RequestMapping("administrador/condominio/editar/{id}") 
//	public ModelAndView editarCondominio(@PathVariable Integer id, Condominio condominio)  
//	{
//		ModelAndView mv = new ModelAndView("/syndic/condominios/incluircondominio");
//
//		condominio = condominioRepository.findOne(id);  
//
//		int listpessoaFisicaCondominio = pessoaFisicaCondominioRepository.countByCondominioIdAndTipoPessoaFisicaCondominioId(id,TipoPessoaFisicaCondominioEnum.SINDICO.getId());
//		List<Unidade> listUnidade = unidadeRepository.buscarUnidadesPorCondominio(id);
//
//
//
//		//mv.addObject("marca", marcaRepository.findAll());
//		int listFuncionariosCondominio = pessoaFisicaCondominioRepository.countByCondominioIdAndTipoPessoaFisicaCondominioId(id,TipoPessoaFisicaCondominioEnum.FUNCIONARIO.getId());
//		
//		
//		mv.addObject("condominio",condominio);
//		mv.addObject("quantSindicos",listpessoaFisicaCondominio);
//		mv.addObject("quantUnidades",listUnidade.size()); 
//		mv.addObject("quantFuncionarios",listFuncionariosCondominio);
//		//mv.addObject("listRecursos",listRecursos);
//
//		return mv;
//	}	
//	
	




	
	
	




	


	private void movimentarFoto(String foto,Integer pasta) {

		String nomePasta = "LogoMarca";

		if(StringUtils.isEmpty(foto) && foto!=null){
			criarPastas(pasta+getDefault().getSeparator()+nomePasta);

		}
		else{
			criarPastas(pasta+getDefault().getSeparator()+nomePasta);

			Path local = getDefault().getPath(fotoStorageLocal.getLocal()+getDefault().getSeparator()+pasta+getDefault().getSeparator()+nomePasta+getDefault().getSeparator());
			Path localTemporario = getDefault().getPath(fotoStorageLocal.getLocal()+getDefault().getSeparator()+getDefault().getSeparator()+"0"+getDefault().getSeparator()+nomePasta+getDefault().getSeparator());

			try {

				Files.move(localTemporario.resolve(foto),local.resolve(foto));
				Files.move(localTemporario.resolve("thumbnail."+foto),local.resolve("thumbnail."+foto));
			} catch (IOException e) {
				throw new RuntimeException("Erro movendo a foto para destino final", e);
			}
		}


	}


	private void criarPastas(String local) {
		try {


			//File file = new File(fotoStorageLocal.getLocal()+getDefault().getSeparator()+local);

			//if(!file.exists()){
			Files.createDirectories(getDefault().getPath(fotoStorageLocal.getLocal()+"",local));
			Files.createDirectories(getDefault().getPath(fotoStorageLocal.getLocal()+getDefault().getSeparator()+local, "temp"));
			//}

			//else  if(file.exists() && !file.isDirectory()){
			//Files.createDirectories(getDefault().getPath(fotoStorageLocal.getLocal()+"",local));
			//Files.createDirectories(getDefault().getPath(fotoStorageLocal.getLocal()+getDefault().getSeparator()+local, "temp"));
			//}
		} catch (IOException e) {
			throw new RuntimeException("Erro criando pasta para salvar foto", e);
		}
	}



	@RequestMapping("/condominio/excluir/{id}")
	public @ResponseBody Condominio excluir(Condominio condominio)
	{  

		condominioService.excluirCondominio(condominio);
		return condominio;
	}	



	


	@RequestMapping("/condominio/perfilrecurso") 
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<String> listaFuncaoUpmLotacao(@RequestParam("nomes[]") String[] nomes, @RequestParam("id") Integer id) {

		condominioService.atualizarPerfilCondominioRecurso(nomes, id);

		return ResponseEntity.ok("ok");
	}
	
	
	
	
	
	/**
	 * TELA PARA ADICIONAR UM DOCUMENTO A UMA UNIDADE
	 * LEMBRANDO QUE O DOCUMENTO É VINCULADO A PESSOAFISICAUNIDADE
	 * @param id DA UNIDADE
	 * @return
	 */
	@RequestMapping("/condominio/documentos/{id}")
	public ModelAndView novoDocumentolUnidade(@PathVariable Integer id) {

		ModelAndView mv = new ModelAndView("/syndic/condominios/documento/incluir");  
		
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);	
		
		List<TipoDocumento> listTipoDocumento = tipoDocumentoRepository.findAll();
		
		List<StatusDocumento> listStatusDocumento = statusDocumentoRepository.findAll(); 
		
		//List<TipoEnderecoUnidade> listTipoEnderecoUnidade = tipoEnderecoUnidadeRepository.findByCondominioId(id);
		
		List<EnderecoUnidade> listEnderecoUnidade = enderecoUnidadeRepository.findByCondominioId(id); 
		
		mv.addObject("unidades", condominio.getUnidades());
		
		mv.addObject("listTipoDocumento", listTipoDocumento); 
		
		//mv.addObject("listTipoEnderecoUnidade", listTipoEnderecoUnidade);
		mv.addObject("listEnderecoUnidade", listEnderecoUnidade); 
		
		mv.addObject("listStatusDocumento", listStatusDocumento);
		mv.addObject("condominio", condominio);
		
		
		return mv;
	}
	
	
	/**
	 * TELA PARA ADICIONAR UM DOCUMENTO A UMA UNIDADE
	 * LEMBRANDO QUE O DOCUMENTO É VINCULADO A PESSOAFISICAUNIDADE
	 * @param id DA UNIDADE
	 * @return
	 */
	@RequestMapping("/condominio/documentos/buscar/{id}")
	public ModelAndView buscarDocumentolUnidade(@PathVariable Integer id,DocumentoFiltro documentoFiltro) {

		ModelAndView mv = new ModelAndView("/syndic/condominios/documento/buscar");  
		
		
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);	
		
		documentoFiltro.setCondominio(condominio);
		
		List<TipoDocumento> listTipoDocumento = tipoDocumentoRepository.findAll();
		
		List<StatusDocumento> listStatusDocumento = statusDocumentoRepository.findAll(); 
		
		//List<TipoEnderecoUnidade> listTipoEnderecoUnidade = tipoEnderecoUnidadeRepository.findByCondominioId(id);
		
		List<EnderecoUnidade> listEnderecoUnidade = enderecoUnidadeRepository.findByCondominioId(id); 
		
		
		mv.addObject("listTipoDocumento", listTipoDocumento); 
		
		//mv.addObject("listTipoEnderecoUnidade", listTipoEnderecoUnidade);
		mv.addObject("listEnderecoUnidade", listEnderecoUnidade); 
		
		mv.addObject("listStatusDocumento", listStatusDocumento);
		
		mv.addObject("documentoFiltro", documentoFiltro);
		
		
		return mv;
	}
	
	
	
	/**
	 * TELA PARA ADICIONAR UM DOCUMENTO A UMA UNIDADE
	 * LEMBRANDO QUE O DOCUMENTO É VINCULADO A PESSOAFISICAUNIDADE
	 * @param id DA UNIDADE
	 * @return
	 */
	@PostMapping("/condominio/documentos/buscar/{id}")
	public ModelAndView recuperarDocumentolUnidade(@Valid DocumentoFiltro documentoFiltro, BindingResult result,RedirectAttributes attributes) {

		ModelAndView mv = new ModelAndView("/syndic/condominios/documento/buscar");  
		
		
		if(documentoService.temErrosBusca(documentoFiltro)){  
			
			return buscarDocumentolUnidade(documentoFiltro.getCondominio().getId(), documentoFiltro);
		}	
		
		
		List<Documento> listDocumentos = new ArrayList<>();
		
		listDocumentos = documentoRepository.aplicarFiltro(documentoFiltro);
		
		List<TipoDocumento> listTipoDocumento = tipoDocumentoRepository.findAll();
		
		List<StatusDocumento> listStatusDocumento = statusDocumentoRepository.findAll(); 
		
		//List<TipoEnderecoUnidade> listTipoEnderecoUnidade = tipoEnderecoUnidadeRepository.findByCondominioId(id);
		
		List<EnderecoUnidade> listEnderecoUnidade = enderecoUnidadeRepository.findByCondominioId(documentoFiltro.getCondominio().getId()); 
		
		mv.addObject("listTipoDocumento", listTipoDocumento); 
		
		//mv.addObject("listTipoEnderecoUnidade", listTipoEnderecoUnidade);
		mv.addObject("listEnderecoUnidade", listEnderecoUnidade); 
		mv.addObject("listStatusDocumento", listStatusDocumento);
		mv.addObject("documentoFiltro", documentoFiltro);
		
		mv.addObject("listDocumentos", listDocumentos);
		
		
		
		return mv;
	}
	

}
