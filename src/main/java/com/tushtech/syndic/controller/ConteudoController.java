package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.Conteudo;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.ConteudoRepository;
import com.tushtech.syndic.repository.TipoConteudoRepository;
import com.tushtech.syndic.service.ConteudoService;

@Controller
@RequestMapping("/conteudo")
public class ConteudoController {
	
	@Autowired
	private ConteudoRepository conteudoRepository; 
	
	@Autowired
	private TipoConteudoRepository tipoConteudoRepository; 	
	
	@Autowired
	private ConteudoService conteudoService;	
	

	@RequestMapping("/listar")
	public ModelAndView listarConteudo(@AuthenticationPrincipal UsuarioSistema usuario) {
		
		ModelAndView mv = new ModelAndView("/syndic/sistema/conteudo/listar");
		
		Condominio cond = usuario.getPessoaFisica().getCondominioAtual();
		
		mv.addObject("listaConteudo", conteudoRepository.findByCondominioId(cond.getId()));

		return mv;
		
	}
	
	@RequestMapping("/incluir")
	public ModelAndView incluirConteudo(Conteudo conteudo) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/conteudo/incluir");
		
		mv.addObject("conteudo", conteudo);
		mv.addObject("listaTipoConteudo", tipoConteudoRepository.findByAtivo(1));
		
		return mv;
	}
	
	@PostMapping("/incluir")
	public ModelAndView salvarConteudo(@Valid Conteudo conteudo, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {
			
		if(result.hasErrors()){
			return incluirConteudo(conteudo);
		}
		
		if(conteudo.isNovo()) {
			attributes.addFlashAttribute("mensagem","Conteudo cadastrado com sucesso!");
		}
		else {
			attributes.addFlashAttribute("mensagem","Conteudo editado com sucesso!");
		}
		
		conteudo.setPessoaFisicaPublica(usuario.getPessoaFisica());
		conteudo.setCondominio(usuario.getPessoaFisica().getCondominioAtual());
		
		conteudoService.salvarConteudo(conteudo);
		
		return new ModelAndView("redirect:/conteudo/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarConteudo(@PathVariable Integer id, Conteudo conteudo)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/conteudo/incluir");

		mv.addObject("conteudo", conteudoRepository.findOne(conteudo.getId()));
		mv.addObject("listaTipoConteudo", tipoConteudoRepository.findByAtivo(1));
		
		return mv;
	}	
	

}