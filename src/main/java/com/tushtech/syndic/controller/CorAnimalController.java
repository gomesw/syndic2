package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.CorAnimal;
import com.tushtech.syndic.repository.CorAnimalRepository;
import com.tushtech.syndic.service.CorAnimalService;

@Controller
@RequestMapping("/coranimal")
public class CorAnimalController {
	
	@Autowired
	private CorAnimalRepository corAnimalRepository; 
	
	@Autowired
	private CorAnimalService corAnimalService;	
	

	@RequestMapping("/listar")
	public ModelAndView getListar(CorAnimal corAnimal) {
		ModelAndView mv = new ModelAndView("/syndic/sistema/coranimal/listar");
		mv.addObject("coranimal", corAnimalRepository.findAll());
		return mv;
	}	
	
	@RequestMapping("/incluir") 
	public ModelAndView novo(CorAnimal corAnimal) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/coranimal/incluir");
		mv.addObject("coranimal", corAnimal);
		return mv;
	}
	
	
	@PostMapping("/incluir")
	public ModelAndView salvar(@Valid CorAnimal corAnimal, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novo(corAnimal);
		}
		
		
		if(corAnimal.isNovo()) {
		attributes.addFlashAttribute("mensagem","Cor de Animal cadastrada com sucesso!");
		}
		else {
			attributes.addFlashAttribute("mensagem","Cor de Animal editada com sucesso!");
		}
		
		corAnimalService.salvar(corAnimal);
		
		return new ModelAndView("redirect:/coranimal/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable Integer id, CorAnimal corAnimal)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/coranimal/incluir");
		
		mv.addObject("coranimal",corAnimalRepository.findOne(corAnimal.getId()));
		
		return mv;
	}	 
//	
//	@RequestMapping("/excluir/{id}")
//	public @ResponseBody TipoAnimal excluir(TipoAnimal tipoAnimal)
//	{  
//		tipoAnimalService.excluirTipoAnimal(tipoAnimal);
//		return tipoAnimal;
//	}	
}