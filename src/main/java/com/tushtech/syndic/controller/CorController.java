package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Cor;
import com.tushtech.syndic.repository.CorRepository;
import com.tushtech.syndic.service.CorService;

@Controller
@RequestMapping("/cor")
public class CorController {
	
	@Autowired
	private CorRepository corRepository; 
	
	@Autowired
	private CorService corService;	
	

	@RequestMapping("/listar")
	public ModelAndView getListar(Cor cor) {
		ModelAndView mv = new ModelAndView("/syndic/sistema/cor/listar");
		mv.addObject("cor", corRepository.findAll());
		return mv;
	}	
	
	@RequestMapping("/incluir") 
	public ModelAndView novo(Cor cor) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/cor/incluir");
		mv.addObject("cor", cor);
		return mv;
	}
	
	
	@PostMapping("/incluir")
	public ModelAndView salvar(@Valid Cor cor, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novo(cor);
		}
		
		corService.salvar(cor);
		
		attributes.addFlashAttribute("mensagem","Cor cadastrada com sucesso!");
		return new ModelAndView("redirect:/cor/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable Integer id, Cor cor)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/cor/incluir");
		
		mv.addObject("cor",corRepository.findOne(cor.getId()));
		
		return mv;
	}	 
//	
//	@RequestMapping("/excluir/{id}")
//	public @ResponseBody TipoAnimal excluir(TipoAnimal tipoAnimal)
//	{  
//		tipoAnimalService.excluirTipoAnimal(tipoAnimal);
//		return tipoAnimal;
//	}	
}