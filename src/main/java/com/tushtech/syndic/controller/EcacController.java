package com.tushtech.syndic.controller;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.dto.AtendimentoMes;
import com.tushtech.syndic.dto.AtendimentoMesStatus;
import com.tushtech.syndic.dto.AtendimentoMesTipo;
import com.tushtech.syndic.dto.AtendimentoQuantidade;
import com.tushtech.syndic.entity.Atendimento;
import com.tushtech.syndic.entity.AtendimentoResposta;
import com.tushtech.syndic.entity.TipoAtendimento;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.enumeration.CoresRGB;
import com.tushtech.syndic.entity.enumeration.MesesDoAno;
import com.tushtech.syndic.repository.AtendimentoRepository;
import com.tushtech.syndic.repository.AtendimentoRespostaRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.repository.TipoAtendimentoRepository;
import com.tushtech.syndic.service.AtendimentoRespostaService;
import com.tushtech.syndic.service.AtendimentoService;

@Controller
@RequestMapping("")
public class EcacController {
	

	@Autowired
	private AtendimentoRepository atendimentoRepository; 

	@Autowired
	private TipoAtendimentoRepository tipoAtendimentoRepository;

	@Autowired 
	private AtendimentoService atendimentoService;

	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository; 
	
	@Autowired
	private AtendimentoRespostaRepository atendimentoRespostaRepository;
	
	@Autowired
	private AtendimentoRespostaService atendimentoRespostaService; 

	
	
	@RequestMapping("/sindico/cac/painelresumo")
	public ModelAndView nova(Atendimento atendimento) {
		
		List<TipoAtendimento> listTipoAtendimento = tipoAtendimentoRepository.findByAtivoAndIdSubordinacaoIsNull(1);
		
		List<AtendimentoQuantidade> listAtendimentoQuantidade = new ArrayList<AtendimentoQuantidade>();
		
		List<Atendimento> atendimentosbanco = atendimentoRepository.findAll();
		
		for (TipoAtendimento atendimentoQuantidade : listTipoAtendimento) {
			
			int cont =0;
			for (Atendimento ate : atendimentosbanco) {
				if(ate.getTipoAtendimento().getIdSubordinacao().getId()==atendimentoQuantidade.getId()) {
					cont++;
				}
			}
			
			AtendimentoQuantidade ata = new AtendimentoQuantidade(atendimentoQuantidade.getId(), atendimentoQuantidade.getNome(), cont);
			listAtendimentoQuantidade.add(ata);
			
		}
		
		ModelAndView mv = new ModelAndView("syndic/cac/painelresumo"); 
		mv.addObject("atendimento", atendimentoRepository.findAll());
		
		mv.addObject("listTipoAtendimento", listAtendimentoQuantidade);
	
		return mv;
	}	
	
	
	@RequestMapping("/sindico/cac/listar")
	public ModelAndView nova(Atendimento atendimento, @AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/layout/ecac/listaratendimento");
		
		if(usuario.getPessoaFisica().isSindico()){
			mv.addObject("atendimento", atendimentoRepository.findAll());			
		}else{
			List<Atendimento> listAtendimento = atendimentoRepository.findByPessoaFisicaId(usuario.getPessoaFisica().getId());
			mv.addObject("atendimento", listAtendimento);
		}

		return mv;
	}

	@RequestMapping("/sindico/cac/pormes")
	public @ResponseBody List<AtendimentoMes> atendimentosMes() {

		List<AtendimentoMes> atendimentos = new ArrayList<AtendimentoMes>();
		atendimentos = atendimentoRepository.totalPorMes();

		return atendimentos;
	}

	@RequestMapping("/sindico/cac/pormespizza")
	public @ResponseBody List<AtendimentoMesStatus> atendimentosMespizza() {

		List<AtendimentoMesStatus> atendimentos = new ArrayList<AtendimentoMesStatus>();
		atendimentos = atendimentoRepository.totalPorMesStatus();

		return atendimentos;
	}

	@RequestMapping("/sindico/cac/pormestipo")
	public @ResponseBody List<AtendimentoMesTipo> atendimentosMesTipo() {

		LocalDate hoje = LocalDate.now();

		List<TipoAtendimento> tipos = tipoAtendimentoRepository.findByAtivoAndIdSubordinacaoIsNull(1);


		List<Atendimento> atendimentos = atendimentoRepository.findAll();

		List<AtendimentoMesTipo> atendimentoMesTipos = new ArrayList<>();
		
		List<String> meses = new ArrayList<>();

		hoje = hoje.minusMonths(6);
		
		for (int i = 1; i <= 7; i++) {
			String mes  =  MesesDoAno.MES.getPorNumero(hoje.getMonthValue())+"/"+hoje.getYear();
			meses.add(mes);
			hoje = hoje.plusMonths(1);
		}
		
		int quantidade[] = new int[meses.size()];



		int contcor = 1;
		for (TipoAtendimento tipo : tipos) {
			AtendimentoMesTipo atendimentoMesTipo = new AtendimentoMesTipo(tipo.getNome(),CoresRGB.COR.getPorNumero(contcor),meses,quantidade);
			atendimentoMesTipos.add(atendimentoMesTipo);
			contcor++;
		}
		

		int indextipo = 0;
		
		for (AtendimentoMesTipo atendimento : atendimentoMesTipos) {
			int quantidadesnovas[] = new int[meses.size()];
			int indexmes = 0;
			for (String mes : atendimento.getMes()) { 
				

				for (Atendimento ate : atendimentos) {
					if((atendimento.getTipo().equals(
							ate.getTipoAtendimento().getIdSubordinacao().getNome())) &&  
							(MesesDoAno.MES.getPorNumero(toLocalDate(ate.getDataCadastro()).getMonthValue())+"/"+toLocalDate(ate.getDataCadastro()).getYear()).equals(mes)) {
						quantidadesnovas[indexmes]++;
						
					}
				}
			
				atendimento.setQuantidade(quantidadesnovas) ;
				
				indexmes++;
			}
			
			indextipo++;
	}
		
		
		return atendimentoMesTipos;
	}

	@RequestMapping("/sindico/cac/incluir")
	public ModelAndView incluir(Atendimento atendimento) {

		ModelAndView mv = new ModelAndView("/layout/ecac/incluiratendimento");
		
		mv.addObject("listTipoAtendimentoPrincipal", tipoAtendimentoRepository.findByAtivoAndIdSubordinacaoIsNull(1));
		mv.addObject("atendimento", atendimento);

		return mv;
	}

	//@Secured("ROLE_INCLUIR_FUNCAO")
	@PostMapping(value="/sindico/cac/incluir")
	public ModelAndView incluir(@Valid Atendimento atendimento, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {


		if(result.hasErrors()){
			return incluir(atendimento);
		}		

		atendimento.setDataCadastro(java.sql.Date.valueOf(LocalDate.now()));
		atendimento.setPessoaFisica(pessoaFisicaRepository.findOne(usuario.getPessoaFisica().getId()));
		atendimento.setCondominio(usuario.getPessoaFisica().getPessoaFisicaCondominio().get(0).getCondominio());
		
		atendimentoService.salvar(atendimento);

		attributes.addFlashAttribute("mensagem"," Atendimento cadastrado com sucesso!");		
		return new ModelAndView("redirect:/atendimento/listar");
	}

	@RequestMapping("/sindico/cac/editar/{id}")
	public ModelAndView editar(@PathVariable Integer id, Atendimento atendimento) 
	{
		ModelAndView mv = new ModelAndView("/layout/ecac/incluiratendimento");

		if(id!=null){
			atendimento = atendimentoRepository.findOne(id);
		}

		mv.addObject("tipoAtendimento", tipoAtendimentoRepository.findAll());
		mv.addObject("atendimento", atendimento);
		return mv;
	}
	
	@RequestMapping("/sindico/cac/resposta/{idatendimento}")
	public ModelAndView enviarResposta(@PathVariable("idatendimento") Integer idAtendimentoAnterior,AtendimentoResposta atendimentoResposta)
	{
		ModelAndView mv = new ModelAndView("/layout/ecac/atendimentoresposta");
		
		Atendimento atendimento = atendimentoRepository.findOne(idAtendimentoAnterior);
		
		atendimentoResposta.setAtendimento(atendimento);
		mv.addObject("listAtendimentoResposta", atendimentoRespostaRepository.findByAtendimentoId(atendimento.getId())); 
		mv.addObject("atendimentoResposta", atendimentoResposta);
		
		return mv;
	}
	
	@RequestMapping(value="/sindico/cac/resposta", method=RequestMethod.POST)
	public ModelAndView enviarResposta(@Valid AtendimentoResposta atendimentoResposta, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {

		if(result.hasErrors()){
			return enviarResposta(atendimentoResposta.getAtendimento().getId(),atendimentoResposta);
		}		

		atendimentoResposta.setAtendimento(atendimentoRepository.findOne(atendimentoResposta.getAtendimento().getId()));
		atendimentoResposta.setDataResposta(new Timestamp(System.currentTimeMillis()));
		atendimentoResposta.setPessoaFisica(usuario.getPessoaFisica());
		
		atendimentoRespostaService.salvar(atendimentoResposta);

		attributes.addFlashAttribute("mensagem"," Mensagem enviada com sucesso!!");		
		return new ModelAndView("redirect:/atendimento/resposta/"+atendimentoResposta.getAtendimento().getId());
	}

	/**
	 * Converte Date para LocalDate
	 *
	 * @param d
	 * @return LocalDate
	 */
	private static LocalDate toLocalDate(Date d) {
		Instant instant = Instant.ofEpochMilli(d.getTime());
		LocalDate localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
		return localDate;
	}
	
	//retorna os subtipos de um tipo de atendimento
	 @RequestMapping(value = "/sindico/cac/listarsubtipoatendimento",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody List<TipoAtendimento> pesquisarSubTipoAtendimento(@RequestParam(name="id",defaultValue="-1") Integer id) {
		 
//		 List<TipoAtendimento> tipoAtendimentoRepository.findByIdSubordinacaoId(id); = tipoAtendimentoRepository.findByIdSubordinacaoId(id);
		 	
		 return tipoAtendimentoRepository.findByIdSubordinacaoId(id);
	 }
	
	
}