package com.tushtech.syndic.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tushtech.syndic.dto.UnidadeDTO;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.EnderecoUnidade;
import com.tushtech.syndic.entity.TipoEnderecoUnidade;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.EnderecoUnidadeRepository;
import com.tushtech.syndic.repository.TipoEnderecoUnidadeRepository;
import com.tushtech.syndic.repository.TipoUnidadeRepository;
import com.tushtech.syndic.repository.UnidadeRepository;
import com.tushtech.syndic.service.EnderecoUnidadeService;
import com.tushtech.syndic.service.TipoEnderecoUnidadeService;
import com.tushtech.syndic.service.UnidadeService;


@Controller
@RequestMapping("/enderecamento")
public class EnderecamentoController {

	@Autowired
	private UnidadeRepository unidadeRepository; 
	
	@Autowired
	private TipoUnidadeRepository tipoUnidadeRepository; 
	
	@Autowired
	private TipoEnderecoUnidadeRepository tipoEnderecoUnidadeRepository;	

	@Autowired
	private EnderecoUnidadeRepository enderecoUnidadeRepository;
	
	@Autowired
	private UnidadeService unidadeService;
	
	@Autowired
	private EnderecoUnidadeService enderecoUnidadeService;
	
	
	@Autowired
	private TipoEnderecoUnidadeService tipoEnderecoUnidadeService;

	@Autowired
	private CondominioRepository condominioRepository;


	@RequestMapping("/incluir/{id}") 
	public ModelAndView listaCondominios(@PathVariable Integer id,@AuthenticationPrincipal UsuarioSistema usuario) {

		
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);
		
		ModelAndView mv = new ModelAndView("/syndic/enderecamento/formularioInclusao");
		
		EnderecoUnidade endereco = new EnderecoUnidade();
		TipoEnderecoUnidade tipoendereco = new TipoEnderecoUnidade();
		
		
		
		mv.addObject("endereco", endereco);
		mv.addObject("tipoendereco", tipoendereco);
		mv.addObject("condominio", condominio);
		
		
		return mv;
	}
	
	
	
	
	@RequestMapping("/excluir/{id}") 
	public ModelAndView exluirCondominios(@PathVariable Integer id,@AuthenticationPrincipal UsuarioSistema usuario) {

		
		EnderecoUnidade enderecoUnidade = enderecoUnidadeRepository.findById(id);
		
		Condominio condominio = enderecoUnidade.getCondominio();
		
		enderecoUnidadeRepository.delete(id);
		
		ModelAndView mv = new ModelAndView("/syndic/enderecamento/formularioInclusao");
		
		EnderecoUnidade endereco = new EnderecoUnidade();
		TipoEnderecoUnidade tipoendereco = new TipoEnderecoUnidade();
		
		mv.addObject("endereco", endereco);
		mv.addObject("tipoendereco", tipoendereco);
		mv.addObject("condominio", condominio);
		
		
		return mv;
	}
	
	
	@RequestMapping(value="/endereco/novo", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public final ResponseEntity<?>  novoEndereco(@RequestBody @Valid EnderecoUnidade enderecoUnidade,BindingResult result) {
		
		//atribui erros ao formulário
		if (result.hasErrors()) {
			String erros ="";
			for (ObjectError e : result.getAllErrors()) {
				erros+="<i class=\"fa fa-exclamation-circle fa-lg\"></i>  <span> "+e.getDefaultMessage()+"</span><br/>";
			}
			return ResponseEntity.badRequest().body(erros);
			
		}
		
		
		//verifica se existe histórico
		EnderecoUnidade isenderecoUnidade = enderecoUnidadeRepository.findByCondominioIdAndDescricaoLike(enderecoUnidade.getCondominio().getId(),enderecoUnidade.getDescricao());
		
		
		
		if(isenderecoUnidade==null){ 
			enderecoUnidadeService.salvarEnderecoUnidade(enderecoUnidade);
		}
		else {
			String erros ="";
				erros+="<i class=\"fa fa-exclamation-circle fa-lg\"></i>  <span> Ops! Esse já está cadastrado. </span><br/>";
			return ResponseEntity.badRequest().body(erros);
		}
		
		
		
		return ResponseEntity.ok("200");
		
	}
	
	
	
	@RequestMapping("/excluirtipo/{id}") 
	public ModelAndView exluirTipoEndereco(@PathVariable Integer id,@AuthenticationPrincipal UsuarioSistema usuario) {

		
		TipoEnderecoUnidade enderecoUnidade = tipoEnderecoUnidadeRepository.findById(id);
		
		Condominio condominio = enderecoUnidade.getCondominio();
		
		tipoEnderecoUnidadeRepository.delete(id);
		
		ModelAndView mv = new ModelAndView("/syndic/enderecamento/formularioInclusao");
		
		EnderecoUnidade endereco = new EnderecoUnidade();
		TipoEnderecoUnidade tipoendereco = new TipoEnderecoUnidade();
		
		mv.addObject("endereco", endereco);
		mv.addObject("tipoendereco", tipoendereco);
		mv.addObject("condominio", condominio);
		
		
		return mv;
	}
	
	
	@RequestMapping(value="/tipoendereco/novo", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public final ResponseEntity<?>  novoTipoEndereco(@RequestBody @Valid TipoEnderecoUnidade tipoEnderecoUnidade,BindingResult result) {
		
		//atribui erros ao formulário
		if (result.hasErrors()) {
			String erros ="";
			for (ObjectError e : result.getAllErrors()) {
				erros+="<i class=\"fa fa-exclamation-circle fa-lg\"></i>  <span> "+e.getDefaultMessage()+"</span><br/>";
			}
			return ResponseEntity.badRequest().body(erros);
			
		}
		
		
		//verifica se existe histórico
		TipoEnderecoUnidade isenderecoUnidade = tipoEnderecoUnidadeRepository.findByCondominioIdAndDescricaoLike(tipoEnderecoUnidade.getCondominio().getId(),tipoEnderecoUnidade.getDescricao());
		
		
		
		if(isenderecoUnidade==null){ 
			tipoEnderecoUnidadeService.salvarTipoEnderecoUnidade(tipoEnderecoUnidade);
		}
		else {
			String erros ="";
				erros+="<i class=\"fa fa-exclamation-circle fa-lg\"></i>  <span> Ops! Esse já está cadastrado. </span><br/>";
			return ResponseEntity.badRequest().body(erros);
		}
		
		
		
		return ResponseEntity.ok("200");
		
	}
	
	
	@RequestMapping(value = "/pesquisarporendereco/{idcondominio}/",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody List<TipoEnderecoUnidade> pesquisarTipoEnderecoporEndereco(@PathVariable("idcondominio") Integer condominio,@RequestParam(name="id",defaultValue="-1") Integer id) {
		 
		
		Condominio condo = condominioRepository.findById(condominio);
		List<TipoEnderecoUnidade> listRetorno  = new ArrayList<>();

		for (Unidade unidade : condo.getUnidades()) {
			
			if(unidade.getEnderecoUnidade().getId()==id) {
				listRetorno.add(unidade.getTipoEnderecoUnidade());
			}
		}
		
		return listRetorno;
		 	
	 }
	
	
	@RequestMapping(value = "/pesquisarportipoendereco/{idcondominio}/",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody List<UnidadeDTO> pesquisarUnidadePorTipo(@PathVariable("idcondominio") Integer condominio,@RequestParam(name="id",defaultValue="-1") Integer id) {
		 
		
		Condominio condo = condominioRepository.findById(condominio);
		List<UnidadeDTO> listRetorno  = new ArrayList<>();
		
		

		for (Unidade unidade : condo.getUnidades()) {
			
			if(unidade.getTipoEnderecoUnidade().getId()==id) {
				UnidadeDTO und = new UnidadeDTO(unidade.getId(),unidade.getUnidadeHabitacionalDescricao());
				listRetorno.add(und);
			}
		}
		
		return listRetorno;
		 	
	 }
	
	
		

}	



