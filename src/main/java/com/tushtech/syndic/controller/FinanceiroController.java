package com.tushtech.syndic.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tushtech.syndic.entity.HistoricoFinanceiroContasReceber;
import com.tushtech.syndic.entity.TipoContasReceber;
import com.tushtech.syndic.repository.FinanceiroRepository;
import com.tushtech.syndic.repository.TipoContasReceberRepository;

@Controller
@RequestMapping("/financeiro")
public class FinanceiroController {
	
	@Autowired
	private FinanceiroRepository financeiroRepository;
	
	
	@Autowired
	private TipoContasReceberRepository tipoContasReceberRepository;
	
	@RequestMapping
	public ModelAndView getPrincipal() {
		ModelAndView mv = new ModelAndView("/layout/financeiro/financeiro");
		return mv;
		
	}
	
	
	@RequestMapping("/listarcontasreceber")
	public ModelAndView listarContasReceber() {

		ModelAndView mv = new ModelAndView("/layout/financeiro/listarcontasreceber");
		
		List<HistoricoFinanceiroContasReceber> listaHistoricoFinanceiro = financeiroRepository.findAll();
		
//		Condominio condominio = condominioRepository.findOne(id);
		
		mv.addObject("dataAtual", LocalDate.now());
		mv.addObject("listaHistoricoFinanceiro", listaHistoricoFinanceiro);
		
		return mv;
	}
	
	
	@RequestMapping("/inlcuircontasreceber/{id}")
	public ModelAndView contasReceber(@PathVariable Integer id, HistoricoFinanceiroContasReceber historicoFinanceiro) {

		ModelAndView mv = new ModelAndView("/layout/financeiro/incluircontasreceber");
		
		//Condominio condominio = condominioRepository.findOne(id);
		
		List<TipoContasReceber> listaTipos = tipoContasReceberRepository.findAll();
		
		historicoFinanceiro = (historicoFinanceiro==null?new HistoricoFinanceiroContasReceber():historicoFinanceiro);
		
		mv.addObject("dataAtual", LocalDate.now());
		mv.addObject("historicoFinanceiro", historicoFinanceiro);
		mv.addObject("listaTipos", listaTipos);
		
		return mv;
	}

}
