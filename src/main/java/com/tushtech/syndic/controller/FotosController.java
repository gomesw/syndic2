package com.tushtech.syndic.controller;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.tushtech.syndic.dto.ArquivoDTO;
import com.tushtech.syndic.dto.FotoDTO;
import com.tushtech.syndic.entity.Animal;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.FotoAnimal;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.AnimalRepository;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.FotoAnimalRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.storage.ArquivoStorage;
import com.tushtech.syndic.storage.ArquivoStorageRunnable;
import com.tushtech.syndic.storage.FotoStorage;
import com.tushtech.syndic.storage.FotoStorageRunnable;

@RestController
@RequestMapping("/foto")
public class FotosController {
	
	@Autowired
	private FotoStorage fotoStorage;
	
	@Autowired
	private ArquivoStorage arquivoStorage;  


	@Autowired 
	private AnimalRepository animalRepository;
	
	@Autowired
	private CondominioRepository condominioRepository;
	
	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;
	
	@Autowired
	private FotoAnimalRepository fotoAnimalRepository;

	
	@PostMapping
	public DeferredResult<FotoDTO> upload(@RequestParam("files[]") MultipartFile[] files,@RequestParam("diretorio") String diretorio,@RequestParam("id") String id) {
		DeferredResult<FotoDTO> resultado = new DeferredResult<>();
		
		String ids[] = id.split(",");
		
		Thread thread = new Thread(new FotoStorageRunnable(files, resultado, fotoStorage,File.separator+"ARQUIVOS"+File.separator+diretorio));
		thread.start();
		
		try { Thread.sleep (1000); } catch (InterruptedException ex) {}
		if(diretorio.equals("LogoMarca")){
			adicionarLogoCondominio(ids[0],resultado);
		}
		
		if(diretorio.equals("FotoPessoa")){
			
			adicionarFotoPessoa(ids[1].toString(),resultado);
		}
		
		
		
		return resultado;
	}
	
	
	@PutMapping("/animal/{idcondominio}/{nome:.*}") 
	public void deletarFotoAnimalPorNome(@PathVariable String nome,@PathVariable String idcondominio) {
		arquivoStorage.remover(nome,File.separator+"ARQUIVOS"+File.separator+"FotoAnimal");

	}
	
	
	@DeleteMapping("/animal/{idcondominio}/{id}") 
	public void deletarFotoAnimalPorId(@PathVariable Integer id,@PathVariable String idcondominio) {
		
		FotoAnimal foto = fotoAnimalRepository.findById(id);
		
		arquivoStorage.remover(foto.getNome(),File.separator+"ARQUIVOS"+File.separator+"FotoAnimal");
		
		removerFotoAnimal(foto);

	}
	
	
	@GetMapping("/animal/{id}") 
	public HttpEntity<byte[]> recuperarFotoAnimal(@PathVariable Integer id) { 

		FotoAnimal foto = fotoAnimalRepository.findById(id);

		byte[] arquivo = arquivoStorage.recuperar(foto.getNome(),File.separator+"ARQUIVOS"+File.separator+"FotoAnimal");
		HttpHeaders httpHeaders = new HttpHeaders();

		httpHeaders.add("Content-Disposition", "attachment;filename=\""+foto.getAnimal().getRaca().getTipoAnimal().getNome().replaceAll(" ","")+"."+foto.getTipoReduzido()+"\"");

		HttpEntity<byte[]> entity = new HttpEntity<byte[]>(arquivo,httpHeaders); 

		return entity;
	}
	
	@PostMapping("/animal")
	public DeferredResult<ArquivoDTO> uploadFinanceiro(
			@RequestParam("file") MultipartFile[] files,
			@RequestParam("diretorio") String diretorio,
			@RequestParam("id") String id,
			@RequestParam("idcondominio") String idcondominio) {
		DeferredResult<ArquivoDTO> resultado = new DeferredResult<>();

		
		Thread thread = new Thread(new ArquivoStorageRunnable(files, resultado, arquivoStorage,File.separator+"ARQUIVOS"+File.separator+diretorio));
		thread.start();

		try { Thread.sleep (1000); } catch (InterruptedException ex) {}
		adicionarFotosAnimais(id,resultado);


		return resultado;
	}
	
	private void adicionarFotosAnimais(String id,DeferredResult<ArquivoDTO> arquivoDTO){


		Animal animal =  animalRepository.findOne(Integer.valueOf(id));

		FotoAnimal  foto =  new FotoAnimal();

		if(animal!=null){

			ArquivoDTO arquivo = (ArquivoDTO)arquivoDTO.getResult();

			foto.setNome(arquivo.getNome());
			foto.setContentType(arquivo.getContentType());
			foto.setAnimal(animal);
			fotoAnimalRepository.save(foto);
		}

	}
	
	
	@GetMapping("/{diretorio}/{id}/{nome:.*}")
	public byte[] recuperar(@PathVariable String nome,@PathVariable String diretorio,@PathVariable String id) {
		
		return fotoStorage.recuperar(nome,File.separator+"ARQUIVOS"+File.separator+diretorio);
	}
	
	
	@GetMapping("/condominio/{id}")
	public byte[] fotoLogoCondominio(
			@PathVariable Integer id) {
		
		Condominio condominio = condominioRepository.findById(id);
		return fotoStorage.recuperar(condominio.getNomeFoto(),File.separator+"ARQUIVOS"+File.separator+"LogoMarca");
	}
	
	
	@RequestMapping( method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public int removerFoto(@RequestBody String parametro) {
		
		
		String arr[] = parametro.split("&");
		
		String id = arr[0].split("=")[1];
		String ids[] = id.split("%2C");
		
		
		String diretorio = arr[1].split("=")[1];
		String nome = arr[2].split("=")[1];
		
		
		if(diretorio.equals("LogoMarca")){
			removerLogoCondominio(ids[0]);
		}
		

		if(diretorio.equals("FotoPessoa")){
			
			removerFotoPessoa(ids[1]);
		}
		
		return fotoStorage.remover(nome,"ARQUIVOS"+File.separator+diretorio);
	} 
	
	private void removerLogoCondominio(String id){
		Condominio condominio = condominioRepository.findOne(Integer.valueOf(id));
		if(condominio!=null) {
			condominio.setNomeFoto(null);
			condominio.setContentType(null);
			condominioRepository.save(condominio);
		}

	}
	
	private void removerFotoPessoa(String id){
		PessoaFisica pf = pessoaFisicaRepository.findOne(Integer.valueOf(id));
		if(pf!=null) {
			pf.setNomeFoto(null);
			pf.setContentType(null);
			pessoaFisicaRepository.save(pf);
		}

	}
	
	
	private void removerFotoAnimal(FotoAnimal foto){
		
		
		if(foto!=null) {
			fotoAnimalRepository.delete(foto);
		}

	}
	
	
	private void adicionarLogoCondominio(String id,DeferredResult<FotoDTO> FotoDTO){
		Condominio condominio = condominioRepository.findOne(Integer.valueOf(id));
		
		FotoDTO foto = (FotoDTO)FotoDTO.getResult();
		
		if(condominio!=null) {
			condominio.setNomeFoto(foto.getNome());
			condominio.setContentType(foto.getContentType());
			condominioRepository.save(condominio);
		}
		
	}
	
	private void adicionarFotoPessoa(String id,DeferredResult<FotoDTO> FotoDTO){
		PessoaFisica pf = pessoaFisicaRepository.findOne(Integer.valueOf(id));
		
		FotoDTO foto = (FotoDTO)FotoDTO.getResult();
		
		if(pf!=null) {
			pf.setNomeFoto(foto.getNome());
			pf.setContentType(foto.getContentType());
			pessoaFisicaRepository.save(pf);
		}
		
	}
	
	
	
	
	
	
	@RequestMapping("/embranco") 
	public ModelAndView semFoto() 
	{
		ModelAndView mv = new ModelAndView("/syndic/fotos/semfoto");

		return mv;
	}
	
	
	@RequestMapping(value="/comfoto/{diretorio}/{id}/{nome:.*}",method = RequestMethod.GET) 
	public ModelAndView comFoto(@PathVariable String nome,@PathVariable String diretorio,@PathVariable String id) 
	{
		ModelAndView mv = new ModelAndView("/syndic/fotos/comfoto");
		mv.addObject("url","/foto/"+diretorio+"/"+id+"/"+nome);

		return mv;
	}
	
	
	@RequestMapping(value="/fotografar",method = RequestMethod.GET) 
	public void fotografar(@AuthenticationPrincipal UsuarioSistema usuarioSistema) 
	{
		
		//try { Thread.sleep (500); } catch (InterruptedException ex) {}
		
		
//		Webcam webcam = Webcam.getDefault();
//		webcam.close();
//		webcam.open();
		
//		String nome = UUID.randomUUID().toString();
//		
//		try {
//			ImageIO.write(webcam.getImage(), "PNG", new File(defaultDirectory()+File.separator+"temp"+File.separator+nome+".png"));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		webcam.close();
//		
//		File foto = new File(defaultDirectory()+File.separator+"temp"+File.separator+nome+".png");
//		if (!foto.exists()) {
//			System.out.println("Foto não encontrada");
//		}
//		
//		else {
//			fotoStorage.moverWebCamPasta(nome+".png","1/FotoPessoa");
//			adicionarFotoPessoaWebCam(String.valueOf(usuarioSistema.getPessoaFisica().getId()),nome+".png","image/png");
//		}
		
		
		
		
	}
	
	
//	private void adicionarFotoPessoaWebCam(String id,String foto, String contenType){
//		PessoaFisica pf = pessoaFisicaRepository.findOne(Integer.valueOf(id));
//		
//		if(pf!=null) {
//			pf.setNomeFoto(foto);
//			pf.setContentType(contenType);
//			pessoaFisicaRepository.save(pf);
//		}
//		
//	}
	
	
	
	public void salvar(MultipartFile[] files,String diretorio,String id,UsuarioSistema usuarioSistema) {
		DeferredResult<FotoDTO> resultado = new DeferredResult<>();
		
		String ids[] = id.split(",");
		
		Thread thread = new Thread(new FotoStorageRunnable(files, resultado, fotoStorage,"ARQUIVOS"+File.separator+diretorio));
		thread.start();
		
		try { Thread.sleep (1000); } catch (InterruptedException ex) {}
		if(diretorio.equals("LogoMarca")){
			adicionarLogoCondominio(ids[0],resultado);
		}
		
		if(diretorio.equals("FotoPessoa")){
			
			adicionarFotoPessoa(ids[1].toString(),resultado);
		}
		
		
	}
	
	
	
	
	
	private static String defaultDirectory()
	{
	    String OS = System.getProperty("os.name").toUpperCase();
	    
	    String nome = "";
	    
	    if (OS.contains("WIN"))
	    	nome = System.getenv("APPDATA");
	    else if (OS.contains("MAC"))
	    	nome = System.getProperty("user.home") + "/Library/Application "
	                + "Support";
	    else if (OS.contains("NUX"))
	    	nome = System.getProperty("user.home");
	   
	    return nome;
	}


}
