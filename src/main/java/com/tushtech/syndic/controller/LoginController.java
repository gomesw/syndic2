package com.tushtech.syndic.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tushtech.syndic.entity.UsuarioSistema;

@Controller
@RequestMapping("/")	
public class LoginController {
	
	
	
	@RequestMapping("login")
	public String login(@AuthenticationPrincipal UsuarioSistema user) {
		
		
		
		if (user != null) {
			return "redirect:/";
		}
		
		return "/syndic/login";
	}
		

}
