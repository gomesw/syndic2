package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Marca;
import com.tushtech.syndic.entity.TipoVeiculo;
import com.tushtech.syndic.repository.MarcaRepository;
import com.tushtech.syndic.repository.TipoVeiculoRepository;
import com.tushtech.syndic.service.MarcaService;

@Controller
@RequestMapping("/marca")
public class MarcaController {
	
	@Autowired
	private MarcaRepository marcaRepository; 
	
	
	@Autowired
	private TipoVeiculoRepository tipoVeiculoRepository; 
	
	
	@Autowired
	private MarcaService marcaService;	
	

	@RequestMapping("/listar")
	public ModelAndView marca() {
		ModelAndView mv = new ModelAndView("/syndic/sistema/marca/listar");
		mv.addObject("marca", marcaRepository.findAll());
		return mv;
	}	
	
	@RequestMapping("/incluir")
	public ModelAndView novoMarca(Marca marca) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/marca/incluir");
		
		List<TipoVeiculo> listaTipoVeiculo  = tipoVeiculoRepository.findAll();
		
		mv.addObject("listaTipoVeiculo", listaTipoVeiculo);
		mv.addObject("marca", marca);
		return mv;
	}
	
	@PostMapping("/incluir")
	public ModelAndView salvarTipoAnimal(@Valid Marca marca, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novoMarca(marca);
		}
		
		marcaService.salvarMarca(marca);
		
		if(marca.isNovo()) {
		attributes.addFlashAttribute("mensagem","Marca cadastrada com sucesso!");
		}
		else {
			attributes.addFlashAttribute("mensagem","Marca editada com sucesso!");
		}
		return new ModelAndView("redirect:/marca/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarCondominio(@PathVariable Integer id, Marca marca)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/marca/incluir");
		
		List<TipoVeiculo> listaTipoVeiculo  = tipoVeiculoRepository.findAll();
		
		mv.addObject("listaTipoVeiculo", listaTipoVeiculo);
		mv.addObject("marca",marcaRepository.findOne(marca.getId()));
		
		return mv;
	}	 
	
	 @RequestMapping(value = "/pesquisarmarcas",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody List<Marca> pesquisarModelosPorMarca(@RequestParam(name="id",defaultValue="-1") Integer tipo) {
		 
		 	return marcaRepository.findByTipoVeiculoIdOrderByNomeAsc(tipo);
		 	
	 }	
}