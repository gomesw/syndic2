package com.tushtech.syndic.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Mensagem;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaOrdemServico;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.MensagemRepository;
import com.tushtech.syndic.repository.OrdemServicoRepository;
import com.tushtech.syndic.repository.PessoaFisicaOrdemServicoRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.service.MensagemService;
import com.tushtech.syndic.service.OrdemServicoService;

@Controller
@RequestMapping("/mensagem")
public class MensagemController {
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	
	@Autowired
	private PessoaFisicaOrdemServicoRepository pessoaFisicaOrdemServicoRepository;
	
	@Autowired
	private MensagemService mensagemService;
	
	@Autowired
	private OrdemServicoRepository ordemServicoRepository;
	
	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;
	
	@Autowired
	private OrdemServicoService ordemServicoService;
	
	@RequestMapping
	public ModelAndView listar(@AuthenticationPrincipal UsuarioSistema usuario) { 		
		ModelAndView mv = new ModelAndView("/syndic/mensagem/listar"); 
		
		List<Mensagem> listMensagem = mensagemRepository.findByDestinatarioIdAndAtivoOrderByDataInclusaoDesc(usuario.getPessoaFisica().getId(), 1);
		
		mv.addObject("listMensagem", listMensagem);
		
		return mv;
	}
	
	@RequestMapping("/novamensagem")
	public ModelAndView novaMensagem(Mensagem mensagem) { 
		
		ModelAndView mv = new ModelAndView("/syndic/mensagem/novamensagem");
		
		List<PessoaFisica> listaPessoas = pessoaFisicaRepository.findByAtivoOrderByNome(1);
		
		mv.addObject("mensagem", mensagem);
		mv.addObject("listaPessoas", listaPessoas);
		
		return mv;
	}
	
	@RequestMapping(value = {"/novamensagem"}, method = RequestMethod.POST)
	public ModelAndView novaMensagemSalvar(@Valid Mensagem mensagem, BindingResult result, RedirectAttributes attribute, @AuthenticationPrincipal UsuarioSistema usuario) {

		mensagemService.atribuirErros(mensagem, result);
		
		if(result.hasErrors()){
			return novaMensagem(mensagem);
		}
		
		mensagem.setRemetente(usuario.getPessoaFisica());
		mensagem.setDataInclusao(Date.valueOf(LocalDate.now()));
		mensagem.setAtivo(1);
		mensagem.setAlerta(0);
		
		mensagemRepository.saveAndFlush(mensagem);
			
		attribute.addFlashAttribute("mensagem"," Mensagem enviada com sucesso!");
		return new ModelAndView("redirect:/mensagem");
	}
	
	@RequestMapping("/ler/{id}")
	public ModelAndView leitura(@PathVariable int id, @AuthenticationPrincipal UsuarioSistema usuario) { 
		
		ModelAndView mv = new ModelAndView("/syndic/mensagem/ler");
		
		Mensagem mensagem = mensagemRepository.findById(id);
		
		mensagemService.marcarLida(mensagem);
		
		mv.addObject("mensagem", mensagem);
		
		return mv;
	}
	
	@RequestMapping(value = "/responsavel",consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Mensagem>  mensagemPessoa(@AuthenticationPrincipal UsuarioSistema usuario) { 
		
		List<Mensagem> mensagens = new ArrayList<Mensagem>(); 
		mensagens  = mensagemRepository.findByDestinatarioIdAndDataRecebimentoIsNullAndAtivoOrderByDataInclusaoDesc(usuario.getPessoaFisica().getId(), 1);
		
		return mensagens;
	}
	
	@RequestMapping(value = "/marcarlidapopup/{idmensagem}",consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody void mensagemPessoa(@PathVariable String idmensagem) { 
		
		Mensagem mensagem = mensagemRepository.findById(Integer.valueOf(idmensagem));
		if(mensagem != null){ 
			mensagemService.marcarLidaPopUp(mensagem);
		}
	}
	
	
	@GetMapping("/ordemservico/ficha/{id}/{idmsg}")
	public ModelAndView ficah(@PathVariable int idmsg,@PathVariable int id) {

		Mensagem msg = mensagemRepository.findById(idmsg);
		mensagemService.marcarLida(msg);
		
		return  new ModelAndView("redirect:/ordemservico/ficha/"+id);

	}
	
	@GetMapping("/funcionario/ordemservico/ficha/{id}/{idmsg}")
	public ModelAndView fichaFuncionario(@PathVariable int idmsg,@PathVariable int id) {

		PessoaFisicaOrdemServico pessoaFisicaOrdemServico = pessoaFisicaOrdemServicoRepository.findById(id);
		Mensagem msg = mensagemRepository.findById(idmsg);
		
		//marca que aquela pessoa está ciente da ordem de serviço
		pessoaFisicaOrdemServico.setCiente(true);
		pessoaFisicaOrdemServicoRepository.save(pessoaFisicaOrdemServico);
		
		mensagemService.marcarLida(msg);
		
		return  new ModelAndView("redirect:/ordemservico/ficha/"+pessoaFisicaOrdemServico.getOrdemServico().getId());
	}
	
	 
	@RequestMapping(value = "/modal/{idmsg}",consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Mensagem>  mensagemAlertaPessoa(@AuthenticationPrincipal UsuarioSistema usuario,@PathVariable String idmsg) { 
		 List<Mensagem> mensagens = new ArrayList<Mensagem>(); 
		 
		 mensagens  = mensagemRepository.findByDestinatarioIdAndAtivoAndAlertaOrderByDataInclusaoDesc(usuario.getPessoaFisica().getId(), 1, 0);
		 
		 for (Mensagem mensagem : mensagens) {
			 
			 mensagem.setAssunto(mensagem.getAssuntoReduzida(30));
			 mensagem.setObservacao(mensagem.getObservacaoReduzida(120));
			 
			 if(mensagem.getCondominio()==null || mensagem.getCondominio().getNomeFoto()==null){
				 mensagem.setImagem("/assets/img/default-avatar.png"); 
				 
			 }
			 else{ 
				 mensagem.setImagem("/foto/condominio/"+mensagem.getCondominio().getId());
			 }
			
		 }
		 
		 usuario.setMensagens(idmsg);
		 
		 return mensagens;
	}
	
	
	@RequestMapping(value = "/notificarorcamento/sindico", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer inserirResponsavelOS(@RequestParam(name="idOs") int idOs ,@AuthenticationPrincipal UsuarioSistema usuario)
	{
		
		OrdemServico os = ordemServicoRepository.findById(idOs);
		
		mensagemService.gerarMensagemNotificacaoOrcamento(os,usuario.getPessoaFisica());
		OrdemServico ordemServico = ordemServicoRepository.findById(idOs);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);
		return os.getId();
	}
	
	
	
	@RequestMapping(value = "/alerta/ler",consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody void mensagemAlertaPessoaLer(@RequestParam(name="id") int id, @AuthenticationPrincipal UsuarioSistema usuario) { 

		Mensagem mensagem = mensagemRepository.getOne(id);
		mensagem.setAlerta(1);
			
		mensagemRepository.saveAndFlush(mensagem);
	}
	
}
