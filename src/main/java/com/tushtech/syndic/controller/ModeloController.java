package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Modelo;
import com.tushtech.syndic.repository.MarcaRepository;
import com.tushtech.syndic.repository.ModeloRepository;
import com.tushtech.syndic.service.ModeloService;

@Controller
@RequestMapping("/modelo")
public class ModeloController {
	
	@Autowired
	private ModeloRepository modeloRepository; 
	
	@Autowired
	private MarcaRepository marcaRepository;	
	
	@Autowired
	private ModeloService modeloService;	
	

	@RequestMapping("/listar")
	public ModelAndView getListarModelo() {
		ModelAndView mv = new ModelAndView("/syndic/sistema/modelo/listar");
		
		mv.addObject("modelo", modeloRepository.buscarModelosMarcas());
		return mv;
		
	}	
	
	@RequestMapping("/incluir")
	public ModelAndView incluir(Modelo modelo) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/modelo/incluir");
				
		mv.addObject("modelo", modelo);
		mv.addObject("listMarcas", marcaRepository.findAll());
		
		return mv;
	}
	
	//@Secured("ROLE_INCLUIR_FUNCAO")
	@PostMapping(value="/incluir")
	public ModelAndView incluir(@Valid Modelo modelo, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return incluir(modelo);
		}
		
		modeloService.salvar(modelo);
		
		attributes.addFlashAttribute("mensagem"," Modelo cadastrado com sucesso!");
		return new ModelAndView("redirect:/modelo/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarCondominio(@PathVariable Integer id, Modelo modelo)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/modelo/incluir");
		
		mv.addObject("modelo",modeloRepository.findOne(id));
		mv.addObject("listMarcas",marcaRepository.findAll());
		
		return mv;
	}	 
	
	
//	@RequestMapping("/excluir/{id}")
//	public @ResponseBody Modelo excluir(Modelo modelo)
//	{  
//		modeloService.excluirModelo(modelo);
//		return modelo;
//	}
//	
	 //retorna todas as cidades de um determinado estado
	 @RequestMapping(value = "/pesquisarmodelos",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody List<Modelo> pesquisarModelosPorMarca(@RequestParam(name="id",defaultValue="-1") Integer marca) {
		 
		 	return modeloRepository.findByMarcaIdOrderByNomeAsc(marca);
		 	
	 }	
}