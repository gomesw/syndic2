package com.tushtech.syndic.controller;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tushtech.syndic.entity.HistoricoOrdemServico;
import com.tushtech.syndic.entity.Orcamento;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.HistoricoOrdemServicoRepository;
import com.tushtech.syndic.repository.OrcamentoRepository;
import com.tushtech.syndic.repository.OrdemServicoRepository;
import com.tushtech.syndic.repository.OrdemServicoStatusRepository;
import com.tushtech.syndic.service.MensagemService;
import com.tushtech.syndic.service.OrdemServicoService;



@Controller
@RequestMapping("/orcamento")
public class OrcamentoController {
	
	@Autowired
	private MensagemService mensagemService;
	
	@Autowired
	private OrdemServicoRepository ordemServicoRepository; 
	
	@Autowired
	private OrcamentoRepository orcamentoRepository;
	
	@Autowired
	private OrdemServicoStatusRepository ordemServicoStatusRepository;
	
	@Autowired
	private HistoricoOrdemServicoRepository historicoOrdemServicoRepository;
	
	
	@Autowired
	private OrdemServicoService ordemServicoService;
	
	@GetMapping("/{id}")
	public ModelAndView orcamento(@PathVariable int id,@AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/orcamento/detalheorcamento");
		
		OrdemServico ordemServico = ordemServicoRepository.findById(id);
		
		List<Orcamento> ocamentosSindico = new ArrayList<>();
		List<Orcamento> ocamentosEmpresa = new ArrayList<>();
		List<Orcamento> ocamentos = new ArrayList<Orcamento>(); 
		
		for (int i = 0; i < 4; i++) {
			ocamentos.add(new Orcamento());
			
		}
		
		
		for (Orcamento orcamento : ordemServico.getListOrcamento()) {
			if (orcamento.getSindicoUpload()==1) {
				ocamentosSindico.add(orcamento);
			}
			else {
				ocamentosEmpresa.add(orcamento);
			}
			
		}
		
		
		
		int cont = 0;
		for (Orcamento orcamento : ocamentosEmpresa) {
			ocamentos.add(cont,orcamento);
			cont++;
		}
		
		if(!ocamentosSindico.isEmpty()) {
			ocamentos.add(3,ocamentosSindico.get(0));
		}
		
		
		ordemServico.setListOrcamento(ocamentos);
		
		if(usuario.getPessoaFisica().isSindico()) {
			mv.addObject("orcamentos", ocamentosSindico);
		}
		else {
			mv.addObject("orcamentos", ocamentosEmpresa);
		}
		
		mv.addObject("ordemServico", ordemServico);
		
		return mv;
	}
	
	
	
	
	@RequestMapping(value = "/selecionar", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer selecionarOrcamento(@RequestParam(name="idOrcamento") int idOrcamento,@AuthenticationPrincipal UsuarioSistema usuario)
	{			
		
		Orcamento orcamento = orcamentoRepository.findById(idOrcamento);
		
		
		OrdemServico ordemServico = ordemServicoRepository.findById(orcamento.getOrdemServico().getId());
		
		for (Orcamento orcamento1 : ordemServico.getListOrcamento()) {
			orcamento1.setAceite(0);
			orcamentoRepository.saveAndFlush(orcamento1);
		}
		
		orcamento.setAceite(1);
		orcamentoRepository.saveAndFlush(orcamento);
		
		boolean isjaescolhido = false;
		for (HistoricoOrdemServico hist : ordemServico.getListHistoricoOrdemServico()) {
			
			if(hist.getOrdemServicoStatus().getId()==4 && hist.getDataFim()==null){
				hist.setDataInicio(Timestamp.valueOf(LocalDateTime.now()));
				historicoOrdemServicoRepository.save(hist);
				isjaescolhido = true;
			}
		}
		
		if(!isjaescolhido){
			HistoricoOrdemServico historicoOrdemServico = new HistoricoOrdemServico();
			historicoOrdemServico.setOrdemServico(ordemServico);
			historicoOrdemServico.setOrdemServicoStatus(ordemServicoStatusRepository.getOne(4));
			
			ordemServicoService.salvarNovoHistoricoOS(historicoOrdemServico);
		}
		
		mensagemService.gerarMensagemOrcamentoEscolhido(ordemServico,usuario.getPessoaFisica());
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);
			
		return 1;
	}
	
	
	

}