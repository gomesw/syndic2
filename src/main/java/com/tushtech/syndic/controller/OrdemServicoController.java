package com.tushtech.syndic.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.AnexoOrdemServico;
import com.tushtech.syndic.entity.AvaliacaoOrdemServico;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.FotoOrdemServico;
import com.tushtech.syndic.entity.HistoricoOrdemServico;
import com.tushtech.syndic.entity.Mensagem;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.entity.OrdemServicoStatus;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaFisicaOrdemServico;
import com.tushtech.syndic.entity.PessoaJuridica;
import com.tushtech.syndic.entity.PessoaJuridicaCondominio;
import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisica;
import com.tushtech.syndic.entity.TipoOrdemServico;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.enumeration.TipoFotoOrdemServicoEnum;
import com.tushtech.syndic.entity.enumeration.TipoServicoIdentificado;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.FotoOrdemServicoRepository;
import com.tushtech.syndic.repository.HistoricoOrdemServicoRepository;
import com.tushtech.syndic.repository.MensagemRepository;
import com.tushtech.syndic.repository.OrdemServicoRepository;
import com.tushtech.syndic.repository.OrdemServicoStatusRepository;
import com.tushtech.syndic.repository.PerguntaAvaliacaoRepository;
import com.tushtech.syndic.repository.PessoaFisicaOrdemServicoRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.repository.PessoaJuridicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaJuridicaPessoaFisicaRepository;
import com.tushtech.syndic.repository.PessoaJuridicaRepository;
import com.tushtech.syndic.repository.TipoOrdemServicoRepository;
import com.tushtech.syndic.service.HistoricoOrdemServicoService;
import com.tushtech.syndic.service.MensagemService;
import com.tushtech.syndic.service.OrdemServicoService;


@Controller
@RequestMapping("/ordemservico")
public class OrdemServicoController {

	@Autowired
	private OrdemServicoRepository ordemServicoRepository; 

	@Autowired
	private OrdemServicoService ordemServicoService;

	@Autowired
	private CondominioRepository condominioRepository;

	@Autowired
	private TipoOrdemServicoRepository tipoOrdemServicoRepository;

	@Autowired
	private OrdemServicoStatusRepository ordemServicoStatusRepository;

	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;

	@Autowired
	private FotoOrdemServicoRepository fotoOrdemServicoRepository;

	@Autowired
	private PessoaFisicaOrdemServicoRepository pessoaFisicaOrdemServicoRepository;

	@Autowired
	private PerguntaAvaliacaoRepository perguntaAvaliacaoRepository;
	
	@Autowired
	private MensagemService mensagemService;
	
	@Autowired
	private PessoaJuridicaRepository pessoaJuridicaRepository;
	
	@Autowired
	private PessoaJuridicaPessoaFisicaRepository pessoaJuridicaPessoaFisicaRepository;
	
	@Autowired
	private PessoaJuridicaCondominioRepository pessoaJuridicaCondominioRepository;
	
	@Autowired
	private HistoricoOrdemServicoRepository historicoOrdemServicoRepository;

	@Autowired
	private MensagemRepository mensagemRepository;
	
	@Autowired
	private HistoricoOrdemServicoService historicoOrdemServicoService;
	
	@RequestMapping
	private ModelAndView lista(@AuthenticationPrincipal UsuarioSistema usuario) { 

		ModelAndView mv = new ModelAndView("/syndic/ordemservico/listar");
		List<Condominio> listCondominio = new ArrayList<>();
		List<OrdemServico> listOrdemServico = new ArrayList<>();
		List<OrdemServicoStatus> listOrdemServicoStatus = ordemServicoStatusRepository.findAll();

		//verificar se é sindico ou o gestor da empresa
		if(usuario.getPessoaFisica().isSindico()){
			Condominio condominio = condominioRepository.getOne(usuario.getPessoaFisica().getCondominioAtual().getId());
			listCondominio.add(condominio);
			List<PessoaFisicaCondominio> listCondominioDoSindico = new ArrayList<>();
			listCondominioDoSindico = condominioRepository.condominiosPorSindico(usuario.getPessoaFisica().getId());
			int id = listCondominioDoSindico.get(0).getCondominio().getId();
			
			listOrdemServico =  ordemServicoRepository.findByCondominioIdOrderByEmergencia(id);

		}else if (usuario.getPessoaFisica().isAdministradorEmpresa() && usuario.getPessoaJuridica().getId()!=1) {
			
			List<PessoaJuridicaCondominio> listPessoaJuridicaCondominio = pessoaJuridicaCondominioRepository.findByPessoaJuridicaId(usuario.getPessoaJuridica().getId());
			
			for (PessoaJuridicaCondominio pessoaJuridicaCondominio : listPessoaJuridicaCondominio) {
				listCondominio.add(pessoaJuridicaCondominio.getCondominio());
			}
			
			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
		}else{

			listCondominio = condominioRepository.findByAtivo(1);
			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
		}
		
		
		for (OrdemServico os : listOrdemServico) {
			os.getCondominio().getPessoaJuridica().setNomeFantasia(SyndicController.deAccent(os.getCondominio().getPessoaJuridica().getNomeFantasia()).toUpperCase());
			
			os.getTipoOrdemServico().setNome(SyndicController.deAccent(os.getTipoOrdemServico().getNome()).toUpperCase());
			
			os.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().setNome(SyndicController.deAccent(os.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().getNome()).toUpperCase());
			
			os.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().setNome(SyndicController.deAccent(os.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome()).toUpperCase());
			
			
			for (PessoaFisica pf : os.getResponsaveisExecucaoOS()) {
				pf.setNome(SyndicController.deAccent(pf.getNome()).toUpperCase());
			}
		}

		FotoOrdemServico fotoOrdemServico = new FotoOrdemServico();

		mv.addObject("listOrdemServico", listOrdemServico);	
		mv.addObject("fotoOrdemServico", fotoOrdemServico);
		mv.addObject("listCondominio", listCondominio);
		mv.addObject("listOrdemServicoStatus", listOrdemServicoStatus); 
		
		return mv;
	}

	@GetMapping("/{id}")
	public ModelAndView inicio(OrdemServico ordemServico, @AuthenticationPrincipal UsuarioSistema usuario) {
		return formulario(ordemServico, usuario);
	}

	
	@GetMapping("/editardescricaohistorico/{id}")
	public ModelAndView editarDescricaoOrdemServico(HistoricoOrdemServico historicoOrdemServico) {
		return formularioHistorico(historicoOrdemServico);
	}
	
	private ModelAndView formularioHistorico(HistoricoOrdemServico historicoOrdemServico) { 

		ModelAndView mv = new ModelAndView("/syndic/ordemservico/editarhistorico");
		
		HistoricoOrdemServico hist = historicoOrdemServicoRepository.findById(historicoOrdemServico.getId());
		
		mv.addObject("historicoOrdemServico", hist); 

		return mv;
	}
	
	@PostMapping("/editardescricaohistorico/{id}")
	public ModelAndView editarDescricaoOrdemServicoSalvar(@Valid HistoricoOrdemServico historicoOrdemServico, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {
		
		HistoricoOrdemServico hist = historicoOrdemServicoRepository.findById(historicoOrdemServico.getId());
		historicoOrdemServico.setDataInicio(hist.getDataInicio());
		historicoOrdemServico.setDataFim(hist.getDataFim());
		
		if (result.hasErrors()) {
			return formularioHistoricoEdicao(historicoOrdemServico);
		}
		
		historicoOrdemServicoService.salvar(historicoOrdemServico);
		
		attributes.addFlashAttribute("mensagem", "Registro salvo com sucesso!");
		
		return new ModelAndView("redirect:/ordemservico/ficha/"+historicoOrdemServico.getOrdemServico().getId()); 
	} 
	
	private ModelAndView formularioHistoricoEdicao(HistoricoOrdemServico historicoOrdemServico) { 

		ModelAndView mv = new ModelAndView("/syndic/ordemservico/editarhistorico");
		
		mv.addObject("historicoOrdemServico", historicoOrdemServico); 

		return mv;
	}
	
	@RequestMapping("/finalizadas")
	private ModelAndView listaFinalizadas(@AuthenticationPrincipal UsuarioSistema usuario) { 

		ModelAndView mv = new ModelAndView("/syndic/ordemservico/listarfinalizadas");
		List<Condominio> listCondominio = new ArrayList<>();
		List<OrdemServico> listOrdemServico = new ArrayList<>();
		List<OrdemServicoStatus> listOrdemServicoStatus = ordemServicoStatusRepository.findAll();

		//verificar se é sindico ou o gestor da empresa
		if(usuario.getPessoaFisica().isSindico()){
			Condominio condominio = condominioRepository.getOne(usuario.getPessoaFisica().getCondominioAtual().getId());
			listCondominio.add(condominio);
			List<PessoaFisicaCondominio> listCondominioDoSindico = new ArrayList<>();
			listCondominioDoSindico = condominioRepository.condominiosPorSindico(usuario.getPessoaFisica().getId());
			int id = listCondominioDoSindico.get(0).getCondominio().getId();
			listOrdemServico =  ordemServicoRepository.findByCondominioIdOrderByEmergencia(id);

		}else if (usuario.getPessoaFisica().isAdministradorEmpresa()) {
			
			List<PessoaJuridicaCondominio> listPessoaJuridicaCondominio = pessoaJuridicaCondominioRepository.findByPessoaJuridicaId(usuario.getPessoaJuridica().getId());
			
			for (PessoaJuridicaCondominio pessoaJuridicaCondominio : listPessoaJuridicaCondominio) {
				listCondominio.add(pessoaJuridicaCondominio.getCondominio());
			}
			
			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
		}else{

			listCondominio = condominioRepository.findByAtivo(1);
			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
		}
		
		
		for (OrdemServico os : listOrdemServico) {
			os.getCondominio().getPessoaJuridica().setNomeFantasia(SyndicController.deAccent(os.getCondominio().getPessoaJuridica().getNomeFantasia()).toUpperCase());
			
			os.getTipoOrdemServico().setNome(SyndicController.deAccent(os.getTipoOrdemServico().getNome()).toUpperCase());
			
			os.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().setNome(SyndicController.deAccent(os.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().getNome()).toUpperCase());
			
			os.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().setNome(SyndicController.deAccent(os.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome()).toUpperCase());
			
			
			for (PessoaFisica pf : os.getResponsaveisExecucaoOS()) {
				pf.setNome(SyndicController.deAccent(pf.getNome()).toUpperCase());
			}
		}

		FotoOrdemServico fotoOrdemServico = new FotoOrdemServico();
		
		
		List<OrdemServico> listOrdemServicoResultado = new ArrayList<>();
		
		for (OrdemServico ordemServico : listOrdemServico) {
//			if(ordemServico.isFinalizada() || ordemServico.isGarantia()) {
			if(ordemServico.isFinalizada()) {
				listOrdemServicoResultado.add(ordemServico);
			}
			
		}

		mv.addObject("listOrdemServico", listOrdemServicoResultado);	
		mv.addObject("fotoOrdemServico", fotoOrdemServico);
		mv.addObject("listCondominio", listCondominio);
		mv.addObject("listOrdemServicoStatus", listOrdemServicoStatus); 
		
		return mv;
	}
	
	
	//FORMULARIO PARA E EDITAR E SALVAR

	private ModelAndView formulario(OrdemServico ordemServico, @AuthenticationPrincipal UsuarioSistema usuario) { 

		ModelAndView mv = new ModelAndView("/syndic/ordemservico/formulario");
		List<Condominio> listCondominio = new ArrayList<>();

		if(usuario.getPessoaFisica().isSindico()){
			Condominio condominio = condominioRepository.getOne(usuario.getPessoaFisica().getCondominioAtual().getId());
			listCondominio.add(condominio);
			
			List<PessoaJuridicaCondominio> listPessoaJuridicaCondominio =  pessoaJuridicaCondominioRepository.findByCondominioId(usuario.getPessoaFisica().getCondominioAtual().getId());			
			mv.addObject("listPessoaJuridicaCondominio", listPessoaJuridicaCondominio);	
			
		}else if (usuario.getPessoaFisica().isAdministradorEmpresa()) {
			List<PessoaJuridicaCondominio> listPessoaJuridicaCondominio = pessoaJuridicaCondominioRepository.findByPessoaJuridicaId(usuario.getPessoaJuridica().getId());
			
			for (PessoaJuridicaCondominio pessoaJuridicaCondominio : listPessoaJuridicaCondominio) {
				listCondominio.add(pessoaJuridicaCondominio.getCondominio());
			}
			
			PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica = pessoaJuridicaPessoaFisicaRepository.findByPessoaFisicaId(usuario.getPessoaFisica().getId());  
			
			mv.addObject("pessoaJuridicaPessoaFisica", pessoaJuridicaPessoaFisica);
		}

		List<TipoOrdemServico> listTipoOrdemServico = tipoOrdemServicoRepository.findByTipoOrdemServicoSubordinacaoIsNullAndAtivo(1);
		FotoOrdemServico fotoOrdemServico = new FotoOrdemServico();

		mv.addObject("listTipoOrdemServico", listTipoOrdemServico);	
		mv.addObject("ordemServico", ordemServico); 
		mv.addObject("listCondominio", listCondominio);	
		mv.addObject("fotoOrdemServico", fotoOrdemServico);
		return mv;
	}

//filtro os
	@PostMapping
	public ModelAndView filtraOS(@RequestParam(name="condominio") String idCondominio, @RequestParam(name="status") String status, @AuthenticationPrincipal UsuarioSistema usuario) { 
		
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/listar");
		
//		System.err.println(idCondominio);
//		System.err.println(status); 
		
		int intStatus = 0;
		int intCondominio = 0;
		
		if(idCondominio != ""){
			intCondominio = Integer.parseInt(idCondominio);			
		}
		
		if(status != ""){
			intStatus =  Integer.parseInt(status);
		}
		
		List<Condominio> listCondominio = new ArrayList<>();
		List<OrdemServico> listOrdemServico = new ArrayList<>();
		List<OrdemServicoStatus> listOrdemServicoStatus = ordemServicoStatusRepository.findAll();

		//verificar se é sindico ou o gestor da empresa
		if(usuario.getPessoaFisica().isSindico()){
			Condominio condominio = condominioRepository.getOne(usuario.getPessoaFisica().getCondominioAtual().getId());
			listCondominio.add(condominio);
			List<PessoaFisicaCondominio> listCondominioDoSindico = new ArrayList<>();
			listCondominioDoSindico = condominioRepository.condominiosPorSindico(usuario.getPessoaFisica().getId());
			int id = listCondominioDoSindico.get(0).getCondominio().getId();
			
//			listOrdemServico =  ordemServicoRepository.findByCondominioIdOrderByEmergencia(id);
			listOrdemServico =  ordemServicoRepository.osPorCondominioStatus(intCondominio, intStatus);

		}else if (usuario.getPessoaFisica().isAdministradorEmpresa()) {
			listCondominio = condominioRepository.findByAtivo(1);

//			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
			listOrdemServico =  ordemServicoRepository.osPorCondominioStatus(intCondominio, intStatus);
		}else{

			listCondominio = condominioRepository.findByAtivo(1);
//			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
			listOrdemServico =  ordemServicoRepository.osPorCondominioStatus(intCondominio, intStatus);
		}

		FotoOrdemServico fotoOrdemServico = new FotoOrdemServico();

		mv.addObject("listOrdemServico", listOrdemServico);	
		mv.addObject("fotoOrdemServico", fotoOrdemServico);
		mv.addObject("listCondominio", listCondominio);
		mv.addObject("listOrdemServicoStatus", listOrdemServicoStatus); 
		mv.addObject("condominio", idCondominio); 
		mv.addObject("status", status); 
		
		return mv;
	}

	@PostMapping("/{id}")
	public ModelAndView salvar(@Valid OrdemServico ordemServico, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {
		
		if (result.hasErrors()) {
			return formulario(ordemServico, usuario);
		}

		List<FotoOrdemServico> listFotoOS = new ArrayList<>();

		//carregar anexos no historico
		carregarAnexoOS(ordemServico, listFotoOS);

		ordemServico.setPessoaFisica(usuario.getPessoaFisica());
		ordemServicoService.salvar(ordemServico);
		
		ordemServicoService.enviarMensagensAoSalvar(ordemServico,usuario);
		
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);

		persistirAnexos(listFotoOS, ordemServico);

		attributes.addFlashAttribute("mensagem", "Registro salvo com sucesso!");
		return new ModelAndView("redirect:/ordemservico");
	} 

	private void persistirAnexos(List<FotoOrdemServico> anexosOS, OrdemServico ordemServico) {
		for(FotoOrdemServico anexo:	anexosOS){

			if(anexo.getNome()!=null && !anexo.getNome().isEmpty()){
				anexo.setOrdemServico(ordemServico);
				anexo.setAtivo(1);
				anexo.setTipoAnexo(TipoFotoOrdemServicoEnum.DURANTEABERTURA.getId());
				fotoOrdemServicoRepository.save(anexo);
			}
		}
	}

	private void carregarAnexoOS(OrdemServico ordemServico, List<FotoOrdemServico> anexosOs) {

		for(FotoOrdemServico anexos:	ordemServico.getListFotoOrdemServico()){

			if(anexos.getNome()!=null){
				anexos.setOrdemServico(ordemServico);
				anexosOs.add(anexos);
			}

		}
	}



	@GetMapping("/detalhar/{id}")
	public ModelAndView inicio(@PathVariable int id,@AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/detalheordemservico");

		OrdemServico ordemServico = ordemServicoRepository.findOne(id);
		
          PessoaJuridica pj = pessoaJuridicaRepository.findById(usuario.getPessoaJuridica().getId());
		
		List<PessoaFisica> listFuncionario = new ArrayList<>();
		
		if(pj != null){
			listFuncionario = pj.getFuncionarios();
		}
				
		mv.addObject("ordemServico", ordemServico); 
		mv.addObject("listFuncionario",listFuncionario);
		mv.addObject("listServicoIdentificado", TipoServicoIdentificado.values());

		return mv;
	}
	
	
	@GetMapping("/anexo/{id}")
	public ModelAndView anexo(@PathVariable int id,@AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/anexo/detalheanexo");
		OrdemServico ordemServico = ordemServicoRepository.findById(id);
		List<AnexoOrdemServico> anexos = new ArrayList<AnexoOrdemServico>(); 
		
		for (int i = 0; i < 5; i++) {
			anexos.add(new AnexoOrdemServico());
			
		}
		
		for (AnexoOrdemServico anexo : ordemServico.getAnexosOrdemServico()) {
				anexos.add(anexo);
		}
			mv.addObject("anexos", anexos);
			mv.addObject("ordemServico", ordemServico); 
		
		return mv;
	}
	
	
	
	
	@GetMapping("/foto/{id}")
	public ModelAndView orcamento(@PathVariable int id,@AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/anexo/detalhefoto");
		OrdemServico ordemServico = ordemServicoRepository.findById(id);
		
		List<FotoOrdemServico> fotos = new ArrayList<>();
		
		for (int i = 0; i <= 4; i++) {
			fotos.add(new FotoOrdemServico());
		}
		
		int cont = 0;
		
		
		for (FotoOrdemServico foto : ordemServico.getFotosAndamentoOs()) {
			fotos.add(cont,foto);
			cont++;
		}
		
		
		ordemServico.setListFotoOrdemServico(fotos);
	    mv.addObject("fotos", fotos);
	    mv.addObject("ordemServico", ordemServico);
		
		
		return mv;
	}
	
	
	@GetMapping("/detalhar/{id}/{idos}")
	public ModelAndView marcarVisitado(@PathVariable int id,@PathVariable int idos) {
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/ficha");

		OrdemServico ordemServico = ordemServicoRepository.findOne(idos);
		
		Condominio condominio = condominioRepository.findById(ordemServico.getCondominio().getId());
		
		mv.addObject("ordemServico", ordemServico); 
		mv.addObject("condominio", condominio); 
		mv.addObject("listFuncionario", pessoaFisicaRepository.findByAtivoOrderByNome(1));
		mv.addObject("listServicoIdentificado", TipoServicoIdentificado.values());

		return mv;
	}
	
	@GetMapping("/ficha/{id}")
	public ModelAndView ficha(@PathVariable int id,@AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/ficha");
		
		List<Mensagem> listMensagem = mensagemRepository.findByDestinatarioIdAndDataRecebimentoIsNull(usuario.getPessoaFisica().getId());
		
		for (Mensagem mensagem : listMensagem) {
			
			String acao[] = mensagem.getAcao().split("/");
			
			if(Integer.valueOf(acao[acao.length - 2]).equals(id)){
				mensagemService.marcarLida(mensagem);
			} 
		}
		
		
		OrdemServico ordemServico = ordemServicoRepository.findOne(id);
		
		marcarComoCiente(usuario, ordemServico);
		
		PessoaJuridica pj = pessoaJuridicaRepository.findById(ordemServico.getEmpresa().getId());
		
		
		List<PessoaFisica> listFuncionario =  new ArrayList<>();
		List<PessoaJuridicaPessoaFisica> listPessoaJuridicaPessoaFisica = pessoaFisicaRepository.funcionarioDisponivelPorOS(id, usuario);
		
		//adiciona na lista de funcionario somente os disponiveis
		for (PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica : listPessoaJuridicaPessoaFisica) {
			listFuncionario.add(pessoaJuridicaPessoaFisica.getPessoaFisica());
		}
		
		AvaliacaoOrdemServico avaliacaoOrdemServico = new AvaliacaoOrdemServico();
		
		
		Condominio condominio = condominioRepository.findById(ordemServico.getCondominio().getId());
		
		mv.addObject("ordemServico", ordemServico); 
		mv.addObject("condominio", condominio); 
		mv.addObject("listFuncionario", listFuncionario);
		mv.addObject("listServicoIdentificado", TipoServicoIdentificado.values());
		mv.addObject("listPessoaJuridicaPessoaFisica", listPessoaJuridicaPessoaFisica);
		mv.addObject("avaliacaoOrdemServico", avaliacaoOrdemServico);
		mv.addObject("listPerguntaAvaliacao", perguntaAvaliacaoRepository.findByAtivo(1));
		mv.addObject("listOrdemServicoStatus", ordemServicoStatusRepository.findAll()); 
		
		return mv;
	}
	
	
	
	@GetMapping("/visitaexecutada/{id}")
	public ModelAndView realizandoVisita(@PathVariable int id,@AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/ficha");

		OrdemServico ordemServico = ordemServicoRepository.findOne(id);
		marcarComoVisitado(usuario, ordemServico);
		
		Condominio condominio = condominioRepository.findById(ordemServico.getCondominio().getId());
		
		List<PessoaFisica> listFuncionario = pessoaFisicaRepository.findByAtivoOrderByNomeAsc(1);
		
		mv.addObject("ordemServico", ordemServico); 
		mv.addObject("condominio", condominio); 
		mv.addObject("listFuncionario", listFuncionario); 
		mv.addObject("listServicoIdentificado", TipoServicoIdentificado.values());

		return mv;
	}
	
	
	private void marcarComoVisitado(UsuarioSistema usuario, OrdemServico ordemServico) {
		List<PessoaFisicaOrdemServico> funcionarios = ordemServico.getListPessoaFisicaOrdemServico();
		for (PessoaFisicaOrdemServico pessoaFisicaOrdemServico : funcionarios) {
			
			if(pessoaFisicaOrdemServico.getPessoaFisica().getId() == usuario.getPessoaFisica().getId()){
				pessoaFisicaOrdemServico.setVisita(true);
				pessoaFisicaOrdemServicoRepository.save(pessoaFisicaOrdemServico);
			}
			
		}
		
		//ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);
	}
	

	private void marcarComoCiente(UsuarioSistema usuario, OrdemServico ordemServico) {
		
		List<PessoaFisicaOrdemServico> funcionarios = ordemServico.getListPessoaFisicaOrdemServico();
		for (PessoaFisicaOrdemServico pessoaFisicaOrdemServico : funcionarios) {
			
			if(pessoaFisicaOrdemServico.getPessoaFisica().getId() == usuario.getPessoaFisica().getId()){
				pessoaFisicaOrdemServico.setCiente(true);
				pessoaFisicaOrdemServicoRepository.save(pessoaFisicaOrdemServico);
			}
			
		}
	}
	
	@GetMapping("/avaliacao/{id}")
	public ModelAndView avaliacao(@PathVariable int id) {
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/avaliacao");

		OrdemServico ordemServico = ordemServicoRepository.findOne(id);
		AvaliacaoOrdemServico avaliacaoOrdemServico = new AvaliacaoOrdemServico();
//		avaliacaoOrdemServico.get
		mv.addObject("ordemServico", ordemServico); 
		mv.addObject("avaliacaoOrdemServico", avaliacaoOrdemServico);
		mv.addObject("listPerguntaAvaliacao", perguntaAvaliacaoRepository.findByAtivo(1));

		return mv;
	}

	@GetMapping("/responsavel/{id}")
	public ModelAndView responsavel(@PathVariable int id, @AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/syndic/ordemservico/responsavel/detalheresponsavel");

		OrdemServico ordemServico = ordemServicoRepository.findOne(id);
		PessoaJuridica pj = pessoaJuridicaRepository.findById(usuario.getPessoaJuridica().getId());
		
		List<PessoaFisica> listFuncionario =  pj.getFuncionarios();
		
		mv.addObject("ordemServico", ordemServico); 
		mv.addObject("listFuncionario", listFuncionario);

		return mv;
	}
	
	@PostMapping(value = "/valores")
	public ModelAndView  inserirResponsavelOS( OrdemServico ordemServico)
	{
//		OrdemServico ordemServico = ordemServicoRepository.findById(Integer.valueOf(os));
		
		BigDecimal valor = ordemServico.getValorMaoDeObra();
		
		BigDecimal valor2 = ordemServico.getValorMaterial();
		
		
		ordemServico = ordemServicoRepository.findById(ordemServico.getId());
		
		ordemServico.setValorMaoDeObra(valor);
		ordemServico.setValorMaterial(valor2);
		
		ordemServicoRepository.save(ordemServico);

		ModelAndView mv = new ModelAndView();
		return new ModelAndView("redirect:/ordemservico/ficha/"+ordemServico.getId()); 
	}

	@RequestMapping(value = "/responsavel/inserir", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer inserirResponsavelOS(@RequestParam(name="os") int os, @RequestParam(name="responsavel") int responsavel, @RequestParam(name="observacao") String observacao,@AuthenticationPrincipal UsuarioSistema usuario)
	{
		PessoaFisicaOrdemServico pessoaFisicaOrdemServico = new PessoaFisicaOrdemServico();

		pessoaFisicaOrdemServico.setDataInclusao(new Timestamp(System.currentTimeMillis()));
		pessoaFisicaOrdemServico.setOrdemServico(ordemServicoRepository.getOne(os));
		pessoaFisicaOrdemServico.setPessoaFisica(pessoaFisicaRepository.getOne(responsavel));
		pessoaFisicaOrdemServico.setObservacao(observacao);
		pessoaFisicaOrdemServico.setCiente(false);
		pessoaFisicaOrdemServico.setVisita(false);

		pessoaFisicaOrdemServico = pessoaFisicaOrdemServicoRepository.saveAndFlush(pessoaFisicaOrdemServico);

		
		mensagemService.gerarMensagemParaResponsavel(pessoaFisicaOrdemServico,usuario);
		
		OrdemServico ordemServico = ordemServicoRepository.findById(os);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);
		
		return pessoaFisicaOrdemServico.getId();
	}

	@RequestMapping(value = "/responsavel/excluir", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer excluirResponsavelOS(@RequestParam(name="id") int id)
	{
		pessoaFisicaOrdemServicoRepository.delete(id);

		return id;
	}
	
	@RequestMapping(value = "/executar", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer executarOS(@RequestParam(name="idOS") int idOS, @RequestParam(name="obs") String obs, @AuthenticationPrincipal UsuarioSistema usuario)
	{
		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(6);

		salvarNovoStatusOS(idOS, obs, ordemServicoStatus, usuario);
		OrdemServico ordemServico = ordemServicoRepository.findById(idOS);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);
		return 1;
	}
	
	@RequestMapping(value = "/garantia", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer garantiaOS(@RequestParam(name="idOS") int idOS, @RequestParam(name="obs") String obs, @AuthenticationPrincipal UsuarioSistema usuario)
	{
		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(12);

		salvarNovoStatusOS(idOS, obs, ordemServicoStatus, usuario);
		
		OrdemServicoStatus ordemServicoStatus2 = ordemServicoStatusRepository.getOne(1);

		salvarNovoStatusOS(idOS, obs, ordemServicoStatus2, usuario);
		
		statusInicialResponsaveis(idOS);
		OrdemServico ordemServico = ordemServicoRepository.findById(idOS);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);

		return 1;
	}


	private void statusInicialResponsaveis(int idOS) {
		
		List<PessoaFisicaOrdemServico> listPessoaFisicaOrdemServico = pessoaFisicaOrdemServicoRepository.findByOrdemServicoId(idOS);
		
		for (PessoaFisicaOrdemServico pessoaFisicaOrdemServico : listPessoaFisicaOrdemServico) {
			pessoaFisicaOrdemServico.setCiente(false);
			pessoaFisicaOrdemServico.setVisita(false);
		}
		
		pessoaFisicaOrdemServicoRepository.save(listPessoaFisicaOrdemServico);
	}

	@RequestMapping(value = "/visitaexecutada", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer visitaExecutadaOS(@RequestParam(name="idOS") int idOS, @RequestParam(name="obs") String obs, @AuthenticationPrincipal UsuarioSistema usuario)
	{
		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(2);
		salvarNovoStatusOS(idOS, obs, ordemServicoStatus, usuario);
		OrdemServico ordemServico = ordemServicoRepository.ordemServicoCompletaPorId(idOS);
		
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);

		return 1;
	}

	@RequestMapping(value = "/cancelar", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer cancelarOS(@RequestParam(name="idOS") int idOS, @RequestParam(name="obs") String obs, @AuthenticationPrincipal UsuarioSistema usuario)
	{
		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(10);

		salvarNovoStatusOS(idOS, obs, ordemServicoStatus, usuario);
		OrdemServico ordemServico = ordemServicoRepository.findById(idOS);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);

		return 1;
	}

	@RequestMapping(value = "/executada", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer OSExecutada(@RequestParam(name="idOS") int idOS, @RequestParam(name="obs") String obs, @AuthenticationPrincipal UsuarioSistema usuario)
	{
		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(7);

		salvarNovoStatusOS(idOS, obs, ordemServicoStatus, usuario);
		
		OrdemServicoStatus ordemServicoStatus2 = new OrdemServicoStatus();
		
		//pesquisa de satisfação apenas se n tiver passado pela garantia	
		HistoricoOrdemServico historicoOrdemServico = historicoOrdemServicoRepository.findByOrdemServicoIdAndOrdemServicoStatusId(idOS,12);
		
		
		if(historicoOrdemServico == null){
			ordemServicoStatus2 = ordemServicoStatusRepository.getOne(9);
		}else{
			ordemServicoStatus2 = ordemServicoStatusRepository.getOne(11); 
		}

		salvarNovoStatusOS(idOS, obs, ordemServicoStatus2, usuario);
		OrdemServico ordemServico = ordemServicoRepository.findById(idOS);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);

		return 1;
	}
	
	@RequestMapping(value = "/cotacao", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer OScotacao(@RequestParam(name="idOS") int idOS, @RequestParam(name="obs") String obs, @AuthenticationPrincipal UsuarioSistema usuario)
	{
		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(3);

		salvarNovoStatusOS(idOS, obs, ordemServicoStatus, usuario);
		OrdemServico ordemServico = ordemServicoRepository.findById(idOS);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);

		return 1;
	}
	
	@RequestMapping(value = "/compra", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer OSCompra(@RequestParam(name="idOS") int idOS, @RequestParam(name="obs") String obs, @AuthenticationPrincipal UsuarioSistema usuario)
	{
		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(5);

		salvarNovoStatusOS(idOS, obs, ordemServicoStatus, usuario);
		OrdemServico ordemServico = ordemServicoRepository.findById(idOS);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);

		return 1;
	}

	@RequestMapping(value = "/naoexecutada", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer OSNaoExecutada(@RequestParam(name="idOS") int idOS, @RequestParam(name="obs") String obs, @AuthenticationPrincipal UsuarioSistema usuario)
	{
		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(5);

		salvarNovoStatusOS(idOS, obs, ordemServicoStatus, usuario);
		OrdemServico ordemServico = ordemServicoRepository.findById(idOS);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);

		return 1;
	}	
	
	@RequestMapping(value = "/novostatus", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer OSNovoStatus(@RequestParam(name="idOs") int idOs, @RequestParam(name="status") int status, @RequestParam(name="observacao") String observacao, @AuthenticationPrincipal UsuarioSistema usuario)
	{
		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(status);

		salvarNovoStatusOS(idOs, observacao, ordemServicoStatus, usuario);
		OrdemServico ordemServico = ordemServicoRepository.findById(idOs);
		ordemServicoService.enviarEmailAoSalvar(ordemServico,usuario);

		return 1;
	}	
	

	private void salvarNovoStatusOS(int idOS, String obs, OrdemServicoStatus ordemServicoStatus, @AuthenticationPrincipal UsuarioSistema usuario) {
		HistoricoOrdemServico historicoOrdemServico = new HistoricoOrdemServico();

		historicoOrdemServico.setOrdemServico(ordemServicoRepository.getOne(idOS));
		historicoOrdemServico.setOrdemServicoStatus(ordemServicoStatus);
		if(obs != null || obs != ""){
			historicoOrdemServico.setDescricao(obs);			
		}

		historicoOrdemServico = ordemServicoService.salvarNovoHistoricoOS(historicoOrdemServico);
		
		ordemServicoService.enviarMensagensAoSalvarParaSindico(historicoOrdemServico.getOrdemServico(), usuario);
	}

	@RequestMapping(value = "/tiposervico", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer OsTipoServico(@RequestParam(name="idOS") int idOS, @RequestParam(name="idTipoOS") int idTipoOS)
	{
		
		OrdemServico ordemServico = ordemServicoRepository.getOne(idOS);
		ordemServico.setServicoIdentificado(idTipoOS);
		ordemServico = ordemServicoRepository.saveAndFlush(ordemServico);

		//alterar o status da OS - status 2 visita executada
//		OrdemServicoStatus ordemServicoStatus = ordemServicoStatusRepository.getOne(2);
//		salvarNovoStatusOS(idOS, TipoServicoIdentificado.getServico(idTipoOS), ordemServicoStatus);

		return ordemServico.getTipoFluxo();
	}
	
	@RequestMapping(value = "/emergencia", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer EmergenciaOs(@RequestParam(name="idOS") int idOS, @RequestParam(name="emergencia") Boolean emergencia)
	{
		OrdemServico ordemServico = ordemServicoRepository.getOne(idOS);
		ordemServico.setEmergencia(emergencia);
		ordemServicoRepository.saveAndFlush(ordemServico);

		return 1;
	}
	
	@RequestMapping(value = "/restante", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody float quantidadeOSRestante(@RequestParam(name="condominio") int idCondominio)
	{
		float restante = 0;
		
		List<OrdemServico> listOrdemServico = ordemServicoRepository.osPorCondominioPorMes(idCondominio);
		Condominio condominio = condominioRepository.getOne(idCondominio);
		
		if(condominio.getCondominioPacoteServicoAtivo() != null){
			restante = condominio.getCondominioPacoteServicoAtivo().getPacoteServico().getQtdVisitas() - listOrdemServico.size();			
		}
		
		if(restante <= 0){
			restante = condominio.getCondominioPacoteServicoAtivo().getPacoteServico().getValorVisitaExcedente();
		}else{
			restante = 1;
		}
				
		return restante;
	}

	@RequestMapping(value = "/busca/nova", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Integer OsTipoServico(@RequestParam(name="id") int id)
	{
		int retorno = 0;

		List<OrdemServico> listOrdemServico = ordemServicoRepository.findByIdGreaterThan(id);

		if(listOrdemServico.size() > 0){
			retorno = 1;
		}

		return retorno;
	}


}