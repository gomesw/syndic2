package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.PacoteServico;
import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisica;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.PacoteServicoRepository;
import com.tushtech.syndic.repository.PessoaJuridicaPessoaFisicaRepository;
import com.tushtech.syndic.repository.PessoaJuridicaRepository;
import com.tushtech.syndic.service.PacoteServicosService;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;


@Controller
@RequestMapping("/pacoteservicos")
public class PacoteServicosController {
	
	@Autowired
	private PacoteServicoRepository pacoteServicoRepository; 
	
	@Autowired
	private PacoteServicosService pacoteServicosService;
	
	@Autowired
	private PessoaJuridicaRepository pessoaJuridicaRepository;

	@Autowired
	private PessoaJuridicaPessoaFisicaRepository pessoaJuridicaPessoaFisicaRepository;
	
	@RequestMapping
	public ModelAndView inicio(PacoteServico pacoteServico,  @AuthenticationPrincipal UsuarioSistema usuario) {
		
		return formulario(pacoteServico, usuario);
	}
	
	@RequestMapping("/{id}")
	public ModelAndView editar(PacoteServico pacoteServico, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario){
		
		pacoteServico = pacoteServicoRepository.findOne(pacoteServico.getId());
		
		if(pacoteServico != null && pacoteServico.getAtivo() != 1 && !pacoteServico.isNovo()){
			attributes.addFlashAttribute("proibido", " Pacote de Servico está desativado! <a href=\"/pacoteservicos/ativar/" + pacoteServico.getId() + "\">Deseja Ativá-la ?</a>");
			return new ModelAndView("redirect:/pacoteservicos/gerenciar");
		}
		
		if(pacoteServico==null || pacoteServico.isNovo()){
			pacoteServico = new PacoteServico();
		}else{			
			pacoteServico = pacoteServicoRepository.findOne(pacoteServico.getId());
		}
		ModelAndView mv = formulario(pacoteServico, usuario);
		return mv;
	}
	
	@PostMapping("/{id}")
	public ModelAndView salvar(@Valid PacoteServico pacoteServico, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {
		if (result.hasErrors()) {
			return formulario(pacoteServico, usuario);
		}	
		
		//seta a pessoa juridica a qual o funcionário logado está ligado
		PessoaJuridicaPessoaFisica pfpj = pessoaJuridicaPessoaFisicaRepository.findByPessoaFisicaId(usuario.getPessoaFisica().getId());
		pacoteServico.setPessoaJuridica(pfpj.getPessoaJuridica()); 		 	
		
		if(pacoteServico.getValorVisitaExcedente() == null){
			pacoteServico.setValorVisitaExcedente((float) 0);
		}
		if(pacoteServico.getQtdVisitas() == null){
			pacoteServico.setQtdVisitas(0);
		}
		
		try {
			pacoteServicosService.salvar(pacoteServico);
		} catch (NomeDuplicadoException e) { 
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return formulario(pacoteServico, usuario);
		}
		
		attributes.addFlashAttribute("mensagem", "Registro salvo com sucesso!");
		return new ModelAndView("redirect:/pacoteservicos");
	}
	
	
	@GetMapping("/ativar/{id}")
	public ModelAndView ativar(PacoteServico pacoteServico,RedirectAttributes attributes)
	{
		pacoteServico = pacoteServicosService.ativar(pacoteServico);
		attributes.addFlashAttribute("mensagem", "Registro ativado com sucesso!");
		return new ModelAndView("redirect:/pacoteservicos");
	}
	
	@DeleteMapping("/{id}")
	public @ResponseBody Integer desativar(PacoteServico pacoteServico)
	{
		pacoteServicosService.desativar(pacoteServico);
		return pacoteServico.getId();
	}	

	//FORMULARIO PARA E EDITAR E SALVAR
	private ModelAndView formulario(PacoteServico pacoteServico, @AuthenticationPrincipal UsuarioSistema usuario) { 
		
		ModelAndView mv = new ModelAndView("/syndic/sistema/pacoteservicos/formulario");
		
		List<PacoteServico> listPacoteServico =  pacoteServicoRepository.findByAtivo(1);
		mv.addObject("listPacoteServico", listPacoteServico);		
		mv.addObject("pacoteServico", pacoteServico);
//		mv.addObject("listPessoaJuridica", pessoaJuridicaRepository.findByAtivo(1));
		return mv;
	}
}