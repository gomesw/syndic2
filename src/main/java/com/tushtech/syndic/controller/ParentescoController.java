package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Parentesco;
import com.tushtech.syndic.repository.ParentescoRepository;
import com.tushtech.syndic.service.ParentescoService;

@Controller
@RequestMapping("/parentesco")
public class ParentescoController {
	
	@Autowired
	private ParentescoRepository parentescoRepository; 
	
	@Autowired
	private ParentescoService parentescoService;	
	

	@RequestMapping("/listar")
	public ModelAndView listar(Parentesco parentesco) {
		ModelAndView mv = new ModelAndView("/syndic/sistema/parentesco/listar");
		mv.addObject("parentesco", parentescoRepository.findAll());
		return mv;
	}	
	
	@RequestMapping("/incluir") 
	public ModelAndView novo(Parentesco parentesco) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/parentesco/incluir");
		mv.addObject("parentesco", parentesco);
		return mv;
	}
	
	
	@PostMapping("/incluir")
	public ModelAndView salvar(@Valid Parentesco parentesco, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novo(parentesco);
		}
		
		parentescoService.salvar(parentesco);
		
		attributes.addFlashAttribute("mensagem","Parentesco cadastrado com sucesso!");
		return new ModelAndView("redirect:/parentesco/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarCondominio(@PathVariable Integer id, Parentesco parentesco)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/parentesco/incluir");
		
		mv.addObject("parentesco", parentescoRepository.findOne(parentesco.getId()));
		
		return mv;
	}	 
//	
//	@RequestMapping("/excluir/{id}")
//	public @ResponseBody TipoAnimal excluir(TipoAnimal tipoAnimal)
//	{  
//		tipoAnimalService.excluirTipoAnimal(tipoAnimal);
//		return tipoAnimal;
//	}	
}