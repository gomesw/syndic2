package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.Perfil;
import com.tushtech.syndic.entity.PerfilCondominio;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.PerfilCondominioRepository;
import com.tushtech.syndic.repository.PerfilRepository;
import com.tushtech.syndic.repository.RecursoRepository;
import com.tushtech.syndic.service.PerfilService;

/**
 * @author Wallace
 *
 */
@Controller
@RequestMapping("/perfil")

public class PerfilController {


	@Autowired
	private PerfilRepository perfilRepository; 

	@Autowired
	private RecursoRepository recursoRepository; 

	@Autowired
	private PerfilService perfilService;	


	@Autowired
	PerfilCondominioRepository perfilCondominioRepository;

	
	@Autowired
	CondominioRepository condominioRepository;

	
	@RequestMapping("")
	public ModelAndView listaPerfil() {

		ModelAndView mv = new ModelAndView("layout/sistema/perfil/listarperfil");
		List<Perfil> listPerfis = perfilRepository.findByAtivo(1);	
		mv.addObject("perfis",listPerfis);

		return mv;
	}

	@RequestMapping("/incluir")
	public ModelAndView novoPerfil(Perfil perfil) {
		ModelAndView mv = new ModelAndView("layout/sistema/perfil/incluirperfil");
		mv.addObject("perfil",perfil);
		return mv;
	}
	
	

	@RequestMapping(value="/inserirperfilcondominio", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public final @ResponseBody PerfilCondominio inserirPerfilrecurso(@RequestBody PerfilCondominio perfilCondominio) {
		
		System.err.println("TESTE TESTE TESTE TESTE TESTE TESTE TESTE");
		Condominio com = condominioRepository.findOne(perfilCondominio.getCondominio().getId());
		Perfil per = perfilRepository.findById(perfilCondominio.getPerfil().getId());
		
		perfilCondominio.setPerfil(per);
		perfilCondominio.setCondominio(com);
		
		PerfilCondominio perfilCondo =  perfilCondominioRepository.saveAndFlush(perfilCondominio);
		
		
		return perfilCondo;
		
	}
	
	@RequestMapping(value="/excluirperfilcondominio", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public final ResponseEntity<?>  deletarPerfilrecurso(@RequestBody PerfilCondominio perfilCondominio) {
		
		perfilCondominioRepository.delete(perfilCondominio);
		 
		return ResponseEntity.ok("200");
		
	}
	

	@RequestMapping(value= {"/incluir","/incluir/{\\d+}"},method= RequestMethod.POST)
	public ModelAndView salvarPerfil(@Valid Perfil perfil,BindingResult result,RedirectAttributes attribute) {

		if(result.hasErrors()){
			return novoPerfil(perfil);
		}
		//verificar se o recurso ja existe no banco
		Perfil perfilBanco = (!perfil.isNovo()?null:perfilRepository.findByNomeAndAtivo(perfil.getNome(),1));

		if(perfilBanco == null || !perfil.isNovo()){
			perfilService.salvarPerfil(perfil);		
			attribute.addFlashAttribute("mensagem"," Realizado com sucesso!");
			return new ModelAndView("redirect:/perfil");
		}else{			
			attribute.addFlashAttribute("mensagem"," Já existe um perfil cadastrado com o mesmo nome!");
			return new ModelAndView("redirect:/perfil");
		}


	}

	@RequestMapping("/editar/{id}")
	public ModelAndView visualizarPerfil(@PathVariable Integer id,Perfil perfil) 
	{
		ModelAndView mv = new ModelAndView("layout/sistema/perfil/incluirperfil");
		if(id!=null){
			perfil = perfilRepository.findOne(id);
		}
		mv.addObject("recurso", recursoRepository.findByAtivo(1));
		mv.addObject("perfil",perfil);
		return mv;
	}



	@RequestMapping("/desativar/{id}")
	public ModelAndView desativarPerfil(@PathVariable Integer id,RedirectAttributes attribute) 
	{
		Perfil perfil = perfilRepository.findById(id);
		perfilService.desativarPerfil(perfil);
		attribute.addFlashAttribute("mensagem"," Perfil desativado com sucesso!");
		return new ModelAndView("redirect:/perfil");
	}



	@RequestMapping("/ativar/{id}")
	public ModelAndView ativarPerfil(@PathVariable Integer id,RedirectAttributes attribute) 
	{
		Perfil perfil = perfilRepository.findById(id);   
		perfilService.ativarPerfil(perfil);
		attribute.addFlashAttribute("mensagem"," Perfil ativado com sucesso!");
		return new ModelAndView("redirect:/perfil");
	}



	@RequestMapping("/excluir/{id}")
	public @ResponseBody Perfil excluirPerfil(Perfil perfil) 
	{
		perfil = perfilRepository.findById(perfil.getId());
		perfilRepository.delete(perfil);
		return perfil;
	}

	@RequestMapping("/listaperfilrecurso")
	@ResponseStatus(HttpStatus.OK)
	public void listaFuncaoRecurso(@RequestParam("nomes[]") String[] nomes, @RequestParam("id") Integer id) {
		System.err.println("TESTE TESTE TESTE TEST E");
		perfilService.atualizarPerfilRecurso(nomes, id);					
	}


}



