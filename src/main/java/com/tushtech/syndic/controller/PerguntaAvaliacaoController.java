package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.PerguntaAvaliacao;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.PerguntaAvaliacaoRepository;
import com.tushtech.syndic.service.PerguntaAvaliacaoService;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;


@Controller
@RequestMapping("/perguntaavaliacao")
public class PerguntaAvaliacaoController {
	
	@Autowired
	private PerguntaAvaliacaoRepository perguntaAvaliacaoRepository;
	
	@Autowired
	private PerguntaAvaliacaoService perguntaAvaliacaoService; 
	
	@RequestMapping
	public ModelAndView inicio(PerguntaAvaliacao perguntaAvaliacao) {
		
		return formulario(perguntaAvaliacao);
	}
	
	@RequestMapping("/{id}")
	public ModelAndView editar(PerguntaAvaliacao perguntaAvaliacao, RedirectAttributes attributes){
		
		perguntaAvaliacao = perguntaAvaliacaoRepository.findOne(perguntaAvaliacao.getId());
		
		if(perguntaAvaliacao != null && perguntaAvaliacao.getAtivo() != 1 && !perguntaAvaliacao.isNovo()){
			attributes.addFlashAttribute("proibido", " Essa pergunta está desativado! <a href=\"/perguntaavaliacao/ativar/" + perguntaAvaliacao.getId() + "\">Deseja Ativá-la ?</a>");
			return new ModelAndView("redirect:/perguntaavaliacao");
		}
		
		if(perguntaAvaliacao==null || perguntaAvaliacao.isNovo()){
			perguntaAvaliacao = new PerguntaAvaliacao();
		}else{			
			perguntaAvaliacao = perguntaAvaliacaoRepository.findOne(perguntaAvaliacao.getId());
		}
		ModelAndView mv = formulario(perguntaAvaliacao);
		return mv;
	}
	
	@PostMapping("/{id}")
	public ModelAndView salvar(@Valid PerguntaAvaliacao perguntaAvaliacao, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {
		if (result.hasErrors()) {
			return formulario(perguntaAvaliacao);
		}
		
		try {
			perguntaAvaliacaoService.salvar(perguntaAvaliacao);
		} catch (NomeDuplicadoException e) { 
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return formulario(perguntaAvaliacao);
		}
		
		attributes.addFlashAttribute("mensagem", "Registro salvo com sucesso!");
		return new ModelAndView("redirect:/perguntaavaliacao");
	}
	
	
	@GetMapping("/ativar/{id}")
	public ModelAndView ativar(PerguntaAvaliacao perguntaAvaliacao, RedirectAttributes attributes)
	{
		perguntaAvaliacao = perguntaAvaliacaoService.ativar(perguntaAvaliacao);
		attributes.addFlashAttribute("mensagem", "Registro ativado com sucesso!");
		return new ModelAndView("redirect:/perguntaavaliacao");
	}
	
	@DeleteMapping("/{id}")
	public @ResponseBody Integer desativar(PerguntaAvaliacao perguntaAvaliacao)
	{
		perguntaAvaliacaoService.desativar(perguntaAvaliacao);
		return perguntaAvaliacao.getId();
	}	

	//FORMULARIO PARA E EDITAR E SALVAR
	private ModelAndView formulario(PerguntaAvaliacao perguntaAvaliacao) { 
		
		ModelAndView mv = new ModelAndView("/syndic/sistema/perguntaavaliacao/formulario");
		
		List<PerguntaAvaliacao> listPerguntaAvaliacao =  perguntaAvaliacaoRepository.findByAtivo(1);
		mv.addObject("listPerguntaAvaliacao", listPerguntaAvaliacao);		
		mv.addObject("perguntaAvaliacao", perguntaAvaliacao);
		return mv;
	}
}