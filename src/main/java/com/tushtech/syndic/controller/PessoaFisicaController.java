package com.tushtech.syndic.controller;


import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.dto.VisitanteDTO;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.Cor;
import com.tushtech.syndic.entity.Email;
import com.tushtech.syndic.entity.Marca;
import com.tushtech.syndic.entity.Modelo;
import com.tushtech.syndic.entity.Parentesco;
import com.tushtech.syndic.entity.Perfil;
import com.tushtech.syndic.entity.PerfilCondominio;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.PessoaFisicaUnidadeRecurso;
import com.tushtech.syndic.entity.PessoaFisicaVeiculo;
import com.tushtech.syndic.entity.PessoaJuridica;
import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisica;
import com.tushtech.syndic.entity.Telefone;
import com.tushtech.syndic.entity.TipoPessoaFisicaUnidade;
import com.tushtech.syndic.entity.TipoVeiculo;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.Veiculo;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaUnidadeEnum;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.CorRepository;
import com.tushtech.syndic.repository.EmailRepository;
import com.tushtech.syndic.repository.FuncaoPessoaJuridicaRepository;
import com.tushtech.syndic.repository.GrauParentescoRepository;
import com.tushtech.syndic.repository.MarcaRepository;
import com.tushtech.syndic.repository.ModeloRepository;
import com.tushtech.syndic.repository.PerfilCondominioRepository;
import com.tushtech.syndic.repository.PerfilRepository;
import com.tushtech.syndic.repository.PessoaFisicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.repository.PessoaFisicaUnidadeRepository;
import com.tushtech.syndic.repository.PessoaFisicaVeiculoRepository;
import com.tushtech.syndic.repository.PessoaJuridicaPessoaFisicaRepository;
import com.tushtech.syndic.repository.PessoaJuridicaRepository;
import com.tushtech.syndic.repository.ProfissaoRepository;
import com.tushtech.syndic.repository.SexoRepository;
import com.tushtech.syndic.repository.TelefoneRepository;
import com.tushtech.syndic.repository.TipoFuncionarioRepository;
import com.tushtech.syndic.repository.TipoPessoaFisicaUnidadeRepository;
import com.tushtech.syndic.repository.TipoVeiculoRepository;
import com.tushtech.syndic.repository.UnidadeRepository;
import com.tushtech.syndic.service.CondominioService;
import com.tushtech.syndic.service.PessoaFisicaService;
import com.tushtech.syndic.service.PessoaFisicaUnidadeService;
import com.tushtech.syndic.service.PessoaFisicaVeiculoService;
import com.tushtech.syndic.service.UnidadeService;
import com.tushtech.syndic.service.VeiculoService;
import com.tushtech.syndic.service.exception.VeiculoJaCadastradoException;



@Controller
@RequestMapping()
public class PessoaFisicaController {
	
	
	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;	
	
	
	@Autowired
	private CondominioRepository condominioRepository;	

	@Autowired
	private PerfilRepository perfilRepository;
	
	@Autowired
	private CondominioService condominioService;
	
	@Autowired
	private TelefoneRepository telefoneRepository;

	@Autowired
	private UnidadeRepository unidadeRepository;

	@Autowired
	private MarcaRepository marcaRepository;	

	@Autowired
	private TipoPessoaFisicaUnidadeRepository tipoPessoaFisicaUnidadeRepository;	

	@Autowired
	private PessoaFisicaService pessoaFisicaService;

	@Autowired
	private SexoRepository sexoRepository;

	@Autowired
	private PessoaFisicaCondominioRepository pessoaFisicaCondominioRepository;

	@Autowired
	private PerfilCondominioRepository perfilCondominioRepository;

	@Autowired
	private EmailRepository emailRepository;
	
	
	@Autowired
	private PessoaJuridicaRepository pessoaJuridicaRepository;
	
	
	@Autowired
	private PessoaFisicaUnidadeRepository pessoaFisicaUnidadeRepository;
	
	@Autowired
	private CorRepository corRepository;  

	@Autowired
	private ModeloRepository modeloRepository; 	

	@Autowired
	private TipoVeiculoRepository tipoVeiculoRepository; 	

	@Autowired
	private GrauParentescoRepository grauParentescoRepository;
	
	@Autowired
	private UnidadeService unidadeService;
	
	@Autowired
	private PessoaFisicaUnidadeService pessoaFisicaUnidadeService;
	
	
	
	
	@Autowired
	private VeiculoService veiculoService;
	
	@Autowired
	private PessoaFisicaVeiculoService pessoaFisicaVeiculoService;
	
	@Autowired
	private TipoFuncionarioRepository tipoFuncionarioRepository;
	
	
	@Autowired
	private FuncaoPessoaJuridicaRepository funcaoPessoaJuridicaRepository;
	
	
	@Autowired
	private ProfissaoRepository profissaoRepository;

	@Autowired
	private PessoaJuridicaPessoaFisicaRepository pessoaJuridicaPessoaFisicaRepository;
	
	
	
	@Autowired
	private PessoaFisicaVeiculoRepository pessoaFisicaVeiculoRepository;
	 
	 	@RequestMapping(name="/pessoafisica", consumes = { MediaType.APPLICATION_JSON_VALUE })
		public  @ResponseBody List<VisitanteDTO>  listaVisitanteMoradoresPermitidos(String parametro) {
			List<VisitanteDTO> unidades  = pessoaFisicaRepository.porParametro(parametro);
			return unidades;
			
	}
	 	
	 	
	 	/**
	 	 * TELA DO ADMINISTRADOR DO CONDOMINIO PARA CADASTRAR UM SINDICO
	 	 * @param idcondominio
	 	 * @return
	 	 */
	 	@RequestMapping("/sindico/{id}") 
		public ModelAndView sindicoCondominio(@PathVariable("id") Integer idcondominio) {

			PessoaFisicaCondominio pessoaFisicaCondominio = new PessoaFisicaCondominio();
			
			
			Condominio condominio = condominioRepository.condominioComPessoaJuridica(idcondominio); 
			
			
			//Perfil perfil =  perfilRepository.findByNomeAndAtivo("Síndico", 1);//seta o perfil de sistema para sindico

			PessoaFisica pessoaFisica = new PessoaFisica();//instancia classe PessoaFisica

			

			//List<PessoaFisicaPerfilCondominio> listPessoaFisicaPerfilCondominio = new  ArrayList<>();//lista de perfis dentro do condominio

			
			//PessoaFisicaPerfilCondominio pessoaFisicaPerfilCondominio = new PessoaFisicaPerfilCondominio();//Perfil Especifico da Pessoa

			//PerfilCondominio perfilCondominio = perfilCondominioRepository.findByCondominioIdAndPerfilId(condominio.getId(),perfil.getId());//Perfil de Sindico do condominio Especifico

			//setar o perfil de sistema da pessoa cadastrada com sindico do condominio
			//pessoaFisicaPerfilCondominio.setPerfilCondominio(perfilCondominio);
			
			//listPessoaFisicaPerfilCondominio.add(pessoaFisicaPerfilCondominio);
			
			//pessoaFisicaPerfilCondominio.setPerfilCondominio(perfilCondominio);
			//pessoaFisicaPerfilCondominio.setPessoaFisica(pessoaFisica);
			
			//pessoaFisica.setPessoaFisicaPerfilCondominio(listPessoaFisicaPerfilCondominio);



			//setar a pessoa fisica criada como sindico dentro do condominio


			pessoaFisicaCondominio.setCondominio(condominio);
			pessoaFisicaCondominio.setPessoaFisica(pessoaFisica);


			ModelAndView mv = new ModelAndView("/syndic/condominios/pessoas/sindico/formulario");

			mv.addObject("pessoaFisicaCondominio", pessoaFisicaCondominio);
			mv.addObject("sexo", sexoRepository.findAll());
			//mv.addObject("perfilCondominio", perfilCondominio);

			return mv;
		}	
	 	
	 	
	 	

		/**
		 * TELA DO ADMINISTRADOR DO CONDOMINIO PARA ADCIONAR MORADORES A UMA UNIDADE
		 * @param id
		 * @return
		 */
		@RequestMapping("/adicionarmoradores/{id}")
		public ModelAndView formMoredoresCondominio(@PathVariable Integer id) { 

			ModelAndView mv = new ModelAndView("/syndic/condominios/pessoas/morador/formulario");
			Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);

			List<TipoPessoaFisicaUnidade> listTipoPessoaFisicaUnidade = tipoPessoaFisicaUnidadeRepository.findByAtivo(1);
			
			PessoaFisicaUnidade pessoaFisicaUnidade = new PessoaFisicaUnidade();
			List<Parentesco> listGrauParentesco = grauParentescoRepository.findAll();
			
			Integer recursos[] = new Integer[0];  
			
			mv.addObject("condominio", condominio); 
			mv.addObject("unidades", condominio.getUnidades()); 
			mv.addObject("listTipoPessoaFisicaUnidade", listTipoPessoaFisicaUnidade); 
			mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade); 
			mv.addObject("listGrauParentesco", listGrauParentesco); 
			mv.addObject("sexo", sexoRepository.findAll()); 
			mv.addObject("recursos", recursos);
			
			return mv;

		}
		
		
		
		
		
		@RequestMapping("/condominio/editarmoradores/{id}")
		public ModelAndView formEditarMoredoresCondominio(@PathVariable Integer id) { 

			ModelAndView mv = new ModelAndView("/syndic/condominios/morador/formincluir");
			
			
			PessoaFisicaUnidade pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.findById(id);
			
			Condominio condominio = condominioRepository.condominioComPessoaJuridica(pessoaFisicaUnidade.getUnidade().getCondominio().getId());

			List<TipoPessoaFisicaUnidade> listTipoPessoaFisicaUnidade = tipoPessoaFisicaUnidadeRepository.findByAtivo(1);
			
			List<Parentesco> listGrauParentesco = grauParentescoRepository.findAll();
			

			Integer recursos[] = new Integer[pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso().size()];  
			
			
			String recursosids = "";
			
			int index =0; 
			for (PessoaFisicaUnidadeRecurso pessoaFisicaUnidadeRecurso : pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso()) {
				recursos[index]=pessoaFisicaUnidadeRecurso.getRecurso().getId();
				index++;
				recursosids+=pessoaFisicaUnidadeRecurso.getRecurso().getId()+",";
			}
			
			mv.addObject("condominio", condominio); 
			mv.addObject("unidades", condominio.getUnidades()); 
			mv.addObject("listTipoPessoaFisicaUnidade", listTipoPessoaFisicaUnidade); 
			mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade); 
			mv.addObject("listGrauParentesco", listGrauParentesco); 
			mv.addObject("sexo", sexoRepository.findAll()); 
			mv.addObject("recursos", recursos);
			mv.addObject("recursosids", recursosids+"0");
			
			return mv;

		}
		
		
		
		/**
		 * SALVAR UM MORADOR/PROPRIETÁRIO/INQUILINO/ETC PARA UNIDADE NO BANCO DE DADOS
		 * @param pessoaFisicaUnidade
		 * @param result
		 * @param attributes
		 * @return
		 */
		@PostMapping(value = {"/condominio/morador/novo", "/condominio/morador/novo/{\\d+}"})
		public ModelAndView salvarProprietarioUnidade(@Valid PessoaFisicaUnidade pessoaFisicaUnidade,
				BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuarioSistema,
				@RequestParam(value  = "recursos", required = false) String recursos,
				@RequestParam(value  = "snveiculo", required = false) Integer snveiculo) {

			
			pessoaFisicaUnidade.setPessoaFisicaCadastrante(usuarioSistema.getPessoaFisica());
			
			
			//ATRIBUIR ERROS NO FORMULÁRIO POR TIPO
			pessoaFisicaUnidadeService.atribuirErrosPorTipo(pessoaFisicaUnidade, result, recursos, snveiculo);
			

			boolean isnovo = pessoaFisicaUnidade.isNovo();
			
			
			//SE TIVER ERRO REDIRECIONA PARA TELA DE ERROS
			if(result.hasErrors()){ 
				return formMoradoresCondominioErro(pessoaFisicaUnidade,recursos);
			}		

			Email email =  pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0);
			List<Telefone> telefones =  pessoaFisicaUnidade.getPessoaFisica().getTelefones();

			pessoaFisicaUnidade.getPessoaFisica().setAtivo(1); 

			pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());;



			PessoaFisica pf = pessoaFisicaService.salvarcomretorno(pessoaFisicaUnidade.getPessoaFisica());

			pf.setTelefones(telefones);
			unidadeService.inserirTelefonePessoaFisicaUnidade(pf);
			unidadeService.inserirEmailPessoaFisicaUnidade(email);
			pessoaFisicaUnidade.setPessoaFisica(pf);
			
			
			
			if(pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo()!=null && !pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().isEmpty()){
				PessoaFisicaVeiculo pessoaFisicaVeiculo = new PessoaFisicaVeiculo();
				pessoaFisicaVeiculo = pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0);
			}
			
			pessoaFisicaUnidade = pessoaFisicaUnidadeService.salvarPessoaFisicaUnidade(pessoaFisicaUnidade); 
			
			//PessoaFisicaCondominio pessoaFisicaCondominio = pessoaFisicaService.setarPessoaFisicaCondominio(pessoaFisicaUnidade);
			
			//INCLUIR NO BANCO DE DADOS DE ACORDO COM O TIPO
			//pessoaFisicaUnidade = inclusaoPorTipo(pessoaFisicaUnidade, usuarioSistema, recursos, snveiculo, isnovo, pf,
					//pessoaFisicaVeiculo);
			
			
			
			//mudar a mensagem de retorno de acordo com o tipo
			//mensagemRetornoPorTipo(pessoaFisicaUnidade, attributes, isnovo);
			
			return new ModelAndView("redirect:/sindico/condominio/editar/" + pessoaFisicaUnidade.getUnidade().getCondominio().getId());
		}
		
		
		
		
		
		
		
		

		
		
		/**
		 * RESETA E ENVIA UMA NOVA SENHA PARA O PROPRIETÁRIO
		 * @param pessoaFisicaUnidade
		 * @param result
		 * @param attributes
		 * @param usuario
		 * @return
		 */
		@GetMapping("/pessoa/enviarsenha/{id}")
		public ModelAndView enviarSenhaSindico(@PathVariable Integer id,RedirectAttributes attributes) {

			
			PessoaFisicaUnidade pfu = pessoaFisicaUnidadeRepository.findById(id);
			
			pessoaFisicaService.alterarSenhasEnviarEmail(pfu.getPessoaFisica());
			
			attributes.addFlashAttribute("mensagem"," Senha enviada com sucesso!");		
			return new ModelAndView("redirect:/sindico/condominio/editar/" + pfu.getUnidade().getCondominio().getId());
		}
		
		
		
		private ModelAndView formMoradoresCondominioErro(PessoaFisicaUnidade pessoaFisicaUnidade,String recursosids) {

			ModelAndView mv = new ModelAndView("/syndic/condominios/morador/formincluir");

			
			List<TipoPessoaFisicaUnidade> listTipoPessoaFisicaUnidade = tipoPessoaFisicaUnidadeRepository.findByAtivo(1);
			
			List<Parentesco> listGrauParentesco = grauParentescoRepository.findAll();
			
			
			String recursosstring[] = recursosids.split(",");
			Integer recursos[] = new Integer[recursosstring.length];//= new Integer[pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso() == null ||pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso().isEmpty()?0:pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso().size()];  
			
			int cont = 0;
			if(recursosstring.length>0) {
				for (String i : recursosstring) {
					if(!i.isEmpty()){
						recursos[cont]= Integer.valueOf(i);
					}
					cont++;
				}
			}
			
			mv.addObject("unidades", pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getUnidades()); 
			mv.addObject("listTipoPessoaFisicaUnidade", listTipoPessoaFisicaUnidade); 
			mv.addObject("listGrauParentesco", listGrauParentesco); 
			mv.addObject("recursos", recursos);
			mv.addObject("recursosids", recursosids);
			mv.addObject("sexo", sexoRepository.findAll()); 
			mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
			mv.addObject("condominio", pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio());


			return mv;
		}
		
		
		
		@PostMapping("/sindico/novo")
		public ModelAndView incluirSindico(@Valid PessoaFisicaCondominio pessoafisicaCondominio, BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

			pessoaFisicaService.atribuirErrosSindico(pessoafisicaCondominio, result); 

			
			boolean isnovo = pessoafisicaCondominio.isNovo();
			if(result.hasErrors()){ 
				return sindicoCondominiocomerro(pessoafisicaCondominio);
			}		

			
			pessoafisicaCondominio.getPessoaFisica().getEmails().get(0).setPessoaFisica(pessoafisicaCondominio.getPessoaFisica());;

			List<Telefone> telefones = pessoafisicaCondominio.getPessoaFisica().getTelefones();
			
			PessoaFisicaCondominio pfc = pessoaFisicaService.incluirSindico(pessoafisicaCondominio,usuario,isnovo);//pessoaFisicaService.salvarcomretorno(pessoafisicaCondominio.getPessoaFisica());
			
			
			//salvar telefones
			//atribui a pessoa salva aos telefones
			for (Telefone telefone : telefones) {
				telefone.setPessoaFisica(pfc.getPessoaFisica());
			}
			
			//salva os telefones
			telefoneRepository.save(telefones);
			

			attributes.addFlashAttribute("mensagem"," Síndico cadastrado com sucesso!");		
			return new ModelAndView("redirect:/condominio/editar/"+pessoafisicaCondominio.getCondominio().getId());
		}
		
		
		
		
		
		@PostMapping("/sindico/enviarsenha")
		public ModelAndView enviarSenhaSindico(@Valid PessoaFisicaCondominio pessoafisicaCondominio, BindingResult result, 
				RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

			List<Telefone> telefones = pessoafisicaCondominio.getPessoaFisica().getTelefones();
			for (Telefone telefone : telefones) {
				telefone.setPessoaFisica(pessoafisicaCondominio.getPessoaFisica());
			}
			
			pessoaFisicaService.atribuirErrosSindico(pessoafisicaCondominio, result);

			if(result.hasErrors()){ 
				return sindicoCondominiocomerro(pessoafisicaCondominio); 
			}		


			//pessoafisicaCondominio.getPessoaFisica().setCpf(pessoafisicaCondominio.getPessoaFisica().getCpfSemMascara());

			pessoafisicaCondominio.getPessoaFisica().getEmails().get(0).setPessoaFisica(pessoafisicaCondominio.getPessoaFisica());;

			

			//salva os telefones
			telefoneRepository.save(pessoafisicaCondominio.getPessoaFisica().getTelefones());
			
			
			
			List<Email> emails = pessoafisicaCondominio.getPessoaFisica().getEmails();
			for (Email email : emails) {
				email.setAtivo(1);
			}
			
			
			//salva os emails
			emailRepository.save(emails);
			
			
			pessoaFisicaService.salvar(pessoafisicaCondominio.getPessoaFisica());
			
			
			condominioService.incluirSindicoEdicaoEnviandoSenha(pessoafisicaCondominio,usuario);

			attributes.addFlashAttribute("mensagem"," Senha Enviada com sucesso!");		
			return new ModelAndView("redirect:/condominio/editar/"+pessoafisicaCondominio.getCondominio().getId());
		}
		
		
		
		
		
		
		/**
		 * Editando o sindico de um condomínio
		 * @param pessoafisicaCondominio
		 * @param result
		 * @param attributes
		 * @param usuario
		 * @return
		 */
		@PostMapping("/sindico/editar/{id}")  
		public ModelAndView editarSindico(@Valid PessoaFisicaCondominio pessoafisicaCondominio, BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

			
			List<Telefone> telefones = pessoafisicaCondominio.getPessoaFisica().getTelefones();
			
			for (Telefone telefone : telefones) {
				telefone.setPessoaFisica(pessoafisicaCondominio.getPessoaFisica());
			}
			
			pessoaFisicaService.atribuirErrosSindico(pessoafisicaCondominio, result);

			if(result.hasErrors()){ 
				return sindicoCondominiocomerro(pessoafisicaCondominio); 
			}		


			pessoafisicaCondominio.getPessoaFisica().setCpf(pessoafisicaCondominio.getPessoaFisica().getCpfSemMascara());

			pessoafisicaCondominio.getPessoaFisica().getEmails().get(0).setPessoaFisica(pessoafisicaCondominio.getPessoaFisica());;

			

			//salva os telefones
			telefoneRepository.save(pessoafisicaCondominio.getPessoaFisica().getTelefones());
			
			
			
			List<Email> emails = pessoafisicaCondominio.getPessoaFisica().getEmails();
			for (Email email : emails) {
				email.setAtivo(1);
			}
			
			
			//salva os emails
			emailRepository.save(emails);
			
			
			pessoaFisicaService.salvar(pessoafisicaCondominio.getPessoaFisica());
			
			
			condominioService.salvarSindicoEdicao(pessoafisicaCondominio,usuario);

			attributes.addFlashAttribute("mensagem"," Síndico editado com sucesso!");		
			return new ModelAndView("redirect:/condominio/editar/"+pessoafisicaCondominio.getCondominio().getId());
		}
		
		
		
		
		
		
		
		
		
		

		/**
		 * Monta o formulario caso tenha erro
		 * @param pessoaFisicaCondominio
		 * @return
		 */
		public ModelAndView sindicoCondominiocomerro(PessoaFisicaCondominio pessoaFisicaCondominio) {

			Perfil perfil =  perfilRepository.findByNomeAndAtivo("Síndico", 1);//seta o perfil de sistema para sindico
			PerfilCondominio perfilCondominio = perfilCondominioRepository.findByCondominioIdAndPerfilId(pessoaFisicaCondominio.getCondominio().getId(),perfil.getId());//Perfil de Sindico do condominio Especifico

			ModelAndView mv = new ModelAndView("/syndic/condominios/pessoas/sindico/formulario");

			mv.addObject("condominio", pessoaFisicaCondominio.getCondominio());
			mv.addObject("pessoaFisicaCondominio", pessoaFisicaCondominio);
			mv.addObject("sexo", sexoRepository.findAll());
			mv.addObject("perfilCondominio", perfilCondominio);

			return mv;
		}	

		
		
		//Inclusao de funcionarios para o condominio
		@RequestMapping("administrador/condominio/funcionario/{id}") 
		public ModelAndView incluirFuncionario(@PathVariable("id") Integer idcondominio) {

			ModelAndView mv = new ModelAndView("/syndic/condominios/funcionario/formincluir"); 

			Condominio condominio = condominioRepository.findById(idcondominio); //unidadeRepository.buscarUnidadeCompleta(id);
			PessoaFisicaCondominio pessoaFisicaCondominio = new PessoaFisicaCondominio();
			pessoaFisicaCondominio.setCondominio(condominio);
			
			
//				PessoaFisicaVeiculo pessoaFisicaVeiculo = new PessoaFisicaVeiculo(); 
//				PessoaFisica pf= new PessoaFisica();
			
			
			List<Modelo> modeloVeiculo = new ArrayList<>();
			
			List<Marca> marcaVeiculo = marcaRepository.findByTipoVeiculoIdOrderByNomeAsc(0);
			List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
			
			List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
			
			mv.addObject("modeloVeiculo", modeloVeiculo); 
			mv.addObject("marcaVeiculo", marcaVeiculo); 
			//mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
			mv.addObject("tipoVeiculo", tipoVeiculo); 
			mv.addObject("cores", cores); 
			mv.addObject("sexo", sexoRepository.findAll()); 
			mv.addObject("pessoaFisicaCondominio", pessoaFisicaCondominio);
			mv.addObject("listaTipoFuncionario", tipoFuncionarioRepository.findByAtivoOrderByNomeAsc(1));
			mv.addObject("condominio", condominio);
			

			return mv;
		}
		
		
		/**
		 * Monta o formulario caso tenha erro
		 * @param pessoaFisicaCondominio
		 * @return
		 */
		public ModelAndView funcionarioCondominiocomerro(PessoaFisicaCondominio pessoaFisicaCondominio, int snveiculo) {

			ModelAndView mv = new ModelAndView("/syndic/condominios/funcionario/formincluir");
			//pessoafisicaCondominio.getPessoaFisica().getPessoaFisicaVeiculo().get(0)
			
			
			List<Modelo> modeloVeiculo = new ArrayList<>();
			
			List<Marca> marcaVeiculo = marcaRepository.findByIdGreaterThanOrderByNomeAsc(0);
			List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
			
			List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
			
			if(snveiculo==1) {
				
				PessoaFisicaVeiculo pessoaFisicaVeiculo = pessoaFisicaCondominio.getPessoaFisica().getPessoaFisicaVeiculo().get(0);
				
				
				modeloVeiculo = modeloRepository.findByMarcaIdOrderByNomeAsc(snveiculo==1 && pessoaFisicaVeiculo.getVeiculo().getModelo()!=null && pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca()
						!=null?pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getId():0);
				marcaVeiculo = marcaRepository.findByIdGreaterThanOrderByNomeAsc(0);
				tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
				
				cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
			}
			
			
			mv.addObject("modeloVeiculo", modeloVeiculo); 
			mv.addObject("marcaVeiculo", marcaVeiculo); 
			//mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
			mv.addObject("tipoVeiculo", tipoVeiculo); 
			mv.addObject("cores", cores); 
			mv.addObject("sexo", sexoRepository.findAll()); 
			mv.addObject("pessoaFisicaCondominio", pessoaFisicaCondominio);
			mv.addObject("listaTipoFuncionario", tipoFuncionarioRepository.findByAtivoOrderByNomeAsc(1));
			mv.addObject("condominio", pessoaFisicaCondominio.getCondominio());
			mv.addObject("snveiculo", snveiculo);
			
			
			
			
//				Perfil perfil =  perfilRepository.findByNomeAndAtivo("Funcionario", 1);//seta o perfil de sistema para funcionario
//				PerfilCondominio perfilCondominio = perfilCondominioRepository.findByCondominioIdAndPerfilId(pessoaFisicaCondominio.getCondominio().getId(),perfil.getId());//Perfil de funcionario do condominio Especifico
	//
//				ModelAndView mv = new ModelAndView("/syndic/condominios/incluirfuncionario");
	//
//				mv.addObject("condominio", pessoaFisicaCondominio.getCondominio());
//				mv.addObject("pessoaFisicaCondominio", pessoaFisicaCondominio);
//				mv.addObject("sexo", sexoRepository.findAll());
//				mv.addObject("perfilCondominio", perfilCondominio);
			
			
			return mv;
		}
		
		
		
		@PostMapping("administrador/condominio/funcionario/novo")
		public ModelAndView incluirFuncionario(@Valid PessoaFisicaCondominio pessoafisicaCondominio, @RequestParam(name="snveiculo",defaultValue="0") Integer snveiculo, 
				BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {

//			System.err.println(pessoafisicaCondominio.getTipoFuncionario().getId());
			pessoaFisicaService.atribuirErrosFuncionarioCondominio(pessoafisicaCondominio, result,snveiculo);
			
			
			boolean isnovo = pessoafisicaCondominio.isNovo();
			
			if(result.hasErrors()){ 
				return funcionarioCondominiocomerro(pessoafisicaCondominio, snveiculo);
			}		

			//TipoPessoa tipoPessoa = new TipoPessoa(); 
			//tipoPessoa.setId(4);//Tipo de Pessoa Funcionario
			

			Email email =  pessoafisicaCondominio.getPessoaFisica().getEmails().get(0);
			List<Telefone> telefones =  pessoafisicaCondominio.getPessoaFisica().getTelefones();
			
			
			PessoaFisicaVeiculo pessoaFisicaVeiculo = pessoafisicaCondominio.getPessoaFisica().getPessoaFisicaVeiculo().get(0);
			

			pessoafisicaCondominio.getPessoaFisica().getEmails().get(0).setPessoaFisica(pessoafisicaCondominio.getPessoaFisica());


			pessoafisicaCondominio.getPessoaFisica().setCpf(pessoafisicaCondominio.getPessoaFisica().getCpfSemMascara());
			//pessoafisicaCondominio.getPessoaFisica().setTipoPessoa(tipoPessoa);
			pessoafisicaCondominio.getPessoaFisica().setAtivo(1); 
			PessoaFisica pf = pessoaFisicaService.salvarcomretorno(pessoafisicaCondominio.getPessoaFisica());

			
			pf.setTelefones(telefones);
			
			
			unidadeService.inserirTelefonePessoaFisicaUnidade(pf);
			unidadeService.inserirEmailPessoaFisicaUnidade(email);
			
			if(snveiculo==1){
				
				if(pessoaFisicaVeiculo.isNovo()){
					pessoaFisicaVeiculo.getVeiculo().setCadastrante(usuario.getPessoaFisica());
					pessoaFisicaVeiculo.setPessoaFisica(pf);
					//pessoaFisicaVeiculo.setDataInicio(Date.valueOf(LocalDate.now()));
					pessoaFisicaVeiculo.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidadeRepository.findById(TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()));
				}
				
				
				Veiculo veiculo = veiculoService.salvarVeiculo(pessoaFisicaVeiculo.getVeiculo());

				pessoaFisicaVeiculo.setVeiculo(veiculo);
				
				pessoaFisicaVeiculoService.salvar(pessoaFisicaVeiculo);
				
//				unidadeService.inserirVeiculo(pessoaFisicaVeiculo);
			}
			else if(snveiculo==0 && !pessoaFisicaVeiculo.isNovo()){
				
				pessoaFisicaVeiculoService.excluir(pessoaFisicaVeiculo);
				veiculoService.excluirVeiculo(pessoaFisicaVeiculo.getVeiculo());
				
			}
			
			pessoafisicaCondominio.setPessoaFisica(pf);
			pessoaFisicaCondominioRepository.save(pessoafisicaCondominio);

//				TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade = new TipoPessoaFisicaUnidade(); 
//				tipoPessoaFisicaUnidade.setId(TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId());//Tipo de Pessoa FUNCIONARIO
//				pessoafisicaCondominio.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidade);

			
		//	unidadeService.incluirFuncionario(pessoaFisicaUnidade); 

			if(isnovo) {
				attributes.addFlashAttribute("mensagem","Funcionário cadastrado com sucesso!");
			}
			else {
				attributes.addFlashAttribute("mensagem","Funcionário editado com sucesso!");
			}
			
			return new ModelAndView("redirect:/administrador/condominio/funcionario/listar/" + pessoafisicaCondominio.getCondominio().getId());
		}
		
		
		@RequestMapping("/administrador/condominio/funcionario/listar/{id}")
		public ModelAndView listarFuncionarios(@PathVariable Integer id) { 

			ModelAndView mv = new ModelAndView("/syndic/condominios/funcionario/listarfuncionarios");
			Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);

			mv.addObject("condominio", condominio); 
			return mv;

		}


		
		
		 @RequestMapping(value = "/condominio/unidademoradores",consumes = MediaType.APPLICATION_JSON_VALUE)
		 public @ResponseBody List<VisitanteDTO> pesquisarMoradoreDeUnidade(@RequestParam(name="id",defaultValue="-1") Integer id) {

			 	
			 	Unidade unidade = unidadeRepository.findById(id);
			 
			 	List<VisitanteDTO> listVisitanteDTO = new ArrayList<VisitanteDTO>();
			 	
			 	List<PessoaFisicaUnidade> listPessoaFisicaUnidade = unidade.getListaPessoasAtivasUnidade();
			    
			 	for (PessoaFisicaUnidade pessoaFisicaUnidade : listPessoaFisicaUnidade) {
			 		VisitanteDTO visitanteDTO = new VisitanteDTO();
			 		visitanteDTO.setId(pessoaFisicaUnidade.getId());
			 		visitanteDTO.setNome(pessoaFisicaUnidade.getPessoaFisica().getNome());
			 		listVisitanteDTO.add(visitanteDTO);
				}
			 	
			    return listVisitanteDTO; 
			 	
		 }
		 
		 
		 @RequestMapping("/sindico/editar/{id}") 
			public ModelAndView editarSindicoCondominio(@PathVariable Integer id) 
			{
				
				PessoaFisicaCondominio pessoaFisicaCondominio = pessoaFisicaCondominioRepository.findOne(id);
				

				ModelAndView mv = new ModelAndView("/syndic/condominios/pessoas/sindico/formulario");

				mv.addObject("condominio", pessoaFisicaCondominio.getCondominio());
				mv.addObject("pessoaFisicaCondominio", pessoaFisicaCondominio);
				mv.addObject("sexo", sexoRepository.findAll());
				//mv.addObject("perfilCondominio", perfilCondominio);

				return mv;
			}
		 
		 
		 
		 	@RequestMapping("/sindico/listar/{id}")
			public ModelAndView getSidicosCondominio(@PathVariable Integer id) { 

				ModelAndView mv = new ModelAndView("/syndic/condominios/pessoas/sindico/listar");
				Condominio condominio = condominioRepository.condominioComPessoaJuridica(id); 

				mv.addObject("condominio", condominio); 
				return mv;

			}
		 
		 
			@RequestMapping("/condominio/listarmoradores/{id}")
			public ModelAndView getMoredoresCondominio(@PathVariable Integer id) { 

				ModelAndView mv = new ModelAndView("/syndic/condominios/morador/listar");
				Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);

				mv.addObject("condominio", condominio); 
				return mv;

			}
			


			



			
			
			@RequestMapping("/unidade/morador/listar/{id}")
			public ModelAndView getMoredoresUnidades(@PathVariable Integer id) {

				ModelAndView mv = new ModelAndView("/syndic/unidades/morador/listar");

				Unidade unidade = unidadeRepository.findById(id);

				List<PessoaFisicaUnidade> listPessoaFisicaUnidade =  pessoaFisicaUnidadeRepository.moradorPorUnidadePorCondominio(id, unidade.getCondominio().getId());


				mv.addObject("listPessoaFisicaUnidade",listPessoaFisicaUnidade);

				mv.addObject("unidade",unidade);
				mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId())); 
				return mv;

			}
			
			
			@RequestMapping("/unidade/inquilino/listar/{id}")
			public ModelAndView getInquilinoUnidades(@PathVariable Integer id) {

				ModelAndView mv = new ModelAndView("/syndic/unidades/inquilino/listar");

				Unidade unidade = unidadeRepository.findById(id);

				List<PessoaFisicaUnidade> listPessoaFisicaUnidade =  pessoaFisicaUnidadeRepository.inquilinoPorUnidadePorCondominio(id, unidade.getCondominio().getId());


				mv.addObject("listPessoaFisicaUnidade",listPessoaFisicaUnidade);

				mv.addObject("unidade",unidade);
				mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId())); 
				return mv;

			}
			
			


			


			/**
			 * TELA PARA ADICIONAR UM MORADOR A UMA UNIDADE
			 * @param id DA UNIDADE
			 * @return
			 */
			@RequestMapping("/unidade/morador/adicionar/{id}")
			public ModelAndView novoMoradorUnidade(@PathVariable Integer id) {

				ModelAndView mv = new ModelAndView("/syndic/unidades/morador/formincluir"); 

				Unidade unidade = unidadeRepository.findById(id); //unidadeRepository.buscarUnidadeCompleta(id);
				PessoaFisicaUnidade pessoaFisicaUnidade = new PessoaFisicaUnidade(); 
				pessoaFisicaUnidade.setUnidade(unidade);

				
				TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade = new TipoPessoaFisicaUnidade();
				tipoPessoaFisicaUnidade.setId(TipoPessoaFisicaUnidadeEnum.MORADOR.getId());
				pessoaFisicaUnidade.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidade);
				
				List<Parentesco> listGrauParentesco = grauParentescoRepository.findAll();
				
				Integer recursos[] = new Integer[0];  
				String recursosids = "";
				
				
				mv.addObject("sexo", sexoRepository.findAll()); 
				
				mv.addObject("listGrauParentesco", listGrauParentesco); 
				mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
				mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());
				mv.addObject("recursosids", recursosids);
				mv.addObject("recursos", recursos);
				
				
				
				return mv;
			}
			
			
			
			
			
			
			/**
			 * TELA PARA ADICIONAR UM INQUILINO A UMA UNIDADE
			 * @param id DA UNIDADE
			 * @return
			 */
			@RequestMapping("/unidade/inquilino/adicionar/{id}")
			public ModelAndView novoInquilinoUnidade(@PathVariable Integer id,@AuthenticationPrincipal UsuarioSistema usuario) {

				ModelAndView mv = new ModelAndView("/syndic/unidades/inquilino/formincluir"); 

				Unidade unidade = unidadeRepository.buscarUnidadeCompleta(id);
				PessoaFisicaUnidade pessoaFisicaUnidade = new PessoaFisicaUnidade(); 
				pessoaFisicaUnidade.setUnidade(unidade);

				
				
				
				int[] tipos = {
						TipoPessoaFisicaUnidadeEnum.COMODATO.getId(),
						TipoPessoaFisicaUnidadeEnum.INQUILINO.getId()};

				List<TipoPessoaFisicaUnidade> listPessoaFisicaUnidade = tipoPessoaFisicaUnidadeRepository.findByIdIn(tipos);
				
				
				List<Parentesco> listGrauParentesco = grauParentescoRepository.findAll();
				
				Integer recursos[] = new Integer[0];  
				String recursosids = "";
				
				mv.addObject("sexo", sexoRepository.findAll()); 
				mv.addObject("listGrauParentesco", listGrauParentesco); 
				mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
				mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());
				mv.addObject("recursosids", recursosids);
				mv.addObject("listTipoPessoaFisicaUnidade", listPessoaFisicaUnidade); 
				mv.addObject("recursos", recursos);
				mv.addObject("usuario", usuario.getPessoaFisica());
				
				
				
				return mv;
			}
			
			
			/**
			 * TELA PARA EDIÇÃO DE MORADOR DE UNIDADE
			 * @param id PESSOAFISICAUNIDADE QUE REPRESENTA O MORADOR
			 * @param pessoaFisicaUnidade
			 * @return
			 */
			@RequestMapping("/unidade/morador/editar/{id}")
			public ModelAndView editarMoradorUnidade(@PathVariable Integer id,PessoaFisicaUnidade pessoaFisicaUnidade) {
				ModelAndView mv ;


				
				pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.findById(id);
				
				if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId() 
						|| pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId()) {
					mv = new  ModelAndView("/syndic/unidades/proprietarios/formincluir");
				}
				else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.INQUILINO.getId()) {
					mv = new  ModelAndView("/syndic/unidades/inquilino/formincluir"); 
				}
				else {
					mv = new  ModelAndView("/syndic/unidades/morador/formincluir"); 
				}
				
				Unidade unidade = unidadeRepository.buscarUnidadeCompleta(pessoaFisicaUnidade.getUnidade().getId());
				
				int[] tipos = {
						TipoPessoaFisicaUnidadeEnum.MORADOR.getId(),
						TipoPessoaFisicaUnidadeEnum.COMODATO.getId(),
						TipoPessoaFisicaUnidadeEnum.INQUILINO.getId()};

				List<TipoPessoaFisicaUnidade> listPessoaFisicaUnidade = tipoPessoaFisicaUnidadeRepository.findByIdIn(tipos);
				
				
				List<Parentesco> listGrauParentesco = grauParentescoRepository.findAll();
				
				
				Integer recursos[] = new Integer[pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso().size()];  
				
				String recursosids = "";
				
				recursosids = configurarRecursos(pessoaFisicaUnidade, recursos, recursosids);
				
				
				mv.addObject("sexo", sexoRepository.findAll()); 
				mv.addObject("listTipoPessoaFisicaUnidade", listPessoaFisicaUnidade); 
				mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
				mv.addObject("listGrauParentesco", listGrauParentesco); 
				mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());
				mv.addObject("recursos", recursos);
				mv.addObject("recursosids", recursosids);
				return mv;
			}
			
			
			
			
			
			
			
			
			/**
			 * TELA PARA EDIÇÃO DE INQUILINO DE UNIDADE
			 * @param id PESSOAFISICAUNIDADE QUE REPRESENTA O INQUILINO
			 * @param pessoaFisicaUnidade
			 * @return
			 */
			@RequestMapping("/unidade/inquilino/editar/{id}")
			public ModelAndView editarInquilinoUnidade(@PathVariable Integer id,PessoaFisicaUnidade pessoaFisicaUnidade,@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
				ModelAndView mv ;


				
				pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.findById(id);
				
			
				mv = new  ModelAndView("/syndic/unidades/inquilino/formincluir"); 
				
				Unidade unidade = unidadeRepository.buscarUnidadeCompleta(pessoaFisicaUnidade.getUnidade().getId());
				
				int[] tipos = {
						TipoPessoaFisicaUnidadeEnum.MORADOR.getId(),
						TipoPessoaFisicaUnidadeEnum.COMODATO.getId(),
						TipoPessoaFisicaUnidadeEnum.INQUILINO.getId()};

				List<TipoPessoaFisicaUnidade> listPessoaFisicaUnidade = tipoPessoaFisicaUnidadeRepository.findByIdIn(tipos);
				
				
				List<Parentesco> listGrauParentesco = grauParentescoRepository.findAll();
				
				
				Integer recursos[] = new Integer[pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso().size()];  
				
				String recursosids = "";
				
				recursosids = configurarRecursos(pessoaFisicaUnidade, recursos, recursosids);
				
				PessoaFisicaUnidade pessoaFisicaUnidadeusuario =  pessoaFisicaUnidadeRepository.pessoasPorUnidadePorIdPessoa(pessoaFisicaUnidade.getUnidade().getId(), usuarioSistema.getPessoaFisica().getId());
				
				PessoaFisica usuario = new PessoaFisica();
				
				if(pessoaFisicaUnidadeusuario==null){
					
					usuario = usuarioSistema.getPessoaFisica();
				}
				else{
					usuario = usuarioSistema.getPessoaFisica();
					usuario.setTipoPessoaFisicaUnidade(pessoaFisicaUnidadeusuario.getTipoPessoaFisicaUnidade().getId());
				}
				mv.addObject("sexo", sexoRepository.findAll()); 
				mv.addObject("listTipoPessoaFisicaUnidade", listPessoaFisicaUnidade); 
				mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
				mv.addObject("listGrauParentesco", listGrauParentesco); 
				mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());
				mv.addObject("recursos", recursos);
				mv.addObject("recursosids", recursosids);
				mv.addObject("usuario", usuario);
				
				return mv;
			}
			
			
			
			
			@RequestMapping("/unidade/proprietario/listar/{id}")
			public ModelAndView getProprietariosUnidades(@PathVariable Integer id) {

				ModelAndView mv = new ModelAndView("/syndic/unidades/proprietarios/listar");

				Unidade unidade = unidadeRepository.findById(id);

				List<PessoaFisicaUnidade> listPessoaFisicaUnidade =  pessoaFisicaUnidadeRepository.proprietarioPorUnidadePorCondominio(id, unidade.getCondominio().getId());


				mv.addObject("listPessoaFisicaUnidade",listPessoaFisicaUnidade);

				mv.addObject("unidade",unidade);
				mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId())); 
				return mv;
			}
			



			/**
			 * TELA PARA ADICIONAR UM PROPRIETÁRIO A UMA UNIDADE
			 * @param id DA UNIDADE
			 * @return
			 */
			@RequestMapping("/unidade/proprietario/adicionar/{id}")
			public ModelAndView novoProprietarioUnidade(@PathVariable Integer id) {

				ModelAndView mv = new ModelAndView("/syndic/unidades/proprietarios/formincluir"); 

				Unidade unidade = unidadeRepository.findById(id);//unidadeRepository.buscarUnidadeCompleta(id);
				PessoaFisicaUnidade pessoaFisicaUnidade = new PessoaFisicaUnidade(); 
				pessoaFisicaUnidade.setUnidade(unidade);
				mv.addObject("sexo", sexoRepository.findAll()); 
				mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
				mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());

				
				return mv;
			}
			
			
			


			/**
			 * TELA PARA EDIÇÃO DE PROPRIETÁRIO DE UNIDADE
			 * @param id DA PESSOAFISICAUNIDADE QUE REPRESENTA O PROPRIETÁRIO
			 * @param pessoaFisicaUnidade
			 * @return
			 */
			@RequestMapping("/unidade/proprietario/editar/{id}")
			public ModelAndView editarProprietarioUnidade(@PathVariable Integer id,PessoaFisicaUnidade pessoaFisicaUnidade) {
				ModelAndView mv = new ModelAndView("/syndic/unidades/proprietarios/formincluir"); 


				pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.findById(id);
				
				

				Unidade unidade = unidadeRepository.buscarUnidadeCompleta(pessoaFisicaUnidade.getUnidade().getId());

				mv.addObject("sexo", sexoRepository.findAll()); 
				mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
				mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());

				return mv;
			}



			/**
			 * SALVAR UM PROPRIETÁRIO PARA UNIDADE NO BANCO DE DADOS
			 * @param pessoaFisicaUnidade
			 * @param compartilhada
			 * @param result
			 * @param attributes
			 * @return
			 */
			@PostMapping(value = {"/unidade/proprietario/novo", "/unidade/proprietario/novo/{\\d+}"})
			public ModelAndView salvarProprietarioUnidade(@Valid PessoaFisicaUnidade pessoaFisicaUnidade,
					@RequestParam(value  = "compartilhada", required = false) String compartilhada, 
					BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuarioSistema) {

				
				pessoaFisicaUnidade.setPessoaFisicaCadastrante(usuarioSistema.getPessoaFisica());
				
				pessoaFisicaService.atribuirErrosProprietario(pessoaFisicaUnidade, result);

				boolean isnovo = pessoaFisicaUnidade.isNovo();
				
				if(result.hasErrors()){ 
					return novoProprietarioUnidadeErro(pessoaFisicaUnidade);
				}		
				
				//TipoPessoa tipoPessoa = new TipoPessoa(); tipoPessoa.setId(TipoPessoaFisicaEnum.MORADOR.getId());//Tipo de Pessoa mORADOR 
				TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade = new TipoPessoaFisicaUnidade(); 

				Email email =  pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0);
				List<Telefone> telefones =  pessoaFisicaUnidade.getPessoaFisica().getTelefones();
				
				
				if(compartilhada==null) {
					tipoPessoaFisicaUnidade.setId(TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId());//Tipo de Pessoa Proprietário
				}
				else {
					tipoPessoaFisicaUnidade.setId(TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId());//Tipo de Pessoa Proprietário
				}

				pessoaFisicaUnidade.getPessoaFisica().setCpf(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara());
				//pessoaFisicaUnidade.getPessoaFisica().setTipoPessoa(tipoPessoa);
				
				pessoaFisicaUnidade.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidade);


				pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());



				PessoaFisica pf = pessoaFisicaService.salvarcomretorno(pessoaFisicaUnidade.getPessoaFisica());

				pf.setTelefones(telefones);
				
				unidadeService.inserirTelefonePessoaFisicaUnidade(pf);
				unidadeService.inserirEmailPessoaFisicaUnidade(email);
				pessoaFisicaUnidade.setPessoaFisica(pf);

				pessoaFisicaService.incluirProprietario(pessoaFisicaUnidade,isnovo); 

				if(isnovo) {
				attributes.addFlashAttribute("mensagem","Proprietário cadastrado com sucesso!");
				}
				else {
					attributes.addFlashAttribute("mensagem","Proprietário editado com sucesso!");
				}
				
				return new ModelAndView("redirect:/unidade/editar/" + pessoaFisicaUnidade.getUnidade().getId());
			}
			
			


			/**
			 * SALVAR UM NOVO MORADOR NO BANCO DE DADOS
			 * @param pessoaFisicaUnidade
			 * @param recursos
			 * @param result
			 * @param attributes
			 * @return
			 */
			@PostMapping(value = {"/unidade/morador/novo", "/unidade/morador/novo/{\\d+}"})
			public ModelAndView salvarMoradorUnidade(@Valid PessoaFisicaUnidade pessoaFisicaUnidade,
					@RequestParam(value  = "recursos", required = false) String recursos, 
					BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuarioSistema) {

				
				pessoaFisicaUnidade.setPessoaFisicaCadastrante(usuarioSistema.getPessoaFisica());
				
				pessoaFisicaService.atribuirErrosMorador(pessoaFisicaUnidade, result,recursos);

				boolean isNovo = pessoaFisicaUnidade.isNovo();
				
				if(result.hasErrors()){ 
					return novoMoradorUnidadeErro(pessoaFisicaUnidade,recursos);
				}		
				

				//TipoPessoa tipoPessoa = new TipoPessoa(); tipoPessoa.setId(TipoPessoaFisicaCondominioEnum.MORADOR.getId());//Tipo de Pessoa Proprietário 
				//pessoaFisicaUnidade.getPessoaFisica().setTipoPessoa(tipoPessoa);
				pessoaFisicaUnidade.getPessoaFisica().setCpf(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara());
				pessoaFisicaUnidade.getPessoaFisica().setAtivo(1); 
				
				Email email =  pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0);
				//List<Telefone> telefones =  pessoaFisicaUnidade.getPessoaFisica().getTelefones();
				
				

				TipoPessoaFisicaUnidade tpu =  tipoPessoaFisicaUnidadeRepository.findById(TipoPessoaFisicaUnidadeEnum.MORADOR.getId());
				
				PessoaFisica pf = pessoaFisicaService.salvarcomretorno(pessoaFisicaUnidade.getPessoaFisica());
				
				email.setPessoaFisica(pf);
				
				unidadeService.inserirTelefonePessoaFisicaUnidade(pf);
				
				unidadeService.inserirEmailPessoaFisicaUnidade(email);

				pessoaFisicaUnidade.setPessoaFisica(pf);
				pessoaFisicaUnidade.setTipoPessoaFisicaUnidade(tpu);

				pessoaFisicaUnidade  = pessoaFisicaService.incluirMorador(pessoaFisicaUnidade,recursos);
				unidadeService.inserirRecursos(pessoaFisicaUnidade, recursos);


				if(isNovo) {
				attributes.addFlashAttribute("mensagem","Morador cadastrado com sucesso!");
				}
				else {
					attributes.addFlashAttribute("mensagem","Morador editado com sucesso!");
				}
				return new ModelAndView("redirect:/unidade/editar/" + pessoaFisicaUnidade.getUnidade().getId());
			}
			
			
			
			/**
			 * SALVAR UM NOVO INQUILINO NO BANCO DE DADOS
			 * @param pessoaFisicaUnidade
			 * @param recursos
			 * @param result
			 * @param attributes
			 * @return
			 */
			@PostMapping(value = {"/unidade/inquilino/novo", "/unidade/inquilino/novo/{\\d+}"})
			public ModelAndView salvarInquilinoUnidade(@Valid PessoaFisicaUnidade pessoaFisicaUnidade,
					@RequestParam(value  = "recursos", required = false) String recursos, 
					BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuarioSistema) {

				pessoaFisicaUnidade.setPessoaFisicaCadastrante(usuarioSistema.getPessoaFisica());
				
				
				pessoaFisicaService.atribuirErrosInquilino(pessoaFisicaUnidade, result,recursos);

				if(result.hasErrors()){ 
					return novoInquilinoUnidadeErro(pessoaFisicaUnidade,recursos);
				}		

				List<Telefone> telefones =  pessoaFisicaUnidade.getPessoaFisica().getTelefones();
				
				
				//TipoPessoa tipoPessoa = new TipoPessoa(); 
				//tipoPessoa.setId(TipoPessoaFisicaCondominioEnum.MORADOR.getId());//Tipo de Pessoa inquilino 
			
				//pessoaFisicaUnidade.getPessoaFisica().setTipoPessoa(tipoPessoa);
				pessoaFisicaUnidade.getPessoaFisica().setCpf(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara());
				pessoaFisicaUnidade.getPessoaFisica().setAtivo(1); 

				Email email =  pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0);
				

				PessoaFisica pf = pessoaFisicaService.salvarcomretorno(pessoaFisicaUnidade.getPessoaFisica());

				
				email.setPessoaFisica(pf);
				pf.setTelefones(telefones);
				
				unidadeService.inserirTelefonePessoaFisicaUnidade(pf);
				unidadeService.inserirEmailPessoaFisicaUnidade(email);

				pessoaFisicaUnidade.setPessoaFisica(pf);
				
				pessoaFisicaUnidade  = pessoaFisicaService.incluirInquilino(pessoaFisicaUnidade,recursos,email);


				unidadeService.inserirRecursos(pessoaFisicaUnidade, recursos);


				attributes.addFlashAttribute("mensagem","Inquilino cadastrado com sucesso!");
				
				PessoaFisicaUnidade pessoa =  pessoaFisicaUnidadeRepository.pessoasPorUnidadePorIdPessoa(pessoaFisicaUnidade.getUnidade().getId(), usuarioSistema.getPessoaFisica().getId());
				
				if(pessoa.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.INQUILINO.getId()
						|| pessoa.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.COMODATO.getId()
						|| pessoa.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.MORADOR.getId()){
					return new ModelAndView("redirect:/unidade/editarpormorador/" + pessoaFisicaUnidade.getUnidade().getId());
				}
				else{
					return new ModelAndView("redirect:/unidade/editar/" + pessoaFisicaUnidade.getUnidade().getId());
				}
			}


			
			/**
			 * TELA PARA EDIÇÃO DE FUNCIONARIO DE UNIDADE
			 * @param id DA PESSOAFISICAUNIDADE QUE REPRESENTA O PROPRIETÁRIO
			 * @param pessoaFisicaUnidade
			 * @return
			 */
			@RequestMapping("/unidade/visitante/editar/{id}")
			public ModelAndView editarFuncionarioUnidade(@PathVariable Integer id,PessoaFisicaUnidade pessoaFisicaUnidade) {
				ModelAndView mv = new ModelAndView("/syndic/unidades/visitante/formincluir"); 
				
				pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.findById(id);
				
				List<PessoaFisicaVeiculo> listPessoaFisicaVeiculo = pessoaFisicaVeiculoRepository.findByPessoaFisicaId(pessoaFisicaUnidade.getPessoaFisica().getId());
				
				Unidade unidade = unidadeRepository.buscarUnidadeCompleta(pessoaFisicaUnidade.getUnidade().getId());
				
				
				int snveiculo =0;
				if(!listPessoaFisicaVeiculo.isEmpty()){
					snveiculo = 1;
				}
				
				List<Modelo> modeloVeiculo = modeloRepository.findByMarcaIdOrderByNomeAsc(
						!pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().isEmpty() && pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo()!=null && 
								pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca()
						!=null?pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca().getId():0); 
				
				List<Marca> marcaVeiculo = marcaRepository.findByIdGreaterThanOrderByNomeAsc(0);
				List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
				
				List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
				
				mv.addObject("modeloVeiculo", modeloVeiculo); 
				mv.addObject("marcaVeiculo", marcaVeiculo); 
				mv.addObject("snveiculo", snveiculo); 
				mv.addObject("tipoVeiculo", tipoVeiculo); 
				mv.addObject("cores", cores); 
				mv.addObject("sexo", sexoRepository.findAll()); 
				mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
				mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());

				return mv;
			}
		 
		 
			@RequestMapping("/unidade/visitante/listar/{id}")
			public ModelAndView getFuncionarioUnidades(@PathVariable Integer id) {

				ModelAndView mv = new ModelAndView("/syndic/unidades/visitante/listar");

				Unidade unidade = unidadeRepository.findById(id);

				//List<PessoaFisicaUnidade> listPessoaFisicaUnidade =  pessoaFisicaUnidadeRepository.funcionarioPorUnidadePorCondominio(id, unidade.getCondominio().getId());


				//mv.addObject("listPessoaFisicaUnidade",listPessoaFisicaUnidade);
				mv.addObject("unidade",unidade);
				mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId())); 
				return mv;

			}
		 
		 
		 /**
			 * SALVAR UM VISITANTE PARA UNIDADE NO BANCO DE DADOS
			 * @param pessoaFisicaUnidade
			 * @param compartilhada
			 * @param result
			 * @param attributes
			 * @return
			 */
			@PostMapping(value = {"/unidade/visitante/novo", "/visitante/novo/{\\d+}"})
			public ModelAndView salvarVisitanteUnidade(@Valid PessoaFisicaUnidade pessoaFisicaUnidade,
					BindingResult result, RedirectAttributes attributes,
					@RequestParam(name="snveiculo",defaultValue="0") Integer snveiculo,@AuthenticationPrincipal UsuarioSistema usuario) {

				
				pessoaFisicaUnidade.setPessoaFisicaCadastrante(usuario.getPessoaFisica());
				
				pessoaFisicaService.atribuirErrosVisitante(pessoaFisicaUnidade, result,snveiculo);
				

				
				boolean isnovo = pessoaFisicaUnidade.isNovo();
				
				if(result.hasErrors()){ 
					return novoVisitanteUnidadeErro(pessoaFisicaUnidade, pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0),snveiculo);
				}		

				//TipoPessoa tipoPessoa = new TipoPessoa(); tipoPessoa.setId(TipoPessoaFisicaCondominioEnum.VISITANTE.getId());//Tipo de Pessoa mORADOR 
				TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade = new TipoPessoaFisicaUnidade(); 

				Email email =  pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0);
				List<Telefone> telefones =  pessoaFisicaUnidade.getPessoaFisica().getTelefones(); 
				
				
				PessoaFisicaVeiculo pessoaFisicaVeiculo = pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0);
			
				tipoPessoaFisicaUnidade.setId(TipoPessoaFisicaUnidadeEnum.VISITANTE.getId());//Tipo de Pessoa Proprietário
				

				pessoaFisicaUnidade.getPessoaFisica().setCpf(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara());
				//pessoaFisicaUnidade.getPessoaFisica().setTipoPessoa(tipoPessoa);
				pessoaFisicaUnidade.getPessoaFisica().setAtivo(1); 
				pessoaFisicaUnidade.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidade);


				pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());



				PessoaFisica pf = pessoaFisicaService.salvarcomretorno(pessoaFisicaUnidade.getPessoaFisica());

				
				pf.setTelefones(telefones); 
				
				
				unidadeService.inserirTelefonePessoaFisicaUnidade(pf);
				
				
				unidadeService.inserirEmailPessoaFisicaUnidade(email);
				if(snveiculo==1){
					
					if(pessoaFisicaVeiculo.isNovo()){
						pessoaFisicaVeiculo.getVeiculo().setCadastrante(usuario.getPessoaFisica());
						pessoaFisicaVeiculo.setPessoaFisica(pf);
						//pessoaFisicaVeiculo.setDataInicio(Date.valueOf(LocalDate.now()));
						pessoaFisicaVeiculo.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidadeRepository.findById(TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()));
					}
					
					try {
						unidadeService.inserirVeiculo(pessoaFisicaVeiculo);
					} catch (VeiculoJaCadastradoException e) {
						result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.placa", e.getMessage(), e.getMessage());
						return novoVisitanteUnidadeErro(pessoaFisicaUnidade, pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0),snveiculo);
					}
				}
				else if(snveiculo==0 && !pessoaFisicaVeiculo.isNovo()){
					
					pessoaFisicaVeiculoService.excluir(pessoaFisicaVeiculo);
					veiculoService.excluirVeiculo(pessoaFisicaVeiculo.getVeiculo());
					
				}
				
				pessoaFisicaUnidade.setPessoaFisica(pf);

				pessoaFisicaService.incluirVisitante(pessoaFisicaUnidade); 

				if(isnovo) {
					attributes.addFlashAttribute("mensagem","Visitante cadastrado com sucesso!");
				}
				else {
					attributes.addFlashAttribute("mensagem","Visitante editado com sucesso!");
				}
				
				return new ModelAndView("redirect:/unidade/editar/" + pessoaFisicaUnidade.getUnidade().getId());
			}
			
			
			private ModelAndView novoVisitanteUnidadeErro(PessoaFisicaUnidade pessoaFisicaUnidade,PessoaFisicaVeiculo pessoaFisicaVeiculo,Integer snveiculo) {

				ModelAndView mv = new ModelAndView("/syndic/unidades/visitante/formincluir"); 

//				System.err.println(pessoaFisicaVeiculo.getVeiculo());
//				System.err.println(pessoaFisicaVeiculo.getVeiculo().getModelo());
//				System.err.println(pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca());
				
				List<Modelo> modeloVeiculo = modeloRepository.findByMarcaIdOrderByNomeAsc(snveiculo==1 && pessoaFisicaVeiculo.getVeiculo().getModelo()!=null && pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca()
						!=null?pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getId():0);
				List<Marca> marcaVeiculo = marcaRepository.findByIdGreaterThanOrderByNomeAsc(0);
				List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
				
				List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
				
				mv.addObject("modeloVeiculo", modeloVeiculo); 
				mv.addObject("marcaVeiculo", marcaVeiculo); 
				//mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
				mv.addObject("tipoVeiculo", tipoVeiculo); 
				mv.addObject("cores", cores); 
				mv.addObject("sexo", sexoRepository.findAll()); 
				mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
				mv.addObject("sexo", sexoRepository.findAll()); 
				mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
				mv.addObject("condominio", pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio());
				mv.addObject("snveiculo", snveiculo);
				

				return mv;
			}
			
			//retorna todosProprietarios as cidades de um determinado estado
			 @RequestMapping(value = "/unidade/proprietario",consumes = MediaType.APPLICATION_JSON_VALUE)
			 public @ResponseBody String[] pesquisarProprietariosPorUnidade(@RequestParam(name="unidade",defaultValue="-1") Integer unidade) {

				 	String retorno[] = new String[1];
				 
				    Unidade uni = unidadeRepository.findById(unidade);
				 	
				    retorno[0] = uni.getListaProprietarioUnidadeNomes();
				    
				    return retorno;
				 	
			 }

			 
			 
			 
				/**
				 * CONFIGURAR RECURSOS PARA MOSTRAR NO CHECKBOX DO CADASTRO DE MORADORES
				 * @param pessoaFisicaUnidade
				 * @param recursos
				 * @param recursosids
				 * @return
				 */
				private String configurarRecursos(PessoaFisicaUnidade pessoaFisicaUnidade, Integer[] recursos, String recursosids) {
					int cont = 0;
					if(pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso()!=null){
						for (PessoaFisicaUnidadeRecurso recs : pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso()) {
							recursos[cont]=recs.getRecurso().getId();
							if(cont==0) {
								recursosids=recs.getRecurso().getId()+"";
							}
							else {
								recursosids=recursosids+","+recs.getRecurso().getId();
							}
							cont++;
						}
					}
					return recursosids;
				}

			
				private ModelAndView novoProprietarioUnidadeErro(PessoaFisicaUnidade pessoaFisicaUnidade) {

					ModelAndView mv = new ModelAndView("/syndic/unidades/proprietarios/formincluir"); 

					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
					mv.addObject("condominio", pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio());


					return mv;
				}
				
				



				private ModelAndView novoMoradorUnidadeErro(PessoaFisicaUnidade pessoaFisicaUnidade,String recursosids) {

					ModelAndView mv = new ModelAndView("/syndic/unidades/morador/formincluir"); 

					
					int[] tipos = {
							TipoPessoaFisicaUnidadeEnum.MORADOR.getId(),
							TipoPessoaFisicaUnidadeEnum.COMODATO.getId(),
							TipoPessoaFisicaUnidadeEnum.INQUILINO.getId()};

					List<TipoPessoaFisicaUnidade> listPessoaFisicaUnidade = tipoPessoaFisicaUnidadeRepository.findByIdIn(tipos);
					
					List<Parentesco> listGrauParentesco = grauParentescoRepository.findAll();
					
					
					String recursosstring[] = recursosids.split(",");
					Integer recursos[] = new Integer[recursosstring.length];//= new Integer[pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso() == null ||pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso().isEmpty()?0:pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso().size()];  
					
					int cont = 0;
					if(recursosstring.length>0) {
						for (String i : recursosstring) {
							if(!i.isEmpty()){
								recursos[cont]= Integer.valueOf(i);
							}
							cont++;
						}
					}
					
					
					mv.addObject("recursos", recursos);
					mv.addObject("recursosids", recursosids);
					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
					mv.addObject("listTipoPessoaFisicaUnidade", listPessoaFisicaUnidade); 
					
					mv.addObject("listGrauParentesco", listGrauParentesco); 
					mv.addObject("condominio", pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio());


					return mv;
				}
				
				
				
				private ModelAndView novoInquilinoUnidadeErro(PessoaFisicaUnidade pessoaFisicaUnidade,String recursosids) {

					ModelAndView mv = new ModelAndView("/syndic/unidades/inquilino/formincluir"); 

					
					String recursosstring[] = recursosids.split(",");
					Integer recursos[] = new Integer[recursosstring.length];//= new Integer[pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso() == null ||pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso().isEmpty()?0:pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso().size()];  
					
					int cont = 0;
					if(recursosstring.length>0) {
						for (String i : recursosstring) {
							if(!i.isEmpty()){
								recursos[cont]= Integer.valueOf(i);
							}
							cont++;
						}
					}

					int[] tipos = {
							TipoPessoaFisicaUnidadeEnum.COMODATO.getId(),
							TipoPessoaFisicaUnidadeEnum.INQUILINO.getId()};

					List<TipoPessoaFisicaUnidade> listPessoaFisicaUnidade = tipoPessoaFisicaUnidadeRepository.findByIdIn(tipos);
					
					
					List<Parentesco> listGrauParentesco = grauParentescoRepository.findAll();
					
					mv.addObject("listGrauParentesco", listGrauParentesco); 
					mv.addObject("listTipoPessoaFisicaUnidade", listPessoaFisicaUnidade); 
					mv.addObject("recursos", recursos);
					mv.addObject("recursosids", recursosids);
					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
					mv.addObject("condominio", pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio());


					return mv;
				}
				
				/**
				 * RESETA E ENVIA UMA NOVA SENHA PARA O PROPRIETÁRIO
				 * @param pessoaFisicaUnidade
				 * @param result
				 * @param attributes
				 * @param usuario
				 * @return
				 */
				@PostMapping("/unidade/proprietario/enviarsenha")
				public ModelAndView enviarSenhaSindico(@Valid PessoaFisicaUnidade pessoaFisicaUnidade, BindingResult result, 
						RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

					List<Telefone> telefones = pessoaFisicaUnidade.getPessoaFisica().getTelefones();

					for (Telefone telefone : telefones) {
						telefone.setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());
					}

					
					pessoaFisicaService.atribuirErrosProprietario(pessoaFisicaUnidade, result);

					if(result.hasErrors()){ 
						return novoProprietarioUnidadeErro(pessoaFisicaUnidade); 
					}		


					pessoaFisicaUnidade.getPessoaFisica().setCpf(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara());

					pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());
						
					TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade = new TipoPessoaFisicaUnidade();
					tipoPessoaFisicaUnidade.setId(TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId());//Tipo de Pessoa Proprietário.setId(TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId());//Tipo de Pessoa Proprietário
					pessoaFisicaUnidade.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidade);
					//salva os telefones
					telefoneRepository.save(pessoaFisicaUnidade.getPessoaFisica().getTelefones());

					List<Email> emails = pessoaFisicaUnidade.getPessoaFisica().getEmails();
					for (Email email : emails) {
					
						email.setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());
						email.setAtivo(1);
					}

					//salva os emails
					emailRepository.save(emails);
					pessoaFisicaService.salvar(pessoaFisicaUnidade.getPessoaFisica());
					pessoaFisicaService.incluirProprietarioEdicaoEnviandoSenha(pessoaFisicaUnidade,usuario);
					attributes.addFlashAttribute("mensagem"," Senha enviada com sucesso!");		
					return new ModelAndView("redirect:/unidade/editar/"+pessoaFisicaUnidade.getUnidade().getId());
				}
				
				
				
				 /**tLoad
				 * SALVAR UM FUNCIONARIO PARA UNIDADE NO BANCO DE DADOS
				 * @param pessoaFisicaUnidade
				 * @param compartilhada
				 * @param result
				 * @param attributes
				 * @return
				 */
				@PostMapping(value = {"/unidade/funcionario/novo", "/funcionario/novo/{\\d+}"})
				public ModelAndView salvarFuncionarioUnidade(@Valid PessoaFisicaUnidade pessoaFisicaUnidade,
						BindingResult result, RedirectAttributes attributes,
						@RequestParam(name="snveiculo",defaultValue="0") Integer snveiculo,@AuthenticationPrincipal UsuarioSistema usuario) {

					pessoaFisicaService.atribuirErrosFuncionario(pessoaFisicaUnidade, result,snveiculo);
					
					pessoaFisicaUnidade.setPessoaFisicaCadastrante(usuario.getPessoaFisica());
					
					boolean isnovo = pessoaFisicaUnidade.isNovo();
					
					if(result.hasErrors()){ 
						return novoFuncionarioUnidadeErro(pessoaFisicaUnidade, pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0),snveiculo);
					}		

					//TipoPessoa tipoPessoa = new TipoPessoa(); tipoPessoa.setId(TipoPessoaFisicaCondominioEnum.FUNCIONARIO.getId());//Tipo de Pessoa mORADOR 
					

					Email email =  pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0);
					List<Telefone> telefones =  pessoaFisicaUnidade.getPessoaFisica().getTelefones();
					
					
					PessoaFisicaVeiculo pessoaFisicaVeiculo = pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0);
				
					
					

					

					pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());


					pessoaFisicaUnidade.getPessoaFisica().setCpf(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara());
					//pessoaFisicaUnidade.getPessoaFisica().setTipoPessoa(tipoPessoa);
					pessoaFisicaUnidade.getPessoaFisica().setAtivo(1); 
					PessoaFisica pf = pessoaFisicaService.salvarcomretorno(pessoaFisicaUnidade.getPessoaFisica());

					
					pf.setTelefones(telefones);
					
					
					
					unidadeService.inserirTelefonePessoaFisicaUnidade(pf);
					unidadeService.inserirEmailPessoaFisicaUnidade(email);
					
					if(snveiculo==1){
						
						if(pessoaFisicaVeiculo.isNovo()){
							pessoaFisicaVeiculo.getVeiculo().setCadastrante(usuario.getPessoaFisica());
							pessoaFisicaVeiculo.setPessoaFisica(pf);
							//pessoaFisicaVeiculo.setDataInicio(Date.valueOf(LocalDate.now()));
							pessoaFisicaVeiculo.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidadeRepository.findById(TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()));
						}
						
						
						try {
							unidadeService.inserirVeiculo(pessoaFisicaVeiculo);
						} catch (VeiculoJaCadastradoException e) {
							result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.placa", e.getMessage(), e.getMessage());
							return novoFuncionarioUnidadeErro(pessoaFisicaUnidade, pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0),snveiculo);
						}
						
						
					}
					else if(snveiculo==0 && !pessoaFisicaVeiculo.isNovo()){
						
						pessoaFisicaVeiculoService.excluir(pessoaFisicaVeiculo);
						veiculoService.excluirVeiculo(pessoaFisicaVeiculo.getVeiculo());
						
					}
					
					pessoaFisicaUnidade.setPessoaFisica(pf);

					TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade = new TipoPessoaFisicaUnidade(); 
					tipoPessoaFisicaUnidade.setId(TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId());//Tipo de Pessoa FUNCIONARIO
					pessoaFisicaUnidade.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidade);

					
					pessoaFisicaService.incluirFuncionario(pessoaFisicaUnidade); 

					if(isnovo) {
						attributes.addFlashAttribute("mensagem","Funcionário cadastrado com sucesso!");
					}
					else {
						attributes.addFlashAttribute("mensagem","Funcionário editado com sucesso!");
					}
					
					return new ModelAndView("redirect:/unidade/editar/" + pessoaFisicaUnidade.getUnidade().getId());
				}
				
				
				
				
				
				
				/**
				 * RESETA E ENVIA UMA NOVA SENHA PARA O FUNCIONARIO DA EMPRESA
				 * @param pessoaFisicaUnidade
				 * @param result
				 * @param attributes
				 * @param usuario
				 * @return
				 */
				@PostMapping("/empresa/funcionario/enviarsenha")
				public ModelAndView enviarSenhaSindico(@Valid PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica, BindingResult result, 
						RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

					

					
					pessoaFisicaService.atribuirErrosFuncionarioEmpresa(pessoaJuridicaPessoaFisica, result);

					if(result.hasErrors()){ 
						return novoFuncionarioEmpresaErro(pessoaJuridicaPessoaFisica); 
					}		



					pessoaJuridicaPessoaFisica.getPessoaFisica().setCpf(pessoaJuridicaPessoaFisica.getPessoaFisica().getCpfSemMascara());
					
					//pessoaFisicaUnidade.getPessoaFisica().setTipoPessoa(tipoPessoa);
					pessoaJuridicaPessoaFisica.getPessoaFisica().setAtivo(1); 
					
					pessoaFisicaService.incluirFuncionarioEmpresa(pessoaJuridicaPessoaFisica,false);
					
					
					
					pessoaJuridicaPessoaFisica.getPessoaFisica().setPapeis(pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getNome());
					
					//salva os emails
					pessoaFisicaService.incluirFuncionarioEdicaoEnviandoSenha(pessoaJuridicaPessoaFisica,usuario);
					
					attributes.addFlashAttribute("mensagem"," Senha enviada com sucesso!");		
					
					return new ModelAndView("redirect:/empresa/funcionario/editar/"+pessoaJuridicaPessoaFisica.getId());
				}
				
				
				
				
				@PostMapping(value = {"/empresa/funcionario", "/empresa/funcionario/{\\d+}"})
				public ModelAndView salvarFuncionarioUnidade(@Valid PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica,
						BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

					
					
					
					pessoaFisicaService.atribuirErrosFuncionarioEmpresa(pessoaJuridicaPessoaFisica, result);
					
					
					boolean isnovo = pessoaJuridicaPessoaFisica.isNovo();
					
					if(result.hasErrors()){ 
						return novoFuncionarioEmpresaErro(pessoaJuridicaPessoaFisica);
					}		

					
					pessoaJuridicaPessoaFisica.getPessoaFisica().setCpf(pessoaJuridicaPessoaFisica.getPessoaFisica().getCpfSemMascara());
					
					//pessoaFisicaUnidade.getPessoaFisica().setTipoPessoa(tipoPessoa);
					pessoaJuridicaPessoaFisica.getPessoaFisica().setAtivo(1); 
					
					pessoaFisicaService.incluirFuncionarioEmpresa(pessoaJuridicaPessoaFisica,isnovo);

					
					

					if(isnovo) {
						attributes.addFlashAttribute("mensagem","Funcionário cadastrado com sucesso!");
					}
					else {
						attributes.addFlashAttribute("mensagem","Funcionário editado com sucesso!");
					}
					
					return new ModelAndView("redirect:/empresa/funcionarios");
				}
				
				
				
				
				
				private ModelAndView novoFuncionarioUnidadeErro(PessoaFisicaUnidade pessoaFisicaUnidade,PessoaFisicaVeiculo pessoaFisicaVeiculo,Integer snveiculo) {

					ModelAndView mv = new ModelAndView("/syndic/unidades/funcionario/formincluir"); 

//					System.err.println(pessoaFisicaVeiculo.getVeiculo());
//					System.err.println(pessoaFisicaVeiculo.getVeiculo().getModelo());
//					System.err.println(pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca());
					
					List<Modelo> modeloVeiculo = modeloRepository.findByMarcaIdOrderByNomeAsc(snveiculo==1 && pessoaFisicaVeiculo.getVeiculo().getModelo()!=null && pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca()
							!=null?pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getId():0);
					List<Marca> marcaVeiculo = marcaRepository.findByIdGreaterThanOrderByNomeAsc(0);
					List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
					
					List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
					
					mv.addObject("modeloVeiculo", modeloVeiculo); 
					mv.addObject("marcaVeiculo", marcaVeiculo); 
					//mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
					mv.addObject("tipoVeiculo", tipoVeiculo); 
					mv.addObject("cores", cores); 
					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
					mv.addObject("condominio", pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio());
					mv.addObject("snveiculo", snveiculo);
					

					return mv;
				}


				
				
				
				
				 /**
				 * TELA PARA ADICIONAR UM VISITANTE A UMA UNIDADE
				 * @param id DA UNIDADE
				 * @return
				 */
				@RequestMapping("/unidade/visitante/adicionar/{id}")
				public ModelAndView novoFuncionarioUnidade(@PathVariable Integer id) {

					ModelAndView mv = new ModelAndView("/syndic/unidades/visitante/formincluir"); 

					Unidade unidade = unidadeRepository.findById(id); //unidadeRepository.buscarUnidadeCompleta(id);
					PessoaFisicaUnidade pessoaFisicaUnidade = new PessoaFisicaUnidade(); 
					pessoaFisicaUnidade.setUnidade(unidade);

					
					TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade = new TipoPessoaFisicaUnidade();
					tipoPessoaFisicaUnidade.setId(TipoPessoaFisicaUnidadeEnum.VISITANTE.getId());
					pessoaFisicaUnidade.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidade);
					
					
					
//					PessoaFisicaVeiculo pessoaFisicaVeiculo = new PessoaFisicaVeiculo(); 
//					PessoaFisica pf= new PessoaFisica();
					
					
					List<Modelo> modeloVeiculo = new ArrayList<>();
					
					List<Marca> marcaVeiculo = marcaRepository.findByIdGreaterThanOrderByNomeAsc(0);
					List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
					
					List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
					
					mv.addObject("modeloVeiculo", modeloVeiculo); 
					mv.addObject("marcaVeiculo", marcaVeiculo); 
					//mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
					mv.addObject("tipoVeiculo", tipoVeiculo); 
					mv.addObject("cores", cores); 
					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
					mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio()); 
					
					
					
					return mv;
				}
				

				
				
				
				 /**
				 * TELA PARA ADICIONAR UM VISITANTE A UMA UNIDADE
				 * @param id DA UNIDADE
				 * @return
				 */
				@RequestMapping("/unidade/funcionario/adicionar/{id}")
				public ModelAndView novoVisitanteUnidade(@PathVariable Integer id) {

					ModelAndView mv = new ModelAndView("/syndic/unidades/funcionario/formincluir"); 

					Unidade unidade = unidadeRepository.findById(id); //unidadeRepository.buscarUnidadeCompleta(id);
					PessoaFisicaUnidade pessoaFisicaUnidade = new PessoaFisicaUnidade(); 
					pessoaFisicaUnidade.setUnidade(unidade);

					
					TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade = new TipoPessoaFisicaUnidade();
					tipoPessoaFisicaUnidade.setId(TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId());
					pessoaFisicaUnidade.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidade);
					
					
					
//					PessoaFisicaVeiculo pessoaFisicaVeiculo = new PessoaFisicaVeiculo(); 
//					PessoaFisica pf= new PessoaFisica();
					
					
					List<Modelo> modeloVeiculo = new ArrayList<>();
					
					List<Marca> marcaVeiculo = marcaRepository.findByIdGreaterThanOrderByNomeAsc(0);
					List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
					
					List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
					
					mv.addObject("modeloVeiculo", modeloVeiculo); 
					mv.addObject("marcaVeiculo", marcaVeiculo); 
					//mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
					mv.addObject("tipoVeiculo", tipoVeiculo); 
					mv.addObject("cores", cores); 
					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
					mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());
					
					
					
					return mv;
				}
				
				
				@RequestMapping("/empresa/funcionarios")
				public ModelAndView getMoradoresCondominio(@AuthenticationPrincipal UsuarioSistema usuarioSistema) {

					ModelAndView mv = new ModelAndView("/syndic/empresa/pessoas/funcionario/listar");
					
					PessoaJuridica empresa = pessoaJuridicaRepository.findById(usuarioSistema.getPessoaJuridica().getId());
					mv.addObject("empresa", empresa); 
					
					
					return mv;

				}
				
				
				
				
				/**
				 * TELA PARA EDIÇÃO DE FUNCIONARIO DE UNIDADE
				 * @param id DA PESSOAFISICAUNIDADE QUE REPRESENTA O PROPRIETÁRIO
				 * @param pessoaFisicaUnidade
				 * @return
				 */
				@RequestMapping("/empresa/funcionario/editar/{id}")
				public ModelAndView editarFuncionarioEmpresa(@PathVariable Integer id,PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica,@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
					ModelAndView mv = new ModelAndView("/syndic/empresa/pessoas/funcionario/formincluir"); 
					
					pessoaJuridicaPessoaFisica = pessoaJuridicaPessoaFisicaRepository.findById(id);
					pessoaJuridicaPessoaFisica.carregarProfissoesIds();
					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("profissoes", profissaoRepository.findAll());
					mv.addObject("funcoes", funcaoPessoaJuridicaRepository.findByPessoaJuridicaId(usuarioSistema.getPessoaJuridica().getId()));
					mv.addObject("pessoaJuridicaPessoaFisica", pessoaJuridicaPessoaFisica);

					return mv;
				}
				
				@RequestMapping("/empresa/funcionario/excluir/{id}")
				@ResponseStatus(value = HttpStatus.OK)
				public ResponseEntity<String> excluirFuncionario(@PathVariable("id") int id) {
					
					PessoaFisica pessoaFisica = pessoaFisicaRepository.findById(id);
					
					System.err.println(pessoaFisica.getNome());
					
					pessoaFisica.setAtivo(0);
					pessoaFisicaRepository.save(pessoaFisica);
					 
					return ResponseEntity.ok("ok");
				}
				
				
				 /**
				 * TELA PARA ADICIONAR UM NOVO FUNCIONARIO
				 * @param 
				 * @return
				 */
				@RequestMapping("/empresa/funcionario/adicionar")
				public ModelAndView novoFuncionarioEmpresa(@AuthenticationPrincipal UsuarioSistema usuarioSistema) {

					ModelAndView mv = new ModelAndView("/syndic/empresa/pessoas/funcionario/formincluir"); 

					PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica = new PessoaJuridicaPessoaFisica(); 
					pessoaJuridicaPessoaFisica.setPessoajuridica(usuarioSistema.getPessoaJuridica());
					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("profissoes", profissaoRepository.findAll());
					mv.addObject("funcoes", funcaoPessoaJuridicaRepository.findByPessoaJuridicaId(usuarioSistema.getPessoaJuridica().getId()));
					mv.addObject("pessoaJuridicaPessoaFisica", pessoaJuridicaPessoaFisica);
					
					return mv;
				}
				
				
				
				private ModelAndView novoFuncionarioEmpresaErro(PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica) {

					ModelAndView mv = new ModelAndView("/syndic/empresa/pessoas/funcionario/formincluir"); 

					
					
					mv.addObject("sexo", sexoRepository.findAll()); 
					
					mv.addObject("profissoes", profissaoRepository.findAll());
					mv.addObject("funcoes", funcaoPessoaJuridicaRepository.findAll());
					mv.addObject("pessoaJuridicaPessoaFisica", pessoaJuridicaPessoaFisica);

					return mv;
				}
				
				
				
				
				
				
				
				/**
				 * TELA PARA EDIÇÃO DE VISITANTE DE UNIDADE
				 * @param id DA PESSOAFISICAUNIDADE QUE REPRESENTA O PROPRIETÁRIO
				 * @param pessoaFisicaUnidade
				 * @return
				 */
				@RequestMapping("/unidade/funcionario/editar/{id}")
				public ModelAndView editarVisitanteUnidade(@PathVariable Integer id,PessoaFisicaUnidade pessoaFisicaUnidade) {
					ModelAndView mv = new ModelAndView("/syndic/unidades/funcionario/formincluir"); 
					
					pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.findById(id);
					
					List<PessoaFisicaVeiculo> listPessoaFisicaVeiculo = pessoaFisicaVeiculoRepository.findByPessoaFisicaId(pessoaFisicaUnidade.getPessoaFisica().getId());
					
					Unidade unidade = unidadeRepository.buscarUnidadeCompleta(pessoaFisicaUnidade.getUnidade().getId());
					
					
					int snveiculo =0;
					if(!listPessoaFisicaVeiculo.isEmpty()){
						snveiculo = 1;
					}
					
					List<Modelo> modeloVeiculo = modeloRepository.findByMarcaIdOrderByNomeAsc(
							!pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().isEmpty() && pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo()!=null && 
									pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca()
							!=null?pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca().getId():0); 
					
					List<Marca> marcaVeiculo = marcaRepository.findByIdGreaterThanOrderByNomeAsc(0);
					List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);
					
					List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);
					
					mv.addObject("modeloVeiculo", modeloVeiculo); 
					mv.addObject("marcaVeiculo", marcaVeiculo); 
					mv.addObject("snveiculo", snveiculo); 
					mv.addObject("tipoVeiculo", tipoVeiculo); 
					mv.addObject("cores", cores); 
					mv.addObject("sexo", sexoRepository.findAll()); 
					mv.addObject("pessoaFisicaUnidade", pessoaFisicaUnidade);
					mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());

					return mv;
				}
			 
			 
				@RequestMapping("/unidade/funcionario/listar/{id}")
				public ModelAndView getVisitanteUnidades(@PathVariable Integer id) {

					ModelAndView mv = new ModelAndView("/syndic/unidades/funcionario/listar");

					Unidade unidade = unidadeRepository.findById(id);

					//List<PessoaFisicaUnidade> listPessoaFisicaUnidade =  pessoaFisicaUnidadeRepository.funcionarioPorUnidadePorCondominio(id, unidade.getCondominio().getId());


					//mv.addObject("listPessoaFisicaUnidade",listPessoaFisicaUnidade);
					mv.addObject("unidade",unidade);
					mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId())); 
					return mv;

				}
	 	
	 	
	
}