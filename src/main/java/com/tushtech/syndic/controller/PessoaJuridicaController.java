package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.AreaAtuacao;
import com.tushtech.syndic.entity.PessoaJuridica;
import com.tushtech.syndic.repository.AreaAtuacaoRepository;
import com.tushtech.syndic.repository.PessoaJuridicaRepository;
import com.tushtech.syndic.service.PessoaJuridicaService;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;


@Controller
@RequestMapping("/pessoajuridica")
public class PessoaJuridicaController {
	
	@Autowired
	private PessoaJuridicaRepository pessoaJuridicaRepository; 
	
	@Autowired
	private PessoaJuridicaService pessoaJuridicaService;
	
	@Autowired
	private AreaAtuacaoRepository areaAtuacaoRepository;
	
	@RequestMapping
	public ModelAndView inicio(PessoaJuridica pessoaJuridica) { 
		
		return formulario(pessoaJuridica);
	}
	
	@RequestMapping("/{id}")
	public ModelAndView editar(PessoaJuridica pessoaJuridica , RedirectAttributes attributes){
		
		pessoaJuridica = pessoaJuridicaRepository.findOne(pessoaJuridica.getId());
		
		if(pessoaJuridica != null && pessoaJuridica.getAtivo() != 1 && !pessoaJuridica.isNovo()){
			attributes.addFlashAttribute("proibido", " Pessoa Jurídica está desativada! <a href=\"/pessoajuridica/ativar/" + pessoaJuridica.getId() + "\">Deseja Ativá-lo? <strong>SIM</strong></a>");
			return new ModelAndView("redirect:/material/gerenciar");
		}
		
		if(pessoaJuridica==null || pessoaJuridica.isNovo()){
			pessoaJuridica = new PessoaJuridica();
		}else{			
			pessoaJuridica = pessoaJuridicaRepository.findOne(pessoaJuridica.getId());
		}
		ModelAndView mv = formulario(pessoaJuridica);
		return mv;
	}
	
	@PostMapping("/{id}")
	public ModelAndView salvar(@Valid PessoaJuridica pessoaJuridica, BindingResult result, RedirectAttributes attributes) {
		if (result.hasErrors()) {
			return formulario(pessoaJuridica);
		}
		
		try {
			pessoaJuridicaService.salvar(pessoaJuridica);
		} catch (NomeDuplicadoException e) { 
			result.rejectValue("cnpj", e.getMessage(), e.getMessage());
			return formulario(pessoaJuridica);
		}
		
		attributes.addFlashAttribute("mensagem", "Registro salvo com sucesso!");
		return new ModelAndView("redirect:/pessoajuridica");
	}
	
	
	@GetMapping("/ativar/{id}")
	public ModelAndView ativar(PessoaJuridica pessoaJuridica,RedirectAttributes attributes)
	{
		pessoaJuridica = pessoaJuridicaService.ativar(pessoaJuridica);
		attributes.addFlashAttribute("mensagem", "Registro ativado com sucesso!");
		return new ModelAndView("redirect:/pessoajuridica"); 
	}
	
	@DeleteMapping("/{id}")
	public @ResponseBody Integer desativar(PessoaJuridica pessoaJuridica)
	{
		pessoaJuridicaService.desativar(pessoaJuridica);
		return pessoaJuridica.getId();
	}	

	//FORMULARIO PARA E EDITAR E SALVAR
	private ModelAndView formulario(PessoaJuridica pessoaJuridica) { 
		
		ModelAndView mv = new ModelAndView("/syndic/sistema/pessoajuridica/formulario");
		
		List<AreaAtuacao> listAreaAtuacao =  areaAtuacaoRepository.findByAtivo(1);
		List<PessoaJuridica> listPessoaJuridica = pessoaJuridicaRepository.findByAtivoAndCondominioIsNull(1); 
		
		mv.addObject("listAreaAtuacao", listAreaAtuacao);	
		mv.addObject("listPessoaJuridica", listPessoaJuridica);	
		mv.addObject("pessoaJuridica", pessoaJuridica);	
		return mv;
	}
}