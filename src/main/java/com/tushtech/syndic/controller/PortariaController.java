package com.tushtech.syndic.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.dto.VisitanteDTO;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.HistoricoPortaria;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.HistoricoPortariaRepository;
import com.tushtech.syndic.repository.PessoaFisicaUnidadeRepository;

@Controller
@RequestMapping("/portaria")
public class PortariaController {
	
	@Autowired
	private PessoaFisicaUnidadeRepository pessoaFisicaUnidadeRepository; 
	
	@Autowired
	private HistoricoPortariaRepository historicoPortariaRepository; 
	
	
	@Autowired
	private CondominioRepository condominioRepository;
	
	 //retorna todas as cidades de um determinado estado
	 @RequestMapping(value = "/localizar",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody List<VisitanteDTO> pesquisarPessoas(
			 String parametro) {
		 	
		
		 List<PessoaFisicaUnidade> listPessoa =  pessoaFisicaUnidadeRepository.findByPessoaFisicaNomeContaining(parametro);
		 
		 List<VisitanteDTO> listaRetorno = new ArrayList<>();
		 
		 for (PessoaFisicaUnidade pessoaFisicaUnidade : listPessoa) {
			 VisitanteDTO visita = new VisitanteDTO();
			 visita.setNome(pessoaFisicaUnidade.getPessoaFisica().getNome());
			 listaRetorno.add(visita);
		 }
		 
		 return listaRetorno;
		 
		 	
	 }
	 
	 
	 @RequestMapping("/{id}")  
		public ModelAndView telaPortaria(@PathVariable Integer id, @AuthenticationPrincipal UsuarioSistema usuario) {

			List<PessoaFisicaUnidade> listPessoaFisicaUnidade = new ArrayList<>();
			
			Condominio condominio = condominioRepository.findOne(id);
			
			listPessoaFisicaUnidade = pessoaFisicaUnidadeRepository.proprietariosMoradoresInquilinosFuncionariosUnidade(id);
			
			HistoricoPortaria historicoPortaria = new HistoricoPortaria();
			
			historicoPortaria.setCadastrante(usuario.getPessoaFisica());
			historicoPortaria.setCondominio(condominio);
			
			ModelAndView mv = new ModelAndView("/syndic/portaria/telaporteiro"); 

			mv.addObject("historicoPortaria",historicoPortaria);
			mv.addObject("listPessoaFisicaUnidade",listPessoaFisicaUnidade);
			
			return mv;
		}
	 
	 
	 @PostMapping("/entrada")
		public ModelAndView salvarMoradorUnidade(@Valid HistoricoPortaria historicoPortaria,
				BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

		 
		 	if(result.hasErrors()){ 
				return telaPortaria(historicoPortaria.getCondominio().getId(),usuario);
			}
		 
		     historicoPortaria  = historicoPortariaRepository.saveAndFlush(historicoPortaria);
		 	
			return new ModelAndView("redirect:/sindico/portaria/"+historicoPortaria.getCondominio().getId());
		}
	 
	 
	 
	
}