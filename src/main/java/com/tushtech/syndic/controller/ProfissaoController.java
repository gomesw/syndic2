package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Profissao;
import com.tushtech.syndic.repository.ProfissaoRepository;
import com.tushtech.syndic.service.ProfissaoService;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;

@Controller
@RequestMapping("/profissao")
public class ProfissaoController {
	
	@Autowired
	private ProfissaoRepository profissaoRepository; 
	
	@Autowired
	private ProfissaoService profissaoService;

	@RequestMapping
	public ModelAndView inicio(Profissao profissao) {
		return formulario(profissao);
	}
	
	@RequestMapping("/{id}")
	public ModelAndView editar(Profissao profissao, RedirectAttributes attributes){
		
		profissao = profissaoRepository.findOne(profissao.getId());
		
		if(profissao != null && profissao.getAtivo() != 1 && !profissao.isNovo()){
			attributes.addFlashAttribute("proibido", " Marca de veículo está desativada! <a href=\"/marcaveiculo/ativar/" + profissao.getId() + "\">Deseja AtivÁ-la ?</a>");
			return new ModelAndView("redirect:/profissao/gerenciar");
		}
		
		if(profissao==null || profissao.isNovo()){
			profissao = new Profissao();
		}else{			
			profissao = profissaoRepository.findOne(profissao.getId());
		}
		ModelAndView mv = formulario(profissao);
		return mv;
	}
	
	@PostMapping("/{id}")
	public ModelAndView salvar(@Valid Profissao profissao, BindingResult result, RedirectAttributes attributes) {
		
		if (result.hasErrors()) {
			return formulario(profissao);
		}
		
		
		try {
			profissaoService.salvar(profissao);			
		} catch (NomeDuplicadoException e) {
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return formulario(profissao);
		}

		attributes.addFlashAttribute("mensagem", "Profissão cadastrada com sucesso!");
		return new ModelAndView("redirect:/profissao");
	}
	
	//FORMULARIO PARA E EDITAR E SALVAR
	private ModelAndView formulario(Profissao profissao) {
		ModelAndView mv = new ModelAndView("/syndic/sistema/profissao/formulario");

		mv.addObject("listProfissao", profissaoRepository.findByAtivo(1));
		mv.addObject("profissao", profissao);
		return mv;
	}
	
	@GetMapping("/ativar/{id}")
	public ModelAndView ativar(Profissao profissao,RedirectAttributes attributes)
	{
		profissao = profissaoService.ativar(profissao);
		attributes.addFlashAttribute("mensagem", "Profissão ativada com sucesso!");
		return new ModelAndView("redirect:/profissao");
	}
	
	@DeleteMapping("/{id}")
	public @ResponseBody Integer desativar(Profissao profissao)
	{
		profissaoService.desativar(profissao);
		return profissao.getId();
	}	
	
}