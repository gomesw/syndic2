package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Raca;
import com.tushtech.syndic.entity.TipoAnimal;
import com.tushtech.syndic.repository.RacaRepository;
import com.tushtech.syndic.repository.TipoAnimalRepository;
import com.tushtech.syndic.service.RacaService;


@Controller
@RequestMapping("/raca")
public class RacaController {
	
	@Autowired
	private RacaRepository racaRepository; 
	
	
	@Autowired
	private TipoAnimalRepository tipoAnimalRepository; 
	
	
	@Autowired
	private RacaService racaService;	
	

	@RequestMapping("/listar")
	public ModelAndView marca() {
		ModelAndView mv = new ModelAndView("/syndic/sistema/raca/listar");
		mv.addObject("raca", racaRepository.findAll());
		return mv;
	}	
	
	@RequestMapping("/incluir")
	public ModelAndView novoRaca(Raca raca) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/raca/incluir");
		
		List<TipoAnimal> listaTipoAnimal  = tipoAnimalRepository.findAll();
		
		mv.addObject("listaTipoAnimal", listaTipoAnimal);
		mv.addObject("raca", raca);
		return mv;
	}
	
	@PostMapping("/incluir")
	public ModelAndView salvarTipoAnimal(@Valid Raca raca, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novoRaca(raca);
		}
		
		racaService.salvarRaca(raca);
		
		if(raca.isNovo()) {
		attributes.addFlashAttribute("mensagem","Raça cadastrada com sucesso!");
		}
		else {
			attributes.addFlashAttribute("mensagem","Raça editada com sucesso!");
		}
		return new ModelAndView("redirect:/raca/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarCondominio(@PathVariable Integer id, Raca marca)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/raca/incluir");
		
		List<TipoAnimal> listaTipoAnimal  = tipoAnimalRepository.findAll();
		
		mv.addObject("listaTipoAnimal", listaTipoAnimal);
		mv.addObject("raca",racaRepository.findOne(marca.getId()));
		
		return mv;
	}	 
	
	 @RequestMapping(value = "/pesquisarracas",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody List<Raca> pesquisarRAacasPorTipoAnimal(@RequestParam(name="id",defaultValue="-1") Integer tipo) {
		 
		 	return racaRepository.findByTipoAnimalIdOrderByNomeAsc(tipo);
		 	
	 }	
}