package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Recurso;
import com.tushtech.syndic.repository.RecursoRepository;
import com.tushtech.syndic.service.RecursoService;

/**
 * @author Wallace
 *
 */
@Controller
@RequestMapping("/recurso")

public class RecursoController {
	
	@Autowired
	private RecursoRepository recursoRepository; 
	
	@Autowired
	private RecursoService recursoService;	
	
	
	@RequestMapping("/listar")
	public ModelAndView listaRecurso() {

		ModelAndView mv = new ModelAndView("syndic/sistema/recurso/listar");
		List<Recurso> listRecursos = recursoRepository.findByAtivo(1);	
		mv.addObject("recursos",listRecursos);
		
		return mv;
	}
	
	
	@RequestMapping("/incluir")
	public ModelAndView novoRecurso(Recurso recurso) {
		ModelAndView mv = new ModelAndView("syndic/sistema/recurso/incluir");
		mv.addObject("recurso",recurso);
		return mv;
	}
	
	
	@RequestMapping(value = {"/incluir", "/incluir/{\\d+}"}, method= RequestMethod.POST)
	public ModelAndView salvarRecurso(@Valid Recurso recurso, BindingResult result, RedirectAttributes attributes) {
		
		if(result.hasErrors()){
			return novoRecurso(recurso);
		}
		
		//verificar se o recurso ja existe no banco
		Recurso recursoBanco = (!recurso.isNovo()?null:recursoRepository.findByNomeAndAtivo(recurso.getNome(),1));
		
		if(recursoBanco == null || !recurso.isNovo()){
			recursoService.salvarRecurso(recurso);			
			attributes.addFlashAttribute("mensagem"," Cadastro Realizado com sucesso!");
			return new ModelAndView("redirect:/recurso/listar");
		}else{			
			attributes.addFlashAttribute("mensagem"," Já existe um recurso cadastrado com o mesmo nome!");
			return new ModelAndView("redirect:/recurso/listar");
		}		
		
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView visualizarRecurso(@PathVariable Integer id,Recurso recurso) 
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/recurso/incluir");
		if(id!=null){
			recurso = recursoRepository.findOne(id);
		}
		mv.addObject("recurso",recurso);
		return mv;
	}

	
	
//	@RequestMapping("/excluir/{id}")
//	public @ResponseBody Recurso  desativarRecurso(Recurso recurso) 
//	{
//		recursoRepository.delete(recurso);
//		return recurso;
//	}
	
	
//	@RequestMapping(value="/ativar/{id}")
//	public ModelAndView ativarRecurso(@PathVariable Integer id,RedirectAttributes attribute) 
//	{
//		Recurso recurso = recursoRepository.findOne(id);
//		recursoService.ativarRecurso(recurso);
//		attribute.addFlashAttribute("mensagem"," Recurso ativado com sucesso!");
//		return new ModelAndView("redirect:/recurso/listar");
//
//
//	}
	
	
//	@RequestMapping(value="/desativar/{id}")
//	public ModelAndView desativarRecurso(@PathVariable Integer id,RedirectAttributes attribute) 
//	{
//		Recurso recurso = recursoRepository.findOne(id);
//		recursoService.desativarRecurso(recurso);
//		
//		attribute.addFlashAttribute("mensagem"," Recurso Desativado com sucesso!");
//		return new ModelAndView("redirect:/recurso/listar");
//
//
//	}
	
	
}



