package com.tushtech.syndic.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.tushtech.syndic.dto.OrdemServicoPorNaturezaDoServico;
import com.tushtech.syndic.dto.PacoteServicoPorCondominio;
import com.tushtech.syndic.dto.RelatorioConsumoCotasOSDTO;
import com.tushtech.syndic.dto.TiposDeServicoOS;
import com.tushtech.syndic.entity.Atendimento;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.CondominioPacoteServico;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.entity.OrdemServicoStatus;
import com.tushtech.syndic.entity.PacoteServico;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaJuridicaCondominio;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.enumeration.CoresRGB;
import com.tushtech.syndic.entity.enumeration.MesesDoAno;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaCondominioEnum;
import com.tushtech.syndic.entity.enumeration.TipoServicoIdentificado;
import com.tushtech.syndic.repository.AtendimentoRepository;
import com.tushtech.syndic.repository.CondominioPacoteServicoRepository;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.OrdemServicoRepository;
import com.tushtech.syndic.repository.OrdemServicoStatusRepository;
import com.tushtech.syndic.repository.PacoteServicoRepository;
import com.tushtech.syndic.repository.PessoaFisicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaJuridicaCondominioRepository;

@Controller
@RequestMapping()
public class RelatorioController {
	
	@Autowired
	private CondominioPacoteServicoRepository condominioPacoteServicoRepository;

	@Autowired
	private PacoteServicoRepository pacoteServicoRepository;
	
	@Autowired
	private OrdemServicoRepository ordemServicoRepository;
	
	@Autowired
	private OrdemServicoStatusRepository ordemServicoStatusRepository;
	
	@Autowired
	private CondominioRepository condominioRepository;
	
	@Autowired
	private PessoaFisicaCondominioRepository pessoaFisicaCondominioRepository;
	
	@Autowired
	private PessoaJuridicaCondominioRepository pessoaJuridicaCondominioRepository;
	
	@Autowired
	private AtendimentoRepository atendimentoRepository;
	
	
	@RequestMapping("/relatorio")
	public ModelAndView inicio() {
		ModelAndView mv = new ModelAndView("/syndic/relatorio/os");
		
		List<CondominioPacoteServico> listCondominioPacoteServico = condominioPacoteServicoRepository.findByAtivo(1);
		List<PacoteServico> listPacoteServico = pacoteServicoRepository.findByAtivo(1);
	
		List<RelatorioConsumoCotasOSDTO> listRelatorioConsumoCotasOSDTO =  new ArrayList<RelatorioConsumoCotasOSDTO>(); 
		
		
		List<OrdemServico> ospormes = ordemServicoRepository.osPorMesAno(LocalDate.now().getMonthValue(), LocalDate.now().getYear());
 		
		
		for (CondominioPacoteServico condominioPacoteServico : listCondominioPacoteServico) { 
			
			RelatorioConsumoCotasOSDTO relatorio = new RelatorioConsumoCotasOSDTO();
				relatorio.setCodigo(condominioPacoteServico.getCondominio().getId());
				relatorio.setNome(condominioPacoteServico.getCondominio().getPessoaJuridica().getNomeFantasia());
				relatorio.setNomePacoteServico(condominioPacoteServico.getPacoteServico().getNome());
				
				relatorio.setQuantidadeVisitas(condominioPacoteServico.getPacoteServico().getQtdVisitas());
				
				relatorio.setValorMaodeObra(BigDecimal.ZERO);
				relatorio.setValorMaterial(BigDecimal.ZERO);
				
				for (OrdemServico os : ospormes) {
					
					if(os.getCondominio().getId()==condominioPacoteServico.getCondominio().getId()) {
						relatorio.setOsAberta(relatorio.getOsAberta()+1);
						
						relatorio.setValorMaodeObra(relatorio.getValorMaodeObra().add(os.getValorMaoDeObra()==null?BigDecimal.ZERO:os.getValorMaoDeObra()));
						relatorio.setValorMaterial(relatorio.getValorMaterial().add(os.getValorMaterial()==null?BigDecimal.ZERO:os.getValorMaterial()));
						
					}
				}
				
				listRelatorioConsumoCotasOSDTO.add(relatorio);
		}
		
		
		List<PacoteServicoPorCondominio> pct = new ArrayList<>();
		
		int cont = 1;
		for (PacoteServico pacoteServico : listPacoteServico) {
			pct.add(new PacoteServicoPorCondominio(pacoteServico.getId(), pacoteServico.getNome(), 0, CoresRGB.COR.getPorNumero(cont)));
			cont++;
		}
		
		for (CondominioPacoteServico condominioPacoteServico : listCondominioPacoteServico) { 
			for (PacoteServicoPorCondominio pacoteServicoPorCondominio : pct) {
				if(pacoteServicoPorCondominio.getPacoteId() == condominioPacoteServico.getPacoteServico().getId()){
					pacoteServicoPorCondominio.setQuantidade(pacoteServicoPorCondominio.getQuantidade()+1);
				}
			}
			
		}
		
		List<PacoteServicoPorCondominio> pctRetorno = new ArrayList<>();
		for (PacoteServicoPorCondominio pacoteServicoPorCondominio : pct) {
			if(pacoteServicoPorCondominio.getQuantidade()!=0) {
				pctRetorno.add(pacoteServicoPorCondominio);
			}
		}
		
		
		List<OrdemServico> listOS = ordemServicoRepository.findAll();
		List<TiposDeServicoOS> listTiposDeServicoOS = new ArrayList<>(); 
		
		cont = 1;
		for (TipoServicoIdentificado tipoServico : TipoServicoIdentificado.values()) {
			listTiposDeServicoOS.add(new TiposDeServicoOS(tipoServico.getId(), tipoServico.getDescricao().replace("Serviço", "").trim(), 0, CoresRGB.COR.getPorNumero(cont)));
			cont++;
		}
		
		for (OrdemServico ordemServico : listOS) {
			for (TiposDeServicoOS tiposDeServicoOS : listTiposDeServicoOS) {
				if(ordemServico.getServicoIdentificado() == tiposDeServicoOS.getTipoServicoId()){
					tiposDeServicoOS.setQuantidade(tiposDeServicoOS.getQuantidade()+1);
				}
			}
		}	
		
		List<OrdemServicoPorNaturezaDoServico> listOrdemServicoPorNaturezaDoServico = ordemServicoRepository.porNatureza();
		
		cont = 1;
		for (OrdemServicoPorNaturezaDoServico ordemServicoPorNaturezaDoServico : listOrdemServicoPorNaturezaDoServico) {
			ordemServicoPorNaturezaDoServico.setCor(CoresRGB.COR.getPorNumero(cont));
			cont++;
		}
		
		mv.addObject("pacoteServico", pctRetorno);
		mv.addObject("tipoServico", listTiposDeServicoOS);
		mv.addObject("OsPorNatureza", listOrdemServicoPorNaturezaDoServico);
		mv.addObject("listRelatorioConsumoCotasOSDTO", listRelatorioConsumoCotasOSDTO);
		
		return mv;
	}

	
	
	@RequestMapping("/relatorio/condominio/{id}")
	public ModelAndView realtorioCondominio(@PathVariable("id") Integer id) {
		ModelAndView mv = new ModelAndView("/syndic/relatorio/condominio");
		
		Condominio condominio = condominioRepository.findById(id);
		
		//List<PessoaFisicaCondominio> listCondominio = new ArrayList<>();
		//listCondominio = condominioRepository.condominiosPorSindico(condominio.getPessoaJuridica().getId());
		
		
		int quantSindicos = pessoaFisicaCondominioRepository.countByCondominioIdAndTipoPessoaFisicaCondominioId(id,TipoPessoaFisicaCondominioEnum.SINDICO.getId());
		int quantAtendimentos = atendimentoRepository.countByCondominioIdOrderByTipoAtendimentoPrioridade(id);
		
		List<OrdemServico> listOs= ordemServicoRepository.ordemServicoEmAndamentoPorCondominio(id);
		
		
		List<Atendimento> listAtendimentoAberto = new ArrayList<>();
		List<OrdemServico> listOSMesCondominio = ordemServicoRepository.osPorCondominioPorMes(condominio.getId());
		
		List<OrdemServico> listOrdemServicoAvaliacao = ordemServicoRepository.osPorCondominioAguardandoAvaliacao(condominio.getId());
		
		for (Atendimento atendimento : condominio.getAtendimentos()) {
			if(atendimento.getUltimoStatusAtendimento().getStatusChamado().getId() == 1){
				listAtendimentoAberto.add(atendimento);
			}
		}		
		
		LocalDate hoje = LocalDate.now();
		
		String mesAnoAtual = MesesDoAno.MES.getPorNumero(hoje.getMonthValue())+"/"+hoje.getYear();
		
		String mesAnoAnterior = MesesDoAno.MES.getPorNumero(hoje.minusMonths(1).getMonthValue())+"/"+hoje.minusMonths(1).getYear();
		
		List<OrdemServico> listOsAtual = ordemServicoRepository.osPorCondominioPorMesAno(condominio.getId(),hoje.getMonthValue(),hoje.getYear());
		
		List<OrdemServico> listOsAnterior = ordemServicoRepository.osPorCondominioPorMesAno(condominio.getId(),hoje.minusMonths(1).getMonthValue(),hoje.minusMonths(1).getYear());
		
//		for (OrdemServico ordemServico : listOsAtual) {
//			System.err.println(">>>>>>>"+ordemServico.getHistoricoAberturaOrdemServico().getDataInicio());
//		}
//		
		
		mv.addObject("listOsAnterior", listOsAnterior);
		mv.addObject("listOsAtual", listOsAtual);
		mv.addObject("mesAnoAtual", mesAnoAtual); 
		mv.addObject("mesAnoAnterior",mesAnoAnterior); 
		mv.addObject("quantOs", listOs.size()); 
		mv.addObject("listOs",listOs);
		mv.addObject("condominio",condominio);
		mv.addObject("quantSindicos",quantSindicos);
		mv.addObject("quantUnidades",condominio.getUnidades().size());
		mv.addObject("quantMoradores",condominio.moradoresCondominio().size());
		mv.addObject("quantVeiculos", condominio.veiculosCondominio().size()); 
		mv.addObject("quantAnimais", condominio.animaisCondominio().size()); 
		mv.addObject("quantAtendimentos", quantAtendimentos);
		mv.addObject("listAtendimentoAberto", listAtendimentoAberto);
		mv.addObject("listOSMesCondominio", listOSMesCondominio);
		mv.addObject("listOrdemServicoAvaliacao", listOrdemServicoAvaliacao);
		
		
		return mv;
	}
	
	@RequestMapping("/relatoriomensal")
	public ModelAndView relatorioMensal(@AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/syndic/relatorio/osmensal");
		
		LocalDate hoje = LocalDate.now();
		
		int mesAtual = hoje.getMonthValue();
		int anoAtual = hoje.getYear();
		
		
		List<Condominio> listCondominio = new ArrayList<>();
		List<OrdemServico> listOrdemServico = new ArrayList<>();
		
		List<Integer> listIdCondominio = new ArrayList<>();
		
		//verificar se é sindico ou o gestor da empresa
		if(usuario.getPessoaFisica().isSindico()){
			Condominio condominio = condominioRepository.getOne(usuario.getPessoaFisica().getCondominioAtual().getId());
			listCondominio.add(condominio);
			List<PessoaFisicaCondominio> listCondominioDoSindico = new ArrayList<>();
			listCondominioDoSindico = condominioRepository.condominiosPorSindico(usuario.getPessoaFisica().getId());
//			int id = listCondominioDoSindico.get(0).getCondominio().getId();
			
			listIdCondominio.add(condominio.getId());
			
//			listOrdemServico =  ordemServicoRepository.findByCondominioIdOrderByEmergencia(id);
			listOrdemServico =  ordemServicoRepository.relatorioOSPorAnoMesCond(listIdCondominio,mesAtual, anoAtual);

		}else if (usuario.getPessoaFisica().isAdministradorEmpresa() && usuario.getPessoaJuridica().getId()!=1) {
			List<PessoaJuridicaCondominio> listPessoaJuridicaCondominio = pessoaJuridicaCondominioRepository.findByPessoaJuridicaId(usuario.getPessoaJuridica().getId());
			
			for (PessoaJuridicaCondominio pessoaJuridicaCondominio : listPessoaJuridicaCondominio) {
				listCondominio.add(pessoaJuridicaCondominio.getCondominio());
				listIdCondominio.add(pessoaJuridicaCondominio.getCondominio().getId());
			}
			
//			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
			listOrdemServico =  ordemServicoRepository.relatorioOSPorAnoMesCond(listIdCondominio,mesAtual, anoAtual);
			
		}else{
			listCondominio = condominioRepository.findByAtivo(1);
			
			for (Condominio listaCondominio : listCondominio) {
				listIdCondominio.add(listaCondominio.getId());
			}
			
//			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
			listOrdemServico =  ordemServicoRepository.relatorioOSPorAnoMesCond(listIdCondominio,mesAtual, anoAtual);
		}
		
		
		for (OrdemServico os : listOrdemServico) {
			os.getCondominio().getPessoaJuridica().setNomeFantasia(SyndicController.deAccent(os.getCondominio().getPessoaJuridica().getNomeFantasia()).toUpperCase());
			
			os.getTipoOrdemServico().setNome(SyndicController.deAccent(os.getTipoOrdemServico().getNome()).toUpperCase());
			
			os.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().setNome(SyndicController.deAccent(os.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().getNome()).toUpperCase());
			
			os.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().setNome(SyndicController.deAccent(os.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome()).toUpperCase());
			
			
			for (PessoaFisica pf : os.getResponsaveisExecucaoOS()) {
				pf.setNome(SyndicController.deAccent(pf.getNome()).toUpperCase());
			}
		}
		
		
//		LISTA ESTATISTICA
		List<RelatorioConsumoCotasOSDTO> listRelatorioConsumoCotasOSDTO =  new ArrayList<RelatorioConsumoCotasOSDTO>();
		List<CondominioPacoteServico> listCondominioPacoteServico = condominioPacoteServicoRepository.findByAtivo(1);
		
//		List<OrdemServico> ospormes = ordemServicoRepository.osPorMesAno(LocalDate.now().getMonthValue(), LocalDate.now().getYear());
 		
		
		for (CondominioPacoteServico condominioPacoteServico : listCondominioPacoteServico) { 
			
			RelatorioConsumoCotasOSDTO relatorio = new RelatorioConsumoCotasOSDTO();
				relatorio.setCodigo(condominioPacoteServico.getCondominio().getId());
				relatorio.setNome(condominioPacoteServico.getCondominio().getPessoaJuridica().getNomeFantasia());
				relatorio.setNomePacoteServico(condominioPacoteServico.getPacoteServico().getNome());
				
				relatorio.setQuantidadeVisitas(condominioPacoteServico.getPacoteServico().getQtdVisitas());
				
				relatorio.setValorMaodeObra(BigDecimal.ZERO);
				relatorio.setValorMaterial(BigDecimal.ZERO);
				
				relatorio.setMes(LocalDate.now().getMonthValue());
				relatorio.setAno(LocalDate.now().getYear());
				
				for (OrdemServico os : listOrdemServico) {
					
					if(os.getCondominio().getId()==condominioPacoteServico.getCondominio().getId()) {
						relatorio.setOsAberta(relatorio.getOsAberta()+1);
						
						relatorio.setValorMaodeObra(relatorio.getValorMaodeObra().add(os.getValorMaoDeObra()==null?BigDecimal.ZERO:os.getValorMaoDeObra()));
						relatorio.setValorMaterial(relatorio.getValorMaterial().add(os.getValorMaterial()==null?BigDecimal.ZERO:os.getValorMaterial()));
						
					}
				}
				
				listRelatorioConsumoCotasOSDTO.add(relatorio);
		}
//		LISTA ESTATISTICA		
		
		
		
//		GERAÇÃO DO GRÁFICO 1
		List<TiposDeServicoOS> listTiposDeServicoOS = new ArrayList<>(); 
		
		int cont = 1;
		
		
		for (Condominio cond : listCondominio) {
			listTiposDeServicoOS.add(new TiposDeServicoOS(cond.getId(), cond.getPessoaJuridica().getNomeFantasia().replace("Condominio", "").trim(), 0, CoresRGB.COR.getPorNumero(cont)));
			cont++;
		}
		
		
		for (OrdemServico ordem : listOrdemServico) {
			
			for (TiposDeServicoOS tiposDeServicoOS : listTiposDeServicoOS) {
				
				if(ordem.getCondominio().getId() == tiposDeServicoOS.getTipoServicoId()) {
					tiposDeServicoOS.setQuantidade(tiposDeServicoOS.getQuantidade()+1);
				}
			}
			
		}
		
//		GERAÇÃO DO GRÁFICO 1
		
		
//		GERAÇÃO DO GRÁFICO 2
		
//		List<OrdemServicoPorNaturezaDoServico> listOrdemServicoPorNaturezaDoServico = ordemServicoRepository.porNatureza();
		
//		List<OrdemServicoPorNaturezaDoServico> listOrdemServicoPorNaturezaDoServico = ordemServicoRepository.porNaturezaMesAnoCond(listIdCondominio, mesAtual, anoAtual );
//		
//		cont = 1;
//		for (OrdemServicoPorNaturezaDoServico ordemServicoPorNaturezaDoServico : listOrdemServicoPorNaturezaDoServico) {
//			ordemServicoPorNaturezaDoServico.setCor(CoresRGB.COR.getPorNumero(cont));
//			cont++;
//		}
		
		
		
		
		
		
		
		
		
		List<String> listaNomes = new ArrayList<String>();
		List<Integer> listaQtde = new ArrayList<Integer>();
//		List<Integer> listaCores = new ArrayList<Integer>();

//		cont = 0;
//		for(OrdemServico listaor : listOrdemServico) {
//			
//			if(!listaNomes.contains(listaor.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome())) {
//				listaNomes.add(listaor.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome());
////				listaCores.add(cont);
//				cont++;
//			}
//			
//		}
		
			
		int contOrdem;
		int status = 0;
		
		for(OrdemServicoStatus ordStatus : ordemServicoStatusRepository.findAll()) {
			
			contOrdem = 0;	
			for(OrdemServico listass : listOrdemServico) {
				
				
					if(listass.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getId() == ordStatus.getId()) {
						contOrdem++;
						status = ordStatus.getId();
						
						System.err.println("idididid: "+listass.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getId()+""+
								listass.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome());
						
						System.err.println("status: "+status+" - "+ordStatus.getId()+" - "+ordStatus.getNome());
						
						if(!listaNomes.contains(listass.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome())) {
							listaNomes.add(listass.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome());
//							listaCores.add(cont);
							
						}
						
					}
				
			}
			
			if(status==ordStatus.getId()) {
				System.err.println("status11: "+status+" - "+ordStatus.getId()+" - "+ordStatus.getNome()+" - "+contOrdem);
				listaQtde.add(contOrdem);
			}
			
			
		}
		
		List<OrdemServicoPorNaturezaDoServico> listOrdemServicoPorNaturezaDoServico = new ArrayList<OrdemServicoPorNaturezaDoServico>();
		OrdemServicoPorNaturezaDoServico ordemServicoPorNaturezaDoServico = new OrdemServicoPorNaturezaDoServico();
		
		int rot;
		for (rot = 0; rot < listaNomes.size(); rot++ ) {
			
			System.err.println("status12: "+listaNomes.get(rot).toString()+" - "+listaQtde.get(rot));
			ordemServicoPorNaturezaDoServico = new OrdemServicoPorNaturezaDoServico(listaNomes.get(rot).toString(), listaQtde.get(rot));
			
//			ordemServicoPorNaturezaDoServico.setNome(listaNomes.get(rot).toString());
//			ordemServicoPorNaturezaDoServico.setQuantidade(listaQtde.get(rot));
			ordemServicoPorNaturezaDoServico.setCor(CoresRGB.COR.getPorNumero(rot+1));
			
			listOrdemServicoPorNaturezaDoServico.add(ordemServicoPorNaturezaDoServico);
		}
		
		
		
		
		
		
//		GERAÇÃO DO GRÁFICO 2
		
		mv.addObject("listOrdemServico", listOrdemServico);
		
		mv.addObject("listCondominio", listCondominio);
		mv.addObject("listaMesesDoAno", MesesDoAno.values());
		
		mv.addObject("mesSelecionado", mesAtual);
		mv.addObject("mesNome", MesesDoAno.MES.getPorNumeroDesc(mesAtual));
		mv.addObject("anoSelecionado", anoAtual);
		
		mv.addObject("condSelecionado", 0);
		
		mv.addObject("tipoServico", listTiposDeServicoOS);
		mv.addObject("OsPorNatureza", listOrdemServicoPorNaturezaDoServico);
		
		mv.addObject("listRelatorioConsumoCotasOSDTO", listRelatorioConsumoCotasOSDTO);
		
		
		return mv;
	}
	
	@PostMapping("/relatoriomensal")
	public ModelAndView filtraRelatorio(@RequestParam(name="condominio") String idCondominio, @RequestParam(name="ano") String ano,
			@RequestParam(name="mes") String mes, @AuthenticationPrincipal UsuarioSistema usuario) { 
		
		ModelAndView mv = new ModelAndView("/syndic/relatorio/osmensal");
		int intAno = 0;
		int intMes = 0;
		int intCondominio = 0;
		
		if(idCondominio != ""){
			intCondominio = Integer.parseInt(idCondominio);			
		}
		
		if(ano != ""){
			intAno =  Integer.parseInt(ano);
		}
		
		if(mes != ""){
			intMes =  Integer.parseInt(mes);
		}
		
		
		List<Condominio> listCondominio = new ArrayList<>();
		List<OrdemServico> listOrdemServico = new ArrayList<>();
		
		List<Integer> listIdCondominio = new ArrayList<>();
		
		//verificar se é sindico ou o gestor da empresa
		if(usuario.getPessoaFisica().isSindico()){
			Condominio condominio = condominioRepository.getOne(usuario.getPessoaFisica().getCondominioAtual().getId());
			listCondominio.add(condominio);
			List<PessoaFisicaCondominio> listCondominioDoSindico = new ArrayList<>();
			listCondominioDoSindico = condominioRepository.condominiosPorSindico(usuario.getPessoaFisica().getId());
			int id = listCondominioDoSindico.get(0).getCondominio().getId();
			
			listIdCondominio.add(intCondominio);
			
			listOrdemServico =  ordemServicoRepository.relatorioOSPorAnoMesCond(listIdCondominio, intMes, intAno );
//			listOrdemServico =  ordemServicoRepository.findByCondominioIdOrderByEmergencia(id);

		}else if (usuario.getPessoaFisica().isAdministradorEmpresa() && usuario.getPessoaJuridica().getId()!=1) {
			List<PessoaJuridicaCondominio> listPessoaJuridicaCondominio = pessoaJuridicaCondominioRepository.findByPessoaJuridicaId(usuario.getPessoaJuridica().getId());
			
			for (PessoaJuridicaCondominio pessoaJuridicaCondominio : listPessoaJuridicaCondominio) {
				listCondominio.add(pessoaJuridicaCondominio.getCondominio());
				if(intCondominio==0) {
					listIdCondominio.add(pessoaJuridicaCondominio.getCondominio().getId());
				}else {
					listIdCondominio.add(intCondominio);
				}
				
			}
			
			listOrdemServico =  ordemServicoRepository.relatorioOSPorAnoMesCond(listIdCondominio, intMes, intAno );
			
//			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
		}else{
			listCondominio = condominioRepository.findByAtivo(1);
			if(intCondominio==0) {
				for (Condominio listaCondominio : listCondominio) {
					listIdCondominio.add(listaCondominio.getId());
				}
			}else {
				listIdCondominio.add(intCondominio);
			}
			
//			listOrdemServico =  ordemServicoRepository.findByOrderByEmergenciaDesc();
			listOrdemServico =  ordemServicoRepository.relatorioOSPorAnoMesCond(listIdCondominio, intMes, intAno );
			
		}
		
//		LISTA ESTATISTICA
		List<RelatorioConsumoCotasOSDTO> listRelatorioConsumoCotasOSDTO =  new ArrayList<RelatorioConsumoCotasOSDTO>();
		
		List<CondominioPacoteServico> listCondominioPacoteServico = new ArrayList<CondominioPacoteServico>();
		
		if(intCondominio==0) {
			listCondominioPacoteServico = condominioPacoteServicoRepository.findByAtivo(1);
		}else {
			listCondominioPacoteServico = condominioPacoteServicoRepository.findByCondominioIdAndAtivo(intCondominio, 1);
		}
//		List<OrdemServico> ospormes = ordemServicoRepository.osPorMesAno(intMes, intAno);
		
		LocalDate hoje = LocalDate.now();
		
		int anoAtual = hoje.getYear();
		
		int ia;
		if(intAno == 0 && intMes == 0) {
			for(ia = 2019; ia <= anoAtual; ia++) {
				int i;
				for(i = 1; i <=12; i++) {
					
//					listOrdemServico =  ordemServicoRepository.relatorioOSPorAnoMesCond(listIdCondominio, i, ia );
					
					
					for (CondominioPacoteServico condominioPacoteServico : listCondominioPacoteServico) { 
						
						RelatorioConsumoCotasOSDTO relatorio = new RelatorioConsumoCotasOSDTO();
							relatorio.setCodigo(condominioPacoteServico.getCondominio().getId());
							relatorio.setNome(condominioPacoteServico.getCondominio().getPessoaJuridica().getNomeFantasia());
							relatorio.setNomePacoteServico(condominioPacoteServico.getPacoteServico().getNome());
							
							relatorio.setQuantidadeVisitas(condominioPacoteServico.getPacoteServico().getQtdVisitas());
							
							relatorio.setValorMaodeObra(BigDecimal.ZERO);
							relatorio.setValorMaterial(BigDecimal.ZERO);
							
							relatorio.setMes(i);
							relatorio.setAno(ia);
							
							for (OrdemServico os : listOrdemServico) {
								if(os.getHistoricoAberturaOrdemServico().getDataInicio().getMonth()+1 == i) {
									if(os.getCondominio().getId()==condominioPacoteServico.getCondominio().getId()) {
										
										relatorio.setOsAberta(relatorio.getOsAberta()+1);
										
										relatorio.setValorMaodeObra(relatorio.getValorMaodeObra().add(os.getValorMaoDeObra()==null?BigDecimal.ZERO:os.getValorMaoDeObra()));
										relatorio.setValorMaterial(relatorio.getValorMaterial().add(os.getValorMaterial()==null?BigDecimal.ZERO:os.getValorMaterial()));
										
									}
								}
								
								
							}
							
							listRelatorioConsumoCotasOSDTO.add(relatorio);
					}
				}
			}
			
		}else
		
		if(intAno != 0 && intMes == 0) {
			int i;
			for(i = 1; i <=12; i++) {
				
//				listOrdemServico =  ordemServicoRepository.relatorioOSPorAnoMesCond(listIdCondominio, i, intAno );
				
				for (CondominioPacoteServico condominioPacoteServico : listCondominioPacoteServico) { 
					
					RelatorioConsumoCotasOSDTO relatorio = new RelatorioConsumoCotasOSDTO();
						relatorio.setCodigo(condominioPacoteServico.getCondominio().getId());
						relatorio.setNome(condominioPacoteServico.getCondominio().getPessoaJuridica().getNomeFantasia());
						relatorio.setNomePacoteServico(condominioPacoteServico.getPacoteServico().getNome());
						
						relatorio.setQuantidadeVisitas(condominioPacoteServico.getPacoteServico().getQtdVisitas());
						
						relatorio.setValorMaodeObra(BigDecimal.ZERO);
						relatorio.setValorMaterial(BigDecimal.ZERO);
						
						relatorio.setMes(i);
						relatorio.setAno(intAno);
						
						for (OrdemServico os : listOrdemServico) {
							if(os.getHistoricoAberturaOrdemServico().getDataInicio().getMonth()+1 == i) {
								if(os.getCondominio().getId()==condominioPacoteServico.getCondominio().getId()) {
									
									relatorio.setOsAberta(relatorio.getOsAberta()+1);
									
									relatorio.setValorMaodeObra(relatorio.getValorMaodeObra().add(os.getValorMaoDeObra()==null?BigDecimal.ZERO:os.getValorMaoDeObra()));
									relatorio.setValorMaterial(relatorio.getValorMaterial().add(os.getValorMaterial()==null?BigDecimal.ZERO:os.getValorMaterial()));
									
								}
							}
							
							
						}
						
						listRelatorioConsumoCotasOSDTO.add(relatorio);
				}
			}
		}else if(intAno == 0 && intMes != 0) {
			
			
			for(ia = 2019; ia <= anoAtual; ia++) {
									
//					listOrdemServico =  ordemServicoRepository.relatorioOSPorAnoMesCond(listIdCondominio, intMes, ia );
					
					
					for (CondominioPacoteServico condominioPacoteServico : listCondominioPacoteServico) { 
						
						RelatorioConsumoCotasOSDTO relatorio = new RelatorioConsumoCotasOSDTO();
							relatorio.setCodigo(condominioPacoteServico.getCondominio().getId());
							relatorio.setNome(condominioPacoteServico.getCondominio().getPessoaJuridica().getNomeFantasia());
							relatorio.setNomePacoteServico(condominioPacoteServico.getPacoteServico().getNome());
							
							relatorio.setQuantidadeVisitas(condominioPacoteServico.getPacoteServico().getQtdVisitas());
							
							relatorio.setValorMaodeObra(BigDecimal.ZERO);
							relatorio.setValorMaterial(BigDecimal.ZERO);
							
							relatorio.setMes(intMes);
							relatorio.setAno(ia);
							
							for (OrdemServico os : listOrdemServico) {
									if(os.getCondominio().getId()==condominioPacoteServico.getCondominio().getId()) {
										
										relatorio.setOsAberta(relatorio.getOsAberta()+1);
										
										relatorio.setValorMaodeObra(relatorio.getValorMaodeObra().add(os.getValorMaoDeObra()==null?BigDecimal.ZERO:os.getValorMaoDeObra()));
										relatorio.setValorMaterial(relatorio.getValorMaterial().add(os.getValorMaterial()==null?BigDecimal.ZERO:os.getValorMaterial()));
										
									}
								
							}
							
							listRelatorioConsumoCotasOSDTO.add(relatorio);
					}
				
			}
			
			
		}else {
			for (CondominioPacoteServico condominioPacoteServico : listCondominioPacoteServico) { 
				
				RelatorioConsumoCotasOSDTO relatorio = new RelatorioConsumoCotasOSDTO();
					relatorio.setCodigo(condominioPacoteServico.getCondominio().getId());
					relatorio.setNome(condominioPacoteServico.getCondominio().getPessoaJuridica().getNomeFantasia());
					relatorio.setNomePacoteServico(condominioPacoteServico.getPacoteServico().getNome());
					
					relatorio.setQuantidadeVisitas(condominioPacoteServico.getPacoteServico().getQtdVisitas());
					
					relatorio.setValorMaodeObra(BigDecimal.ZERO);
					relatorio.setValorMaterial(BigDecimal.ZERO);
					
					relatorio.setMes(intMes);
					relatorio.setAno(intAno);
					
					for (OrdemServico os : listOrdemServico) {
							if(os.getCondominio().getId()==condominioPacoteServico.getCondominio().getId()) {
								
								relatorio.setOsAberta(relatorio.getOsAberta()+1);
								
								relatorio.setValorMaodeObra(relatorio.getValorMaodeObra().add(os.getValorMaoDeObra()==null?BigDecimal.ZERO:os.getValorMaoDeObra()));
								relatorio.setValorMaterial(relatorio.getValorMaterial().add(os.getValorMaterial()==null?BigDecimal.ZERO:os.getValorMaterial()));
								
							}
						
					}
					
					listRelatorioConsumoCotasOSDTO.add(relatorio);
			}
		}
		
		
//		for (OrdemServico relatorio : listOrdemServico) {
//			System.err.println("relatorio1 "+relatorio.getId());
//			System.err.println("relatorio1 "+relatorio.getNumeroProtocolo());
//		}
		
		
//		LISTA ESTATISTICA	
		
		
		for (OrdemServico os : listOrdemServico) {
			
			
			os.getCondominio().getPessoaJuridica().setNomeFantasia(SyndicController.deAccent(os.getCondominio().getPessoaJuridica().getNomeFantasia()).toUpperCase());
			
			os.getTipoOrdemServico().setNome(SyndicController.deAccent(os.getTipoOrdemServico().getNome()).toUpperCase());
			
			os.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().setNome(SyndicController.deAccent(os.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().getNome()).toUpperCase());
			
			os.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().setNome(SyndicController.deAccent(os.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome()).toUpperCase());
			
			for (PessoaFisica pf : os.getResponsaveisExecucaoOS()) {
				pf.setNome(SyndicController.deAccent(pf.getNome()).toUpperCase());
			}
		}
		
		
//		GERAÇÃO DO GRÁFICO 1
		List<TiposDeServicoOS> listTiposDeServicoOS = new ArrayList<>(); 
		
		int cont = 1;
		
		
		for (Condominio cond : listCondominio) {
			listTiposDeServicoOS.add(new TiposDeServicoOS(cond.getId(), cond.getPessoaJuridica().getNomeFantasia().replace("Condominio", "").trim(), 0, CoresRGB.COR.getPorNumero(cont)));
			cont++;
		}
		
		
		for (OrdemServico ordem : listOrdemServico) {
			
			for (TiposDeServicoOS tiposDeServicoOS : listTiposDeServicoOS) {
				
				if(ordem.getCondominio().getId() == tiposDeServicoOS.getTipoServicoId()) {
					tiposDeServicoOS.setQuantidade(tiposDeServicoOS.getQuantidade()+1);
				}
			}
			
		}
		
//		GERAÇÃO DO GRÁFICO 1
		
		
//		GERAÇÃO DO GRÁFICO 2
		
//		List<OrdemServicoPorNaturezaDoServico> listOrdemServicoPorNaturezaDoServico = ordemServicoRepository.porNatureza();
		
//		List<OrdemServicoPorNaturezaDoServico> listOrdemServicoPorNaturezaDoServico = new ArrayList<OrdemServicoPorNaturezaDoServico>();
		
//		if(intMes==0) {
//			listOrdemServicoPorNaturezaDoServico = ordemServicoRepository.porNatureza();
//		}else {
//			listOrdemServicoPorNaturezaDoServico = ordemServicoRepository.porNaturezaMesAnoCond(listIdCondominio, intMes, intAno );
//		}
//		
//		
//		cont = 1;
//		for (OrdemServicoPorNaturezaDoServico ordemServicoPorNaturezaDoServico : listOrdemServicoPorNaturezaDoServico) {
//			ordemServicoPorNaturezaDoServico.setCor(CoresRGB.COR.getPorNumero(cont));
//			cont++;
//		}
		
		
		
		
		//Gerando de forma diferente o grafico 2
		List<String> listaNomes = new ArrayList<String>();
		List<Integer> listaQtde = new ArrayList<Integer>();
//		List<Integer> listaCores = new ArrayList<Integer>();

//		cont = 1;
//		for(OrdemServico listaor : listOrdemServico) {
//			
//			if(!listaNomes.contains(listaor.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome())) {
//				listaNomes.add(listaor.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome());
////				listaCores.add(cont);
//				cont++;
//			}
//			
//		}
		
			
		int contOrdem;
		int status = 0;
		
		for(OrdemServicoStatus ordStatus : ordemServicoStatusRepository.findAll()) {
			
			contOrdem = 0;	
			for(OrdemServico listass : listOrdemServico) {
				
				
					if(listass.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getId() == ordStatus.getId()) {
						contOrdem++;
						status = ordStatus.getId();
						
						if(!listaNomes.contains(listass.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome())) {
							listaNomes.add(listass.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome());
//							listaCores.add(cont);
							
						}
					}
				
			}
			
			if(status==ordStatus.getId()) {
				listaQtde.add(contOrdem);
			}
			
			
		}
		
		List<OrdemServicoPorNaturezaDoServico> listOrdemServicoPorNaturezaDoServico = new ArrayList<OrdemServicoPorNaturezaDoServico>();
		OrdemServicoPorNaturezaDoServico ordemServicoPorNaturezaDoServico = new OrdemServicoPorNaturezaDoServico();
		
		int rot;
		for (rot = 0; rot < listaNomes.size(); rot++ ) {
			
			ordemServicoPorNaturezaDoServico = new OrdemServicoPorNaturezaDoServico(listaNomes.get(rot).toString(), listaQtde.get(rot));
			
//			ordemServicoPorNaturezaDoServico.setNome(listaNomes.get(rot).toString());
//			ordemServicoPorNaturezaDoServico.setQuantidade(listaQtde.get(rot));
			ordemServicoPorNaturezaDoServico.setCor(CoresRGB.COR.getPorNumero(rot+1));
			
			listOrdemServicoPorNaturezaDoServico.add(ordemServicoPorNaturezaDoServico);
		}
		
		
//		GERAÇÃO DO GRÁFICO 2
		
//		for (OrdemServico relatorio : listOrdemServico) {
//			System.err.println("relatorio2 "+relatorio.getId());
//			System.err.println("relatorio2 "+relatorio.getNumeroProtocolo());
//		}

		
		
		mv.addObject("listOrdemServico", listOrdemServico);
		
		mv.addObject("listCondominio", listCondominio);
		mv.addObject("listaMesesDoAno", MesesDoAno.values());
		
		mv.addObject("mesSelecionado", intMes);
		mv.addObject("anoSelecionado", intAno);
		mv.addObject("condSelecionado", intCondominio);
		if(intMes == 0) {
			mv.addObject("mesNome", "Geral");
		}else {
			mv.addObject("mesNome", MesesDoAno.MES.getPorNumeroDesc(intMes));
		}
		
		
		mv.addObject("tipoServico", listTiposDeServicoOS);
		mv.addObject("OsPorNatureza", listOrdemServicoPorNaturezaDoServico);
		
		mv.addObject("listRelatorioConsumoCotasOSDTO", listRelatorioConsumoCotasOSDTO);
		
//		mv.addObject("listaNomes", listaNomes);
//		mv.addObject("listaQtde", listaQtde);
//		mv.addObject("listaCores", listaCores);
		
		return mv;
	}
	
}