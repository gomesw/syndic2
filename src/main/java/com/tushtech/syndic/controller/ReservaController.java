package com.tushtech.syndic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tushtech.syndic.entity.AreaComum;
import com.tushtech.syndic.entity.Reserva;
import com.tushtech.syndic.repository.AreaComumRepository;
import com.tushtech.syndic.repository.ReservaRepository;

/**
 * @author Paulo Goncalves
 *
 */
@Controller
@RequestMapping("/reserva")

public class ReservaController {


	
	@Autowired
	private ReservaRepository reservaRepository;
	
	@Autowired
	private AreaComumRepository areaComumRepository;
	
	
	@RequestMapping("")
	public ModelAndView listaReservas() {

		ModelAndView mv = new ModelAndView("layout/reservas/listarreservas");
		List<Reserva> listResevas = reservaRepository.findAll();	
		mv.addObject("reservas",listResevas);
		
		return mv;
	}
	
	@RequestMapping("/{id}")
	public ModelAndView listarReservas(@PathVariable Integer id,Reserva reserva) 
	{
		ModelAndView mv = new ModelAndView("/layout/reservas/listarreservas");
		if(id!=null){
			reserva = reservaRepository.findOne(id);
		}
		mv.addObject("reservas",reserva);
		return mv;
	}
	
	
	@RequestMapping("/incluir")
	public ModelAndView novaReserva(Reserva reserva) {
		ModelAndView mv = new ModelAndView("layout/reservas/incluir");
		
		List<AreaComum> local = areaComumRepository.findAll();
		
		mv.addObject("reserva",reserva);
		mv.addObject("local",local);
		return mv;
	}
	
	
//	@RequestMapping(value = {"/incluir", "/incluir/{\\d+}"}, method= RequestMethod.POST)
//	public ModelAndView salvarRecurso(@Valid Recurso recurso, BindingResult result, RedirectAttributes attributes) {
//		
//		if(result.hasErrors()){
//			return novoRecurso(recurso);
//		}
//		
//		//verificar se o recurso ja existe no banco
//		Recurso recursoBanco = (!recurso.isNovo()?null:recursoRepository.findByNomeAndAtivo(recurso.getNome(),1));
//		
//		if(recursoBanco == null || !recurso.isNovo()){
//			recursoService.salvarRecurso(recurso);			
//			attributes.addFlashAttribute("mensagem"," Realizado com sucesso!");
//			return new ModelAndView("redirect:/recurso");
//		}else{			
//			attributes.addFlashAttribute("mensagem"," Já existe um recurso cadastrado com o mesmo nome!");
//			return new ModelAndView("redirect:/recurso");
//		}		
//		
//	}
//	
//	@RequestMapping("/editar/{id}")
//	public ModelAndView visualizarRecurso(@PathVariable Integer id,Recurso recurso) 
//	{
//		ModelAndView mv = new ModelAndView("/layout/sistema/recurso/incluirrecurso");
//		if(id!=null){
//			recurso = recursoRepository.findOne(id);
//		}
//		mv.addObject("recurso",recurso);
//		return mv;
//	}
//
//	
//	
//	@RequestMapping("/excluir/{id}")
//	public @ResponseBody Recurso  desativarRecurso(Recurso recurso) 
//	{
//		recursoRepository.delete(recurso);
//		return recurso;
//	}
	
	
}



