package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.dto.AlterarSenhaDTO;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.Senhas;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.repository.SenhasRepository;
import com.tushtech.syndic.service.SenhaService;

@Controller
@RequestMapping("/senha") 
public class SenhaController {
	
	
	
	@Autowired
	private SenhaService senhaService;
	
	@Autowired
	private SenhasRepository senhasRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;
	
	
	
//	private Webcam webcam = Webcam.getDefault();
//    private WebcamPanel webcamPanel = new WebcamPanel(webcam, false);
	
	
	
	@RequestMapping("/alterarsenha")
	public ModelAndView formularioAlterarSenha(AlterarSenhaDTO alterarSenhaDTO,@AuthenticationPrincipal UsuarioSistema usuarioSistema) {
		
		
		ModelAndView mv = new ModelAndView("/syndic/senha/alterarsenha");
		
		
		alterarSenhaDTO.setCondominio(usuarioSistema.getPessoaFisica().getCondominioAtual());
		
		mv.addObject("alterarSenhaDTO", alterarSenhaDTO);
		return mv;
		
	}
	
	
	
	
	@PostMapping("/alterar") 
	public ModelAndView alterarSenha(@Valid AlterarSenhaDTO alterarSenhaDTO, BindingResult result, 
			RedirectAttributes attributes,
			@AuthenticationPrincipal UsuarioSistema usuario) {
		
		
		senhaService.atribuirErros(alterarSenhaDTO,usuario,result);
		
		
		if(result.hasErrors()){
			return formularioAlterarSenha(alterarSenhaDTO,usuario);
		}
		
		List<Senhas> senhas  = senhasRepository.findByPessoaFisicaId(usuario.getPessoaFisica().getId());
		
		senhas.get(0).setSenha(passwordEncoder.encode(alterarSenhaDTO.getRegisterPassword()));
		
		
		senhaService.salvar(senhas.get(0));
		
		PessoaFisica pf = pessoaFisicaRepository.findOne(usuario.getPessoaFisica().getId());
		
		
		
		
		senhaService.enviarEmailComAlteracaoSenha(pf, alterarSenhaDTO.getRegisterPassword());
		
		attributes.addFlashAttribute("mensagem"," Senha alterada com sucesso!");		
		return new ModelAndView("redirect:/senha/alterarsenha");
		
		
	}
	
	
	
	

}
