
package com.tushtech.syndic.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tushtech.syndic.entity.Atendimento;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.HistoricoPortaria;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.AtendimentoRepository;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.HistoricoPortariaRepository;
import com.tushtech.syndic.repository.PessoaFisicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaUnidadeRepository;
import com.tushtech.syndic.repository.TipoAtendimentoRepository;
import com.tushtech.syndic.repository.UnidadeRepository;

@Controller
@RequestMapping("/sindico")
public class SindicoController {

	@Autowired
	private PessoaFisicaCondominioRepository pessoaFisicaCondominioRepository;

	
	@Autowired
	private UnidadeRepository unidadeRepository;
	
	
	@Autowired
	private HistoricoPortariaRepository historicoPortariaRepository;
	
	@Autowired
	private PessoaFisicaUnidadeRepository pessoaFisicaUnidadeRepository;
	
	@Autowired
	private CondominioRepository condominioRepository;

	@Autowired
	private AtendimentoRepository atendimentoRepository;
	
	@Autowired
	private TipoAtendimentoRepository tipoAtendimentoRepository;

	@RequestMapping("/condominio") 
	public ModelAndView listaCondominios(@AuthenticationPrincipal UsuarioSistema usuario) {

		ModelAndView mv = new ModelAndView("/syndic/condominios/listarcondominioporsindico");
		
		List<PessoaFisicaCondominio> listPessoaFisicaCondominio = condominioRepository.condominiosPorSindico(usuario.getPessoaFisica().getId());
		
		if(listPessoaFisicaCondominio.size()==1) {
			return new ModelAndView("redirect:/sindico/condominio/editar/"+listPessoaFisicaCondominio.get(0).getCondominio().getId()); 
			
		}
		
		mv.addObject("listPessoaFisicaCondominio", listPessoaFisicaCondominio); 
		mv.addObject("usuario", usuario.getPessoaFisica()); 

		return mv;
	}
	
	
	@RequestMapping("/portaria/{id}")  
	public ModelAndView telaPortaria(@PathVariable Integer id, @AuthenticationPrincipal UsuarioSistema usuario) {

		List<PessoaFisicaUnidade> listPessoaFisicaUnidade = new ArrayList<>();
		
		Condominio condominio = condominioRepository.findOne(id);
		
		List<HistoricoPortaria> listHistoricoPortaria = historicoPortariaRepository.findByIdNot(0); 
		
		//findByLastnameNot
		
		//findByAgeOrderByLastnameDesc
		
		listPessoaFisicaUnidade = pessoaFisicaUnidadeRepository.proprietariosMoradoresInquilinosFuncionariosUnidade(id);
		
		HistoricoPortaria historicoPortaria = new HistoricoPortaria();
		
		historicoPortaria.setCadastrante(usuario.getPessoaFisica());
		historicoPortaria.setCondominio(condominio);
		
		ModelAndView mv = new ModelAndView("/syndic/portaria/telaporteiro");

		mv.addObject("historicoPortaria",historicoPortaria);
		mv.addObject("listPessoaFisicaUnidade",listPessoaFisicaUnidade);
		mv.addObject("listHistoricoPortaria",listHistoricoPortaria);
		
		return mv;
	}
	
	
	@RequestMapping("/atendimento/listar/{id}")
	public ModelAndView nova(@PathVariable Integer id) {
		ModelAndView mv = new ModelAndView("/syndic/unidades/atendimento/listar");
		
		List<Atendimento> listAtendimento = atendimentoRepository.findByCondominioId(id);
		List<Atendimento> listAtendimentoAberto = new ArrayList<>();
		
		for (Atendimento atendimento : listAtendimento) {
			if(atendimento.getUltimoStatusAtendimento().getStatusChamado().getId() == 1){
				listAtendimentoAberto.add(atendimento);
			}
		}
		
		mv.addObject("listAtendimento", listAtendimentoAberto);

		return mv;
	}
}	



