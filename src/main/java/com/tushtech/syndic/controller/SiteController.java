package com.tushtech.syndic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/site")
public class SiteController {

	@RequestMapping
	public ModelAndView getPrincipal() {
		ModelAndView mv = new ModelAndView("/syndic/syndic");
		return mv;
	}
	
}
