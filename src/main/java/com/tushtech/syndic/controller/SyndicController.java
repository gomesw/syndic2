package com.tushtech.syndic.controller;



import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.dto.EsqueciSenhaDTO;
import com.tushtech.syndic.entity.Atendimento;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaJuridicaCondominio;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaCondominioEnum;
import com.tushtech.syndic.repository.AtendimentoRepository;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.EnderecoUnidadeRepository;
import com.tushtech.syndic.repository.OrdemServicoRepository;
import com.tushtech.syndic.repository.PessoaFisicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.repository.PessoaJuridicaCondominioRepository;
import com.tushtech.syndic.repository.TipoEnderecoUnidadeRepository;
import com.tushtech.syndic.repository.UnidadeRepository;
import com.tushtech.syndic.service.PessoaFisicaService;

@Controller
@RequestMapping("/")
public class SyndicController {
	 
//	
//	@Autowired
//	private FotoStorageLocal fotoStorageLocal;

	@Autowired
	private CondominioRepository condominioRepository;	
	
	@Autowired
	private PessoaFisicaService pessoaFisicaService;

	@Autowired
	private PessoaFisicaCondominioRepository pessoaFisicaCondominioRepository;
	
	@Autowired
	private AtendimentoRepository atendimentoRepository;
	
	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;
	
	@Autowired
	private TipoEnderecoUnidadeRepository tipoEnderecoUnidadeRepository;	

	@Autowired
	private EnderecoUnidadeRepository enderecoUnidadeRepository;

	@Autowired
	private UnidadeRepository unidadeRepository;
	
	@Autowired
	private OrdemServicoRepository ordemServicoRepository;
	
	@Autowired
	private PessoaJuridicaCondominioRepository pessoaJuridicaCondominioRepository;
	
	@RequestMapping
	public ModelAndView inicio(@AuthenticationPrincipal UsuarioSistema usuario) { 
		
		
		
		List<PessoaFisicaCondominio> listCondominio = new ArrayList<>();

		int quantCondominios = 0;

		
		ModelAndView mv = new ModelAndView("/syndic/index");  
		
		if(usuario.getPessoaFisica().isAdministradorSistema()) {//Administrador do sistema
			quantCondominios = condominioRepository.findByAtivo(1).size();
			
		}

		if(usuario.getPessoaFisica().isSindico()) {//sindico
			listCondominio = condominioRepository.condominiosPorSindico(usuario.getPessoaFisica().getId());
			int id = listCondominio.get(0).getCondominio().getId();
			
			Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);
			
			int quantSindicos = pessoaFisicaCondominioRepository.countByCondominioIdAndTipoPessoaFisicaCondominioId(id,TipoPessoaFisicaCondominioEnum.SINDICO.getId());
			int quantAtendimentos = atendimentoRepository.countByCondominioIdOrderByTipoAtendimentoPrioridade(id);
			
			List<OrdemServico> listOs= ordemServicoRepository.ordemServicoEmAndamentoPorCondominio(id);
			
			List<Atendimento> listAtendimentoAberto = new ArrayList<>();
			List<OrdemServico> listOSMesCondominio = ordemServicoRepository.osPorCondominioPorMes(condominio.getId());
			
			List<OrdemServico> listOrdemServicoAvaliacao = ordemServicoRepository.osPorCondominioAguardandoAvaliacao(condominio.getId());
			
			for (Atendimento atendimento : condominio.getAtendimentos()) {
				if(atendimento.getUltimoStatusAtendimento().getStatusChamado().getId() == 1){
					listAtendimentoAberto.add(atendimento);
				}
			}		
			
			
			
			mv.addObject("quantOs", listOs.size()); 
			mv.addObject("listOs",listOs);
			mv.addObject("condominio",condominio);
			mv.addObject("quantSindicos",quantSindicos);
			mv.addObject("quantUnidades",condominio.getUnidades().size());
			mv.addObject("quantMoradores",condominio.moradoresCondominio().size());
			mv.addObject("quantVeiculos", condominio.veiculosCondominio().size()); 
			mv.addObject("quantAnimais", condominio.animaisCondominio().size()); 
			mv.addObject("quantAtendimentos", quantAtendimentos);
			mv.addObject("listAtendimentoAberto", listAtendimentoAberto);
			mv.addObject("listOSMesCondominio", listOSMesCondominio);
			mv.addObject("listOrdemServicoAvaliacao", listOrdemServicoAvaliacao);
			
		}
		
		if(usuario.getPessoaFisica().isAdministradorEmpresa()) {//empresa
			
			System.err.println("EMPRESA EMPRESA");
			List<OrdemServico> listOs= ordemServicoRepository.ordemServicoEmAndamento();
			List<Condominio> listCondominios = new ArrayList<>();
			List<PessoaJuridicaCondominio> listPessoaJuridicaCondominio = pessoaJuridicaCondominioRepository.findByPessoaJuridicaId(usuario.getPessoaJuridica().getId());
			for (PessoaJuridicaCondominio pessoaJuridicaCondominio : listPessoaJuridicaCondominio) {
				listCondominios.add(pessoaJuridicaCondominio.getCondominio());
			}
			
			quantCondominios = listCondominios.size();
			
			List<OrdemServico> listOsFuncionario= ordemServicoRepository.ordemServicoEmAndamentoFuncionario(usuario.getPessoaFisica().getId());
			
			List<Atendimento> listAtendimentoAberto = new ArrayList<>();
			mv.addObject("quantOs", listOs.size()); 
			mv.addObject("listOs",listOs);
			mv.addObject("listAtendimentoAberto", listAtendimentoAberto);
			mv.addObject("listOsFuncionario", listOsFuncionario);
		}
		
		if(usuario.getPessoaFisica().isMorador()) { 
			
			List<Unidade> listaUnidade = unidadeRepository.buscarUnidadesPorPessoaFisica(usuario.getPessoaFisica().getId());
			Condominio condominio =  condominioRepository.condominioComPessoaJuridica(usuario.getPessoaFisica().getCondominioAtual().getId());
			mv.addObject("tipoEnderecoUnidade", tipoEnderecoUnidadeRepository.findByCondominioId(condominio.getId()));
			mv.addObject("enderecoUnidade", enderecoUnidadeRepository.findByCondominioId(condominio.getId()));
			mv.addObject("listaUnidade",listaUnidade); 
			mv.addObject("unidade",listaUnidade.get(0)); 
			mv.addObject("condominio",condominio); 
			mv.addObject("quantUnidades",listaUnidade.size()); 
			
			
		}
		
		
		mv.addObject("quantCondominios", quantCondominios);
		
		return mv;
		
		
	}

	
	@RequestMapping("/esquecisenha")
	public ModelAndView novaSenha() { 
		EsqueciSenhaDTO esqueciSenhaDTO = new EsqueciSenhaDTO();
		
		
		ModelAndView mv = new ModelAndView("/syndic/esquecisenha"); 
		
		
		mv.addObject("esqueciSenhaDTO",esqueciSenhaDTO);
		
		
		return mv;
	}
	
	@PostMapping("/esquecisenha") 
	public ModelAndView alterarSenha(EsqueciSenhaDTO esqueciSenhaDTO, 
			RedirectAttributes attributes) {
		
		PessoaFisica usuario = pessoaFisicaRepository.porRecuperarSenha(esqueciSenhaDTO);
		
		
		if(usuario!=null) {
		
		//alterar senha de usuário
	    pessoaFisicaService.alterarSenhasEnviarEmail(usuario);
	    	attributes.addFlashAttribute("mensagem"," Senha Enviada para e-mail! ("+usuario.getEmails().get(0).getNome()+")");	
		
		}
		else {
			attributes.addFlashAttribute("erro"," Não foram encontradas informações referentes a este usuário ");	
			return new ModelAndView("redirect:/esquecisenha");
		}
		
		
		
		return new ModelAndView("redirect:/login");
		
		
	}
	
	
	@RequestMapping("/alterarsenha")
	public ModelAndView alterarSenha() { 
		
		
		ModelAndView mv = new ModelAndView("/syndic/alterarsenha"); 
		return mv;
	}
	
	
	 public static String deAccent(String str) {
		    String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
		    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		    return pattern.matcher(nfdNormalizedString).replaceAll("");
	}
	

}
