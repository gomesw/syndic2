package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.TipoAnimal;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.repository.TipoAnimalRepository;
import com.tushtech.syndic.service.TipoAnimalService;

@Controller
@RequestMapping("/tipoanimal")
public class TipoAnimalController {
	
	@Autowired
	private TipoAnimalRepository tipoAnimalRepository; 
	
	@Autowired
	private TipoAnimalService tipoAnimalService;	
	

	@RequestMapping("/listar")
	public ModelAndView getListarTipoAnimal(Unidade unidade) {
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoanimal/listar");
		mv.addObject("tipoAnimal", tipoAnimalRepository.findAll());
		return mv;
	}	
	
	@RequestMapping("/incluir") 
	public ModelAndView novoTipoAnimal(TipoAnimal tipoAnimal) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoanimal/incluir");
		mv.addObject("tipoAnimal", tipoAnimal);
		return mv;
	}
	
	
	@PostMapping("/incluir")
	public ModelAndView salvarTipoAnimal(@Valid TipoAnimal tipoAnimal, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novoTipoAnimal(tipoAnimal);
		}
		
		tipoAnimalService.salvarTipoAnimal(tipoAnimal);
		
		attributes.addFlashAttribute("mensagem","Tipo de animal cadastrado com sucesso!");
		return new ModelAndView("redirect:/tipoanimal/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarCondominio(@PathVariable Integer id, TipoAnimal tipoAnimal)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoanimal/incluir");
		
		mv.addObject("tipoAnimal",tipoAnimalRepository.findOne(tipoAnimal.getId()));
		
		return mv;
	}	 
//	
//	@RequestMapping("/excluir/{id}")
//	public @ResponseBody TipoAnimal excluir(TipoAnimal tipoAnimal)
//	{  
//		tipoAnimalService.excluirTipoAnimal(tipoAnimal);
//		return tipoAnimal;
//	}	
}