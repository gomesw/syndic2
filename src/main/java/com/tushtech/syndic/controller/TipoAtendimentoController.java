package com.tushtech.syndic.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.TipoAtendimento;
import com.tushtech.syndic.repository.TipoAtendimentoRepository;
import com.tushtech.syndic.service.TipoAtendimentoService;

@Controller
@RequestMapping("/tipoatendimento")
public class TipoAtendimentoController {

	@Autowired
	private TipoAtendimentoRepository tipoAtendimentoRepository; 
	
	@Autowired
	private TipoAtendimentoService tipoAtendimentoService; 
	
	@RequestMapping("/listar")
	public ModelAndView nova() {
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoatendimento/listar");
		
		List<TipoAtendimento> l = tipoAtendimentoRepository.findAll();
				
//		mv.addObject("listTipoAtendimento", tipoAtendimentoService.paisComFilhosOrdenada());
		mv.addObject("listTipoAtendimento", tipoAtendimentoRepository.findAll());
	
		return mv;
	}	
	
	@RequestMapping("/incluir")
	public ModelAndView incluir(TipoAtendimento tipoAtendimento) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoatendimento/incluir");
		
		List<TipoAtendimento> listTipoAtendimento = tipoAtendimentoRepository.findByAtivo(1);
		
		mv.addObject("tipoAtendimento", tipoAtendimento);
		mv.addObject("listTipoAtendimento", listTipoAtendimento);
		
		return mv;
	}
	
	//@Secured("ROLE_INCLUIR_FUNCAO")
	@PostMapping(value="/incluir")
	public ModelAndView incluir(@Valid TipoAtendimento tipoAtendimento, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return incluir(tipoAtendimento);
		}
		
		tipoAtendimentoService.salvar(tipoAtendimento);
		
		attributes.addFlashAttribute("mensagem"," Tipo de Atendimento cadastrada com sucesso!");
		return new ModelAndView("redirect:/tipoatendimento/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable Integer id, TipoAtendimento tipoAtendimento) 
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoatendimento/incluir");
		
		if(id!=null){
			tipoAtendimento = tipoAtendimentoRepository.findOne(id);
		}
		
		List<TipoAtendimento> listTipoAtendimento = tipoAtendimentoRepository.findByAtivo(1);
		
		mv.addObject("tipoAtendimento", tipoAtendimento);
		mv.addObject("listTipoAtendimento", listTipoAtendimento);
		return mv;
	}
	
//	@RequestMapping("/excluir/{id}")
//	public ModelAndView excluir(@PathVariable("id") Integer id){
//		
//		TipoAtendimento tipoAtendimento = tipoAtendimentoRepository.findOne(id);
//		
//		tipoAtendimentoService.desativar(tipoAtendimento);		
//		return new ModelAndView("redirect:/tipoatendimento");
//	}
	
}