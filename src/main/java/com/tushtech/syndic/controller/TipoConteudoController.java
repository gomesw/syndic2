package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.TipoConteudo;
import com.tushtech.syndic.repository.TipoConteudoRepository;
import com.tushtech.syndic.service.TipoConteudoService;

@Controller
@RequestMapping("/tipoconteudo")
public class TipoConteudoController {
	
	@Autowired
	private TipoConteudoRepository tipoConteudoRepository; 
	
	@Autowired
	private TipoConteudoService tipoConteudoService;	
	

	@RequestMapping("/listar")
	public ModelAndView listarTipoConteudo() {
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoconteudo/listar");
		mv.addObject("tipoConteudo", tipoConteudoRepository.findAll());
		return mv;
	}
	
	@RequestMapping("/incluir")
	public ModelAndView novoTipoConteudo(TipoConteudo tipoConteudo) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoconteudo/incluir");
		mv.addObject("tipoConteudo", tipoConteudo);
		return mv;
	}
	
	@PostMapping("/incluir")
	public ModelAndView salvarTipoConteudo(@Valid TipoConteudo tipoConteudo, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novoTipoConteudo(tipoConteudo);
		}
		
		if(tipoConteudo.isNovo()) {
			attributes.addFlashAttribute("mensagem","Tipo de conteudo cadastrado com sucesso!");
		}
		else {
			attributes.addFlashAttribute("mensagem","Tipo de conteudo editado com sucesso!");
		}
		
		tipoConteudoService.salvarTipoConteudo(tipoConteudo);
		
		return new ModelAndView("redirect:/tipoconteudo/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarTipoConteudo(@PathVariable Integer id, TipoConteudo tipoConteudo)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoconteudo/incluir");
				
		mv.addObject("tipoConteudo",tipoConteudoRepository.findOne(tipoConteudo.getId()));
		return mv;
	}	
	
	@RequestMapping("/excluir/{id}")
	public @ResponseBody TipoConteudo excluir(TipoConteudo tipoConteudo)
	{  
		tipoConteudoService.excluirTipoConteudo(tipoConteudo);
		return tipoConteudo;
	}	
}