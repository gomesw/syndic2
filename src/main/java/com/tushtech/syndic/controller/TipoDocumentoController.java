package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.TipoDocumento;
import com.tushtech.syndic.repository.TipoDocumentoRepository;
import com.tushtech.syndic.service.TipoDocumentoService;

@Controller
@RequestMapping("/tipodocumento")
public class TipoDocumentoController {
	
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository; 
	
	@Autowired
	private TipoDocumentoService tipoDocumentoService;	
	

	@RequestMapping("/listar")
	public ModelAndView getListarTipoDocumento() {
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipodocumento/listar");
		mv.addObject("tipoDocumento", tipoDocumentoRepository.findAll());
		return mv;
	}	
	
	@RequestMapping("/incluir") 
	public ModelAndView novoTipoDocumento(TipoDocumento tipoDocumento) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/tipodocumento/incluir");
		mv.addObject("tipoDocumento", tipoDocumento);
		return mv;
	}
	
	
	@PostMapping("/incluir")
	public ModelAndView salvarTipoDocumento(@Valid TipoDocumento tipoDocumento, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novoTipoDocumento(tipoDocumento);
		}
		
		if(tipoDocumento.isNovo()) {
			attributes.addFlashAttribute("mensagem","Tipo de documento cadastrado com sucesso!");
		}
		else {
			attributes.addFlashAttribute("mensagem","Tipo de documento editado com sucesso!");
		}
		
		tipoDocumentoService.salvarTipoDocumento(tipoDocumento);
		
		return new ModelAndView("redirect:/tipodocumento/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarCondominio(@PathVariable Integer id, TipoDocumento tipoDocumento)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipodocumento/incluir");
		
		mv.addObject("tipoDocumento",tipoDocumentoRepository.findOne(tipoDocumento.getId()));
		
		return mv;
	}	 

}