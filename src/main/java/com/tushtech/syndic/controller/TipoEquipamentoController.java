package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.TipoEquipamento;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.repository.TipoEquipamentoRepository;
import com.tushtech.syndic.service.TipoEquipamentoService;

@Controller
@RequestMapping("/tipoequipamento")
public class TipoEquipamentoController {
	
	@Autowired
	private TipoEquipamentoRepository tipoEquipamentoRepository; 
	
	@Autowired
	private TipoEquipamentoService tipoEquipamentoService;	
	

	@RequestMapping("")
	public ModelAndView getListarTipoEquipamento(Unidade unidade) {
		ModelAndView mv = new ModelAndView("/layout/cadastro/tipoequipamento/listartipoequipamento");
		mv.addObject("tipoEquipamento", tipoEquipamentoRepository.findAll());
		return mv;
		
	}	
	
	@RequestMapping("/incluir")
	public ModelAndView novoTipoEquipamento(TipoEquipamento tipoEquipamento) {

		ModelAndView mv = new ModelAndView("/layout/cadastro/tipoequipamento/incluirtipoequipamento");
		mv.addObject("tipoEquipamento", tipoEquipamento);
		return mv;
	}
	
	
	@PostMapping(value = {"/incluir", "/incluir/{\\d+}"})
	public ModelAndView salvarTipoEquipamento(@Valid TipoEquipamento tipoEquipamento, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novoTipoEquipamento(tipoEquipamento);
		}
		tipoEquipamentoService.salvarTipoEquipamento(tipoEquipamento);
		attributes.addFlashAttribute("mensagem","Tipo de animal cadastrado com sucesso!");
		return new ModelAndView("redirect:/tipoequipamento/");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarCondominio(@PathVariable Integer id, TipoEquipamento tipoEquipamento)
	{
		ModelAndView mv = new ModelAndView("/layout/cadastro/tipoequipamento/incluirtipoequipamento");
		mv.addObject("tipoEquipamento",tipoEquipamentoRepository.findOne(tipoEquipamento.getId()));
		return mv;
	}	
	
	@RequestMapping("/excluir/{id}")
	public @ResponseBody TipoEquipamento excluir(TipoEquipamento tipoEquipamento)
	{  
		tipoEquipamentoService.excluirTipoEquipamento(tipoEquipamento);
		return tipoEquipamento;
	}	
}