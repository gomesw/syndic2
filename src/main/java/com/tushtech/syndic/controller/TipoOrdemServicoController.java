package com.tushtech.syndic.controller;

import java.text.Normalizer;
import java.util.List;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.TipoOrdemServico;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.TipoOrdemServicoRepository;
import com.tushtech.syndic.service.TipoOrdemServicoService;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;


@Controller
@RequestMapping("/tipoordemservico")
public class TipoOrdemServicoController {
	
	@Autowired
	private TipoOrdemServicoRepository tipoOrdemServicoRepository;
	
	@Autowired
	private TipoOrdemServicoService tipoOrdemServicoService; 
	
	@RequestMapping
	public ModelAndView inicio(TipoOrdemServico tipoOrdemServico) {
		
		return formulario(tipoOrdemServico);
	}
	
	@RequestMapping("/{id}")
	public ModelAndView editar(TipoOrdemServico tipoOrdemServico, RedirectAttributes attributes){
		
		tipoOrdemServico = tipoOrdemServicoRepository.findOne(tipoOrdemServico.getId());
		
		if(tipoOrdemServico != null && tipoOrdemServico.getAtivo() != 1 && !tipoOrdemServico.isNovo()){
			attributes.addFlashAttribute("proibido", " Essa pergunta está desativado! <a href=\"/perguntaavaliacao/ativar/" + tipoOrdemServico.getId() + "\">Deseja Ativá-la ?</a>");
			return new ModelAndView("redirect:/tipoordemservico");
		}
		
		if(tipoOrdemServico==null || tipoOrdemServico.isNovo()){
			tipoOrdemServico = new TipoOrdemServico();
		}else{			
			tipoOrdemServico = tipoOrdemServicoRepository.findOne(tipoOrdemServico.getId());
		}
		ModelAndView mv = formulario(tipoOrdemServico);
		return mv;
	}
	
	@PostMapping("/{id}")
	public ModelAndView salvar(@Valid TipoOrdemServico tipoOrdemServico, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {
		if (result.hasErrors()) {
			return formulario(tipoOrdemServico);
		}
		
		try {
			tipoOrdemServicoService.salvar(tipoOrdemServico);
		} catch (NomeDuplicadoException e) { 
			result.rejectValue("nome", e.getMessage(), e.getMessage());
			return formulario(tipoOrdemServico);
		}
		
		attributes.addFlashAttribute("mensagem", "Registro salvo com sucesso!");
		return new ModelAndView("redirect:/tipoordemservico");
	}
	
	
	@GetMapping("/ativar/{id}")
	public ModelAndView ativar(TipoOrdemServico tipoOrdemServico, RedirectAttributes attributes)
	{
		tipoOrdemServico = tipoOrdemServicoService.ativar(tipoOrdemServico);
		attributes.addFlashAttribute("mensagem", "Registro ativado com sucesso!"); 
		return new ModelAndView("redirect:/tipoordemservico"); 
	}
	
	@DeleteMapping("/{id}")
	public @ResponseBody Integer desativar(TipoOrdemServico tipoOrdemServico)
	{
		tipoOrdemServicoService.desativar(tipoOrdemServico);
		return tipoOrdemServico.getId();
	}	

	//FORMULARIO PARA E EDITAR E SALVAR
	private ModelAndView formulario(TipoOrdemServico tipoOrdemServico) { 
		
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoordemservico/formulario");
		
		List<TipoOrdemServico> listTipoOrdemServico =  tipoOrdemServicoRepository.findByTipoOrdemServicoSubordinacaoIsNullAndAtivo(1);
		List<TipoOrdemServico> listaTipoOrdemServico =  tipoOrdemServicoRepository.findByAtivo(1);
		
		
		for (TipoOrdemServico tipo : listaTipoOrdemServico) {
			tipo.setNome(tipo.getNome());
		}
		
//		for (TipoOrdemServico tipo : listaTipoOrdemServico) {
//			tipo.setNome(deAccent(tipo.getNome()));
//		}
		
		
		mv.addObject("listTipoOrdemServico", listTipoOrdemServico);		
		mv.addObject("listaTipoOrdemServico", listaTipoOrdemServico);
		mv.addObject("tipoOrdemServico", tipoOrdemServico);
		return mv;
	}
	
	//retorna os subtipos de umA OS
	 @RequestMapping(value = "/listarsubtipoordemservico",consumes = MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody List<TipoOrdemServico> pesquisarSubTipoAtendimento(@RequestParam(name="id",defaultValue="-1") Integer id) {
		
		 return tipoOrdemServicoRepository.findByTipoOrdemServicoSubordinacaoId(id);
	 }
	 
	 
	 private static String deAccent(String str) {
		    String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
		    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		    return pattern.matcher(nfdNormalizedString).replaceAll("");
		}
	 
}