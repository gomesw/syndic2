package com.tushtech.syndic.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.TipoVeiculo;
import com.tushtech.syndic.repository.TipoVeiculoRepository;
import com.tushtech.syndic.service.TipoVeiculoService;

@Controller
@RequestMapping("/tipoveiculo")
public class TipoVeiculoController {
	
	@Autowired
	private TipoVeiculoRepository tipoVeiculoRepository; 
	
	@Autowired
	private TipoVeiculoService tipoVeiculoService;	
	

	@RequestMapping("/listar")
	public ModelAndView marca() {
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoveiculo/listar");
		mv.addObject("tipoVeiculo", tipoVeiculoRepository.findByAtivo(1));
		return mv;
	}	
	
	@RequestMapping("/incluir")
	public ModelAndView novoMarca(TipoVeiculo tipoVeiculo) {

		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoveiculo/incluir");
		mv.addObject("tipoVeiculo", tipoVeiculo);
		return mv;
	}
	
	@PostMapping("/incluir")
	public ModelAndView salvarTipoAnimal(@Valid TipoVeiculo tipoVeiculo, BindingResult result, RedirectAttributes attributes) {
			
		if(result.hasErrors()){
			return novoMarca(tipoVeiculo);
		}
		
		tipoVeiculo.setAtivo(1);
		tipoVeiculoService.salvarTipoVeiculo(tipoVeiculo);
		
		attributes.addFlashAttribute("mensagem","Tipo de veículo cadastrado com sucesso!");
		return new ModelAndView("redirect:/tipoveiculo/listar");
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView editarCondominio(@PathVariable Integer id, TipoVeiculo tipoVeiculo)
	{
		ModelAndView mv = new ModelAndView("/syndic/sistema/tipoveiculo/incluir");
		
		mv.addObject("tipoVeiculo", tipoVeiculoRepository.findOne(tipoVeiculo.getId()));
		
		return mv;
	}	 
	
//	@RequestMapping("/excluir/{id}")
//	public @ResponseBody Marca excluir(Marca marca)
//	{  
//		marcaService.excluirMarca(marca);
//		return marca;
//	}	
}