package com.tushtech.syndic.controller;


import java.io.File;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Atendimento;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.Cor;
import com.tushtech.syndic.entity.Documento;
import com.tushtech.syndic.entity.Marca;
import com.tushtech.syndic.entity.Modelo;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.PessoaFisicaVeiculo;
import com.tushtech.syndic.entity.StatusDocumento;
import com.tushtech.syndic.entity.TipoAtendimento;
import com.tushtech.syndic.entity.TipoDocumento;
import com.tushtech.syndic.entity.TipoPessoaFisicaUnidade;
import com.tushtech.syndic.entity.TipoVeiculo;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.Veiculo;
import com.tushtech.syndic.repository.AtendimentoRepository;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.CorRepository;
import com.tushtech.syndic.repository.DocumentoRepository;
import com.tushtech.syndic.repository.EnderecoUnidadeRepository;
import com.tushtech.syndic.repository.MarcaRepository;
import com.tushtech.syndic.repository.ModeloRepository;
import com.tushtech.syndic.repository.PessoaFisicaUnidadeRepository;
import com.tushtech.syndic.repository.PessoaFisicaVeiculoRepository;
import com.tushtech.syndic.repository.StatusDocumentoRepository;
import com.tushtech.syndic.repository.TipoAtendimentoRepository;
import com.tushtech.syndic.repository.TipoDocumentoRepository;
import com.tushtech.syndic.repository.TipoEnderecoUnidadeRepository;
import com.tushtech.syndic.repository.TipoUnidadeRepository;
import com.tushtech.syndic.repository.TipoVeiculoRepository;
import com.tushtech.syndic.repository.UnidadeRepository;
import com.tushtech.syndic.service.AtendimentoService;
import com.tushtech.syndic.service.DocumentoService;
import com.tushtech.syndic.service.PessoaFisicaService;
import com.tushtech.syndic.service.PessoaFisicaVeiculoService;
import com.tushtech.syndic.service.UnidadeService;
import com.tushtech.syndic.service.VeiculoService;
import com.tushtech.syndic.service.exception.VeiculoJaCadastradoException;
import com.tushtech.syndic.storage.ArquivoStorage;



@Controller
@RequestMapping() 
public class UnidadesController {

	@Autowired
	private UnidadeRepository unidadeRepository; 

	@Autowired
	private TipoUnidadeRepository tipoUnidadeRepository; 

	@Autowired
	private CondominioRepository condominioRepository; 

	@Autowired
	private MarcaRepository marcaRepository; 	

	@Autowired
	private PessoaFisicaUnidadeRepository pessoaFisicaUnidadeRepository; 	

	@Autowired
	private DocumentoService documentoService; 	



	@Autowired
	private StatusDocumentoRepository statusDocumentoRepository;



	@Autowired
	private CorRepository corRepository;  




	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;


	@Autowired
	private ArquivoStorage arquivoStorage;

	@Autowired
	private DocumentoRepository documentoRepository; 


	@Autowired
	private ModeloRepository modeloRepository; 	

	@Autowired
	private TipoVeiculoRepository tipoVeiculoRepository; 	




	@Autowired
	private TipoEnderecoUnidadeRepository tipoEnderecoUnidadeRepository;	

	@Autowired
	private EnderecoUnidadeRepository enderecoUnidadeRepository;


	@Autowired
	private UnidadeService unidadeService;

	@Autowired
	private PessoaFisicaService pessoaFisicaService;


	@Autowired
	private VeiculoService veiculoService;



	@Autowired
	private PessoaFisicaVeiculoService pessoaFisicaVeiculoService;

	@Autowired
	private PessoaFisicaVeiculoRepository pessoaFisicaVeiculoRepository;

	@Autowired
	private TipoAtendimentoRepository tipoAtendimentoRepository;


	@Autowired
	private AtendimentoService atendimentoService;

	@Autowired
	private AtendimentoRepository atendimentoRepository;

	
	
	@RequestMapping("/unidade")
	public ModelAndView getUnidadesPessoa(@AuthenticationPrincipal UsuarioSistema usuario) {
		
		ModelAndView mv = new ModelAndView("/syndic/unidades/formeditarpormorador");
		List<Unidade> listaUnidade = unidadeRepository.buscarUnidadesPorPessoaFisica(usuario.getPessoaFisica().getId());
		Condominio condominio =  condominioRepository.condominioComPessoaJuridica(usuario.getPessoaFisica().getCondominioAtual().getId());
		mv.addObject("tipoEnderecoUnidade", tipoEnderecoUnidadeRepository.findByCondominioId(condominio.getId()));
		mv.addObject("enderecoUnidade", enderecoUnidadeRepository.findByCondominioId(condominio.getId()));
		mv.addObject("listaUnidade",listaUnidade); 
		mv.addObject("unidade",listaUnidade.get(0)); 
		mv.addObject("condominio",condominio); 
		mv.addObject("quantUnidades",listaUnidade.size()); 
		return mv;

	}
	
	
//	@RequestMapping("/unidade/listar/{id}")
//	public ModelAndView getUnidades(@PathVariable Integer id) {
//		ModelAndView mv = new ModelAndView("/syndic/unidades/listar");
//
//
//		List<Unidade> unidades =  unidadeRepository.buscarUnidadesPorCondominio(id);
//
//		mv.addObject("unidades", unidades);
//		mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(id)); 
//		return mv;
//
//	}

	
	private ModelAndView formulario(Unidade unidade) {
		ModelAndView mv = new ModelAndView("/syndic/unidades/formulario"); 
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId());
		unidade.setCondominio(condominio);
		List<Unidade> unidades =  unidadeRepository.buscarUnidadesPorCondominio(unidade.getCondominio().getId());
		mv.addObject("tipoUnidade", tipoUnidadeRepository.findAll());
		mv.addObject("tipoEnderecoUnidade", tipoEnderecoUnidadeRepository.findByCondominioId(unidade.getCondominio().getId()));
		mv.addObject("enderecoUnidade", enderecoUnidadeRepository.findByCondominioId(unidade.getCondominio().getId()));
		mv.addObject("unidades", unidades);
		mv.addObject("unidade", unidade);
		mv.addObject("condominio", condominio);
		return mv;
	}
	
	
	

	@RequestMapping("/unidade/gerenciar/{id}")
	public ModelAndView novaUnidade(@PathVariable Integer id) {
		Unidade unidade = new Unidade();
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(id);
		unidade.setCondominio(condominio);
		ModelAndView mv = formulario(unidade);
		return mv;
	}

	
	
	@RequestMapping(value = "/unidade/editar/{id}", method = RequestMethod.GET)
	public ModelAndView editarUnidade(@PathVariable Integer id) 
	{

		Unidade unidade = unidadeRepository.findById(id); //unidadeRepository.buscarUnidadeCompleta(id); 
		ModelAndView mv = formulario(unidade);
		
		return mv; 
	}



	/**
	 * SALVAR UMA NOVA UNIDADE NO BANCO DE DADOS
	 * @param unidade
	 * @param result
	 * @param attributes
	 * @return
	 */
	@PostMapping(value = {"/unidade/nova", "/unidade/nova/{\\d+}"})
	public ModelAndView salvarUnidade(@Valid Unidade unidade, BindingResult result, RedirectAttributes attributes) {


		boolean isNova = unidade.isNovo();


		unidadeService.atribuirErrosUnidade(unidade, result); 

		if(result.hasErrors()){
			return formulario(unidade); 
		}

		unidade  = unidadeService.salvarUnidadeRetornar(unidade);

		if(isNova) {
			attributes.addFlashAttribute("mensagem"," Unidade cadastrada com sucesso!");
		}
		else {
			attributes.addFlashAttribute("mensagem"," Unidade editada com sucesso!");
		}
		
		
		return new ModelAndView("redirect:/unidade/editar/" + unidade.getId()); 
	}




	
//	/**
//	 * TELA PARA O MORADOR EDITAR SUA PRÓPRIA UNIDADE
//	 * @param id
//	 * @param unidade
//	 * @return
//	 */
//	@RequestMapping(value = "/unidade/editarpormorador/{id}", method = RequestMethod.GET)
//	public ModelAndView editarUnidadePorMorador(@PathVariable Integer id,Unidade unidade) 
//	{
//		ModelAndView mv = new ModelAndView("/syndic/unidades/formeditarpormorador"); 
//
//		unidade = unidadeRepository.buscarUnidadeCompleta(id); 
//		
//		Condominio condominio =  condominioRepository.condominioComPessoaJuridica(unidade.getEnderecoUnidade().getCondominio().getId());
//		
//		mv.addObject("tipoUnidade", tipoUnidadeRepository.findAll());
//		mv.addObject("tipoEnderecoUnidade", tipoEnderecoUnidadeRepository.findByCondominioId(unidade.getEnderecoUnidade().getCondominio().getId()));
//		mv.addObject("enderecoUnidade", enderecoUnidadeRepository.findByCondominioId(unidade.getEnderecoUnidade().getCondominio().getId()));
//		mv.addObject("unidade",unidade); 
//		mv.addObject("condominio",condominio); 
//		return mv; 
//	}
	

	@RequestMapping("/unidade/documento/listar/{id}")
	public ModelAndView getDocumentosUnidades(@PathVariable Integer id) {
		ModelAndView mv = new ModelAndView("/syndic/unidades/documento/listar");
		Unidade unidade = unidadeRepository.findById(id);
		List<Documento> listDocumento =  unidade.getListaDocumentos();


		mv.addObject("listDocumento",listDocumento);
		mv.addObject("unidade",unidade);
		mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId())); 
		return mv;

	}


	/**
	 * TELA PARA ADICIONAR UM DOCUMENTO A UMA UNIDADE
	 * LEMBRANDO QUE O DOCUMENTO É VINCULADO A PESSOAFISICAUNIDADE
	 * @param id DA UNIDADE
	 * @return
	 */
	@RequestMapping("/unidade/documento/adicionar/{id}")
	public ModelAndView novoDocumentolUnidade(@PathVariable Integer id) {

		ModelAndView mv = new ModelAndView("/syndic/unidades/documento/formincluir"); 
		Unidade unidade = unidadeRepository.buscarUnidadeCompleta(id);
		Documento documento = new Documento(); 
		PessoaFisicaUnidade pessoaFisicaUnidade = new PessoaFisicaUnidade();
		documento.setPessoaFisicaUnidade(pessoaFisicaUnidade);

		StatusDocumento statusDocumento = new StatusDocumento();

		statusDocumento.setId(1);
		documento.setStatusDocumento(statusDocumento);

		documento.setUnidade(unidade);


		List<PessoaFisicaUnidade> listProprietarios =  unidade.getListaProprietarioUnidade();

		List<TipoDocumento> listTipoDocumento = tipoDocumentoRepository.findAll();

		List<StatusDocumento> listStatusDocumento = statusDocumentoRepository.findAll(); 

		mv.addObject("listTipoDocumento", listTipoDocumento); 

		mv.addObject("listStatusDocumento", listStatusDocumento);
		mv.addObject("listProprietarios",listProprietarios);
		mv.addObject("documento", documento); 
		mv.addObject("unidade", unidade); 
		mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());


		return mv;
	}

	@RequestMapping("/unidade/documento/adicionarpormorador/{id}")
	public ModelAndView novoDocumentolUnidadePorMorador(@PathVariable Integer id,@AuthenticationPrincipal UsuarioSistema usuario) {

		ModelAndView mv = new ModelAndView("/syndic/unidades/documento/formincluirpormorador"); 

		Unidade unidade = unidadeRepository.buscarUnidadeCompleta(id);
		Documento documento = new Documento(); 
		PessoaFisicaUnidade pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.pessoasPorUnidadePorIdPessoa(id, usuario.getPessoaFisica().getId());
		documento.setPessoaFisicaUnidade(pessoaFisicaUnidade);

		documento.setUnidade(unidade);



		List<TipoDocumento> listTipoDocumento = tipoDocumentoRepository.findAll();
		mv.addObject("listTipoDocumento", listTipoDocumento); 
		mv.addObject("documento", documento); 
		mv.addObject("unidade", unidade); 
		mv.addObject("condominio", unidade.getEnderecoUnidade().getCondominio());


		return mv;
	}


	private ModelAndView novoDocumentoUnidadeErro(Documento documento) {

		ModelAndView mv = new ModelAndView("/syndic/unidades/documento/formincluir"); 


		List<PessoaFisicaUnidade> listProprietarios =  documento.getUnidade().getListaProprietarioUnidade();

		List<TipoDocumento> listTipoDocumento = tipoDocumentoRepository.findAll();
		mv.addObject("listTipoDocumento", listTipoDocumento);
		mv.addObject("listProprietarios", listProprietarios); 
		mv.addObject("documento", documento); 
		mv.addObject("unidade", documento.getUnidade()); 
		mv.addObject("condominio", documento.getUnidade().getEnderecoUnidade().getCondominio());


		return mv;
	}


	@RequestMapping(value = "/unidade/documento/editar/{id}", method = RequestMethod.GET)
	public ModelAndView editarDocumento(@PathVariable Integer id,Documento documento) 
	{
		ModelAndView mv = new ModelAndView("/syndic/unidades/documento/formincluir"); 

		documento = documentoRepository.findOne(id); 

		List<PessoaFisicaUnidade> listProprietarios =  documento.getUnidade().getListaProprietarioUnidade();

		List<TipoDocumento> listTipoDocumento = tipoDocumentoRepository.findAll();

		List<StatusDocumento> listStatusDocumento = statusDocumentoRepository.findAll();

		mv.addObject("listTipoDocumento", listTipoDocumento);
		mv.addObject("listStatusDocumento", listStatusDocumento);
		mv.addObject("listProprietarios", listProprietarios); 
		mv.addObject("documento", documento); 
		mv.addObject("unidade", documento.getUnidade()); 
		mv.addObject("condominio", documento.getUnidade().getEnderecoUnidade().getCondominio());
		return mv; 
	}

	@RequestMapping(value = "/unidade/documento/editarpormorador/{id}", method = RequestMethod.GET)
	public ModelAndView editarDocumentoPorMorador(@PathVariable Integer id,Documento documento) 
	{
		ModelAndView mv = new ModelAndView("/syndic/unidades/documento/formincluirpormorador"); 

		documento = documentoRepository.findOne(id); 

		List<PessoaFisicaUnidade> listProprietarios =  documento.getUnidade().getListaProprietarioUnidade();

		List<TipoDocumento> listTipoDocumento = tipoDocumentoRepository.findAll();
		mv.addObject("listTipoDocumento", listTipoDocumento);
		mv.addObject("listProprietarios", listProprietarios); 
		mv.addObject("documento", documento); 
		mv.addObject("unidade", documento.getUnidade()); 
		mv.addObject("condominio", documento.getUnidade().getEnderecoUnidade().getCondominio());
		return mv; 
	}




	@RequestMapping("/unidade/veiculo/listar/{id}")
	public ModelAndView getVeiculosUnidades(@PathVariable Integer id) {
		ModelAndView mv = new ModelAndView("/syndic/unidades/veiculo/listar");

		Unidade unidade = unidadeRepository.buscarUnidadeCompleta(id);

		//		List<Veiculo> listVeiculo =  unidade.getVeiculo();
		//		mv.addObject("listVeiculo",listVeiculo);
		mv.addObject("unidade",unidade);
		mv.addObject("condominio", condominioRepository.condominioComPessoaJuridica(unidade.getCondominio().getId())); 
		return mv;

	}

	/**
	 * TELA PARA ADICIONAR UM VEICULO A UMA UNIDADE
	 * @param id DA UNIDADE
	 * @return
	 */
	@RequestMapping("/unidade/veiculo/adicionar/{id}")
	public ModelAndView novoVeiculoUnidade(@PathVariable Integer id) {
		ModelAndView mv = new ModelAndView("/syndic/unidades/veiculo/formincluir"); 

		Unidade unidade = unidadeRepository.findById(id);

		List<PessoaFisicaUnidade> listPessoaFisicaUnidade = unidade.getListaPessoasAtivasUnidade();
		PessoaFisicaVeiculo pessoaFisicaVeiculo = new PessoaFisicaVeiculo(); 
		List<Modelo> modeloVeiculo = new ArrayList<>();

		List<Marca> marcaVeiculo = marcaRepository.findByTipoVeiculoIdOrderByNomeAsc(0);
		List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);

		List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);

		mv.addObject("modeloVeiculo", modeloVeiculo); 
		mv.addObject("marcaVeiculo", marcaVeiculo); 
		mv.addObject("listPessoaFisicaUnidade", listPessoaFisicaUnidade); 
		mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
		mv.addObject("tipoVeiculo", tipoVeiculo); 
		mv.addObject("cores", cores); 
		mv.addObject("unidade",unidade);


		return mv;
	}

	@RequestMapping(value = "/unidade/veiculo/editar/{id}/{idunidade}", method = RequestMethod.GET)
	public ModelAndView editarVeiculo(@PathVariable("id") Integer id,@PathVariable("idunidade") Integer idunidade,PessoaFisicaVeiculo pessoaFisicaVeiculo) 
	{

		pessoaFisicaVeiculo = pessoaFisicaVeiculoRepository.findOne(id); 
		Unidade unidade = unidadeRepository.buscarUnidadeCompleta(idunidade);
		ModelAndView mv = formularioVeiculo(pessoaFisicaVeiculo,unidade);
		return mv; 
	}

	private ModelAndView formularioVeiculo(PessoaFisicaVeiculo pessoaFisicaVeiculo,Unidade unidade) {

		ModelAndView mv = new ModelAndView("/syndic/unidades/veiculo/formincluir"); 
		unidade = unidadeRepository.buscarUnidadeCompleta(unidade.getId());
		List<PessoaFisicaUnidade> listPessoaFisicaUnidade = unidade.getListaPessoasAtivasUnidade();

		List<Modelo> modeloVeiculo = modeloRepository.findByMarcaIdOrderByNomeAsc(pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getId());
		List<Marca> marcaVeiculo = marcaRepository.findByTipoVeiculoIdOrderByNomeAsc(pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getTipoVeiculo()==null?0:pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getTipoVeiculo().getId());
		List<TipoVeiculo> tipoVeiculo = tipoVeiculoRepository.findByIdGreaterThanOrderByNomeAsc(0);

		List<Cor> cores = corRepository.findByIdGreaterThanOrderByNomeAsc(0);

		mv.addObject("modeloVeiculo", modeloVeiculo); 

		mv.addObject("marcaVeiculo", marcaVeiculo); 
		mv.addObject("listPessoaFisicaUnidade", listPessoaFisicaUnidade); 
		mv.addObject("pessoaFisicaVeiculo", pessoaFisicaVeiculo); 
		mv.addObject("tipoVeiculo", tipoVeiculo); 
		mv.addObject("cores", cores); 
		mv.addObject("unidade",unidade);


		return mv;
	}



	/**
	 * SALVAR UM NOVO VEICULO NO BANCO DE DADOS
	 * @param pessoaFisicaUnidade
	 * @param recursos
	 * @param result
	 * @param attributes
	 * @return
	 */
	@PostMapping(value = {"/unidade/veiculo/novo", "/veiculo/novo/{\\d+}"})
	public ModelAndView salvarMoradorUnidade(@Valid PessoaFisicaVeiculo pessoaFisicaVeiculo,
			@RequestParam(value  = "unidade", required = false) Integer unidade, 
			BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuario) {

		boolean isNovo = pessoaFisicaVeiculo.isNovo();

		pessoaFisicaService.atribuirErrosPessoaFisicaVeiculo(pessoaFisicaVeiculo, result);

		PessoaFisicaUnidade pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.findById(pessoaFisicaVeiculo.getPessoaFisicaUnidade().getId());


		Unidade unid = unidadeRepository.findById(Integer.valueOf(unidade));


		Veiculo veiculo = new Veiculo();


		try {
			veiculo =  veiculoService.salvarVeiculo(pessoaFisicaVeiculo.getVeiculo());
		} catch (VeiculoJaCadastradoException e) {
			result.rejectValue("veiculo.placa", e.getMessage(), e.getMessage());
			return formularioVeiculo(pessoaFisicaVeiculo,unid);
		}

		if(result.hasErrors()){  
			return formularioVeiculo(pessoaFisicaVeiculo,unid);
		}




		if(isNovo){
			pessoaFisicaVeiculo.getVeiculo().setCadastrante(usuario.getPessoaFisica());
		}

		pessoaFisicaVeiculo.setTipoPessoaFisicaUnidade(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade());
		pessoaFisicaVeiculo.setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());




		pessoaFisicaVeiculo.setVeiculo(veiculo);

		pessoaFisicaVeiculoService.salvar(pessoaFisicaVeiculo);


		if(isNovo){
			attributes.addFlashAttribute("mensagem","Veículo cadastrado com sucesso!");
		}
		else{
			attributes.addFlashAttribute("mensagem","Veículo editado com sucesso!");
		}
		return new ModelAndView("redirect:/unidade/editar/"+unidade);
	}











	/**
	 * SALVAR DOCUMENTO  NO BANCO DE DADOS
	 * @param unidade
	 * @param result
	 * @param attributes
	 * @return
	 */
	@PostMapping(value = {"/unidade/documento/novo", "/documento/novo/{\\d+}"})
	public ModelAndView salvarDocumento(@Valid Documento documento, BindingResult result, RedirectAttributes attributes,@AuthenticationPrincipal UsuarioSistema usuarioSistema) {

		documento.setCadastrante(usuarioSistema.getPessoaFisica());
		documento.setAtivo(1);

		if(documento.isNovo()){
			StatusDocumento statusDocumento = new StatusDocumento();
			statusDocumento.setId(1);

			documento.setStatusDocumento(statusDocumento);
		}

		Timestamp dataDeHoje = new Timestamp(System.currentTimeMillis());
		documento.setDataCadastro(dataDeHoje);

		if(result.hasErrors()){
			return novoDocumentoUnidadeErro(documento);
		}
		documentoService.salvarDocumento(documento);
		attributes.addFlashAttribute("mensagem"," Documento cadastrado com sucesso!");

		if(usuarioSistema.getPessoaFisica().isProprietario()) {
			return new ModelAndView("redirect:/unidade/editarpormorador/" + documento.getUnidade().getId());
		}
		else {
			return new ModelAndView("redirect:/unidade/editar/" + documento.getUnidade().getId());
		}


	} 


	@RequestMapping(value ="/unidade/documento/excluir/{id}", method = RequestMethod.GET)
	public ModelAndView excluirDocumento(@PathVariable Integer id) 
	{
		Documento documento = documentoRepository.findById(id);

		Condominio condominio = documento.getUnidade().getEnderecoUnidade().getCondominio();

		arquivoStorage.remover(documento.getNome(),condominio.getId()+File.separator+"ArquivosUnidades");

		documentoRepository.delete(documento.getId());



		return new ModelAndView("redirect:/unidade/documento/listar/"+documento.getUnidade().getId());
	}





	@RequestMapping("/unidade/excluir/{id}")
	public @ResponseBody Unidade excluir(Unidade unidade)
	{  
		unidadeService.excluirUnidade(unidade);
		return unidade;
	}












	//retorna todas as cidades de um determinado estado
	@RequestMapping(value = "/unidade/buscartipopessoa",consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody TipoPessoaFisicaUnidade pesquisarTipoPessoaPorCodigoPessoaCodigoUnidade(
			@RequestParam(name="idunidade",defaultValue="-1") Integer unidade,
			@RequestParam(name="idpessoa",defaultValue="-1") Integer pessoa) {

		PessoaFisicaUnidade pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.pessoasPorUnidadePorIdPessoa(unidade,pessoa);

		TipoPessoaFisicaUnidade tipo = new TipoPessoaFisicaUnidade();
		tipo.setId(0);

		if(pessoaFisicaUnidade!=null){
			tipo = pessoaFisicaUnidade.getTipoPessoaFisicaUnidade();
		}
		//		 	

		return tipo;

	}





	/**
	 * ******************************************************************************************************************************************************************************
	 * Morador abrir uma nova solicitação
	 * ******************************************************************************************************************************************************************************
	 */

	@RequestMapping("/unidade/atendimento/listar")
	public ModelAndView nova(Atendimento atendimento, @AuthenticationPrincipal UsuarioSistema usuario) {
		ModelAndView mv = new ModelAndView("/syndic/unidades/atendimento/listar");

		List<Atendimento> listAtendimento = atendimentoRepository.findByPessoaFisicaIdOrderByProtocoloDesc(usuario.getPessoaFisica().getId());

		mv.addObject("listAtendimento", listAtendimento);

		return mv;
	}

	@RequestMapping("/unidade/atendimento/incluir")
	public ModelAndView novoAtendimento(Atendimento atendimento) {

		ModelAndView mv = new ModelAndView("/syndic/unidades/atendimento/incluir");
		mv.addObject("atendimento", atendimento);
		mv.addObject("listTipoAtendimentoPrincipal", tipoAtendimentoRepository.findByAtivoAndIdSubordinacaoIsNull(1));
		return mv;
	}

	public ModelAndView novoAtendimentoComErro(Atendimento atendimento) {

		ModelAndView mv = new ModelAndView("/syndic/unidades/atendimento/incluir");

		mv.addObject("atendimento", atendimento);
		mv.addObject("listTipoAtendimentoPrincipal", tipoAtendimentoRepository.findByAtivoAndIdSubordinacaoIsNull(1));
		mv.addObject("subtipoatendimento", atendimento.getTipoAtendimento().getIdSubordinacao().getTipoAtendimentosFilhos());
		return mv;
	}

	@PostMapping("/unidade/atendimento/incluir")
	public ModelAndView novoAtendimento(@Valid Atendimento atendimento, BindingResult result, RedirectAttributes attributes, @AuthenticationPrincipal UsuarioSistema usuario) {

		atendimentoService.atribuirErros(atendimento, result);

		if(result.hasErrors()){
			return novoAtendimentoComErro(atendimento);
		}		

		//PessoaFisica pfu = pessoaFisicaUnidadeRepository.findByPessoaFisicaId(usuario.getPessoaFisica().getId());

		atendimento.setDataCadastro(java.sql.Date.valueOf(LocalDate.now()));
//		atendimento.setPessoaFisica(usuario.getPessoaFisica());
//		atendimento.setCondominio(usuario.getPessoaFisica().getCondominioAtual()); 
//		atendimento.setUnidade(pfu.getUnidade());


		atendimentoService.setProtocolo(atendimento);//setar protocolo

		atendimentoService.salvar(atendimento);

		attributes.addFlashAttribute("mensagem"," Soliciatação cadastrada com sucesso!");		
		return new ModelAndView("redirect:/unidade/atendimento/listar");
	}

	@RequestMapping(value = "/unidade/atendimento/listarsubtipoatendimento",consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<TipoAtendimento> pesquisarSubTipoAtendimento(@RequestParam(name="id",defaultValue="-1") Integer id) {

		return tipoAtendimentoRepository.findByIdSubordinacaoId(id);
	}


}