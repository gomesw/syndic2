package com.tushtech.syndic.controller.handler;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.tushtech.syndic.service.exception.NomeDuplicadoException;
import com.tushtech.syndic.service.exception.VeiculoJaCadastradoException;

@ControllerAdvice
public class ControllerAdviceExceptionHandler {
	
	@ExceptionHandler(VeiculoJaCadastradoException.class)
	public ResponseEntity<String> handleVeiculoJaCadastradoException(VeiculoJaCadastradoException e) {
		return ResponseEntity.badRequest().body(e.getMessage());
	}
	
	@ExceptionHandler(NomeDuplicadoException.class)
	public ResponseEntity<String> handleNomeDuplicadoException(NomeDuplicadoException e) {
		return ResponseEntity.badRequest().body(e.getMessage());
	}
}
