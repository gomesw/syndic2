package com.tushtech.syndic.dto;

import com.tushtech.syndic.entity.Condominio;

public class AlterarSenhaDTO {
	
	private String passwordOld;
	private String registerPassword;
	private String registerPasswordConfirmation;
	private Condominio condominio;
	
	
	public Condominio getCondominio() {
		return condominio;
	}
	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}
	public String getPasswordOld() {
		return passwordOld;
	}
	public void setPasswordOld(String passwordOld) {
		this.passwordOld = passwordOld;
	}
	public String getRegisterPassword() {
		return registerPassword;
	}
	public void setRegisterPassword(String registerPassword) {
		this.registerPassword = registerPassword;
	}
	public String getRegisterPasswordConfirmation() {
		return registerPasswordConfirmation;
	}
	public void setRegisterPasswordConfirmation(String registerPasswordConfirmation) {
		this.registerPasswordConfirmation = registerPasswordConfirmation;
	}
	
	
	
	

}
