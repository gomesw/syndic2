package com.tushtech.syndic.dto;

public class AtendimentoMesStatus {
	
	private String status;
	private Integer total;
	private String cor;
	
	
	
	public AtendimentoMesStatus(String status, Integer total, String cor) {
		super();
		this.status = status;
		this.total = total;
		this.cor = cor;
	}

	
	
	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	
	
	

}
