package com.tushtech.syndic.dto;

import java.util.Arrays;
import java.util.List;

public class AtendimentoMesTipo {
	
	private String tipo;
	private String cor;
	private List<String> mes;
	private int[] quantidade;
	
	
	public AtendimentoMesTipo(String tipo, String cor, List<String> mes, int[] quantidade) {
		super();
		this.tipo = tipo;
		this.cor = cor;
		this.mes = mes;
		this.quantidade = quantidade;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public List<String> getMes() {
		return mes;
	}
	public void setMes(List<String> mes) {
		this.mes = mes;
	}
	public int[] getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int[] quantidade) {
		this.quantidade = quantidade;
	}
	@Override
	public String toString() {
		return "AtendimentoMesTipo [" + (tipo != null ? "tipo=" + tipo + ", " : "")
				+ (cor != null ? "cor=" + cor + ", " : "") + (mes != null ? "mes=" + mes + ", " : "")
				+ (quantidade != null ? "quantidade=" + Arrays.toString(quantidade) : "") + "]";
	}
	
	


}
