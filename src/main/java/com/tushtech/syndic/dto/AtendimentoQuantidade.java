package com.tushtech.syndic.dto;

public class AtendimentoQuantidade {
	
	private Integer id;
	private String tipo;
	private int quantidade;
	
	public AtendimentoQuantidade(Integer id, String tipo, int quantidade) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.quantidade = quantidade;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	
	
	
	
	

}
