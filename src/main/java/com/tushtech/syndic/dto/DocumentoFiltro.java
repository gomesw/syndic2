package com.tushtech.syndic.dto;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.EnderecoUnidade;
import com.tushtech.syndic.entity.TipoDocumento;
import com.tushtech.syndic.entity.TipoEnderecoUnidade;
import com.tushtech.syndic.entity.Unidade;

public class DocumentoFiltro {
	
	
	private TipoDocumento tipoDocumento;
	
	private EnderecoUnidade enderecoUnidade;
	
	private TipoEnderecoUnidade tipoEnderecoUnidade;
	
	private String descricao;
	
	private Unidade unidade;
	
	private Condominio condominio;
	
	

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public EnderecoUnidade getEnderecoUnidade() {
		return enderecoUnidade;
	}

	public void setEnderecoUnidade(EnderecoUnidade enderecoUnidade) {
		this.enderecoUnidade = enderecoUnidade;
	}

	public TipoEnderecoUnidade getTipoEnderecoUnidade() {
		return tipoEnderecoUnidade;
	}

	public void setTipoEnderecoUnidade(TipoEnderecoUnidade tipoEnderecoUnidade) {
		this.tipoEnderecoUnidade = tipoEnderecoUnidade;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
	

}
