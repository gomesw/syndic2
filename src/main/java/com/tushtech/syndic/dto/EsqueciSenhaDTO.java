package com.tushtech.syndic.dto;

import java.util.Date;

public class EsqueciSenhaDTO {

	String nome;
	String cpf;
	Date dataNascimento;
	
	
	public EsqueciSenhaDTO() {
		
	}
	
	public EsqueciSenhaDTO(String nome, String cpf, Date dataNascimento) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.dataNascimento = dataNascimento;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Date getDataNascimento() { 
		return dataNascimento;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	
	
	
}
