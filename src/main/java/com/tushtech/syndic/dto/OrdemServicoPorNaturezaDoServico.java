package com.tushtech.syndic.dto;

public class OrdemServicoPorNaturezaDoServico {
	
	private String nome;
	private int quantidade;
	private String cor;
	
	public OrdemServicoPorNaturezaDoServico() {
		
	}
	
	public OrdemServicoPorNaturezaDoServico(String nome, int quantidade) {
		super();
		this.nome = nome;
		this.quantidade = quantidade;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}
	
	
	
}
