package com.tushtech.syndic.dto;

public class PacoteServicoPorCondominio {
	
	private int pacoteId;
	private String pacoteServico;
	private int quantidade;
	private String cor;
	
	public PacoteServicoPorCondominio(int pacoteId, String pacoteServico, int quantidade, String cor) {
		super();
		this.pacoteId = pacoteId;
		this.pacoteServico = pacoteServico;
		this.quantidade = quantidade;
		this.cor = cor;
	}

	public int getPacoteId() {
		return pacoteId;
	}

	public void setPacoteId(int pacoteId) {
		this.pacoteId = pacoteId;
	}

	public String getPacoteServico() {
		return pacoteServico;
	}

	public void setPacoteServico(String pacoteServico) {
		this.pacoteServico = pacoteServico;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}
	
}
