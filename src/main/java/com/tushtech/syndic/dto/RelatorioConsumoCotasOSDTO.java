package com.tushtech.syndic.dto;

import java.math.BigDecimal;

public class RelatorioConsumoCotasOSDTO {
	
	private String nomePacoteServico;
	private String nome;
	private int quantidadeVisitas;
	private int osAberta;
	private int codigo;
	private BigDecimal valorMaodeObra;
	private BigDecimal valorMaterial;
	private int mes;
	private int ano;
	
	
	
	
	public RelatorioConsumoCotasOSDTO(String nome, String nomePacoteServico, int quantidadeVisitas, int osAberta,
			int codigo,BigDecimal valorMaodeObra,BigDecimal valorMaterial, int mes, int ano) {
		this.nomePacoteServico = nomePacoteServico;
		this.nome = nome;
		this.quantidadeVisitas = quantidadeVisitas;
		this.osAberta = osAberta;
		this.codigo = codigo;
		this.valorMaodeObra = valorMaodeObra;
		this.valorMaterial = valorMaterial;
		this.mes = mes;
		this.ano = ano;
	}

	public RelatorioConsumoCotasOSDTO() {
		// TODO Auto-generated constructor stub
	}


	public String getNomePacoteServico() {
		return nomePacoteServico;
	}

	public void setNomePacoteServico(String nomePacoteServico) {
		this.nomePacoteServico = nomePacoteServico;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQuantidadeVisitas() {
		return quantidadeVisitas;
	}

	public void setQuantidadeVisitas(int quantidadeVisitas) {
		this.quantidadeVisitas = quantidadeVisitas;
	}

	public int getOsAberta() {
		return osAberta;
	}

	public void setOsAberta(int osAberta) {
		this.osAberta = osAberta;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getValorMaodeObra() {
		return valorMaodeObra;
	}

	public void setValorMaodeObra(BigDecimal valorMaodeObra) {
		this.valorMaodeObra = valorMaodeObra;
	}

	public BigDecimal getValorMaterial() {
		return valorMaterial;
	}

	public void setValorMaterial(BigDecimal valorMaterial) {
		this.valorMaterial = valorMaterial;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
	

}

