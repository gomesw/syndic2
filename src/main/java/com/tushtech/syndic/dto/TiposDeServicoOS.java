package com.tushtech.syndic.dto;

public class TiposDeServicoOS {
	
	private int tipoServicoId;
	private String nome;
	private int quantidade;
	private String cor;
	
	public TiposDeServicoOS(int tipoServicoId, String nome, int quantidade,String cor) {
		super();
		this.tipoServicoId = tipoServicoId;
		this.nome = nome;
		this.quantidade = quantidade;
		this.cor=cor;
	}
	
	public int getTipoServicoId() {
		return tipoServicoId;
	}

	public void setTipoServicoId(int tipoServicoId) {
		this.tipoServicoId = tipoServicoId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}
	
	
	
}
