package com.tushtech.syndic.dto;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.PessoaFisica;

public class UsuarioDTO {
	
	PessoaFisica pessoaFisica;
	Condominio condominio;
	
	
	public UsuarioDTO(PessoaFisica pessoaFisica, Condominio condominio) {
		super();
		this.pessoaFisica = pessoaFisica;
		this.condominio = condominio;
	}
	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}
	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}
	public Condominio getCondominio() {
		return condominio;
	}
	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	} 
	
	
	
	

}
