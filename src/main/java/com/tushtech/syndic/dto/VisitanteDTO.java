package com.tushtech.syndic.dto;

public class VisitanteDTO {
	
	private Integer id;
	private String nome;
	private String documento;
	private String unidade;
	private String tipopessoa;
	private Integer codigoPessoaFisicaUnidade;
	private Integer codigoUnidade;
	
	public VisitanteDTO(){
		
	}

	public VisitanteDTO(Integer id, String nome, String documento, String unidade, String tipopessoa,Integer codigoPessoaFisicaUnidade,Integer codigoUnidade) {
		super();
		this.id = id;
		this.nome = nome;
		this.documento = documento;
		this.unidade = unidade;
		this.tipopessoa = tipopessoa;
		this.codigoPessoaFisicaUnidade = codigoPessoaFisicaUnidade;
		this.codigoUnidade = codigoUnidade;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}


	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public String getTipopessoa() {
		return tipopessoa;
	}

	public void setTipopessoa(String tipopessoa) {
		this.tipopessoa = tipopessoa;
	}

	public Integer getCodigoPessoaFisicaUnidade() {
		return codigoPessoaFisicaUnidade;
	}

	public void setCodigoPessoaFisicaUnidade(Integer codigoPessoaFisicaUnidade) {
		this.codigoPessoaFisicaUnidade = codigoPessoaFisicaUnidade;
	}

	public Integer getCodigoUnidade() {
		return codigoUnidade;
	}

	public void setCodigoUnidade(Integer codigoUnidade) {
		this.codigoUnidade = codigoUnidade;
	}

	

}
