package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the anexoCondominio database table.
 * 
 */
@Entity
@Table(name="ANEXOCONDOMINIO")
public class AnexoCondominio implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACO_CODIGO",unique=true, nullable=false)
	private int id;
	
	@Column(name="ACO_NOME")
	private String nome;
	
	@Column(name="ACO_CONTENTTYPE")
	private String contentType;

	@Column(name="ACO_ATIVO")
	private int ativo;
	
	@ManyToOne
	@JoinColumn(name="CON_CODIGO")
	private Condominio condominio;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public String getTipoReduzido(){
		
		String arrayNome[] = this.nome.split("\\."); 
		
		return arrayNome[1];
		
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnexoCondominio other = (AnexoCondominio) obj;
		if (id != other.id)
			return false;
		return true;
	}

	
}