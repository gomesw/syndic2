package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the animais database table.
 * 
 */
@Entity
@Table(name="ANIMAL")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Version
	private int version;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ANI_CODIGO",unique=true, nullable=false)
	private int id; 
	
	
	@NotBlank(message="Nome do Animal é Obrigatório")
	@Column(name="ANI_NOME")
	private String nome;
	
	@NotBlank(message="Descrição é Obrigatório")
	@Column(name="ANI_DESCRICAO")
	private String descricao;

	@ManyToOne
	@JoinColumn(name="COA_CODIGO")
	 @NotNull(message= "Cor Predominante é Obrigatório")
	private CorAnimal corAnimal;
	
	@ManyToOne
	@JoinColumn(name="RAC_CODIGO")
    @NotNull(message= "Raça é Obrigatório")
	private Raca raca;
	
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="UNI_CODIGO", referencedColumnName = "UNI_CODIGO")
	private Unidade unidade;
	

	@OneToMany(mappedBy="animal")
	@JsonIgnore
	private List<FotoAnimal> listFotoAnimal;

	
	public boolean isNovo() {
		return this.id==0;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}



	public CorAnimal getCorAnimal() {
		return corAnimal;
	}



	public void setCorAnimal(CorAnimal corAnimal) {
		this.corAnimal = corAnimal;
	}



	public Raca getRaca() {
		return raca;
	}



	public void setRaca(Raca raca) {
		this.raca = raca;
	}



	public List<FotoAnimal> getListFotoAnimal() {
		return listFotoAnimal;
	}



	public void setListFotoAnimal(List<FotoAnimal> listFotoAnimal) {
		this.listFotoAnimal = listFotoAnimal;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animal other = (Animal) obj;
		if (id != other.id)
			return false;
		return true;
	}    
	
	

}