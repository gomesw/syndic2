package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the animais database table.
 * 
 */
@Entity
@Table(name="AREACOMUM")
public class AreaComum implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ARC_CODIGO",unique=true, nullable=false)
	private int id; 
	
	@Column(name="ARC_NOME")
	private String nome;
	
	@Column(name="ARC_VALOR")
	private Double valor;
	
	@Column(name="ARC_QTDMAXCONVIDADOS")
	private Integer qtdConvidados;
	
	@Column(name="ARC_STATUS")
	private String status;
	
	@Column(name="ARC_QTDMAXRESERVAS")
	private Integer qtdReservas;
	
	//bi-directional many-to-one association to PessoafisicaUnidade
	@OneToMany(mappedBy="areaComum")
	private List<AreaComumFuncionamento> listAreaComumFuncionamento;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Integer getQtdConvidados() {
		return qtdConvidados;
	}

	public void setQtdConvidados(Integer qtdConvidados) {
		this.qtdConvidados = qtdConvidados;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getQtdReservas() {
		return qtdReservas;
	}

	public void setQtdReservas(Integer qtdReservas) {
		this.qtdReservas = qtdReservas;
	}
	
	public List<AreaComumFuncionamento> getListAreaComumFuncionamento() {
		return listAreaComumFuncionamento;
	}

	public void setListAreaComumFuncionamento(List<AreaComumFuncionamento> listAreaComumFuncionamento) {
		this.listAreaComumFuncionamento = listAreaComumFuncionamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AreaComum other = (AreaComum) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}