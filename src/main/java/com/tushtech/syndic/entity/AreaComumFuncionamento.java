package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the animais database table.
 * 
 */
@Entity
@Table(name="AREACOMUMFUNCIONAMENTO")
public class AreaComumFuncionamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ACF_CODIGO",unique=true, nullable=false)
	private int id; 
	
	@Column(name="ACF_DTINICIO")
	@Temporal(TemporalType.TIMESTAMP)
//	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataInicio;
	
	@Column(name="ACF_DTFIM")
	@Temporal(TemporalType.TIMESTAMP)
//	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFim;
	
	@Column(name="ACF_DIASEMANA")
	private String diaSemana;

	//bi-directional many-to-one association to Unidade
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="ARC_CODIGO", referencedColumnName = "ARC_CODIGO")
	private AreaComum areaComum;
	
	//bi-directional many-to-one association to PessoafisicaUnidade
	@OneToMany(mappedBy="areaComumFuncionamento")
	private List<Reserva> listReserva;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}

	public AreaComum getAreaComum() {
		return areaComum;
	}

	public void setAreaComum(AreaComum areaComum) {
		this.areaComum = areaComum;
	}

	public List<Reserva> getListReserva() {
		return listReserva;
	}

	public void setListReserva(List<Reserva> listReserva) {
		this.listReserva = listReserva;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AreaComumFuncionamento other = (AreaComumFuncionamento) obj;
		if (id != other.id)
			return false;
		return true;
	}

}