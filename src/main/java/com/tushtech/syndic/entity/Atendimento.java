package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;

import com.tushtech.syndic.entity.enumeration.CoresBootstrap;


/**
 * The persistent class for the pessoafisica database table.
 * 
 */
@Entity
@Table(name="ATENDIMENTO")
public class Atendimento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ATE_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="A descrição é obrigatória")
	@Column(name="ATE_DESCRICAO")
	private String descricao;

	@Column(name="ATE_DTCADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
//	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataCadastro;

	//bi-directional many-to-one association to Funcao
	@ManyToOne
	@JoinColumn(name="TAT_CODIGO")
	private TipoAtendimento tipoAtendimento;
	
	//bi-directional many-to-one association to Unidade
	@ManyToOne
	@JoinColumn(name="UNI_CODIGO")
	private Unidade unidade;

	//bi-directional many-to-one association to Pessoafisica
	@ManyToOne(optional = false, fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="PEF_CODIGO", nullable=false)
	private PessoaFisica pessoaFisica;
	
	//bi-directional many-to-one association to Pessoafisica
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="CON_CODIGO", nullable=false)
	private Condominio condominio;
	
	//bi-directional many-to-one association to PfCondominio
	@OneToMany(mappedBy="atendimento",fetch = FetchType.LAZY)
	private List<AtendimentoStatus> listAtendimentoStatus;
	
	@OneToMany(mappedBy="atendimento",fetch = FetchType.LAZY)
	private List<AtendimentoResposta> listAtendimentoResposta;
	
	@OneToOne(mappedBy = "atendimento", fetch = FetchType.LAZY)
	private OrdemServico ordemServico;

	@Column(name="ATE_NRPROTOCOLO")
	private int protocolo;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	
	public TipoAtendimento getTipoAtendimento() {
		return tipoAtendimento;
	}

	public void setTipoAtendimento(TipoAtendimento tipoAtendimento) {
		this.tipoAtendimento = tipoAtendimento;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}
	
	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}
	
	public List<AtendimentoStatus> getListAtendimentoStatus() {
		return listAtendimentoStatus;
	}

	public void setListAtendimentoStatus(List<AtendimentoStatus> listAtendimentoStatus) {
		this.listAtendimentoStatus = listAtendimentoStatus;
	}

	public List<AtendimentoResposta> getListAtendimentoResposta() {
		return listAtendimentoResposta;
	}

	public void setListAtendimentoResposta(List<AtendimentoResposta> listAtendimentoResposta) {
		this.listAtendimentoResposta = listAtendimentoResposta;
	}	
	
	public OrdemServico getOrdemServico() {
		return ordemServico;
	}

	public void setOrdemServico(OrdemServico ordemServico) {
		this.ordemServico = ordemServico;
	}

	public int getProtocolo() {
		return protocolo;
	}
	
	
	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public String getProtocoloZeros() {
		
		String retorno = completarZero(6,this.protocolo+"");
		
		LocalDate ld = toLocalDate(this.dataCadastro);
		
		return retorno+"/"+ld.getYear();
	}

	public void setProtocolo(int protocolo) {
		this.protocolo = protocolo;
	}

	public AtendimentoStatus getUltimoStatusAtendimento(){
		
		AtendimentoStatus atendimentoStatusRetorno = new AtendimentoStatus();
		
		for (AtendimentoStatus atendimentoStatus : this.listAtendimentoStatus) {
			if(atendimentoStatus.getAtivo() == 1){
				atendimentoStatusRetorno = atendimentoStatus;
			}
		}
		
		return atendimentoStatusRetorno;
	}	

	
	public String getCor() {
		
		return CoresBootstrap.COR.getPorNumero(this.tipoAtendimento.getPrioridade());
		
	}
	
	@Override
	public String toString() {
		return "Atendimento [id=" + id + ", " + (descricao != null ? "descricao=" + descricao + ", " : "")
				+ (dataCadastro != null ? "dataCadastro=" + dataCadastro + ", " : "")
				+ (tipoAtendimento != null ? "tipoAtendimento=" + tipoAtendimento + ", " : "")
				+ (pessoaFisica != null ? "pessoaFisica=" + pessoaFisica : "") + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atendimento other = (Atendimento) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	private String  completarZero(int tamanho, String valor)
	{
		String retorno = valor;
		int len = retorno.length();
		// Calculamos o total de caracteres possiveis
		for (int n = 0; n < tamanho-len; n++) {
			// Concatenamos um dos caracteres na variavel $retorno
			retorno = '0'+retorno;
		}
		return retorno;

	}
	
	private static LocalDate toLocalDate(Date d) {
		Instant instant = Instant.ofEpochMilli(d.getTime());
		LocalDate localDate = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
		return localDate;
	}
		
}