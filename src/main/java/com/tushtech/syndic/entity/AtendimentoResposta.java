package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotBlank;


/**
 * The persistent class for the pessoafisica database table.
 * 
 */
@Entity
@Table(name="ATENDIMENTORESPOSTA")
public class AtendimentoResposta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ARE_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="A mensagem é obrigatória")
	@Column(name="ARE_RESPOSTA")
	private String resposta;

	@Column(name="ARE_DTRESPOSTA")
	@Temporal(TemporalType.TIMESTAMP)
//	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataResposta;
	
	//bi-directional many-to-one association to Funcao
	@ManyToOne
	@JoinColumn(name="ATE_CODIGO")
	private Atendimento atendimento;

	//bi-directional many-to-one association to Pessoafisica
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="PEF_CODIGO", nullable=false)
	private PessoaFisica pessoaFisica;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ARE_SUBORDINACAO",referencedColumnName = "ARE_CODIGO")
	private AtendimentoResposta atendimentoRespostaSubordinacao;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "atendimentoRespostaSubordinacao", fetch = FetchType.LAZY)
    private List<AtendimentoResposta> respostasFilhas;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	public Date getDataResposta() {
		return dataResposta;
	}

	public void setDataResposta(Date dataResposta) {
		this.dataResposta = dataResposta;
	}

	public Atendimento getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public AtendimentoResposta getAtendimentoRespostaSubordinacao() {
		return atendimentoRespostaSubordinacao;
	}

	public void setAtendimentoRespostaSubordinacao(AtendimentoResposta atendimentoRespostaSubordinacao) {
		this.atendimentoRespostaSubordinacao = atendimentoRespostaSubordinacao;
	}


	
	public List<AtendimentoResposta> getRespostasFilhas() {
		
		List<AtendimentoResposta> listaRetorno = new ArrayList<AtendimentoResposta>();
		
		for (AtendimentoResposta atendimentoResposta: respostasFilhas) {
				listaRetorno.add(atendimentoResposta);
		}
		
		return listaRetorno;
	}

	public void setRespostasFilhas(List<AtendimentoResposta> respostasFilhas) {
		this.respostasFilhas = respostasFilhas;
	}

	public AtendimentoResposta addRespsotasFilha(AtendimentoResposta atendimentoResposta) {
		getRespostasFilhas().add(atendimentoResposta);
		atendimentoResposta.setAtendimentoRespostaSubordinacao(this);
		return atendimentoResposta;
	}

	public AtendimentoResposta removeRespostaFilha(AtendimentoResposta atendimentoResposta) {
		getRespostasFilhas().remove(atendimentoResposta);
		atendimentoResposta.setAtendimentoRespostaSubordinacao(null);
		return atendimentoResposta;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtendimentoResposta other = (AtendimentoResposta) obj;
		if (id != other.id)
			return false;
		return true;
	}		
}