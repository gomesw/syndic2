package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="ATENDIMENTO_STATUS")
public class AtendimentoStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ATS_CODIGO",unique=true, nullable=false)
	private int id;

	//bi-directional many-to-one association to Funcao
	@ManyToOne
	@JoinColumn(name="STA_CODIGO")
	private StatusAtendimento statusChamado;

	//bi-directional many-to-one association to Recurso
	@JoinColumn(name="ATE_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Atendimento atendimento;

	@Column(name="ATS_DTMUDANCA")
	@Temporal(TemporalType.TIMESTAMP)
//	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataMudanca;
	
	@Column(name="ATS_ATIVO")
	private int ativo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public StatusAtendimento getStatusChamado() {
		return statusChamado;
	}

	public void setStatusChamado(StatusAtendimento statusChamado) {
		this.statusChamado = statusChamado;
	}

	public Atendimento getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

	public Date getDataMudanca() {
		return dataMudanca;
	}

	public void setDataMudanca(Date dataMudanca) {
		this.dataMudanca = dataMudanca;
	}	

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtendimentoStatus other = (AtendimentoStatus) obj;
		if (id != other.id)
			return false;
		return true;
	}
}