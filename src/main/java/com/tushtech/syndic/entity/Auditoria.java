package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the auditoria database table.
 * 
 */
@Entity
@Table(name="AUDITORIA")
//@NamedQuery(name="Auditoria.findAll", query="SELECT a FROM Auditoria a")
public class Auditoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="AUD_CODIGO")
	private int audCodigo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="AUD_DTFIMACESSO")
	private Date audDtfimacesso;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="AUD_DTINICIOACESSO")
	private Date audDtinicioacesso;

	//bi-directional many-to-one association to Perfil
	@ManyToOne
	@JoinColumn(name="PER_CODIGO")
	private Perfil perfil;

	public Auditoria() {
	}

	public int getAudCodigo() {
		return this.audCodigo;
	}

	public void setAudCodigo(int audCodigo) {
		this.audCodigo = audCodigo;
	}

	public Date getAudDtfimacesso() {
		return this.audDtfimacesso;
	}

	public void setAudDtfimacesso(Date audDtfimacesso) {
		this.audDtfimacesso = audDtfimacesso;
	}

	public Date getAudDtinicioacesso() {
		return this.audDtinicioacesso;
	}

	public void setAudDtinicioacesso(Date audDtinicioacesso) {
		this.audDtinicioacesso = audDtinicioacesso;
	}

	public Perfil getPerfil() {
		return this.perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

}