package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the avaliacaoOSPerguntaAvaliacao database table.
 * 
 */
@Entity
@Table(name="AVALIACAOOS_PERGUNTAAVALIACAO")
public class AvaliacaoOSPerguntaAvaliacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="APA_CODIGO",unique=true, nullable=false)
	private int id;

	//bi-directional many-to-one association to AvaliacaoOrdemServico
	@ManyToOne
	@JoinColumn(name="AOS_CODIGO")
	private AvaliacaoOrdemServico avaliacaoOrdemServico;
	
	//bi-directional many-to-one association to PerguntaAvaliacao
	@ManyToOne
	@JoinColumn(name="PAV_CODIGO")
	private PerguntaAvaliacao perguntaAvaliacao;

	@Column(name="APA_PONTUACAO")
	private Integer pontuacao;
	
		
	public boolean isNovo(){
		return this.id==0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AvaliacaoOrdemServico getAvaliacaoOrdemServico() {
		return avaliacaoOrdemServico;
	}

	public void setAvaliacaoOrdemServico(AvaliacaoOrdemServico avaliacaoOrdemServico) {
		this.avaliacaoOrdemServico = avaliacaoOrdemServico;
	}

	public PerguntaAvaliacao getPerguntaAvaliacao() {
		return perguntaAvaliacao;
	}

	public void setPerguntaAvaliacao(PerguntaAvaliacao perguntaAvaliacao) {
		this.perguntaAvaliacao = perguntaAvaliacao;
	}

	public Integer getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}
	
	public String getPontuacaoExtenso(){
		String retorno = "";
		
		if(this.perguntaAvaliacao.getObjetiva() == 1){
			if(this.pontuacao == 1){
				retorno = "Sim";
			}else{
				retorno = "Não";
			}
		}else if (this.perguntaAvaliacao.getObjetiva() == 0) {
			switch (this.pontuacao) {
				case 1: retorno = "Muito Ruim"; break;
				case 2: retorno = "Ruim"; break;
				case 3: retorno = "Regular"; break;
				case 4: retorno = "Bom"; break;
				case 5: retorno = "Muito Bom"; break;
			}
		}
		
		return retorno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoOSPerguntaAvaliacao other = (AvaliacaoOSPerguntaAvaliacao) obj;
		if (id != other.id)
			return false;
		return true;
	}



}