package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the avaliacaoOrdemServico database table.
 * 
 */
@Entity
@Table(name="AVALIACAOORDEMSERVICO")
public class AvaliacaoOrdemServico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="AOS_CODIGO",unique=true, nullable=false)
	private int id;

	//bi-directional many-to-one association to OrdemServico
	@ManyToOne
	@JoinColumn(name="ORD_CODIGO")
	private OrdemServico ordemServico;
	
	//bi-directional many-to-one association to PessoafisicaAvaliador
	@ManyToOne
	@JoinColumn(name="PEF_CODIGOAVALIADOR")
	private PessoaFisica avaliador;

	//bi-directional many-to-one association to PessoafisicaAvaliado
	@ManyToOne
	@JoinColumn(name="PEF_CODIGOAVALIADO")
	private PessoaFisica avaliado;
	
	@Column(name="AOS_DTAVALIACAO")
	//@Temporal(TemporalType.DATE)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataAvaliacao;
	
	@Column(name="AOS_OBSERVACAO")
	private String observacao;
	
	@OneToMany(mappedBy = "avaliacaoOrdemServico", fetch = FetchType.LAZY)
	private List<AvaliacaoOSPerguntaAvaliacao> listAvaliacaoOSPerguntaAvaliacao;	
		
	public boolean isNovo(){
		return this.id==0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public OrdemServico getOrdemServico() {
		return ordemServico;
	}

	public void setOrdemServico(OrdemServico ordemServico) {
		this.ordemServico = ordemServico;
	}

	public PessoaFisica getAvaliador() {
		return avaliador;
	}

	public void setAvaliador(PessoaFisica avaliador) {
		this.avaliador = avaliador;
	}

	public PessoaFisica getAvaliado() {
		return avaliado;
	}

	public void setAvaliado(PessoaFisica avaliado) {
		this.avaliado = avaliado;
	}

	public Date getDataAvaliacao() {
		return dataAvaliacao;
	}

	public void setDataAvaliacao(Date dataAvaliacao) {
		this.dataAvaliacao = dataAvaliacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public List<AvaliacaoOSPerguntaAvaliacao> getListAvaliacaoOSPerguntaAvaliacao() {
		return listAvaliacaoOSPerguntaAvaliacao;
	}

	public void setListAvaliacaoOSPerguntaAvaliacao(List<AvaliacaoOSPerguntaAvaliacao> listAvaliacaoOSPerguntaAvaliacao) {
		this.listAvaliacaoOSPerguntaAvaliacao = listAvaliacaoOSPerguntaAvaliacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AvaliacaoOrdemServico other = (AvaliacaoOrdemServico) obj;
		if (id != other.id)
			return false;
		return true;
	}



}