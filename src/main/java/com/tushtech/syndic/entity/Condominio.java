package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotBlank;

import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaCondominioEnum;


/**
 * The persistent class for the condominio database table.
 * 
 */
@Entity
@Table(name="CONDOMINIO")
//@NamedQuery(name="Condominio.findAll", query="SELECT c FROM Condominio c")
public class Condominio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CON_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="CON_LOGO")
	private String logo;

	@Column(name = "CON_NOMEFOTO")
	private String nomeFoto;

	@Column(name = "CON_CONTENTTYPE")
	private String contentType;

	@Column(name = "CON_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	@Column(name = "CON_ADIMPLENTE")
	private boolean adimplente;

	@NotBlank(message="Endereço é obrigatório")
	@Column(name = "CON_ENDERECO")
	private String endereco;

	
	@Column(name = "CON_TELEFONE_1")
	private String telefone1;
	
	@Column(name = "CON_TELEFONE_2")
	private String telefone2;
	
	@Column(name = "CON_TELEFONE_3")
	private String telefone3;
	
	@Column(name = "CON_TELEFONE_4")
	private String telefone4;

	@Column(name = "CON_EMAIL_1")
	private String email1;
	
	@Column(name = "CON_EMAIL_2")
	private String email2;
	
	@OneToMany(mappedBy="condominio",fetch = FetchType.LAZY)
	private List<EnderecoUnidade> enderecoUnidade;
	
	@OneToMany(mappedBy="condominio",fetch = FetchType.LAZY)
	private List<Atendimento> atendimentos;
	
	@OneToMany(mappedBy="condominio",fetch = FetchType.LAZY)
	private List<Unidade> unidades;

	@OneToMany(mappedBy="condominio",fetch = FetchType.LAZY)
	private List<TipoEnderecoUnidade> tipoEnderecoUnidade;
	
	@OneToMany(mappedBy="condominio",fetch = FetchType.LAZY)
	private List<Imagem> imagem;

	@Transient
	private String novaFoto;

	@Transient
	private String urlFoto;	
	
	
	@Transient
	private boolean isSindico;	
	

	@OneToMany(mappedBy="condominio", fetch = FetchType.LAZY)
	private List<PerfilCondominio> listPerfilCondominio;

	@JoinColumn(name="PEJ_CODIGO")
	@ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private PessoaJuridica pessoaJuridica;

	//bi-directional many-to-one association to PfCondominio
	@OneToMany(mappedBy="condominio")
	private List<PessoaFisicaCondominio> pessoaFisicaCondominio;
	
	@OneToMany(mappedBy="condominio")
	private List<CondominioPacoteServico> listCondominioPacoteServico;

	public boolean isNovo() {  
		return id == 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}


	public Integer getAtivo() {
		return ativo;
	}
	

	public boolean isAdimplente() {
		return adimplente;
	}

	public void setAdimplente(boolean adimplente) {
		this.adimplente = adimplente;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}
	
	

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public List<PessoaFisicaCondominio> getPessoaFisicaCondominio() {
		return pessoaFisicaCondominio;
	}

	public void setPessoaFisicaCondominio(List<PessoaFisicaCondominio> pessoaFisicaCondominio) {
		this.pessoaFisicaCondominio = pessoaFisicaCondominio;
	}

	public List<PerfilCondominio> getListPerfilCondominio() {
		return listPerfilCondominio;
	}

	public void setListPerfilCondominio(List<PerfilCondominio> listPerfilCondominio) {
		this.listPerfilCondominio = listPerfilCondominio;
	}

	public List<CondominioPacoteServico> getListCondominioPacoteServico() {
		return listCondominioPacoteServico;
	}

	public void setListCondominioPacoteServico(List<CondominioPacoteServico> listCondominioPacoteServico) {
		this.listCondominioPacoteServico = listCondominioPacoteServico;
	}

	public List<Atendimento> getAtendimentos() {
		return atendimentos;
	}

	public void setAtendimentos(List<Atendimento> atendimentos) {
		this.atendimentos = atendimentos;
	}

	public void setUnidades(List<Unidade> unidades) {
		this.unidades = unidades;
	}

	public List<EnderecoUnidade> getEnderecoUnidade() {
		return enderecoUnidade;
	}

	public void setEnderecoUnidade(List<EnderecoUnidade> enderecoUnidade) {
		this.enderecoUnidade = enderecoUnidade;
	}

	public List<TipoEnderecoUnidade> getTipoEnderecoUnidade() {
		return tipoEnderecoUnidade;
	}

	public void setTipoEnderecoUnidade(List<TipoEnderecoUnidade> tipoEnderecoUnidade) {
		this.tipoEnderecoUnidade = tipoEnderecoUnidade;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

		
	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public CondominioPacoteServico getCondominioPacoteServicoAtivo(){
		
		CondominioPacoteServico condominioPacoteServico = new CondominioPacoteServico();
		
		
		if(listCondominioPacoteServico!=null) {
			for (CondominioPacoteServico condominioPacoteServico2 : this.listCondominioPacoteServico) {

				if((condominioPacoteServico2.getDataFim() == null || condominioPacoteServico2.getDataFim().after(Date.valueOf(LocalDate.now()))) && condominioPacoteServico2.getAtivo() == 1){
					condominioPacoteServico = condominioPacoteServico2;
				}
			}
		}
		return condominioPacoteServico;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Condominio other = (Condominio) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getNomeFoto() {
		return nomeFoto;
	}

	public void setNomeFoto(String nomeFoto) {
		this.nomeFoto = nomeFoto;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getNovaFoto() {
		return novaFoto;
	}

	public void setNovaFoto(String novaFoto) {
		this.novaFoto = novaFoto;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}
	
	
	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getTelefone3() {
		return telefone3;
	}

	public void setTelefone3(String telefone3) {
		this.telefone3 = telefone3;
	}

	public String getTelefone4() {
		return telefone4;
	}

	public void setTelefone4(String telefone4) {
		this.telefone4 = telefone4;
	}
	
	
	public String getTelefones() {
		
		String retorno = "";
		
		retorno += (telefone1!=null?telefone1:"")+" / "+(telefone2!=null?telefone2:"");
		
		return retorno;
		
	}
	
	public String getTodosTelefones() {
		
		String retorno = "";
		
		retorno += (telefone1!=null?telefone1:"")+" / "+(telefone2!=null?telefone2:"")+" / "+(telefone3!=null?telefone3:"")+" / "+(telefone4!=null?telefone4:"");
		
		return retorno;
		
	}
	

	public List<Imagem> getImagem() {
		return imagem;
	}

	public void setImagem(List<Imagem> imagem) {
		this.imagem = imagem;
	}

	/**
	 * ENCONTRA O SINDICO ATUAL DE UM CONDOMINIO
	 * @return PessoaFisica
	 * 
	 */
	public PessoaFisica sidicoAtual() {

		PessoaFisica sindico = new PessoaFisica();

		for (PessoaFisicaCondominio pfcon : this.pessoaFisicaCondominio) {
			if(pfcon.getTipoPessoaFisicaCondominio().getId()==TipoPessoaFisicaCondominioEnum.SINDICO.getId()
					&& (pfcon.getDataFim()==null || pfcon.getDataFim().after(Date.valueOf(LocalDate.now())))){//se tipo == sindico
				sindico = pfcon.getPessoaFisica();
			}

		}
		if(sindico.getNome()==null) {
			sindico.setNome("Nenhum Sindico Cadastrado");
			sindico.setId(0);
		}

		return sindico;

	}
	
	
	/**
	 * ENCONTRA O SINDICO ATUAL DE UM CONDOMINIO
	 * @return PessoaFisicaCondominio
	 */
	public PessoaFisicaCondominio pfCondominioSindicoAtual() { 
		PessoaFisicaCondominio sindico = new PessoaFisicaCondominio();
		sindico.setId(0);
		for (PessoaFisicaCondominio pfcon : pessoaFisicaCondominio) {
			
			
			if(pfcon.getTipoPessoaFisicaCondominio().getId()==TipoPessoaFisicaCondominioEnum.SINDICO.getId() && pfcon.getDataFim()==null){//se tipo == sindico
				sindico = pfcon;
			}else if (pfcon.getDataFim()!=null && pfcon.getDataFim().after(Date.valueOf(LocalDate.now()))) {
				sindico = pfcon;
			}
		}
		return sindico;
	}
	
	public List<PessoaFisicaUnidade> moradoresCondominio(){
		
		List<PessoaFisicaUnidade> listMoradores = new ArrayList<>();
		
		for (EnderecoUnidade endereco : this.enderecoUnidade) {
			for (Unidade unidade : endereco.getUnidade()) {
				for (PessoaFisicaUnidade pfunidade : unidade.getListaPessoasUnidade()) {
					listMoradores.add(pfunidade);
				}
			}
		}
		return  listMoradores;
	}
	
	
	public List<PessoaFisicaCondominio> sindicosCondominio(){
		
		List<PessoaFisicaCondominio> listSindicos = new ArrayList<>();
		
			for (PessoaFisicaCondominio pfcon : pessoaFisicaCondominio) {
			
			if(pfcon.getTipoPessoaFisicaCondominio().getId()==TipoPessoaFisicaCondominioEnum.SINDICO.getId()
					){//se tipo == sindico
				listSindicos.add(pfcon);
			}
				}
		return  listSindicos;
	}
	
	
	
	//return veiculos
	public List<PessoaFisicaVeiculo> veiculosCondominio(){
		
		List<PessoaFisicaVeiculo> listVeiculos = new ArrayList<>();
		
		for (EnderecoUnidade endereco : this.enderecoUnidade) {
			for (Unidade unidade : endereco.getUnidade()) {
				for (PessoaFisicaVeiculo pfVeiculo : unidade.getListaVeiculosUnidade()) {
					listVeiculos.add(pfVeiculo);
				}
			}
		}
		return  listVeiculos;
	}
	
	
	//return veiculos
		public List<Animal> animaisCondominio(){
			
			List<Animal> listAnimais = new ArrayList<>();
			
			for (EnderecoUnidade endereco : this.enderecoUnidade) {
				for (Unidade unidade : endereco.getUnidade()) {
					for (Animal animal : unidade.getAnimais()) {
						listAnimais.add(animal);
					}
			}
			}
			return  listAnimais;
		}
	
	
	public List<Unidade> getUnidades(){
		List<Unidade> listUnidade = new ArrayList<>();
		
		if(this.enderecoUnidade != null){
			for (EnderecoUnidade endereco : this.enderecoUnidade) {
				for (Unidade unidade : endereco.getUnidade()) {
					listUnidade.add(unidade);
				}
			}
		}
		return  listUnidade;
		
	}
	


	public boolean isSindico() {
		boolean retorno = false;
		
		if(pessoaFisicaCondominio!=null) {
		
		for (PessoaFisicaCondominio pfcon : pessoaFisicaCondominio) {
			
			if(pfcon.getTipoPessoaFisicaCondominio().getId()==TipoPessoaFisicaCondominioEnum.SINDICO.getId()
					&& (pfcon.getDataFim()==null  || pfcon.getDataFim().after(Date.valueOf(LocalDate.now())))){//se tipo == sindico
				retorno = true;
			}
		}
		}
		return retorno;
	}
	

	public void setSindico(boolean isSindico) {
		this.isSindico = isSindico;
	}

	public List<PessoaFisicaCondominio> funcionariosCondominio(){
		
		List<PessoaFisicaCondominio> listFuncionarios = new ArrayList<>();
		
		for (PessoaFisicaCondominio pfcon : pessoaFisicaCondominio) {
			if(pfcon.getTipoPessoaFisicaCondominio().getId()==TipoPessoaFisicaCondominioEnum.FUNCIONARIO.getId() && pfcon.getDataFim()==null) {
				listFuncionarios.add(pfcon);
			}

		}
		
		return  listFuncionarios;
		
	}


}