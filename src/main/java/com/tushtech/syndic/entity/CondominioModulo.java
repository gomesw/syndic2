package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;


/**
 * The persistent class for the condominio_modulo database table.
 * 
 */
@Entity
@Table(name="CONDOMINIO_MODULO")
public class CondominioModulo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CMO_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="CMO_DTINICIO")
	//@NotNull(message="A data de inicío é obrigatória")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date inicio;
	
	@Column(name="CMO_DTFIM")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFim;

	//bi-directional many-to-one association to Modulo
	@ManyToOne
	@JoinColumn(name="MOU_CODIGO")
	private PessoaFisica pessoaFisica;

	//bi-directional many-to-one association to Condominio
	@ManyToOne
	@JoinColumn(name="CON_CODIGO")
	private Condominio condominio;
	
	@NumberFormat(pattern = "#,###,###,###.##") 
	@Column(name="CMO_VALOR")
	private Float valor;
	
	
	public boolean isNovo() {
		return id==0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}
	
	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CondominioModulo other = (CondominioModulo) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}