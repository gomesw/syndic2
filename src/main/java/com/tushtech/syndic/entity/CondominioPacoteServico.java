package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the condominio_pacoteservico database table.
 * 
 */
@Entity
@Table(name="CONDOMINIO_PACOTESERVICO")
public class CondominioPacoteServico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COP_CODIGO",unique=true, nullable=false)
	private int id;
	
	//bi-directional many-to-one association to PessoaJuridica
	@NotNull(message="Informe o Pacote de Serviço para o Condomínio")
	@ManyToOne
	@JoinColumn(name="PCS_CODIGO")
	private PacoteServico pacoteServico;
	
	//bi-directional many-to-one association to Condominio
	@ManyToOne
	@JoinColumn(name="CON_CODIGO")
	private Condominio condominio;

	@Column(name="COP_DTINICIO")
	//@NotNull(message="A data de inicío do exercício é obrigatório")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date inicio;
	
	@Column(name="COP_DTFIM")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFim;
	
//	ALTER TABLE `tushtec_syndic`.`condominio_pacoteservico` 
//	ADD COLUMN `COP_ATIVO` INT NOT NULL AFTER `COP_VALOR`;

	@Column(name = "COP_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

	public boolean isNovo() {
		return id==0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	
	
	public PacoteServico getPacoteServico() {
		return pacoteServico;
	}

	public void setPacoteServico(PacoteServico pacoteServico) {
		this.pacoteServico = pacoteServico;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CondominioPacoteServico other = (CondominioPacoteServico) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}