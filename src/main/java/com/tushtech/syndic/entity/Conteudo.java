package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the conteudo database table.
 * 
 */
@Entity
@Table(name="CONTEUDO")
//@NamedQuery(name="Conteudo.findAll", query="SELECT c FROM Conteudo c")
public class Conteudo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COT_CODIGO",unique=true, nullable=false)
	private int id;

	@Temporal(TemporalType.DATE)
	@Column(name="COT_DTFIM")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dtfim;

	@Temporal(TemporalType.DATE)
	@Column(name="COT_DTINICIO")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dtinicio;

	@Lob
	@Column(name="COT_TEXTO")
	@NotBlank(message="Descrição é Obrigatória.")
	private String texto;

	@Column(name="COT_TITULO")
	@NotBlank(message="Título é Obrigatório.")
	private String titulo;

	//bi-directional many-to-one association to TipoConteudo
	@ManyToOne
	@JoinColumn(name="TIC_CODIGO")
	@NotNull(message="Tipo de Conteúdo é Obrigatório.")
	private TipoConteudo tipoConteudo;

	//bi-directional many-to-one association to PessoaFisica
	@ManyToOne
	@JoinColumn(name="PEF_CODIGOUSUARIOPUBLICA")
	private PessoaFisica pessoaFisicaPublica;

	//bi-directional many-to-one association to PessoaFisica
	@ManyToOne
	@JoinColumn(name="PEF_CODIGOUSUARIORETIRA")
	private PessoaFisica pessoaFisicaRetira;

	//bi-directional many-to-one association to ConteudoImagem
	@OneToMany(mappedBy="conteudo")
	private List<ConteudoImagem> conteudoImagem;

	//bi-directional many-to-one association to TipoConteudo
	@ManyToOne
	@JoinColumn(name="CON_CODIGO")
	private Condominio condominio;	
	
	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public Conteudo() {
	}

    public boolean isNovo() {  
  		return id == 0;
  	}	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public Date getDtfim() {
		return dtfim;
	}



	public void setDtfim(Date dtfim) {
		this.dtfim = dtfim;
	}



	public Date getDtinicio() {
		return dtinicio;
	}



	public void setDtinicio(Date dtinicio) {
		this.dtinicio = dtinicio;
	}



	public String getTexto() {
		return texto;
	}



	public void setTexto(String texto) {
		this.texto = texto;
	}



	public String getTitulo() {
		return titulo;
	}



	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}



	public TipoConteudo getTipoConteudo() {
		return tipoConteudo;
	}



	public void setTipoConteudo(TipoConteudo tipoConteudo) {
		this.tipoConteudo = tipoConteudo;
	}



	public PessoaFisica getPessoaFisicaPublica() {
		return pessoaFisicaPublica;
	}



	public void setPessoaFisicaPublica(PessoaFisica pessoaFisicaPublica) {
		this.pessoaFisicaPublica = pessoaFisicaPublica;
	}



	public PessoaFisica getPessoaFisicaRetira() {
		return pessoaFisicaRetira;
	}



	public void setPessoaFisicaRetira(PessoaFisica pessoaFisicaRetira) {
		this.pessoaFisicaRetira = pessoaFisicaRetira;
	}



	public List<ConteudoImagem> getConteudoImagem() {
		return conteudoImagem;
	}



	public void setConteudoImagem(List<ConteudoImagem> conteudoImagem) {
		this.conteudoImagem = conteudoImagem;
	}



	public ConteudoImagem addConteudoImagem(ConteudoImagem conteudoImagem) {
		getConteudoImagem().add(conteudoImagem);
		conteudoImagem.setConteudo(this);

		return conteudoImagem;
	}

	public ConteudoImagem removeConteudoImagem(ConteudoImagem conteudoImagem) {
		getConteudoImagem().remove(conteudoImagem);
		conteudoImagem.setConteudo(null);

		return conteudoImagem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conteudo other = (Conteudo) obj;
		if (id != other.id)
			return false;
		return true;
	}
	

}
