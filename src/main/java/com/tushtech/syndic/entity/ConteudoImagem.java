package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the conteudo_imagem database table.
 * 
 */
@Entity
@Table(name="conteudo_imagem")
@NamedQuery(name="ConteudoImagem.findAll", query="SELECT c FROM ConteudoImagem c")
public class ConteudoImagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COI_CODIGO")
	private int coiCodigo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="COI_DTFIM")
	private Date coiDtfim;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="COI_DTINICIO")
	private Date coiDtinicio;

	//bi-directional many-to-one association to Conteudo
	@ManyToOne
	@JoinColumn(name="COT_CODIGO")
	private Conteudo conteudo;

	//bi-directional many-to-one association to Imagem
	@ManyToOne
	@JoinColumn(name="IMA_CODIGO")
	private Imagem imagem;

	
	public ConteudoImagem() {
	}

	public int getCoiCodigo() {
		return this.coiCodigo;
	}

	public void setCoiCodigo(int coiCodigo) {
		this.coiCodigo = coiCodigo;
	}

	public Date getCoiDtfim() {
		return this.coiDtfim;
	}

	public void setCoiDtfim(Date coiDtfim) {
		this.coiDtfim = coiDtfim;
	}

	public Date getCoiDtinicio() {
		return this.coiDtinicio;
	}

	public void setCoiDtinicio(Date coiDtinicio) {
		this.coiDtinicio = coiDtinicio;
	}

	public Conteudo getConteudo() {
		return this.conteudo;
	}

	public void setConteudo(Conteudo conteudo) {
		this.conteudo = conteudo;
	}

	public Imagem getImagem() {
		return this.imagem;
	}

	public void setImagem(Imagem imagem) {
		this.imagem = imagem;
	}

	
}