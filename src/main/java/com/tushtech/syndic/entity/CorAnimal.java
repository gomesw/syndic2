package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;


@Entity
@Table(name="CORANIMAL")
public class CorAnimal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COA_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe a cor")
	@Column(name="COA_NOME")
	private String nome;
	
	@Column(name = "COA_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
//	//bi-directional many-to-one association to Animal
//	@OneToMany(mappedBy="corAnimal")
//	private List<Animal> animal;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
    public boolean isNovo() {  
  		return id == 0;
  	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

//	public List<Animal> getAnimal() {
//		return animal;
//	}
//
//	public void setAnimal(List<Animal> animal) {
//		this.animal = animal;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CorAnimal other = (CorAnimal) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	


}