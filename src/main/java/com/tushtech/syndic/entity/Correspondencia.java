package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the correspondencia database table.
 * 
 */
@Entity
@Table(name="CORRESPONDENCIA")
//@NamedQuery(name="Correspondencia.findAll", query="SELECT c FROM Correspondencia c")
public class Correspondencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COR_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="COR_CODRASTREAMENTO")
	private String codigoRastreamento;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="COR_DTENTREGA")
	private Date dataEntrega;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="COR_DTRECEBIMENTO")
	private Date dataRecebimento;

	//bi-directional many-to-one association to Unidade
	@ManyToOne
	@JoinColumn(name="UNI_CODIGO")
	private Unidade unidade;

	//bi-directional many-to-one association to Tipocorrespondencia
	@ManyToOne
	@JoinColumn(name="TPC_CODIGO")
	private TipoCorrespondencia tipoCorrespondencia;

	//bi-directional many-to-one association to Pessoajuridica
	@ManyToOne
	@JoinColumn(name="PEJ_CODIGOFUNCIONARIO")
	private PessoaJuridica pessoaJuridica;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigoRastreamento() {
		return codigoRastreamento;
	}

	public void setCodigoRastreamento(String codigoRastreamento) {
		this.codigoRastreamento = codigoRastreamento;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Date getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(Date dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public TipoCorrespondencia getTipoCorrespondencia() {
		return tipoCorrespondencia;
	}

	public void setTipoCorrespondencia(TipoCorrespondencia tipoCorrespondencia) {
		this.tipoCorrespondencia = tipoCorrespondencia;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	

}