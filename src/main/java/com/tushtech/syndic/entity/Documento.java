package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the equipamento database table.
 * 
 */
@Entity
@Table(name="DOCUMENTO")
//@NamedQuery(name="Equipamento.findAll", query="SELECT e FROM Documento e")
public class Documento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="DOC_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="DOC_NOME")
	private String nome;

	//bi-directional many-to-one association to Tipodocumento
	@NotNull(message="Tipo de Documento é obrigatório")
	@ManyToOne
	@JoinColumn(name="TID_CODIGO")
	private TipoDocumento tipoDocumento;

	//bi-directional many-to-one association to Unidade
	@ManyToOne
	@JoinColumn(name="UNI_CODIGO")
	private Unidade unidade;
	
	
	@NotNull(message="Pessoa Responsável é obrigatório")
	@ManyToOne
	@JoinColumn(name="PFU_CODIGO")
	private PessoaFisicaUnidade pessoaFisicaUnidade;
	
	@Column(name = "DOC_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	@Column(name = "DOC_CONTENTTYPE")
	private String contentType;
	
	@NotEmpty(message="Observação é Obrigatório")
	@Column(name = "DOC_DESCRICAO")
	private String descricao;
	
	@Transient
	private String novoArquivo; 
	
	@Transient
	private String urlArquivo;
	
	
	@ManyToOne
	@JoinColumn(name="PEF_CODIGOCADASTRANTE")
	private PessoaFisica cadastrante;
	
	@ManyToOne
	@JoinColumn(name="PEF_CODIGOVALIDADOR")
	private PessoaFisica validador;
	
	@Column(name="DOC_DTCADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm:ss")
	private Date dataCadastro;
	
	@Column(name="DOC_DTVALIDACAO")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm:ss")
	private Date dataValidacao;
	
	
	@NotNull(message="Status é obrigatório")
	@ManyToOne
	@JoinColumn(name="STD_CODIGO")
	private StatusDocumento statusDocumento;
	
	@PrePersist
	public void setarAtivo() {
		this.ativo = 1;
		this.dataCadastro = new Date(); 
	}
	
	
	public String getTipoReduzido(){
		
		String arrayNome[] = this.nome.split("\\."); 
		
		return arrayNome[1];
		
	}
	
	public PessoaFisicaUnidade getPessoaFisicaUnidade() {
		return pessoaFisicaUnidade;
	}


	public void setPessoaFisicaUnidade(PessoaFisicaUnidade pessoaFisicaUnidade) {
		this.pessoaFisicaUnidade = pessoaFisicaUnidade;
	}


	public String getNovoArquivo() {
		return novoArquivo;
	}


	public void setNovoArquivo(String novoArquivo) {
		this.novoArquivo = novoArquivo;
	}


	public String getUrlArquivo() {
		return urlArquivo;
	}


	public void setUrlArquivo(String urlArquivo) {
		this.urlArquivo = urlArquivo;
	}


	public boolean isNovo() {
		return this.id == 0;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}


	public Unidade getUnidade() {
		return unidade;
	}


	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}


	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public PessoaFisica getCadastrante() {
		return cadastrante;
	}


	public void setCadastrante(PessoaFisica cadastrante) {
		this.cadastrante = cadastrante;
	}


	public PessoaFisica getValidador() {
		return validador;
	}


	public void setValidador(PessoaFisica validador) {
		this.validador = validador;
	}


	public Date getDataCadastro() {
		return dataCadastro;
	}


	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}


	public Date getDataValidacao() {
		return dataValidacao;
	}


	public void setDataValidacao(Date dataValidacao) {
		this.dataValidacao = dataValidacao;
	}


	public StatusDocumento getStatusDocumento() {
		return statusDocumento;
	}


	public void setStatusDocumento(StatusDocumento statusDocumento) {
		this.statusDocumento = statusDocumento;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documento other = (Documento) obj;
		if (id != other.id)
			return false;
		return true;
	}

	

	

}