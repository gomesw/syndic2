package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * The persistent class for the enderecounidade database table.
 * 
 */
@Entity
@Table(name="ENDERECOUNIDADE")
//@NamedQuery(name="Enderecounidade.findAll", query="SELECT e FROM Enderecounidade e")
public class EnderecoUnidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ENU_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe a Descrição Para o Endereço")
	@Column(name="ENU_DESCRICAO")
	private String descricao;

	
	
	
	@JoinColumn(name = "CON_CODIGO", referencedColumnName = "CON_CODIGO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope=Condominio.class)
    @JsonIdentityReference(alwaysAsId = true)
	private Condominio condominio;

	//bi-directional many-to-one association to Unidade
	@OneToMany(mappedBy="enderecoUnidade")
	private List<Unidade> unidade; 

	public EnderecoUnidade() {
	}

    public boolean isNovo() {  
  		return id == 0;
  	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public List<Unidade> getUnidade() {
		return unidade;
	}

	public void setUnidade(List<Unidade> unidade) {
		this.unidade = unidade;
	}
	
	@JsonProperty("condominio")
	public void setIdDependente(Integer idCondominio) {
		Condominio cond = new Condominio();
		cond.setId(idCondominio);
		this.condominio = cond;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnderecoUnidade other = (EnderecoUnidade) obj;
		if (id != other.id)
			return false;
		return true;
	}
	


}