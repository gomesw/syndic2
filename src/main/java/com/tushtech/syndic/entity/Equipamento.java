package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the equipamento database table.
 * 
 */
@Entity
@Table(name="EQUIPAMENTO")
//@NamedQuery(name="Equipamento.findAll", query="SELECT e FROM Equipamento e")
public class Equipamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="EQU_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="EQU_NOME")
	private String nome;

	//bi-directional many-to-one association to Tipoequipamento
	@ManyToOne
	@JoinColumn(name="TIE_CODIGO")
	private TipoEquipamento tipoEquipamento;

	//bi-directional many-to-one association to Unidade
	@ManyToOne
	@JoinColumn(name="UNI_CODIGO")
	private Unidade unidade;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoEquipamento getTipoEquipamento() {
		return tipoEquipamento;
	}

	public void setTipoEquipamento(TipoEquipamento tipoEquipamento) {
		this.tipoEquipamento = tipoEquipamento;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	

}