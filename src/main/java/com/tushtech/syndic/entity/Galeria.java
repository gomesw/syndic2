package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


/**
 * The persistent class for the galeria database table.
 * 
 */
@Entity
@NamedQuery(name="Galeria.findAll", query="SELECT g FROM Galeria g")
public class Galeria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="GAL_CODIGO")
	private int galCodigo;

	@Lob
	@Column(name="GAL_DESCRICAO")
	private String galDescricao;

	@Column(name="GAL_TITULO")
	private String galTitulo;
	
	@Column(name = "GAL_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

	//bi-directional many-to-one association to GaleriaImagem
	@OneToMany(mappedBy="galeria")
	private List<GaleriaImagem> galeriaImagems;

	public Galeria() {
	}

	public int getGalCodigo() {
		return this.galCodigo;
	}

	public void setGalCodigo(int galCodigo) {
		this.galCodigo = galCodigo;
	}

	public String getGalDescricao() {
		return this.galDescricao;
	}

	public void setGalDescricao(String galDescricao) {
		this.galDescricao = galDescricao;
	}

	public String getGalTitulo() {
		return this.galTitulo;
	}

	public void setGalTitulo(String galTitulo) {
		this.galTitulo = galTitulo;
	}

	public List<GaleriaImagem> getGaleriaImagems() {
		return this.galeriaImagems;
	}

	public void setGaleriaImagems(List<GaleriaImagem> galeriaImagems) {
		this.galeriaImagems = galeriaImagems;
	}

	public GaleriaImagem addGaleriaImagem(GaleriaImagem galeriaImagem) {
		getGaleriaImagems().add(galeriaImagem);
		galeriaImagem.setGaleria(this);

		return galeriaImagem;
	}

	public GaleriaImagem removeGaleriaImagem(GaleriaImagem galeriaImagem) {
		getGaleriaImagems().remove(galeriaImagem);
		galeriaImagem.setGaleria(null);

		return galeriaImagem;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}
	

}