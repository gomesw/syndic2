package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the galeria_imagem database table.
 * 
 */
@Entity
@Table(name="galeria_imagem")
@NamedQuery(name="GaleriaImagem.findAll", query="SELECT g FROM GaleriaImagem g")
public class GaleriaImagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="GAI_CODIGO")
	private int gaiCodigo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="GAI_DTFIM")
	private Date gaiDtfim;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="GAI_DTINICIO")
	private Date gaiDtinicio;

	//bi-directional many-to-one association to Galeria
	@ManyToOne
	@JoinColumn(name="GAL_CODIGO")
	private Galeria galeria;

	//bi-directional many-to-one association to Imagem
	@ManyToOne
	@JoinColumn(name="IMA_CODIGO")
	private Imagem imagem;

	//bi-directional many-to-one association to PessoaFisica
	@ManyToOne
	@JoinColumn(name="PEF_CODIGOPUBLICA")
	private PessoaFisica pessoafisica1;

	//bi-directional many-to-one association to PessoaFisica
	@ManyToOne
	@JoinColumn(name="PEF_CODIGORETIRA")
	private PessoaFisica pessoafisica2;

	public GaleriaImagem() {
	}

	public int getGaiCodigo() {
		return this.gaiCodigo;
	}

	public void setGaiCodigo(int gaiCodigo) {
		this.gaiCodigo = gaiCodigo;
	}

	public Date getGaiDtfim() {
		return this.gaiDtfim;
	}

	public void setGaiDtfim(Date gaiDtfim) {
		this.gaiDtfim = gaiDtfim;
	}

	public Date getGaiDtinicio() {
		return this.gaiDtinicio;
	}

	public void setGaiDtinicio(Date gaiDtinicio) {
		this.gaiDtinicio = gaiDtinicio;
	}

	public Galeria getGaleria() {
		return this.galeria;
	}

	public void setGaleria(Galeria galeria) {
		this.galeria = galeria;
	}

	public Imagem getImagem() {
		return this.imagem;
	}

	public void setImagem(Imagem imagem) {
		this.imagem = imagem;
	}

	public PessoaFisica getPessoafisica1() {
		return this.pessoafisica1;
	}

	public void setPessoafisica1(PessoaFisica pessoafisica1) {
		this.pessoafisica1 = pessoafisica1;
	}

	public PessoaFisica getPessoafisica2() {
		return this.pessoafisica2;
	}

	public void setPessoafisica2(PessoaFisica pessoafisica2) {
		this.pessoafisica2 = pessoafisica2;
	}

}