package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * The persistent class for the funcionario database table.
 * 
 */
@Entity
@Table(name="HISTORICOFINANCEIROCONTASRECEBER")
public class HistoricoFinanceiroContasReceber implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="HFC_CODIGO",unique=true, nullable=false)
	private int id;
	
	@Column(name="HFC_DTVENCIMENTO")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataVencimento;
	
	@Column(name="HFC_DTBAIXA")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataBaixa;
	
	@Column(name="HFC_VALORPRINCIPAL")
	private Float valorPrincipal;
	
	@Column(name="HFC_VALORJUROS")
	private Float valorJuros;
	
	@Column(name="HFC_VALORDESCONTO")
	private Float valorDesconto;
	
	@Column(name="HFC_OBSERVACAO")
	private String observacao;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="UNI_CODIGO", referencedColumnName = "UNI_CODIGO")
	private Unidade unidade;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="TCR_CODIGO", referencedColumnName = "TCR_CODIGO")
	private TipoContasReceber tipoContasReceber;
	
	
	public boolean isNovo() {  
  		return id == 0;
  	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataBaixa() {
		return dataBaixa;
	}

	public void setDataBaixa(Date dataBaixa) {
		this.dataBaixa = dataBaixa;
	}

	public Float getValorPrincipal() {
		return valorPrincipal;
	}

	public void setValorPrincipal(Float valorPrincipal) {
		this.valorPrincipal = valorPrincipal;
	}

	public Float getValorJuros() {
		return valorJuros;
	}

	public void setValorJuros(Float valorJuros) {
		this.valorJuros = valorJuros;
	}

	public Float getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(Float valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public TipoContasReceber getTipoContasReceber() {
		return tipoContasReceber;
	}

	public void setTipoContasReceber(TipoContasReceber tipoContasReceber) {
		this.tipoContasReceber = tipoContasReceber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoFinanceiroContasReceber other = (HistoricoFinanceiroContasReceber) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HistoricoFinanceiroContasReceber [id=" + id + ", "
				+ (dataVencimento != null ? "dataVencimento=" + dataVencimento + ", " : "")
				+ (dataBaixa != null ? "dataBaixa=" + dataBaixa + ", " : "")
				+ (valorPrincipal != null ? "valorPrincipal=" + valorPrincipal + ", " : "")
				+ (valorJuros != null ? "valorJuros=" + valorJuros + ", " : "")
				+ (valorDesconto != null ? "valorDesconto=" + valorDesconto + ", " : "")
				+ (observacao != null ? "observacao=" + observacao + ", " : "")
				+ (unidade != null ? "unidade=" + unidade + ", " : "")
				+ (tipoContasReceber != null ? "tipoContasReceber=" + tipoContasReceber : "") + "]";
	}
	
}
