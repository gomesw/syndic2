package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;


/**
 * The persistent class for the funcao database table.
 * 
 */
@Entity
@Table(name="HISTORICOORDEMSERVICO")
public class HistoricoOrdemServico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="HOS_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name = "HOS_DESCRICAO")
    private String descricao;
	
	@Column(name="HOS_DTINICIO")
	@Temporal(TemporalType.TIMESTAMP)
//	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataInicio;
	 
	@Column(name="HOS_DTFIM")
	@Temporal(TemporalType.TIMESTAMP)
//	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFim;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="ORD_CODIGO", nullable=false)
	private OrdemServico ordemServico;
	
	@NotNull(message="Informe o Status da OS")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="OSS_CODIGO")
	private OrdemServicoStatus ordemServicoStatus;	
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="PEF_CODIGOEXECUTOR", nullable=false)
	private PessoaFisica pessoaFisicaExecutor;
	
	@Transient
	private boolean orcamento;
	
	@Transient
	private boolean atrasado ; 

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public OrdemServico getOrdemServico() {
		return ordemServico;
	}

	public void setOrdemServico(OrdemServico ordemServico) {
		this.ordemServico = ordemServico;
	}

	public String getDescricao() { 
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public OrdemServicoStatus getOrdemServicoStatus() {
		return ordemServicoStatus;
	}

	public void setOrdemServicoStatus(OrdemServicoStatus ordemServicoStatus) {
		this.ordemServicoStatus = ordemServicoStatus;
	}

	public PessoaFisica getPessoaFisicaExecutor() {
		return pessoaFisicaExecutor;
	}

	public void setPessoaFisicaExecutor(PessoaFisica pessoaFisicaExecutor) {
		this.pessoaFisicaExecutor = pessoaFisicaExecutor;
	}
	
	public boolean isOrcamento() {
		return orcamento;
	}

	public void setOrcamento(boolean orcamento) {
		this.orcamento = orcamento;
	}
	
	public boolean isAtrasado() {
		getDiferencaData();
		return atrasado;
	}

	public void setAtrasado(boolean atrasado) {
		this.atrasado = atrasado;
	}

	public String getDiferencaData(){
		
		long segundos = 0;
		long sec = 0;
	    long minutes = 0;
	    long hours = 0;
	    long days = 0;
	    long horasSLA = 0;
	    String retorno = "";
		
		Instant instant = Instant.ofEpochMilli(this.getDataInicio().getTime());
		Instant instant2;
		
		if(this.getDataFim() != null){
			instant2 = Instant.ofEpochMilli(this.getDataFim().getTime());
		}else{
			instant2 = Instant.now();
		}
		
		segundos = ChronoUnit.SECONDS.between(instant, instant2);
		
		horasSLA = ChronoUnit.HOURS.between(instant, instant2);
		
		verificarAtraso(horasSLA);
		
	    minutes = segundos % 3600 / 60;
	    hours = segundos % 86400 / 3600;
	    days = segundos / 86400;
	    
	    retorno = days + " Dia(s) " + hours + " Hora(s) " + minutes + " Minuto(s) "; 
		
		return retorno;				
	}

	private void verificarAtraso(long horasSLA) {
		
		if(this.ordemServicoStatus.getId() == 1){ //aberta
			if(horasSLA >= 24){ this.atrasado = true; }
		}
		
		if(this.ordemServicoStatus.getId() == 2){ //visita executada
			if(horasSLA >= 24){ this.atrasado = true; }
		}
		
		if(this.ordemServicoStatus.getId() == 3){ //COTAÇÃO DE MATERIAL
			if(horasSLA >= 24){ this.atrasado = true; }
		}
		
		if(this.ordemServicoStatus.getId() == 4){ //SERVIÇO AUTORIZADO
			if(horasSLA >= 48){ this.atrasado = true; }
		}
		
		if(this.ordemServicoStatus.getId() == 5){ //COMPRA DE MATERIAL
			if(horasSLA >= 48){ this.atrasado = true; }
		}
		
		if(this.ordemServicoStatus.getId() == 6){ //EM EXECUÇÃO
			if(this.ordemServico.getServicoIdentificado() == 3){//servico do tipo intermediario
				if(horasSLA >= 24){ this.atrasado = true; }
			}else if (this.ordemServico.getServicoIdentificado() == 4) {//servico do tipo crítico
				if(horasSLA >= 48){ this.atrasado = true; }
			}
		} 
		
		if(this.ordemServicoStatus.getId() == 7){ //SERVIÇO EXECUTADO
			if(horasSLA >= 24){ this.atrasado = true; }
		}
		
		if(this.ordemServicoStatus.getId() == 8){ //ACEITE DO SERVIÇO
			if(horasSLA >= 24){ this.atrasado = true; }
		}
		
		if(this.ordemServicoStatus.getId() == 9){ //PESQUISA DE SATISFAÇÃO
			if(horasSLA >= 24){ this.atrasado = true; }
		}
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoOrdemServico other = (HistoricoOrdemServico) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	
}