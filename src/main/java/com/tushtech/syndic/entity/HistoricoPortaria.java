package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name="HISTORICOPORTARIA")
public class HistoricoPortaria implements Serializable{
	
	
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="HIP_CODIGO",unique=true, nullable=false)
	private int id;
	
	@Column(name="HIP_DTENTRADA")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm:ss")
	private Date dataEntrada;
	
	@Column(name="HIP_DTSAIDA")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm:ss")
	private Date dataSaida;
	
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="PEF_CODIGOCADASTRANTE", referencedColumnName = "PEF_CODIGO")
	private PessoaFisica cadastrante;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="UNI_CODIGO", referencedColumnName = "UNI_CODIGO")
	private Unidade unidade;
	
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="CON_CODIGO", referencedColumnName = "CON_CODIGO")
	private Condominio condominio;
	
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="PFU_CODIGO", referencedColumnName = "PFU_CODIGO")
	private PessoaFisicaUnidade pessoaFisica;
	
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="PFV_CODIGO", referencedColumnName = "PFV_CODIGO")
	private PessoaFisicaVeiculo pessoaFisicaVeiculo;
	
		
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Date getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(Date dataSaida) {
		this.dataSaida = dataSaida;
	}


	public PessoaFisica getCadastrante() {
		return cadastrante;
	}

	public void setCadastrante(PessoaFisica cadastrante) {
		this.cadastrante = cadastrante;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public PessoaFisicaUnidade getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisicaUnidade pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public PessoaFisicaVeiculo getPessoaFisicaVeiculo() {
		return pessoaFisicaVeiculo;
	}

	public void setPessoaFisicaVeiculo(PessoaFisicaVeiculo pessoaFisicaVeiculo) {
		this.pessoaFisicaVeiculo = pessoaFisicaVeiculo;
	}
	
	
	@PrePersist
    protected void onDataAtual() {
        this.dataEntrada = new Date();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoricoPortaria other = (HistoricoPortaria) obj;
		if (id != other.id)
			return false;
		return true;
	}

	
}
