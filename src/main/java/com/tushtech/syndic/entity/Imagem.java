package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


/**
 * The persistent class for the imagem database table.
 * 
 */
@Entity
@NamedQuery(name="Imagem.findAll", query="SELECT i FROM Imagem i")
public class Imagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IMA_CODIGO")
	private int id;

	@Column(name="IMA_CONTENTTYPE")
	private String imaContentType;

	@Column(name="IMA_NOME")
	private String imaNome;
	
	@ManyToOne
	@JoinColumn(name="CON_CODIGO")
	private Condominio condominio;

	//bi-directional many-to-one association to ConteudoImagem
	@OneToMany(mappedBy="imagem")
	private List<ConteudoImagem> conteudoImagems;

	//bi-directional many-to-one association to GaleriaImagem
	@OneToMany(mappedBy="imagem")
	private List<GaleriaImagem> galeriaImagems;

	public Imagem() {
	}

	public String getImaContentType() {
		return imaContentType;
	}

	public void setImaContentType(String imaContentType) {
		this.imaContentType = imaContentType;
	}

	public String getImaNome() {
		return this.imaNome;
	}

	public void setImaNome(String imaNome) {
		this.imaNome = imaNome;
	}

	public List<ConteudoImagem> getConteudoImagems() {
		return this.conteudoImagems;
	}

	public void setConteudoImagems(List<ConteudoImagem> conteudoImagems) {
		this.conteudoImagems = conteudoImagems;
	}

	public ConteudoImagem addConteudoImagem(ConteudoImagem conteudoImagem) {
		getConteudoImagems().add(conteudoImagem);
		conteudoImagem.setImagem(this);

		return conteudoImagem;
	}

	public ConteudoImagem removeConteudoImagem(ConteudoImagem conteudoImagem) {
		getConteudoImagems().remove(conteudoImagem);
		conteudoImagem.setImagem(null);

		return conteudoImagem;
	}

	public List<GaleriaImagem> getGaleriaImagems() {
		return this.galeriaImagems;
	}

	public void setGaleriaImagems(List<GaleriaImagem> galeriaImagems) {
		this.galeriaImagems = galeriaImagems;
	}

	public GaleriaImagem addGaleriaImagem(GaleriaImagem galeriaImagem) {
		getGaleriaImagems().add(galeriaImagem);
		galeriaImagem.setImagem(this);

		return galeriaImagem;
	}

	public GaleriaImagem removeGaleriaImagem(GaleriaImagem galeriaImagem) {
		getGaleriaImagems().remove(galeriaImagem);
		galeriaImagem.setImagem(null);

		return galeriaImagem;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Imagem other = (Imagem) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

}