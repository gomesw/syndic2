package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the login database table.
 * 
 */
@Entity
@Table(name="LOGIN")
//@NamedQuery(name="Equipamento.findAll", query="SELECT e FROM Login e")
public class Login implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="LOG_CODIGO",unique=true, nullable=false)
	private int id;

	@ManyToOne
	@JoinColumn(name="PEF_CODIGO")
	private PessoaFisica pessoaFisica;
	
	
	@Column(name="LOG_DTHORALOGIN")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm:ss")
	private Date dataLogin;
	
	@Column(name="LOG_DTHORALOGOUT")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm:ss")
	private Date dataLogout;
	
	
	@Column(name="LOG_SESSAO")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm:ss")
	private String sessao;
	

	
	@PrePersist
    protected void onDataAtual() {
        this.dataLogin = new Date();
    }
	
	@PreUpdate
    protected void onDataAdicao() {
        this.dataLogout = new Date();
    }

	public boolean isNovo() {
		return this.id == 0;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public String getSessao() {
		return sessao;
	}

	public void setSessao(String sessao) {
		this.sessao = sessao;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}


	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}


	public Date getDataLogin() {
		return dataLogin;
	}


	public void setDataLogin(Date dataLogin) {
		this.dataLogin = dataLogin;
	}


	public Date getDataLogout() {
		return dataLogout;
	}


	public void setDataLogout(Date dataLogout) {
		this.dataLogout = dataLogout;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Login other = (Login) obj;
		if (id != other.id)
			return false;
		return true;
	}

	

	

}