package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;


/**
 * The persistent class for the marca database table.
 * 
 */
@Entity
@Table(name="MARCA")
//@NamedQuery(name="marca.findAll", query="SELECT m FROM marca m")
public class Marca implements Serializable {
	private static final long serialVersionUID = 1L; 

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MAR_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe a Marca do Veículo")
	@Column(name="MAR_NOME")
	private String nome;
	
	@Column(name = "MAR_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

//	//bi-directional many-to-one association to Modelo
//	@OneToMany(mappedBy="marca")
//	@JsonIgnore
//	private List<Modelo> modelo;
//	

	@ManyToOne
	@JoinColumn(name="TIV_CODIGO")
	private TipoVeiculo tipoVeiculo;	
	

    public boolean isNovo() {  
  		return id == 0;
  	}	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

//	public List<Modelo> getModelo() {
//		return modelo;
//	}
//
//	public void setModelo(List<Modelo> modelo) {
//		this.modelo = modelo;
//	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	
	public TipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marca other = (Marca) obj;
		if (id != other.id)
			return false;
		return true;
	}

	
	
}