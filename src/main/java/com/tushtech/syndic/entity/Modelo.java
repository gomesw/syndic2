package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;


/**
 * The persistent class for the modelo database table.
 * 
 */
@Entity
@Table(name="MODELO")
//@NamedQuery(name="modelo.findAll", query="SELECT m FROM modelo m")
public class Modelo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="MOD_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe o Modelo")
	@Column(name="MOD_NOME")
	private String nome;
	
	@Column(name = "MOD_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

	//bi-directional many-to-one association to Marca
	@NotNull(message="Informe a Marca do Veículo")
	@ManyToOne
	@JoinColumn(name="MAR_CODIGO")
	private Marca marca;

//	//bi-directional many-to-one association to Veiculo
//	@OneToMany(mappedBy="modelo")
//	@JsonIgnore
//	private List<Veiculo> veiculo;

    public boolean isNovo() {  
  		return id == 0;
  	}		
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

//	public List<Veiculo> getVeiculo() {
//		return veiculo;
//	}
//
//	public void setVeiculo(List<Veiculo> veiculo) {
//		this.veiculo = veiculo;
//	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Modelo other = (Modelo) obj;
		if (id != other.id)
			return false;
		return true;
	}

	
}