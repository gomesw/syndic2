package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the orcamento database table.
 * 
 */
@Entity
@Table(name="ORCAMENTO")
public class Orcamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ORC_CODIGO",unique=true, nullable=false)
	private int id;
		
	//bi-directional many-to-one association to OrdemServico
	@ManyToOne
	@JoinColumn(name="ORD_CODIGO")
	private OrdemServico ordemServico;
	
	@Column(name="ORC_DTORCAMENTO")
	//@Temporal(TemporalType.DATE)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataOrcamento;
	
	@Column(name="ORC_VALORTOTAL")
	private Float valorTotal;
	
	@Column(name = "ORC_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private int ativo;
	
	@Column(name="ORC_NOMEANEXO")
	private String nome;
	
	@Column(name="ORC_CONTENTTYPE")
	private String contentType;
	
	@Column(name = "ORC_ACEITE")
	private int aceite;
	
	@Column(name = "ORC_SINDICOUPLOAD", columnDefinition = "int default 0")
	private int sindicoUpload;
	
	public boolean isNovo(){
		return this.id==0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public OrdemServico getOrdemServico() {
		return ordemServico;
	}

	public void setOrdemServico(OrdemServico ordemServico) {
		this.ordemServico = ordemServico;
	}

	public Date getDataOrcamento() {
		return dataOrcamento;
	}

	public void setDataOrcamento(Date dataOrcamento) {
		this.dataOrcamento = dataOrcamento;
	}

	public Float getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Float valorTotal) {
		this.valorTotal = valorTotal;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public int getAceite() {
		return aceite;
	}

	public void setAceite(int aceite) {
		this.aceite = aceite;
	}

	public String getTipoReduzido(){
		
		String arrayNome[] = this.nome.split("\\."); 
		
		return arrayNome[1];
		
	}

	public int getSindicoUpload() {
		return sindicoUpload;
	}

	public void setSindicoUpload(int sindicoUpload) {
		this.sindicoUpload = sindicoUpload;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orcamento other = (Orcamento) obj;
		if (id != other.id)
			return false;
		return true;
	}



}