package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.NumberFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tushtech.syndic.entity.enumeration.StatusOrdemServico;
import com.tushtech.syndic.entity.enumeration.TipoFotoOrdemServicoEnum;
import com.tushtech.syndic.entity.enumeration.TipoServicoIdentificado;

@Entity
@Table(name="ORDEMSERVICO")
public class OrdemServico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ORD_CODIGO",unique=true, nullable=false)
	private int id;
	
	@NotNull(message="Informe o Tipo de Ordem de Serviço")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="TOS_CODIGO", nullable=false)
	private TipoOrdemServico tipoOrdemServico;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="ATE_CODIGO", nullable=false)
	private Atendimento atendimento;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="PEF_CODIGO", nullable=false)
	private PessoaFisica pessoaFisica;
	
	@OneToMany(mappedBy = "ordemServico", fetch = FetchType.LAZY)
	private List<HistoricoOrdemServico> listHistoricoOrdemServico;
	
	@OneToMany(mappedBy = "ordemServico", fetch = FetchType.LAZY)
	private List<PessoaFisicaOrdemServico> listPessoaFisicaOrdemServico;
	
	@OneToMany(mappedBy = "ordemServico")
	private List<Orcamento> listOrcamento;
	
	
	@OneToMany(mappedBy = "ordemServico")
	private List<AnexoOrdemServico> anexosOrdemServico;
	
	@OneToMany(mappedBy = "ordemServico")
	@JsonIgnore
	private List<FotoOrdemServico> listFotoOrdemServico;
	
	
	
	//bi-directional many-to-one association to condominio
	@NotNull(message="Informe o Condomínio")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="CON_CODIGO", nullable=false)
	private Condominio condominio;
	
	@NotNull(message="Informe a empresa para execução da OS!")
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="PEJ_CODIGO", nullable=false)
	private PessoaJuridica empresa;
	
	@Column(name="ORD_OBSERVACAO")
	private String observacao;
	
	@Column(name="ORD_EMERGENCIA")
	private Boolean emergencia;
	
	@Column(name = "ORD_SERVICOIDENTIFICADO")
	private Integer servicoIdentificado;
	
	@OneToMany(mappedBy = "ordemServico")
	private List<AvaliacaoOrdemServico> listAvaliacaoOrdemServico;
		
	@NumberFormat(pattern = "#,###,###,###.##")
	@Column(name="ORD_VALORMATERIAL")
	private BigDecimal valorMaterial;
	
	@NumberFormat(pattern = "#,###,###,###.##")
	@Column(name="ORD_VALORMAODEOBRA")
	private BigDecimal valorMaoDeObra;
	
	@Column(name="ORD_NRPROTOCOLO", unique=true, nullable=false)
	private Integer numeroProtocolo;
	
	@Transient
	private int tipoPessoaVisualizando;
	
	@Transient
	private boolean finalizada;
	
	public boolean isNovo(){
		return this.id==0;
	}
	
	public int getId() {		
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	


	public Integer getNumeroProtocolo() {
		return numeroProtocolo==null?0:numeroProtocolo;
	}

	public void setNumeroProtocolo(Integer numeroProtocolo) {
		this.numeroProtocolo = numeroProtocolo;
	}

	public List<AnexoOrdemServico> getAnexosOrdemServico() {
		return anexosOrdemServico;
	}

	public void setAnexosOrdemServico(List<AnexoOrdemServico> anexosOrdemServico) {
		this.anexosOrdemServico = anexosOrdemServico;
	}

	public TipoOrdemServico getTipoOrdemServico() {
		return tipoOrdemServico;
	}

	public void setTipoOrdemServico(TipoOrdemServico tipoOrdemServico) {
		this.tipoOrdemServico = tipoOrdemServico;
	}

	public Atendimento getAtendimento() {
		return atendimento;
	}

	public void setAtendimento(Atendimento atendimento) {
		this.atendimento = atendimento;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}	

	public List<HistoricoOrdemServico> getListHistoricoOrdemServico() {
		return listHistoricoOrdemServico;
	}

	public void setListHistoricoOrdemServico(List<HistoricoOrdemServico> listHistoricoOrdemServico) {
		this.listHistoricoOrdemServico = listHistoricoOrdemServico;
	}
	
	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}
	
	public List<Orcamento> getListOrcamento() {
		return listOrcamento;
	}

	public void setListOrcamento(List<Orcamento> listOrcamento) {
		this.listOrcamento = listOrcamento;
	}
	

//	public HistoricoOrdemServico getUltimoHistoricoOrdemServico(){
//		
//		HistoricoOrdemServico historicoOrdemServicoRetorno = new HistoricoOrdemServico();
//		
//		for (HistoricoOrdemServico historicoOrdemServico : this.listHistoricoOrdemServico) {
//			
//			System.err.println("id:1111 "+historicoOrdemServicoRetorno.getId());
//			
//			if(historicoOrdemServico.getDataFim() == null){
//				
//				System.err.println("id:222222 "+historicoOrdemServicoRetorno.getId());
//				
//				historicoOrdemServicoRetorno = historicoOrdemServico;
//			}
//		}
//		
//		
//		
//		if(historicoOrdemServicoRetorno.getId()==0){
//			
//			historicoOrdemServicoRetorno = listHistoricoOrdemServico.get(listHistoricoOrdemServico.size()-1);
//			System.err.println("size "+listHistoricoOrdemServico.size());
//		}
//		
//		System.err.println("id:33333 "+historicoOrdemServicoRetorno.getOrdemServicoStatus().getNome());
//		
//		historicoOrdemServicoRetorno.getOrdemServicoStatus().setNome(deAccent(historicoOrdemServicoRetorno.getOrdemServicoStatus().getNome()));		
//		
//		
//		System.err.println("id:44444 "+historicoOrdemServicoRetorno.getId());
//		
//		return historicoOrdemServicoRetorno;
//	}
	
	
	public HistoricoOrdemServico getUltimoHistoricoOrdemServico(){
		HistoricoOrdemServico retorno = new HistoricoOrdemServico();
		
		if(this.listHistoricoOrdemServico.isEmpty()){
			return retorno;
		}else{
			for (HistoricoOrdemServico histOrd : this.listHistoricoOrdemServico) {
				
				if(histOrd.getDataFim() == null){
					retorno = histOrd;
				}
				
			}
			return retorno;
		}
	}
	
	
	public HistoricoOrdemServico getHistoricoAberturaOrdemServico(){
		HistoricoOrdemServico historicoOrdemServicoRetorno = new HistoricoOrdemServico();
		
		for (HistoricoOrdemServico historicoOrdemServico : listHistoricoOrdemServico) {
			System.err.println("Wallace 2");
			if(historicoOrdemServico.getOrdemServicoStatus().getId()==1){
				historicoOrdemServicoRetorno = historicoOrdemServico;
			}
		}
		return historicoOrdemServicoRetorno;
	}
	
	
	
	
	public String getProtocolo(){
		String numero = String.valueOf(this.numeroProtocolo==null?0:numeroProtocolo);
		if(this.numeroProtocolo<10) {
			numero = "0"+numero;
		}
		return numero + "/" + Integer.parseInt(new SimpleDateFormat("yy").format(this.listHistoricoOrdemServico.get(0).getDataInicio()));
	}
	
	public List<PessoaFisica> getResponsaveisExecucaoOS(){
		 List<PessoaFisica> funcionarios = new ArrayList<>();
		for (PessoaFisicaOrdemServico pessoas : this.listPessoaFisicaOrdemServico) {
			funcionarios.add(pessoas.getPessoaFisica());
		}
		
		return funcionarios;
	}
	
	public boolean getVisitaExecutada(){
			
		boolean retorno = true;
		
		for (PessoaFisicaOrdemServico pessoa : this.listPessoaFisicaOrdemServico) {
			if(!pessoa.getVisita()){
				retorno = false;
			}
		}
		
		return retorno;
	}
	
	
	public String getNomesResponsavelExecucaoOS(){
		
		String retorno = "";
		
		for (PessoaFisicaOrdemServico pessoa : this.listPessoaFisicaOrdemServico) {
			
			if(pessoa.getVisita()){
				retorno+=pessoa.getPessoaFisica().getNome()+ " <i class='material-icons btn-success'>check_circle_outline</i>" + "<br/>";				
			}else{
				retorno+=pessoa.getPessoaFisica().getNome()+ " <i class='material-icons btn-danger'>highlight_off</i>" + "<br/>";	
			}
		}
		
		if(this.listPessoaFisicaOrdemServico.isEmpty()){
			retorno = "SEM INDICACOES";
		}
		
		return retorno;
	}
	
	
	
	
	public boolean isFinalizada() {
		
		boolean retorno = false;
		
		
		if(getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getId()==StatusOrdemServico.FINALIZADA.getId()) {
			retorno = true;
		}
		
		
		return retorno;
	}
	
     public boolean isGarantia() {
		
		boolean retorno = false;
		
		for (HistoricoOrdemServico historicoOrdemServico : listHistoricoOrdemServico) {
			if(historicoOrdemServico.getOrdemServicoStatus().getId()==StatusOrdemServico.GARANTIA.getId()) {
				retorno = true;
			}
		}
		return retorno;
	}

	public void setFinalizada(boolean finalizada) {
		this.finalizada = finalizada;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	


	public PessoaJuridica getEmpresa() {
		return empresa;
	}

	public void setEmpresa(PessoaJuridica empresa) {
		this.empresa = empresa;
	}

	public Boolean getEmergencia() {
		return emergencia;
	}

	public void setEmergencia(Boolean emergencia) {
		this.emergencia = emergencia;
	}
	
	public List<FotoOrdemServico> getListFotoOrdemServico() {
		return listFotoOrdemServico;
	}

	public void setListFotoOrdemServico(List<FotoOrdemServico> listFotoOrdemServico) {
		this.listFotoOrdemServico = listFotoOrdemServico;
	}
	
	public BigDecimal getValorMaterial() {
		return valorMaterial;
	}

	public void setValorMaterial(BigDecimal valorMaterial) {
		this.valorMaterial = valorMaterial;
	}

	public BigDecimal getValorMaoDeObra() {
		return valorMaoDeObra;
	}

	public void setValorMaoDeObra(BigDecimal valorMaoDeObra) {
		this.valorMaoDeObra = valorMaoDeObra;
	}

	public List<PessoaFisicaOrdemServico> getListPessoaFisicaOrdemServico() {
		return listPessoaFisicaOrdemServico;
	}

	public void setListPessoaFisicaOrdemServico(List<PessoaFisicaOrdemServico> listPessoaFisicaOrdemServico) {
		this.listPessoaFisicaOrdemServico = listPessoaFisicaOrdemServico;
	}

	public Integer getServicoIdentificado() {
		return servicoIdentificado;
	}

	public void setServicoIdentificado(Integer servicoIdentificado) {
		this.servicoIdentificado = servicoIdentificado;
	}
	
	public List<AvaliacaoOrdemServico> getListAvaliacaoOrdemServico() {
		return listAvaliacaoOrdemServico;
	}

	public void setListAvaliacaoOrdemServico(List<AvaliacaoOrdemServico> listAvaliacaoOrdemServico) {
		this.listAvaliacaoOrdemServico = listAvaliacaoOrdemServico;
	}
	
	
	/**
	 * @return
	 * LISTA DE ANEXOS QUE FORAM INSERIDOS na FASE DE COTAÇÃO DE MATERIAL
	 */
	public List<FotoOrdemServico> getFotosAberturaOs(){
		List<FotoOrdemServico> anexos = new ArrayList<>();
		
		for (FotoOrdemServico anexoOrdemServico : this.listFotoOrdemServico ) {
			if(anexoOrdemServico.getAtivo()==1 && anexoOrdemServico.getTipoAnexo().equals(TipoFotoOrdemServicoEnum.DURANTEABERTURA.getId())) {
				anexos.add(anexoOrdemServico);
			}
		}
		

		return anexos;
	}


	/**
	 * @return
	 * LISTA DE ANEXOS QUE SERAM INSERIDOS DURANTE A ABERTURA DA ORDEM DE SERVIÇO
	 */
	public List<FotoOrdemServico> getFotosAndamentoOs(){
		List<FotoOrdemServico> anexos = new ArrayList<>();

		for (FotoOrdemServico anexoOrdemServico : this.listFotoOrdemServico ) {
			if(anexoOrdemServico.getAtivo()==1 && anexoOrdemServico.getTipoAnexo().equals(TipoFotoOrdemServicoEnum.DURANTEANDAMENTO.getId())) {
				anexos.add(anexoOrdemServico);
			}
		}
		
		return anexos;
	}


	/**
	 * 1 - fluxo 1 (simples) <br /> -> serviço básico 1 -> serviço básico 2
	 * 2 - fluxo 2 (complexo) <br /> ->serviço intermediário -> serviço crítico
	 */
	public int getTipoFluxo(){
		int retorno = 0;

		if(servicoIdentificado == 1 || servicoIdentificado == 2){
			retorno = 1;
		}else if (servicoIdentificado == 3 || servicoIdentificado == 4) {
			retorno = 2;
		}
		
		return retorno;
	}
	
	public int getTipoPessoaVisualizando() {
		return tipoPessoaVisualizando;
	}

	public void setTipoPessoaVisualizando(int tipoPessoaVisualizando) {
		this.tipoPessoaVisualizando = tipoPessoaVisualizando;
	}
	
	public String getServicoDescricao(){
		String retorno = "";
		
		for (TipoServicoIdentificado tipoServicoIdentificado : TipoServicoIdentificado.values()) {
			if(servicoIdentificado == tipoServicoIdentificado.getId()){
				retorno = tipoServicoIdentificado.getDescricao();
			}
		}
		
		
		return retorno;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrdemServico other = (OrdemServico) obj;
		if (id != other.id)
			return false;
		return true;
	}

	
	 private static String deAccent(String str) {
		    String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
		    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		    return pattern.matcher(nfdNormalizedString).replaceAll("");
		}
	
		
}