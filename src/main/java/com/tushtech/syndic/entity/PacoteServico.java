package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.NumberFormat;


/**
 * The persistent class for the pacoteServico database table.
 * 
 */
@Entity
@Table(name="PACOTESERVICO")
public class PacoteServico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PCS_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe o nome do Pacote de Serviço")
	@Column(name="PCS_NOME")
	private String nome;
	
	@Column(name="PCS_DESCRICAO")
	private String descricao;
	
//	@NotNull(message="Informe a quantidade de visitas do Pacote de Serviço")
	@Column(name="PCS_QTDVISITAS")
	private Integer qtdVisitas;

//	@NotNull(message="Informe o Valor por Visita Excedente!")
	@NumberFormat(pattern = "#,###,###,###.##") 
	@Column(name="PCS_VALORVISITAEXCEDENTE")
	private Float valorVisitaExcedente;
	
	@NotNull(message="Informe o valor do Pacote de Serviço")
	@NumberFormat(pattern = "#,###,###,###.##") 
	@Column(name="PCS_VALOR")
	private Float valor;
	
	@Column(name = "PCS_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	//bi-directional many-to-one association to PessoaJuridica
	@ManyToOne
	@JoinColumn(name="PEJ_CODIGO")
	private PessoaJuridica pessoaJuridica;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
    public boolean isNovo() {  
  		return id == 0;
  	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getQtdVisitas() {
		return qtdVisitas;
	}

	public void setQtdVisitas(Integer qtdVisitas) {
		this.qtdVisitas = qtdVisitas;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public Float getValorVisitaExcedente() {
		return valorVisitaExcedente;
	}

	public void setValorVisitaExcedente(Float valorVisitaExcedente) {
		this.valorVisitaExcedente = valorVisitaExcedente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PacoteServico other = (PacoteServico) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}