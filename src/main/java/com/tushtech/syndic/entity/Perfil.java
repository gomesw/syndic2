package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="PERFIL")
public class Perfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PER_CODIGO")
	private int id;

	@Column(name="PER_ATIVO")
	private int ativo;

	@NotBlank(message="Descrição é Obrigatória")
	@Column(name="PER_DESCRICAO")
	private String descricao;

	@NotBlank(message="Nome é Obrigatório")
	@Column(name="PER_NOME")
	private String nome;


	//bi-directional many-to-one association to PerfilRecurso
	@JsonIgnore
	@OneToMany(mappedBy = "perfil", fetch = FetchType.LAZY)
	private List<PerfilRecurso> listperfilrecursos;
	
	@Transient
	private boolean marcado;
	
	@Transient
	private int condominioperfil;
	

	public int getCondominioperfil() {
		return condominioperfil;
	}

	public void setCondominioperfil(int condominioperfil) {
		this.condominioperfil = condominioperfil;
	}



	public boolean isMarcado() {
		return marcado;
	}



	public void setMarcado(boolean marcado) {
		this.marcado = marcado;
	}



	public Perfil() {
	}

	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getAtivo() {
		return ativo;
	}



	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}



	public String getDescricao() {
		return descricao;
	}



	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}



	public String getNome() {
		return nome;
	}



	public void setNome(String nome) {
		this.nome = nome;
	}


	public boolean isNovo() {
		return this.id==0;
	}
	



	public List<PerfilRecurso> getListperfilrecursos() {
		return listperfilrecursos;
	}



	public void setListperfilrecursos(List<PerfilRecurso> listperfilrecursos) {
		this.listperfilrecursos = listperfilrecursos;
	}


	
	public List<Recurso> getRecursos(){
		List<Recurso> recursos = new ArrayList<Recurso>();
		
		for(PerfilRecurso perfilrecurso: this.listperfilrecursos){
			recursos.add(perfilrecurso.getRecurso());
		}
		return recursos;
		
	}

	public PerfilRecurso addListPerfilRecurso(PerfilRecurso perfilRecurso) {
		getListperfilrecursos().add(perfilRecurso);
		perfilRecurso.setPerfil(this);
		return perfilRecurso;
	}
	
	



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Perfil other = (Perfil) obj;
		if (id != other.id)
			return false;
		return true;
	}



}