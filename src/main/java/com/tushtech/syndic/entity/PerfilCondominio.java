package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name="PERFIL_CONDOMINIO")
public class PerfilCondominio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PCO_CODIGO",unique=true, nullable=false)
	private int id;

	@JoinColumn(name="PER_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope=Perfil.class)
	@JsonIdentityReference(alwaysAsId = true)
	private Perfil perfil;

	@ManyToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="CON_CODIGO", nullable=false)
	private Condominio condominio;

	//bi-directional many-to-one association to PerfilRecurso
	@JsonIgnore
	@OneToMany(mappedBy="perfilCondominio")
	private List<PerfilCondominioRecurso> perfilCondominioRecurso;

	@JsonIgnore
	@OneToMany(mappedBy="perfilCondominio")
	private List<PessoaFisicaPerfilCondominio> pessoaFisicaPerfilCondominio;	
	
	public PerfilCondominio() {
	}


	@JsonProperty("perfil")
	public void setPerfil(Integer perfil) {
		Perfil per = new Perfil();
		per.setId(perfil);
		this.perfil = per;
	}

	@JsonProperty("condominio")
	public void setCondominio(Integer condominio) {
		Condominio con = new Condominio();
		con.setId(condominio);
		this.condominio = con;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}


	

	public List<PerfilCondominioRecurso> getPerfilCondominioRecurso() {
		return perfilCondominioRecurso;
	}


	public void setPerfilCondominioRecurso(List<PerfilCondominioRecurso> perfilCondominioRecurso) {
		this.perfilCondominioRecurso = perfilCondominioRecurso;
	}


	public List<PessoaFisicaPerfilCondominio> getPessoaFisicaPerfilCondominio() {
		return pessoaFisicaPerfilCondominio;
	}


	public void setPessoaFisicaPerfilCondominio(List<PessoaFisicaPerfilCondominio> pessoaFisicaPerfilCondominio) {
		this.pessoaFisicaPerfilCondominio = pessoaFisicaPerfilCondominio;
	}


	public List<Recurso> getRecursos() {
		
		List<Recurso> listeRecursos = new ArrayList<Recurso>();
		if(this.perfilCondominioRecurso!=null){
			
			for (PerfilCondominioRecurso perfilCondominioRecurso : this.perfilCondominioRecurso) {
				listeRecursos.add(perfilCondominioRecurso.getRecurso());
			}
		}
		return listeRecursos;
	}


	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerfilCondominio other = (PerfilCondominio) obj;
		if (id != other.id)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "PerfilCondominio [id=" + id + ", " + (perfil != null ? "perfil=" + perfil + ", " : "")
				+ (condominio != null ? "condominio=" + condominio + ", " : "")
				+ (perfilCondominioRecurso != null ? "perfilCondominioRecurso=" + perfilCondominioRecurso + ", " : "")
				+ (pessoaFisicaPerfilCondominio != null ? "pessoaFisicaPerfilCondominio=" + pessoaFisicaPerfilCondominio
						: "")
				+ "]";
	}




}