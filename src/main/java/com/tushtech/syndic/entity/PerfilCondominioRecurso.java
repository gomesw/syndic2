package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name="PERFIL_CONDOMINIO_RECURSO")
public class PerfilCondominioRecurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PCR_CODIGO",unique=true, nullable=false)
	private int id;

	@JoinColumn(name="PCO_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope=Perfil.class)
	@JsonIdentityReference(alwaysAsId = true)
	private PerfilCondominio perfilCondominio;

	//bi-directional many-to-one association to Recurso
	@JoinColumn(name="REC_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Recurso recurso;

	

	
	@JsonProperty("perfil")
	public void setPerfil(Integer perfilrecursocondominio) {
		PerfilCondominio per = new PerfilCondominio();
		per.setId(perfilrecursocondominio);
		this.perfilCondominio = per;
	}





	public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}



	public PerfilCondominio getPerfilCondominio() {
		return perfilCondominio;
	}



	public void setPerfilCondominio(PerfilCondominio perfilCondominio) {
		this.perfilCondominio = perfilCondominio;
	}


	public Recurso getRecurso() {
		return recurso;
	}


	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}





	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}





	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerfilCondominioRecurso other = (PerfilCondominioRecurso) obj;
		if (id != other.id)
			return false;
		return true;
	}



}