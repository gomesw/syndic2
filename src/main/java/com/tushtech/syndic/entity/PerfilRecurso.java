package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name="PERFIL_RECURSO")
public class PerfilRecurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PRR_CODIGO",unique=true, nullable=false)
	private int id;

	@JoinColumn(name="PER_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope=Perfil.class)
	@JsonIdentityReference(alwaysAsId = true)
	private Perfil perfil;

	//bi-directional many-to-one association to Recurso
	@JoinColumn(name="REC_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	private Recurso recurso;

	@JoinColumn(name="CON_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY,cascade=CascadeType.ALL)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope=Condominio.class)
	@JsonIdentityReference(alwaysAsId = true)
	private Condominio condominio;

	public PerfilRecurso() {
	}

	
	@JsonProperty("perfil")
	public void setPerfil(Integer perfil) {
		Perfil per = new Perfil();
		per.setId(perfil);
		this.perfil = per;
	}
	
	@JsonProperty("condominio")
	public void setCondominio(Integer condominio) {
		Condominio con = new Condominio();
		con.setId(condominio);
		this.condominio = con;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		perfil = perfil;
	}

	public Recurso getRecurso() {
		return recurso;
	}

	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}


	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerfilRecurso other = (PerfilRecurso) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PerfilRecurso [id=" + id + ", " + (perfil != null ? "perfil=" + perfil + ", " : "")
				+ (recurso != null ? "recurso=" + recurso : "") + "]";
	}



}