package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;


/**
 * The persistent class for the perguntaAvaliacao database table.
 * 
 */
@Entity
@Table(name="PERGUNTAAVALIACAO")
public class PerguntaAvaliacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PAV_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe a Pergunta")
	@Column(name="PAV_NOME")
	private String nome;
	
	@NotNull(message="Informe o Tipo de Pergunta")
	@Column(name="PAV_OBJETIVA")
	private Integer objetiva;
	
	@Column(name = "PAV_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
    public boolean isNovo() {  
  		return id == 0;
  	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Integer getObjetiva() {
		return objetiva;
	}

	public void setObjetiva(Integer objetiva) {
		this.objetiva = objetiva;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PerguntaAvaliacao other = (PerguntaAvaliacao) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}