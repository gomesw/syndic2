package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisica;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaCondominioEnum;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaUnidadeEnum;


/**
 * The persistent class for the pessoafisica database table.
 * 
 */
@Entity
@Table(name="PESSOAFISICA")
public class PessoaFisica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PEF_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="PEF_CPF")
	private String cpf;

	@Column(name="PEF_NOMEFOTO")
	private String nomeFoto;
	
	@Column(name="PEF_CONTENT_TYPE")
	private String contentType;

	
	@Column(name="PEF_NOMEMAE")
	private String nomeMae;

	@Column(name="PEF_NOMEPAI")
	private String nomePai;

	@NotBlank(message="O Nome é obrigatório")
	@Column(name="PEF_NOME")
	private String nome;

	@Column(name="PEF_DTNASCIMENTO")
	@NotNull(message="A data de nascimento é obrigatória")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataNascimento;
	

    @Column(name = "PEF_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

	//bi-directional many-to-one association to PessoafisicaUnidade
	@JsonIgnore
	@OneToMany(mappedBy="pessoaFisica")
	private List<PessoaFisicaUnidade> pessoaFisicaUnidade;
	

	//bi-directional many-to-one association to PfCondominio
	@JsonIgnore
	@OneToMany(mappedBy="pessoaFisica")
	private List<PessoaFisicaCondominio> pessoaFisicaCondominio;
	
	@JsonIgnore
	@OneToMany(mappedBy="pessoaFisica")
	private List<PessoaFisicaPerfilCondominio> pessoaFisicaPerfilCondominio;
	
	
	@JsonIgnore
	@OneToMany(mappedBy="pessoaFisica")
	private List<PessoaFisicaProfissao> pessoaFisicaProfissoes;


	//bi-directional many-to-one association to PfCondominio
	@JsonIgnore
	@OneToMany(mappedBy="pessoaFisica",fetch = FetchType.LAZY)
	private List<Email> emails;
	

	//bi-directional many-to-one association to PfCondominio
	@JsonIgnore
	@OneToMany(mappedBy="pessoaFisica",fetch = FetchType.LAZY)
	private List<Telefone> telefones;

	//bi-directional many-to-one association to PessoafisicaUnidade
	@JsonIgnore
	@OneToMany(mappedBy="pessoaFisica",fetch = FetchType.LAZY)
	private List<Senhas> senhas;
	
	//bi-directional many-to-one association to PfCondominio
	@JsonIgnore
	@OneToMany(mappedBy="pessoaFisica",fetch = FetchType.LAZY)
	private List<PessoaFisicaVeiculo> pessoaFisicaVeiculo;

	@JsonIgnore
	@JoinColumn(name = "sex_Codigo", referencedColumnName = "sex_Codigo")
	@OneToOne(optional = false, fetch = FetchType.LAZY)
	private Sexo sexo;
	
	

	public List<PessoaFisicaProfissao> getPessoaFisicaProfissoes() {
		return pessoaFisicaProfissoes;
	}

	public void setPessoaFisicaProfissoes(List<PessoaFisicaProfissao> pessoaFisicaProfissoes) {
		this.pessoaFisicaProfissoes = pessoaFisicaProfissoes;
	}

	@Transient
	private String novaFoto;
	
	@Transient
	private String urlFoto;
	
	@Transient
	private int tipoPessoaFisicaUnidade;
	
	
	@Transient
	private FuncaoPessoaJuridica funcaoEmpresa;
	
	
	@Transient
	private int tipoPessoa;
	
	
	@Transient
	public String papeis;
	

	public int getTipoPessoaFisicaUnidade() {
		return tipoPessoaFisicaUnidade;
	}

	public void setTipoPessoaFisicaUnidade(int tipoPessoaFisicaUnidade) {
		this.tipoPessoaFisicaUnidade = tipoPessoaFisicaUnidade;
	}

	public List<PessoaFisicaVeiculo> getPessoaFisicaVeiculo() {
		return pessoaFisicaVeiculo;
	}

	public void setPessoaFisicaVeiculo(List<PessoaFisicaVeiculo> pessoaFisicaVeiculo) {
		this.pessoaFisicaVeiculo = pessoaFisicaVeiculo;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

		

	public int getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(int tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public void setPapeis(String papeis) {
		this.papeis = papeis;
	}

	public boolean isNovo() {
		return id==0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCpfSemMascara(){
		return  this.cpf.trim().replaceAll("\\.|-|/", "");
	}	
	
	
	public String getNomeFoto() {
		return nomeFoto;
	}

	public void setNomeFoto(String nomeFoto) {
		this.nomeFoto = nomeFoto;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getNovaFoto() {
		return novaFoto;
	}

	public void setNovaFoto(String novaFoto) {
		this.novaFoto = novaFoto;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getNomePai() {
		return nomePai;
	}

	public void setNomePai(String nomePai) {
		this.nomePai = nomePai;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	

	public List<PessoaFisicaUnidade> getPessoaFisicaUnidade() {
		return pessoaFisicaUnidade;
	}

	public void setPessoaFisicaUnidade(List<PessoaFisicaUnidade> pessoaFisicaUnidade) {
		this.pessoaFisicaUnidade = pessoaFisicaUnidade;
	}

	public List<PessoaFisicaCondominio> getPessoaFisicaCondominio() {
		return pessoaFisicaCondominio;
	}

	public void setPessoaFisicaCondominio(List<PessoaFisicaCondominio> pessoaFisicaCondominio) {
		this.pessoaFisicaCondominio = pessoaFisicaCondominio;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}


	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

	public List<Senhas> getSenhas() {
		return this.senhas;
	}

	public void setSenhas(List<Senhas> senhas) {
		this.senhas = senhas;
	}

	public List<Email> getEmails() {
		return emails;
	}

	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}

	
	
	
	
	public List<PessoaFisicaPerfilCondominio> getPessoaFisicaPerfilCondominio() {
		return pessoaFisicaPerfilCondominio;
	}

	public void setPessoaFisicaPerfilCondominio(List<PessoaFisicaPerfilCondominio> pessoaFisicaPerfilCondominio) {
		this.pessoaFisicaPerfilCondominio = pessoaFisicaPerfilCondominio;
	}
	
	
	
	public FuncaoPessoaJuridica getFuncaoEmpresa() {
		return funcaoEmpresa;
	}

	public void setFuncaoEmpresa(FuncaoPessoaJuridica funcaoEmpresa) {
		this.funcaoEmpresa = funcaoEmpresa;
	}

	@PrePersist @PreUpdate
	private void prePersistPreUpdate() {
		this.cpf = this.cpf.trim().replaceAll("\\.|-|/", "");
	}
	
	@PostLoad
	private void postLoad() {
		this.cpf = (this.cpf!=null?this.cpf.replaceAll("(\\d{3})(\\d{3})(\\d{3})", "$1.$2.$3-").trim():null); 
	}
	
	
	

	public Email addEmail(Email email) {
		getEmails().add(email);
		email.setPessoaFisica(this);
		return email;
	}
	
	
	public String getPapeis() {
		papeis = "";
		
		int cont = 0;
		for (PessoaFisicaCondominio pessoaFisicaCondominio : this.pessoaFisicaCondominio) {
			if(cont==0) {
				papeis +=pessoaFisicaCondominio.getTipoPessoaFisicaCondominio().getNome();
			}
			else {
				papeis +=","+pessoaFisicaCondominio.getTipoPessoaFisicaCondominio().getNome();
			}
			cont++;
		}
		return papeis;
	}
	
	public String getPapeisTexo() {
		return papeis;
	}

	
	@JsonIgnore
	public String getTipoPessoaFormatado(){
		
		String retorno = "";
		if(!this.pessoaFisicaCondominio.isEmpty()){
			for (PessoaFisicaCondominio pessoaFisicaCondominio : this.pessoaFisicaCondominio) {
				retorno += pessoaFisicaCondominio.getTipoPessoaFisicaCondominio().getNome()+"/";
				
			}
		}
		return retorno;
		
	}
	
	
	@JsonIgnore
	public boolean isSindico(){
		boolean retorno = false;
		
		if(!this.pessoaFisicaCondominio.isEmpty()){
			for (PessoaFisicaCondominio pessoaFisicaCondominio : this.pessoaFisicaCondominio) {
				if(pessoaFisicaCondominio.getTipoPessoaFisicaCondominio().getId() == TipoPessoaFisicaCondominioEnum.SINDICO.getId()
						&& pessoaFisicaCondominio.getDataFim()==null){
					retorno = true;
				}
			}
		}
		
		return retorno;
	}
	
	
	@JsonIgnore
	public boolean isProprietario(){
		boolean retorno = false;
		
		if(!this.pessoaFisicaUnidade.isEmpty()){
			for (PessoaFisicaUnidade pessoaFisicaUnidade : this.pessoaFisicaUnidade) {
				if((pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId() == TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId() 
						&& pessoaFisicaUnidade.getDataFim()==null ) || 
						(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId() == TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId() 
						&& pessoaFisicaUnidade.getDataFim()==null )
						){
					retorno = true;
				}
			}
		}
		
		return retorno;
	}
	
	
	@JsonIgnore
	public boolean isInquilino(){
		boolean retorno = false;
		
		if(this.pessoaFisicaUnidade!=null && !this.pessoaFisicaUnidade.isEmpty()){
			for (PessoaFisicaUnidade pessoaFisicaUnidade : this.pessoaFisicaUnidade) {
				if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId() == TipoPessoaFisicaUnidadeEnum.INQUILINO.getId() 
						&& pessoaFisicaUnidade.getDataFim()==null){
					retorno = true;
				} 
			}
		}
		
		return retorno;
	}
	
	
	
	@JsonIgnore
	public boolean isAdministradorEmpresa(){
		boolean retorno = false;
		
		if(this.tipoPessoa!=0){
				if(this.tipoPessoa == TipoPessoaFisica.PESSOAFISICAPESSOAJURIDICA.getId()){
					retorno = true;
				} 
		}
		
		return retorno;
	}
	
	
	
	
	@JsonIgnore
	public boolean isFuncionario(){
		boolean retorno = false;
		
		if(!this.pessoaFisicaCondominio.isEmpty()){
			for (PessoaFisicaCondominio pessoaFisicaCondominio : this.pessoaFisicaCondominio) {
				if(pessoaFisicaCondominio.getTipoPessoaFisicaCondominio().getId() == TipoPessoaFisicaCondominioEnum.FUNCIONARIO.getId()
						&& pessoaFisicaCondominio.getDataFim()==null){
					retorno = true;
				}
			}
		}
		
		return retorno;
	}
	
	
	@JsonIgnore
	public boolean isMorador(){
		boolean retorno = false;
		
		if(!this.pessoaFisicaCondominio.isEmpty()){
			for (PessoaFisicaCondominio pessoaFisicaCondominio : this.pessoaFisicaCondominio) {
				if(pessoaFisicaCondominio.getTipoPessoaFisicaCondominio().getId() == TipoPessoaFisicaCondominioEnum.MORADOR.getId()
						&& pessoaFisicaCondominio.getDataFim()==null){
					retorno = true;
				}
			}
		}
		
		return retorno;
	}
	
	
	
	@JsonIgnore
	public boolean isAdministradorSistema(){
		if(this.funcaoEmpresa != null){
			return this.funcaoEmpresa.getNome().equals("ADMINISTRADOR SISTEMA");			
		}else{
			return false;
		}
	}
	 
	
	
	@JsonIgnore
	public boolean isVisitante(){
		boolean retorno = false;
		if(!this.pessoaFisicaCondominio.isEmpty()){
			for (PessoaFisicaCondominio pessoaFisicaCondominio : this.pessoaFisicaCondominio) {
				if(pessoaFisicaCondominio.getTipoPessoaFisicaCondominio().getId() == TipoPessoaFisicaCondominioEnum.VISITANTE.getId()
						&& pessoaFisicaCondominio.getDataFim()==null){
					retorno = true;
				}
			}
		}
		
		return retorno;
	}
	
	
	
	public String getTelefonesFormatados(){
		String retono = "";
		
		int cont = 0;
		if(this.telefones!=null) {
			for (Telefone tel : this.telefones) {
				if(cont==0){
					retono+=" "+tel.getNumeroFormatado()+" ";
				}
				else{
					retono+=" / "+tel.getNumeroFormatado()+" ";
				}
				cont++;
			}
		}
		else {
			retono = "Sem Contatos";
		}

		return retono;
	}
	
	@JsonIgnore
	public Condominio getCondominioAtual() {
		Condominio retorno = new Condominio();
		
		for (PessoaFisicaCondominio pfcon : pessoaFisicaCondominio) {
			if(pfcon.getDataFim()==null){
				retorno = pfcon.getCondominio();
			}

		}
		
		if(retorno==null) {
			for (PessoaFisicaUnidade pfuni : this.pessoaFisicaUnidade) {
				if(pfuni.getDataFim()==null){
					retorno = pfuni.getUnidade().getCondominio();
				}
			}
		}
		

		return retorno;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaFisica other = (PessoaFisica) obj;
		if (id != other.id)
			return false;
		return true;
	}

}