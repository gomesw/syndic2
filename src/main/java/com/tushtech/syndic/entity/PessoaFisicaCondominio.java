package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;


/**
 * The persistent class for the pf_condominio database table.
 * 
 */
@Entity
@Table(name="PESSOAFISICA_CONDOMINIO")
//@NamedQuery(name="pessoaFisica_Condominio.findAll", query="SELECT p FROM pessoaFisica_Condominio p")
public class PessoaFisicaCondominio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PFC_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="PFC_DTFIM")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFim;

	@Column(name="PFC_DTINICIO")
	//@NotNull(message="A data de inicío do exercício é obrigatório")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date inicio;

	//bi-directional many-to-one association to Pessoafisica
	@ManyToOne(cascade = {CascadeType.PERSIST })
	@JoinColumn(name="PEF_CODIGO")
	private PessoaFisica pessoaFisica;

	//bi-directional many-to-one association to Condominio
	@ManyToOne
	@JoinColumn(name="CON_CODIGO")
	private Condominio condominio;
	
	//bi-directional many-to-one association to TipoFuncionario
	@ManyToOne
	@JoinColumn(name="TIF_CODIGO")
	private TipoFuncionario tipoFuncionario;
	
	//bi-directional many-to-one association to TipoFuncionario
	@ManyToOne
	@JoinColumn(name="TPC_CODIGO")
	private TipoPessoaFisicaCondominio tipoPessoaFisicaCondominio;
	
	

	//bi-directional many-to-one association to Condominio
	@ManyToOne
	@JoinColumn(name="REG_CODIGO")
	private RegimeTrabalho regimeTrabalho;
	
	@NumberFormat(pattern = "#,###,###,###.##") 
	@Column(name="PFC_VALORSALARIO")
	private Float valorSalario;
	
	
	public boolean isNovo() {
		return id==0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public TipoFuncionario getTipoFuncionario() {
		return tipoFuncionario;
	}

	public void setTipoFuncionario(TipoFuncionario tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}

	public RegimeTrabalho getRegimeTrabalho() {
		return regimeTrabalho;
	}

	public void setRegimeTrabalho(RegimeTrabalho regimeTrabalho) {
		this.regimeTrabalho = regimeTrabalho;
	}

	public Float getValorSalario() {
		return valorSalario;
	}

	public void setValorSalario(Float valorSalario) {
		this.valorSalario = valorSalario;
	}
	

	public TipoPessoaFisicaCondominio getTipoPessoaFisicaCondominio() {
		return tipoPessoaFisicaCondominio;
	}

	public void setTipoPessoaFisicaCondominio(TipoPessoaFisicaCondominio tipoPessoaFisicaCondominio) {
		this.tipoPessoaFisicaCondominio = tipoPessoaFisicaCondominio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaFisicaCondominio other = (PessoaFisicaCondominio) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}