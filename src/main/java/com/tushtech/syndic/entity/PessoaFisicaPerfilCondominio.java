package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="PESSOAFISICA_PERFIL_CONDOMINIO")
public class PessoaFisicaPerfilCondominio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PPC_CODIGO",unique=true, nullable=false)
	private int id;
	

	@JoinColumn(name="PCO_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private PerfilCondominio perfilCondominio;

	@JoinColumn(name="PEF_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private PessoaFisica pessoaFisica;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PerfilCondominio getPerfilCondominio() {
		return perfilCondominio;
	}

	public void setPerfilCondominio(PerfilCondominio perfilCondominio) {
		this.perfilCondominio = perfilCondominio;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaFisicaPerfilCondominio other = (PessoaFisicaPerfilCondominio) obj;
		if (id != other.id)
			return false;
		return true;
	}

	

}