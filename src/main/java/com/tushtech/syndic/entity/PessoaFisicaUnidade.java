package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the pessoafisica_unidade database table.
 * 
 */
@Entity
@Table(name="PESSOAFISICA_UNIDADE")
//@NamedQuery(name="pessoaFisica_unidade.findAll", query="SELECT p FROM pessoaFisica_unidade p")
public class PessoaFisicaUnidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PFU_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="PFU_DTFIM")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFim;

	@Column(name="PFU_DTINICIO")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataInicio;
	
	@Column(name="PFU_DTCADASTRO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;

	//bi-directional many-to-one association to Pessoafisica
	@ManyToOne
	@JoinColumn(name="PEF_CODIGO")
	private PessoaFisica pessoaFisica;
	
	//bi-directional many-to-one association to Pessoafisica
	@ManyToOne
	@JoinColumn(name="PEF_CODIGOCADASTRANTE")
	private PessoaFisica pessoaFisicaCadastrante;

	//bi-directional many-to-one association to Unidade
	@ManyToOne
	@JoinColumn(name="UNI_CODIGO")
	private Unidade unidade;


	//bi-directional many-to-one association to Tipopessoa
	@ManyToOne
	@JoinColumn(name="TPF_CODIGO")
	private TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade;


	//bi-directional many-to-one association to Tipopessoa
	@ManyToOne
	@JoinColumn(name="PAR_CODIGO")
	private Parentesco parentesco;

	//bi-directional many-to-one association to Tipopessoa
	@ManyToOne
	@JoinColumn(name="TIF_CODIGO")
	private TipoFuncionario tipoFuncionario;
	
	@Column(name="PFU_AVISARMORADOR")
	private int avisaMorador;
	
	@Column(name="PFU_PARTEIMOVEL")
	private String parteImovel;
	
	@Column(name="PFU_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private int ativo;

	//bi-directional many-to-one association to PfCondominio
	@OneToMany(mappedBy="pessoaFisicaUnidade",fetch = FetchType.LAZY)
	private List<PessoaFisicaUnidadeRecurso> pessoaFisicaUnidadeRecurso;
	
	@OneToMany(mappedBy="pessoaFisicaUnidade",fetch = FetchType.LAZY)
	private List<PessoaFisicaUnidadeDiaSemana> pessoaFisicaUnidadeDiaSemana;

	@Transient
	private int tipoPessoa;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}


	public boolean isNovo() { 
		return id==0;
	}


	public Parentesco getParentesco() {
		return parentesco;
	}

	public void setParentesco(Parentesco parentesco) {
		this.parentesco = parentesco;
	}

	public TipoPessoaFisicaUnidade getTipoPessoaFisicaUnidade() {
		return tipoPessoaFisicaUnidade;
	}

	public void setTipoPessoaFisicaUnidade(TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade) {
		this.tipoPessoaFisicaUnidade = tipoPessoaFisicaUnidade;
	}

	
	
	public List<PessoaFisicaUnidadeDiaSemana> getPessoaFisicaUnidadeDiaSemana() {
		return pessoaFisicaUnidadeDiaSemana;
	}

	public void setPessoaFisicaUnidadeDiaSemana(List<PessoaFisicaUnidadeDiaSemana> pessoaFisicaUnidadeDiaSemana) {
		this.pessoaFisicaUnidadeDiaSemana = pessoaFisicaUnidadeDiaSemana;
	}

	public int getTipoPessoa() {
		return tipoPessoa;
	}

	public void setTipoPessoa(int tipoPessoa) {
		this.tipoPessoa = tipoPessoa;
	}

	public List<PessoaFisicaUnidadeRecurso> getPessoaFisicaUnidadeRecurso() {
		return pessoaFisicaUnidadeRecurso;
	}

	public void setPessoaFisicaUnidadeRecurso(List<PessoaFisicaUnidadeRecurso> pessoaFisicaUnidadeRecurso) {
		this.pessoaFisicaUnidadeRecurso = pessoaFisicaUnidadeRecurso;
	}
	
	
	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	
	public PessoaFisica getPessoaFisicaCadastrante() {
		return pessoaFisicaCadastrante;
	}

	public void setPessoaFisicaCadastrante(PessoaFisica pessoaFisicaCadastrante) {
		this.pessoaFisicaCadastrante = pessoaFisicaCadastrante;
	}


	public String getParteImovel() {
		return parteImovel;
	}

	public void setParteImovel(String parteImovel) {
		this.parteImovel = parteImovel;
	}

	public TipoFuncionario getTipoFuncionario() {
		return tipoFuncionario;
	}

	public void setTipoFuncionario(TipoFuncionario tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}

	public int getAvisaMorador() {
		return avisaMorador;
	}

	public void setAvisaMorador(int avisaMorador) {
		this.avisaMorador = avisaMorador;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaFisicaUnidade other = (PessoaFisicaUnidade) obj;
		if (id != other.id)
			return false;
		return true;
	}



}