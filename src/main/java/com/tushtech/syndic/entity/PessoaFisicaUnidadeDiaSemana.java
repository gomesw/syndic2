package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name="PESSOAFISICA_UNIDADE_DIASEMANA")
public class PessoaFisicaUnidadeDiaSemana implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PFD_CODIGO",unique=true, nullable=false)
	private int id;

	@JoinColumn(name="DSS_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope=Perfil.class)
	@JsonIdentityReference(alwaysAsId = true)
	private DiaSemana diaSemana;

	
	@JoinColumn(name="TUR_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JsonIdentityReference(alwaysAsId = true)
	private Turno turno;
	
	@JoinColumn(name="PFU_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private PessoaFisicaUnidade pessoaFisicaUnidade;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public DiaSemana getDiaSemana() {
		return diaSemana;
	}


	public void setDiaSemana(DiaSemana diaSemana) {
		this.diaSemana = diaSemana;
	}



	public PessoaFisicaUnidade getPessoaFisicaUnidade() {
		return pessoaFisicaUnidade;
	}


	public void setPessoaFisicaUnidade(PessoaFisicaUnidade pessoaFisicaUnidade) {
		this.pessoaFisicaUnidade = pessoaFisicaUnidade;
	}

	public Turno getTurno() {
		return turno;
	}

	public void setTurno(Turno turno) {
		this.turno = turno;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaFisicaUnidadeDiaSemana other = (PessoaFisicaUnidadeDiaSemana) obj;
		if (id != other.id)
			return false;
		return true;
	}



}