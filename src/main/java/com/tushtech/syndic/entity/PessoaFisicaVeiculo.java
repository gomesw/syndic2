package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the pessoafisica_unidade database table.
 * 
 */
@Entity
@Table(name="PESSOAFISICA_VEICULO")
public class PessoaFisicaVeiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PFV_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="PFV_DTFIM")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFim;

	@Column(name="PFV_DTINICIO")
	//@Temporal(TemporalType.DATE)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataInicio;

	//bi-directional many-to-one association to Pessoafisica
	@ManyToOne
	@JoinColumn(name="PEF_CODIGO")
	private PessoaFisica pessoaFisica;

	//bi-directional many-to-one association to Unidade
	@ManyToOne
	@JoinColumn(name="TPF_CODIGO")
	private TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade;


	//bi-directional many-to-one association to Veiculo
	@ManyToOne
	@JoinColumn(name="VEI_CODIGO")
	private Veiculo veiculo;
	
	//bi-directional many-to-one association to PessoafisicaUnidade
	@ManyToOne
	@JoinColumn(name="PFU_CODIGO")
	private PessoaFisicaUnidade pessoaFisicaUnidade;
	
	public boolean isNovo(){
		return this.id==0;
	}
	
	//bi-directional many-to-one association to PfCondominio
	@OneToMany(mappedBy="veiculo",fetch = FetchType.LAZY)
	private List<PessoaFisicaVeiculo> pessoaFisicaVeiculo;

	@Transient
	private Unidade unidade;
	
	
	public List<PessoaFisicaVeiculo> getPessoaFisicaVeiculo() {
		return pessoaFisicaVeiculo;
	}

	public void setPessoaFisicaVeiculo(List<PessoaFisicaVeiculo> pessoaFisicaVeiculo) {
		this.pessoaFisicaVeiculo = pessoaFisicaVeiculo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}


	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public TipoPessoaFisicaUnidade getTipoPessoaFisicaUnidade() {
		return tipoPessoaFisicaUnidade;
	}

	public void setTipoPessoaFisicaUnidade(TipoPessoaFisicaUnidade tipoPessoaFisicaUnidade) {
		this.tipoPessoaFisicaUnidade = tipoPessoaFisicaUnidade;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
	@PrePersist
    protected void onDataAtual() {
        this.dataInicio = new Date();
        
    }
	
	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	
	public PessoaFisicaUnidade getPessoaFisicaUnidade() {
		return pessoaFisicaUnidade;
	}

	public void setPessoaFisicaUnidade(PessoaFisicaUnidade pessoaFisicaUnidade) {
		this.pessoaFisicaUnidade = pessoaFisicaUnidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaFisicaVeiculo other = (PessoaFisicaVeiculo) obj;
		if (id != other.id)
			return false;
		return true;
	}



}