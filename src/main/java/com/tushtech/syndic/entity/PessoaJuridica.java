package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.br.CNPJ;


/**
 * The persistent class for the pessoajuridica database table.
 * 
 */
@Entity
@Table(name="PESSOAJURIDICA")
//@NamedQuery(name="pessoaJuridica.findAll", query="SELECT p FROM pessoaJuridica p")
public class PessoaJuridica implements Serializable {
	
	
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PEJ_CODIGO",unique=true, nullable=false)
	private int id;	

	@CNPJ
	@Column(name="PEJ_CNPJ")
	private String cnpj;

	@NotBlank(message = "Nome Fantasia é Obrigatório")
	@Column(name="PEJ_NOMEFANTASIA")
	private String nomeFantasia;
	
	@NotBlank(message = "Razão Social é Obrigatório")
	@Column(name="PEJ_RAZAOSOCIAL")
	private String razaoSocial;

	@Column(name = "PEJ_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	//bi-directional many-to-one association to Condominio
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "pessoaJuridica", fetch = FetchType.LAZY)
	private List<Condominio> condominio;

	//bi-directional many-to-one association to Correspondencia
	@OneToMany(mappedBy="pessoaJuridica")
	private List<Correspondencia> correspondencia;
	
	
	@OneToMany(mappedBy="pessoaJuridica")
	private List<PessoaJuridicaPessoaFisica> pessoaJuridicaPessoaFisica;
	
	//bi-directional many-to-one association to areaatuacao
	@ManyToOne
	@JoinColumn(name="AAT_CODIGO")
	private AreaAtuacao areaAtuacao;
		

	public boolean isNovo(){
		return this.id==0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public List<Condominio> getCondominio() {
		return condominio;
	}

	public void setCondominio(List<Condominio> condominio) {
		this.condominio = condominio;
	}

	public List<Correspondencia> getCorrespondencia() {
		return correspondencia;
	}

	public void setCorrespondencia(List<Correspondencia> correspondencia) {
		this.correspondencia = correspondencia;
	}
	
	@PostLoad
	private void postLoad() {
		this.cnpj = (this.cnpj!=null?this.cnpj.replaceAll("(\\d{2})(\\d{3})(\\d{3})(\\d{4})(\\d{2})", "$1.$2.$3/$4-$5").trim():null); 
	}

	
	@PrePersist @PreUpdate
	private void prePersistPreUpdate() {
		this.cnpj = this.cnpj.trim().replaceAll("\\.|-|/", "");
	}
	
	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	public AreaAtuacao getAreaAtuacao() {
		return areaAtuacao;
	}

	public void setAreaAtuacao(AreaAtuacao areaAtuacao) {
		this.areaAtuacao = areaAtuacao;
	}
	
	
	public List<PessoaFisica> getFuncionarios(){
		List<PessoaFisica> funcionarios = new ArrayList<>();
		
		
		if( this.pessoaJuridicaPessoaFisica!=null) {
		for (PessoaJuridicaPessoaFisica pessoajuridicapessoaFisica : this.pessoaJuridicaPessoaFisica) {
			if(pessoajuridicapessoaFisica.getDataFim()==null && pessoajuridicapessoaFisica.getFuncaoPessoaJuridica().getId() == 4){
				funcionarios.add(pessoajuridicapessoaFisica.getPessoaFisica());
			}
		}
		}
		return  funcionarios;
	}
	
	
	public List<PessoaJuridicaPessoaFisica> getFuncionariosPFPJ(){
		List<PessoaJuridicaPessoaFisica> funcionarios = new ArrayList<>();
		for (PessoaJuridicaPessoaFisica pessoajuridicapessoaFisica : this.pessoaJuridicaPessoaFisica) {
			if(pessoajuridicapessoaFisica.getDataFim()==null){
				funcionarios.add(pessoajuridicapessoaFisica);
			}
		}
		return  funcionarios;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridica other = (PessoaJuridica) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
}