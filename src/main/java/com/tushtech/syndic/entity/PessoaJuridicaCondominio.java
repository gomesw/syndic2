package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the pj_condominio database table.
 * 
 */
@Entity
@Table(name="PESSOAJURIDICA_CONDOMINIO")
public class PessoaJuridicaCondominio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PJC_CODIGO",unique=true, nullable=false)
	private int id;
	
	//bi-directional many-to-one association to PessoaJuridica
	@ManyToOne
	@JoinColumn(name="PEJ_CODIGO")
	private PessoaJuridica pessoaJuridica;
	
	//bi-directional many-to-one association to Condominio
	@ManyToOne
	@JoinColumn(name="CON_CODIGO")
	private Condominio condominio;

	//bi-directional many-to-one association to PessoaFisicaResponsavel
	@ManyToOne
	@JoinColumn(name="PEF_CODIGORESPONSAVEL")
	private PessoaFisica pessoaFisicaResponsavel;
		
	@Column(name="PJC_DTINICIO")
	//@NotNull(message="A data de inicío do exercício é obrigatório")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date inicio;
	
	@Column(name="PJC_DTFIM")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFim;
	
	@Column(name = "PJC_ADIMPLENTE", columnDefinition = "int default 0")
	private int adimplente;

	public boolean isNovo() {
		return id==0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public PessoaFisica getPessoaFisicaResponsavel() {
		return pessoaFisicaResponsavel;
	}

	public void setPessoaFisicaResponsavel(PessoaFisica pessoaFisicaResponsavel) {
		this.pessoaFisicaResponsavel = pessoaFisicaResponsavel;
	}

	public int getAdimplente() {
		return adimplente;
	}

	public void setAdimplente(int adimplente) {
		this.adimplente = adimplente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridicaCondominio other = (PessoaJuridicaCondominio) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}