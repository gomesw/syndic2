package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the pj_pf database table.
 * 
 */
@Entity
@Table(name="PESSOAJURIDICA_PESSOAFISICA")
public class PessoaJuridicaPessoaFisica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PJP_CODIGO",unique=true, nullable=false)
	private int id;
	
	//bi-directional many-to-one association to PessoaJuridica
	@ManyToOne
	@JoinColumn(name="PEJ_CODIGO")
	private PessoaJuridica pessoaJuridica;
	
	//bi-directional many-to-one association to PessoaFisica
	@ManyToOne(cascade = {CascadeType.PERSIST })
	@JoinColumn(name="PEF_CODIGO")
	private PessoaFisica pessoaFisica;

	@Column(name="PJP_DTINICIO")
	//@NotNull(message="A data de inicío do exercício é obrigatório")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date inicio;
	
	@Column(name="PJP_DTFIM")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataFim;
	
	@Column(name="PJP_ATIVO")
	private int ativo;

	
	@ManyToOne
	@JoinColumn(name="FPJ_CODIGO") 
	private FuncaoPessoaJuridica funcaoPessoaJuridica;
	
	
	@Transient
	private String profissoesIds;
	
	
	public String getProfissoesIds() {
		return profissoesIds;
	}
	
	public String carregarProfissoesIds() {
		
		profissoesIds = "0,";
		for (PessoaFisicaProfissao pessoaFisicaProfissao : pessoaFisica.getPessoaFisicaProfissoes()) {
			profissoesIds+=pessoaFisicaProfissao.getProfissao().getId()+",";
		}
		return profissoesIds+"0";
	}

	public void setProfissoesIds(String profissoesIds) {
		this.profissoesIds = profissoesIds;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public FuncaoPessoaJuridica getFuncaoPessoaJuridica() {
		return funcaoPessoaJuridica;
	}

	public void setFuncaoPessoaJuridica(FuncaoPessoaJuridica funcaoPessoaJuridica) {
		this.funcaoPessoaJuridica = funcaoPessoaJuridica;
	}

	public boolean isNovo() {
		return id==0;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	

	public void setPessoajuridica(PessoaJuridica pessoajuridica) {
		this.pessoaJuridica = pessoajuridica;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridicaPessoaFisica other = (PessoaJuridicaPessoaFisica) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}