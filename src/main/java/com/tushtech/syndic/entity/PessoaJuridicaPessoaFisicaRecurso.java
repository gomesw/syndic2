package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name="RECURSO_PESSOAJURIDICA_PESSOAFISICA")
public class PessoaJuridicaPessoaFisicaRecurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="RJP_CODIGO",unique=true, nullable=false)
	private int id;

	@JoinColumn(name="PJP_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope=Perfil.class)
	@JsonIdentityReference(alwaysAsId = true)
	private PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica;

	//bi-directional many-to-one association to Recurso
	@JoinColumn(name="REC_CODIGO", nullable=false)
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Recurso recurso;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PessoaJuridicaPessoaFisica getPessoaJuridicaPessoaFisica() {
		return pessoaJuridicaPessoaFisica;
	}

	public void setPessoaJuridicaPessoaFisica(PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica) {
		this.pessoaJuridicaPessoaFisica = pessoaJuridicaPessoaFisica;
	}

	public Recurso getRecurso() {
		return recurso;
	}

	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridicaPessoaFisicaRecurso other = (PessoaJuridicaPessoaFisicaRecurso) obj;
		if (id != other.id)
			return false;
		return true;
	}

	




}