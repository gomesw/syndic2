package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;


@Entity
@Table(name="RACA")
public class Raca implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="RAC_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe a cor")
	@Column(name="RAC_NOME")
	private String nome;
	
	@Column(name = "RAC_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	@NotNull(message="Tipo de Animal é Obrigatório")
	@ManyToOne
	@JoinColumn(name="TIA_CODIGO")
	private TipoAnimal tipoAnimal;
	
//	//bi-directional many-to-one association to Animal
//	@OneToMany(mappedBy="raca")
//	private List<Animal> animal;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
    public boolean isNovo() {  
  		return id == 0;
  	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}
//
//	public List<Animal> getAnimal() {
//		return animal;
//	}
//
//	public void setAnimal(List<Animal> animal) {
//		this.animal = animal;
//	}

	public TipoAnimal getTipoAnimal() {
		return tipoAnimal;
	}

	public void setTipoAnimal(TipoAnimal tipoAnimal) {
		this.tipoAnimal = tipoAnimal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Raca other = (Raca) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	


}