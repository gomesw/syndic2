package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;


@Entity
@Table(name="RECURSO")
public class Recurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="REC_CODIGO")
	private int id;

	@Column(name="REC_ATIVO")
	private int ativo;

	@NotBlank(message="Descrição é Obrigatório")
	@Column(name="REC_DESCRICAO")
	private String descricao;

	@NotBlank(message="Nome é Obrigatório")
	@Column(name="REC_NOME")
	private String nome;

	//bi-directional many-to-one association to PerfilRecurso
	@OneToMany(mappedBy="recurso")
	private List<PerfilRecurso> lisperfilecursos;
	
	
	

	public Recurso() {
	}


	
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getAtivo() {
		return ativo;
	}



	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}



	public String getDescricao() {
		return descricao;
	}



	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}



	public String getNome() {
		return nome;
	}



	public void setNome(String nome) {
		this.nome = nome;
	}



	public List<PerfilRecurso> getLisperfilecursos() {
		return lisperfilecursos;
	}



	public void setLisperfilecursos(List<PerfilRecurso> lisperfilecursos) {
		this.lisperfilecursos = lisperfilecursos;
	}



	public PerfilRecurso addListPerfilRecurso(PerfilRecurso perfilRecurso) {
		getLisperfilecursos().add(perfilRecurso);
		perfilRecurso.setRecurso(this);
		return perfilRecurso;
	}

	public PerfilRecurso removeListPerfilRecurso(PerfilRecurso perfilRecurso) {
		getLisperfilecursos().remove(perfilRecurso);
		perfilRecurso.setRecurso(null);
		return perfilRecurso;
	}
	
	public boolean isNovo() {
		
		return this.id == 0;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Recurso other = (Recurso) obj;
		if (id != other.id)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Recurso [id=" + id + ", ativo=" + ativo + ", "
				+ (descricao != null ? "descricao=" + descricao + ", " : "")
				+ (nome != null ? "nome=" + nome + ", " : "")
				+ (lisperfilecursos != null ? "lisperfilecursos=" + lisperfilecursos : "") + "]";
	}

	
	
}