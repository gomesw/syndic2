package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the RegimeTrabalho database table.
 * 
 */
@Entity
@Table(name="REGIMETRABALHO")
//@NamedQuery(name="TipoConteudo.findAll", query="SELECT t FROM TipoConteudo t")
public class RegimeTrabalho implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="REG_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="REG_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private int ativo;

	@Column(name="REG_NOME")
	private String nome;
	
	@Column(name="REG_NRHORASSEMANA")
	private int NumeroHorasSemana;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getNumeroHorasSemana() {
		return NumeroHorasSemana;
	}

	public void setNumeroHorasSemana(int numeroHorasSemana) {
		NumeroHorasSemana = numeroHorasSemana;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegimeTrabalho other = (RegimeTrabalho) obj;
		if (id != other.id)
			return false;
		return true;
	}	
	
}