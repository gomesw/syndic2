package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * The persistent class for the reserva database table.
 * 
 */
@Entity
@Table(name="RESERVA")
public class Reserva implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="RES_CODIGO",unique=true, nullable=false)
	private int id; 
	
	//bi-directional many-to-one association to Unidade
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="PEF_CODIGO", referencedColumnName = "PEF_CODIGO")
	private PessoaFisica idPessoaFisica;
	
	//bi-directional many-to-one association to Unidade
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name="ACF_CODIGO", referencedColumnName = "ACF_CODIGO")
	private AreaComumFuncionamento areaComumFuncionamento;
	
	@Column(name="RES_DTCADASTRO")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dataCadastro;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PessoaFisica getIdPessoaFisica() {
		return idPessoaFisica;
	}

	public void setIdPessoaFisica(PessoaFisica idPessoaFisica) {
		this.idPessoaFisica = idPessoaFisica;
	}

	public AreaComumFuncionamento getAreaComumFuncionamento() {
		return areaComumFuncionamento;
	}

	public void setAreaComumFuncionamento(AreaComumFuncionamento areaComumFuncionamento) {
		this.areaComumFuncionamento = areaComumFuncionamento;
	}
	
	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reserva other = (Reserva) obj;
		if (id != other.id)
			return false;
		return true;
	}

}