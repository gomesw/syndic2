package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the funcao database table.
 * 
 */
@Entity
@Table(name="STATUSATENDIMENTO")
public class StatusAtendimento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="STA_CODIGO",unique=true, nullable=false)
	private int id;

//	@NotBlank(message="Informe o nome do Atendimento")
	@Column(name="STA_NOME")
	private String nome;
	
	@Column(name="STA_ATIVO")
	private int ativo;

	//bi-directional many-to-one association to Funcionario
	@OneToMany(mappedBy="statusChamado")
	private List<AtendimentoStatus> atendimentoStatus; 

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public java.util.List<AtendimentoStatus> getAtendimentoStatus() {
		return atendimentoStatus;
	}

	public void setAtendimentoStatus(java.util.List<AtendimentoStatus> atendimentoStatus) {
		this.atendimentoStatus = atendimentoStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusAtendimento other = (StatusAtendimento) obj;
		if (id != other.id)
			return false;
		return true;
	}	

//	public List<Atendimento> getAtendimentos() {
//		return atendimentos;
//	}
//
//	public void setAtendimentos(List<Atendimentos> atendimentos) {
//		this.atendimentos = Atendimentos;
//	}
	
	
}