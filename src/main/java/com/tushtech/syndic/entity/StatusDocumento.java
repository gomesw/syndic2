package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the statusdocumento database table.
 * 
 */
@Entity
@Table(name="STATUSDOCUMENTO")
//@NamedQuery(name="tipoEquipamento.findAll", query="SELECT t FROM statusDocumento t")
public class StatusDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="STD_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="STD_NOME")
	private String nome;
	
	@Column(name = "STD_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

	//bi-directional many-to-one association to Documento
	@OneToMany(mappedBy="statusDocumento")
	private List<Documento> documento;

	public int getId() {
		return id;
	}
    public boolean isNovo() {  
  		return id == 0;
  	}
	
	
	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
	public Integer getAtivo() {
		return ativo;
	}
	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusDocumento other = (StatusDocumento) obj;
		if (id != other.id)
			return false;
		return true;
	}

	
}