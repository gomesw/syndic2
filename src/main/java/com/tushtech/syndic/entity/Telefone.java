package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the telefone database table.
 * 
 */
@Entity
@Table(name="TELEFONE")
//@NamedQuery(name="Telefone.findAll", query="SELECT t FROM Telefone t")
public class Telefone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TEL_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="TEL_DDD")
	private String ddd;

	@Column(name="TEL_NR")
	private String numero;
	
	@Column(name = "TEL_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

	//bi-directional many-to-one association to Pessoafisica
	@ManyToOne
	@JoinColumn(name="PEF_CODIGO")
	private PessoaFisica pessoaFisica;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String ddd) {
		this.ddd = ddd;
	}

	

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}
		
	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	public String getNumeroFormatado(){
		
		numero = numero.replace("-", "");
		String vRetorno = "";
		if(numero!= null && numero.length()==8){
			vRetorno = (ddd!=null?"("+ddd.trim()+") ":"")
					+numero.substring(0,4)+"-"+
					numero.substring(4,numero.length());
			numero = vRetorno;
		}
		else if(numero!= null && numero.length()==9){
			vRetorno = (ddd!=null?"("+ddd.trim()+") ":"")
			
					+numero.substring(0,5)+"-"+
					numero.substring(5,numero.length());
			numero = vRetorno;
		}
		
		else{
			vRetorno = numero;
		}
		return numero;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Telefone other = (Telefone) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Telefone [id=" + id + ", " + (ddd != null ? "ddd=" + ddd + ", " : "")
				+ (numero != null ? "numero=" + numero + ", " : "")
				+ (pessoaFisica != null ? "pessoaFisica=" + pessoaFisica : "") + "]";
	}
	
	
	
	
	

}