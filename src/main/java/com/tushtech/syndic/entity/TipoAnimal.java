package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;


/**
 * The persistent class for the tipoanimail database table.
 * 
 */
@Entity
@Table(name="TIPOANIMAL")
//@NamedQuery(name="tipoAnimal.findAll", query="SELECT t FROM tipoAnimal t")
public class TipoAnimal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TIA_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe o Tipo de Animal")
	@Column(name="TIA_NOME")
	private String nome;
	
	@Column(name = "TIA_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

//	//bi-directional many-to-one association to Animai
//	@OneToMany(mappedBy="tipoAnimal")
//	private List<Raca> raca;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
    public boolean isNovo() {  
  		return id == 0;
  	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}
	
//	public List<Raca> getRaca() {
//		return raca;
//	}
//
//	public void setRaca(List<Raca> raca) {
//		this.raca = raca;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoAnimal other = (TipoAnimal) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}