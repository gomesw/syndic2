package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the funcao database table.
 * 
 */
@Entity
@Table(name="TIPOATENDIMENTO")
public class TipoAtendimento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TAT_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe o Tipo de Atendimento")
	@Column(name="TAT_NOME")
	private String nome;
	
	@Column(name="TAT_ATIVO")
	private int ativo;
	
	
	@Column(name="TAT_PRIORIDADE")
	private Integer prioridade = 1;
	
	
	

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TAT_SUBORDINACAO",referencedColumnName = "TAT_CODIGO")
	private TipoAtendimento idSubordinacao;
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "idSubordinacao", fetch = FetchType.LAZY)
    private List<TipoAtendimento> tipoAtendimentosFilhos;
	
//	@JsonIgnore
//	//bi-directional many-to-one association to Funcionario
//	@OneToMany(mappedBy="tipoAtendimento")
//	private List<Atendimento> atendimento;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	
	public Integer getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(Integer prioridade) {
		this.prioridade = prioridade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

//	public List<Atendimento> getAtendimento() {
//		return atendimento;
//	}
//
//	public void setAtendimento(List<Atendimento> atendimento) {
//		this.atendimento = atendimento;
//	}



	public TipoAtendimento getIdSubordinacao() {
		return idSubordinacao;
	}

	public void setIdSubordinacao(TipoAtendimento idSubordinacao) {
		this.idSubordinacao = idSubordinacao;
	}

	public List<TipoAtendimento> getTipoAtendimentosFilhos() {
		return tipoAtendimentosFilhos;
	}

	public void setTipoAtendimentosFilhos(List<TipoAtendimento> tipoAtendimentosFilhos) {
		this.tipoAtendimentosFilhos = tipoAtendimentosFilhos;
	}
	
	

//	public List<Atendimento> getAtendimentos() {
//		return atendimentos;
//	}
//
//	public void setAtendimentos(List<Atendimentos> atendimentos) {
//		this.atendimentos = Atendimentos;
//	}
}