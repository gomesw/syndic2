package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the tipocorrespondencia database table.
 * 
 */
@Entity
@Table(name="TIPOCONTASRECEBER")
public class TipoContasReceber implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TCR_CODIGO",unique=true, nullable=false)
	private int id;
	
	@Column(name="TCR_NOME")
	private String nome;
	
	@Column(name="TCR_ATIVO")
	private int ativo;
	
	@OneToMany(mappedBy="tipoContasReceber")
	private List<HistoricoFinanceiroContasReceber> historicoFinanceiro;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public List<HistoricoFinanceiroContasReceber> getHistoricoFinanceiro() {
		return historicoFinanceiro;
	}

	public void setHistoricoFinanceiro(List<HistoricoFinanceiroContasReceber> historicoFinanceiro) {
		this.historicoFinanceiro = historicoFinanceiro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoContasReceber other = (TipoContasReceber) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TipoContasReceber [id=" + id + ", " + (nome != null ? "nome=" + nome + ", " : "") + "ativo=" + ativo
				+ ", " + (historicoFinanceiro != null ? "historicoFinanceiro=" + historicoFinanceiro : "") + "]";
	}
	
}
