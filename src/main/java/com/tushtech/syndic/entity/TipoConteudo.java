package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;


/**
 * The persistent class for the tipoconteudo database table.
 * 
 */
@Entity
@Table(name="TIPOCONTEUDO")
//@NamedQuery(name="TipoConteudo.findAll", query="SELECT t FROM TipoConteudo t")
public class TipoConteudo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TIC_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="TIC_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private int ativo;

	@NotBlank(message="Tipo de conteudo é Obrigatório.")
	@Column(name="TIC_NOME")
	private String nome;

	//bi-directional many-to-one association to Conteudo
	@OneToMany(mappedBy="tipoConteudo")
	private List<Conteudo> conteudo;

	public TipoConteudo() {
	}

	public int getId() {
		return id;
	}

    public boolean isNovo() {  
  		return id == 0;
  	}	

	public void setId(int id) {
		this.id = id;
	}


	public int getAtivo() {
		return ativo;
	}


	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public List<Conteudo> getConteudo() {
		return conteudo;
	}


	public void setConteudo(List<Conteudo> conteudo) {
		this.conteudo = conteudo;
	}
	
	public Conteudo addConteudo(Conteudo conteudo) {
		getConteudo().add(conteudo);
		conteudo.setTipoConteudo(this);

		return conteudo;
	}

	public Conteudo removeConteudo(Conteudo conteudo) {
		getConteudo().remove(conteudo);
		conteudo.setTipoConteudo(null);

		return conteudo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoConteudo other = (TipoConteudo) obj;
		if (id != other.id)
			return false;
		return true;
	}	
	
}