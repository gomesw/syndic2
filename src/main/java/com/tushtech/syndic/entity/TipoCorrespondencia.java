package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the tipocorrespondencia database table.
 * 
 */
@Entity
@Table(name="TIPOCORRESPONDENCIA")
//@NamedQuery(name="tipoCorrespondencia.findAll", query="SELECT t FROM tipoCorrespondencia t")
public class TipoCorrespondencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TPC_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="TPC_NOME")
	private String nome;

	@Column(name = "TPC_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
	
	//bi-directional many-to-one association to Correspondencia
	@OneToMany(mappedBy="tipoCorrespondencia")
	private List<Correspondencia> correspondencia;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Correspondencia> getCorrespondencia() {
		return correspondencia;
	}

	public void setCorrespondencia(List<Correspondencia> correspondencia) {
		this.correspondencia = correspondencia;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoCorrespondencia other = (TipoCorrespondencia) obj;
		if (id != other.id)
			return false;
		return true;
	}

	
}