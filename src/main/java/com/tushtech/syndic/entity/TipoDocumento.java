package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;


/**
 * The persistent class for the tipoequipamento database table.
 * 
 */
@Entity
@Table(name="TIPODOCUMENTO")
//@NamedQuery(name="tipoEquipamento.findAll", query="SELECT t FROM tipoEquipamento t")
public class TipoDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TID_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="TID_NOME")
	private String nome;
	
	@Column(name = "TID_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

//	//bi-directional many-to-one association to Documento
//	@OneToMany(mappedBy="tipoDocumento")
//	private List<Documento> documento;

	
	
	public int getId() {
		return id;
	}
    public boolean isNovo() {  
  		return id == 0;
  	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getAtivo() {
		return ativo;
	}
	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}
//	public List<Documento> getDocumento() {
//		return documento;
//	}
//	public void setDocumento(List<Documento> documento) {
//		this.documento = documento;
//	}
	
	
	@PrePersist
	public void setaAtivo() {
		this.ativo = 1;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoDocumento other = (TipoDocumento) obj;
		if (id != other.id)
			return false;
		return true;
	}

	

}