package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * The persistent class for the tipoenderecounidade database table.
 * 
 */
@Entity
@Table(name="TIPOENDERECOUNIDADE")
//@NamedQuery(name="Tipoenderecounidade.findAll", query="SELECT t FROM Tipoenderecounidade t")
public class TipoEnderecoUnidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TEU_CODIGO",unique=true, nullable=false)
	private int id;	
	
	@NotBlank(message="Informe a Descrição Para o Sub Tipo Endereço")
	@Column(name="TEU_DESCRICAO")
	private String descricao;
	
	@Column(name = "TEU_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

	//bi-directional many-to-one association to Unidade
	@OneToMany(mappedBy="tipoEnderecoUnidade")
	@JsonIgnore
	private List<Unidade> unidade;
	
	//bi-directional many-to-one association to Condominio
	@JoinColumn(name = "CON_CODIGO", referencedColumnName = "CON_CODIGO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope=Condominio.class)
    @JsonIdentityReference(alwaysAsId = true)
	private Condominio condominio;
	
	@Transient
	public String nome;

	public TipoEnderecoUnidade() {
	}

    public boolean isNovo() {  
  		return id == 0;
  	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Unidade> getUnidade() {
		return unidade;
	}

	public void setUnidade(List<Unidade> unidade) {
		this.unidade = unidade;
	}

	public Condominio getCondominio() {
		return condominio;
	}

	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}
	
	
	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	
	
	public String getNome() {
		return this.descricao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@JsonProperty("condominio")
	public void setIdDependente(Integer idCondominio) {
		Condominio cond = new Condominio();
		cond.setId(idCondominio);
		this.condominio = cond;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoEnderecoUnidade other = (TipoEnderecoUnidade) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}