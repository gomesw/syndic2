package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the tipoequipamento database table.
 * 
 */
@Entity
@Table(name="TIPOEQUIPAMENTO")
//@NamedQuery(name="tipoEquipamento.findAll", query="SELECT t FROM tipoEquipamento t")
public class TipoEquipamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TIE_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="TIE_NOME")
	private String nome;

	//bi-directional many-to-one association to Equipamento
	@OneToMany(mappedBy="tipoEquipamento")
	private List<Equipamento> equipamento;

	public int getId() {
		return id;
	}
    public boolean isNovo() {  
  		return id == 0;
  	}
	
	
	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Equipamento> getEquipamento() {
		return equipamento;
	}

	public void setEquipamento(List<Equipamento> equipamento) {
		this.equipamento = equipamento;
	}

}