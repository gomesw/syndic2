package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;


@Entity
@Table(name="TIPOFUNCIONARIO")
public class TipoFuncionario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TIF_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe a cor")
	@Column(name="TIF_NOME")
	private String nome;
	
	@Column(name = "TIF_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;
	
//	@OneToMany(mappedBy="tipoFuncionario")
//	private List<PessoaFisicaUnidade> pessoaFisicaUnidade;
//	
//	@OneToMany(mappedBy="tipoFuncionario")
//	private List<PessoaFisicaCondominio> pessoaFisicaCondominio;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
    public boolean isNovo() {  
  		return id == 0;
  	}	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

//	public List<PessoaFisicaUnidade> getPessoaFisicaUnidade() {
//		return pessoaFisicaUnidade;
//	}
//
//	public void setPessoaFisicaUnidade(List<PessoaFisicaUnidade> pessoaFisicaUnidade) {
//		this.pessoaFisicaUnidade = pessoaFisicaUnidade;
//	}
//	
//	public List<PessoaFisicaCondominio> getPessoaFisicaCondominio() {
//		return pessoaFisicaCondominio;
//	}
//
//	public void setPessoaFisicaCondominio(List<PessoaFisicaCondominio> pessoaFisicaCondominio) {
//		this.pessoaFisicaCondominio = pessoaFisicaCondominio;
//	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoFuncionario other = (TipoFuncionario) obj;
		if (id != other.id)
			return false;
		return true;
	}


}