package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the tipoOrdemServico database table.
 * 
 */
@Entity
@Table(name="TIPOORDEMSERVICO")
public class TipoOrdemServico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TOS_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Informe o Tipo de Ordem de Serviço")
	@Column(name="TOS_NOME")
	private String nome;
	
	@Column(name="TOS_ATIVO")
	private int ativo;
	
	@Column(name="TOS_PRIORIDADE")
	private int prioridade;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name = "TOS_SUBORDINACAO",referencedColumnName = "TOS_CODIGO")
	private TipoOrdemServico tipoOrdemServicoSubordinacao;

	public boolean isNovo() {  
  		return id == 0;
  	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public int getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}

	public TipoOrdemServico getTipoOrdemServicoSubordinacao() {
		return tipoOrdemServicoSubordinacao;
	}

	public void setTipoOrdemServicoSubordinacao(TipoOrdemServico tipoOrdemServicoSubordinacao) {
		this.tipoOrdemServicoSubordinacao = tipoOrdemServicoSubordinacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoOrdemServico other = (TipoOrdemServico) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}