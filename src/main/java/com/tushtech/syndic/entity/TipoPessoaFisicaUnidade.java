package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the tipopessoa database table.
 * 
 */
@Entity
@Table(name="TIPOPESSOAFISICAUNIDADE")
//@NamedQuery(name="TipoPessoa.findAll", query="SELECT t FROM TipoPessoa t")
public class TipoPessoaFisicaUnidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TPF_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="TPF_ATIVO")
	private int ativo;

	@Column(name="TPF_NOME")
	private String nome;


	public int getId() {
		return id;
	}

    public boolean isNovo() {  
  		return id == 0;
  	}
	
	public void setId(int id) {
		this.id = id;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoPessoaFisicaUnidade other = (TipoPessoaFisicaUnidade) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

}