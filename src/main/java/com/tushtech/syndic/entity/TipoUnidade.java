package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the tipounidade database table.
 * 
 */
@Entity
@Table(name="TIPOUNIDADE")
//@NamedQuery(name="TipoUnidade.findAll", query="SELECT t FROM TipoUnidade t")
public class TipoUnidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TPU_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="TPU_NOME")
	private String nome;
	
	
	@Column(name="TPU_SIGLA")
	private String sigla;
	
	@Column(name = "TPU_ATIVO", nullable = false,  columnDefinition = "int default 1", insertable = false, updatable = true)
	private Integer ativo;

	//bi-directional many-to-one association to Unidade
//	@OneToMany(mappedBy="tipoUnidade")
//	private List<Unidade> unidade;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

//	public List<Unidade> getUnidade() {
//		return unidade;
//	}
//
//	public void setUnidade(List<Unidade> unidade) {
//		this.unidade = unidade;
//	}
	
	public Integer getAtivo() {
		return ativo;
	}

	public void setAtivo(Integer ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoUnidade other = (TipoUnidade) obj;
		if (id != other.id)
			return false;
		return true;
	}


}