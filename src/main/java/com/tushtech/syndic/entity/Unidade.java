package com.tushtech.syndic.entity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaUnidadeEnum;


/**
 * The persistent class for the unidade database table.
 * 
 */
@Entity
@Table(name="UNIDADE")
//@NamedQuery(name="Unidade.findAll", query="SELECT u FROM Unidade u")
public class Unidade implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="UNI_CODIGO",unique=true, nullable=false)
	private int id;

	@NotBlank(message="Unidade é obrigatório")
	@Column(name="UNI_UNIDADEHABITACIONAL")
	private String unidadeHabitacional;


	//bi-directional many-to-one association to Enderecounidade
	@NotNull(message="Tipo de Endereço é obrigatório")
	@ManyToOne
	@JoinColumn(name="ENU_CODIGO")
	@JsonIgnore
	private EnderecoUnidade enderecoUnidade;

	//bi-directional many-to-one association to Enderecounidade
	@NotNull(message="Sub - Tipo de Endereço é obrigatório")
	@ManyToOne
	@JoinColumn(name="TEU_CODIGO")
	@JsonIgnore
	private TipoEnderecoUnidade tipoEnderecoUnidade;



	//bi-directional many-to-one association to Tipounidade
	@NotNull(message="Tipo de Unidade é obrigatório")
	@ManyToOne
	@JoinColumn(name="TPU_CODIGO")
	@JsonIgnore
	private TipoUnidade tipoUnidade;

	//bi-directional many-to-one association to Animai
	@OneToMany(mappedBy="unidade")
	@JsonIgnore
	private List<Animal> animais;

	//bi-directional many-to-one association to Correspondencia
	@OneToMany(mappedBy="unidade")
	@JsonIgnore
	private List<Correspondencia> correspondencias= new ArrayList<>();

	//bi-directional many-to-one association to Equipamento
	@OneToMany(mappedBy="unidade")
	@JsonIgnore
	private List<Equipamento> equipamentos;

	//bi-directional many-to-one association to PessoafisicaUnidade
	@OneToMany(mappedBy="unidade")
	@JsonIgnore
	private List<PessoaFisicaUnidade> pessoaFisicaUnidade;
	
	
	//bi-directional many-to-one association to PessoafisicaUnidade
		@OneToMany(mappedBy="unidade")
		@JsonIgnore
		private List<Documento> documentos;

	@ManyToOne
	@JoinColumn(name="CON_CODIGO")
	@JsonIgnore
	private Condominio condominio;


	@OneToMany(mappedBy="unidade")
	@JsonIgnore
	private List<HistoricoFinanceiroContasReceber> historicoFinanceiro;

	
	
	@Transient
	public String nome;
	
	public boolean isNovo() {  
		return id == 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUnidadeHabitacional() {
		return unidadeHabitacional;
	}

	public void setUnidadeHabitacional(String unidadeHabitacional) {
		this.unidadeHabitacional = unidadeHabitacional;
	}

	public List<Animal> getAnimais() {
		return animais;
	}

	public void setAnimais(List<Animal> animais) {
		this.animais = animais;
	}

	public List<Correspondencia> getCorrespondencias() {
		return correspondencias;
	}

	public void setCorrespondencias(List<Correspondencia> correspondencias) {
		this.correspondencias = correspondencias;
	}

	public List<Equipamento> getEquipamentos() {
		return equipamentos;
	}

	public void setEquipamentos(List<Equipamento> equipamentos) {
		this.equipamentos = equipamentos;
	}

	public List<PessoaFisicaUnidade> getPessoaFisicaUnidade() {
		return pessoaFisicaUnidade;
	}

	public void setPessoaFisicaUnidade(List<PessoaFisicaUnidade> pessoaFisicaUnidade) {
		this.pessoaFisicaUnidade = pessoaFisicaUnidade;
	}


	public TipoUnidade getTipoUnidade() {
		return tipoUnidade;
	}

	public void setTipoUnidade(TipoUnidade tipoUnidade) {
		this.tipoUnidade = tipoUnidade;
	}


	public EnderecoUnidade getEnderecoUnidade() {
		return enderecoUnidade;
	}

	public void setEnderecoUnidade(EnderecoUnidade enderecoUnidade) {
		this.enderecoUnidade = enderecoUnidade;
	}

	public TipoEnderecoUnidade getTipoEnderecoUnidade() {
		return tipoEnderecoUnidade;
	}

	public void setTipoEnderecoUnidade(TipoEnderecoUnidade tipoEnderecoUnidade) {
		this.tipoEnderecoUnidade = tipoEnderecoUnidade;
	}



	public Condominio getCondominio() {
		return condominio;
	}

	public List<HistoricoFinanceiroContasReceber> getHistoricoFinanceiro() {
		return historicoFinanceiro;
	}

	public void setHistoricoFinanceiro(List<HistoricoFinanceiroContasReceber> historicoFinanceiro) {
		this.historicoFinanceiro = historicoFinanceiro;
	}

	

	public List<Documento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<Documento> documentos) {
		this.documentos = documentos;
	}
	
	
	

	public String getNome() {
		return this.getUnidadeHabitacionalDescricao();
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUnidadeHabitacionalDescricao() {

		String retorno = "";
		
		retorno+=this.getEnderecoUnidade().getDescricao()+" "+this.getTipoEnderecoUnidade().getDescricao()+" "+this.tipoUnidade.getNome()+" "+this.unidadeHabitacional;

		return retorno;
	}
	
	@JsonIgnore
	public List<PessoaFisicaUnidade> getListaProprietarioUnidade() {

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		if(pessoaFisicaUnidade!=null){
			for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
				if((pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()
						|| pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId())
						&& pfunidade.getDataFim()==null) {
					pessoaFisicaUnidadeRetorno.add(pfunidade);
				}

			}
		}

		return pessoaFisicaUnidadeRetorno;
	}
	
	
	@JsonIgnore
	public float getTotalPartes() {

		
		float retorno = 0F;
		
		if(pessoaFisicaUnidade!=null){
			for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
				if((pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()
						|| pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId())
						&& pfunidade.getDataFim()==null) {
					retorno +=(pfunidade.getParteImovel()==null?0:Float.valueOf(pfunidade.getParteImovel()));
				}

			}
		}

		return retorno;
	}


	
	
	@JsonIgnore
	public List<PessoaFisicaUnidade> getListaInquilinoUnidade() {

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		if(pessoaFisicaUnidade!=null){
			for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
				if((pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.INQUILINO.getId())
						&& pfunidade.getDataFim()==null) {
					pessoaFisicaUnidadeRetorno.add(pfunidade);
				}

			}
		}
		return pessoaFisicaUnidadeRetorno;
	}
	
	@JsonIgnore
	public List<PessoaFisicaUnidade> getListaPessoasUnidade() {

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		if(pessoaFisicaUnidade!=null){
			for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
				if(pfunidade.getDataFim()==null) {
					pessoaFisicaUnidadeRetorno.add(pfunidade);
				}

			}
		}
		return pessoaFisicaUnidadeRetorno;
	}


	@JsonIgnore
	public List<PessoaFisicaUnidade> getListaMoradoresUnidade() {

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		if(pessoaFisicaUnidade!=null){
			for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
				if(pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.MORADOR.getId()
						&& pfunidade.getDataFim()==null) {
					pessoaFisicaUnidadeRetorno.add(pfunidade);
				}

			}
		}

		return pessoaFisicaUnidadeRetorno;
	}


	@JsonIgnore
	public List<PessoaFisicaUnidade> getListaInquilinosUnidade() {

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		if(pessoaFisicaUnidade!=null){
			for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
				if(pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.INQUILINO.getId() && pfunidade.getDataFim()==null) {
					pessoaFisicaUnidadeRetorno.add(pfunidade);
				}

			}
		}

		return pessoaFisicaUnidadeRetorno;
	}


	@JsonIgnore
	public String getListaProprietarioUnidadeNomes() {

		String nomesretornos = "";
		if(pessoaFisicaUnidade!=null){
			int cont = 0;
			for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
				if((pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()
						|| pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId())
						&& pfunidade.getDataFim()==null) {
					if(cont==0){
						nomesretornos = nomesretornos+pfunidade.getPessoaFisica().getNome();
					}
					else{
						nomesretornos = nomesretornos+"<br/>"+pfunidade.getPessoaFisica().getNome();
					}
					cont++;
				}

			}
		}

		return nomesretornos;
	}


	@JsonIgnore
	public String getListaProprietarioUnidadeTelefones() {

		String nomesretornos = "";
		if(pessoaFisicaUnidade!=null){
			int cont = 0;
			for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
				if((pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()
						|| pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId())
						&& pfunidade.getDataFim()==null) {
					if(cont==0){
						nomesretornos = nomesretornos+pfunidade.getPessoaFisica().getTelefonesFormatados();
					}
					else{
						nomesretornos = nomesretornos+"<br/>"+pfunidade.getPessoaFisica().getTelefonesFormatados();
					}
					cont++;
				}

			}
		}

		return nomesretornos;
	}


	public String descricao(){

		String descricao = "";

		descricao = this.enderecoUnidade.getDescricao()+" "+this.tipoEnderecoUnidade.getDescricao()+" "+this.tipoUnidade.getNome()+" "+this.unidadeHabitacional;

		return descricao;

	}

	@JsonIgnore
	public List<PessoaFisicaUnidade> getListaTodosProprietarioUnidade() { 

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		
		if(this.pessoaFisicaUnidade!=null) {
		for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
			if(pfunidade.getDataFim()==null && (pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()
					|| pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId())) {
				pessoaFisicaUnidadeRetorno.add(pfunidade);
			} 

		}
		}
		return pessoaFisicaUnidadeRetorno;
	}

	
	
//	public List<Documento> getListaDocumentos(){
//		List<Documento> listaDocumentos = new ArrayList<>();
//		List<PessoaFisicaUnidade> listaPessoas =  getListaTodosProprietarioUnidade();
//		for (PessoaFisicaUnidade pfunidade : listaPessoas) {
//			for (Documento documento : pfunidade.getDocumentos()) {
//				listaDocumentos.add(documento);
//			}
//		}
//		return listaDocumentos;
//		
//	}
	
	
	
	public List<Documento> getListaDocumentos(){

		List<Documento> listaDocumentos = new ArrayList<>();

		for (Documento documento : this.documentos) {
			if(documento.getAtivo()==1) {
				listaDocumentos.add(documento);
			}
		}
		return listaDocumentos;

	}
	
	
	

	
	
	/**
	 * @return
	 * LISTA DE TODAS PESSOAS ATIVAS DA UNIDADE
	 * EXCLUIDO FUNCIONARIO E VISITANTE
	 * 
	 */
	public List<PessoaFisicaUnidade> getListaPessoasAtivasUnidade() { 

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
			if(pfunidade.getDataFim()==null 
					&& pfunidade.getTipoPessoaFisicaUnidade().getId()!=TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()
					&& pfunidade.getTipoPessoaFisicaUnidade().getId()!=TipoPessoaFisicaUnidadeEnum.VISITANTE.getId()) {
				pessoaFisicaUnidadeRetorno.add(pfunidade);
			} 

		}

		return pessoaFisicaUnidadeRetorno;
	}


	public List<PessoaFisicaUnidade> getListaFuncionariosUnidade() { 

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
			if(pfunidade.getDataFim()==null && pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()) {
				pessoaFisicaUnidadeRetorno.add(pfunidade);
			} 

		}

		return pessoaFisicaUnidadeRetorno;
	}


	/**
	 * @return
	 */
	public List<PessoaFisicaUnidade> getListaVisitantesAgendadosUnidade() { 

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); 
		Date hoje = new Date();
		
		String hojestring = dateFormat.format(hoje);  
		
		for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
			
			if(pfunidade.getDataFim()==null 
					&& pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.VISITANTE.getId()) {
				pessoaFisicaUnidadeRetorno.add(pfunidade);
			} 
			else if( pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.VISITANTE.getId() && 
					(pfunidade.getDataFim().toString().equals(hojestring) || hoje.before(pfunidade.getDataFim()))) {
				
				pessoaFisicaUnidadeRetorno.add(pfunidade);
			}

		}

		return pessoaFisicaUnidadeRetorno;
	}


	/**
	 * @return
	 */
	public List<PessoaFisicaUnidade> getListaVisitantesUnidade() { 

		List<PessoaFisicaUnidade> pessoaFisicaUnidadeRetorno = new ArrayList<PessoaFisicaUnidade>();
		for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
			if(pfunidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.VISITANTE.getId()) {
				pessoaFisicaUnidadeRetorno.add(pfunidade);
			} 

		}

		return pessoaFisicaUnidadeRetorno;
	}



	public List<PessoaFisicaVeiculo> getListaVeiculosUnidade() { 

		List<PessoaFisicaUnidade> listPessoaFisicaUnidade = new ArrayList<PessoaFisicaUnidade>();
		List<PessoaFisicaVeiculo> listPessoaFisicaVeiculo = new ArrayList<PessoaFisicaVeiculo>();
		
		if(this.pessoaFisicaUnidade!=null) {
		for (PessoaFisicaUnidade pfunidade : pessoaFisicaUnidade) {
			if((pfunidade.getDataFim()==null && 
					pfunidade.getTipoPessoaFisicaUnidade().getId()!=TipoPessoaFisicaUnidadeEnum.VISITANTE.getId()
					&& pfunidade.getTipoPessoaFisicaUnidade().getId()!=TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId())) {
				listPessoaFisicaUnidade.add(pfunidade);
			} 
		}

		for (PessoaFisicaUnidade pessoaFisicaUnidade : listPessoaFisicaUnidade) {
			for (PessoaFisicaVeiculo pessoaFisicaVeiculo : pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo()) {
				if(pessoaFisicaVeiculo!=null){
					listPessoaFisicaVeiculo.add(pessoaFisicaVeiculo);
				}
			}
		}
		}

		return listPessoaFisicaVeiculo;
	}



	public void setCondominio(Condominio condominio) {
		this.condominio = condominio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unidade other = (Unidade) obj;
		if (id != other.id)
			return false;
		return true;
	}



}