package com.tushtech.syndic.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;




/**
 * The persistent class for the veiculo database table.
 * 
 */
@Entity
@Table(name="VEICULO")
//@NamedQuery(name="Veiculo.findAll", query="SELECT v FROM Veiculo v")
public class Veiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="VEI_CODIGO",unique=true, nullable=false)
	private int id;

	@Column(name="VEI_PLACA")
	private String placa;
	
	@Column(name="VEI_ANO")
	private Integer ano;
	
	@Column(name="VEI_ANOMODELO")
	private Integer anomodelo;

	//bi-directional many-to-one association to Modelo
    @JoinColumn(name = "MOD_CODIGO", referencedColumnName = "MOD_CODIGO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope=Modelo.class)
    @JsonIdentityReference(alwaysAsId = true)
    private Modelo modelo;

	@ManyToOne
	@JoinColumn(name="CRR_CODIGO")
	private Cor cor;

	@ManyToOne
	@JoinColumn(name="PEF_CODIGOCADASTRANTE")
	private PessoaFisica cadastrante;
	
		
	public PessoaFisica getCadastrante() {
		return cadastrante;
	}

	public void setCadastrante(PessoaFisica cadastrante) {
		this.cadastrante = cadastrante;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public Integer getAnomodelo() {
		return anomodelo;
	}

	public void setAnomodelo(Integer anomodelo) {
		this.anomodelo = anomodelo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}
	
	@PrePersist @PreUpdate
    protected void onPlacaMaiuscula() {
		if(this.placa!=null) {
			this.placa = this.placa.toUpperCase();
		}
    }


	@JsonProperty("modelo")
	public void setModelo(Integer modelo1) {
		Modelo modelo = new Modelo();
		modelo.setId(modelo1);
		this.modelo = modelo;
	}

	public Cor getCor() {
		return cor;
	}

	public void setCor(Cor cor) {
		this.cor = cor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Veiculo other = (Veiculo) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	

	public String getDescricao() {
		String retorno = ""; 
		
		retorno += this.getModelo().getMarca().getTipoVeiculo().getNome()+" "+this.getModelo().getMarca().getNome()+"/"+this.getModelo().getNome()+" Ano/Modelo  ("+this.getAno()+"/"+this.getAnomodelo()+") Placa:"+this.getPlaca(); 
		
		return retorno;
	}	

	
	
	
}