package com.tushtech.syndic.entity.enumeration;

public enum CoresBootstrap {
	
	VERMELHO(1, "danger"),
	LARANJA(2, "warning"),
	ROXO(3, "success"),
	CINZA(4, "info"),
	MARROM(5, "default"),
	COR(100, "default");

	private int id;
	
	private String descricao;
	
	
	private CoresBootstrap(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
       
    }  
  
	
	
    public String getDescricao() {  
        return descricao;  
    }  
  
    public int getId() {  
        return id;  
    } 
    
    public String getPorNumero(int cod){
		
		for (CoresBootstrap cor : CoresBootstrap.values()){
			if (cor.id == cod)
				return cor.descricao;
		}
		return null;
	}
    
   
}
