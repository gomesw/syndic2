package com.tushtech.syndic.entity.enumeration;

public enum CoresHexadecimal {
	
	VERMELHO(1, "#FF0000"),
	LARANJA(2, "#FF8000"),
	ROXO(3, "#298A08"),
	CINZA(4, "#0080FF"),
	MARROM(5, "#6E6E6E"),
	COR(100, "#5F04B4");

	private int id;
	
	private String descricao;
	
	
	private CoresHexadecimal(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
       
    }  
  
	
	
    public String getDescricao() {  
        return descricao;  
    }  
  
    public int getId() {  
        return id;  
    } 
    
    public String getPorNumero(int cod){
		
		for (CoresHexadecimal cor : CoresHexadecimal.values()){
			if (cor.id == cod)
				return cor.descricao;
		}
		return null;
	}
    
   
}
