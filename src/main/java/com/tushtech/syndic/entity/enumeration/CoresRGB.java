package com.tushtech.syndic.entity.enumeration;

public enum CoresRGB {
	
	VERMELHO(1, "rgb(255, 99, 132)"),
	LARANJA(2, "rgb(255, 159, 64)"),
	ROXO(3, "rgb(153, 102, 255)"),
	CINZA(4, "rgb(201, 203, 207)"),
	MARROM(5, "rgb(92, 51, 23)"),
	AMARELO(6, "rgb(255, 205, 86)"),
	VERDE(7,"rgb(75, 192, 192)"),
	AZUL(8,"rgb(54, 162, 235)"),
	PRETO(9,"rgb(0, 0, 0)"),
	VERDEFLORESTA(10,"rgb(107,142,35)"),
	COR(11,"rgb(230,232,250)");
	

	private int id;
	
	private String descricao;
	
	
	private CoresRGB(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
       
    }  
  
	
	
    public String getDescricao() {  
        return descricao;  
    }  
  
    public int getId() {  
        return id;  
    } 
    
    public String getPorNumero(int cod){
		
		for (CoresRGB cor : CoresRGB.values()){
			if (cor.id == cod)
				return cor.descricao;
		}
		return null;
	}
    
   
}
