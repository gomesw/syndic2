package com.tushtech.syndic.entity.enumeration;

public enum MesesDoAno {
	
	JANEIRO(1, "Janeiro","Jan"),
	FEVEREIRO(2, "Fevereiro","Fev"),
	MARCO(3, "Março","Mar"),
	ABRIL(4, "Abril","Abr"),
	MAIO(5, "Maio","Mai"),
	JUNHO(6, "Junho","Jun"),
	JULHO(7, "Julho","Jul"),
	AGOSTO(8, "Agosto","Ago"),
	SETEMBRO(9, "Setembro","Set"),
	OUTUBRO(10, "Outubro","Out"),
	NOVEMBRO(11, "Novembro","Nov"),
	DEZEMBRO(12, "Dezembro","Dez"),
	MES(0, "Mês Inexistente","NaN");
	

	private int id;
	
	private String descricao;
	private String reduzida;
	
	private MesesDoAno(int id, String descricao,String reduzida) {  
        this.id = id;  
        this.descricao = descricao;  
        this.reduzida = reduzida;  
    }  
  
	public String getReduzida() {  
        return reduzida;  
    }  
	
    public String getDescricao() {  
        return descricao;  
    }  
  
    public int getId() {  
        return id;  
    } 
    
    public String getPorNumero(int cod){
		if (cod == 0)
			return "Mês Inexistente!";
		for (MesesDoAno mes : MesesDoAno.values()){
			if (mes.id == cod)
				return mes.reduzida;
		}
		return null;
	}
    
    public String getPorNumeroDesc(int cod){
		if (cod == 0)
			return "Mês Inexistente!";
		for (MesesDoAno mes : MesesDoAno.values()){
			if (mes.id == cod)
				return mes.descricao;
		}
		return null;
	}
}
