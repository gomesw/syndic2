package com.tushtech.syndic.entity.enumeration;

public enum StatusOrdemServico {

	ABERTA(1, "ABERTA"),
	VISITAEXECUTADA(2, "VISITA EXECUTADA"),
	COTACAODEMATERIAL(3, "COTAÇÃO DE MATERIAL"),
	SERVICOAUTORIZADO(4, "SERVIÇO AUTORIZADO"),
	COMPRADEMATERIAL(5, "COMPRA DE MATERIAL"),
	EMEXECUCAO(6, "EM EXECUÇÃO"),
	SERVICOEXECUTADO(7, "SERVIÇO EXECUTADO"),
	ACEITEDOSERVICO(8, "ACEITE DO SERVIÇO"),
	PESQUISADESATISFACAO(9, "PESQUISA DE SATISFAÇÃO"),
	CANCELADA(10, "CANCELADA"),
	FINALIZADA(11, "FINALIZADA"),
	GARANTIA(12, "GARANTIA");
	
	private int id;
	private String descricao;
	
	private StatusOrdemServico(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}  
	
	
}
