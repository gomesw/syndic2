package com.tushtech.syndic.entity.enumeration;

public enum TipoFotoOrdemServicoEnum {

	DURANTEABERTURA(1, "Durante Abertura"),
	DURANTEANDAMENTO(2, "Durante Andamento");
	
	private int id;
	private String descricao;
	
	private TipoFotoOrdemServicoEnum(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}  
	
	
}
