package com.tushtech.syndic.entity.enumeration;

public enum TipoPessoaFisica {

	PESSOAFISICAPESSOAJURIDICA(1, "Pessoa Fisica Vinculada a Pessoa Jurídica"),
	PESSOAFISICACONDOMINIO(2, "Pessoa Fisica Vinculada ao Condomínio"),
	PESSOAFISICAUNIDADE(3, "Pessoa Fisica Vinculada a Unidade");
	
	private int id;
	private String descricao;
	
	private TipoPessoaFisica(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}  
	
	
}
