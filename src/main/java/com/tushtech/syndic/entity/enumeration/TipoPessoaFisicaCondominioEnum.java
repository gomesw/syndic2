package com.tushtech.syndic.entity.enumeration;

public enum TipoPessoaFisicaCondominioEnum {

	ADMINISTRADORSISTEMA(1, "ADMINISTRADOR DO SISTEMA"),
	SINDICO(2, "SINDICO"),
	SUBSINDICO(3, "SUBSÍNDICO"),
	FUNCIONARIO(4,"FUNCIONÁRIO"),
	VISITANTE(5,"VISITANTE"),
	MORADOR(6,"MORADOR"),
	MEMBRO_DE_CONSELHO(7, "MEMBRO DE CONSELHO"),
	FUNCIONARIO_UNIDADE(8, "FUNCIONÁRIO UNIDADE");
	
	
	private int id;
	private String descricao;
	
	private TipoPessoaFisicaCondominioEnum(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}  
	
	
}
