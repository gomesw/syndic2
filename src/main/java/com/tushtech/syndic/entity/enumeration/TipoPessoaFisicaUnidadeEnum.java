package com.tushtech.syndic.entity.enumeration;

public enum TipoPessoaFisicaUnidadeEnum {

	PROPRIETARIO(1, "PROPRIETARIO"),
	MORADOR(2, "MORADOR"),
	INQUILINO(3, "INQUILINO"),
	VISITANTE(4, "VISITANTE"),
	COMODATO(5, "COMODATO"),
	PROPRIETARIODEPARTE(6, "PROPRIETÁRIO DE PARTE"),
	FUNCIONARIO(7, "FUNCIONARIO");
	
	

	private int id;
	private String descricao;
	
	private TipoPessoaFisicaUnidadeEnum(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}  
	
	
}
