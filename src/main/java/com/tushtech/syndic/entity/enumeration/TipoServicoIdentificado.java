package com.tushtech.syndic.entity.enumeration;

public enum TipoServicoIdentificado {
	
	BASICO_1(1, "Serviço Básico 1"),
	BASICO_2(2, "Serviço Básico 2"),
	INTERMEDIARIO(3, "Serviço Intermediário"),
	CRITICO(4, "Serviço Crítico");

	private int id;
	private String descricao;
	
	private TipoServicoIdentificado(int id, String descricao) {  
        this.id = id;  
        this.descricao = descricao;  
    }  
  
    public String getDescricao() {  
        return descricao;  
    }  
  
    public int getId() {  
        return id;  
    }

	public void setId(int id) {
		this.id = id;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	} 
	
	public static String getServico(int i){
		String retorno = "";
		
		for (TipoServicoIdentificado tipoServicoIdentificado : TipoServicoIdentificado.values()) {
			if(i == tipoServicoIdentificado.getId()){
				retorno = tipoServicoIdentificado.getDescricao();
			}
		}
		
		return retorno;
	}
}
