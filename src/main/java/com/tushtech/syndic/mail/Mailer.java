package com.tushtech.syndic.mail;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.util.StringUtils;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.Email;
import com.tushtech.syndic.entity.HistoricoOrdemServico;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaFisicaOrdemServico;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.HistoricoOrdemServicoRepository;
import com.tushtech.syndic.repository.OrdemServicoRepository;
import com.tushtech.syndic.repository.PessoaFisicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaOrdemServicoRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.storage.FotoStorage;


@Component
public class Mailer {
	
	private static Logger logger = LoggerFactory.getLogger(Mailer.class);

	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private TemplateEngine thymeleaf;
	
	@Autowired
	private FotoStorage fotoStorage;
	
	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;
	
	@Autowired
	private CondominioRepository condominioRepository;
	
	@Autowired
	private PessoaFisicaCondominioRepository pessoaFisicaCondominioRepository;
	
	@Autowired
	private PessoaFisicaOrdemServicoRepository pessoaFisicaOrdemServicoRepository;
	
	@Autowired
	private HistoricoOrdemServicoRepository historicoOrdemServicoRepository;
	
	@Autowired
	private OrdemServicoRepository ordemServicoRepository;

	@Async
	public void enviar(PessoaFisica pessoaFisica,String senha,Email pemail) {
		Context context = new Context(new Locale("pt", "BR"));
		
		context.setVariable("funcao", pessoaFisica.getPapeisTexo());
		context.setVariable("pf", pessoaFisica);
		context.setVariable("email", pemail);
		context.setVariable("senha", senha);
		
		Map<String, String> imgens = new HashMap<>();
		
		try {
			String email = thymeleaf.process("mail/MessagemSenha", context);
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			helper.setFrom("contato@condominiodireto.com.br");
			helper.setTo(pemail.getNome().trim());
			helper.setSubject("CONDOMÍNIO DIRETO - Senha de Acesso Sistema");
			helper.setText(email, true);
			
			//logo do condominio ou do systema
			//helper.addInline("logo", new ClassPathResource("static/images/logo-gray.png"));
			
			
			for (String cid : imgens.keySet()) {
				String[] fotoContentType = imgens.get(cid).split("\\|");
				String foto = fotoContentType[0];
				String contentType = fotoContentType[1];
				byte[] arrayFoto = fotoStorage.recuperar(foto,"logoCondominio");
				//helper.addInline(cid, new ByteArrayResource(arrayFoto), contentType);
				
				helper.addInline("logo", new ClassPathResource("assets/img/logo_sindic_azul.png"));
			}
		
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			logger.error("Erro enviando e-mail", e);
		}
	}
	
	
	@Async
	public void enviarEmailOS(UsuarioSistema usuario,OrdemServico ordemServico,String conteudo) {
		Context context = new Context(new Locale("pt", "BR"));
		
		
		ordemServico = ordemServicoRepository.ordemServicoCompletaPorId(ordemServico.getId());
		
		List<HistoricoOrdemServico> listHistoricoOrdemServico = historicoOrdemServicoRepository.historicoCompletoPorOs(ordemServico.getId());
		
	    ordemServico.setListHistoricoOrdemServico(listHistoricoOrdemServico);
		
		Map<String, String> imgens = new HashMap<>();
		
		String emails = "";
		
		Condominio condominio = condominioRepository .condominioComPessoaJuridica(ordemServico.getCondominio().getId());
		
		List<PessoaFisicaCondominio> listPessoaFisicaCondominio = pessoaFisicaCondominioRepository.findByCondominioId(ordemServico.getCondominio().getId());
		
		List<PessoaFisicaOrdemServico> listPessoaFisicaOrdemServico = pessoaFisicaOrdemServicoRepository.findByOrdemServicoId(ordemServico.getId());
		
//		for (PessoaFisicaOrdemServico pessoaFisicaOrdemServico : listPessoaFisicaOrdemServico) {
//			System.err.println(pessoaFisicaOrdemServico.getId());
//		}
		
		//ordemServico.setListPessoaFisicaOrdemServico(listPessoaFisicaOrdemServico);
		
		condominio.setPessoaFisicaCondominio(listPessoaFisicaCondominio);
		
		ordemServico.setCondominio(condominio);
		
		context.setVariable("ordemServico", ordemServico);
		
		
		//EMAILS DO PROPRIO CONDOMINIO
		if(condominio.getEmail1()!=null  && !StringUtils.isEmpty(condominio.getEmail1())) {
			emails +=condominio.getEmail1()+";"; 
		}
		
		if(condominio.getEmail2()!=null && !StringUtils.isEmpty(condominio.getEmail2()) ) {
			emails +=condominio.getEmail2()+";"; 
		}
		
		//SINDICO ATUAL DO CONDOMINIO
		PessoaFisica sindico =   pessoaFisicaRepository.pessoaCompletaPorId(condominio.sidicoAtual().getId());
		
		for (Email email: sindico.getEmails()) {
			if(email.getNome()!=null) {
				emails +=email.getNome()+";"; 
			} 
		}
		
		
		//emails do funcionário vinculado a OS
		List<PessoaFisica> funcionarios =  ordemServico.getResponsaveisExecucaoOS();
		for (PessoaFisica pessoaFisica : funcionarios) {
			
			PessoaFisica pessoa =   pessoaFisicaRepository.pessoaCompletaPorId(pessoaFisica.getId());
			
			for (Email email: pessoa.getEmails()) {
				if(email.getNome()!=null) {
					emails +=email.getNome()+";"; 
				}
			}
			
		}
		
		
		//EMAIL JCDHIEL EMAIL PADRÃO DA JC DIHEL
		PessoaFisica  jcdiehl = pessoaFisicaRepository.pessoaCompletaPorId(2);
		for (Email email: jcdiehl.getEmails()) {
			if(email.getNome()!=null) {
				emails +=email.getNome(); 
			}
		}
		System.err.println(emails);
		try {
			String email = thymeleaf.process("mail/MessagemControleOrdemServico", context);
			String[] to = emails.split(";") ;
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			
			helper.setFrom("contato@condominiodireto.com.br");
			helper.setTo(to);
			helper.setSubject("CONDOMÍNIO DIRETO - "+ordemServico.getUltimoHistoricoOrdemServico().
					getOrdemServicoStatus().
					getNome()+" -"+ordemServico.getCondominio().getPessoaJuridica().getNomeFantasia().trim()+"-"+LocalDateTime.now());
			helper.setText(email, true);
			
			//logo do condominio ou do systema
			//helper.addInline("logo", new ClassPathResource("static/images/logo-gray.png"));
			
			
			for (String cid : imgens.keySet()) {
				String[] fotoContentType = imgens.get(cid).split("\\|");
				String foto = fotoContentType[0];
				String contentType = fotoContentType[1];
				byte[] arrayFoto = fotoStorage.recuperar(foto,"logoCondominio");
				//helper.addInline(cid, new ByteArrayResource(arrayFoto), contentType);
				
				helper.addInline("logo", new ClassPathResource("assets/img/logo_sindic_azul.png"));
			}
		
			mailSender.send(mimeMessage);
		} catch (MessagingException e) {
			logger.error("Erro enviando e-mail", e);
		}
	}
	

}
