package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.AnexoCondominio;

@Repository
public interface AnexoCondominioRepositoy extends JpaRepository<AnexoCondominio, Integer>{

}
