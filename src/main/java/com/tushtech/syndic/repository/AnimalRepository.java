package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Animal;
@Repository
public interface AnimalRepository extends JpaRepository<Animal, Integer> {
	
	List<Animal> findByUnidadeId(Integer IdUnidade);

	Animal findById(Integer id);

}
