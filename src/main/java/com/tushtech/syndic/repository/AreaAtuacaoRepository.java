package com.tushtech.syndic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.AreaAtuacao;

@Repository
public interface AreaAtuacaoRepository extends JpaRepository<AreaAtuacao, Integer>{

	Optional<AreaAtuacao> findByNome(String nome);

	AreaAtuacao findById(int id);

	List<AreaAtuacao> findByAtivo(int i);

}
