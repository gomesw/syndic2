package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.AreaComum;

@Repository
public interface AreaComumRepository extends JpaRepository<AreaComum, Integer> {

}
