package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Atendimento;
import com.tushtech.syndic.repository.helper.AtendimentoRepositoryQueries;

@Repository
public interface AtendimentoRepository extends JpaRepository<Atendimento, Integer>,AtendimentoRepositoryQueries{

	List<Atendimento> findByPessoaFisicaId(int id);

	List<Atendimento> findByPessoaFisicaIdOrderByIdDesc(int id);

	List<Atendimento> findByCondominioId(int id);

	Atendimento findTop1ByCondominioIdOrderByProtocoloAsc(int id);

	List<Atendimento> findByPessoaFisicaIdOrderByProtocoloDesc(int id);

	List<Atendimento> findByCondominioIdOrderByTipoAtendimentoPrioridade(int id);


	int countByCondominioIdOrderByTipoAtendimentoPrioridade(int id);

	
}
