package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.AtendimentoResposta;

@Repository
public interface AtendimentoRespostaRepository extends JpaRepository<AtendimentoResposta, Integer>{

	List<AtendimentoResposta> findByAtendimentoId(int id);

}
