package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.AtendimentoStatus;

@Repository
public interface AtendimentoStatusRepository extends JpaRepository<AtendimentoStatus, Integer>{
	
}
