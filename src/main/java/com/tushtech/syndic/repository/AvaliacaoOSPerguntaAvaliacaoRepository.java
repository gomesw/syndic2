package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.AvaliacaoOSPerguntaAvaliacao;

@Repository
public interface AvaliacaoOSPerguntaAvaliacaoRepository extends JpaRepository<AvaliacaoOSPerguntaAvaliacao, Integer>{

}
