package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.AvaliacaoOrdemServico;

@Repository
public interface AvaliacaoOrdemServicoRepository extends JpaRepository<AvaliacaoOrdemServico, Integer>{

}
