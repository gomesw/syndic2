package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.CondominioModulo;

@Repository
public interface CondominioModuloRepository extends JpaRepository<CondominioModulo, Integer> {

}
