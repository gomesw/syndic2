package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.CondominioPacoteServico;

@Repository
public interface CondominioPacoteServicoRepository extends JpaRepository<CondominioPacoteServico, Integer>{

	List<CondominioPacoteServico> findByCondominioId(int id);

	List<CondominioPacoteServico> findByCondominioIdAndAtivo(int id, int i);

	List<CondominioPacoteServico> findByAtivo(int i);

}
