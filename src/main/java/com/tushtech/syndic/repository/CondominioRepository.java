package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.repository.helper.CondominioRepositoryQueries;

@Repository
public interface CondominioRepository extends JpaRepository<Condominio, Integer>,CondominioRepositoryQueries{

	List<Condominio> findByAtivo(int i);

	Condominio findById(Integer id);

	List<Condominio> findByIdAndAtivo(int intCondominio, int i);

}

