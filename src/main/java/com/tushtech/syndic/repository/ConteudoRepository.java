package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Conteudo;

@Repository
public interface ConteudoRepository extends JpaRepository<Conteudo, Integer> {
	
	List<Conteudo> findByTipoConteudoId(Integer idTipoConteudo);
	List<Conteudo> findByCondominioId(Integer idCondominio);
	
	@Query(value = "SELECT *FROM CONTEUDO WHERE NOW() BETWEEN COT_DTINICIO and COT_DTFIM and CON_CODIGO = ?1", nativeQuery = true)
	List<Conteudo> findByCondominioIdPeriodo(Integer idCondominio);
	
	
}
