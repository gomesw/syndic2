package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.CorAnimal;

@Repository
public interface CorAnimalRepository extends JpaRepository<CorAnimal, Integer>{

}
