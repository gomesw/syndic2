package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Cor;

@Repository
public interface CorRepository extends JpaRepository<Cor, Integer>{

	List<Cor> findByIdGreaterThanOrderByNomeAsc(int i);

	List<Cor> findByAtivo(int i);

}
