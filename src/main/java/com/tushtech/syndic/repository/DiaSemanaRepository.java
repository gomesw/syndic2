package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.DiaSemana;

@Repository
public interface DiaSemanaRepository extends JpaRepository<DiaSemana, Integer>{

}
