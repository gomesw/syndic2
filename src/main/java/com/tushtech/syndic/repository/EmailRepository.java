package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tushtech.syndic.entity.Email;

public interface EmailRepository extends JpaRepository<Email, Integer>{

	List<Email> findByPessoaFisicaId(int id);
	
	@Query(value = "SELECT *FROM EMAIL WHERE PEF_CODIGO = ?1 LIMIT 1", nativeQuery = true)
	Email findByIdPessoaFisicaOne(Integer idPessoFisica);

	Email findByNome(String nome);


	
}
