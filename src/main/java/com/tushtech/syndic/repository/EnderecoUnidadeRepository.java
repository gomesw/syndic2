package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.EnderecoUnidade;
@Repository
public interface EnderecoUnidadeRepository extends JpaRepository<EnderecoUnidade, Integer> {

	List<EnderecoUnidade> findByCondominioId(int IdCondominio);

	EnderecoUnidade findByCondominioIdAndDescricaoLike(int id,String descricao);

	EnderecoUnidade findById(Integer id);
	
}
