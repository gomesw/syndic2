package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.HistoricoFinanceiroContasReceber;

@Repository
public interface FinanceiroRepository extends JpaRepository<HistoricoFinanceiroContasReceber, Integer>{

}
