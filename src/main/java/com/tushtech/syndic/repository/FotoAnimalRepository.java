package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.FotoAnimal;

@Repository

public interface FotoAnimalRepository extends JpaRepository<FotoAnimal, Integer>{

	FotoAnimal findById(Integer id);

}
