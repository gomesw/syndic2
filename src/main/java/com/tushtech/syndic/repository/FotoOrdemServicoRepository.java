package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.FotoOrdemServico;

@Repository
public interface FotoOrdemServicoRepository extends JpaRepository<FotoOrdemServico, Integer>{

	FotoOrdemServico findById(Integer id);

	FotoOrdemServico findByNome(String nome);

}
