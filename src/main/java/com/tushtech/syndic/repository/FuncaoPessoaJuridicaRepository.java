package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.FuncaoPessoaJuridica;
import com.tushtech.syndic.entity.TipoVeiculo;
@Repository
public interface FuncaoPessoaJuridicaRepository extends JpaRepository<FuncaoPessoaJuridica, Integer> {


	List<TipoVeiculo> findByIdGreaterThanOrderByNomeAsc(int i);

	List<TipoVeiculo> findByAtivo(int i);

	List<FuncaoPessoaJuridica> findByPessoaJuridicaId(int id); 
}
