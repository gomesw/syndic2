package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.HistoricoOrdemServico;
import com.tushtech.syndic.repository.helper.HistoricoOrdemServicoRepositoryQueries;

@Repository
public interface HistoricoOrdemServicoRepository extends JpaRepository<HistoricoOrdemServico, Integer>,HistoricoOrdemServicoRepositoryQueries{
	
	HistoricoOrdemServico findByOrdemServicoIdAndDataFimIsNull(int ordemServico);

	List<HistoricoOrdemServico> findByOrdemServicoId(Integer id);

	HistoricoOrdemServico findByOrdemServicoIdAndOrdemServicoStatusId(int i, int a);

	HistoricoOrdemServico findById(int id);
	
}
