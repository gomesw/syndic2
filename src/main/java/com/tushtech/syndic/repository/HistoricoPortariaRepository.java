package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tushtech.syndic.entity.HistoricoPortaria;

public interface HistoricoPortariaRepository extends JpaRepository<HistoricoPortaria, Integer> {

	List<HistoricoPortaria> findByIdNot(int i);


}
