package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Login;

@Repository
public interface LoginRepository extends JpaRepository<Login, Integer>{

	List<Login> findByPessoaFisicaId(int id);

	List<Login> findByPessoaFisicaIdAndDataLogoutIsNull(int id);

}
