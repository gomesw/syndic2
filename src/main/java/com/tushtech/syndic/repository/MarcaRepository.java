package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Marca;
@Repository
public interface MarcaRepository extends JpaRepository<Marca, Integer> {




	List<Marca> findByIdGreaterThanOrderByNomeAsc(int i);

	List<Marca> findByAtivo(int i);

	List<Marca> findByTipoVeiculoIdOrderByNomeAsc(int i);


	

}
