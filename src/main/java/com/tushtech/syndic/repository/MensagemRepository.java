package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Mensagem;

@Repository
public interface MensagemRepository extends JpaRepository<Mensagem, Integer>{


	List<Mensagem> findByDestinatarioIdAndAtivoOrderByDataInclusaoDesc(int id, int i);

	List<Mensagem> findByDestinatarioIdAndAtivoAndAlertaOrderByDataInclusaoDesc(int id, int i, int j);

	List<Mensagem> findByDestinatarioIdAndDataRecebimentoIsNullAndAtivoOrderByDataInclusaoDesc(int id, int i);
	
	List<Mensagem> findByDestinatarioIdAndDataRecebimentoIsNullAndAtivo(int id, int i);
	
	List<Mensagem> findByDestinatarioIdAndDataRecebimentoIsNull(int id);
	
	Mensagem findById(int idmsg); 

}
