package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Modelo;


@Repository
public interface ModeloRepository extends JpaRepository<Modelo, Integer> {
	
	//Marca findByMarcaId(Integer IdMarca);
	List<Modelo> findByMarcaId(Integer IdMarca);
	List<Modelo> findByMarcaIdOrderByNomeAsc(int id);
		
	@Query(value = "select * from MARCA m inner join MODELO mm on mm.MAR_CODIGO = m.MAR_CODIGO", nativeQuery = true)
	List<Modelo> buscarModelosMarcas(); 
}
