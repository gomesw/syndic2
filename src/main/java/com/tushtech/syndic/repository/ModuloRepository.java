package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Modulo;

@Repository
public interface ModuloRepository extends JpaRepository<Modulo, Integer>{

}
