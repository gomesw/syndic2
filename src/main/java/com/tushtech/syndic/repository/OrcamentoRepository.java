package com.tushtech.syndic.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Orcamento;

@Repository
public interface OrcamentoRepository extends JpaRepository<Orcamento, Integer>{

	Orcamento findByOrdemServicoIdAndAtivo(int os, int ativo);

	Optional<Orcamento> findByOrdemServicoId(int ordemServico);

	Orcamento findByNome(String nome);

	Orcamento findById(Integer valueOf);

}
