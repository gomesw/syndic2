package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.repository.helper.OrdemServicoRepositoryQueries;

@Repository
public interface OrdemServicoRepository extends JpaRepository<OrdemServico, Integer>,OrdemServicoRepositoryQueries{
	
	OrdemServico findByAtendimentoId(Integer id);

	OrdemServico findByAtendimentoIdAndTipoOrdemServicoId(int atendimentoId, int tipoOrdemServicoId);

	List<OrdemServico> findByCondominioIdOrderByIdDesc(int id);

	List<OrdemServico> findByIdGreaterThan(int id);

	List<OrdemServico> findByCondominioIdOrderByEmergencia(int id);

	List<OrdemServico> findByOrderByEmergenciaDesc();

	OrdemServico findById(int id);

	List<OrdemServico> findByOrderByIdDesc(); 

	OrdemServico findFirstByOrderByIdAsc();
	
	OrdemServico findFirstByOrderByIdDesc();

}
