package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.OrdemServicoStatus;

@Repository
public interface OrdemServicoStatusRepository extends JpaRepository<OrdemServicoStatus, Integer>{

	OrdemServicoStatus findById(int i);
	
}
