package com.tushtech.syndic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tushtech.syndic.entity.PacoteServico;
public interface PacoteServicoRepository extends JpaRepository<PacoteServico, Integer>{

	Optional<PacoteServico> findByNome(String nome);

	List<PacoteServico> findByAtivo(int i);


}
