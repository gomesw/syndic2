package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Parentesco;
@Repository
public interface ParentescoRepository extends JpaRepository<Parentesco, Integer> {

}
