package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tushtech.syndic.entity.PerfilCondominioRecurso;



public interface PerfilCondominioRecursoRepository extends JpaRepository<PerfilCondominioRecurso, Integer>{
	
	@Query(value="Select pcr,rec,pc from PerfilCondominioRecurso pcr "
			+ " join fetch pcr.recurso rec "
			+ " join fetch pcr.perfilCondominio pc"
			+ " where pcr.perfilCondominio.id =:id ")
	List<PerfilCondominioRecurso> porIdPerfilCondominioConsulta(@Param("id") Integer id);

	//@Query(value = "select * from Funcao_Upm_lotacao u where u.upl_codigo = ?1 and u.fun_codigo = ?2", nativeQuery = true)
	//PerfilCondominioRecurso ConsultarFuncaoUpmLotacao(Integer idUpmLotacao, Integer idFuncao);	

}
