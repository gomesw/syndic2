package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tushtech.syndic.entity.PerfilCondominio;

public interface PerfilCondominioRepository  extends JpaRepository<PerfilCondominio,Integer>{

	PerfilCondominio findByCondominioIdAndPerfilId(int id, int id2);

	PerfilCondominio findByPerfilIdAndCondominioId(int id, int id2);

}
