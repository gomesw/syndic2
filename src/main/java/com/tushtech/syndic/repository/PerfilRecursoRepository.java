package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PerfilRecurso;



@Repository
public interface PerfilRecursoRepository extends JpaRepository<PerfilRecurso, Integer>{
	List<PerfilRecurso> findByPerfilId(int id);
}
