package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Perfil;



@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Integer> {

	List<Perfil> findByAtivo(int ativo);
	
	List<Perfil> findByNomeIn(String[] nomes);	
	
	Perfil findById(Integer id);

	Perfil findByNomeAndAtivo(String nome, int i);
	
}
