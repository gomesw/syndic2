package com.tushtech.syndic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PerguntaAvaliacao;

@Repository
public interface PerguntaAvaliacaoRepository extends JpaRepository<PerguntaAvaliacao, Integer>{

	List<PerguntaAvaliacao> findByAtivo(int i);

	Optional<PerguntaAvaliacao> findByNome(String nome);

}
