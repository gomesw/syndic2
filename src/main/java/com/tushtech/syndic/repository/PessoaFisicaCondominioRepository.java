package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tushtech.syndic.entity.PessoaFisicaCondominio;

public interface PessoaFisicaCondominioRepository extends JpaRepository<PessoaFisicaCondominio, Integer>{

	PessoaFisicaCondominio findByPessoaFisicaIdAndCondominioId(int pessoaFisicaId, int pessoaFisicaCondominioId);
	
//	List<PessoaFisicaCondominio> findByCondominioIdAndPessoaFisicaTipoPessoaId(int condominio,int tipo);

	List<PessoaFisicaCondominio> findByPessoaFisicaId(int id);

	int countByCondominioIdAndTipoPessoaFisicaCondominioId(Integer id, int i);

	List<PessoaFisicaCondominio> findByCondominioIdAndTipoPessoaFisicaCondominioId(Integer id, int i);

	List<PessoaFisicaCondominio> findByCondominioId(int id);

	//int countByCondominioIdAndPessoaFisicaTipoPessoaId(Integer id, int i);

}
