package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PessoaFisicaOrdemServico;

@Repository
public interface PessoaFisicaOrdemServicoRepository extends JpaRepository<PessoaFisicaOrdemServico, Integer>{

	List<PessoaFisicaOrdemServico> findByOrdemServicoId(int id);

	PessoaFisicaOrdemServico findById(int id);

}
