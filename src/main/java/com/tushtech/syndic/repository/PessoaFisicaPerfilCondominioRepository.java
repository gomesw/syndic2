package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tushtech.syndic.entity.PessoaFisicaPerfilCondominio;
import com.tushtech.syndic.repository.helper.PessoaFisicaPerfilCondominioRepositoryQueries;

public interface PessoaFisicaPerfilCondominioRepository extends 
JpaRepository<PessoaFisicaPerfilCondominio, Integer>,PessoaFisicaPerfilCondominioRepositoryQueries{

	
	

}
