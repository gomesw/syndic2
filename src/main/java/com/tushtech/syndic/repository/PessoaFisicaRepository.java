package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.repository.helper.PessoaFisicaRepositoryQueries;

@Repository
public interface PessoaFisicaRepository extends JpaRepository<PessoaFisica, Integer>,PessoaFisicaRepositoryQueries {

	PessoaFisica findById(Integer id);

	List<PessoaFisica> findByAtivoOrderByNomeAsc(int i);
	
	List<PessoaFisica> findByAtivoOrderByNome(int i);

	PessoaFisica findByCpf(String login);
}
