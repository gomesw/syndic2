package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PessoaFisicaUnidadeDiaSemana;

@Repository
public interface PessoaFisicaUnidadeDiaSemanaRepository extends JpaRepository<PessoaFisicaUnidadeDiaSemana, Integer>{

}
