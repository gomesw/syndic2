package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tushtech.syndic.entity.PessoaFisicaUnidadeRecurso;



public interface PessoaFisicaUnidadeRecursoRepository extends JpaRepository<PessoaFisicaUnidadeRecurso, Integer>{

	List<PessoaFisicaUnidadeRecurso> findByPessoaFisicaUnidadeId(int id);
	

}
