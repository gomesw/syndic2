package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.repository.helper.PessoaFisicaUnidadeRepositoryQueries;

@Repository
public interface PessoaFisicaUnidadeRepository extends JpaRepository<PessoaFisicaUnidade, Integer>,PessoaFisicaUnidadeRepositoryQueries {
	
	List<PessoaFisicaUnidade> findByUnidadeIdAndDataFimIsNull(Integer IdUnidade);
	List<PessoaFisicaUnidade> findByPessoaFisicaId(Integer IdPessoaFisica);
	
	PessoaFisicaUnidade findByPessoaFisicaIdAndDataFimIsNull(Integer IdPessoaFisica);
	
	PessoaFisicaUnidade findById(Integer id);
	List<PessoaFisicaUnidade> findByPessoaFisicaNomeContaining(String parametro);
} 
