package com.tushtech.syndic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PessoaFisicaVeiculo;

@Repository
public interface PessoaFisicaVeiculoRepository extends JpaRepository<PessoaFisicaVeiculo, Integer>{

	List<PessoaFisicaVeiculo> findByPessoaFisicaId(int id);

	PessoaFisicaVeiculo findById(Integer id);

	Optional<PessoaFisicaVeiculo> findByVeiculoPlaca(String upperCase);

}
