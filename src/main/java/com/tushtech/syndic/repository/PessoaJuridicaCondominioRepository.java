package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PessoaJuridicaCondominio;
@Repository

public interface PessoaJuridicaCondominioRepository extends JpaRepository<PessoaJuridicaCondominio, Integer>{

	List<PessoaJuridicaCondominio> findByCondominioId(int id);

	List<PessoaJuridicaCondominio> findByPessoaJuridicaId(int id);

}
