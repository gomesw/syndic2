package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisicaRecurso;

@Repository
public interface PessoaJuridicaPessoaFisicaRecursoRepository extends JpaRepository<PessoaJuridicaPessoaFisicaRecurso, Integer>{

}
