package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisica;

@Repository
public interface PessoaJuridicaPessoaFisicaRepository extends JpaRepository<PessoaJuridicaPessoaFisica, Integer>{

	PessoaJuridicaPessoaFisica findByPessoaFisicaId(int id);
	
	PessoaJuridicaPessoaFisica findById(Integer id);

	List<PessoaJuridicaPessoaFisica> findByPessoaJuridicaId(int id);
	

}
