package com.tushtech.syndic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.PessoaJuridica;

@Repository
public interface PessoaJuridicaRepository extends JpaRepository<PessoaJuridica, Integer>{

	List<PessoaJuridica> findByAtivo(int ativo);

	Optional<PessoaJuridica> findByCnpj(String cnpj);

	List<PessoaJuridica> findByAtivoAndCondominioIsNull(int i);

	PessoaJuridica findById(int id);

}

