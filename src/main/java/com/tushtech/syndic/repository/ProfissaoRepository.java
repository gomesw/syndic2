package com.tushtech.syndic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Profissao;

@Repository
public interface ProfissaoRepository extends JpaRepository<Profissao, Integer>{

	List<Profissao> findByAtivo(int i);

	Optional<Profissao> findByNome(String nome);

	List<Profissao> findByIdIn(Integer[] profissoesids);

}
