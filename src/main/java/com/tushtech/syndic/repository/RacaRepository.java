package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Raca;

@Repository
public interface RacaRepository extends JpaRepository<Raca, Integer>{

	List<Raca> findByTipoAnimalIdOrderByNomeAsc(Integer tipo);

}
