package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Recurso;


@Repository
public interface RecursoRepository extends JpaRepository<Recurso, Integer> {

	List<Recurso> findByAtivo(int ativo);
	
	//List<Recurso> findByNomeIn(String nome);
	
	List<Recurso> findByNomeIn(String[] nomes);
	
	Recurso findByNomeAndAtivo(String nome, Integer Ativo);

	List<Recurso> findByIdIn(Integer[] idsRecursos);

	
	//@Query("select * from recurso where nome in :lista");
}
