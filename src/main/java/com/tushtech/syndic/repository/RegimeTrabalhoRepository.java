package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.RegimeTrabalho;

@Repository
public interface RegimeTrabalhoRepository extends JpaRepository<RegimeTrabalho, Integer>{

}
