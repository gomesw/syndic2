package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tushtech.syndic.entity.Senhas;

public interface SenhasRepository extends JpaRepository<Senhas, Integer>{

	List<Senhas> findByPessoaFisicaId(int id);

}
