package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.StatusAtendimento;

@Repository
public interface StatusChamadoRepository extends JpaRepository<StatusAtendimento, Integer>{
	
	List<StatusAtendimento> findByAtivo(int Ativo);
}
