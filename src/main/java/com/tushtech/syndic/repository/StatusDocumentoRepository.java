package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.StatusDocumento;

@Repository
public interface StatusDocumentoRepository extends JpaRepository<StatusDocumento, Integer> {

	StatusDocumento findById(int i);

}
