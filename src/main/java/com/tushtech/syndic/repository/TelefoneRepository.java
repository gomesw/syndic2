package com.tushtech.syndic.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Telefone;


@Repository
public interface TelefoneRepository extends JpaRepository<Telefone,Integer>{

}
