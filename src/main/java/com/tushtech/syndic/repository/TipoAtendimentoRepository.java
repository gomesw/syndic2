package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.TipoAtendimento;
import com.tushtech.syndic.repository.helper.TipoAtendimentoRepositoryQueries;

@Repository
public interface TipoAtendimentoRepository extends JpaRepository<TipoAtendimento, Integer>,TipoAtendimentoRepositoryQueries{
	
	List<TipoAtendimento> findByAtivo(int Ativo); 

	//List<TipoAtendimento> findByAtendimentoSubordinacaoIsNull();

	List<TipoAtendimento> findByAtivoAndIdSubordinacaoIsNull(int ativo);

	List<TipoAtendimento> findByAtivoAndIdSubordinacaoIsNotNull(int i);

	List<TipoAtendimento> findByIdSubordinacaoId(Integer id);  
}
