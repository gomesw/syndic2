package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.TipoContasReceber;

@Repository
public interface TipoContasReceberRepository extends JpaRepository<TipoContasReceber, Integer>{

}
