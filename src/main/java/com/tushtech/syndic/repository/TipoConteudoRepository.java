package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.TipoConteudo;
@Repository
public interface TipoConteudoRepository extends JpaRepository<TipoConteudo, Integer> {
	
	List<TipoConteudo> findByAtivo(int ativo);
}
