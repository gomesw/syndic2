package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.TipoDocumento;

@Repository

public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, Integer>{

	TipoDocumento findById(Integer tipo);

}
