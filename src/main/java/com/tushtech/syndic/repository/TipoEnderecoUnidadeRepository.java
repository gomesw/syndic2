package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.TipoEnderecoUnidade;
@Repository
public interface TipoEnderecoUnidadeRepository extends JpaRepository<TipoEnderecoUnidade, Integer> {
	
	List<TipoEnderecoUnidade> findByCondominioId(int IdCondominio);

	TipoEnderecoUnidade findByCondominioIdAndDescricaoLike(int id, String descricao);

	TipoEnderecoUnidade findById(Integer id);

}
