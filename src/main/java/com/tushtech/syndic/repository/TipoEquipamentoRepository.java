package com.tushtech.syndic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.TipoEquipamento;
@Repository
public interface TipoEquipamentoRepository extends JpaRepository<TipoEquipamento, Integer> {
	

}
