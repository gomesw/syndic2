package com.tushtech.syndic.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.TipoOrdemServico;

@Repository
public interface TipoOrdemServicoRepository extends JpaRepository<TipoOrdemServico, Integer>{
	
	List<TipoOrdemServico> findByAtivo(int Ativo);

	Optional<TipoOrdemServico> findByNome(String nome);

	List<TipoOrdemServico> findByTipoOrdemServicoSubordinacaoId(Integer id);

	List<TipoOrdemServico> findByTipoOrdemServicoSubordinacaoIsNullAndAtivo(int i);
}
