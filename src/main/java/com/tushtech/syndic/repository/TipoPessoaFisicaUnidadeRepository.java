package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.TipoPessoaFisicaUnidade;

@Repository
public interface TipoPessoaFisicaUnidadeRepository extends JpaRepository<TipoPessoaFisicaUnidade, Integer> {


	List<TipoPessoaFisicaUnidade> findByIdIn(int[] tipos);

	TipoPessoaFisicaUnidade findById(int id);

	List<TipoPessoaFisicaUnidade> findByAtivo(int i);
	

}
