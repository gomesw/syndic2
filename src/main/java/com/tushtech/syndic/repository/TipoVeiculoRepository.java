package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.TipoVeiculo;
@Repository
public interface TipoVeiculoRepository extends JpaRepository<TipoVeiculo, Integer> {


	List<TipoVeiculo> findByIdGreaterThanOrderByNomeAsc(int i);

	List<TipoVeiculo> findByAtivo(int i); 
}
