package com.tushtech.syndic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.repository.helper.UnidadeRepositoryQueries;

@Repository
public interface UnidadeRepository extends JpaRepository<Unidade, Integer>,UnidadeRepositoryQueries {

	List<Unidade> findByEnderecoUnidadeCondominioId(Integer idCondominio);

	Unidade findById(int id);

	Unidade findById(Unidade unidade);

	List<Unidade> findByCondominioId(Integer condominio);



}
