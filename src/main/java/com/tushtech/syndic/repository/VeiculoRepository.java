package com.tushtech.syndic.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tushtech.syndic.entity.Veiculo;
@Repository
public interface VeiculoRepository extends JpaRepository<Veiculo, Integer> {

	Optional<Veiculo> findByPlaca(String upperCase);
	
	
}
