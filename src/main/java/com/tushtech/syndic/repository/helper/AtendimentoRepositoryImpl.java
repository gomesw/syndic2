package com.tushtech.syndic.repository.helper;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.tushtech.syndic.dto.AtendimentoMes;
import com.tushtech.syndic.dto.AtendimentoMesStatus;
import com.tushtech.syndic.entity.enumeration.CoresRGB;

public class AtendimentoRepositoryImpl implements AtendimentoRepositoryQueries{

	@PersistenceContext
	private EntityManager manager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AtendimentoMes> totalPorMes() {
		List<AtendimentoMes> vendasMes = manager.createNamedQuery("Atendimento.totalPorMes").getResultList();
		
		LocalDate hoje = LocalDate.now();
		for (int i = 1; i <= 6; i++) {
			String mesIdeal = String.format("%d/%02d", hoje.getYear(), hoje.getMonthValue());
			
			boolean possuiMes = vendasMes.stream().filter(v -> v.getMes().equals(mesIdeal)).findAny().isPresent();
			if (!possuiMes) {
				vendasMes.add(i - 1, new AtendimentoMes(mesIdeal, 0));
			}
			
			hoje = hoje.minusMonths(1);
		}
		
		return vendasMes;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AtendimentoMesStatus> totalPorMesStatus() {
		List<AtendimentoMesStatus> vendasMes = manager.createNamedQuery("Atendimento.totalPorMesStatus").getResultList();
		
		int cont = 1;
		for (AtendimentoMesStatus atendimentoMesStatus : vendasMes) {
			atendimentoMesStatus.setCor(CoresRGB.LARANJA.getPorNumero(cont));
			cont++;
		}
		
		return vendasMes;
	}

}
