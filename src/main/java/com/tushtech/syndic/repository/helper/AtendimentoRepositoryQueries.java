package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.dto.AtendimentoMes;
import com.tushtech.syndic.dto.AtendimentoMesStatus;

public interface AtendimentoRepositoryQueries {
	
	public List<AtendimentoMes> totalPorMes();

	public List<AtendimentoMesStatus> totalPorMesStatus();

}
