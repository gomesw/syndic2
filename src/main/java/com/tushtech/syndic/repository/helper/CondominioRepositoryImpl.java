package com.tushtech.syndic.repository.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;


public class CondominioRepositoryImpl implements CondominioRepositoryQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<PessoaFisicaCondominio> condominiosPorSindico(Integer id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaCondominio.class);
		criteria.createAlias("pessoaFisica", "pf", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("tipoPessoaFisicaCondominio", "tipo", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("condominio", "con", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("con.pessoaJuridica", "pj", JoinType.LEFT_OUTER_JOIN); 
		
		if(id!=0) {
			Criterion c1;

			c1 = Restrictions.eq("tipo.id", 2);

			Disjunction orExp = Restrictions.or(c1);
			criteria.add(orExp);

			criteria.add(Restrictions.eq("pf.id",id));
			criteria.add(Restrictions.isNull("dataFim"));
		}		
		
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaFisicaCondominio>) criteria.list();
	}

	@Override
	@Transactional(readOnly = true)
	public Condominio condominioComPessoaJuridica(Integer id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Condominio.class);
		criteria.createAlias("pessoaJuridica", "pj", JoinType.INNER_JOIN);
		criteria.createAlias("enderecoUnidade", "eu", JoinType.NONE);
		criteria.createAlias("tipoEnderecoUnidade", "teu", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("id",id));
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (Condominio) criteria.uniqueResult();
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public List<Condominio> condominioAtivos() {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Condominio.class);
		criteria.createAlias("pessoaJuridica", "pj", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisicaCondominio", "pfcon", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("pfcon.tipoPessoaFisicaCondominio", "tpfcon", JoinType.NONE);
		criteria.createAlias("pfcon.pessoaFisica", "pf", JoinType.NONE);
		criteria.add(Restrictions.eq("ativo",1));
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<Condominio>) criteria.list();
	}
}
