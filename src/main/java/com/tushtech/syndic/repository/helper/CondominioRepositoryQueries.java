package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;

public interface CondominioRepositoryQueries {

	public List<PessoaFisicaCondominio> condominiosPorSindico(Integer pessoa);
	
	
	public Condominio condominioComPessoaJuridica(Integer id);


	List<Condominio> condominioAtivos();
}
