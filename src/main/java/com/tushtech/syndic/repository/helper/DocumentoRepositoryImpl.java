package com.tushtech.syndic.repository.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import com.tushtech.syndic.dto.DocumentoFiltro;
import com.tushtech.syndic.entity.Documento;

public class DocumentoRepositoryImpl implements DocumentoRepositoryQueries{
	
	
	@PersistenceContext
	private EntityManager manager;
	



	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Documento> aplicarFiltro(DocumentoFiltro filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Documento.class);
	
		
		criteria.createAlias("unidade", "uni", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("uni.condominio", "con", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("uni.enderecoUnidade", "endereco", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("uni.tipoEnderecoUnidade", "tipoendereco", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.eq("con.id",filtro.getCondominio().getId()));
		
		
		Criterion c1;
		Criterion c2;
		Criterion c3;
		Criterion c4;
		Criterion c5;
		
		
		c1 = Restrictions.ilike("descricao", filtro.getDescricao(), MatchMode.ANYWHERE);
		
		c2 = Restrictions.eq("tipoDocumento.id",  filtro.getTipoDocumento()==null?0:filtro.getTipoDocumento().getId());
		
		c3 = Restrictions.eq("endereco.id", filtro.getEnderecoUnidade()==null?0:filtro.getEnderecoUnidade().getId());
		
		c4 = Restrictions.eq("tipoendereco.id", filtro.getTipoEnderecoUnidade()==null?0:filtro.getTipoEnderecoUnidade().getId());
		
		c5 = Restrictions.eq("unidade.id", filtro.getUnidade()==null?0:filtro.getUnidade().getId());
		
		Disjunction orExp = Restrictions.or(c1,c2,c3,c4,c5);
		criteria.add(orExp);
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<Documento>) criteria.uniqueResult(); 
	}

}
