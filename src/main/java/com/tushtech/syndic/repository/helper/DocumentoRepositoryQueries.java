package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.dto.DocumentoFiltro;
import com.tushtech.syndic.entity.Documento;

public interface DocumentoRepositoryQueries {

	List<Documento> aplicarFiltro(DocumentoFiltro filtro);

}
