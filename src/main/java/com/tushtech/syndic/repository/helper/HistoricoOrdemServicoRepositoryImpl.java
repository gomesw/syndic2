package com.tushtech.syndic.repository.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import com.tushtech.syndic.entity.HistoricoOrdemServico;

public class HistoricoOrdemServicoRepositoryImpl implements HistoricoOrdemServicoRepositoryQueries{

	@PersistenceContext
	private EntityManager manager;
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<HistoricoOrdemServico> historicoCompletoPorOs(int id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(HistoricoOrdemServico.class);
		criteria.createAlias("ordemServicoStatus", "status", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("ordemServico", "os", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("os.id",id));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<HistoricoOrdemServico>) criteria.list();
	}

	
	
	
}
