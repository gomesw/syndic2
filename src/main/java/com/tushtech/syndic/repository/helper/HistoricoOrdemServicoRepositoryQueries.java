package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.entity.HistoricoOrdemServico;

public interface HistoricoOrdemServicoRepositoryQueries {
	
	List<HistoricoOrdemServico> historicoCompletoPorOs(int id);

}
