package com.tushtech.syndic.repository.helper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.tushtech.syndic.dto.OrdemServicoPorNaturezaDoServico;
import com.tushtech.syndic.dto.RelatorioConsumoCotasOSDTO;
import com.tushtech.syndic.dto.VisitanteDTO;
import com.tushtech.syndic.entity.OrdemServico;

public class OrdemServicoRepositoryImpl implements OrdemServicoRepositoryQueries{

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public List<OrdemServico> ordemServicoEmAndamento() {
		List<Integer> status = new ArrayList<>();
		status.add(1);
		status.add(2);
		status.add(3);
		status.add(4);
		status.add(5);
		status.add(6);
//		status.add(7);
		status.add(8);
		status.add(9);
		status.add(12);
		
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("histo.ordemServicoStatus", "status", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("condominio", "con", JoinType.NONE);
		criteria.add(Restrictions.isNull("histo.dataFim"));
		criteria.add(Restrictions.in("status.id",status));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
//		criteria.addOrder(Order.asc("id"));
		return (List<OrdemServico>) criteria.list();
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public List<OrdemServico> ordemServicoEmAndamentoFuncionario(Integer funcionario) {
		List<Integer> status = new ArrayList<>();
		status.add(1);
		status.add(2);
		status.add(3);
		status.add(4);
		status.add(5);
		status.add(6);
//		status.add(7);
		status.add(8);
		status.add(9);
//		status.add(10);
		
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);
		criteria.createAlias("listPessoaFisicaOrdemServico", "funcionario", JoinType.INNER_JOIN);
		criteria.createAlias("tipoOrdemServico", "tipo", JoinType.INNER_JOIN);
		criteria.createAlias("tipo.tipoOrdemServicoSubordinacao", "pai", JoinType.INNER_JOIN);
		criteria.createAlias("funcionario.pessoaFisica", "pffun", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("histo.ordemServicoStatus", "status", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("condominio", "con", JoinType.NONE);
		criteria.add(Restrictions.isNull("histo.dataFim"));
		criteria.add(Restrictions.eq("pffun.id",funcionario));
		criteria.add(Restrictions.in("status.id",status));
		criteria.addOrder(Order.desc("histo.dataInicio"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<OrdemServico>) criteria.list();
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemServico> ordemServicoEmAndamentoPorCondominio(Integer condominio) {
		List<Integer> status = new ArrayList<>();
		status.add(1);
		status.add(2);
		status.add(3);
		status.add(4);
		status.add(5);
		status.add(6);
//		status.add(7);
		status.add(8);
		status.add(9);
		status.add(12);
		
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);

		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("condominio", "con", JoinType.INNER_JOIN);
		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.INNER_JOIN);
		criteria.createAlias("histo.ordemServicoStatus", "status", JoinType.INNER_JOIN);
		criteria.add(Restrictions.isNull("histo.dataFim"));
		criteria.add(Restrictions.in("status.id",status));
		criteria.add(Restrictions.eq("con.id",condominio));
		criteria.addOrder(Order.desc("histo.dataInicio"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<OrdemServico>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<OrdemServico> osPorCondominioStatus(Integer condominio, Integer status) {
		
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);
		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.isNull("histo.dataFim"));
		
		if(condominio!=0){
			criteria.add(Restrictions.eq("condominio.id", condominio));				
		}
		
		if(status!=0){
			criteria.add(Restrictions.eq("histo.ordemServicoStatus.id", status));				
		}	
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<OrdemServico>) criteria.list(); 
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<OrdemServico> osPorCondominioPorMes(Integer condominio) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);
		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("condominio.id",condominio));
//		criteria.add(Restrictions.eq("histo.ordemServicoStatus.id",1));
		criteria.add(Restrictions.sqlRestriction("month(histo1_.HOS_DTINICIO) = '" + LocalDate.now().getMonthValue() + "' and year(histo1_.HOS_DTINICIO) = '" + LocalDate.now().getYear() + "'"));
		
		criteria.addOrder(Order.asc("id"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<OrdemServico>) criteria.list(); 
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<OrdemServico> osPorCondominioPorMesAno(Integer condominio,int mes,int ano) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);
		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("condominio.id",condominio));
		criteria.add(Restrictions.sqlRestriction("month(histo1_.HOS_DTINICIO) = '" +mes + "' and year(histo1_.HOS_DTINICIO) = '" + ano + "'"));
		criteria.add(Restrictions.eq("histo.ordemServicoStatus.id",1));
		criteria.addOrder(Order.asc("id"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<OrdemServico>) criteria.list(); 
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<OrdemServico> relatorioOSPorAnoMesCond(List<Integer> condominio,int mes,int ano) {
	
		
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);
		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.INNER_JOIN);
		criteria.createAlias("histo.ordemServicoStatus", "status", JoinType.INNER_JOIN);
		
//		if(condominio!=0) {
//			criteria.add(Restrictions.eq("condominio.id",condominio));
//		}
		
		criteria.add(Restrictions.in("condominio.id",condominio));
		
		if(ano!=0 && mes==0) {
			System.err.println("teste1 "+ano+" - "+mes);
			criteria.add(Restrictions.sqlRestriction("year(histo1_.HOS_DTINICIO) = '" + ano + "'"));
		}else if(ano==0 && mes!=0){
			System.err.println("teste2 "+ano+" - "+mes);
			criteria.add(Restrictions.sqlRestriction("month(histo1_.HOS_DTINICIO) = '" +mes + "'"));
		}else if(ano!=0 && mes!=0) {
			System.err.println("teste3 "+ano+" - "+mes);
			criteria.add(Restrictions.sqlRestriction("month(histo1_.HOS_DTINICIO) = '" +mes + "' and year(histo1_.HOS_DTINICIO) = '" + ano + "'"));
		}
		
		criteria.add(Restrictions.eq("status.id",1));
		
		criteria.addOrder(Order.asc("histo.dataInicio"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<OrdemServico>) criteria.list(); 
		
	}
	
	
	@Transactional(readOnly = true)
	@Override
	public List<OrdemServico> osPorMesAno(int mes,int ano) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);
		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.sqlRestriction("month(histo1_.HOS_DTINICIO) = '" +mes + "' and year(histo1_.HOS_DTINICIO) = '" + ano + "'"));
		criteria.add(Restrictions.eq("histo.ordemServicoStatus.id",1));
		criteria.addOrder(Order.asc("id"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<OrdemServico>) criteria.list(); 
	}
	
	

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<OrdemServico> osPorCondominioAguardandoAvaliacao(Integer condominio) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);
		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("histo.ordemServicoStatus", "status", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("condominio.id",condominio));
		criteria.add(Restrictions.eq("status.id",7));
		criteria.add(Restrictions.isNull("histo.dataFim"));
		criteria.addOrder(Order.desc("histo.dataInicio"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<OrdemServico>) criteria.list(); 
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<OrdemServico> osOrdenada() {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);

		criteria.addOrder(Order.desc("emergencia"));
		criteria.addOrder(Order.asc("id"));
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<OrdemServico>) criteria.list(); 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemServicoPorNaturezaDoServico> porNatureza() {
		
		TypedQuery<OrdemServicoPorNaturezaDoServico> query = manager.createNamedQuery("busca.osPorTipoDeServico", OrdemServicoPorNaturezaDoServico.class);
		
		List<OrdemServicoPorNaturezaDoServico> listOrdemServicoPorNaturezaDoServico = query.getResultList();
		
		return listOrdemServicoPorNaturezaDoServico;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdemServicoPorNaturezaDoServico> porNaturezaMesAnoCond(List<Integer> cond, int mes, int ano) {
		
		TypedQuery<OrdemServicoPorNaturezaDoServico> query = manager.createNamedQuery("busca.porNaturezaMesAnoCond", OrdemServicoPorNaturezaDoServico.class);
		query.setParameter("mes", mes);
		query.setParameter("ano", ano);
		List<OrdemServicoPorNaturezaDoServico> listOsNatDTO = query.getResultList();
		
		return listOsNatDTO;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<RelatorioConsumoCotasOSDTO> consumoCotasOS() {
		
		TypedQuery<RelatorioConsumoCotasOSDTO> query = manager.createNamedQuery("busca.consumoCotasOS", RelatorioConsumoCotasOSDTO.class);
		List<RelatorioConsumoCotasOSDTO> listRelatorioConsumoCotasOSDTO = query.getResultList();
		
		return listRelatorioConsumoCotasOSDTO;
	}



	@Transactional(readOnly = true)
	@Override
	public OrdemServico ordemServicoCompletaPorId(int id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(OrdemServico.class);
		criteria.createAlias("condominio", "con", JoinType.INNER_JOIN);
		criteria.createAlias("listPessoaFisicaOrdemServico", "funcionarios", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("funcionarios.pessoaFisica", "funcionario", JoinType.INNER_JOIN);
		criteria.createAlias("funcionario.emails", "emailsfun", JoinType.INNER_JOIN);
//		criteria.createAlias("listHistoricoOrdemServico", "histo", JoinType.RIGHT_OUTER_JOIN);
//		criteria.createAlias("histo.ordemServicoStatus", "status", JoinType.RIGHT_OUTER_JOIN);
		criteria.add(Restrictions.eq("id",id));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (OrdemServico) criteria.uniqueResult(); 
	}
	
}
