package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.dto.OrdemServicoPorNaturezaDoServico;
import com.tushtech.syndic.dto.RelatorioConsumoCotasOSDTO;
import com.tushtech.syndic.entity.OrdemServico;

public interface OrdemServicoRepositoryQueries {
	
	List<OrdemServico> ordemServicoEmAndamento();
	
	OrdemServico ordemServicoCompletaPorId(int id);

	List<OrdemServico> ordemServicoEmAndamentoPorCondominio(Integer condominio);

	List<OrdemServico> ordemServicoEmAndamentoFuncionario(Integer funcionario);

	List<OrdemServico> osPorCondominioStatus(Integer condominio, Integer status);

	List<OrdemServico> osPorCondominioPorMes(Integer condominio);

	List<OrdemServico> osPorCondominioAguardandoAvaliacao(Integer condominio);

	List<OrdemServico> osOrdenada();

	List<OrdemServicoPorNaturezaDoServico> porNatureza();

	List<RelatorioConsumoCotasOSDTO> consumoCotasOS();

	List<OrdemServico> osPorCondominioPorMesAno(Integer condominio, int mes, int ano);

	List<OrdemServico> osPorMesAno(int mes, int ano);

	List<OrdemServico> relatorioOSPorAnoMesCond(List<Integer> condominio, int mes, int ano);

	List<OrdemServicoPorNaturezaDoServico> porNaturezaMesAnoCond(List<Integer> cond, int mes, int ano);

}
