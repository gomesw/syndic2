package com.tushtech.syndic.repository.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import com.tushtech.syndic.entity.PessoaFisicaPerfilCondominio;


public class PessoaFisicaPerfilCondominioRepositoryImpl implements PessoaFisicaPerfilCondominioRepositoryQueries {

	@PersistenceContext
	private EntityManager manager;
	

	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<PessoaFisicaPerfilCondominio> porIdPessoaFisica(Integer pessoa){
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaPerfilCondominio.class);
		criteria.createAlias("perfilCondominio", "pcon", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("pcon.perfil", "per", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("pcon.perfilCondominioRecurso", "pconr", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("pconr.recurso", "rec", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("pessoaFisica.id",pessoa));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaFisicaPerfilCondominio>) criteria.list();
		
	}


}
