package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.entity.PessoaFisicaPerfilCondominio;

public interface PessoaFisicaPerfilCondominioRepositoryQueries {

	
	public List<PessoaFisicaPerfilCondominio> porIdPessoaFisica(Integer pessoa);
	
}
