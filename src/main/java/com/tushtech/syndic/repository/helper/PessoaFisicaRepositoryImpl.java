package com.tushtech.syndic.repository.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;

import com.tushtech.syndic.dto.EsqueciSenhaDTO;
import com.tushtech.syndic.dto.VisitanteDTO;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisica;
import com.tushtech.syndic.entity.Senhas;
import com.tushtech.syndic.entity.UsuarioSistema;

public class PessoaFisicaRepositoryImpl implements PessoaFisicaRepositoryQueries{
	
	
	@PersistenceContext
	private EntityManager manager;
	
	@Transactional(readOnly = true)
	@Override
	public PessoaFisica porEmail(String email) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisica.class);
		criteria.createAlias("emails", "ema", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisicaCondominio", "pfcon", JoinType.INNER_JOIN);
		//criteria.createAlias("pessoaFisicaUnidade", "pfuni", JoinType.INNER_JOIN);
		criteria.createAlias("pfcon.tipoPessoaFisicaCondominio", "tipo", JoinType.INNER_JOIN);
		criteria.createAlias("pfcon.condominio", "con", JoinType.INNER_JOIN);
		criteria.createAlias("con.pessoaJuridica", "pj", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("ema.nome",email));
		criteria.add(Restrictions.isNull("pfcon.dataFim"));
		//criteria.add(Restrictions.isNull("pfuni.dataFim"));
		criteria.add(Restrictions.eq("ema.ativo",1)); 
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (PessoaFisica) criteria.uniqueResult(); 
	}
	
	@Transactional(readOnly = true)
	@Override
	public PessoaFisica porCpf(String cpf) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisica.class);
		criteria.createAlias("emails", "ema", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisicaCondominio", "pfcon", JoinType.INNER_JOIN);
		//criteria.createAlias("pessoaFisicaUnidade", "pfuni", JoinType.INNER_JOIN);
		criteria.createAlias("pfcon.tipoPessoaFisicaCondominio", "tipo", JoinType.INNER_JOIN);
		criteria.createAlias("pfcon.condominio", "con", JoinType.INNER_JOIN);
		criteria.createAlias("con.pessoaJuridica", "pj", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("cpf",cpf));
		criteria.add(Restrictions.isNull("pfcon.dataFim"));
		//criteria.add(Restrictions.isNull("pfuni.dataFim"));
		criteria.add(Restrictions.eq("ema.ativo",1)); 
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (PessoaFisica) criteria.uniqueResult(); 
	}
	
	
	@Transactional(readOnly = true)
	@Override
	public PessoaJuridicaPessoaFisica porEmailPJ(String email) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaJuridicaPessoaFisica.class);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("pf.emails", "ema", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaJuridica", "pj", JoinType.INNER_JOIN);
		criteria.add(Restrictions.eq("ema.nome",email));
		criteria.add(Restrictions.isNull("dataFim"));
		criteria.add(Restrictions.eq("ema.ativo",1)); 
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (PessoaJuridicaPessoaFisica) criteria.uniqueResult(); 
	}
	
	@Transactional(readOnly = true)
	@Override
	public PessoaJuridicaPessoaFisica porCpfPJ(String cpf) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaJuridicaPessoaFisica.class);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("pf.emails", "ema", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaJuridica", "pj", JoinType.INNER_JOIN);
		criteria.add(Restrictions.eq("pf.cpf",cpf));
		criteria.add(Restrictions.isNull("dataFim"));
		criteria.add(Restrictions.eq("ema.ativo",1)); 
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (PessoaJuridicaPessoaFisica) criteria.uniqueResult(); 
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Senhas> retornaSenhas(Integer pessoa){
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Senhas.class);
		criteria.add(Restrictions.eq("pessoaFisica.id",pessoa));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<Senhas>) criteria.list();
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VisitanteDTO> porParametro(String parametro) {
		
		
		TypedQuery<VisitanteDTO> query = manager.createNamedQuery("busca.porParametroVisitante", VisitanteDTO.class);
		query.setParameter("parametro", "%"+parametro+"%");
		List<VisitanteDTO> listVisitanteDTO = query.getResultList();
		
		return listVisitanteDTO;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<PessoaJuridicaPessoaFisica> funcionarioDisponivelPorOS(int idOS, @AuthenticationPrincipal UsuarioSistema usuario) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaJuridicaPessoaFisica.class);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
//		criteria.createAlias("pf.pessoaFisicaOrdemServico", "pfos", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("funcaoPessoaJuridica.id",4));  
		criteria.add(Restrictions.sqlRestriction("pf1_.PEF_CODIGO not in (select PEF_CODIGO from PESSOAFISICA_ORDEMSERVICO where ORD_CODIGO = " + idOS + ")"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaJuridicaPessoaFisica>) criteria.list();
	}
	
	
	
	@Transactional(readOnly = true)
	@Override
	public PessoaFisica porRecuperarSenha(EsqueciSenhaDTO esqueciSenhaDTO) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisica.class);
		criteria.createAlias("emails", "ema", JoinType.INNER_JOIN);
		//criteria.createAlias("listPessoasTipos", "pessotipopessoa", JoinType.LEFT_OUTER_JOIN);
		//criteria.createAlias("pessotipopessoa.tipoPessoa", "tipo", JoinType.INNER_JOIN);
//		criteria.createAlias("tipoFuncionario", "tF", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("ema.nome",esqueciSenhaDTO.getNome()));
		//criteria.add(Restrictions.eq("dataNascimento",esqueciSenhaDTO.getDataNascimento()));
		criteria.add(Restrictions.eq("cpf",esqueciSenhaDTO.getCpf().trim().replaceAll("\\.|-|/", "")));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (PessoaFisica) criteria.uniqueResult(); 
	}


	@Transactional(readOnly = true)
	@Override
	public PessoaFisica pessoaCompletaPorId(Integer id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisica.class);
		criteria.createAlias("emails", "ema", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("id",id)); 
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (PessoaFisica) criteria.uniqueResult(); 
	}
	
	
	

}
