package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.dto.EsqueciSenhaDTO;
import com.tushtech.syndic.dto.VisitanteDTO;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisica;
import com.tushtech.syndic.entity.Senhas;
import com.tushtech.syndic.entity.UsuarioSistema;

public interface PessoaFisicaRepositoryQueries {

	PessoaFisica porEmail(String email);
	
	
	PessoaFisica pessoaCompletaPorId(Integer id);

	List<Senhas> retornaSenhas(Integer pessoa);


	List<VisitanteDTO> porParametro(String parametro);

	PessoaJuridicaPessoaFisica porEmailPJ(String email);

	List<PessoaJuridicaPessoaFisica> funcionarioDisponivelPorOS(int idOS, UsuarioSistema usuario);

	PessoaFisica porRecuperarSenha(EsqueciSenhaDTO esqueciSenhaDTO);


	PessoaFisica porCpf(String cpf);


	PessoaJuridicaPessoaFisica porCpfPJ(String cpf);

}
