package com.tushtech.syndic.repository.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaUnidadeEnum;


public class PessoaFisicaUnidadeRepositoryImpl implements PessoaFisicaUnidadeRepositoryQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<PessoaFisicaUnidade> pessoasPorUnidadePorTipo(Integer id,Integer tipo) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaUnidade.class);
		criteria.createAlias("tipoPessoaFisicaUnidade", "tpfu", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("pf.pessoaFisicaCondominio", "pc", JoinType.INNER_JOIN);
		criteria.createAlias("pc.tipoPessoaFisicaCondominio", "tpc", JoinType.INNER_JOIN);
		criteria.createAlias("unidade", "un", JoinType.INNER_JOIN);
		criteria.createAlias("un.enderecoUnidade", "eun", JoinType.INNER_JOIN);
		criteria.createAlias("un.tipoEnderecoUnidade", "teun", JoinType.INNER_JOIN);
		criteria.createAlias("un.condominio", "con", JoinType.INNER_JOIN);
		criteria.add(Restrictions.eq("un.id",id));
		criteria.add(Restrictions.eq("tpfu.id",tipo));
		//criteria.add(Restrictions.isNull("dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaFisicaUnidade>) criteria.list();
	}

	
	
	@Transactional(readOnly = true)
	@Override
	public PessoaFisicaUnidade pessoasPorUnidadePorIdPessoa(Integer id) {
		
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaUnidade.class);
		criteria.createAlias("pessoaFisicaUnidadeRecurso", "pfur", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("pfur.recurso", "rec", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("tipoPessoaFisicaUnidade", "tpfu", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("pf.pessoaFisicaCondominio", "pc", JoinType.INNER_JOIN);
		criteria.createAlias("pc.tipoPessoaFisicaCondominio", "tpc", JoinType.INNER_JOIN);
		criteria.createAlias("unidade", "un", JoinType.INNER_JOIN);
		criteria.createAlias("un.enderecoUnidade", "eun", JoinType.INNER_JOIN);
		criteria.createAlias("un.tipoEnderecoUnidade", "teun", JoinType.INNER_JOIN);
		criteria.createAlias("un.condominio", "con", JoinType.INNER_JOIN);
		criteria.add(Restrictions.eq("pf.id",id));
		//criteria.add(Restrictions.isNull("dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (PessoaFisicaUnidade) criteria.uniqueResult();
	}
	
	
	@Transactional(readOnly = true)
	@Override
	public PessoaFisicaUnidade pessoasPorUnidadePorIdPessoa(Integer unidade,Integer pessoa) {
		
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaUnidade.class);
		criteria.createAlias("pessoaFisicaUnidadeRecurso", "pfur", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("pfur.recurso", "rec", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("tipoPessoaFisicaUnidade", "tpfu", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("pf.pessoaFisicaCondominio", "pc", JoinType.INNER_JOIN);
		criteria.createAlias("pc.tipoPessoaFisicaCondominio", "tpc", JoinType.INNER_JOIN);
		criteria.createAlias("unidade", "un", JoinType.INNER_JOIN);
		criteria.createAlias("un.enderecoUnidade", "eun", JoinType.INNER_JOIN);
		criteria.createAlias("un.tipoEnderecoUnidade", "teun", JoinType.INNER_JOIN);
		criteria.createAlias("un.condominio", "con", JoinType.INNER_JOIN);
		criteria.add(Restrictions.eq("un.id",unidade));
		criteria.add(Restrictions.eq("pf.id",pessoa));
		//criteria.add(Restrictions.isNull("dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (PessoaFisicaUnidade) criteria.uniqueResult();
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<PessoaFisicaUnidade> listaPessoasPorUnidadePorIdPessoa(Integer id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaUnidade.class);
		criteria.createAlias("pessoaFisicaUnidadeRecurso", "pfur", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("tipoPessoaFisicaUnidade", "tpfu", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("pf.pessoaFisicaCondominio", "pc", JoinType.INNER_JOIN);
		criteria.createAlias("pc.tipoPessoaFisicaCondominio", "tpc", JoinType.INNER_JOIN);
		criteria.createAlias("unidade", "un", JoinType.INNER_JOIN);
		criteria.createAlias("un.enderecoUnidade", "eun", JoinType.INNER_JOIN);
		criteria.createAlias("un.tipoEnderecoUnidade", "teun", JoinType.INNER_JOIN);
		criteria.createAlias("un.condominio", "con", JoinType.INNER_JOIN);
		criteria.add(Restrictions.eq("pf.id",id));
		//criteria.add(Restrictions.isNull("dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaFisicaUnidade>) criteria.list();
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<PessoaFisicaUnidade> proprietarioPorUnidadePorCondominio(Integer id, Integer condominio) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaUnidade.class);
		criteria.createAlias("tipoPessoaFisicaUnidade", "tpfu", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("pf.pessoaFisicaCondominio", "pc", JoinType.INNER_JOIN);
		criteria.createAlias("pc.tipoPessoaFisicaCondominio", "tpc", JoinType.INNER_JOIN);
		criteria.createAlias("unidade", "un", JoinType.INNER_JOIN);
		criteria.createAlias("un.enderecoUnidade", "eun", JoinType.INNER_JOIN);
		criteria.createAlias("un.tipoEnderecoUnidade", "teun", JoinType.INNER_JOIN);
		criteria.createAlias("un.condominio", "con", JoinType.INNER_JOIN);
		criteria.add(Restrictions.eq("un.id",id)); 
		criteria.add(Restrictions.eq("con.id",condominio));
		criteria.add(Restrictions.isNull("dataFim"));
		criteria.add(Restrictions.isNull("pc.dataFim"));
		
		Criterion c1;
		Criterion c2;

		c1 = Restrictions.eq("tpfu.id",TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId());
		c2 = Restrictions.eq("tpfu.id",TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId());

		Disjunction orExp = Restrictions.or(c1,c2,c2);
		criteria.add(orExp);
		
		//criteria.add(Restrictions.isNull("dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaFisicaUnidade>) criteria.list();
	}


	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<PessoaFisicaUnidade> moradorPorUnidadePorCondominio(Integer id, Integer condominio) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaUnidade.class);
		criteria.createAlias("tipoPessoaFisicaUnidade", "tpfu", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("parentesco", "par", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("pf.pessoaFisicaCondominio", "pc", JoinType.INNER_JOIN);
		criteria.createAlias("pc.tipoPessoaFisicaCondominio", "tpc", JoinType.INNER_JOIN);
		criteria.createAlias("unidade", "un", JoinType.INNER_JOIN);
		criteria.createAlias("un.enderecoUnidade", "eun", JoinType.INNER_JOIN);
		criteria.createAlias("un.tipoEnderecoUnidade", "teun", JoinType.INNER_JOIN);
		criteria.createAlias("un.condominio", "con", JoinType.INNER_JOIN);
		
		
		criteria.add(Restrictions.eq("un.id",id)); 
		criteria.add(Restrictions.eq("con.id",condominio));
		
		Criterion c1,c2,c3,c4,c5;

		c1 = Restrictions.eq("tpfu.id",TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId());
		c2 = Restrictions.eq("tpfu.id",TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId());
		c3 = Restrictions.eq("tpfu.id",TipoPessoaFisicaUnidadeEnum.MORADOR.getId());
		c4 = Restrictions.eq("tpfu.id",TipoPessoaFisicaUnidadeEnum.COMODATO.getId());
		c5 = Restrictions.eq("tpfu.id",TipoPessoaFisicaUnidadeEnum.INQUILINO.getId());

		Disjunction orExp = Restrictions.or(c1,c2,c3,c4,c5);
		criteria.add(orExp);
		
		
		
		
		//criteria.add(Restrictions.isNull("dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaFisicaUnidade>) criteria.list();
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<PessoaFisicaUnidade> inquilinoPorUnidadePorCondominio(Integer id, Integer condominio) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaUnidade.class);
		criteria.createAlias("tipoPessoaFisicaUnidade", "tpfu", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("parentesco", "par", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("pf.pessoaFisicaCondominio", "pc", JoinType.INNER_JOIN);
		criteria.createAlias("pc.tipoPessoaFisicaCondominio", "tpc", JoinType.INNER_JOIN);
		criteria.createAlias("unidade", "un", JoinType.INNER_JOIN);
		criteria.createAlias("un.enderecoUnidade", "eun", JoinType.INNER_JOIN);
		criteria.createAlias("un.tipoEnderecoUnidade", "teun", JoinType.INNER_JOIN);
		criteria.createAlias("un.condominio", "con", JoinType.INNER_JOIN);
		
		
		criteria.add(Restrictions.eq("un.id",id)); 
		criteria.add(Restrictions.eq("con.id",condominio));
		criteria.add(Restrictions.eq("tpfu.id",TipoPessoaFisicaUnidadeEnum.INQUILINO.getId()));
		
		
		//criteria.add(Restrictions.isNull("dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaFisicaUnidade>) criteria.list();
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<PessoaFisicaUnidade> funcionarioPorUnidadePorCondominio(Integer id, Integer condominio) {
		
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaUnidade.class);
		criteria.createAlias("tipoPessoaFisicaUnidade", "tpfu", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.createAlias("parentesco", "par", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("pf.pessoaFisicaCondominio", "pc", JoinType.INNER_JOIN);
		criteria.createAlias("pc.tipoPessoaFisicaCondominio", "tpc", JoinType.INNER_JOIN);
		criteria.createAlias("unidade", "un", JoinType.INNER_JOIN);
		criteria.createAlias("un.enderecoUnidade", "eun", JoinType.INNER_JOIN);
		criteria.createAlias("un.tipoEnderecoUnidade", "teun", JoinType.INNER_JOIN);
		criteria.createAlias("un.condominio", "con", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.isNull("dataFim"));
		criteria.add(Restrictions.eq("un.id",id)); 
		criteria.add(Restrictions.eq("con.id",condominio));
		criteria.add(Restrictions.eq("tpfu.id",TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()));
		
		
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaFisicaUnidade>) criteria.list();
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true) 
	@Override
	public List<PessoaFisicaUnidade> proprietariosMoradoresInquilinosFuncionariosUnidade(Integer condominio) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisicaUnidade.class);
		criteria.createAlias("pessoaFisica", "pf", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("unidade", "uni", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("uni.enderecoUnidade", "endereco", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("uni.condominio", "con", JoinType.LEFT_OUTER_JOIN);
		criteria.add(Restrictions.eq("con.id",condominio));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<PessoaFisicaUnidade>) criteria.list();
	}
	



}
