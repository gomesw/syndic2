package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.entity.PessoaFisicaUnidade;

public interface PessoaFisicaUnidadeRepositoryQueries {

	public List<PessoaFisicaUnidade> pessoasPorUnidadePorTipo(Integer id,Integer tipo);
	
	public List<PessoaFisicaUnidade> proprietarioPorUnidadePorCondominio(Integer id,Integer condominio);

	List<PessoaFisicaUnidade> moradorPorUnidadePorCondominio(Integer id, Integer condominio);
	List<PessoaFisicaUnidade> inquilinoPorUnidadePorCondominio(Integer id, Integer condominio);

	PessoaFisicaUnidade pessoasPorUnidadePorIdPessoa(Integer id);
	
	PessoaFisicaUnidade pessoasPorUnidadePorIdPessoa(Integer idunidade,Integer idpessoa);

	List<PessoaFisicaUnidade> listaPessoasPorUnidadePorIdPessoa(Integer id);

	List<PessoaFisicaUnidade> funcionarioPorUnidadePorCondominio(Integer id, Integer condominio);

	List<PessoaFisicaUnidade> proprietariosMoradoresInquilinosFuncionariosUnidade(Integer condominio);
	
	
}
