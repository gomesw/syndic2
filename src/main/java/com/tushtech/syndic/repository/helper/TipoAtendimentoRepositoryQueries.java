package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.entity.TipoAtendimento;

public interface TipoAtendimentoRepositoryQueries {

	public List<TipoAtendimento> comIdSubordinacao();
	
}
