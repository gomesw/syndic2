package com.tushtech.syndic.repository.helper;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import com.tushtech.syndic.entity.Unidade;



public class UnidadeRepositoryImpl implements UnidadeRepositoryQueries {

	@PersistenceContext
	private EntityManager manager;
	
	@Transactional(readOnly = true)
	@Override
	public Unidade buscarUnidadeCompleta(Integer id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Unidade.class);
		criteria.createAlias("enderecoUnidade", "ende", JoinType.INNER_JOIN);
		criteria.createAlias("condominio", "con", JoinType.INNER_JOIN);
		criteria.createAlias("animais", "ani", JoinType.NONE); 
		criteria.createAlias("pessoaFisicaUnidade", "pfs", JoinType.INNER_JOIN);
		criteria.createAlias("tipoEnderecoUnidade", "tiend", JoinType.INNER_JOIN);
		criteria.createAlias("tipoUnidade", "pfpcon", JoinType.INNER_JOIN);
		criteria.add(Restrictions.eq("id",id));
		criteria.add(Restrictions.isNull("pfs.dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (Unidade) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Unidade> buscarUnidadesPorPessoaFisica(Integer id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Unidade.class);
		criteria.createAlias("enderecoUnidade", "ende", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisicaUnidade", "pfs", JoinType.INNER_JOIN);
		criteria.createAlias("pfs.pessoaFisica", "pf", JoinType.INNER_JOIN);
		criteria.add(Restrictions.eq("pf.id",id));
		criteria.add(Restrictions.isNull("pfs.dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<Unidade>) criteria.list();
	}
	
	
	@Transactional(readOnly = true)
	@Override
	public Unidade verificarExistenciaUnidadeRepetida(Unidade uni) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Unidade.class);
		criteria.createAlias("enderecoUnidade", "ende", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("condominio", "con", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("tipoEnderecoUnidade", "tiend", JoinType.INNER_JOIN);
		criteria.createAlias("tipoUnidade", "tipoUn", JoinType.INNER_JOIN);
		
		if(uni.getEnderecoUnidade()!=null){
			criteria.add(Restrictions.eq("ende.id",uni.getEnderecoUnidade().getId()));
		}
		
		if(uni.getTipoEnderecoUnidade()!=null){
			criteria.add(Restrictions.eq("tipoEnderecoUnidade.id",uni.getTipoEnderecoUnidade().getId()));
		}
		criteria.add(Restrictions.eq("con.id",uni.getCondominio().getId()));
		if(uni.getTipoUnidade()!=null){
			criteria.add(Restrictions.eq("tipoUn.id",uni.getTipoUnidade().getId()));
		}
		
		if(uni.getUnidadeHabitacional()!=null){
			criteria.add(Restrictions.eq("unidadeHabitacional",uni.getUnidadeHabitacional()));
		}
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (Unidade) criteria.uniqueResult();
	}

	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Unidade> buscarUnidadesPorCondominio(Integer id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Unidade.class);
		criteria.createAlias("enderecoUnidade", "ende", JoinType.INNER_JOIN);
		criteria.createAlias("condominio", "con", JoinType.INNER_JOIN);
//		criteria.createAlias("animais", "ani", JoinType.NONE); 
//		criteria.createAlias("correspondencias", "corr", JoinType.NONE);
		//criteria.createAlias("pessoaFisicaUnidade", "pfs", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("tipoEnderecoUnidade", "tiend", JoinType.INNER_JOIN);
		criteria.createAlias("tipoUnidade", "pfpcon", JoinType.INNER_JOIN);
//		criteria.createAlias("veiculo", "vei", JoinType.NONE);
		criteria.add(Restrictions.eq("con.id",id));
		//criteria.add(Restrictions.isNull("pfs.dataFim"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<Unidade>) criteria.list();
	}
	
	


}
