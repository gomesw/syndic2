package com.tushtech.syndic.repository.helper;

import java.util.List;

import com.tushtech.syndic.entity.Unidade;

public interface UnidadeRepositoryQueries {

	public Unidade buscarUnidadeCompleta(Integer id);

	List<Unidade> buscarUnidadesPorCondominio(Integer id);

	Unidade verificarExistenciaUnidadeRepetida(Unidade uni);

	List<Unidade> buscarUnidadesPorPessoaFisica(Integer id);
	
}
