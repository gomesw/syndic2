package com.tushtech.syndic.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaFisicaPerfilCondominio;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.PessoaFisicaUnidadeRecurso;
import com.tushtech.syndic.entity.PessoaJuridica;
import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisica;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisica;
import com.tushtech.syndic.repository.PessoaFisicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaPerfilCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.repository.PessoaFisicaUnidadeRepository;
import com.tushtech.syndic.service.LoginService;


@Service
public class ProvedorAutenticacao implements AuthenticationProvider {



	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository; 
	
	@Autowired
	private PessoaFisicaPerfilCondominioRepository pessoaFisicaPerfilCondominioRepository;
	
	
	@Autowired
	private PessoaFisicaUnidadeRepository pessoaFisicaUnidadeRepository;
			
	@Autowired
	private PessoaFisicaCondominioRepository pessoaFisicaCondominioRepository;

	
	@Autowired
	private LoginService loginService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {

		String senha = auth.getCredentials().toString();
		String login = auth.getName();
		login  = login.trim().replaceAll("\\.|-|/", "");
		PessoaFisica pessoaFisica  = pessoaFisicaRepository.porCpf(login);
		
		
		PessoaJuridica pessoaJuridica = new PessoaJuridica();
		System.err.println(">>>>>>1");
		if(pessoaFisica==null) { 
			System.err.println(">>>>>>2");
			PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica  = pessoaFisicaRepository.porCpfPJ(login);
			if(pessoaJuridicaPessoaFisica!=null){
				System.err.println(">>>>>>2.5");
				pessoaFisica = pessoaJuridicaPessoaFisica.getPessoaFisica();
				pessoaFisica.setTipoPessoa(TipoPessoaFisica.PESSOAFISICAPESSOAJURIDICA.getId());
				pessoaFisica.setFuncaoEmpresa(pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica());
				pessoaJuridica = pessoaJuridicaPessoaFisica.getPessoaJuridica();
			}
			else{
				
				throw new UsernameNotFoundException("Login e/ou Senha inválidos.");
			}
		}
		
		System.err.println(">>>>>>3");
		if(pessoaFisica!=null && pessoaFisica.getTipoPessoa()!=TipoPessoaFisica.PESSOAFISICAPESSOAJURIDICA.getId()) {
			System.err.println(">>>>>>4");
			List<PessoaFisicaUnidade> listPessoaFisicaUnidade =  pessoaFisicaUnidadeRepository.findByPessoaFisicaId(pessoaFisica.getId());
			List<PessoaFisicaCondominio> listPessoaFisicaCondominio =  pessoaFisicaCondominioRepository.findByPessoaFisicaId(pessoaFisica.getId());
			List<PessoaFisicaPerfilCondominio> pessoaFisicaPerfilCondominio = pessoaFisicaPerfilCondominioRepository.porIdPessoaFisica(pessoaFisica.getId());
			
			pessoaFisica.setPessoaFisicaPerfilCondominio(pessoaFisicaPerfilCondominio);
			pessoaFisica.setSenhas(pessoaFisicaRepository.retornaSenhas(pessoaFisica.getId()));
			pessoaFisica.setPessoaFisicaCondominio(listPessoaFisicaCondominio);
			pessoaFisica.setPessoaFisicaUnidade(listPessoaFisicaUnidade);
			
			pessoaJuridica = (pessoaJuridica ==null && !listPessoaFisicaCondominio.isEmpty()?listPessoaFisicaCondominio.get(0).getCondominio().getPessoaJuridica():null);
			
			
			if(passwordEncoder.matches(senha,pessoaFisica.getSenhas().get(0).getSenha())) {
				System.err.println(">>>>>>5");
				if (senha != null) {
					if (usuarioAtivo(pessoaFisica)) {
						loginService.salvar(pessoaFisica);
						
						
						Collection<? extends GrantedAuthority> authorities = getPermissoes(pessoaFisica);
						
						return new UsuarioSistema(pessoaFisica,pessoaJuridica, senha, authorities);
					} else {
						throw new BadCredentialsException("Este usuário está desativado .");
					}
				}
			}
			throw new UsernameNotFoundException("Login e/ou Senha inválidos.");
		}
		else{
			
			System.err.println(">>>>>>6");
			
			List<PessoaFisicaUnidade> listPessoaFisicaUnidade =  pessoaFisicaUnidadeRepository.findByPessoaFisicaId(pessoaFisica.getId());
			
			List<PessoaFisicaCondominio> listPessoaFisicaCondominio =  pessoaFisicaCondominioRepository.findByPessoaFisicaId(pessoaFisica.getId());
			
			List<PessoaFisicaPerfilCondominio> pessoaFisicaPerfilCondominio = pessoaFisicaPerfilCondominioRepository.porIdPessoaFisica(pessoaFisica.getId());
			
			pessoaFisica.setPessoaFisicaPerfilCondominio(pessoaFisicaPerfilCondominio);
			pessoaFisica.setSenhas(pessoaFisicaRepository.retornaSenhas(pessoaFisica.getId()));
			pessoaFisica.setPessoaFisicaCondominio(listPessoaFisicaCondominio);
			pessoaFisica.setPessoaFisicaUnidade(listPessoaFisicaUnidade);
			
			
			if(passwordEncoder.matches(senha,pessoaFisica.getSenhas().get(0).getSenha())) {
				if (senha != null) {
					if (usuarioAtivo(pessoaFisica)) {
						loginService.salvar(pessoaFisica);
						
						
						Collection<? extends GrantedAuthority> authorities = getPermissoes(pessoaFisica);
						

						return new UsuarioSistema(pessoaFisica,pessoaJuridica, senha, authorities);
					} else {
						throw new BadCredentialsException("Este usuário está desativado .");
					}
				}
			}
			throw new UsernameNotFoundException("Login e/ou Senha inválidos.");
			
			
		}
		

	}

	@Override
	public boolean supports(Class<?> auth) {
		return auth.equals(UsernamePasswordAuthenticationToken.class);
	}


	/**
	 * 
	 * @author Wallace
	 * @param policial
	 * @return carrega os recursos que o usuário pode acessar
	 */
	private Collection<? extends GrantedAuthority> getPermissoes(PessoaFisica pessoaFisica) {

		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		
//		List<PessoaFisicaPerfilCondominio> pessoaFisicaPerfilCondominio = pessoaFisicaPerfilCondominioRepository.porIdPessoaFisica(pessoaFisica.getId());
//		
//		for (PessoaFisicaPerfilCondominio pfpc : pessoaFisicaPerfilCondominio) { 
//			for (PerfilCondominioRecurso pcr : pfpc.getPerfilCondominio().getPerfilCondominioRecurso()) { 
//				authorities.add(new SimpleGrantedAuthority("ROLE_"+pcr.getRecurso().getNome().toUpperCase()));
//				
//			}
//		}
		
		
		if (pessoaFisica.isSindico()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_SINDICO"));
		}
		
		if (pessoaFisica.isAdministradorSistema()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_ADMINISTRADOR"));
		}else if (pessoaFisica.isAdministradorEmpresa()) {
			
			authorities.add(new SimpleGrantedAuthority("ROLE_EMPRESA"));
			authorities.add(new SimpleGrantedAuthority("ROLE_EMPRESA_"+pessoaFisica.getFuncaoEmpresa().getNome().toUpperCase()));
			
		}  
		if (pessoaFisica.isVisitante()) {
			authorities.add(new SimpleGrantedAuthority("ROLE_VISITANTE"));
		}
		
		if (pessoaFisica.isMorador()) {
			PessoaFisicaUnidade pessoaFisicaUnidade =  pessoaFisicaUnidadeRepository.pessoasPorUnidadePorIdPessoa(pessoaFisica.getId());
			if(pessoaFisica.isProprietario()) {
				    authorities.add(new SimpleGrantedAuthority("ROLE_PROPRIETARIO"));
				    
			}
			if(pessoaFisica.isMorador()) {
					authorities.add(new SimpleGrantedAuthority("ROLE_MORADOR"));
					for (PessoaFisicaUnidadeRecurso pessoaFisicaUnidadeRecurso : pessoaFisicaUnidade.getPessoaFisicaUnidadeRecurso()) {
						authorities.add(new SimpleGrantedAuthority("ROLE_"+pessoaFisicaUnidadeRecurso.getRecurso().getNome().toUpperCase()));
					}
			}
			
		}
		
		


		return authorities;
	}


	private boolean usuarioAtivo(PessoaFisica usuario) {
		if (usuario != null) {

			if (usuario.getNome()!=null) {
				return true;
			}
			return true;
		}
		return true;
	}


}
