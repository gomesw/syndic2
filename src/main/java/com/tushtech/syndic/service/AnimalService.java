package com.tushtech.syndic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Animal;
import com.tushtech.syndic.entity.FotoAnimal;
import com.tushtech.syndic.repository.AnimalRepository;
import com.tushtech.syndic.repository.FotoAnimalRepository;

@Service
public class AnimalService {

	@Autowired
	private AnimalRepository AnimalRepository;
	
	
	@Autowired
	private FotoAnimalRepository fotoAnimalRepository;
	
	public Animal salvarAnimal(Animal animal){

		return this.AnimalRepository.saveAndFlush(animal);

	}
	
	
	public void excluirAnimal(Animal animal){
		this.AnimalRepository.delete(animal);
	}
	
	
	public void salvarFotos(Animal animal,List<FotoAnimal> listFotoAnimal) {
		
		
		for (FotoAnimal fotoAnimal : listFotoAnimal) {
			fotoAnimal.setAnimal(animal);
		}
		
		this.fotoAnimalRepository.save(animal.getListFotoAnimal());
		
	}

	
}
