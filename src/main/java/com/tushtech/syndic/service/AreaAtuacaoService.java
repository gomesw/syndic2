package com.tushtech.syndic.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.AreaAtuacao;
import com.tushtech.syndic.repository.AreaAtuacaoRepository;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;

@Service
public class AreaAtuacaoService {

	@Autowired
	private AreaAtuacaoRepository areaAtuacaoRepository;

	@Transactional
	public void salvar(AreaAtuacao areaAtuacao){   

		if(areaAtuacao.isNovo()){
			//VERIFICA SE MARCA DE VEICULO JÁ FOI CADASTRADA
			Optional<AreaAtuacao> areaAtuacaoExiste = areaAtuacaoRepository.findByNome(areaAtuacao.getNome());
																				
			if(areaAtuacaoExiste.isPresent()){
				if(areaAtuacaoExiste.get().getAtivo()==0){
					throw new NomeDuplicadoException(" Área de atuação está desativada! <a style=\"color:#ffffff;\" href=\"/areaatuacao/ativar/"+areaAtuacaoExiste.get().getId()+"\">Deseja Ativá-la ?</a>"	);
				}
				else{
					throw new NomeDuplicadoException("Área de atuação já cadastrada");
				}
			}
		}

		areaAtuacao.setAtivo(1);
		areaAtuacaoRepository.save(areaAtuacao);

	}

	
	@Transactional
	public AreaAtuacao desativar(AreaAtuacao areaAtuacao) {
		areaAtuacao = areaAtuacaoRepository.findById(areaAtuacao.getId());
		areaAtuacao.setAtivo(0);
		this.areaAtuacaoRepository.saveAndFlush(areaAtuacao); 
		return areaAtuacao;
	}
	
	@Transactional
	public AreaAtuacao ativar(AreaAtuacao areaAtuacao) {
		areaAtuacao = areaAtuacaoRepository.findById(areaAtuacao.getId());
		areaAtuacao.setAtivo(1);
		this.areaAtuacaoRepository.saveAndFlush(areaAtuacao); 
		return areaAtuacao;
	}

	
}
