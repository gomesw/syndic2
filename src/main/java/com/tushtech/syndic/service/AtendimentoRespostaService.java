package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.AtendimentoResposta;
import com.tushtech.syndic.repository.AtendimentoRespostaRepository;

@Service
public class AtendimentoRespostaService {
	
	@Autowired 
	private AtendimentoRespostaRepository atendimentoRespostaRepository;
	
//	@Autowired
//	private StatusChamadoRepository statusChamadoRepository;
//	
//	@Autowired
//	private AtendimentoStatusRepository atendimentoStatusRepository;
//	

	public void salvar(AtendimentoResposta atendimentoResposta){
				
		this.atendimentoRespostaRepository.saveAndFlush(atendimentoResposta);
	}

}
