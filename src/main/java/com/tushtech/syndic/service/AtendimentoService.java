package com.tushtech.syndic.service;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.tushtech.syndic.entity.Atendimento;
import com.tushtech.syndic.entity.AtendimentoStatus;
import com.tushtech.syndic.repository.AtendimentoRepository;
import com.tushtech.syndic.repository.AtendimentoStatusRepository;
import com.tushtech.syndic.repository.StatusChamadoRepository;

@Service
public class AtendimentoService {
	
	@Autowired 
	private AtendimentoRepository atendimentoRepository;
	
	@Autowired
	private StatusChamadoRepository statusChamadoRepository;
	
	@Autowired
	private AtendimentoStatusRepository atendimentoStatusRepository;
	

	public void salvar(Atendimento atendimento){
				
		atendimento = this.atendimentoRepository.saveAndFlush(atendimento);
		
		atendimentoStatusInicial(atendimento);
	}

	private void atendimentoStatusInicial(Atendimento atendimento) {
		AtendimentoStatus atendimentoStatus = new AtendimentoStatus();
		
		atendimentoStatus.setStatusChamado(statusChamadoRepository.findOne(1));
		
		atendimentoStatus.setAtendimento(atendimento);
		atendimentoStatus.setDataMudanca(java.sql.Date.valueOf(LocalDate.now()));
		atendimentoStatus.setAtivo(1);
		
		atendimentoStatusRepository.saveAndFlush(atendimentoStatus);
	}
	
	public void atendimentoStatusCancelado(Atendimento atendimento) {
		AtendimentoStatus atendimentoStatus = new AtendimentoStatus();
		
		//3 = cancelado
		atendimentoStatus.setStatusChamado(statusChamadoRepository.findOne(3));
		
		atendimentoStatus.setAtendimento(atendimento);
		atendimentoStatus.setDataMudanca(java.sql.Date.valueOf(LocalDate.now()));
		atendimentoStatus.setAtivo(1);
		
		atendimentoStatusRepository.saveAndFlush(atendimentoStatus);
	}

	public void atribuirErros(Atendimento atendimento, BindingResult result) {
		// TODO Auto-generated method stub
		
		
		if(atendimento.getTipoAtendimento().getId() == 0 || atendimento.getTipoAtendimento() == null){
			result.rejectValue("tipoAtendimento","","Informe o Tipo e o subTipo de Atendimento");
		}
		
	}
	
	
	public void setProtocolo(Atendimento atendimento) {

		Atendimento ultimoatendimento =  atendimentoRepository.findTop1ByCondominioIdOrderByProtocoloAsc(atendimento.getCondominio().getId());
		
		if(ultimoatendimento==null) {
			atendimento.setProtocolo(1);
		}
		else {
			atendimento.setProtocolo(ultimoatendimento.getProtocolo()+1);
		}
		
	}
	


}
