package com.tushtech.syndic.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.CondominioPacoteServico;
import com.tushtech.syndic.repository.CondominioPacoteServicoRepository;

@Service
public class CondominioPacoteServicosService {

	@Autowired
	private CondominioPacoteServicoRepository condominioPacoteServicoRepository;
	
	@Transactional
	public CondominioPacoteServico desativar(CondominioPacoteServico condominioPacoteServico) {
		condominioPacoteServico = condominioPacoteServicoRepository.findOne(condominioPacoteServico.getId());
		this.condominioPacoteServicoRepository.delete(condominioPacoteServico); 
		return condominioPacoteServico;
	}
	
}
