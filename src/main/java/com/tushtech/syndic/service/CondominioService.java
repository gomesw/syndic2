package com.tushtech.syndic.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.Email;
import com.tushtech.syndic.entity.PerfilCondominio;
import com.tushtech.syndic.entity.PerfilCondominioRecurso;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaFisicaPerfilCondominio;
import com.tushtech.syndic.entity.PessoaJuridica;
import com.tushtech.syndic.entity.Recurso;
import com.tushtech.syndic.entity.Senhas;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaCondominioEnum;
import com.tushtech.syndic.mail.Mailer;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.EmailRepository;
import com.tushtech.syndic.repository.PerfilCondominioRecursoRepository;
import com.tushtech.syndic.repository.PerfilCondominioRepository;
import com.tushtech.syndic.repository.PerfilRepository;
import com.tushtech.syndic.repository.PessoaFisicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaPerfilCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.repository.PessoaJuridicaRepository;
import com.tushtech.syndic.repository.RecursoRepository;
import com.tushtech.syndic.repository.SenhasRepository;



@Service
public class CondominioService {

	@Autowired
	private CondominioRepository condominioRepository;

	@Autowired
	private PessoaJuridicaRepository pessoaJuridicaRepository;	

	@Autowired
	private RecursoRepository recursoRepository;

	@Autowired
	private PerfilCondominioRecursoRepository perfilCondominioRecursoRepository;

	@Autowired
	private PerfilCondominioRepository perfilCondominioRepository;

	@Autowired
	private PerfilRepository perfilRepository;


	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;

	@Autowired
	private PessoaFisicaService pessoaFisicaService;
	
	@Autowired
	private PessoaFisicaCondominioRepository pessoaFisicaCondominioRepository; 


	@Autowired
	private SenhasRepository senhasRepository;

	@Autowired 
	EmailRepository emailRepository; 

	@Autowired
	private Mailer mailer;

	@Autowired
	private PasswordEncoder passwordEncoder;

	
	@Autowired
	PessoaFisicaPerfilCondominioRepository pessoaFisicaPerfilCondominioRepository;

	//	--Condominio
	public Condominio salvarCondominio(Condominio condominio){

		int flagCondominio = 0;
		
		if(condominio.getId() > 0){
			flagCondominio = 1;
		}
		
		condominio.setAtivo(1);
		
		PessoaJuridica pessoajuridica = new PessoaJuridica();		

		pessoajuridica = condominio.getPessoaJuridica();
		pessoajuridica.setCnpj(pessoajuridica.getCnpj().replaceAll("\\D", ""));
		
		pessoaJuridicaRepository.saveAndFlush(pessoajuridica);

		condominio = condominioRepository.saveAndFlush(condominio);

		//salvar perfil_condominio default pra cada condominio
		//if(flagCondominio == 0){
			//incluirPerfis(condominio);			
		//}
		return condominio;
			
	}


	
	
	
	public void excluirCondominio(Condominio condominio){

		this.condominioRepository.delete(condominio.getId());

	}	

	private void incluirPerfis(Condominio condominio) {

		int[] perfis = {1,2,4,5,6};

		for (int i=0; i<perfis.length; i++) {

			PerfilCondominio perfilCondominio = new PerfilCondominio();
			perfilCondominio.setCondominio(condominio);
			perfilCondominio.setPerfil(perfilRepository.findOne(perfis[i]));

			perfilCondominio = perfilCondominioRepository.saveAndFlush(perfilCondominio);

			//incluir recursos para os perfis de cada condominio
			incluirRecursos(perfilCondominio);

		}
	}

	private void incluirRecursos(PerfilCondominio perfilCondominio) {
		List<PerfilCondominioRecurso> listPerfilCondominioRecurso = new ArrayList<>();

		//adicionar recursos do síndico
		if(perfilCondominio.getPerfil().getId() == 1){


			int[] recursosSindico = {1,2,3,4,5};
			for(int a=0; a<recursosSindico.length; a++){
				PerfilCondominioRecurso perfilCondominioRecurso = new PerfilCondominioRecurso();
				perfilCondominioRecurso.setPerfilCondominio(perfilCondominio);
				perfilCondominioRecurso.setRecurso(recursoRepository.findOne(recursosSindico[a]));

				listPerfilCondominioRecurso.add(perfilCondominioRecurso);
			}

			perfilCondominioRecursoRepository.save(listPerfilCondominioRecurso);
		}
	}

	/**
	 * Atribuir os erros referentes ao formulário de condominio
	 * @param condominio
	 * @param result
	 */
	public void atribuirErros(Condominio condominio,BindingResult result){

		
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
		Validator validator = validatorFactory . getValidator () ;
		Set<ConstraintViolation<PessoaJuridica>> errors = validator . validate (condominio.getPessoaJuridica()) ;

		if(condominio.getPessoaJuridica().getCnpj()==null){
			result.rejectValue("pessoaJuridica.cnpj","","CNPJ é obrigatório");
			
		}
		for ( ConstraintViolation < PessoaJuridica > error : errors ) {
			result.rejectValue("pessoaJuridica."+error.getPropertyPath(),"",error.getMessage());
		}
		
		if(condominio.getTelefone1().equals("") && condominio.getTelefone2().equals("") && condominio.getTelefone3().equals("")){
			result.rejectValue("telefone1","","Informe um número de telefone");

		}


	}


	@Transactional
	public void atualizarPerfilCondominioRecurso(String[] nomes, Integer id) {

		//Recursos selecionadas pelo usuario
		List<Recurso> listRecursosDepois = recursoRepository.findByNomeIn(nomes);

		//Recursos que ja existem no banco
		List<PerfilCondominioRecurso> listPerfilCondominioRecursoAntes = perfilCondominioRecursoRepository.porIdPerfilCondominioConsulta(id);

		//inserir o objeto funcaoUpmLotacao para manipulação
		List<PerfilCondominioRecurso> listPerfilCondominioRecursoDepois = new ArrayList<>();		

		for (Recurso recurso : listRecursosDepois) {
			PerfilCondominioRecurso perfilCondominioRecurso = new PerfilCondominioRecurso();
			perfilCondominioRecurso.setPerfilCondominio(perfilCondominioRepository.findOne(id));
			perfilCondominioRecurso.setRecurso(recurso);
			listPerfilCondominioRecursoDepois.add(perfilCondominioRecurso);
		}


		if(listPerfilCondominioRecursoAntes.size() > listRecursosDepois.size()){

			List<PerfilCondominioRecurso> listaRemover = new ArrayList<PerfilCondominioRecurso>();

			for (PerfilCondominioRecurso perfilCondominioRecursoantes :listPerfilCondominioRecursoAntes) {

				boolean isexiste = false;

				for (Recurso  recursodepois : listRecursosDepois) {
					if(perfilCondominioRecursoantes.getRecurso().getId() == recursodepois.getId()){
						isexiste = true;
						break;
					}
				}

				if(!isexiste){
					listaRemover.add(perfilCondominioRecursoantes);
				} 
			}

			for (PerfilCondominioRecurso perfilCondominioRecurso : listaRemover) {
				listPerfilCondominioRecursoAntes.remove(perfilCondominioRecurso);
				perfilCondominioRecursoRepository.delete(perfilCondominioRecurso);

			}

			perfilCondominioRecursoRepository.save(listPerfilCondominioRecursoAntes);

		}else{
			for (PerfilCondominioRecurso perfilCondominioRecursodepois : listPerfilCondominioRecursoDepois) {

				boolean isexiste = false;

				for (PerfilCondominioRecurso perfilCondominioRecursoantes : listPerfilCondominioRecursoAntes) {
					if(perfilCondominioRecursodepois.getRecurso().getId() == perfilCondominioRecursoantes.getRecurso().getId()){
						isexiste = true;
						break;
					}
				}

				if(!isexiste){
					perfilCondominioRecursoRepository.save(perfilCondominioRecursodepois);
				}	
			}			
		}


	}


	
	
//	@Transactional
//	public void incluirSindicoEdicao(PessoaFisicaCondominio pessoafisicaCondominio, UsuarioSistema usuarioSistema){
//		
//		pessoaFisicaCondominioRepository.save(pessoafisicaCondominio);
//
//	}
	
	
	@Transactional
	public void incluirFuncionario(PessoaFisicaCondominio pessoafisicaCondominio, UsuarioSistema usuarioSistema){

		pessoafisicaCondominio.getPessoaFisica().getEmails().get(0).setAtivo(1);
		PessoaFisicaPerfilCondominio pessoaFisicaPerfilCondominio = pessoafisicaCondominio.getPessoaFisica().getPessoaFisicaPerfilCondominio().get(0);
		
		
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(pessoafisicaCondominio.getCondominio().getId());
		pessoaFisicaPerfilCondominio.getPerfilCondominio().setCondominio(condominio);
		pessoafisicaCondominio.setCondominio(condominio);
		
		pessoafisicaCondominio.getCondominio().getPessoaJuridica().setCnpj(pessoafisicaCondominio.getCondominio().getPessoaJuridica().getCnpj().trim().replaceAll("\\.|-|/", ""));
		pessoafisicaCondominio = pessoaFisicaCondominioRepository.saveAndFlush(pessoafisicaCondominio);
		
		
		salvarEmail(pessoafisicaCondominio.getPessoaFisica());	

		salvarSenhasEnviarEmail(pessoafisicaCondominio.getPessoaFisica(),condominio);
		
		//vicular o funcionario sindico a um condominio
		//vicularCondominioPessoaFisica(pessoafisicaCondominio.getPessoaFisica(),pessoafisicaCondominio.getCondominio());
		
	}
	
	
	
	
	/**
	 * SALVAR UM SINDICO E ENVIAR NOVA SENHA DE ACESSO
	 * @param pessoafisicaCondominio
	 * @param usuario
	 */
	public void incluirSindicoEdicaoEnviandoSenha(PessoaFisicaCondominio pessoafisicaCondominio,
			UsuarioSistema usuario) {
		
		pessoafisicaCondominio.getPessoaFisica().setPapeis("SÍNDICO");
		
		//pessoafisicaCondominio.getCondominio().getPessoaJuridica().setCnpj(pessoafisicaCondominio.getCondominio().getPessoaJuridica().getCnpj().trim().replaceAll("\\.|-|/", ""));
		
		//pessoaFisicaCondominioRepository.save(pessoafisicaCondominio);
		 Condominio condominio = condominioRepository.condominioComPessoaJuridica(pessoafisicaCondominio.getCondominio().getId());
		 
		 alterarSenhasEnviarEmail(pessoafisicaCondominio.getPessoaFisica(),condominio);
		
	}
	
	
	public void salvarSindicoEdicao(PessoaFisicaCondominio pessoafisicaCondominio,
			UsuarioSistema usuario) {


		Condominio condominio = condominioRepository.findById(pessoafisicaCondominio.getCondominio().getId());

		PessoaFisicaCondominio sindicosAtual = condominio.pfCondominioSindicoAtual();

//		if(sindicosAtual.getId()!=pessoafisicaCondominio.getId()){
//			sindicosAtual.setDataFim(pessoafisicaCondominio.getInicio());
//			
//			pessoaFisicaCondominioRepository.save(sindicosAtual);
//		}

		pessoaFisicaService.salvarPessoaFisicaCondominioTipoPessoa(TipoPessoaFisicaCondominioEnum.SINDICO.getId(),pessoafisicaCondominio);

	}





	private void vicularPerfilCondominioPessoaFisica(PessoaFisica pessoaFisica,
			PerfilCondominio perfilCondominio) {
		
		PessoaFisicaPerfilCondominio pessoaFisicaPerfilCondominio = new PessoaFisicaPerfilCondominio();
		pessoaFisicaPerfilCondominio.setPerfilCondominio(perfilCondominio);
		pessoaFisicaPerfilCondominio.setPessoaFisica(pessoaFisica);
		pessoaFisicaPerfilCondominioRepository.save(pessoaFisicaPerfilCondominio);
		
		
	}


//	private void vicularCondominioPessoaFisica(PessoaFisica pessoaFisica,Condominio condominio) {
//		PessoaFisicaCondominio pessoaFisicaCondominio = new PessoaFisicaCondominio();
//		pessoaFisicaCondominio.setPessoaFisica(pessoaFisica);
//		pessoaFisicaCondominio.setCondominio(condominio);
//		pessoaFisicaCondominioRepository.save(pessoaFisicaCondominio);
//
//	}


		private void salvarEmail(PessoaFisica pessoaFisica) {
				Email email = new Email();
				email.setId(0);
				email.setNome(pessoaFisica.getEmails().get(0).getNome());
				email.setPessoaFisica(pessoaFisica);
				email.setAtivo(1);
				emailRepository.saveAndFlush(email);
		}



	private void salvarSenhasEnviarEmail(PessoaFisica pessoaFisica,Condominio condominio) {

		if((pessoaFisica.getSenhas()==null ||
				pessoaFisica.getSenhas().isEmpty())) {//se for sindico
			String senhaAleatoria  = getSenhaAleatoria(8);
			Senhas senha  = new Senhas();
			senha.setSenha(passwordEncoder.encode(senhaAleatoria));
			senha.setPessoaFisica(pessoaFisica);
			senhasRepository.save(senha);
			enviarEmailComSenha(pessoaFisica, condominio,senhaAleatoria);		
		}
	}
	
	
	private void alterarSenhasEnviarEmail(PessoaFisica pessoaFisica,Condominio condominio) {

		
		List<Senhas> senhas =  senhasRepository.findByPessoaFisicaId(pessoaFisica.getId());
		
		
		if((senhas==null ||
				senhas.isEmpty())) {//se for sindico
			String senhaAleatoria  = getSenhaAleatoria(6);
			Senhas senha  = new Senhas();
			senha.setSenha(passwordEncoder.encode(senhaAleatoria));
			senha.setPessoaFisica(pessoaFisica);
			senhasRepository.save(senha);
			enviarEmailComSenha(pessoaFisica, condominio,senhaAleatoria);		
		}
		else {
			
			
			String senhaAleatoria  = getSenhaAleatoria(6);
			senhas.get(0).setSenha(passwordEncoder.encode(senhaAleatoria));
			senhas.get(0).setPessoaFisica(pessoaFisica);
			senhasRepository.save(senhas.get(0));
			enviarEmailComSenha(pessoaFisica, condominio,senhaAleatoria);
			
		}
	}



	public String getSenhaAleatoria(int tamanho) {
		char[] chart ={'0','1','2','3','4','5','6','7','8','9'};
		char[] senha= new char[tamanho];
		int chartLenght = chart.length;
		Random rdm = new Random();

		for (int i=0; i<tamanho; i++) {
			senha[i] = chart[rdm.nextInt(chartLenght)];
		}

		return new String(senha);

	}

	/**
	 * ATENÇÃO SETAR A SENHA SEM CRIPTOGRAFIA PARA ENVIAR PARA O USUARIO
	 * @param pessoa
	 * @param condominio
	 */
	public void enviarEmailComSenha(PessoaFisica pessoa,Condominio condominio,String senha) {
		System.err.println(senha);
		mailer.enviar(pessoa,senha,pessoa.getEmails().get(0));


	}





	

}
