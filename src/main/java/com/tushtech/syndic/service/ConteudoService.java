package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Conteudo;
import com.tushtech.syndic.repository.ConteudoRepository;

@Service
public class ConteudoService {

	@Autowired
	private ConteudoRepository ConteudoRepository;
	
	public void salvarConteudo(Conteudo conteudo){
		
		this.ConteudoRepository.saveAndFlush(conteudo);
	}
	
	public void excluirConteudo(Conteudo conteudo){
		this.ConteudoRepository.delete(conteudo);
	}

	
}
