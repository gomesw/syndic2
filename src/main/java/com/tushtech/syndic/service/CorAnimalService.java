package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.CorAnimal;
import com.tushtech.syndic.repository.CorAnimalRepository;

@Service
public class CorAnimalService {

	@Autowired
	private CorAnimalRepository corAnimalRepository;
	
	public void salvar(CorAnimal corAnimal){
		corAnimal.setAtivo(1);
		this.corAnimalRepository.saveAndFlush(corAnimal);
	}
	
	public void excluir(CorAnimal corAnimal){
		this.corAnimalRepository.delete(corAnimal);
	}

	
}
