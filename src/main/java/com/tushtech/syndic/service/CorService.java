package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Cor;
import com.tushtech.syndic.repository.CorRepository;

@Service
public class CorService {

	@Autowired
	private CorRepository corRepository;
	
	public void salvar(Cor cor){
		cor.setAtivo(1);
		this.corRepository.saveAndFlush(cor);
	}
	
	public void excluir(Cor cor){
		this.corRepository.delete(cor);
	}

	
}
