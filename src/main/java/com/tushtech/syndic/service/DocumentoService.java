package com.tushtech.syndic.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.multipart.MultipartFile;

import com.tushtech.syndic.dto.ArquivoDTO;
import com.tushtech.syndic.dto.DocumentoFiltro;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.Documento;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.StatusDocumento;
import com.tushtech.syndic.entity.TipoDocumento;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.DocumentoRepository;
import com.tushtech.syndic.repository.StatusDocumentoRepository;
import com.tushtech.syndic.repository.TipoDocumentoRepository;

@Service
public class DocumentoService {

	@Autowired
	private DocumentoRepository documentoRepository;
	
	
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;
	
	
	@Autowired
	private StatusDocumentoRepository statusDocumentoRepository;
	
	
	public void salvarDocumento(Documento documento){
		
		this.documentoRepository.saveAndFlush(documento);
		//return animal.getId();
	}
	
	
	public void excluirDocumento(Documento documento){
		this.documentoRepository.delete(documento);
	}
	
	
	
	
	public void enviarParaUnidadesEscolidas(String tipo, String endereco, String subendereco,
			String unidadehabitacional, String obs, UsuarioSistema usuario, DeferredResult<ArquivoDTO> resultado,
			Condominio condominio) {
		
				if(Integer.valueOf(endereco)==0
						&& Integer.valueOf(subendereco)==0 
						&& Integer.valueOf(unidadehabitacional)==0 
						) {
		
					TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(Integer.parseInt(tipo));
					StatusDocumento statusDocumento = statusDocumentoRepository.findById(1);
		
		
					ArquivoDTO arquivo = (ArquivoDTO)resultado.getResult();
		
		
					for (Unidade unidade : condominio.getUnidades()) {
		
						for (PessoaFisicaUnidade pfunidade :  unidade.getListaProprietarioUnidade()) {
		
							criarDocumentoInserir(obs, usuario, tipoDocumento, statusDocumento, arquivo, unidade,
									pfunidade);
		
						}
		
		
					}
				}		
		
				else if(Integer.valueOf(endereco)!=0
						&& Integer.valueOf(subendereco)==0 
						&& Integer.valueOf(unidadehabitacional) ==0
						) {
		
					TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(Integer.parseInt(tipo));
					StatusDocumento statusDocumento = statusDocumentoRepository.findById(1);
		
		
		
					ArquivoDTO arquivo = (ArquivoDTO)resultado.getResult();
		
					for (Unidade unidade : condominio.getUnidades()) {
		
						if(unidade.getEnderecoUnidade().getId()==Integer.valueOf(endereco)) {
							for (PessoaFisicaUnidade pfunidade :  unidade.getListaProprietarioUnidade()) {
								criarDocumentoInserir(obs, usuario, tipoDocumento, statusDocumento, arquivo, unidade,
										pfunidade);
							}
		
						}
					}
		
		
				}
		
				else if(Integer.valueOf(unidadehabitacional) !=0) {
		
		
					TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(Integer.parseInt(tipo));
					StatusDocumento statusDocumento = statusDocumentoRepository.findById(1);
		
		
					if(Integer.valueOf(tipo)==2) {//enviar um ou vários arquivos para unidades escolhidas
		
						ArquivoDTO arquivo = (ArquivoDTO)resultado.getResult();
		
						for (Unidade unidade : condominio.getUnidades()) {
							if(unidade.getId()==Integer.valueOf(endereco)) {
								for (PessoaFisicaUnidade pfunidade :  unidade.getListaProprietarioUnidade()) {
									criarDocumentoInserir(obs, usuario, tipoDocumento, statusDocumento, arquivo, unidade,
											pfunidade);
								}
		
							}
						}
		
					}
		
				}
		
				else if(Integer.valueOf(subendereco)!=0 
						&& Integer.valueOf(unidadehabitacional) ==0
						) {
		
					TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(Integer.parseInt(tipo));
					StatusDocumento statusDocumento = statusDocumentoRepository.findById(1);
		
					ArquivoDTO arquivo = (ArquivoDTO)resultado.getResult();
		
					for (Unidade unidade : condominio.getUnidades()) {
		
						if(unidade.getTipoEnderecoUnidade().getId()==Integer.valueOf(subendereco)) {
		
							for (PessoaFisicaUnidade pfunidade :  unidade.getListaProprietarioUnidade()) {
								criarDocumentoInserir(obs, usuario, tipoDocumento, statusDocumento, arquivo, unidade,
										pfunidade);
							}
		
						}
					}
		
		}
	}
	
	
	
	
	
	
	
	public void adicionarArquivos(String id,DeferredResult<ArquivoDTO> arquivoDTO){
		Documento documento = documentoRepository.findOne(Integer.valueOf(id));

		if(documento!=null&&documento.getId()!=0){

			ArquivoDTO arquivo = (ArquivoDTO)arquivoDTO.getResult();

			documento.setNome(arquivo.getNome());
			documento.setContentType(arquivo.getContentType()); 
			documentoRepository.save(documento);
		}

	}
	
	
	
	public void removerArquivosUnidade(String id){
		System.err.println("REMOCVER AQUIVOS UNIDADE");

	}
	
	
	
	
	public void criarDocumentoInserir(String obs, UsuarioSistema usuario, TipoDocumento tipoDocumento,
			StatusDocumento statusDocumento, ArquivoDTO arquivo, Unidade unidade, PessoaFisicaUnidade pfunidade) {
		Documento documento = new Documento();
		documento.setAtivo(1);
		documento.setCadastrante(usuario.getPessoaFisica());
		documento.setDataCadastro(Date.valueOf(LocalDate.now()));
		documento.setContentType(arquivo.getContentType());
		documento.setDataValidacao(Date.valueOf(LocalDate.now()));
		documento.setDescricao(obs.isEmpty()?"Sem Observações":obs);
		documento.setNome(arquivo.getNome());
		documento.setPessoaFisicaUnidade(pfunidade);//pessoa de destino
		documento.setTipoDocumento(tipoDocumento);
		documento.setUnidade(unidade);
		documento.setValidador(usuario.getPessoaFisica());
		documento.setStatusDocumento(statusDocumento);
		documentoRepository.save(documento);
	}
	
	
	
	
	
	private Optional<Unidade> verificarPorTipoEndereco(String subendereco, Condominio condominio,
			Optional<Unidade> ptdUnidade, String nomeCerto, String nomeAlternativo, String nomeAlternativo2) {
		
		
		for (Unidade unidade : condominio.getUnidades()) {
			
			String nomes[] = getPossiveisNomes(unidade);
			
			if(unidade.getTipoEnderecoUnidade().getId()==Integer.valueOf(subendereco)) {

				if(isNomeEncontrado(nomeCerto, nomeAlternativo, nomeAlternativo2, nomes)){
					ptdUnidade = Optional.of(unidade);
					break;

				}

			}
		}
		
		return ptdUnidade;
	}


	private boolean isNomeEncontrado(String nomeCerto, String nomeAlternativo, String nomeAlternativo2,
			String[] nomes) {
		return nomes[0].equals(nomeCerto)||nomes[1].equals(nomeCerto) || 
		nomes[0].equals(nomeAlternativo2)||nomes[1].equals(nomeAlternativo2)||
		nomes[0].equals(nomeAlternativo)||nomes[1].equals(nomeAlternativo) ||
		
		nomes[2].equals(nomeCerto)||nomes[3].equals(nomeCerto) || 
		nomes[2].equals(nomeAlternativo2)||nomes[3].equals(nomeAlternativo2)||
		nomes[2].equals(nomeAlternativo)||nomes[3].equals(nomeAlternativo);
	}
	
	
	private String[] getPossiveisNomes(Unidade unidade) {
		
		String nomes[] = new String[4];
		
		nomes[0] = unidade.getTipoEnderecoUnidade().getDescricao().trim().toUpperCase()+" "+unidade.getTipoUnidade().getNome().trim().toUpperCase()+" "+
				unidade.getUnidadeHabitacional().trim().toUpperCase();

		nomes[1] = unidade.getTipoEnderecoUnidade().getDescricao().trim().toUpperCase()+" "+unidade.getTipoUnidade().getSigla().trim().toUpperCase()+" "+
				unidade.getUnidadeHabitacional().trim().toUpperCase();
		
		
		nomes[2] = unidade.getEnderecoUnidade().getDescricao().trim().toUpperCase()+" "+ unidade.getTipoEnderecoUnidade().getDescricao().trim().toUpperCase()+" "+unidade.getTipoUnidade().getNome().trim().toUpperCase()+" "+
				unidade.getUnidadeHabitacional().trim().toUpperCase();

		nomes[3] = unidade.getEnderecoUnidade().getDescricao().trim().toUpperCase()+" "+unidade.getTipoEnderecoUnidade().getDescricao().trim().toUpperCase()+" "+unidade.getTipoUnidade().getSigla().trim().toUpperCase()+" "+
				unidade.getUnidadeHabitacional().trim().toUpperCase();
		
		return nomes;
		
	}


	private Optional<Unidade> verificarUnidadeEspecifica(String endereco, Condominio condominio,
			Optional<Unidade> ptdUnidade, String nomeCerto, String nomeAlternativo, String nomeAlternativo2) {
		for (Unidade unidade : condominio.getUnidades()) {
			
			String nomes[] = getPossiveisNomes(unidade);
			
			if(unidade.getId()==Integer.valueOf(endereco)) {

				if(isNomeEncontrado(nomeCerto, nomeAlternativo, nomeAlternativo2, nomes)){
					ptdUnidade = Optional.of(unidade);
					break;

				}

			}
		}
		return ptdUnidade;
	}


	private Optional<Unidade> verificarUnidadesDoEndereco(String endereco, Condominio condominio,
			Optional<Unidade> ptdUnidade, String nomeCerto, String nomeAlternativo, String nomeAlternativo2) {
		for (Unidade unidade : condominio.getUnidades()) {

			String nomes[] = getPossiveisNomes(unidade);
			
			if(unidade.getEnderecoUnidade().getId()==Integer.valueOf(endereco)) {
				

				if(isNomeEncontrado(nomeCerto, nomeAlternativo, nomeAlternativo2, nomes)){
					ptdUnidade = Optional.of(unidade);
					break;

				}

			}
		}
		return ptdUnidade;
	}


	private Optional<Unidade> verificarTodasUnidadesCondominio(Condominio condominio, Optional<Unidade> ptdUnidade,
			String nomeCerto, String nomeAlternativo, String nomeAlternativo2) {
		for (Unidade unidade : condominio.getUnidades()) {

			String nomes[] = getPossiveisNomes(unidade);
			
			
			if(isNomeEncontrado(nomeCerto, nomeAlternativo, nomeAlternativo2, nomes)){
				ptdUnidade = Optional.of(unidade);
				break;

			}

		}
		return ptdUnidade;
	}

	
	
	public Optional<Unidade> enviarParaUnidadeComNomeEquivalente(String tipo, String endereco, String subendereco,
			String unidadehabitacional, String obs, UsuarioSistema usuario,
			Condominio condominio, MultipartFile[] files) {


		Optional<Unidade> ptdUnidade = Optional.empty();

		String nomeCerto = "";

		String nome[] = files[0].getOriginalFilename().split("-");

		if(nome.length>1) {

			nomeCerto = nome[0];

		}
		else {

			String temp[] = nome[0].split("\\.");

			nomeCerto = temp[0];
		}


		nomeCerto = nomeCerto.toString().trim().toUpperCase();

		String nomeAlternativo =  files[0].getOriginalFilename().substring (0, files[0].getOriginalFilename().length() - 4);
		String nomeAlternativo2 =  files[0].getOriginalFilename().substring (0, files[0].getOriginalFilename().length() - 3);


		if(Integer.valueOf(endereco)==0
				&& Integer.valueOf(subendereco)==0 
				&& Integer.valueOf(unidadehabitacional)==0 
				) {

			//VERIFICAR EM TODAS UNIDADE
			ptdUnidade = verificarTodasUnidadesCondominio(condominio, ptdUnidade, nomeCerto, nomeAlternativo,
					nomeAlternativo2);

		}else if(Integer.valueOf(endereco)!=0
				&& Integer.valueOf(subendereco)==0 
				&& Integer.valueOf(unidadehabitacional) ==0
				) {

			ptdUnidade = verificarUnidadesDoEndereco(endereco, condominio, ptdUnidade, nomeCerto,
					nomeAlternativo, nomeAlternativo2);


		}else if(Integer.valueOf(unidadehabitacional) !=0
				) {


			ptdUnidade = verificarUnidadeEspecifica(endereco, condominio, ptdUnidade, nomeCerto, nomeAlternativo,
					nomeAlternativo2);


		}	else if(Integer.valueOf(subendereco)!=0 
				&& Integer.valueOf(unidadehabitacional) ==0
				) {

			ptdUnidade = verificarPorTipoEndereco(subendereco, condominio, ptdUnidade, nomeCerto, nomeAlternativo,
					nomeAlternativo2);


		}


		return ptdUnidade;
	}

	

	public boolean temErrosBusca(DocumentoFiltro documentoFiltro) {
		
		
		if(documentoFiltro.getDescricao()==null 
				&& documentoFiltro.getEnderecoUnidade().getId()==0 
				&& documentoFiltro.getTipoDocumento().getId()==0 
				&& documentoFiltro.getUnidade().getId()==0){
			
			
			return true;
			
		}
		
		return false;
		
		
		
		
		
	}




	
}
