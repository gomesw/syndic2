package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Email;
import com.tushtech.syndic.repository.EmailRepository;

/**
 * @author Sd octaviano
 * Gerencia 
 * 		-email
 *
 */
@Service
public class EmailService {

	@Autowired
	private EmailRepository emailRepository; 

	public void salvar(Email email){
		email.setAtivo(1);
		this.emailRepository.save(email);

	}
}
