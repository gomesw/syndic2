package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.EnderecoUnidade;
import com.tushtech.syndic.repository.EnderecoUnidadeRepository;

@Service
public class EnderecoUnidadeService {

	@Autowired
	private EnderecoUnidadeRepository enderecoUnidadeRepository;
	
	public void salvarEnderecoUnidade(EnderecoUnidade enderecoUnidade){
		this.enderecoUnidadeRepository.saveAndFlush(enderecoUnidade);
	}
	
	public void excluirEnderecoUnidade(EnderecoUnidade enderecoUnidade){
		this.enderecoUnidadeRepository.delete(enderecoUnidade);
	}

	
}
