package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.tushtech.syndic.entity.HistoricoOrdemServico;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.repository.HistoricoOrdemServicoRepository;

@Service
public class HistoricoOrdemServicoService {
	
	@Autowired
	private HistoricoOrdemServicoRepository historicoOrdemServicoRepository;
	
	public void salvar(HistoricoOrdemServico historicoOrdemServico){
			
		this.historicoOrdemServicoRepository.saveAndFlush(historicoOrdemServico);
	}
	
	public void atribuirErros(OrdemServico ordemServico,BindingResult result){
		
//		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
//		Validator validator = validatorFactory . getValidator () ;
//		Set<ConstraintViolation<TipoOrdemServico>> errors = validator . validate (ordemServico.getTipoOrdemServico()) ;
//		Set<ConstraintViolation<OrdemServicoStatus>> errorsstatus = validator . validate (ordemServico.getListHistoricoOrdemServico().get(0).getOrdemServicoStatus()) ;
		
		if(ordemServico.getTipoOrdemServico() == null){
			result.rejectValue("tipoOrdemServico","","tipo da ordem de serviço é obrigatório");
		}
		
		if(ordemServico.getListHistoricoOrdemServico().get(0).getOrdemServicoStatus()==null){
			result.rejectValue("listHistoricoOrdemServico[0].ordemServicoStatus","","Status da ordem de serviço é obrigatório");
		}
	}
	
}
