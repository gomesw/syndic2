package com.tushtech.syndic.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Login;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.repository.LoginRepository;

@Service
public class LoginService {

	@Autowired
	private LoginRepository loginRepository;
	
	public void salvar(PessoaFisica pf){
		
		Login loginauditoria = new Login();
		loginauditoria.setPessoaFisica(pf);
		loginauditoria.setDataLogin(Date.valueOf(LocalDate.now()));
		this.loginRepository.save(loginauditoria);
	}
	
	
	public void editar(PessoaFisica pf){
		
		List<Login> listaLogins = loginRepository.findByPessoaFisicaIdAndDataLogoutIsNull(pf.getId());
		
		for (Login login : listaLogins) {
			login.setDataLogout(Date.valueOf(LocalDate.now())); 
		}
		
		this.loginRepository.save(listaLogins);
	}
	
	

	
}
