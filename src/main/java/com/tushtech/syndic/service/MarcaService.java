package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Marca;
import com.tushtech.syndic.repository.MarcaRepository;

@Service
public class MarcaService {

	@Autowired
	private MarcaRepository MarcaRepository;
	
	public void salvarMarca(Marca marca){
		
		marca.setAtivo(1);
		this.MarcaRepository.saveAndFlush(marca);
	}
	
	public void excluirMarca(Marca marca){
		this.MarcaRepository.delete(marca);
	}

	
}
