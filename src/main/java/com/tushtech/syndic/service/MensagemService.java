package com.tushtech.syndic.service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.tushtech.syndic.entity.Mensagem;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaOrdemServico;
import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisica;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.repository.MensagemRepository;
import com.tushtech.syndic.repository.PessoaJuridicaPessoaFisicaRepository;

@Service
public class MensagemService {
	
	@Autowired
	private PessoaJuridicaPessoaFisicaRepository pessoaJuridicaPessoaFisicaRepository;
	
	@Autowired
	private MensagemRepository mensagemRepository;
	
	
	@Autowired
	private MensagemService mensagemService;

	public void salvar(Mensagem mensagem) {
		mensagemRepository.save(mensagem);
		
	}
	
	
	public Mensagem salvarRetorno(Mensagem mensagem) {
		 mensagem = mensagemRepository.saveAndFlush(mensagem);
		return mensagem;
	}

	
	
	public void marcarLida(Mensagem mensagem) {
		Calendar cal = Calendar.getInstance();
		mensagem.setDataRecebimento(cal.getTime());
		mensagem.setAlerta(1);
		mensagemRepository.save(mensagem);
	}



	public void gerarMensagemCriacaoOs(OrdemServico ordemServico,UsuarioSistema usuario) {
		
		List<PessoaJuridicaPessoaFisica> funcionarios = pessoaJuridicaPessoaFisicaRepository.findByPessoaJuridicaId(ordemServico.getEmpresa().getId());
		
		for (PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica : funcionarios) {
			
			if(pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==1 || pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==2 || pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==3){
			
				Mensagem mensagem = new Mensagem();
				Calendar cal = Calendar.getInstance();
				mensagem.setAssunto("O Condominio "+ordemServico.getCondominio().getPessoaJuridica().getNomeFantasia()+" abriu uma OS!");
				mensagem.setCondominio(ordemServico.getCondominio());
				mensagem.setObservacao(ordemServico.getObservacao());
				mensagem.setDataInclusao(cal.getTime());
				mensagem.setAtivo(1);
				mensagem.setAlerta(0);
				mensagem.setDestinatario(pessoaJuridicaPessoaFisica.getPessoaFisica());
				mensagem.setRemetente(usuario.getPessoaFisica());
				mensagem = mensagemService.salvarRetorno(mensagem);
				mensagem.setAcao("/mensagem/ordemservico/ficha/"+ordemServico.getId()+"/"+mensagem.getId());
				mensagemService.salvar(mensagem);
				
			}
		}
	}
	
	public void gerarMensagemPesquisaSatisfacao(OrdemServico ordemServico,UsuarioSistema usuario) {
		
		List<PessoaJuridicaPessoaFisica> funcionarios = pessoaJuridicaPessoaFisicaRepository.findByPessoaJuridicaId(ordemServico.getEmpresa().getId());
		
		for (PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica : funcionarios) {
			
			if(pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==1 || pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==2 || pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==3){
			
				Mensagem mensagem = new Mensagem();
				Calendar cal = Calendar.getInstance();
				mensagem.setAssunto("O Condominio "+ordemServico.getCondominio().getPessoaJuridica().getNomeFantasia()+" Respondeu a Pesquisa de Satisfação!");
				mensagem.setCondominio(ordemServico.getCondominio());
				mensagem.setObservacao(ordemServico.getListAvaliacaoOrdemServico().get(0).getObservacao()==null || ordemServico.getListAvaliacaoOrdemServico().get(0).getObservacao().trim().equals("")?"NÃO FEZ OBSERVAÇÕES SEOBRE A AVALIAÇÃO":ordemServico.getListAvaliacaoOrdemServico().get(0).getObservacao().trim());
				mensagem.setDataInclusao(cal.getTime());
				mensagem.setAtivo(1);
				mensagem.setAlerta(0);
				mensagem.setDestinatario(pessoaJuridicaPessoaFisica.getPessoaFisica());
				mensagem.setRemetente(usuario.getPessoaFisica());
				mensagem = mensagemService.salvarRetorno(mensagem);
				mensagem.setAcao("/mensagem/ordemservico/ficha/"+ordemServico.getId()+"/"+mensagem.getId());
				mensagemService.salvar(mensagem);
				
			}
		}
	}

	public void gerarMensagemParaSindico(OrdemServico ordemServico, UsuarioSistema usuario) {
		
		Mensagem mensagem = new Mensagem();
		Calendar cal = Calendar.getInstance();
		mensagem.setAssunto("A empresa "+ usuario.getPessoaJuridica().getNomeFantasia() +" alterou o status da OS " + ordemServico.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().getNome() + "/" 
								+ ordemServico.getTipoOrdemServico().getNome() + " para " + ordemServico.getUltimoHistoricoOrdemServico().getOrdemServicoStatus().getNome());
		
		mensagem.setObservacao(ordemServico.getUltimoHistoricoOrdemServico().getDescricao());
		mensagem.setDataInclusao(cal.getTime());
		mensagem.setAtivo(1);
		mensagem.setAlerta(0);
		mensagem.setDestinatario(ordemServico.getCondominio().sidicoAtual());
		mensagem.setRemetente(usuario.getPessoaFisica());
		mensagem = mensagemService.salvarRetorno(mensagem);
		mensagem.setAcao("/mensagem/ordemservico/ficha/"+ordemServico.getId()+"/"+mensagem.getId());
		mensagemService.salvar(mensagem);
	}


	public void gerarMensagemParaResponsavel(PessoaFisicaOrdemServico pessoaFisicaOrdemServico,
			UsuarioSistema usuario) {
		
		
		Mensagem mensagem = new Mensagem();
		Calendar cal = Calendar.getInstance();
		mensagem.setAssunto("Você foi viculado a Ordem de serviço "+pessoaFisicaOrdemServico.getOrdemServico().getId()+"/"+LocalDate.now().getYear());
		mensagem.setObservacao(pessoaFisicaOrdemServico.getObservacao());
		mensagem.setDataInclusao(cal.getTime());
		
		mensagem.setCondominio(pessoaFisicaOrdemServico.getOrdemServico().getCondominio());
		mensagem.setAtivo(1);
		mensagem.setAlerta(0);
		mensagem.setDestinatario(pessoaFisicaOrdemServico.getPessoaFisica());
		mensagem.setRemetente(usuario.getPessoaFisica());
		mensagem = mensagemService.salvarRetorno(mensagem);
		mensagem.setAcao("/mensagem/funcionario/ordemservico/ficha/"+pessoaFisicaOrdemServico.getId()+"/"+mensagem.getId());
		mensagemService.salvar(mensagem);
		
	}
	

	public void gerarMensagemOrcamentoEscolhido(OrdemServico ordemServico,PessoaFisica pessoa) {
		List<PessoaJuridicaPessoaFisica> funcionarios = pessoaJuridicaPessoaFisicaRepository.findByPessoaJuridicaId(ordemServico.getEmpresa().getId());
		
		for (PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica : funcionarios) {
			
			if(pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==1 || pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==2 || pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==3){
			
				Mensagem mensagem = new Mensagem();
				Calendar cal = Calendar.getInstance();
				mensagem.setAssunto("O Condominio "+ordemServico.getCondominio().getPessoaJuridica().getNomeFantasia()+" escolheu seu orçamento para OS "+ordemServico.getId()+"/2018!");
				mensagem.setCondominio(ordemServico.getCondominio());
				mensagem.setObservacao("O síndico do condomínio escolheu um orçamento  , você já pode dar seguimento na ordem de serviço.");
				mensagem.setDataInclusao(cal.getTime());
				mensagem.setAtivo(1);
				mensagem.setAlerta(0);
				mensagem.setDestinatario(pessoaJuridicaPessoaFisica.getPessoaFisica());
				mensagem.setRemetente(pessoa);
				mensagem = mensagemService.salvarRetorno(mensagem);
				mensagem.setAcao("/mensagem/ordemservico/ficha/"+ordemServico.getId()+"/"+mensagem.getId());
				mensagemService.salvar(mensagem);
				
			}
		}
		
	}
	
	
	public void gerarMensagemNotificacaoOrcamento(OrdemServico ordemServico,PessoaFisica pessoa) {
		List<PessoaJuridicaPessoaFisica> funcionarios = pessoaJuridicaPessoaFisicaRepository.findByPessoaJuridicaId(ordemServico.getEmpresa().getId());
		
		for (PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica : funcionarios) {
			
			if(pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==1 || pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==2 || pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getId()==3){
			
				Mensagem mensagem = new Mensagem();
				Calendar cal = Calendar.getInstance();
				mensagem.setAssunto("Já foram feitos orçamentos para sua Ordem de Serviço");
				mensagem.setCondominio(ordemServico.getCondominio());
				mensagem.setObservacao("Foram incluidos orçamentos a sua ordem de servico "+ordemServico.getId()+"/2018 - "+ordemServico.getTipoOrdemServico().getTipoOrdemServicoSubordinacao().getNome()+" / "+ ordemServico.getTipoOrdemServico().getNome());
				mensagem.setDataInclusao(cal.getTime());
				mensagem.setAtivo(1);
				mensagem.setAlerta(0);
				mensagem.setDestinatario(ordemServico.getCondominio().sidicoAtual());
				mensagem.setRemetente(pessoa);
				mensagem = mensagemService.salvarRetorno(mensagem);
				mensagem.setAcao("/mensagem/ordemservico/ficha/"+ordemServico.getId()+"/"+mensagem.getId());
				mensagemService.salvar(mensagem);
				
			}
		}
	}
	
	public void atribuirErros(Mensagem msg, BindingResult result) {
		if(msg.getDestinatario() == null || msg.getDestinatario().getId() == 0){
			result.rejectValue("destinatario.id","","Informe o Destinatário da Mensagem!");
		} 
		if(msg.getAssunto() == null || msg.getAssunto() == ""){
			result.rejectValue("assunto","","Informe um Título para a Mensagem!");
		} 
	}


	public void marcarLidaPopUp(Mensagem mensagem) {
		mensagem.setAlerta(1);
		mensagemRepository.save(mensagem);
	}

}
