package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Modelo;
import com.tushtech.syndic.repository.ModeloRepository;

@Service
public class ModeloService {

	@Autowired
	private ModeloRepository ModeloRepository;
	
	public void salvar(Modelo modelo){
		this.ModeloRepository.saveAndFlush(modelo);
	}
	
	public void excluirModelo(Modelo modelo){
		this.ModeloRepository.delete(modelo);
	}

	
}
