package com.tushtech.syndic.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.tushtech.syndic.entity.UsuarioSistema;
@Component
public class MyLogoutSuccessHandlerOne implements LogoutSuccessHandler {
	
	
	@Autowired
	LoginService loginService;
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		if(authentication != null) {
			
			Object obj = authentication.getPrincipal();
			
			if (authentication instanceof UsuarioSistema){
			UsuarioSistema usuario =  (UsuarioSistema) authentication;
				loginService.editar(usuario.getPessoaFisica());
			}
			
//			/loginService.editar();
			
		}
		//perform other required operation
		String URL = request.getContextPath() + "/login";
		response.setStatus(HttpStatus.OK.value());
		response.sendRedirect(URL);
	}
} 
