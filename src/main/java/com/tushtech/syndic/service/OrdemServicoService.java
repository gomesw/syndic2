package com.tushtech.syndic.service;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.tushtech.syndic.entity.HistoricoOrdemServico;
import com.tushtech.syndic.entity.OrdemServico;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.mail.Mailer;
import com.tushtech.syndic.repository.HistoricoOrdemServicoRepository;
import com.tushtech.syndic.repository.OrdemServicoRepository;
import com.tushtech.syndic.repository.OrdemServicoStatusRepository;
import com.tushtech.syndic.repository.PessoaJuridicaPessoaFisicaRepository;

@Service
public class OrdemServicoService {
	
	@Autowired
	private OrdemServicoRepository ordemServicoRepository;
	
	@Autowired
	private HistoricoOrdemServicoRepository historicoOrdemServicoRepository;
	
	@Autowired
	private OrdemServicoStatusRepository ordemServicoStatusRepository;
	
	@Autowired
	private PessoaJuridicaPessoaFisicaRepository pessoaJuridicaPessoaFisicaRepository;
	
	@Autowired
	private MensagemService mensagemService;
	
	@Autowired
	private Mailer mailer;

	@Transactional
	public void salvar(OrdemServico ordemServico){
		
		OrdemServico ultimaOS = ordemServicoRepository.findFirstByOrderByIdDesc(); 
		
		if(ultimaOS == null){
			ordemServico.setNumeroProtocolo(1);
		}else{
			ordemServico.setNumeroProtocolo(ultimaOS.getNumeroProtocolo()+1);
		}
		
		//salva a OS com o primeiro historico
		ordemServico.setServicoIdentificado(0);
		
		ordemServico = this.ordemServicoRepository.saveAndFlush(ordemServico);
		
		salvarHistoricoInicial(ordemServico, 1); //ordem de serviço aberta / primeiro status!
	}
	
	@Transactional
	public void salvarHistoricoInicial(OrdemServico ordemServico, int idStatusOrdemServico){
		
		HistoricoOrdemServico historicoOrdemServico = new HistoricoOrdemServico();
		
		//buscar status da ordem de serviço caso não seja a primeira vez que a OS é manipulada pelo usuario
		if(idStatusOrdemServico != 1){
			//setar a data fim caso esteja sendo criado um novo status diferente de 1(aberto)
			setDataFim(ordemServico);
		}
		
		historicoOrdemServico.setOrdemServico(ordemServico);
		historicoOrdemServico.setOrdemServicoStatus(ordemServicoStatusRepository.findOne(idStatusOrdemServico));
		historicoOrdemServico.setDataInicio(new Timestamp(System.currentTimeMillis()));
		
		historicoOrdemServicoRepository.saveAndFlush(historicoOrdemServico);
	}
	
	@Transactional
	public HistoricoOrdemServico salvarNovoHistoricoOS(HistoricoOrdemServico historicoOrdemServico){
		
		//setar a data fim caso esteja sendo criado um novo status diferente de 1(aberto)
		setDataFim(historicoOrdemServico.getOrdemServico());		
		historicoOrdemServico.setDataInicio(new Timestamp(System.currentTimeMillis()));
		
		//setar data fim se for o ultimo status, por exemplo: servico executado - id = 7
//		if(historicoOrdemServico.getOrdemServicoStatus().getId() == 7){
//			historicoOrdemServico.setDataFim(new Timestamp(System.currentTimeMillis()));
//		}
		
		return historicoOrdemServicoRepository.saveAndFlush(historicoOrdemServico);
	}

	private void setDataFim(OrdemServico ordemServico) {
		
		List<HistoricoOrdemServico> listHistorico = historicoOrdemServicoRepository.findByOrdemServicoId(ordemServico.getId());
		
		if(listHistorico.size() > 0){
			for (HistoricoOrdemServico historicoOrdemServico2 : listHistorico) {
				
				if(historicoOrdemServico2.getDataFim() == null){
					historicoOrdemServico2.setDataFim(new Timestamp(System.currentTimeMillis()));
					historicoOrdemServicoRepository.saveAndFlush(historicoOrdemServico2);					
				}
				
			}
		}
	}
	
	public void enviarEmailAoSalvar(OrdemServico ordemServico, UsuarioSistema usuario) {
		
		mailer.enviarEmailOS(usuario,ordemServico,"Conteudo");
		
	}

	public void enviarMensagensAoSalvar(OrdemServico ordemServico, UsuarioSistema usuario) {
		
		mensagemService.gerarMensagemCriacaoOs(ordemServico,usuario);
	}
	
	public void enviarMensagensAoRealizarPesquisaSatisfacao(OrdemServico ordemServico, UsuarioSistema usuario) {
		
		mensagemService.gerarMensagemPesquisaSatisfacao(ordemServico,usuario);
	}
	
	public void enviarMensagensAoSalvarParaSindico(OrdemServico ordemServico, UsuarioSistema usuario) {
		
		mensagemService.gerarMensagemParaSindico(ordemServico,usuario);
	}
	
	public void atribuirErros(OrdemServico ordemServico, BindingResult result){

		if(ordemServico.getValorMaoDeObra() == null){
			result.rejectValue("ordemServico.valorMaoDeObra","","Informe o valor da mão de obra!");
		}

		if(ordemServico.getValorMaterial() == null){
			result.rejectValue("ordemServico.valorMaterial","","Informe o valor do material!");
		}
	}
	
//	@Transactional
//	public OrdemServico desativar(OrdemServico ordemServico) {
//		ordemServico = ordemServicoRepository.getOne(ordemServico.getId());
//		
//		salvarHistorico(ordemServico, 6); //status 6 = cancelado!
//		
//		this.ordemServicoRepository.saveAndFlush(ordemServico); 
//		return ordemServico;
//	}
}