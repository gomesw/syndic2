package com.tushtech.syndic.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.PacoteServico;
import com.tushtech.syndic.repository.PacoteServicoRepository;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;

@Service
public class PacoteServicosService {

	@Autowired
	private PacoteServicoRepository pacoteServicoRepository;

	@Transactional
	public void salvar(PacoteServico pacoteServico){   

		if(pacoteServico.isNovo()){
			//VERIFICA SE MARCA DE VEICULO JÁ FOI CADASTRADA
			Optional<PacoteServico> pacoteServicoExiste = pacoteServicoRepository.findByNome(pacoteServico.getNome());
																				
			if(pacoteServicoExiste.isPresent()){
				if(pacoteServicoExiste.get().getAtivo()==0){
					throw new NomeDuplicadoException(" Pacote de serviço está desativado! <a style=\"color:#ffffff;\" href=\"/pacoteservicos/ativar/"+pacoteServicoExiste.get().getId()+"\">Deseja Ativá-la? <strong>SIM</strong></a>"	);
				}
				else{
					throw new NomeDuplicadoException("Pacote de Serviço já cadastrado");
				}
			}
		}

		pacoteServico.setAtivo(1);
		pacoteServicoRepository.save(pacoteServico);
	}

	
	@Transactional
	public PacoteServico desativar(PacoteServico pacoteServico) {
		pacoteServico = pacoteServicoRepository.findOne(pacoteServico.getId());
		pacoteServico.setAtivo(0);
		this.pacoteServicoRepository.saveAndFlush(pacoteServico); 
		return pacoteServico;
	}
	
	@Transactional
	public PacoteServico ativar(PacoteServico pacoteServico) {
		pacoteServico = pacoteServicoRepository.findOne(pacoteServico.getId());
		pacoteServico.setAtivo(1);
		this.pacoteServicoRepository.saveAndFlush(pacoteServico); 
		return pacoteServico;
	}
}
