package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Parentesco;
import com.tushtech.syndic.repository.ParentescoRepository;

@Service
public class ParentescoService {

	@Autowired
	private ParentescoRepository parentescoRepository;
	
	public void salvar(Parentesco parentesco){
		this.parentescoRepository.saveAndFlush(parentesco);
	}
	
	public void excluir(Parentesco parentesco){
		this.parentescoRepository.delete(parentesco);
	}

	
}
