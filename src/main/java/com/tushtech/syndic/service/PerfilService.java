package com.tushtech.syndic.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Perfil;
import com.tushtech.syndic.entity.PerfilRecurso;
import com.tushtech.syndic.entity.Recurso;
import com.tushtech.syndic.repository.PerfilRecursoRepository;
import com.tushtech.syndic.repository.PerfilRepository;
import com.tushtech.syndic.repository.RecursoRepository;


@Service
public class PerfilService {


	@Autowired
	private PerfilRepository perfilRepository; 


	@Autowired
	private RecursoRepository recursoRepository; 


	@Autowired
	private PerfilRecursoRepository perfilRecursoRepository;





	public void salvarPerfil(Perfil perfil){
		perfil.setAtivo(1);
		this.perfilRepository.saveAndFlush(perfil);

	}


	public void editarPerfil(Perfil perfil){
		perfil.setAtivo(1);
		this.perfilRepository.saveAndFlush(perfil);

	}



	public Perfil desativarPerfil(Perfil perfil){
		perfil.setAtivo(0);
		this.perfilRepository.save(perfil);
		return perfil;
	}

	public Perfil ativarPerfil(Perfil perfil){
		perfil.setAtivo(1);
		this.perfilRepository.save(perfil);
		return perfil;
	}

	public void atualizarPerfilRecurso(String[] nomes, Integer id) {
		Perfil perfil = perfilRepository.findById(id);

		List<PerfilRecurso> listPerfilRecursoAntes = perfil.getListperfilrecursos();

		List<Recurso> listRecursoDepois = recursoRepository.findByNomeIn(nomes);

		List<PerfilRecurso> listPerfilRecursoDepois = new ArrayList<>();

		for (Recurso recurso : listRecursoDepois) {
			PerfilRecurso perfilRecurso = new PerfilRecurso();
			perfilRecurso.setPerfil(perfil);
			perfilRecurso.setRecurso(recurso);
			listPerfilRecursoDepois.add(perfilRecurso);
		}

		//SE FOR EXCLUSÃO
		if(listPerfilRecursoAntes.size() > listRecursoDepois.size()){
			for (PerfilRecurso funrecursoantes : listPerfilRecursoAntes) {
				boolean isexiste = false;

				for (Recurso funrecursodepois : listRecursoDepois) {
					if(funrecursoantes.getRecurso().getId()==funrecursodepois.getId()){
						isexiste = true;
						break;
					}
				}

				if(!isexiste){
					perfilRecursoRepository.delete(funrecursoantes);
				} 

			}

		}
		//SE FOR INCLUSÃO
		else{
			for (PerfilRecurso funrecursodepois : listPerfilRecursoDepois) {
				boolean isexiste = false;
				for (PerfilRecurso funrecursoantes : listPerfilRecursoAntes) {
					if(funrecursodepois.getRecurso().getId()==funrecursoantes.getRecurso().getId()){
						isexiste = true;
						break;
					}
				}
				if(!isexiste){
					perfilRecursoRepository.save(funrecursodepois);
				}
			}
		}

	}

}
