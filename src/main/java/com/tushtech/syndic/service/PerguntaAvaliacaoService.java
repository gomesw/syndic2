package com.tushtech.syndic.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.PerguntaAvaliacao;
import com.tushtech.syndic.repository.PerguntaAvaliacaoRepository;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;

@Service
public class PerguntaAvaliacaoService {

	@Autowired
	private PerguntaAvaliacaoRepository perguntaAvaliacaoRepository;

	@Transactional
	public void salvar(PerguntaAvaliacao perguntaAvaliacao){   

		if(perguntaAvaliacao.isNovo()){
			//VERIFICA SE MARCA DE VEICULO JÁ FOI CADASTRADA
			Optional<PerguntaAvaliacao> perguntaAvaliacaoExiste = perguntaAvaliacaoRepository.findByNome(perguntaAvaliacao.getNome());
																				
			if(perguntaAvaliacaoExiste.isPresent()){
				if(perguntaAvaliacaoExiste.get().getAtivo()==0){
					throw new NomeDuplicadoException(" Pergunta da Avaliação está desativado! <a style=\"color:#ffffff;\" href=\"/avaliacaopergunta/ativar/"+perguntaAvaliacaoExiste.get().getId()+"\">Deseja Ativá-la? <strong>SIM</strong></a>"	);
				}
				else{
					throw new NomeDuplicadoException("Pergunta já cadastrada");
				}
			}
		}

		perguntaAvaliacao.setAtivo(1);
		perguntaAvaliacaoRepository.save(perguntaAvaliacao);
	}

	
	@Transactional
	public PerguntaAvaliacao desativar(PerguntaAvaliacao perguntaAvaliacao) {
		perguntaAvaliacao = perguntaAvaliacaoRepository.findOne(perguntaAvaliacao.getId());
		perguntaAvaliacao.setAtivo(0);
		this.perguntaAvaliacaoRepository.saveAndFlush(perguntaAvaliacao); 
		return perguntaAvaliacao;
	}
	
	@Transactional
	public PerguntaAvaliacao ativar(PerguntaAvaliacao perguntaAvaliacao) {
		perguntaAvaliacao = perguntaAvaliacaoRepository.findOne(perguntaAvaliacao.getId());
		perguntaAvaliacao.setAtivo(1);
		this.perguntaAvaliacaoRepository.saveAndFlush(perguntaAvaliacao); 
		return perguntaAvaliacao;
	}
}
