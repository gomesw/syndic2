package com.tushtech.syndic.service;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.Email;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaCondominio;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.PessoaFisicaVeiculo;
import com.tushtech.syndic.entity.PessoaJuridicaPessoaFisica;
import com.tushtech.syndic.entity.Profissao;
import com.tushtech.syndic.entity.Senhas;
import com.tushtech.syndic.entity.Telefone;
import com.tushtech.syndic.entity.TipoPessoaFisicaCondominio;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaCondominioEnum;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaUnidadeEnum;
import com.tushtech.syndic.mail.Mailer;
import com.tushtech.syndic.repository.CondominioRepository;
import com.tushtech.syndic.repository.EmailRepository;
import com.tushtech.syndic.repository.PessoaFisicaCondominioRepository;
import com.tushtech.syndic.repository.PessoaFisicaRepository;
import com.tushtech.syndic.repository.PessoaFisicaUnidadeRepository;
import com.tushtech.syndic.repository.PessoaJuridicaPessoaFisicaRepository;
import com.tushtech.syndic.repository.ProfissaoRepository;
import com.tushtech.syndic.repository.SenhasRepository;
import com.tushtech.syndic.repository.TelefoneRepository;
import com.tushtech.syndic.repository.TipoPessoaFisicaUnidadeRepository;



/**
 * Gerencia Tipo de Pessoa
 * 		
 *
 */

@Service
public class PessoaFisicaService {

	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;

	@Autowired EmailRepository emailRepository; 

	@Autowired
	private Mailer mailer;

	@Autowired
	private SenhasRepository senhasRepository;	

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private ProfissaoRepository profissaoRepository;

	@Autowired
	private CondominioRepository condominioRepository;


	@Autowired
	private PessoaFisicaUnidadeRepository pessoaFisicaUnidadeRepository;


	@Autowired
	private PessoaFisicaCondominioRepository pessoaFisicaCondominioRepository;

	@Autowired
	private TipoPessoaFisicaUnidadeRepository tipoPessoaFisicaUnidadeRepository;

	@Autowired
	private PessoaFisicaService pessoaFisicaService;


	@Autowired
	private UnidadeService unidadeService;

	@Autowired
	private PessoaFisicaVeiculoService pessoaFisicaVeiculoService;


	@Autowired
	private VeiculoService veiculoService;

	@Autowired
	private ProfissaoService profissaoService;

	@Autowired
	private PessoaJuridicaPessoaFisicaRepository pessoaJuridicaPessoaFisicaRepository;

	@Autowired
	private TelefoneRepository telefoneRepository;



	//	--Condominio
	public void salvar(PessoaFisica pessoaFisica){

		this.pessoaFisicaRepository.save(pessoaFisica);

	}

	public PessoaFisica salvarcomretorno(PessoaFisica pessoaFisica){
		pessoaFisica.setAtivo(1); 
		return this.pessoaFisicaRepository.saveAndFlush(pessoaFisica);

	}

	public void excluir(PessoaFisica pessoaFisica){

		this.pessoaFisicaRepository.delete(pessoaFisica);

	}	









	public void atribuirErrosPessoaFisicaVeiculo(PessoaFisicaVeiculo pessoaFisicaVeiculo,BindingResult result){



		if(pessoaFisicaVeiculo.getPessoaFisicaUnidade()==null || 
				pessoaFisicaVeiculo.getPessoaFisicaUnidade().getId()==0){
			result.rejectValue("pessoaFisica","","Proprietário é Obrigatório"); 
		}

		if(pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca()==null || pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getId()==0){
			result.rejectValue("veiculo.modelo.marca","","Marca é obrigratório "); 
		}


		if(pessoaFisicaVeiculo.getVeiculo().getModelo()==null || pessoaFisicaVeiculo.getVeiculo().getModelo().getId()==0){
			result.rejectValue("veiculo.modelo","","Modelo é obrigratório "); 
		}

		if(pessoaFisicaVeiculo.getVeiculo().getCor()==null || pessoaFisicaVeiculo.getVeiculo().getCor().getId()==0){
			result.rejectValue("veiculo.cor","","Cor é obrigratório "); 
		}

		if(pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getTipoVeiculo()==null || pessoaFisicaVeiculo.getVeiculo().getModelo().getMarca().getTipoVeiculo().getId()==0){
			result.rejectValue("veiculo.modelo.marca.tipoVeiculo","","Tipo de Veiculo é obrigratório "); 
		}


	}


	public void atribuirErrosMorador(PessoaFisicaUnidade pessoaFisicaUnidade,BindingResult result,String recursos){

		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
		Validator validator = validatorFactory . getValidator () ;
		Set<ConstraintViolation<PessoaFisica>> errorspf = validator . validate (pessoaFisicaUnidade.getPessoaFisica()) ;


		for ( ConstraintViolation < PessoaFisica > error : errorspf ) {			
			result.rejectValue("pessoaFisica."+error.getPropertyPath(),"",error.getMessage());
		}


		if(pessoaFisicaUnidade.getPessoaFisica().getCpf()!=null && !pessoaFisicaUnidade.getPessoaFisica().getCpf().isEmpty()){
			if(!isCPF(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara())){
				result.rejectValue("pessoaFisica.cpf","","CPF Inválido"); 
			}
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome()!=null|| 
				!pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome().isEmpty()){

			if(emailExiste(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome(),pessoaFisicaUnidade.getPessoaFisica())) {
				result.rejectValue("pessoaFisica.emails[0].nome","","O E-mail informado já existe, e não pode ser utilizado por mais de uma pessoa tente outro e-mail"); 
			}
		}

		if(pessoaFisicaUnidade.getDataInicio()==null){
			result.rejectValue("dataInicio","","Data Início é obrigatório"); 
		}

		if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade()==null){
			result.rejectValue("tipoPessoaFisicaUnidade","","Tipo de Morador é Obrigatório"); 
		}




		if((recursos!=null && !recursos.isEmpty())){
			if((pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome()==null|| 
					pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome().isEmpty())){
				result.rejectValue("pessoaFisica.emails[0].nome","","E-mail é obrigatório , para moradores que vão acessar o sistema"); 
			}
		}




	}



	public void atribuirErrosInquilino(PessoaFisicaUnidade pessoaFisicaUnidade,BindingResult result,String recursos){

		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
		Validator validator = validatorFactory . getValidator () ;

		//		Set<ConstraintViolation<PessoaFisica>> errorspf = validator . validate (pessoaFisicaUnidade.getPessoaFisica()) ;
		//		
		//		
		//		for ( ConstraintViolation < PessoaFisica > error : errorspf ) {			
		//			result.rejectValue("pessoaFisica."+error.getPropertyPath(),"",error.getMessage());
		//		}

		if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade()==null){
			result.rejectValue("tipoPessoaFisicaUnidade","","Tipo de Morador é Obrigatório"); 
		}

		if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade()==null){
			result.rejectValue("dataInicio","","Data de Início é Obrigatório"); 
		}


		if(pessoaFisicaUnidade.getPessoaFisica().getCpf()!=null && !pessoaFisicaUnidade.getPessoaFisica().getCpf().isEmpty()){
			if(!isCPF(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara())){
				result.rejectValue("pessoaFisica.cpf","","CPF Inválido"); 
			}
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome()!=null|| 
				!pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome().isEmpty()){
			if(emailExiste(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome(),pessoaFisicaUnidade.getPessoaFisica())) {
				result.rejectValue("pessoaFisica.emails[0].nome","","O E-mail informado já existe, e não pode ser utilizado por mais de uma pesso tente outro e-mail"); 
			}
		}



		if((recursos!=null && !recursos.isEmpty())){
			if((pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome()==null|| 
					pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome().isEmpty())){
				result.rejectValue("pessoaFisica.emails[0].nome","","E-mail é obrigatório , para moradores que vão acessar o sistema"); 
			}
		}




	}


	public void atribuirErrosProprietarioParte(PessoaFisicaUnidade pessoaFisicaUnidade,BindingResult result){


		atribuirErrosProprietario(pessoaFisicaUnidade,result);

		if(pessoaFisicaUnidade.getParteImovel()==null){
			result.rejectValue("dataInicio","","Informe o Percentual da Parte"); 
		}

		if(verificarPercentualdaparte(pessoaFisicaUnidade)) {

			result.rejectValue("parteImovel","","Soma de percentual de todas as partes extrapolou limite 100%"); 
		}


	}


	private boolean verificarPercentualdaparte(PessoaFisicaUnidade pessoaFisicaUnidade) {

		Float totalPartes = Float.valueOf(pessoaFisicaUnidade.getParteImovel());

		PessoaFisicaUnidade pessoaAtual = pessoaFisicaUnidadeRepository.findById(pessoaFisicaUnidade.getId());

		totalPartes = (totalPartes+pessoaFisicaUnidade.getUnidade().getTotalPartes())-
				Float.valueOf(pessoaAtual==null || pessoaAtual.getParteImovel()==null?"0":pessoaAtual.getParteImovel());

		boolean  retorno = false;

		if(totalPartes>100F) {
			retorno = true;
		}
		else {
			retorno = false;
		}

		return retorno;



	}

	public void atribuirErrosProprietario(PessoaFisicaUnidade pessoaFisicaUnidade,BindingResult result){

		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
		Validator validator = validatorFactory . getValidator () ;
		Set<ConstraintViolation<PessoaFisica>> errors = validator . validate (pessoaFisicaUnidade.getPessoaFisica()) ;

		for ( ConstraintViolation < PessoaFisica > error : errors ) {			
			result.rejectValue("pessoaFisica."+error.getPropertyPath(),"",error.getMessage());
		}

		if(pessoaFisicaUnidade.getDataInicio()==null){
			result.rejectValue("dataInicio","","Data Início é obrigatório"); 
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getCpf()==null || pessoaFisicaUnidade.getPessoaFisica().getCpf().isEmpty()){
			result.rejectValue("pessoaFisica.cpf","","CPF é Obrigatório"); 
		}
		else if(!isCPF(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara())){
			result.rejectValue("pessoaFisica.cpf","","CPF Inválido"); 
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getSexo()==null){
			result.rejectValue("pessoaFisica.sexo","","sexo é obrigatório"); 
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome()==null|| 
				pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome().isEmpty()){
			result.rejectValue("pessoaFisica.emails[0].nome","","E-mail é obrigatório"); 
		}
		else {
			if(emailExiste(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome(),pessoaFisicaUnidade.getPessoaFisica())) {
				result.rejectValue("pessoaFisica.emails[0].nome","","O E-mail informado já existe, e não pode ser utilizado por mais de uma pesso tente outro e-mail"); 
			}
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getNomeMae()==null|| 
				pessoaFisicaUnidade.getPessoaFisica().getNomeMae().isEmpty()){
			result.rejectValue("pessoaFisica.nomeMae","","O Nome da mãe obrigatório"); 
		}



		if(pessoaFisicaUnidade.getPessoaFisica().getTelefones().get(0).getDdd()==null || 
				pessoaFisicaUnidade.getPessoaFisica().getTelefones().get(0).getDdd().isEmpty()){
			result.rejectValue("pessoaFisica.telefones[0].ddd","","DDD para o telefone fixo é obrigatório"); 
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getTelefones().get(0).getNumero()==null || 
				pessoaFisicaUnidade.getPessoaFisica().getTelefones().get(0).getNumero().isEmpty()){
			result.rejectValue("pessoaFisica.telefones[0].numero","","Número de telefone fixo é obrigatório"); 
		}


	}



	public void atribuirErrosFuncionario(PessoaFisicaUnidade pessoaFisicaUnidade,BindingResult result,Integer snveiculo){

		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
		Validator validator = validatorFactory . getValidator () ;
		Set<ConstraintViolation<PessoaFisica>> errors = validator . validate (pessoaFisicaUnidade.getPessoaFisica()) ;

		for ( ConstraintViolation < PessoaFisica > error : errors ) {			
			result.rejectValue("pessoaFisica."+error.getPropertyPath(),"",error.getMessage());
		}

		if(pessoaFisicaUnidade.getDataInicio()==null){
			result.rejectValue("dataInicio","","Data Início é obrigatório"); 
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getCpf()==null || pessoaFisicaUnidade.getPessoaFisica().getCpf().isEmpty()){
			result.rejectValue("pessoaFisica.cpf","","CPF é Obrigatório"); 
		}
		else if(!isCPF(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara())){
			result.rejectValue("pessoaFisica.cpf","","CPF Inválido"); 
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getSexo()==null){
			result.rejectValue("pessoaFisica.sexo","","Sexo é obrigatório"); 
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome()!=null&& 
				!pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome().trim().isEmpty()){
			if(emailExiste(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome(),pessoaFisicaUnidade.getPessoaFisica())) {
				result.rejectValue("pessoaFisica.emails[0].nome","","O E-mail informado já existe, e não pode ser utilizado por mais de uma pesso tente outro e-mail"); 
			}
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getTelefones().get(0).getDdd()==null || 
				pessoaFisicaUnidade.getPessoaFisica().getTelefones().get(0).getDdd().isEmpty()){
			result.rejectValue("pessoaFisica.telefones[0].ddd","","DDD para o telefone fixo é obrigatório"); 
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getTelefones().get(0).getNumero()==null || 
				pessoaFisicaUnidade.getPessoaFisica().getTelefones().get(0).getNumero().isEmpty()){
			result.rejectValue("pessoaFisica.telefones[0].numero","","Número de telefone fixo é obrigatório"); 
		}

		if(snveiculo==1){

			if(pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.modelo.marca","","Marca é obrigratório "); 
			}


			if(pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.modelo","","Modelo é obrigratório "); 
			}

			if(pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getCor()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.cor","","Cor é obrigratório "); 
			}

			if(pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca().getTipoVeiculo()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.modelo.marca.tipoVeiculo","","Tipo de Veiculo é obrigratório "); 
			}



		}



	}



	public void atribuirErrosFuncionarioEmpresa(PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica,BindingResult result){

		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
		Validator validator = validatorFactory . getValidator () ;
		Set<ConstraintViolation<PessoaFisica>> errors = validator . validate (pessoaJuridicaPessoaFisica.getPessoaFisica()) ;

		for ( ConstraintViolation < PessoaFisica > error : errors ) {			
			result.rejectValue("pessoaFisica."+error.getPropertyPath(),"",error.getMessage());
		}

		if(pessoaJuridicaPessoaFisica.getInicio()==null){
			result.rejectValue("inicio","","Data Início é obrigatório"); 
		}

		if(pessoaJuridicaPessoaFisica.getPessoaFisica().getCpf()==null || pessoaJuridicaPessoaFisica.getPessoaFisica().getCpf().isEmpty()){
			result.rejectValue("pessoaFisica.cpf","","CPF é Obrigatório"); 
		}
		else if(!isCPF(pessoaJuridicaPessoaFisica.getPessoaFisica().getCpfSemMascara())){
			result.rejectValue("pessoaFisica.cpf","","CPF Inválido"); 
		}

		if(pessoaJuridicaPessoaFisica.getPessoaFisica().getSexo()==null){
			result.rejectValue("pessoaFisica.sexo","","Sexo é obrigatório"); 
		}

//		if(pessoaJuridicaPessoaFisica.getPessoaFisica().getEmails().get(0).getNome()!=null&& 
//				!pessoaJuridicaPessoaFisica.getPessoaFisica().getEmails().get(0).getNome().trim().isEmpty()){
//			if(emailExiste(pessoaJuridicaPessoaFisica.getPessoaFisica().getEmails().get(0).getNome(),pessoaJuridicaPessoaFisica.getPessoaFisica())) {
//				result.rejectValue("pessoaFisica.emails[0].nome","","O E-mail informado já existe, e não pode ser utilizado por mais de uma pesso tente outro e-mail"); 
//			}
//		}

//		if(pessoaJuridicaPessoaFisica.getPessoaFisica().getTelefones().get(0).getDdd()==null || 
//				pessoaJuridicaPessoaFisica.getPessoaFisica().getTelefones().get(0).getDdd().isEmpty()){
//			result.rejectValue("pessoaFisica.telefones[0].ddd","","DDD para o telefone fixo é obrigatório"); 
//		}

		if(pessoaJuridicaPessoaFisica.getPessoaFisica().getTelefones().get(0).getNumero()==null || 
				pessoaJuridicaPessoaFisica.getPessoaFisica().getTelefones().get(0).getNumero().isEmpty()){
			result.rejectValue("pessoaFisica.telefones[0].numero","","Número de telefone é obrigatório"); 
		}



	}

	public void atribuirErrosVisitante(PessoaFisicaUnidade pessoaFisicaUnidade,BindingResult result,Integer snveiculo){

		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
		Validator validator = validatorFactory . getValidator () ;
		Set<ConstraintViolation<PessoaFisica>> errors = validator . validate (pessoaFisicaUnidade.getPessoaFisica()) ;

		for ( ConstraintViolation < PessoaFisica > error : errors ) {			
			result.rejectValue("pessoaFisica."+error.getPropertyPath(),"",error.getMessage());
		}

		if(pessoaFisicaUnidade.getDataInicio()==null){
			result.rejectValue("dataInicio","","Data Início é obrigatório"); 
		}

		if(pessoaFisicaUnidade.getPessoaFisica().getCpf()!=null && 
				!pessoaFisicaUnidade.getPessoaFisica().getCpf().isEmpty()){
			if(!isCPF(pessoaFisicaUnidade.getPessoaFisica().getCpfSemMascara())){
				result.rejectValue("pessoaFisica.cpf","","CPF Inválido"); 
			}
		}


		if(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome()!=null&& 
				!pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome().isEmpty()){
			if(emailExiste(pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).getNome(),pessoaFisicaUnidade.getPessoaFisica())) {
				result.rejectValue("pessoaFisica.emails[0].nome","","O E-mail informado já existe, e não pode ser utilizado por mais de uma pesso tente outro e-mail"); 
			}
		}


		if(snveiculo==1){

			if(pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.modelo.marca","","Marca é obrigratório "); 
			}


			if(pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.modelo","","Modelo é obrigratório "); 
			}

			if(pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getCor()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.cor","","Cor é obrigratório "); 
			}

			if(pessoaFisicaUnidade.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca().getTipoVeiculo()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.modelo.marca.tipoVeiculo","","Tipo de Veiculo é obrigratório "); 
			}

		}



	}


	public void atribuirErrosSindico(PessoaFisicaCondominio pessoaFisicaCondominio,BindingResult result){

		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
		Validator validator = validatorFactory . getValidator () ;
		Set<ConstraintViolation<PessoaFisica>> errors = validator . validate (pessoaFisicaCondominio.getPessoaFisica()) ;

		PessoaFisica pessoaFisica = pessoaFisicaCondominio.getPessoaFisica();

		for ( ConstraintViolation < PessoaFisica > error : errors ) {			
			result.rejectValue("pessoaFisica."+error.getPropertyPath(),"",error.getMessage());
		}

		if(pessoaFisicaCondominio.getInicio()==null ){
			result.rejectValue("inicio","","Data de Início é Obrigatória"); 
		}

		if(pessoaFisica.getCpf()==null || pessoaFisica.getCpf().isEmpty()){
			result.rejectValue("pessoaFisica.cpf","","CPF é Obrigatório"); 
		}
		else if(!isCPF(pessoaFisica.getCpfSemMascara())){
			result.rejectValue("pessoaFisica.cpf","","CPF Inválido"); 
		}

		if(pessoaFisica.getSexo()==null){
			result.rejectValue("pessoaFisica.sexo","","sexo é obrigatório"); 
		}

		if(pessoaFisica.getEmails().get(0).getNome()==null|| 
				pessoaFisica.getEmails().get(0).getNome().isEmpty()){
			result.rejectValue("pessoaFisica.emails[0].nome","","E-mail é obrigatório"); 
		}
		else {
			if(emailExiste(pessoaFisica.getEmails().get(0).getNome(),pessoaFisica)) {
				result.rejectValue("pessoaFisica.emails[0].nome","","O E-mail informado já existe, e não pode ser utilizado por mais de uma pesso tente outro e-mail"); 
			}
		}

		//		if(pessoaFisica.getNomeMae()==null|| 
		//				pessoaFisica.getNomeMae().isEmpty()){
		//			result.rejectValue("pessoaFisica.nomeMae","","O Nome da mãe obrigatório"); 
		//		}



		validar01NumeroTelefone(result, pessoaFisica);





	}

	private void validar01NumeroTelefone(BindingResult result, PessoaFisica pessoaFisica) {




		if(pessoaFisica.getTelefones().get(0).getNumero().equals("") && pessoaFisica.getTelefones().get(1).getNumero().equals("")){
			result.rejectValue("pessoaFisica.telefones[0].numero","","Informe um Numero de telefone Fixo ou Móvel"); 
		}
		

	}


	/**
	 * Verifica se o email já existe no banco de dados
	 * @param nome
	 * @return
	 */
	private boolean emailExiste(String nome,PessoaFisica pessoaFisica) {

		boolean retorno = false;

		Email email = emailRepository.findByNome(nome);

		if(email!=null) {
			if(email.getPessoaFisica().getId()!=pessoaFisica.getId()) {
				retorno = true;
			}
		}


		return retorno;
	}

	public static boolean isCPF(String CPF) {
		// considera-se erro CPF's formados por uma sequencia de numeros iguais
		if (CPF.equals("00000000000") || CPF.equals("11111111111") ||
				CPF.equals("22222222222") || CPF.equals("33333333333") ||
				CPF.equals("44444444444") || CPF.equals("55555555555") ||
				CPF.equals("66666666666") || CPF.equals("77777777777") ||
				CPF.equals("88888888888") || CPF.equals("99999999999") ||
				(CPF.length() != 11))
			return(false);

		char dig10, dig11;
		int sm, i, r, num, peso;

		// "try" - protege o codigo para eventuais erros de conversao de tipo (int)
		try {
			// Calculo do 1o. Digito Verificador
			sm = 0;
			peso = 10;
			for (i=0; i<9; i++) {              
				// converte o i-esimo caractere do CPF em um numero:
				// por exemplo, transforma o caractere '0' no inteiro 0         
				// (48 eh a posicao de '0' na tabela ASCII)         
				num = (int)(CPF.charAt(i) - 48); 
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11))
				dig10 = '0';
			else dig10 = (char)(r + 48); // converte no respectivo caractere numerico

			// Calculo do 2o. Digito Verificador
			sm = 0;
			peso = 11;
			for(i=0; i<10; i++) {
				num = (int)(CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11))
				dig11 = '0';
			else dig11 = (char)(r + 48);

			// Verifica se os digitos calculados conferem com os digitos informados.
			if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
				return(true);
			else return(false);
		} catch (InputMismatchException erro) {
			return(false);
		}
	}

	public void atribuirErrosFuncionarioCondominio(PessoaFisicaCondominio pessoafisicaCondominio, BindingResult result,
			Integer snveiculo) {

		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory () ;
		Validator validator = validatorFactory . getValidator () ;
		Set<ConstraintViolation<PessoaFisica>> errors = validator . validate (pessoafisicaCondominio.getPessoaFisica()) ;

		for ( ConstraintViolation < PessoaFisica > error : errors ) {			
			result.rejectValue("pessoaFisica."+error.getPropertyPath(),"",error.getMessage());
		}

		if(pessoafisicaCondominio.getTipoFuncionario()==null){
			result.rejectValue("tipoFuncionario","","Cargo do Funcionário é Obrigatório."); 
		}

		if(pessoafisicaCondominio.getInicio()==null){
			result.rejectValue("inicio","","Data Início é obrigatório"); 
		}

		if(pessoafisicaCondominio.getPessoaFisica().getCpf()==null || pessoafisicaCondominio.getPessoaFisica().getCpf().isEmpty()){
			result.rejectValue("pessoaFisica.cpf","","CPF é Obrigatório"); 
		}
		else if(!isCPF(pessoafisicaCondominio.getPessoaFisica().getCpfSemMascara())){
			result.rejectValue("pessoaFisica.cpf","","CPF Inválido"); 
		}

		if(pessoafisicaCondominio.getPessoaFisica().getSexo()==null){
			result.rejectValue("pessoaFisica.sexo","","Sexo é obrigatório"); 
		}

		if(pessoafisicaCondominio.getPessoaFisica().getEmails().get(0).getNome()!=null&& 
				!pessoafisicaCondominio.getPessoaFisica().getEmails().get(0).getNome().trim().isEmpty()){
			if(emailExiste(pessoafisicaCondominio.getPessoaFisica().getEmails().get(0).getNome(),pessoafisicaCondominio.getPessoaFisica())) {
				result.rejectValue("pessoaFisica.emails[0].nome","","O E-mail informado já existe, e não pode ser utilizado por mais de uma pesso tente outro e-mail"); 
			}
		}

		if(pessoafisicaCondominio.getPessoaFisica().getTelefones().get(0).getDdd()==null || 
				pessoafisicaCondominio.getPessoaFisica().getTelefones().get(0).getDdd().isEmpty()){
			result.rejectValue("pessoaFisica.telefones[0].ddd","","DDD para o telefone é obrigatório"); 
		}

		if(pessoafisicaCondominio.getPessoaFisica().getTelefones().get(0).getNumero()==null || 
				pessoafisicaCondominio.getPessoaFisica().getTelefones().get(0).getNumero().isEmpty()){
			result.rejectValue("pessoaFisica.telefones[0].numero","","Número de telefone é obrigatório"); 
		}

		if(snveiculo==1){

			if(pessoafisicaCondominio.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.modelo.marca","","Marca é obrigratório "); 
			}


			if(pessoafisicaCondominio.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.modelo","","Modelo é obrigratório "); 
			}

			if(pessoafisicaCondominio.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getCor()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.cor","","Cor é obrigratório "); 
			}

			if(pessoafisicaCondominio.getPessoaFisica().getPessoaFisicaVeiculo().get(0).getVeiculo().getModelo().getMarca().getTipoVeiculo()==null){
				result.rejectValue("pessoaFisica.pessoaFisicaVeiculo[0].veiculo.modelo.marca.tipoVeiculo","","Tipo de Veiculo é obrigratório "); 
			}

		}

	}





	@Transactional
	public PessoaFisicaCondominio incluirSindico(PessoaFisicaCondominio pessoafisicaCondominio, UsuarioSistema usuarioSistema,boolean isnovo){

		Condominio condominio = pessoafisicaCondominio.getCondominio();

		PessoaFisicaCondominio sindicosAtual = condominio.pfCondominioSindicoAtual();

		pessoafisicaCondominio.getPessoaFisica().getEmails().get(0).setAtivo(1);
		pessoafisicaCondominio.getPessoaFisica().setAtivo(0);
		//PessoaFisicaPerfilCondominio pessoaFisicaPerfilCondominio = pessoafisicaCondominio.getPessoaFisica().getPessoaFisicaPerfilCondominio().get(0);
		pessoafisicaCondominio  = setarSalvarPessoaFisicaCondominio(pessoafisicaCondominio);		

		if(isnovo) {
			pessoafisicaCondominio.getPessoaFisica().setPapeis("SÍNDICO");

			salvarPessoaFisicaCondominioTipoPessoa(TipoPessoaFisicaCondominioEnum.SINDICO.getId(),pessoafisicaCondominio);
			salvarEmail(pessoafisicaCondominio.getPessoaFisica());	
			salvarSenhasEnviarEmail(pessoafisicaCondominio.getPessoaFisica());
			//vicularPerfilCondominioPessoaFisica(pessoafisicaCondominio.getPessoaFisica(),pessoaFisicaPerfilCondominio.getPerfilCondominio());
		}

		if(sindicosAtual != null && sindicosAtual.getId()!= 0 && sindicosAtual.getId()!=pessoafisicaCondominio.getId()){
			sindicosAtual.setDataFim(pessoafisicaCondominio.getInicio());
			pessoaFisicaCondominioRepository.save(sindicosAtual);
		}

		return pessoafisicaCondominio;

	}



	private PessoaFisicaCondominio setarSalvarPessoaFisicaCondominio(PessoaFisicaCondominio pessoafisicaCondominio) {
		Condominio condominio = condominioRepository.condominioComPessoaJuridica(pessoafisicaCondominio.getCondominio().getId());
		//pessoaFisicaPerfilCondominio.getPerfilCondominio().setCondominio(condominio);
		pessoafisicaCondominio.setCondominio(condominio);
		pessoafisicaCondominio.getCondominio().getPessoaJuridica().setCnpj(pessoafisicaCondominio.getCondominio().getPessoaJuridica().getCnpj().trim().replaceAll("\\.|-|/", ""));

		return  pessoaFisicaCondominioRepository.saveAndFlush(pessoafisicaCondominio);
	}


	@Transactional
	public void incluirProprietario(PessoaFisicaUnidade pessoaFisicaUnidade,boolean isnovo){

		pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).setAtivo(1);
		pessoaFisicaUnidade.setAtivo(1);
		pessoaFisicaUnidade.getPessoaFisica().setAtivo(1);


		//caso tenha proprietarios anteriores vai tratar
		aplicarProprietariosAnteriores(pessoaFisicaUnidade);






		pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.saveAndFlush(pessoaFisicaUnidade);


		PessoaFisicaCondominio pessoafisicaCondominio =  setarTipoPessoaFisicaCondominioSalvar(pessoaFisicaUnidade);



		if(isnovo) {
			salvarPessoaFisicaCondominioTipoPessoa(TipoPessoaFisicaCondominioEnum.MORADOR.getId(), pessoafisicaCondominio);
			salvarEmail(pessoaFisicaUnidade.getPessoaFisica());	
			salvarSenhasEnviarEmail(pessoaFisicaUnidade.getPessoaFisica());
		}

	}


	/**
	 * Salvar o registro da pessoa fisica condominio equivalente ao tipo de pessoa da unidade
	 * @param pessoaFisicaUnidade
	 * @return
	 */
	private PessoaFisicaCondominio setarTipoPessoaFisicaCondominioSalvar(PessoaFisicaUnidade pessoaFisicaUnidade) {

		PessoaFisicaCondominio pessoafisicaCondominio = new PessoaFisicaCondominio();
		pessoafisicaCondominio.setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());


		TipoPessoaFisicaCondominio tipoPessoaFisicaCondominio = new TipoPessoaFisicaCondominio();

		if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()
				|| pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId()
				|| pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.MORADOR.getId()
				|| pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.INQUILINO.getId()
				|| pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.COMODATO.getId()
				) {
			tipoPessoaFisicaCondominio.setId(TipoPessoaFisicaCondominioEnum.MORADOR.getId());
		}

		pessoafisicaCondominio.setCondominio(pessoaFisicaUnidade.getUnidade().getCondominio());


		//pessoafisicaCondominio = pessoaFisicaCondominioRepository.saveAndFlush(pessoafisicaCondominio);


		return pessoafisicaCondominio;

	}






	/**
	 * SE TIVER PROPRIETARIO DE PARTES VAI SETAR DATA FIM IGUAL A DATA INICIO DO NOVO PROPRIETÁRIO
	 * SE ESTIVER MUDANDO DE PROPRIETÁRIO SETAR A DATA FIM DO PROPRIETARIO ANTERIOR
	 * SE ESTIVER MUDANDO DE PROPRIETÁRIO SETAR A DATA FIM DO PROPRIETARIODEPARTE ANTERIOR E INSERIR UM NOVO REGISTRO
	 * @param pessoaFisicaUnidade
	 */
	private void aplicarProprietariosAnteriores(PessoaFisicaUnidade pessoaFisicaUnidade) {

		PessoaFisicaUnidade pessoaFisicaUnidadeBanco = pessoaFisicaUnidadeRepository.findById(pessoaFisicaUnidade.getId());


		int controle = 0;


		if(pessoaFisicaUnidadeBanco!=null){

			for (PessoaFisicaUnidade pfu : pessoaFisicaUnidadeBanco.getUnidade().getListaProprietarioUnidade()) {

				if(pessoaFisicaUnidade.isNovo()) {
					pfu.setDataFim(pessoaFisicaUnidade.getDataInicio());
				}

				else if(pfu.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId()) {
					if(pfu.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()) {
						controle++;
					}
					pfu.setDataFim(pessoaFisicaUnidade.getDataInicio());
					pessoaFisicaUnidadeRepository.save(pfu);
				}


				if(controle==0) {
					pessoaFisicaUnidade.setId(0);
				}
			}
		}



	}

	@Transactional
	public void incluirVisitante(PessoaFisicaUnidade pessoaFisicaUnidade){



		Condominio condominio = condominioRepository.condominioComPessoaJuridica(pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getId());
		pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().setCondominio(condominio);


		pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getPessoaJuridica().setCnpj(pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getPessoaJuridica().getCnpj().trim().replaceAll("\\.|-|/", ""));
		pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.saveAndFlush(pessoaFisicaUnidade);


	}


	@Transactional
	public void incluirFuncionario(PessoaFisicaUnidade pessoaFisicaUnidade){



		Condominio condominio = condominioRepository.condominioComPessoaJuridica(pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getId());
		pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().setCondominio(condominio);


		pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getPessoaJuridica().setCnpj(pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getPessoaJuridica().getCnpj().trim().replaceAll("\\.|-|/", ""));
		pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.saveAndFlush(pessoaFisicaUnidade);


	}


	@Transactional
	public PessoaJuridicaPessoaFisica incluirFuncionarioEmpresa(PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica,boolean isnovo){


		String profissoes[] =  pessoaJuridicaPessoaFisica.getProfissoesIds().split(",");

		Integer profissoesids[] = new Integer[profissoes.length];

		List<Profissao> listaprofissoes = new ArrayList<>();
		if(profissoes.length>1) {
			for (int i = 0 ;i<profissoes.length;i++) {
				profissoesids[i] = Integer.valueOf(profissoes[i]);
			}
		}


		listaprofissoes  = (profissoes.length>1?profissaoRepository.findByIdIn(profissoesids):null);

		if(isnovo) {

			List<Telefone> telefones = pessoaJuridicaPessoaFisica.getPessoaFisica().getTelefones();
			List<Email> emails = pessoaJuridicaPessoaFisica.getPessoaFisica().getEmails();

			
			pessoaJuridicaPessoaFisica = pessoaJuridicaPessoaFisicaRepository.saveAndFlush(pessoaJuridicaPessoaFisica);


			for (Email email : emails) {
				email.setPessoaFisica(pessoaJuridicaPessoaFisica.getPessoaFisica());				}

			for (Telefone tel : telefones) {
				tel.setPessoaFisica(pessoaJuridicaPessoaFisica.getPessoaFisica());
			}


			pessoaJuridicaPessoaFisica.getPessoaFisica().setTelefones(telefones);

			if(pessoaJuridicaPessoaFisica.getPessoaFisica().getEmails().get(0).getNome() != ""){
				salvarEmail(pessoaJuridicaPessoaFisica.getPessoaFisica());
			}
			
			salvarTelefone(pessoaJuridicaPessoaFisica.getPessoaFisica());	

			//anexar profissões ao funcionário
			profissaoService.anexarProfissoesFuncionario(pessoaJuridicaPessoaFisica.getPessoaFisica(),listaprofissoes);

			//setar a função para enviar email
			pessoaJuridicaPessoaFisica.getPessoaFisica().setPapeis(pessoaJuridicaPessoaFisica.getFuncaoPessoaJuridica().getNome().toUpperCase());

			salvarSenhasEnviarEmail(pessoaJuridicaPessoaFisica.getPessoaFisica());

			//vicularPerfilCondominioPessoaFisica(pessoafisicaCondominio.getPessoaFisica(),pessoaFisicaPerfilCondominio.getPerfilCondominio());
		}
		else {
			salvarEmail(pessoaJuridicaPessoaFisica.getPessoaFisica());
			salvarTelefone(pessoaJuridicaPessoaFisica.getPessoaFisica());
			//anexar profissões ao funcionário
			profissaoService.anexarProfissoesFuncionario(pessoaJuridicaPessoaFisica.getPessoaFisica(),listaprofissoes);
			pessoaJuridicaPessoaFisica =  pessoaJuridicaPessoaFisicaRepository.saveAndFlush(pessoaJuridicaPessoaFisica);
		}


		return pessoaJuridicaPessoaFisica;


	}




	@Transactional
	public PessoaFisicaUnidade incluirMorador(PessoaFisicaUnidade pessoaFisicaUnidade,String recursos){


		Condominio condominio = condominioRepository.condominioComPessoaJuridica(pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getId());
		pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().setCondominio(condominio);


		pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getPessoaJuridica().setCnpj(pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getPessoaJuridica().getCnpj().trim().replaceAll("\\.|-|/", ""));
		pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.saveAndFlush(pessoaFisicaUnidade);


		if(!recursos.isEmpty()) {

			salvarSenhasEnviarEmail(pessoaFisicaUnidade.getPessoaFisica());
		}
		else {

			//DELETA A SENHA PARA A PESSOA NÃO TER MAIS ACESSO AO SISTEMA
			if(pessoaFisicaUnidade.getPessoaFisica().getSenhas()!=null && !pessoaFisicaUnidade.getPessoaFisica().getSenhas().isEmpty()) {
				senhasRepository.delete(pessoaFisicaUnidade.getPessoaFisica().getSenhas().get(0));
			}
		}

		return pessoaFisicaUnidade;


	}



	@Transactional
	public PessoaFisicaUnidade incluirInquilino(PessoaFisicaUnidade pessoaFisicaUnidade,String recursos,Email email){

		pessoaFisicaUnidade.getPessoaFisica().getEmails().get(0).setAtivo(1);


		Condominio condominio = condominioRepository.condominioComPessoaJuridica(pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getId());
		pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().setCondominio(condominio);


		pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getPessoaJuridica().setCnpj(pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getPessoaJuridica().getCnpj().trim().replaceAll("\\.|-|/", ""));
		pessoaFisicaUnidade = pessoaFisicaUnidadeRepository.saveAndFlush(pessoaFisicaUnidade);


		if(!recursos.isEmpty()) {

			emailRepository.saveAndFlush(email);
			salvarSenhasEnviarEmail(pessoaFisicaUnidade.getPessoaFisica());
		}
		else {

			//DELETA A SENHA PARA A PESSOA NÃO TER MAIS ACESSO AO SISTEMA
			if(pessoaFisicaUnidade.getPessoaFisica().getSenhas()!=null && !pessoaFisicaUnidade.getPessoaFisica().getSenhas().isEmpty()) {
				senhasRepository.delete(pessoaFisicaUnidade.getPessoaFisica().getSenhas().get(0));
			}
		}

		return pessoaFisicaUnidade;


	}



	/**
	 * SALVA O TIPO DE PESSOA DENTRO DO CONDOMINIO
	 * @param tipo
	 * @param pessoaFisicaCondominio
	 */
	public void salvarPessoaFisicaCondominioTipoPessoa(int tipo,PessoaFisicaCondominio pessoaFisicaCondominio) {
		TipoPessoaFisicaCondominio tipoPessoaFisicaCondominio = new TipoPessoaFisicaCondominio();
		//pessoaFisicaCondominio.setInicio(Date.valueOf(LocalDate.now()));
		tipoPessoaFisicaCondominio.setId(tipo);
		pessoaFisicaCondominio.setTipoPessoaFisicaCondominio(tipoPessoaFisicaCondominio);
		pessoaFisicaCondominioRepository.save(pessoaFisicaCondominio);

	}






	//	private void salvarSenhasEnviarEmail(PessoaFisica pessoaFisica) {
	//
	//		if((pessoaFisica.getSenhas()==null ||
	//				pessoaFisica.getSenhas().isEmpty())) {//se for sindico
	//			String senhaAleatoria  = getSenhaAleatoria(8);
	//			Senhas senha  = new Senhas();
	//			senha.setSenha(passwordEncoder.encode(senhaAleatoria));
	//			senha.setPessoaFisica(pessoaFisica);
	//			senhasRepository.save(senha);
	//			enviarEmailComSenha(pessoaFisica,senhaAleatoria);		
	//		}
	//	}

	public void salvarSenhasEnviarEmail(PessoaFisica pessoaFisica) {
		String senhaAleatoria  = getSenhaAleatoria(8);
		Senhas senha  = new Senhas();
		senha.setSenha(this.passwordEncoder.encode(senhaAleatoria));
		senha.setPessoaFisica(pessoaFisica);
		this.senhasRepository.saveAndFlush(senha);

		enviarEmailComSenha(pessoaFisica,senhaAleatoria);		
	}




	//
	//	private void vicularPerfilCondominioPessoaFisica(PessoaFisica pessoaFisica,
	//			PerfilCondominio perfilCondominio) {
	//
	//		PessoaFisicaPerfilCondominio pessoaFisicaPerfilCondominio = new PessoaFisicaPerfilCondominio();
	//		pessoaFisicaPerfilCondominio.setPerfilCondominio(perfilCondominio);
	//		pessoaFisicaPerfilCondominio.setPessoaFisica(pessoaFisica);
	//
	//		pessoaFisicaPerfilCondominioRepository.save(pessoaFisicaPerfilCondominio);
	//
	//
	//	}





	/**
	 * ATENÇÃO SETAR A SENHA SEM CRIPTOGRAFIA PARA ENVIAR PARA O USUARIO
	 * @param pessoa
	 * @param condominio
	 */
	public void enviarEmailComSenha(PessoaFisica pessoa,String senha) {

		mailer.enviar(pessoa,senha,pessoa.getEmails().get(0));


	}






	private void salvarEmail(PessoaFisica pessoaFisica) {

		System.err.println(pessoaFisica.getNome());
		if((pessoaFisica.getEmails()==null ||
				pessoaFisica.getEmails().isEmpty())) {
			gravarEmail(pessoaFisica);
		}
		else {

			emailRepository.save(pessoaFisica.getEmails().get(0));
		}
	}


	private void salvarTelefone(PessoaFisica pessoaFisica) {

		if((pessoaFisica.getTelefones()==null ||
				pessoaFisica.getTelefones().isEmpty())) {
			gravarTelefone(pessoaFisica);
		}
		else {
			pessoaFisica.getTelefones().get(0).setPessoaFisica(pessoaFisica);
			telefoneRepository.save(pessoaFisica.getTelefones().get(0));
		}
	}



	private void gravarEmail(PessoaFisica pessoaFisica) {

		System.err.println(pessoaFisica.getEmails().get(0).getNome());
		Email email = new Email();
		email.setId(0);
		email.setNome(pessoaFisica.getEmails().get(0).getNome());
		email.setPessoaFisica(pessoaFisica);
		email.setAtivo(1);
		emailRepository.saveAndFlush(email);
	}


	private void gravarTelefone(PessoaFisica pessoaFisica) {

		System.err.println(pessoaFisica.getTelefones().get(0).getNumero());

		Telefone telefone = new Telefone();
		telefone.setId(0);
		telefone.setDdd(pessoaFisica.getTelefones().get(0).getDdd());
		telefone.setPessoaFisica(pessoaFisica);
		telefone.setAtivo(1);
		telefone.setNumero(pessoaFisica.getTelefones().get(0).getNumero());
		telefoneRepository.saveAndFlush(telefone);
	}


	public String getSenhaAleatoriaTudo(int tamanho) {
		char[] chart ={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		char[] senha= new char[tamanho];
		int chartLenght = chart.length;
		Random rdm = new Random();

		for (int i=0; i<tamanho; i++) {
			senha[i] = chart[rdm.nextInt(chartLenght)];
		}

		return new String(senha);

	}


	public String getSenhaAleatoria(int tamanho) {
		char[] chart ={'0','1','2','3','4','5','6','7','8','9'};
		char[] senha= new char[tamanho];
		int chartLenght = chart.length;
		Random rdm = new Random();

		for (int i=0; i<tamanho; i++) {
			senha[i] = chart[rdm.nextInt(chartLenght)];
		}

		return new String(senha);

	}



	public void alterarSenhasEnviarEmail(PessoaFisica pessoaFisica) {


		List<Senhas> senhas =  senhasRepository.findByPessoaFisicaId(pessoaFisica.getId());

		//condominio = condominioRepository.condominioComPessoaJuridica(condominio.getId());

		if((senhas==null ||
				senhas.isEmpty())) {//se for sindico
			String senhaAleatoria  = getSenhaAleatoria(6);

			Senhas senha  = new Senhas();
			senha.setSenha(passwordEncoder.encode(senhaAleatoria));
			senha.setPessoaFisica(pessoaFisica);
			senhasRepository.save(senha);
			enviarEmailComSenha(pessoaFisica,senhaAleatoria);		
		}
		else {


			String senhaAleatoria  = getSenhaAleatoria(6);
			senhas.get(0).setSenha(passwordEncoder.encode(senhaAleatoria));
			senhas.get(0).setPessoaFisica(pessoaFisica);
			senhasRepository.save(senhas.get(0));
			enviarEmailComSenha(pessoaFisica,senhaAleatoria);

		}
	}


	public void incluirProprietarioEdicaoEnviandoSenha(PessoaFisicaUnidade pessoaFisicaUnidade,
			UsuarioSistema usuario) {

		pessoaFisicaUnidade.setPessoaFisicaCadastrante(usuario.getPessoaFisica());
		pessoaFisicaUnidadeRepository.save(pessoaFisicaUnidade);

		Condominio condominio = condominioRepository.condominioComPessoaJuridica(pessoaFisicaUnidade.getUnidade().getEnderecoUnidade().getCondominio().getId());

		alterarSenhasEnviarEmail(pessoaFisicaUnidade.getPessoaFisica());

	}

	public void incluirFuncionarioEdicaoEnviandoSenha(PessoaJuridicaPessoaFisica pessoaJuridicaPessoaFisica,
			UsuarioSistema usuario) {

		pessoaJuridicaPessoaFisicaRepository.save(pessoaJuridicaPessoaFisica);


		alterarSenhasEnviarEmail(pessoaJuridicaPessoaFisica.getPessoaFisica());

	}



	public void mensagemRetornoPorTipo(PessoaFisicaUnidade pessoaFisicaUnidade, RedirectAttributes attributes,
			boolean isnovo) {
		if(isnovo) {
			if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()) {
				attributes.addFlashAttribute("mensagem","Proprietário cadastrado com sucesso!");
			}
			else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.MORADOR.getId()) {
				attributes.addFlashAttribute("mensagem","Morador cadastrado com sucesso!");
			}
			else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()) {
				attributes.addFlashAttribute("mensagem","Funcionario cadastrado com sucesso!");
			}

			else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId()) {
				attributes.addFlashAttribute("mensagem","Proprietário de Parte cadastrado com sucesso!");
			}

		}
		else {
			if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()) {
				attributes.addFlashAttribute("mensagem","Proprietário editado com sucesso!");
			}
			else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.MORADOR.getId()) {
				attributes.addFlashAttribute("mensagem","Morador editado com sucesso!");
			}
			else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()) {
				attributes.addFlashAttribute("mensagem","Funcionario editado com sucesso!");
			}
			else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId()) {
				attributes.addFlashAttribute("mensagem","Proprietário de Parte editado com sucesso!");
			}
		}
	}



	public PessoaFisicaUnidade inclusaoPorTipo(PessoaFisicaUnidade pessoaFisicaUnidade, UsuarioSistema usuarioSistema,
			String recursos, Integer snveiculo, boolean isnovo, PessoaFisica pf,
			PessoaFisicaVeiculo pessoaFisicaVeiculo) {

		if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()) {
			pessoaFisicaService.incluirProprietario(pessoaFisicaUnidade,isnovo); 
		}
		else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId()) {
			pessoaFisicaService.incluirProprietario(pessoaFisicaUnidade,isnovo); 
		}
		else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.MORADOR.getId()) {
			pessoaFisicaUnidade  = pessoaFisicaService.incluirMorador(pessoaFisicaUnidade,recursos);
			unidadeService.inserirRecursos(pessoaFisicaUnidade, recursos);
		}
		else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()) {

			if(snveiculo==1){

				if(pessoaFisicaVeiculo.isNovo()){
					pessoaFisicaVeiculo.getVeiculo().setCadastrante(usuarioSistema.getPessoaFisica());
					pessoaFisicaVeiculo.setPessoaFisica(pf);
					//pessoaFisicaVeiculo.setDataInicio(Date.valueOf(LocalDate.now()));
					pessoaFisicaVeiculo.setTipoPessoaFisicaUnidade(tipoPessoaFisicaUnidadeRepository.findById(TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()));
				}

				unidadeService.inserirVeiculo(pessoaFisicaVeiculo);
			}
			else if(snveiculo==0 && !pessoaFisicaVeiculo.isNovo()){

				pessoaFisicaVeiculoService.excluir(pessoaFisicaVeiculo);
				veiculoService.excluirVeiculo(pessoaFisicaVeiculo.getVeiculo());

			}

			pessoaFisicaService.incluirFuncionario(pessoaFisicaUnidade);


		}
		return pessoaFisicaUnidade;
	}


	public  PessoaFisicaCondominio  setarPessoaFisicaCondominio(PessoaFisicaUnidade pessoaFisicaUnidade) {

		TipoPessoaFisicaCondominio tipoPessoaFisicaCondominio = new TipoPessoaFisicaCondominio();

		if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()
				|| pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId()
				||pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.MORADOR.getId()
				) {

			tipoPessoaFisicaCondominio.setId(TipoPessoaFisicaCondominioEnum.MORADOR.getId());

		}

		else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()) {
			tipoPessoaFisicaCondominio.setId(TipoPessoaFisicaCondominioEnum.FUNCIONARIO_UNIDADE.getId());
		}

		PessoaFisicaCondominio pessoaFisicaCondominio = new PessoaFisicaCondominio();

		pessoaFisicaCondominio.setTipoPessoaFisicaCondominio(tipoPessoaFisicaCondominio);
		pessoaFisicaCondominio.setPessoaFisica(pessoaFisicaUnidade.getPessoaFisica());
		pessoaFisicaCondominio.setCondominio(pessoaFisicaUnidade.getUnidade().getCondominio());
		pessoaFisicaCondominio.setInicio(pessoaFisicaUnidade.getDataInicio());

		return pessoaFisicaCondominio;

	}



}
