package com.tushtech.syndic.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.enumeration.TipoPessoaFisicaUnidadeEnum;
import com.tushtech.syndic.repository.PessoaFisicaUnidadeRepository;

@Service
public class PessoaFisicaUnidadeService {

	@Autowired
	private PessoaFisicaUnidadeRepository PessoaFisicaUnidadeRepository;
	
	@Autowired
	private PessoaFisicaService pessoaFisicaService;
	
	public PessoaFisicaUnidade salvarPessoaFisicaUnidade(PessoaFisicaUnidade pessoaFisicaUnidade){
		return this.PessoaFisicaUnidadeRepository.saveAndFlush(pessoaFisicaUnidade);
	}
	
	
	public void excluirPessoaFisicaUnidade(PessoaFisicaUnidade pessoaFisicaUnidade){
		this.PessoaFisicaUnidadeRepository.delete(pessoaFisicaUnidade);
	}

	
	public void desativar(PessoaFisicaUnidade pessoaFisicaUnidade){
		Date data = new Date(System.currentTimeMillis());
		pessoaFisicaUnidade.setDataFim(data);
		this.PessoaFisicaUnidadeRepository.save(pessoaFisicaUnidade);
	}	
	
	
	public void atribuirErrosPorTipo(PessoaFisicaUnidade pessoaFisicaUnidade, BindingResult result, String recursos,
			Integer snveiculo) {
		
		
		
		
		if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIO.getId()) {
			
			
			pessoaFisicaService.atribuirErrosProprietario(pessoaFisicaUnidade, result);
		}
		
		else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.PROPRIETARIODEPARTE.getId()) {
			
			pessoaFisicaService.atribuirErrosProprietarioParte(pessoaFisicaUnidade, result);
		}
		
		else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.MORADOR.getId()) {
			
			pessoaFisicaService.atribuirErrosMorador(pessoaFisicaUnidade, result,recursos);
			
		}
		
		else if(pessoaFisicaUnidade.getTipoPessoaFisicaUnidade().getId()==TipoPessoaFisicaUnidadeEnum.FUNCIONARIO.getId()) {
			pessoaFisicaService.atribuirErrosFuncionario(pessoaFisicaUnidade, result,snveiculo);
		}
		
		
		
	}
	
}
