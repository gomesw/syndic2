package com.tushtech.syndic.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.PessoaFisicaVeiculo;
import com.tushtech.syndic.repository.PessoaFisicaVeiculoRepository;

@Service
public class PessoaFisicaVeiculoService {

	@Autowired
	private PessoaFisicaVeiculoRepository pessoaFisicaVeiculoRepository;
	
	public void salvar(PessoaFisicaVeiculo pessoaFisicaVeiculo){
		
		pessoaFisicaVeiculo.setDataInicio(Timestamp.valueOf(LocalDateTime.now()));
		this.pessoaFisicaVeiculoRepository.save(pessoaFisicaVeiculo);
	}

	public void excluir(PessoaFisicaVeiculo pessoaFisicaVeiculo) {
		// TODO Auto-generated method stub
		this.pessoaFisicaVeiculoRepository.delete(pessoaFisicaVeiculo);
	}
	
		
}
