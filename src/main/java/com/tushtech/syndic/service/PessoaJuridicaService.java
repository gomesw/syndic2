package com.tushtech.syndic.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.PessoaJuridica;
import com.tushtech.syndic.repository.PessoaJuridicaRepository;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;

@Service
public class PessoaJuridicaService {

	@Autowired
	private PessoaJuridicaRepository pessoaJuridicaRepository;

	@Transactional
	public void salvar(PessoaJuridica pessoaJuridica){   
		
		if(pessoaJuridica.isNovo()){		
			
			//VERIFICA SE MARCA DE VEICULO JÁ FOI CADASTRADA
			Optional<PessoaJuridica> pjExiste = pessoaJuridicaRepository.findByCnpj(pessoaJuridica.getCnpj().replaceAll("\\D", ""));
																				
			if(pjExiste.isPresent()){
				if(pjExiste.get().getAtivo()==0){
					throw new NomeDuplicadoException("Pessoa Jurídica está desativada! <a style=\"color:#ffffff;\" href=\"/pessoajuridica/ativar/"+pjExiste.get().getId()+"\">Deseja Ativá-la?<strong>SIM</strong></a>"	);
				}
				else{
					throw new NomeDuplicadoException("Pessoa Jurídica já cadastrada");
				}
			}
		}

		pessoaJuridica.setAtivo(1);
		pessoaJuridicaRepository.save(pessoaJuridica);
	}

	
	@Transactional
	public PessoaJuridica desativar(PessoaJuridica pessoaJuridica) {
		pessoaJuridica = pessoaJuridicaRepository.getOne(pessoaJuridica.getId());
		pessoaJuridica.setAtivo(0);
		this.pessoaJuridicaRepository.saveAndFlush(pessoaJuridica); 
		return pessoaJuridica;
	}
	
	@Transactional
	public PessoaJuridica ativar(PessoaJuridica pessoaJuridica) {
		pessoaJuridica = pessoaJuridicaRepository.getOne(pessoaJuridica.getId());
		pessoaJuridica.setAtivo(1);
		this.pessoaJuridicaRepository.saveAndFlush(pessoaJuridica); 
		return pessoaJuridica;
	}
}
