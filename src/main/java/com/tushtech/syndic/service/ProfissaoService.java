package com.tushtech.syndic.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaProfissao;
import com.tushtech.syndic.entity.Profissao;
import com.tushtech.syndic.repository.PessoaFisicaProfissaoRepository;
import com.tushtech.syndic.repository.ProfissaoRepository;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;

@Service
public class ProfissaoService {

	@Autowired
	private ProfissaoRepository profissaoRepository;
	
	@Autowired
	private PessoaFisicaProfissaoRepository pessoaFisicaProfissaoRepository;

	@Transactional
	public void salvar(Profissao profissao){   

		if(profissao.isNovo()){
			//VERIFICA SE MARCA DE VEICULO JÁ FOI CADASTRADA
			Optional<Profissao> profissaoExiste = profissaoRepository.findByNome(profissao.getNome());
																				
			if(profissaoExiste.isPresent()){
				if(profissaoExiste.get().getAtivo()==0){
					
					throw new NomeDuplicadoException("Profissão está desativada! <a href=\"/profissao/ativar/"+profissaoExiste.get().getId()+"\">Deseja Ativá-la? <strong>SIM</strong></a>"	);
				}else if (profissaoExiste.get().getAtivo() == 1) {
					throw new NomeDuplicadoException("Profissão já cadastrada, informe outra profissão!");
				}
			}
		}

		profissao.setAtivo(1);
		profissaoRepository.save(profissao);

	}

	public Profissao salvarRetornar(Profissao profissao){
		return this.profissaoRepository.saveAndFlush(profissao);
	}

	public void excluir(Profissao profissao){
		this.profissaoRepository.delete(profissao);
	}
	
	@Transactional
	public Profissao ativar(Profissao profissao) {
		profissao = profissaoRepository.findOne(profissao.getId());
		profissao.setAtivo(1);
		this.profissaoRepository.saveAndFlush(profissao); 
		return profissao;
	}
	
	@Transactional
	public Profissao desativar(Profissao profissao) {
		profissao = profissaoRepository.findOne(profissao.getId());
		profissao.setAtivo(0);
		this.profissaoRepository.saveAndFlush(profissao); 
		return profissao;
	}

	public void anexarProfissoesFuncionario(PessoaFisica pf, List<Profissao> listaprofissoes) {
		
		if(pf.getPessoaFisicaProfissoes()!=null && !pf.getPessoaFisicaProfissoes().isEmpty()) {
			pessoaFisicaProfissaoRepository.delete(pf.getPessoaFisicaProfissoes());
		}
		
		if(listaprofissoes!=null) {
		
		for (Profissao profissao : listaprofissoes) {
			
			PessoaFisicaProfissao pessoaFisicaProfissao = new PessoaFisicaProfissao();
			pessoaFisicaProfissao.setAtivo(1);
			pessoaFisicaProfissao.setPessoaFisica(pf);
			pessoaFisicaProfissao.setProfissao(profissao);
			pessoaFisicaProfissao.setDataCadastro(Date.valueOf(LocalDate.now()));
			pessoaFisicaProfissaoRepository.save(pessoaFisicaProfissao);
		}
		}
		
	}	
}
