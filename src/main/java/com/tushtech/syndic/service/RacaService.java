package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Raca;
import com.tushtech.syndic.repository.RacaRepository;

@Service
public class RacaService {

	@Autowired
	private RacaRepository RacaRepository;
	
	public void salvarRaca(Raca raca){
		
		raca.setAtivo(1);
		this.RacaRepository.saveAndFlush(raca);
	}
	
	public void excluirRaca(Raca raca){
		this.RacaRepository.delete(raca);
	}

	
}
