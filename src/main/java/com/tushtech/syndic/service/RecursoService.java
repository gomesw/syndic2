package com.tushtech.syndic.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Perfil;
import com.tushtech.syndic.entity.PerfilRecurso;
import com.tushtech.syndic.entity.Recurso;
import com.tushtech.syndic.repository.PerfilRecursoRepository;
import com.tushtech.syndic.repository.RecursoRepository;

/**
 * @author CB Wallace 
 * Gerencia 
 * 		-Recursos
 * 		-Perfis
 *
 */
@Service
public class RecursoService {

	@Autowired
	private RecursoRepository recursoRepository; 

	
	@Autowired
	private PerfilRecursoRepository perfilRecursoRepository;

	
	public void salvarRecurso(Recurso recurso){
		recurso.setAtivo(1);
		this.recursoRepository.save(recurso);

	}
	
	
	public void editarRecurso(Recurso recurso){
		recurso.setAtivo(1);
		this.recursoRepository.saveAndFlush(recurso);

	}
	
	public Recurso desativarRecurso(Recurso recurso){
		recurso.setAtivo(0);
		this.recursoRepository.save(recurso);
		return recurso;
	}
	
	public Recurso ativarRecurso(Recurso recurso){
		recurso.setAtivo(1);
		this.recursoRepository.save(recurso);
		return recurso;
	}
	
	
	
	public void atualizarPerfilRecurso(String[] nomes, Integer id) {
		Perfil perfil = null;//perfilRepository.buscarComRecursos(id);
		
		List<PerfilRecurso> listPerfilRecursoAntes = perfil.getListperfilrecursos();
				
		List<Recurso> listRecursoDepois = recursoRepository.findByNomeIn(nomes);
		
		List<PerfilRecurso> listPerfilRecursoDepois = new ArrayList<>();
		
		for (Recurso recurso : listRecursoDepois) {
			PerfilRecurso perfilRecurso = new PerfilRecurso();
			perfilRecurso.setPerfil(perfil);
			perfilRecurso.setRecurso(recurso);
			listPerfilRecursoDepois.add(perfilRecurso);
		}
		
		//SE FOR EXCLUSÃO
		if(listPerfilRecursoAntes.size() > listRecursoDepois.size()){
			System.err.println(listPerfilRecursoAntes.size() +" teste teste teste 3333 "+listRecursoDepois.size());
			for (PerfilRecurso funrecursoantes : listPerfilRecursoAntes) {
				boolean isexiste = false;
				
				for (Recurso funrecursodepois : listRecursoDepois) {
					if(funrecursoantes.getRecurso().getId()==funrecursodepois.getId()){
						isexiste = true;
						break;
					}
				}
				
				if(!isexiste){
					perfilRecursoRepository.delete(funrecursoantes);
				} 
				
			}
			
		}
		//SE FOR INCLUSÃO
		else{
			System.err.println("teste teste teste");
			for (PerfilRecurso funrecursodepois : listPerfilRecursoDepois) {
			
				boolean isexiste = false;
				
				for (PerfilRecurso funrecursoantes : listPerfilRecursoAntes) {
					if(funrecursodepois.getRecurso().getId()==funrecursoantes.getRecurso().getId()){
						isexiste = true;
						break;
					}
				}
				
				if(!isexiste){
					perfilRecursoRepository.save(funrecursodepois);
				}
				
			}
			
			
		}
		
	}

}
