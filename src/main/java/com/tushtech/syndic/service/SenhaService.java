package com.tushtech.syndic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.tushtech.syndic.dto.AlterarSenhaDTO;
import com.tushtech.syndic.entity.Condominio;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.Senhas;
import com.tushtech.syndic.entity.UsuarioSistema;
import com.tushtech.syndic.mail.Mailer;
import com.tushtech.syndic.repository.SenhasRepository;

@Service
public class SenhaService {

	@Autowired
	private SenhasRepository senhasRepository;
	
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private Mailer mailer;
	
	
	public void salvar(Senhas senha){
		this.senhasRepository.save(senha);
	}

	public void atribuirErros(AlterarSenhaDTO alterarSenhaDTO,
			UsuarioSistema usuarioSistema,BindingResult result) {
		
		
		List<Senhas> senhaAntiga = senhasRepository.findByPessoaFisicaId(usuarioSistema.getPessoaFisica().getId());
		 
		
		if(alterarSenhaDTO.getPasswordOld()==null || alterarSenhaDTO.getPasswordOld().isEmpty()) {
			result.rejectValue("passwordOld","","Senha antiga é obrigatório");
		}
		if(alterarSenhaDTO.getRegisterPassword()==null|| alterarSenhaDTO.getRegisterPassword().isEmpty()) {
			result.rejectValue("registerPassword","","Nova é obrigatório");
		}
		if(alterarSenhaDTO.getRegisterPasswordConfirmation()==null|| alterarSenhaDTO.getRegisterPasswordConfirmation().isEmpty()) {
			result.rejectValue("registerPasswordConfirmation","","Confirme a Nova Senha é obrigatório");
		}
		
		if(!passwordEncoder.matches(alterarSenhaDTO.getPasswordOld(),senhaAntiga.get(0).getSenha())) {
			result.rejectValue("passwordOld","","Senha não confere");
		}
		
		
		
	}
	
	public void enviarEmailComSenha(PessoaFisica pessoa,Condominio condominio,String senha) {

		mailer.enviar(pessoa,senha,pessoa.getEmails().get(0));


	}
	
	public void enviarEmailComAlteracaoSenha(PessoaFisica pessoa,String senha) {
		
		
		mailer.enviar(pessoa,senha,pessoa.getEmails().get(0));


	}
	
	
	
}
