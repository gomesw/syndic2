package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.TipoAnimal;
import com.tushtech.syndic.repository.TipoAnimalRepository;

@Service
public class TipoAnimalService {

	@Autowired
	private TipoAnimalRepository TipoAnimalRepository;
	
	public void salvarTipoAnimal(TipoAnimal tipoAnimal){
		this.TipoAnimalRepository.saveAndFlush(tipoAnimal);
	}
	
	public void excluirTipoAnimal(TipoAnimal tipoAnimal){
		this.TipoAnimalRepository.delete(tipoAnimal);
	}

	
}
