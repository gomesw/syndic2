package com.tushtech.syndic.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.TipoAtendimento;
import com.tushtech.syndic.repository.TipoAtendimentoRepository;


@Service
public class TipoAtendimentoService {


	@Autowired
	private TipoAtendimentoRepository tipoAtendimentoRepository; 

	public void salvar(TipoAtendimento tipoAtendimento){
		
		tipoAtendimento.setAtivo(1);
		this.tipoAtendimentoRepository.saveAndFlush(tipoAtendimento);

	}

	public void editar(TipoAtendimento tipoAtendimento){
		tipoAtendimento.setAtivo(1);
		this.tipoAtendimentoRepository.saveAndFlush(tipoAtendimento);

	}

	public TipoAtendimento desativar(TipoAtendimento tipoAtendimento){
		tipoAtendimento.setAtivo(0);
		this.tipoAtendimentoRepository.save(tipoAtendimento);
		return tipoAtendimento;
	}

	public List<TipoAtendimento> paisComFilhosOrdenada(
			) {
		
		List<TipoAtendimento> tiposPais = tipoAtendimentoRepository.findByAtivoAndIdSubordinacaoIsNull(1);
		List<TipoAtendimento> tiposFilhos = tipoAtendimentoRepository.comIdSubordinacao();
		List<TipoAtendimento> listRetorno = new ArrayList<TipoAtendimento>();
		
		for (TipoAtendimento pais : tiposPais) {
			listRetorno.add(pais);
			for (TipoAtendimento filhos : tiposFilhos) {
				if(filhos.getIdSubordinacao()!=null &&(filhos.getIdSubordinacao().getId()
						==pais.getId())) {
					System.err.println(filhos.getIdSubordinacao().getNome());
					System.err.println(filhos.getNome());
					listRetorno.add(filhos);
				}
			}
			
		}
		
		return listRetorno;
	}

}
