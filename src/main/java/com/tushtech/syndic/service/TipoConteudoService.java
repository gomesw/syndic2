package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.TipoConteudo;
import com.tushtech.syndic.repository.TipoConteudoRepository;

@Service
public class TipoConteudoService {

	@Autowired
	private TipoConteudoRepository TipoConteudoRepository;
	
	public void salvarTipoConteudo(TipoConteudo tipoConteudo){
		
		tipoConteudo.setAtivo(1);
		this.TipoConteudoRepository.save(tipoConteudo);
	}
	
	public void excluirTipoConteudo(TipoConteudo tipoConteudo){
		this.TipoConteudoRepository.delete(tipoConteudo);
	}

	
}
