package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.TipoDocumento;
import com.tushtech.syndic.repository.TipoDocumentoRepository;

@Service
public class TipoDocumentoService {

	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository; 
	
	public void salvarTipoDocumento(TipoDocumento tipoDocumento){
		
		tipoDocumento.setAtivo(1);
		this.tipoDocumentoRepository.save(tipoDocumento); 
	}
	
	public void excluirTipoDocumento(TipoDocumento tipoDocumento){
		this.tipoDocumentoRepository.delete(tipoDocumento);
	}
	
}
