package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.TipoEnderecoUnidade;
import com.tushtech.syndic.repository.TipoEnderecoUnidadeRepository;

@Service
public class TipoEnderecoUnidadeService {

	@Autowired
	private TipoEnderecoUnidadeRepository tipoEnderecoUnidadeRepository;
	
	public void salvarTipoEnderecoUnidade(TipoEnderecoUnidade tipoEnderecoUnidade){
		this.tipoEnderecoUnidadeRepository.saveAndFlush(tipoEnderecoUnidade);
	}
	
	public void excluirTipoEnderecoUnidade(TipoEnderecoUnidade tipoEnderecoUnidade){
		this.tipoEnderecoUnidadeRepository.delete(tipoEnderecoUnidade);
	}

	
}
