package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.TipoEquipamento;
import com.tushtech.syndic.repository.TipoEquipamentoRepository;

@Service
public class TipoEquipamentoService {

	@Autowired
	private TipoEquipamentoRepository TipoEquipamentoRepository;
	
	public void salvarTipoEquipamento(TipoEquipamento tipoEquipamento){
		this.TipoEquipamentoRepository.saveAndFlush(tipoEquipamento);
	}
	
	public void excluirTipoEquipamento(TipoEquipamento tipoEquipamento){
		this.TipoEquipamentoRepository.delete(tipoEquipamento);
	}

	
}
