package com.tushtech.syndic.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.TipoOrdemServico;
import com.tushtech.syndic.repository.TipoOrdemServicoRepository;
import com.tushtech.syndic.service.exception.NomeDuplicadoException;


@Service
public class TipoOrdemServicoService {


	@Autowired
	private TipoOrdemServicoRepository tipoOrdemServicoRepository;

	@Transactional
	public void salvar(TipoOrdemServico tipoOrdemServico){   

		if(tipoOrdemServico.isNovo()){
			//VERIFICA SE MARCA DE VEICULO JÁ FOI CADASTRADA
			Optional<TipoOrdemServico> tipoOrdemServicoExiste = tipoOrdemServicoRepository.findByNome(tipoOrdemServico.getNome());
																				
			if(tipoOrdemServicoExiste.isPresent()){
				if(tipoOrdemServicoExiste.get().getAtivo()==0){
					throw new NomeDuplicadoException(" Tipo de OS está desativado! <a style=\"color:#ffffff;\" href=\"/tipoordemservico/ativar/"+tipoOrdemServicoExiste.get().getId()+"\">Deseja Ativá-la? <strong>SIM</strong></a>"	);
				}
				else{
					throw new NomeDuplicadoException("OS já cadastrada");
				}
			}
		}

		tipoOrdemServico.setAtivo(1);
		tipoOrdemServicoRepository.save(tipoOrdemServico);
	}

	
	@Transactional
	public TipoOrdemServico desativar(TipoOrdemServico tipoOrdemServico) {
		tipoOrdemServico = tipoOrdemServicoRepository.findOne(tipoOrdemServico.getId());
		tipoOrdemServico.setAtivo(0);
		this.tipoOrdemServicoRepository.saveAndFlush(tipoOrdemServico); 
		return tipoOrdemServico;
	}
	
	@Transactional
	public TipoOrdemServico ativar(TipoOrdemServico tipoOrdemServico) {
		tipoOrdemServico = tipoOrdemServicoRepository.findOne(tipoOrdemServico.getId());
		tipoOrdemServico.setAtivo(1);
		this.tipoOrdemServicoRepository.saveAndFlush(tipoOrdemServico); 
		return tipoOrdemServico;
	}
}



