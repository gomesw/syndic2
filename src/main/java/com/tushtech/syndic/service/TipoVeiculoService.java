package com.tushtech.syndic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.TipoVeiculo;
import com.tushtech.syndic.repository.TipoVeiculoRepository;

@Service
public class TipoVeiculoService {

	@Autowired
	private TipoVeiculoRepository TipoVeiculoRepository;
	
	public void salvarTipoVeiculo(TipoVeiculo tipoVeiculo){
		this.TipoVeiculoRepository.saveAndFlush(tipoVeiculo);
	}
	
	public void excluirTipoVeiculo(TipoVeiculo tipoVeiculo){
		this.TipoVeiculoRepository.delete(tipoVeiculo);
	}

	
}
