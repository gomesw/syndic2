package com.tushtech.syndic.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.tushtech.syndic.entity.Email;
import com.tushtech.syndic.entity.PessoaFisica;
import com.tushtech.syndic.entity.PessoaFisicaUnidade;
import com.tushtech.syndic.entity.PessoaFisicaUnidadeRecurso;
import com.tushtech.syndic.entity.PessoaFisicaVeiculo;
import com.tushtech.syndic.entity.Recurso;
import com.tushtech.syndic.entity.Telefone;
import com.tushtech.syndic.entity.Unidade;
import com.tushtech.syndic.entity.Veiculo;
import com.tushtech.syndic.repository.EmailRepository;
import com.tushtech.syndic.repository.PessoaFisicaUnidadeRecursoRepository;
import com.tushtech.syndic.repository.PessoaFisicaVeiculoRepository;
import com.tushtech.syndic.repository.RecursoRepository;
import com.tushtech.syndic.repository.TelefoneRepository;
import com.tushtech.syndic.repository.UnidadeRepository;
import com.tushtech.syndic.repository.VeiculoRepository;

@Service
public class UnidadeService {

	@Autowired
	private UnidadeRepository unidadeRepository;

	
	
	@Autowired
	private PessoaFisicaUnidadeRecursoRepository pessoaFisicaUnidadeRecursoRepository;
	
	@Autowired
	private RecursoRepository recursoRepository;




	
	
	@Autowired
	private PessoaFisicaVeiculoRepository pessoaFisicaVeiculoRepository;
	
	
	@Autowired
	private TelefoneRepository telefoneRepository;
	
	

	@Autowired 
	EmailRepository emailRepository; 
	
	
	@Autowired
	private VeiculoService veiculoService;

	@Autowired 
	VeiculoRepository veiculoRepository; 
	


	public void salvarUnidade(Unidade unidade){
		this.unidadeRepository.save(unidade);
	}

	public Unidade salvarUnidadeRetornar(Unidade unidade){
		return this.unidadeRepository.saveAndFlush(unidade);
	}

	public void excluirUnidade(Unidade unidade){
		this.unidadeRepository.delete(unidade);
	}




	/**
	 * Inserir recursos no momento do cadastro do morador
	 * @param pessoaFisicaUnidade
	 * @param recursos
	 */
	public void inserirRecursos(PessoaFisicaUnidade pessoaFisicaUnidade, String recursos) {
		List<PessoaFisicaUnidadeRecurso> listPessoaFisicaUnidadeRecurso = new ArrayList<>();

		
		List<PessoaFisicaUnidadeRecurso> listPessoaFisicaUnidadeRecursoBanco = pessoaFisicaUnidadeRecursoRepository.findByPessoaFisicaUnidadeId(pessoaFisicaUnidade.getId());
		
		for (PessoaFisicaUnidadeRecurso pessoaFisicaUnidadeRecurso : listPessoaFisicaUnidadeRecursoBanco) {
			pessoaFisicaUnidadeRecursoRepository.delete(pessoaFisicaUnidadeRecurso);
		}
		
		
		if(recursos==null || recursos.isEmpty()) {
			System.err.println("Não tem recursos de Sistema");
		}
		else {
			
			String idsRecursos[] = recursos.split(","); 
			Integer ids[] = new Integer[idsRecursos.length];
			
			int c = 0;
			for (String id : idsRecursos) {
				ids[c]= Integer.valueOf(id);
				c++;
			}

			List<Recurso> listrecursos = recursoRepository.findByIdIn(ids);

			for (Recurso recurso : listrecursos) {
				PessoaFisicaUnidadeRecurso pessoaFisicaUnidadeRecurso = new PessoaFisicaUnidadeRecurso();
				pessoaFisicaUnidadeRecurso.setPessoaFisicaUnidade(pessoaFisicaUnidade);
				pessoaFisicaUnidadeRecurso.setRecurso(recurso);
				listPessoaFisicaUnidadeRecurso.add(pessoaFisicaUnidadeRecurso);
			}
			
			pessoaFisicaUnidadeRecursoRepository.save(listPessoaFisicaUnidadeRecurso);

		}
	}

	
	
	public void inserirTelefonePessoaFisica(PessoaFisica pf) {
		List<Telefone> telefones = pf.getTelefones();
		if(!pf.getTelefones().isEmpty()) {
			telefoneRepository.delete(pf.getTelefones());
		}
		for (Telefone telefone : telefones) {
			
				//telefoneRepository.save(telefone);
		} 

	}
	
	
	public void inserirTelefonePessoaFisicaUnidade(PessoaFisica pf) {
		List<Telefone> telefones = pf.getTelefones();
		//salvar telefones
		//atribui a pessoa salva aos telefones
		for (Telefone telefone : telefones) {
			
			if(telefone!=null && telefone.getNumero()!=null && !telefone.getNumero().equals("")) {
				telefone.setPessoaFisica(pf);
				telefoneRepository.save(telefone);
			}
		} 

	}
	
	
	public void inserirEmailPessoaFisicaUnidade(Email email) {
		if(!email.getNome().equals("") && !email.getNome().isEmpty()) {
			emailRepository.save(email);
		}
	}
	
	
	public void inserirVeiculo(PessoaFisicaVeiculo pessoaFisicaVeiculo) {
		
	
		
		
		
		if(pessoaFisicaVeiculo.getVeiculo()!=null && pessoaFisicaVeiculo.getVeiculo().getPlaca() ==null && 
				pessoaFisicaVeiculo.getVeiculo().getPlaca().trim().isEmpty()) {
			Veiculo veiculo = pessoaFisicaVeiculo.getVeiculo();
			if(!pessoaFisicaVeiculo.isNovo()){
				pessoaFisicaVeiculoRepository.delete(pessoaFisicaVeiculo);
				veiculoRepository.delete(veiculo);
			}
		}
		else {	
			Veiculo veiculos = pessoaFisicaVeiculo.getVeiculo();
			Veiculo veiculo = veiculoService.salvarVeiculo(veiculos);
			pessoaFisicaVeiculo.setVeiculo(veiculo);
			pessoaFisicaVeiculoRepository.save(pessoaFisicaVeiculo);
		}
	}

	
	
	public void atribuirErrosUnidade(Unidade unidade,BindingResult result){

		Unidade unidadeexiste = unidadeRepository.verificarExistenciaUnidadeRepetida(
				unidade);
		
		if(unidadeexiste!=null && unidadeexiste.getId()!=unidade.getId()){
		 result.rejectValue("unidadeHabitacional","","Ops, essa Unidade já existe desejá visualiza-lá ? <a  href=\"/unidade/editar/"+unidadeexiste.getId()+"\" ><b>SIM</b></a>"); 
		}
	
	}
	
	


}
