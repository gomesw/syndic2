package com.tushtech.syndic.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tushtech.syndic.entity.Veiculo;
import com.tushtech.syndic.repository.VeiculoRepository;
import com.tushtech.syndic.service.exception.VeiculoJaCadastradoException;

@Service
public class VeiculoService {

	@Autowired
	private VeiculoRepository VeiculoRepository;
	
	
	
	public Veiculo salvarVeiculo(Veiculo veiculo){
		
		verificarExistenciaPlaca(veiculo);
		
		return this.VeiculoRepository.saveAndFlush(veiculo);
	}
	
	public void excluirVeiculo(Veiculo veiculo){
		this.VeiculoRepository.delete(veiculo);
	}
	
	
	/**
	 * VERIFICA SE EXISTE A PLACA JÁ CADASTRADA NA BASE DE DADOS 
	 * @param pessoaFisicaVeiculo
	 * @return
	 */
	private void verificarExistenciaPlaca(Veiculo veiculo) {
		
		
		Optional<Veiculo> veiculoExisteOptional = VeiculoRepository.findByPlaca(veiculo.getPlaca().toUpperCase());
		
		
		if (veiculoExisteOptional.isPresent()) {
			
			Veiculo veiculoExistente = veiculoExisteOptional.get();
			
			throw new VeiculoJaCadastradoException("Veiculo já cadastrado  <b>"+veiculoExistente.getDescricao()+"</b>");
		}
		
	}

	
}
