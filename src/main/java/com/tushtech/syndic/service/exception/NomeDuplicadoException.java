package com.tushtech.syndic.service.exception;

public class NomeDuplicadoException extends RuntimeException{
	
private static final long serialVersionUID = 1L;
	
	public NomeDuplicadoException(String message) {
		super(message);
	}

}
