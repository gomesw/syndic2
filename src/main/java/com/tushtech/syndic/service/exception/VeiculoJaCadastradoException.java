package com.tushtech.syndic.service.exception;

public class VeiculoJaCadastradoException extends RuntimeException{
	
private static final long serialVersionUID = 1L;
	
	public VeiculoJaCadastradoException(String message) {
		super(message);
	}

}
