package com.tushtech.syndic.storage;

import org.springframework.web.multipart.MultipartFile;

public interface ArquivoStorage {

	public String salvarTemporariamente(MultipartFile[] files,String local);

	public byte[] recuperarFotoTemporaria(String nome);

	public void salvar(String foto,String diretorio);

	public byte[] recuperar(String foto,String diretorio);

	public int remover(String nome,String diretorio);

	void moverWebCamPasta(String foto, String diretorio);
	
	byte[] girar90Recuperar(String nome, String diretorio);
	
	byte[] girarAnti90Recuperar(String nome, String diretorio);


}
