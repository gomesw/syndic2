package com.tushtech.syndic.storage.local;



import static java.nio.file.FileSystems.getDefault;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.tushtech.syndic.storage.ArquivoStorage;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;


@Profile("local")
@Component
public class ArquivoStorageLocal implements ArquivoStorage {


		private static final Logger logger = LoggerFactory.getLogger(ArquivoStorageLocal.class);
		
		private Path local;
		private Path localTemporario;

		
		
		public Path getLocal() {
			
			this.local = getDefault().getPath(defaultDirectory());
			
			return local;
		}


		public void setLocal(Path local) {
			this.local = local;
		}


		public Path getLocalTemporario() {
			
			return localTemporario;
		}


		public void setLocalTemporario(Path localTemporario) {
			this.localTemporario = localTemporario;
		}


		private static String defaultDirectory()
		{
		    String OS = System.getProperty("os.name").toUpperCase();
		    
		    String nome = "";
		    
		    if (OS.contains("WIN"))
		    	nome = System.getenv("APPDATA");
		    else if (OS.contains("MAC"))
		    	nome = System.getProperty("user.home") + "/Library/Application "
		                + "Support";
		    else if (OS.contains("NUX"))
		    	nome = System.getProperty("user.home");
		   
		    return nome;
		}
		
		
		public ArquivoStorageLocal() {
			this(getDefault().getPath(defaultDirectory()));
		}
		
		public ArquivoStorageLocal(Path path) {
			this.local = path;
			
		}
		
		

		@Override
		public String salvarTemporariamente(MultipartFile[] files,String diretorio) { 
			
			
			criarPastas(diretorio);
			
			String novoNome = null;
			if (files != null && files.length > 0) {
				MultipartFile arquivo = files[0];
				novoNome = renomearArquivo(arquivo.getOriginalFilename());
				
				String local =  defaultDirectory()+getDefault().getSeparator()+diretorio+getDefault().getSeparator()+"temp"+getDefault().getSeparator()+ novoNome;
				
				try {
					arquivo.transferTo(new File(local));
				} catch (IOException e) {
					throw new RuntimeException("Erro salvando a foto na pasta temporária", e); 
				}
				
				
			}
			salvar(novoNome,diretorio);
			return novoNome;
		}
		
		@Override
		public byte[] recuperarFotoTemporaria(String nome) {
			try {
				return Files.readAllBytes(this.local.resolve(nome));
			} catch (IOException e) {
				throw new RuntimeException("Erro lendo a foto temporária", e);
			}
		}
		
		@Override
		public void salvar(String arquivo,String diretorio) {
			this.local = getDefault().getPath(defaultDirectory()+getDefault().getSeparator()+diretorio);
			this.localTemporario = getDefault().getPath(defaultDirectory()+getDefault().getSeparator()+diretorio+getDefault().getSeparator()+"temp");
			
			try {
				Files.move(this.localTemporario.resolve(arquivo), this.local.resolve(arquivo));
			} catch (IOException e) {
				throw new RuntimeException("Erro movendo a foto para destino final", e);
			}
			System.err.println(diretorio);
			if(diretorio.equals(File.separator+"ARQUIVOS"+File.separator+"FotosAnimais")) {
				try {
					Thumbnails.of(this.local.resolve(arquivo).toString()).size(800, 800).toFile(this.local.resolve(arquivo).toString());
					Thumbnails.of(this.local.resolve(arquivo).toString()).size(200, 200).toFiles(Rename.PREFIX_DOT_THUMBNAIL);
				} catch (IOException e) {
					throw new RuntimeException("Erro gerando thumbnail", e);
				}
			}
			
			if(diretorio.equals(File.separator+"ARQUIVOS"+File.separator+"ArquivosOs")) {
				try {
					Thumbnails.of(this.local.resolve(arquivo).toString()).size(1191, 1684).toFile(this.local.resolve(arquivo).toString());
					Thumbnails.of(this.local.resolve(arquivo).toString()).size(200, 200).toFiles(Rename.PREFIX_DOT_THUMBNAIL);
				} catch (IOException e) {
					throw new RuntimeException("Erro gerando thumbnail", e);
				}
			}
				
		}
		
		@Override
		public byte[] recuperar(String nome,String diretorio) {
			
			
			this.local = getDefault().getPath(defaultDirectory()+File.separator+diretorio);
			
			try {
				return Files.readAllBytes(this.local.resolve(nome));
			} catch (IOException e) {
				throw new RuntimeException("Erro lendo a foto", e);
			}
		}
		
		
		
		private void criarPastas(String local) {
			try {
				
				
				Files.createDirectories(getDefault().getPath(defaultDirectory(),local));
				//this.localTemporario = getDefault().getPath(this.local.toString(), "temp");
				Files.createDirectories(getDefault().getPath(defaultDirectory()+getDefault().getSeparator()+local, "temp"));
				
				if (logger.isDebugEnabled()) {
					logger.debug("Pastas criadas para salvar fotos.");
					logger.debug("Pasta default: " + this.local.toAbsolutePath());
					logger.debug("Pasta temporária: " + this.localTemporario.toAbsolutePath());
				}
			} catch (IOException e) {
				throw new RuntimeException("Erro criando pasta para salvar foto", e);
			}
		}
		
		private String renomearArquivo(String nomeOriginal) {
			
			
			String tem[] = nomeOriginal.split("\\."); 
			
			String novoNome = UUID.randomUUID().toString() + "." +tem[tem.length-1];
			
			if (logger.isDebugEnabled()) {
				logger.debug(String.format("Nome original: %s, novo nome: %s", nomeOriginal, novoNome));
			}
			
			return novoNome;
			
		}


		@Override
		public int remover(String nome, String diretorio) {
				
			this.local = getDefault().getPath(defaultDirectory()+File.separator+diretorio);
			
			try {
				Files.deleteIfExists(this.local.resolve(nome));
				Files.deleteIfExists(this.local.resolve("thumbnail."+nome));
				return 1;
			} catch (IOException e) {
				return 0;
			}
		}


		@Override
		public void moverWebCamPasta(String foto, String diretorio) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public byte[] girar90Recuperar(String nome,String diretorio) {
			
			this.local = getDefault().getPath(defaultDirectory()+File.separator+diretorio);
			
			girar(defaultDirectory()+diretorio,"90",nome);
			
			try {
				return Files.readAllBytes(this.local.resolve(nome));
			} catch (IOException e) {
				throw new RuntimeException("Erro lendo a foto", e);
			}
		}
		
		@Override
		public byte[] girarAnti90Recuperar(String nome,String diretorio) {
			
			this.local = getDefault().getPath(defaultDirectory()+File.separator+diretorio);
			
			girar(defaultDirectory()+diretorio,"-90",nome);
			
			try {
				return Files.readAllBytes(this.local.resolve(nome));
			} catch (IOException e) {
				throw new RuntimeException("Erro lendo a foto", e);
			}
		}
		
		
		
		public void girar(String foto, String graus,String nome){
			BufferedImage image2 =null;
			
			
			try {
				 image2 = ImageIO.read(new File(foto+File.separator+nome));
			} catch (IOException e1) {
				e1.printStackTrace();
			}   
			
			image2 = rotateImage(image2, Double.valueOf(graus));
			
			
			
			File fileout = new File(foto+File.separator+nome);
			
			
			//new File("C:/Users/1954571/AppData/Roaming/teste/watermarkedImage.jpg");
			try {
				ImageIO.write(image2, "jpg", fileout); 

			}catch (FileNotFoundException e) {  
				e.printStackTrace();
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		public static BufferedImage rotateImage(BufferedImage rotateImage, double angle) {
			AffineTransform tx = new AffineTransform();
		    tx.rotate(Math.toRadians(angle), rotateImage.getWidth() / 2.0, rotateImage.getHeight() / 2.0);

		    // Rotaciona as coordenadas dos cantos da imagem
		    Point2D[] aCorners = new Point2D[4];
		    aCorners[0] = tx.transform(new Point2D.Double(0.0, 0.0), null);
		    aCorners[1] = tx.transform(new Point2D.Double(rotateImage.getWidth(), 0.0), null);
		    aCorners[2] = tx.transform(new Point2D.Double(0.0, rotateImage.getHeight()), null);
		    aCorners[3] = tx.transform(new Point2D.Double(rotateImage.getWidth(), rotateImage.getHeight()), null);

		    // Obtém o valor de translação para cada eixo com um canto "escondido"
		    double dTransX = 0;
		    double dTransY = 0;
		    for(int i = 0; i < 4; i++) {
		        if(aCorners[i].getX() < 0 && aCorners[i].getX() < dTransX)
		            dTransX = aCorners[i].getX();
		        if(aCorners[i].getY() < 0 && aCorners[i].getY() < dTransY)
		            dTransY = aCorners[i].getY();
		    }

		    // Aplica a translação para evitar cortes na imagem
		    AffineTransform translationTransform = new AffineTransform();
		    translationTransform.translate(-dTransX, -dTransY);
		    tx.preConcatenate(translationTransform);

		    return new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR).filter(rotateImage, null);
		}
		
		
		

	
}
