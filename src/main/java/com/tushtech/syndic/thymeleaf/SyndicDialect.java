package com.tushtech.syndic.thymeleaf;

import java.util.HashSet;
import java.util.Set;

import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;
import org.thymeleaf.standard.StandardDialect;

import com.tushtech.syndic.thymeleaf.processor.ClassForErrorAttributeTagProcessor;
import com.tushtech.syndic.thymeleaf.processor.MenuAttributeTagProcessor;

public class SyndicDialect extends AbstractProcessorDialect {

	public SyndicDialect() {
		super("SYNDIC", "syndic", StandardDialect.PROCESSOR_PRECEDENCE);
	}
	
	@Override
	public Set<IProcessor> getProcessors(String dialectPrefix) {
		final Set<IProcessor> processadores = new HashSet<>();
		processadores.add(new ClassForErrorAttributeTagProcessor(dialectPrefix));
		processadores.add(new MenuAttributeTagProcessor(dialectPrefix));
		return processadores;
	}

}
