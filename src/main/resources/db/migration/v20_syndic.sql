/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     21/08/2017 20:21:35                          */
/*==============================================================*/
SET FOREIGN_KEY_CHECKS = 0;

drop table if exists ANEXOORDEMSERVICO;

drop table if exists ANEXOCONDOMINIO;

drop table if exists AREAATUACAO;

drop table if exists AVALIACAOORDEMSERVICO;

drop table if exists AVALIACAOOS_PERGUNTAAVALIACAO;

drop table if exists ATENDIMENTO;

drop table if exists ATENDIMENTORESPOSTA;

drop table if exists ATENDIMENTO_STATUS;

drop table if exists ANIMAL;

drop table if exists ANUNCIO;

drop table if exists AREACOMUM;

drop table if exists AREACOMUMFUNCIONAMENTO;

drop table if exists AUDITORIA;

drop table if exists BAIRRO;

drop table if exists CIDADE;

drop table if exists CONDOMINIO_MODULO;

drop table if exists CONDOMINIO;

drop table if exists CONDOMINIO_FUNCIONARIO;

drop table if exists CONTEUDO;

drop table if exists CONTEUDO_IMAGEM;

drop table if exists COR;

drop table if exists CORANIMAL;

drop table if exists CONDOMINIO_PACOTESERVICO;

drop table if exists CORRESPONDENCIA;

drop table if exists DIASEMANA;

drop table if exists DOCUMENTO;

drop table if exists EMAIL;

drop table if exists ENDERECO;

drop table if exists ENDERECOUNIDADE;

drop table if exists ENQUETE;

drop table if exists EQUIPAMENTO;

drop table if exists FOTOANIMAL;

drop table if exists FOTOSCLASSICADOS;

drop table if exists FOTOORDEMSERVICO;

drop table if exists FUNCAO;

drop table if exists FUNCAOPESSOAJURIDICA;

drop table if exists FUNCIONARIO;

drop table if exists GALERIA;

drop table if exists GALERIA_IMAGEM;

drop table if exists HISTORICOFINANCEIROCONTASRECEBER;

drop table if exists HISTORICOPORTARIAPESSOA;

drop table if exists HISTORICOPORTARIAVEICULO;

drop table if exists HISTORICOPORTARIA;

drop table if exists IMAGEM;

drop table if exists ITENSORCAMENTO;

drop table if exists LOGIN;

drop table if exists MARCA;

drop table if exists MATERIAL;

drop table if exists MENSAGEM;

drop table if exists MODELO;

drop table if exists MODULO;

drop table if exists ORDEMSERVICO;

drop table if exists ORCAMENTO;

drop table if exists PARENTESCO;

drop table if exists PERFIL;

drop table if exists PERFIL_CONDOMINIO;

drop table if exists PERFIL_CONDOMINIO_RECURSO;

drop table if exists PERGUNTAENQUETE;

drop table if exists PESSOAFISICA;

drop table if exists PESSOAFISICA_CONDOMINIO;

drop table if exists PESSOAFISICA_PERFIL_CONDOMINIO;

drop table if exists PESSOAJURIDICA_PESSOAFISICA;

drop table if exists PESSOAFISICA_PERGUNTA;

drop table if exists PESSOAFISICA_TIPOPESSOA;

drop table if exists PESSOAFISICA_UNIDADE;

drop table if exists PESSOAFISICA_UNIDADE_DIASEMANA;

drop table if exists PESSOAFISICA_ORDEMSERVICO;

drop table if exists PESSOAFISICA_VEICULO;

drop table if exists PESSOAJURIDICA;

drop table if exists PACOTESERVICO;

drop table if exists PERGUNTAAVALIACAO;

drop table if exists PERFIL_RECURSO;

drop table if exists PESSOAFISICA_PROFISSAO;

drop table if exists PESSOAJURIDICA_CONDOMINIO;

drop table if exists PESSOAJURIDICA_CONDOMINIO_PACOTESERVICO;

drop table if exists PROFISSAO;

drop table if exists RACA;

drop table if exists RECURSO;

drop table if exists RECURSO_PESSOAFISICA_UNIDADE;

drop table if exists REGIMETRABALHO;

drop table if exists RESERVA;

drop table if exists RECURSO_PESSOAJURIDICA_PESSOAFISICA;

drop table if exists SENHAS;

drop table if exists SEXO;

drop table if exists STATUSATENDIMENTO;

drop table if exists STATUSCHAMADO;

drop table if exists STATUSDOCUMENTO;

drop table if exists TELEFONE;

drop table if exists TIPOANIMAIL;

drop table if exists TIPOANUNCIO;

drop table if exists TIPOCONTASRECEBER;

drop table if exists TIPOCONTEUDO;

drop table if exists TIPOCORRESPONDENCIA;

drop table if exists TIPODOCUMENTO;

drop table if exists TIPOENDERECOUNIDADE;

drop table if exists TIPOEQUIPAMENTO;

drop table if exists TIPOATENDIMENTO;

drop table if exists TIPOORDEMSERVICO;

drop table if exists TIPOFUNCIONARIO;

drop table if exists TIPOMATERIAL;

drop table if exists TIPOPESSOA;

drop table if exists TIPOPESSOAFISICAUNIDADE;

drop table if exists TIPOPESSOAFISICACONDOMINIO;

drop table if exists TIPOUNIDADE;

drop table if exists TIPOVEICULO;

drop table if exists UFE;

drop table if exists UNIDADE;

drop table if exists VAGAS;

drop table if exists VEICULO;

drop table if exists HISTORICOORDEMSERVICO;

drop table if exists ORDEMSERVICOSTATUS;

drop table if exists TIPOANIMAL;

drop table if exists TURNO;



SET FOREIGN_KEY_CHECKS = 1;

/*==============================================================*/
/* Table: AREAATUACAO                                           */
/*==============================================================*/
create table AREAATUACAO
(
   AAT_CODIGO           int not null auto_increment,
   AAT_NOME             varchar(255) not null,
   AAT_DESCRICAO        text,
   AAT_ATIVO            int not null DEFAULT '1',
   primary key (AAT_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: AVALIACAOORDEMSERVICO                                 */
/*==============================================================*/
create table AVALIACAOORDEMSERVICO
(
   AOS_CODIGO           int not null auto_increment,
   ORD_CODIGO           int not null,
   PEF_CODIGOAVALIADOR  int not null,
   PEF_CODIGOAVALIADO   int,
   AOS_DTAVALIACAO      DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   AOS_OBSERVACAO       text,
   primary key (AOS_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: ANEXOCONDOMINIO                                       */
/*==============================================================*/
create table ANEXOCONDOMINIO
(
   ACO_CODIGO           int not null auto_increment,
   CON_CODIGO           int not null,
   ACO_NOME             varchar(255) not null,
   ACO_CONTENTTYPE      varchar(255) not null,
   ACO_ATIVO            int not null DEFAULT '1',
   primary key (ACO_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: ANEXOORDEMSERVICO                                     */
/*==============================================================*/
   
create table ANEXOORDEMSERVICO
(
   AOS_CODIGO           int not null auto_increment,
   ORD_CODIGO           int not null,
   AOS_NOME             varchar(255) not null,
   AOS_CONTENTTYPE      varchar(255) not null,
   AOS_ATIVO            int not null DEFAULT '1',
   primary key (AOS_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: AVALIACAOOS_PERGUNTAAVALIACAO                         */
/*==============================================================*/
create table AVALIACAOOS_PERGUNTAAVALIACAO
(
   APA_CODIGO           int not null auto_increment,
   AOS_CODIGO           int not null,
   PAV_CODIGO           int not null,
   APA_PONTUACAO        int not null,
   primary key (APA_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: ATENDIMENTO                                           */
/*==============================================================*/
create table ATENDIMENTO
(
   ATE_CODIGO           int not null auto_increment,
   TAT_CODIGO           int not null,
   PEF_CODIGO           int,
   CON_CODIGO			int not null,
   ATE_DESCRICAO        text,
   ATE_DTCADASTRO       DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UNI_CODIGO           INT NOT NULL,
   ATE_NRPROTOCOLO      int not null,
   primary key (ATE_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: ATENDIMENTORESPOSTA                                   */
/*==============================================================*/
create table ATENDIMENTORESPOSTA
(
   ARE_CODIGO           int not null auto_increment,
   ATE_CODIGO           int not null,
   PEF_CODIGO           int not null,
   ARE_SUBORDINACAO     int,
   ARE_RESPOSTA         text not null,
   ARE_DTRESPOSTA       datetime not null,
   primary key (ARE_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: ATENDIMENTO_STATUS                                    */
/*==============================================================*/
create table ATENDIMENTO_STATUS
(
   ATS_CODIGO           int not null auto_increment,
   STA_CODIGO           int not null,
   ATE_CODIGO           int not null,
   ATS_DTMUDANCA        datetime not null,
   ATS_ATIVO 			INT NOT NULL,
   primary key (ATS_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: ANIMAL                                                */
/*==============================================================*/
create table ANIMAL
(
   ANI_CODIGO           int not null auto_increment,
   UNI_CODIGO           int,
   ANI_NOME             varchar(255),
   ANI_DESCRICAO        varchar(255),
   COA_CODIGO          INT NULL,
   RAC_CODIGO          INT NULL,
   primary key (ANI_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: ANUNCIO                                               */
/*==============================================================*/
create table ANUNCIO
(
   ANU_CODIGO           int not null auto_increment,
   TPU_CODIGO           int not null,
   TIN_CODIGO           int not null,
   PEF_CODIGO           int not null,
   ANU_DESCRICAO        varchar(255) not null,
   ANU_QTDQUARTOS       int not null,
   ANU_TEXTO            text,
   CON_CODIGO 			INT NOT NULL,
   primary key (ANU_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: AREACOMUM                                             */
/*==============================================================*/
create table AREACOMUM
(
   ARC_CODIGO           int not null auto_increment,
   ARC_NOME             varchar(255) not null,
   ARC_VALOR            float(6),
   ARC_QTDMAXCONVIDADOS int,
   ARC_STATUS           varchar(50) not null,
   ARC_QTDMAXRESERVAS   int,
   primary key (ARC_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: AREACOMUMFUNCIONAMENTO                                */
/*==============================================================*/
create table AREACOMUMFUNCIONAMENTO
(
   ACF_CODIGO           int not null auto_increment,
   ARC_CODIGO           int,
   ACF_DTINICIO         datetime not null,
   ACF_DTFIM            datetime not null,
   ACF_DIASEMANA        varchar(30) not null,
   primary key (ACF_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: AUDITORIA                                             */
/*==============================================================*/
create table AUDITORIA
(
   AUD_CODIGO           int not null auto_increment,
   PEF_CODIGOAUTOR      int not null,
   PEF_CODIGOAFETADO    int not null,
   AUD_DTHORAAUDITORIA  datetime not null,
   AUD_VALORANTES       text,
   AUD_VALORDEPOIS      text,
   AUD_ACAO             varchar(100) not null,
   primary key (AUD_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: BAIRRO                                                */
/*==============================================================*/
create table BAIRRO
(
   BAI_CODIGO           int not null auto_increment,
   BAI_NOME             varchar(255) not null,
   BAI_SIGLA            varchar(60) not null,
   CID_CODIGO           int not null,
   primary key (BAI_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: CIDADE                                                */
/*==============================================================*/
create table CIDADE
(
   CID_CODIGO           int not null auto_increment,
   UFE_CODIGO           int not null,
   CID_NOME             varchar(255) not null,
   CID_CEP              varchar(8),
   primary key (CID_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: CONDOMINIO                                            */
/*==============================================================*/
create table CONDOMINIO
(
   CON_CODIGO           int not null auto_increment,
   PEJ_CODIGO           int,
   CON_LOGO             varchar(255),
   CON_NOMEFOTO 		VARCHAR(255),
   CON_CONTENTTYPE		VARCHAR(255), 
   CON_DTCADASTRO		DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   UFE_CODIGO           int,
   CON_ENDERECO			VARCHAR(255),
   CON_TELEFONE_1		VARCHAR(15),
   CON_TELEFONE_2		VARCHAR(15),
   CON_TELEFONE_3		VARCHAR(15),
   CON_TELEFONE_4		VARCHAR(15),
   CON_EMAIL_1          VARCHAR(150) NULL,
   CON_EMAIL_2          VARCHAR(150) NULL,
   CON_ATIVO            int not null DEFAULT '1',
   CON_ADIMPLENTE       int not null DEFAULT '1',
   primary key (CON_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: CONDOMINIO_MODULO                                     */
/*==============================================================*/
create table CONDOMINIO_MODULO
(
   CMO_CODIGO           int not null auto_increment,
   MOU_CODIGO           int not null,
   CON_CODIGO           int not null,
   CMO_DTINICIO         DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   CMO_DTFIM            datetime,
   CMO_VALOR            float,
   primary key (CMO_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: CONDOMINIO_PACOTESERVICO                              */
/*==============================================================*/
create table CONDOMINIO_PACOTESERVICO
(
   COP_CODIGO           int not null auto_increment,
   PCS_CODIGO           int not null,
   CON_CODIGO           int not null,
   COP_DTINICIO         DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   COP_DTFIM            datetime,
   COP_VALOR            float,
   COP_ATIVO            INT NOT NULL DEFAULT 1,
   primary key (COP_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: CONTEUDO                                              */
/*==============================================================*/
create table CONTEUDO
(
   COT_CODIGO               int not null auto_increment,
   TIC_CODIGO               int not null,
   PEF_CODIGOUSUARIOPUBLICA int not null,
   PEF_CODIGOUSUARIORETIRA  int,
   COT_TITULO               varchar(255) not null,
   COT_TEXTO                text,
   COT_DTINICIO             datetime not null,
   COT_DTFIM                datetime,
   CON_CODIGO			    INT NOT NULL,
   COT_DESTINOPUBLICACAO    INT NOT NULL,
   primary key (COT_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: CONTEUDO_IMAGEM                                       */
/*==============================================================*/
create table CONTEUDO_IMAGEM
(
   COI_CODIGO           int not null auto_increment,
   COT_CODIGO           int not null,
   IMA_CODIGO           int not null,
   COI_DTINICIO         datetime not null,
   COI_DTFIM            datetime,
   primary key (COI_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: COR                                                   */
/*==============================================================*/
create table COR
(
   CRR_CODIGO           int not null auto_increment,
   CRR_NOME             varchar (255) not null,
   CRR_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (CRR_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: CORANIMAL                                             */
/*==============================================================*/
create table CORANIMAL
(
   COA_CODIGO           int not null auto_increment,
   COA_NOME             varchar(50) not null,
   COA_ATIVO            int not null DEFAULT '1',
   primary key (COA_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: CORRESPONDENCIA                                       */
/*==============================================================*/
create table CORRESPONDENCIA
(
   COR_CODIGO           int not null auto_increment,
   UNI_CODIGO           int not null,
   TPC_CODIGO           int not null,
   PEJ_CODIGOFUNCIONARIO int not null,
   COR_DTRECEBIMENTO    datetime not null,
   COR_DTENTREGA        datetime,
   COR_CODRASTREAMENTO  varchar(30),
   primary key (COR_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: DIASEMANA                                             */
/*==============================================================*/
create table DIASEMANA
(
   DSS_CODIGO           int not null auto_increment,
   DSS_NOME             varchar(255) not null,
   DSS_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (DSS_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: DOCUMENTO                                             */
/*==============================================================*/
create table DOCUMENTO
(
   DOC_CODIGO           int not null auto_increment,
   TID_CODIGO           int not null,
   PFU_CODIGO           int not null,
   DOC_NOME             varchar(255) null,
   DOC_CONTENTTYPE      varchar(255) null,
   DOC_DESCRICAO        text,
   DOC_ATIVO            int not null DEFAULT '1',
   PEF_CODIGOCADASTRANTE INT(11) NOT NULL,
   PEF_CODIGOVALIDADOR   INT(11) NULL,
   DOC_DTCADASTRO        DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   DOC_DTVALIDACAO       DATETIME NULL,
   UNI_CODIGO            INT NOT NULL,
   STD_CODIGO            INT NOT NULL,
   primary key (DOC_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: EMAIL                                                 */
/*==============================================================*/
create table EMAIL
(
   EMA_CODIGO           int not null auto_increment,
   PEF_CODIGO           int not null,
   EMA_DESCRICAO        varchar(150) not null,
   EMA_ATIVO            int not null DEFAULT '1',
   primary key (EMA_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: ENDERECO                                              */
/*==============================================================*/
create table ENDERECO
(
   END_CODIGO           int not null auto_increment,
   BAI_CODIGO           int not null,
   CON_CODIGO           int,
   END_COMPLEMENTO      varchar(100),
   primary key (END_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: ENDERECOUNIDADE                                       */
/*==============================================================*/
create table ENDERECOUNIDADE
(
   ENU_CODIGO           int not null auto_increment,
   CON_CODIGO           int not null,
   ENU_DESCRICAO        varchar(255) not null,
   primary key (ENU_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: ENQUETE                                               */
/*==============================================================*/
create table ENQUETE
(
   ENQ_CODIGO           int not null auto_increment,
   ENQ_NOME             varchar(255) not null,
   ENQ_DTINICIO         date not null,
   ENQ_DTFIM            date,
   PEF_CODIGOUSUARIO    int not null,
   ENQ_ATIVO            int not null,
   primary key (ENQ_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: EQUIPAMENTO                                           */
/*==============================================================*/
create table EQUIPAMENTO
(
   EQU_CODIGO           int not null auto_increment,
   TIE_CODIGO           int not null,
   CON_CODIGO           int not null,
   EQU_NOME             varchar(255) not null,
   primary key (EQU_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: FOTOANIMAL                                            */
/*==============================================================*/
create table FOTOANIMAL
(
   FOA_CODIGO           int not null auto_increment,
   ANI_CODIGO           int not null,
   FOA_NOME             varchar(255) not null,
   FOA_CONTENTTYPE      varchar(255) not null,
   FOA_ATIVO            int not null DEFAULT '1',
   primary key (FOA_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: FOTOSCLASSICADOS                                      */
/*==============================================================*/
create table FOTOSCLASSICADOS
(
   FOT_CODIGO           int not null auto_increment,
   ANU_CODIGO           int not null,
   FOT_NOME             varchar(255) not null,
   FOT_DESCRICAO        varchar(255),
   primary key (FOT_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: FOTOORDEMSERVICO                                            */
/*==============================================================*/
create table FOTOORDEMSERVICO
(
   FOS_CODIGO           int not null auto_increment,
   ORD_CODIGO           int not null,
   FOS_NOME             varchar(255) not null,
   FOS_CONTENTTYPE      varchar(255) not null,
   FOS_ATIVO            int not null DEFAULT '1',
   primary key (FOS_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: FUNCAOPESSOAJURIDICA                                  */
/*==============================================================*/
create table FUNCAOPESSOAJURIDICA
(
   FPJ_CODIGO           int not null auto_increment,
   FPJ_NOME             varchar(255) not null,
   FPJ_DESCRICAO        text,
   FPJ_ATIVO            int not null DEFAULT '1',
   PEJ_CODIGO           int not null,
   primary key (FPJ_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: GALERIA                                               */
/*==============================================================*/
create table GALERIA
(
   GAL_CODIGO           int not null auto_increment,
   GAL_TITULO           varchar(255) not null,
   GAL_DESCRICAO        text,
   GAL_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (GAL_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: GALERIA_IMAGEM                                        */
/*==============================================================*/
create table GALERIA_IMAGEM
(
   GAI_CODIGO           int not null auto_increment,
   GAL_CODIGO           int not null,
   IMA_CODIGO           int not null,
   PEF_CODIGOPUBLICA    int not null,
   PEF_CODIGORETIRA     int,
   GAI_DTINICIO         datetime not null,
   GAI_DTFIM            datetime,
   primary key (GAI_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: HISTORICOFINANCEIROCONTASRECEBER                      */
/*==============================================================*/
create table HISTORICOFINANCEIROCONTASRECEBER
(
   HFC_CODIGO           int not null auto_increment,
   UNI_CODIGO           int not null,
   TCR_CODIGO           int not null,
   HFC_DTVENCIMENTO     date not null,
   HFC_DTBAIXA          date,
   HFC_VALORPRINCIPAL   float not null,
   HFC_VALORJUROS       float,
   HFC_OBSERVACAO       text,
   HFC_VALORDESCONTO    float,
   HFC_FUNDORESERVA		float,
   primary key (HFC_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: HISTORICOPORTARIA                                     */
/*==============================================================*/
create table HISTORICOPORTARIA
(
   HIP_CODIGO           int not null auto_increment,
   PFV_CODIGO           int null,
   UNI_CODIGO           int not null,
   PFU_CODIGO           INT(11) NOT NULL,
   PEF_CODIGOCADASTRANTE int not null,
   CON_CODIGO           int not null,
   HIP_DTENTRADA        DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   HIP_DTSAIDA          datetime,
   primary key (HIP_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: HISTORICOORDEMSERVICO                                 */
/*==============================================================*/
create table HISTORICOORDEMSERVICO
(
   HOS_CODIGO           int not null auto_increment,
   ORD_CODIGO           int,
   HOS_DESCRICAO        text null,
   HOS_DTINICIO         datetime not null,
   HOS_DTFIM            datetime,
   OSS_CODIGO           INT(11) NOT NULL,
   PEF_CODIGOEXECUTOR	int null,
   primary key (HOS_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: IMAGEM                                                */
/*==============================================================*/
create table IMAGEM
(
   IMA_CODIGO           int not null auto_increment,
   IMA_NOME             varchar(255) null,
   IMA_CONTENTTYPE      varchar(255) null,
   CON_CODIGO           INT NOT NULL,
   primary key (IMA_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: LOGIN                                                 */
/*==============================================================*/
create table LOGIN
(
   LOG_CODIGO           int not null auto_increment,
   PEF_CODIGO           int not null,
   LOG_DTHORALOGIN      DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   LOG_DTHORALOGOUT     datetime,
   LOG_SESSAO		    varchar(100),
   primary key (LOG_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: MARCA                                                 */
/*==============================================================*/
create table MARCA
(
   MAR_CODIGO           int not null auto_increment,
   MAR_NOME             varchar(255) not null,
   MAR_ATIVO            INT NOT NULL DEFAULT '1',
   TIV_CODIGO           int not null,
   primary key (MAR_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: MODULO                                                */
/*==============================================================*/
create table MODULO
(
   MOU_CODIGO           int not null auto_increment,
   MOU_NOME             varchar(255) not null,
   MOU_ATIVO            int not null DEFAULT '1',
   primary key (MOU_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: MENSAGEM                                              */
/*==============================================================*/
create table MENSAGEM
(
   MEN_CODIGO             int not null auto_increment,
   PEF_CODIGOREMETENTE    int not null,
   PEF_CODIGODESTINATARIO int,
   CON_CODIGO			  int,
   MEN_DTINCLUSAO         datetime not null DEFAULT CURRENT_TIMESTAMP,
   MEN_DTRECEBIMENTO      datetime,
   MEN_OBSERVACAO         text not null,
   MEN_ACAO               varchar(150),
   MEN_ATIVO              int(1) not null DEFAULT '1',
   MEN_ALERTA             INT NOT NULL DEFAULT 0,
   MEN_ASSUNTO            VARCHAR(255) NULL,
   primary key (MEN_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: MODELO                                                */
/*==============================================================*/
create table MODELO
(
   MOD_CODIGO           int not null auto_increment, 
   MAR_CODIGO           int not null,
   MOD_NOME             varchar(255) not null,
   MOD_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (MOD_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: ORDEMSERVICO                                          */
/*==============================================================*/
create table ORDEMSERVICO
(
   ORD_CODIGO           int not null auto_increment,
   TOS_CODIGO           int not null,
   ATE_CODIGO           int null,
   PEF_CODIGO           int not null,
   CON_CODIGO           int not null,
   ORD_OBSERVACAO		text,
   ORD_EMERGENCIA       INT(1) NULL,
   ORD_SERVICOIDENTIFICADO  INT(1) NULL,
   PEJ_CODIGO			int not null,
   ORD_NRPROTOCOLO		int,
   ORD_VALORMATERIAL    DECIMAL(9,2),
   ORD_VALORMAODEOBRA   DECIMAL(9,2) NULL,
   primary key (ORD_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: ORDEMSERVICOSTATUS                                    */
/*==============================================================*/
create table ORDEMSERVICOSTATUS
(
   OSS_CODIGO           int not null auto_increment,
   OSS_NOME             varchar(100) not null,
   OSS_ATIVO            int not null,
   primary key (OSS_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: ORCAMENTO                                             */
/*==============================================================*/
create table ORCAMENTO
(
   ORC_CODIGO           int not null auto_increment,
   ORD_CODIGO           int,
   ORC_DTORCAMENTO      DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
   ORC_VALORTOTAL       float,
   ORC_ATIVO            INT NULL DEFAULT '1',
   ORC_NOMEANEXO		varchar(255) not null,
   ORC_CONTENTTYPE		varchar(255) not null,
   ORC_ACEITE           INT NULL DEFAULT '0',
   ORC_SINDICOUPLOAD    INT NULL DEFAULT 0,
   primary key (ORC_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PACOTESERVICO                                         */
/*==============================================================*/
create table PACOTESERVICO
(
   PCS_CODIGO           int not null auto_increment,
   PCS_NOME             varchar(255) not null,
   PCS_DESCRICAO        text,
   PCS_VALOR            float,
   PCS_QTDVISITAS       int,
   PCS_ATIVO            INT NULL DEFAULT '1',
   PEJ_CODIGO INT(11)   NOT NULL,
   PCS_VALORVISITAEXCEDENTE  FLOAT NOT NULL,
   primary key (PCS_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PERGUNTAAVALIACAO                                     */
/*==============================================================*/
create table PERGUNTAAVALIACAO
(
   PAV_CODIGO           int not null auto_increment,
   PAV_NOME             varchar(255) not null,
   PAV_ATIVO            int not null DEFAULT '1',
   PAV_OBJETIVA         INT NULL,
   primary key (PAV_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PESSOAFISICA_PROFISSAO                                */
/*==============================================================*/
create table PESSOAFISICA_PROFISSAO
(
   PFP_CODIGO           int not null auto_increment,
   PRO_CODIGO           int not null,
   PEF_CODIGO           int not null,
   PFP_ATIVO            int not null DEFAULT '1',
   PFP_DTCADASTRO       DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   primary key (PFP_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PESSOAJURIDICA_CONDOMINIO                             */
/*==============================================================*/
create table PESSOAJURIDICA_CONDOMINIO
(
   PJC_CODIGO           int not null auto_increment,
   PEJ_CODIGO           int not null,
   CON_CODIGO           int not null,
   PEF_CODIGORESPONSAVEL int not null,
   PJC_DTINICIO         DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PJC_DTFIM            datetime,
   PJC_ADIMPLENTE       INT NULL DEFAULT 0,
   primary key (PJC_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PROFISSAO                                             */
/*==============================================================*/
create table PROFISSAO
(
   PRO_CODIGO           int not null auto_increment,
   PRO_NOME             varchar(255) not null,
   PRO_ATIVO            int not null DEFAULT '1',
   primary key (PRO_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PARENTESCO                                            */
/*==============================================================*/
create table PARENTESCO
(
   PAR_CODIGO           int not null auto_increment,
   PAR_NOME             varchar(100) not null,
   PAR_ATIVO            int not null DEFAULT '1',
   primary key (PAR_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PERFIL                                                */
/*==============================================================*/
create table PERFIL
(
   PER_CODIGO           int not null auto_increment,
   PER_NOME             varchar(150) not null,
   PER_DESCRICAO        varchar(255),
   PER_ATIVO            int not null,
   primary key (PER_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PERFIL_CONDOMINIO                                     */
/*==============================================================*/
create table PERFIL_CONDOMINIO
(
   PCO_CODIGO           int not null auto_increment,
   PER_CODIGO           int not null,
   CON_CODIGO           int not null,
   primary key (PCO_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PERFIL_CONDOMINIO_RECURSO                             */
/*==============================================================*/
create table PERFIL_CONDOMINIO_RECURSO
(
   PCR_CODIGO           int not null auto_increment,
   REC_CODIGO           int not null,
   PCO_CODIGO           int not null,
   primary key (PCR_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PERGUNTAENQUETE                                       */
/*==============================================================*/
create table PERGUNTAENQUETE
(
   PEE_CODIGO           int not null auto_increment,
   ENQ_CODIGO           int not null,
   PEE_DESCRICAO        text not null,
   PEE_ATIVO            int not null,
   primary key (PEE_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PESSOAFISICA                                          */
/*==============================================================*/
create table PESSOAFISICA
(
   PEF_CODIGO           int not null auto_increment,
   PEF_CPF              char(11) not null,
   SEX_CODIGO           int not null,
   PEF_CODIGOPAI        INT(11) NULL,
   PEF_NOMEMAE          varchar(250),
   PEF_NOMEPAI          varchar(250),
   PEF_NOMEFOTO         varchar(255),
   PEF_CONTENT_TYPE     varchar(50),
   PEF_NOME             varchar(255) not null,
   PEF_DTNASCIMENTO     date,
   PEF_DTCADASTRO		DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PEF_ATIVO            int not null DEFAULT '1',
   PEF_RG               VARCHAR(15) NULL,
   PEF_RGORGAOEXPEDIDOR VARCHAR(10) NULL,
   primary key (PEF_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: PESSOAJURIDICA_PESSOAFISICA                           */
/*==============================================================*/
create table PESSOAJURIDICA_PESSOAFISICA
(
   PJP_CODIGO           int not null auto_increment,
   PEF_CODIGO           int not null,
   PEJ_CODIGO           int not null,
   PJP_DTINICIO         datetime not null,
   PJP_DTFIM            datetime,
   PJP_ATIVO            int not null DEFAULT '1',
   FPJ_CODIGO           INT(11) NOT NULL,
   primary key (PJP_CODIGO))engine = InnoDB;
    
/*==============================================================*/
/* Table: PESSOAFISICA_ORDEMSERVICO                             */
/*==============================================================*/
create table PESSOAFISICA_ORDEMSERVICO
(
   PFO_CODIGO           int not null auto_increment,
   ORD_CODIGO           int not null,
   PEF_CODIGO           int not null,
   PFO_DTINCLUSAO		DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
   PFO_OBSERVACAO		TEXT,
   PFO_VISITA           INT NULL DEFAULT 0,
   PFO_CIENTE           INT NULL DEFAULT 0,
   primary key (PFO_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PESSOAFISICA_CONDOMINIO                               */
/*==============================================================*/
create table PESSOAFISICA_CONDOMINIO
(
   PFC_CODIGO           int not null auto_increment,
   PEF_CODIGO           int not null,
   CON_CODIGO           int not null,
   PFC_DTINICIO         date not null,
   PFC_DTFIM            date,
   TIF_CODIGO           INT NULL,
   REG_CODIGO           INT(11) NULL,
   PFC_VALORSALARIO		float,
   TPC_CODIGO	        INT(11) NULL,
   primary key (PFC_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PESSOAFISICA_PERFIL_CONDOMINIO                        */
/*==============================================================*/
create table PESSOAFISICA_PERFIL_CONDOMINIO
(
   PPC_CODIGO           int not null auto_increment,
   PEF_CODIGO           int not null,
   PCO_CODIGO           int not null,
   primary key (PPC_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PESSOAFISICA_PERGUNTA                                 */
/*==============================================================*/
create table PESSOAFISICA_PERGUNTA
(
   PEP_CODIGO           int not null auto_increment,
   PEE_CODIGO           int not null,
   PEF_CODIGO           int not null,
   primary key (PEP_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PESSOAFISICA_UNIDADE                                  */
/*==============================================================*/
create table PESSOAFISICA_UNIDADE
(
   PFU_CODIGO            int not null auto_increment,
   PEF_CODIGO            int not null,
   UNI_CODIGO            int not null,
   PFU_DTINICIO          date not null,
   PFU_DTFIM             date,
   TPF_CODIGO			 int not null,
   PAR_CODIGO			 int,
   PFU_TIPOMORADOR       varchar(100),
   PEF_CODIGOCADASTRANTE INT NOT NULL,
   PFU_DTCADASTRO        DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
   TIF_CODIGO            INT NULL,
   PFU_AVISARMORADOR     INT(1) NULL,
   PFU_PARTEIMOVEL       CHAR(6) NULL,
   PFU_ATIVO             INT NOT NULL DEFAULT '1',
   primary key (PFU_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: PESSOAFISICA_UNIDADE_DIASEMANA                        */
/*==============================================================*/
create table PESSOAFISICA_UNIDADE_DIASEMANA
(
   PFD_CODIGO           int not null auto_increment,
   DSS_CODIGO           int not null,
   PFU_CODIGO           int not null,
   TUR_CODIGO           INT NULL,
   primary key (PFD_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: PESSOAFISICA_VEICULO                                  */
/*==============================================================*/
create table PESSOAFISICA_VEICULO
(
   PFV_CODIGO           int not null auto_increment,
   VEI_CODIGO           int not null,
   PEF_CODIGO           int not null,
   TPF_CODIGO           int,
   PFV_DTINICIO         DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PFV_DTFIM            datetime,
   PFU_CODIGO           INT NULL,
   primary key (PFV_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: PESSOAJURIDICA                                        */
/*==============================================================*/
create table PESSOAJURIDICA
(
   PEJ_CODIGO           int not null auto_increment,
   PEJ_NOMEFANTASIA     varchar(255),
   PEJ_CNPJ             char(14),
   PEJ_RAZAOSOCIAL      varchar(255),
   AAT_CODIGO           INT(11) NULL,
   PEJ_ATIVO INT NULL DEFAULT '1',
   primary key (PEJ_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: RACA                                                  */
/*==============================================================*/
create table RACA
(
   RAC_CODIGO           int not null auto_increment,
   RAC_NOME             varchar(100) not null,
   RAC_ATIVO            int not null DEFAULT '1',
   TIA_CODIGO           int not null,
   primary key (RAC_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: RECURSO                                               */
/*==============================================================*/
create table RECURSO
(
   REC_CODIGO           int not null auto_increment,
   REC_NOME             varchar(150) not null,
   REC_DESCRICAO        varchar(255),
   REC_MORADOR			int,
   REC_ATIVO            int not null,
   primary key (REC_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: RECURSO_PESSOAFISICA_UNIDADE                          */
/*==============================================================*/
create table RECURSO_PESSOAFISICA_UNIDADE
(
   RPU_CODIGO           int not null auto_increment,
   REC_CODIGO           int not null ,
   PFU_CODIGO           int not null,

   primary key (RPU_CODIGO))engine = InnoDB;   
   
CREATE TABLE REGIMETRABALHO
(
  REG_CODIGO 	int NOT NULL AUTO_INCREMENT,
  REG_NOME 		varchar(150) NOT NULL,
  REG_NRHORASSEMANA int,
  REG_ATIVO 	int(1) not null DEFAULT '1',
  PRIMARY KEY (REG_CODIGO)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*==============================================================*/
/* Table: RECURSO_PESSOAJURIDICA_PESSOAFISICA                          */
/*==============================================================*/
create table RECURSO_PESSOAJURIDICA_PESSOAFISICA
(
   RJF_CODIGO           int not null auto_increment,
   REC_CODIGO           int not null ,
   PJP_CODIGO           int not null,
   primary key (RJF_CODIGO)) engine = InnoDB;

/*==============================================================*/
/* Table: RESERVA                                               */
/*==============================================================*/
create table RESERVA
(
   RES_CODIGO           int not null auto_increment,
   PEF_CODIGO           int not null,
   ACF_CODIGO           int not null,
   RES_DTCADASTRO       DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   primary key (RES_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: SENHAS                                                */
/*==============================================================*/
create table SENHAS
(
   SEN_CODIGO           int not null auto_increment,
   PEF_CODIGO           int not null,
   SEN_SENHA            varchar(150) not null,
   primary key (SEN_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: SEXO                                                  */
/*==============================================================*/
create table SEXO
(
   SEX_CODIGO           int not null auto_increment,
   SEX_NOME             varchar(20) not null,
   SEX_SIGLA			char(1) not null,
   primary key (SEX_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: STATUSATENDIMENTO                                     */
/*==============================================================*/
create table STATUSATENDIMENTO
(
   STA_CODIGO           int not null auto_increment,
   STA_NOME             varchar(50) not null,
   STA_ATIVO            int not null,
   primary key (STA_CODIGO))engine = InnoDB;  
   
/*==============================================================*/
/* Table: STATUSDOCUMENTO                                       */
/*==============================================================*/
create table STATUSDOCUMENTO
(
   STD_CODIGO           int not null auto_increment,
   STD_NOME 			varchar(150) not null,
   STD_ATIVO			int not null DEFAULT '1',
   primary key (STD_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: TELEFONE                                              */
/*==============================================================*/
create table TELEFONE
(
   TEL_CODIGO           int not null auto_increment,
   PEF_CODIGO           int not null,
   TEL_NR               varchar(10) not null,
   TEL_DDD              char(3) not null,
   TEL_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (TEL_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: TIPOATENDIMENTO                                       */
/*==============================================================*/
create table TIPOATENDIMENTO
(
   TAT_CODIGO           int not null auto_increment,
   TAT_NOME             varchar(150) not null,
   TAT_SUBORDINACAO		int,
   TAT_ATIVO            int not null,
   TAT_PRIORIDADE       int,
   
   primary key (TAT_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: TIPOFUNCIONARIO                                       */
/*==============================================================*/
create table TIPOFUNCIONARIO
(
   TIF_CODIGO           int not null auto_increment,
   TIF_NOME             varchar(100) not null,
   TIF_ATIVO            int not null DEFAULT '1',
   primary key (TIF_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: TIPOORDEMSERVICO                                      */
/*==============================================================*/
create table TIPOORDEMSERVICO
(
   TOS_CODIGO           int not null auto_increment,
   TOS_NOME             varchar(150) not null,
   TOS_ATIVO            int not null,
   TOS_PRIORIDADE		int DEFAULT '0',
   TOS_SUBORDINACAO		int,
   primary key (TOS_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: TIPOANIMAL                                           */
/*==============================================================*/
create table TIPOANIMAL
(
   TIA_CODIGO           int not null auto_increment,
   TIA_NOME             varchar(255),
   TIA_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (TIA_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: TIPOANUNCIO                                           */
/*==============================================================*/
create table TIPOANUNCIO
(
   TIN_CODIGO           int not null auto_increment,
   TIN_NOME             varchar(50) not null,
   TIN_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (TIN_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: TIPOCONTASRECEBER                                     */
/*==============================================================*/
create table TIPOCONTASRECEBER
(
   TCR_CODIGO           int not null auto_increment,
   TCR_NOME             varchar(150) not null,
   TCR_ATIVO            int not null,
   primary key (TCR_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: TIPOCONTEUDO                                          */
/*==============================================================*/
create table TIPOCONTEUDO
(
   TIC_CODIGO           int not null auto_increment,
   TIC_NOME             varchar(150) not null,
   TIC_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (TIC_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: TIPOCORRESPONDENCIA                                   */
/*==============================================================*/
create table TIPOCORRESPONDENCIA
(
   TPC_CODIGO           int not null auto_increment,
   TPC_NOME             varchar(150) not null,
   TPC_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (TPC_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: TIPODOCUMENTO                                         */
/*==============================================================*/
create table TIPODOCUMENTO
(
   TID_CODIGO           int not null auto_increment,
   TID_NOME             varchar(255) not null,
   TID_ATIVO            int not null DEFAULT '1',
   primary key (TID_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: TIPOENDERECOUNIDADE                                   */
/*==============================================================*/
create table TIPOENDERECOUNIDADE
(
   TEU_CODIGO           int not null auto_increment,
   CON_CODIGO           int not null,
   TEU_DESCRICAO        varchar(255) not null,
   TEU_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (TEU_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: TIPOEQUIPAMENTO                                       */
/*==============================================================*/
create table TIPOEQUIPAMENTO
(
   TIE_CODIGO           int not null auto_increment,
   TIE_NOME             varchar(255) not null,
   primary key (TIE_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: TIPOPESSOAFISICACONDOMINIO                                       */
/*==============================================================*/
   
   CREATE TABLE TIPOPESSOAFISICACONDOMINIO (
   TPC_CODIGO 		 INT NOT NULL AUTO_INCREMENT,
   TPC_NOME 		 VARCHAR(45) NOT NULL,
   TPC_ATIVO		 INT NOT NULL DEFAULT '1',
PRIMARY KEY (TPC_CODIGO))engine = InnoDB;


/*==============================================================*/
/* Table: TIPOPESSOAFISICAUNIDADE                               */
/*==============================================================*/
create table TIPOPESSOAFISICAUNIDADE
(
   TPF_CODIGO           int not null auto_increment,
   TPF_NOME             varchar(150) not null,
   TPF_ATIVO            int not null DEFAULT '1',
   primary key (TPF_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: TIPOUNIDADE                                           */
/*==============================================================*/
create table TIPOUNIDADE
(
   TPU_CODIGO           int not null auto_increment,
   TPU_NOME             varchar(150) not null,
   TPU_ATIVO            INT NOT NULL DEFAULT '1',
   TPU_SIGLA            VARCHAR(20) NULL,
   primary key (TPU_CODIGO))engine = InnoDB;
   

/*==============================================================*/
/* Table: TIPOVEICULO                                           */
/*==============================================================*/
create table TIPOVEICULO
(
   TIV_CODIGO           int not null auto_increment,
   TIV_NOME             varchar(100) not null,
   TIV_ATIVO            INT NOT NULL DEFAULT '1',
   primary key (TIV_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: TURNO                                                 */
/*==============================================================*/
create table TURNO
(
   TUR_CODIGO           int not null auto_increment,
   TUR_NOME             varchar(50) not null,
   TUR_ATIVO            int not null DEFAULT '1',
   primary key (TUR_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: UFE                                                   */
/*==============================================================*/
create table UFE
(
   UFE_CODIGO           int not null auto_increment,
   UFE_NOME             varchar(255) not null,
   UFE_SIGLA            varchar(2),
   UFE_ATIVO            int not null,
   primary key (UFE_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table: UNIDADE                                               */
/*==============================================================*/
create table UNIDADE
(
   UNI_CODIGO           				int not null auto_increment,
   TPU_CODIGO           				int not null,
   ENU_CODIGO           				int not null,
   TEU_CODIGO           				int,
   UNI_UNIDADEHABITACIONAL varchar(100) not null,
   CON_CODIGO                           INT NULL,
   primary key (UNI_CODIGO))engine = InnoDB;
   
/*==============================================================*/
/* Table: VAGAS                                                 */
/*==============================================================*/
create table VAGAS
(
   VAG_CODIGO           int not null auto_increment,
   UNI_CODIGO           int,
   VAG_NUMERO           varchar(5) not null,
   primary key (VAG_CODIGO))engine = InnoDB;

/*==============================================================*/
/* Table:                                                */
/*==============================================================*/
create table VEICULO
(
   VEI_CODIGO           int not null auto_increment,
   MOD_CODIGO           int not null,
   PEF_CODIGOCADASTRANTE int,
   CRR_CODIGO           int,
   VEI_PLACA            char(8) not null,
   VEI_ANO				int,
   VEI_ANOMODELO		int,
   VEI_DTCADASTRO		DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PES_CODIGOUFPLACA    INT(11) NULL,
   primary key (VEI_CODIGO))engine = InnoDB;
   
alter table ANEXOORDEMSERVICO add constraint FK_RELATIONSHIP_200 foreign key (ORD_CODIGO)
  references ORDEMSERVICO (ORD_CODIGO) on delete restrict on update restrict;
   
alter table ANEXOCONDOMINIO add constraint FK_RELATIONSHIP_182 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;
   
alter table ANIMAL add constraint FK_RELATIONSHIP_133 foreign key (COA_CODIGO)
      references CORANIMAL (COA_CODIGO) on delete restrict on update restrict;
	  
alter table ANIMAL add constraint FK_RELATIONSHIP_134 foreign key (RAC_CODIGO)
      references RACA (RAC_CODIGO) on delete restrict on update restrict;
	  
alter table ANIMAL add constraint FK_RELATIONSHIP_21 foreign key (UNI_CODIGO)
      references UNIDADE (UNI_CODIGO) on delete restrict on update restrict;
	  
alter table AVALIACAOORDEMSERVICO add constraint FK_RELATIONSHIP_171 foreign key (ORD_CODIGO)
      references ORDEMSERVICO (ORD_CODIGO) on delete restrict on update restrict;

alter table AVALIACAOORDEMSERVICO add constraint FK_RELATIONSHIP_172 foreign key (PEF_CODIGOAVALIADOR)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table AVALIACAOORDEMSERVICO add constraint FK_RELATIONSHIP_173 foreign key (PEF_CODIGOAVALIADO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table AVALIACAOOS_PERGUNTAAVALIACAO add constraint FK_RELATIONSHIP_169 foreign key (AOS_CODIGO)
      references AVALIACAOORDEMSERVICO (AOS_CODIGO) on delete restrict on update restrict;

alter table AVALIACAOOS_PERGUNTAAVALIACAO add constraint FK_RELATIONSHIP_170 foreign key (PAV_CODIGO)
      references PERGUNTAAVALIACAO (PAV_CODIGO) on delete restrict on update restrict;
   
alter table ATENDIMENTO add constraint FK_RELATIONSHIP_81 foreign key (TAT_CODIGO)
      references TIPOATENDIMENTO (TAT_CODIGO) on delete restrict on update restrict;

alter table ATENDIMENTO add constraint FK_RELATIONSHIP_83 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table ATENDIMENTO add constraint FK_RELATIONSHIP_135 foreign key (UNI_CODIGO)
      references UNIDADE (UNI_CODIGO) on delete restrict on update restrict;
	  
alter table ATENDIMENTO add constraint FK_RELATIONSHIP_95 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table ATENDIMENTORESPOSTA add constraint FK_RELATIONSHIP_86 foreign key (ATE_CODIGO)
      references ATENDIMENTO (ATE_CODIGO) on delete restrict on update restrict;

alter table ATENDIMENTORESPOSTA add constraint FK_RELATIONSHIP_87 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table ATENDIMENTORESPOSTA add constraint FK_RELATIONSHIP_89 foreign key (ARE_SUBORDINACAO)
      references ATENDIMENTORESPOSTA (ARE_CODIGO) on delete restrict on update restrict;

alter table ATENDIMENTO_STATUS add constraint FK_RELATIONSHIP_84 foreign key (STA_CODIGO)
      references STATUSATENDIMENTO (STA_CODIGO) on delete restrict on update restrict;

alter table ATENDIMENTO_STATUS add constraint FK_RELATIONSHIP_85 foreign key (ATE_CODIGO)
      references ATENDIMENTO (ATE_CODIGO) on delete restrict on update restrict;

alter table ANUNCIO add constraint FK_RELATIONSHIP_44 foreign key (TPU_CODIGO)
      references TIPOUNIDADE (TPU_CODIGO) on delete restrict on update restrict;

alter table ANUNCIO add constraint FK_RELATIONSHIP_45 foreign key (TIN_CODIGO)
      references TIPOANUNCIO (TIN_CODIGO) on delete restrict on update restrict;

alter table ANUNCIO add constraint FK_RELATIONSHIP_47 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table ANUNCIO add constraint FK_RELATIONSHIP_90 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;	

alter table AREACOMUMFUNCIONAMENTO add constraint FK_RELATIONSHIP_41 foreign key (ARC_CODIGO)
      references AREACOMUM (ARC_CODIGO) on delete restrict on update restrict;

alter table AUDITORIA add constraint FK_RELATIONSHIP_50 foreign key (PEF_CODIGOAUTOR)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table AUDITORIA add constraint FK_RELATIONSHIP_51 foreign key (PEF_CODIGOAFETADO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table BAIRRO add constraint FK_RELATIONSHIP_64 foreign key (CID_CODIGO)
      references CIDADE (CID_CODIGO) on delete restrict on update restrict;

alter table CIDADE add constraint FK_RELATIONSHIP_63 foreign key (UFE_CODIGO)
      references UFE (UFE_CODIGO) on delete restrict on update restrict;
	  
alter table CONDOMINIO add constraint FK_RELATIONSHIP_112 foreign key (UFE_CODIGO)
      references UFE (UFE_CODIGO) on delete restrict on update restrict;
	  
alter table CONDOMINIO_MODULO add constraint FK_RELATIONSHIP_165 foreign key (MOU_CODIGO)
      references MODULO (MOU_CODIGO) on delete restrict on update restrict;

alter table CONDOMINIO_MODULO add constraint FK_RELATIONSHIP_166 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table CONDOMINIO add constraint FK_RELATIONSHIP_4 foreign key (PEJ_CODIGO)
      references PESSOAJURIDICA (PEJ_CODIGO) on delete restrict on update restrict;
	  
alter table CONDOMINIO_PACOTESERVICO add constraint FK_RELATIONSHIP_167 foreign key (PCS_CODIGO)
      references PACOTESERVICO (PCS_CODIGO) on delete restrict on update restrict;
	  
alter table CONDOMINIO_PACOTESERVICO add constraint FK_RELATIONSHIP_168 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table CONTEUDO add constraint FK_RELATIONSHIP_30 foreign key (TIC_CODIGO)
      references TIPOCONTEUDO (TIC_CODIGO) on delete restrict on update restrict;

alter table CONTEUDO add constraint FK_RELATIONSHIP_35 foreign key (PEF_CODIGOUSUARIOPUBLICA)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table CONTEUDO add constraint FK_RELATIONSHIP_36 foreign key (PEF_CODIGOUSUARIORETIRA)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table CONTEUDO add constraint FK_RELATIONSHIP_91 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table CONTEUDO_IMAGEM add constraint FK_RELATIONSHIP_31 foreign key (COT_CODIGO)
      references CONTEUDO (COT_CODIGO) on delete restrict on update restrict;

alter table CONTEUDO_IMAGEM add constraint FK_RELATIONSHIP_32 foreign key (IMA_CODIGO)
      references IMAGEM (IMA_CODIGO) on delete restrict on update restrict;

alter table CORRESPONDENCIA add constraint FK_RELATIONSHIP_14 foreign key (UNI_CODIGO)
      references UNIDADE (UNI_CODIGO) on delete restrict on update restrict;

alter table CORRESPONDENCIA add constraint FK_RELATIONSHIP_15 foreign key (TPC_CODIGO)
      references TIPOCORRESPONDENCIA (TPC_CODIGO) on delete restrict on update restrict;

alter table CORRESPONDENCIA add constraint FK_RELATIONSHIP_16 foreign key (PEJ_CODIGOFUNCIONARIO)
      references PESSOAJURIDICA (PEJ_CODIGO) on delete restrict on update restrict;
	  
alter table DOCUMENTO add constraint FK_RELATIONSHIP_115 foreign key (TID_CODIGO)
      references TIPODOCUMENTO (TID_CODIGO) on delete restrict on update restrict;

alter table DOCUMENTO add constraint FK_RELATIONSHIP_116 foreign key (PFU_CODIGO)
      references PESSOAFISICA_UNIDADE (PFU_CODIGO) on delete restrict on update restrict;
	  
alter table DOCUMENTO add constraint FK_RELATIONSHIP_120 foreign key (PEF_CODIGOCADASTRANTE)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table DOCUMENTO add constraint FK_RELATIONSHIP_121 foreign key (PEF_CODIGOVALIDADOR)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table DOCUMENTO add constraint FK_RELATIONSHIP_122 foreign key (UNI_CODIGO)
      references UNIDADE (UNI_CODIGO) on delete restrict on update restrict;
	  
alter table DOCUMENTO add constraint FK_RELATIONSHIP_124 foreign key (STD_CODIGO)
      references STATUSDOCUMENTO (STD_CODIGO) on delete restrict on update restrict;
	  
alter table EMAIL add constraint FK_RELATIONSHIP_78 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
ALTER TABLE EMAIL
	ADD UNIQUE INDEX `UK_EMA_DESCRICAO` (EMA_DESCRICAO);

alter table ENDERECO add constraint FK_RELATIONSHIP_65 foreign key (BAI_CODIGO)
      references BAIRRO (BAI_CODIGO) on delete restrict on update restrict;

alter table ENDERECO add constraint FK_RELATIONSHIP_67 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table ENDERECOUNIDADE add constraint FK_RELATIONSHIP_60 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table EQUIPAMENTO add constraint FK_RELATIONSHIP_23 foreign key (TIE_CODIGO)
      references TIPOEQUIPAMENTO (TIE_CODIGO) on delete restrict on update restrict;

alter table EQUIPAMENTO add constraint FK_RELATIONSHIP_79 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;
	  
alter table FOTOANIMAL add constraint FK_RELATIONSHIP_140 foreign key (ANI_CODIGO)
      references ANIMAL (ANI_CODIGO) on delete restrict on update restrict;
	  
alter table FOTOORDEMSERVICO add constraint FK_RELATIONSHIP_184 foreign key (ORD_CODIGO)
      references ORDEMSERVICO (ORD_CODIGO) on delete restrict on update restrict;

alter table FOTOSCLASSICADOS add constraint FK_RELATIONSHIP_46 foreign key (ANU_CODIGO)
      references ANUNCIO (ANU_CODIGO) on delete restrict on update restrict;
	  
alter table FUNCAOPESSOAJURIDICA add constraint FK_RELATIONSHIP_185 foreign key (PEJ_CODIGO)
      references PESSOAJURIDICA (PEJ_CODIGO) on delete restrict on update restrict;

alter table GALERIA_IMAGEM add constraint FK_RELATIONSHIP_33 foreign key (GAL_CODIGO)
      references GALERIA (GAL_CODIGO) on delete restrict on update restrict;

alter table GALERIA_IMAGEM add constraint FK_RELATIONSHIP_34 foreign key (IMA_CODIGO)
      references IMAGEM (IMA_CODIGO) on delete restrict on update restrict;

alter table GALERIA_IMAGEM add constraint FK_RELATIONSHIP_39 foreign key (PEF_CODIGOPUBLICA)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table GALERIA_IMAGEM add constraint FK_RELATIONSHIP_40 foreign key (PEF_CODIGORETIRA)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table HISTORICOFINANCEIROCONTASRECEBER add constraint FK_RELATIONSHIP_68 foreign key (TCR_CODIGO)
      references TIPOCONTASRECEBER (TCR_CODIGO) on delete restrict on update restrict;

alter table HISTORICOFINANCEIROCONTASRECEBER add constraint FK_RELATIONSHIP_70 foreign key (UNI_CODIGO)
      references UNIDADE (UNI_CODIGO) on delete restrict on update restrict;
	  
alter table HISTORICOORDEMSERVICO add constraint FK_RELATIONSHIP_93 foreign key (ORD_CODIGO)
      references ORDEMSERVICO (ORD_CODIGO) on delete restrict on update restrict;

alter table HISTORICOORDEMSERVICO add constraint FK_RELATIONSHIP_94 foreign key (OSS_CODIGO)
      references ORDEMSERVICOSTATUS (OSS_CODIGO) on delete restrict on update restrict;
	  
alter table HISTORICOORDEMSERVICO add constraint FK_RELATIONSHIP_179 foreign key (PEF_CODIGOEXECUTOR)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table HISTORICOPORTARIA add constraint FK_RELATIONSHIP_100 foreign key (PFV_CODIGO)
      references PESSOAFISICA_VEICULO (PFV_CODIGO) on delete restrict on update restrict;

alter table HISTORICOPORTARIA add constraint FK_RELATIONSHIP_102 foreign key (UNI_CODIGO)
      references UNIDADE (UNI_CODIGO) on delete restrict on update restrict;

alter table HISTORICOPORTARIA add constraint FK_RELATIONSHIP_105 foreign key (PEF_CODIGOCADASTRANTE)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table HISTORICOPORTARIA add constraint FK_RELATIONSHIP_108 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;
	  
ALTER TABLE HISTORICOPORTARIA ADD CONSTRAINT FK_RELATIONSHIP_132  FOREIGN KEY (PFU_CODIGO)
	  REFERENCES PESSOAFISICA_UNIDADE (PFU_CODIGO) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  
alter table IMAGEM add constraint FK_RELATIONSHIP_37 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;
	  
alter table LOGIN add constraint FK_RELATIONSHIP_123 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
ALTER TABLE MARCA ADD CONSTRAINT FK_RELATIONSHIP_75  FOREIGN KEY (TIV_CODIGO)
      REFERENCES TIPOVEICULO (TIV_CODIGO)  ON DELETE NO ACTION  ON UPDATE NO ACTION;
	 	  
alter table MENSAGEM add constraint FK_RELATIONSHIP_143 foreign key (PEF_CODIGOREMETENTE)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table MENSAGEM add constraint FK_RELATIONSHIP_144 foreign key (PEF_CODIGODESTINATARIO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table MENSAGEM add constraint FK_RELATIONSHIP_145 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table MODELO add constraint FK_RELATIONSHIP_17 foreign key (MAR_CODIGO)
      references MARCA (MAR_CODIGO) on delete restrict on update restrict;
	  
alter table ORCAMENTO add constraint FK_RELATIONSHIP_157 foreign key (ORD_CODIGO)
      references ORDEMSERVICO (ORD_CODIGO) on delete restrict on update restrict;
	  
alter table ORDEMSERVICO add constraint FK_RELATIONSHIP_80 foreign key (TOS_CODIGO)
      references TIPOORDEMSERVICO (TOS_CODIGO) on delete restrict on update restrict;

alter table ORDEMSERVICO add constraint FK_RELATIONSHIP_82 foreign key (ATE_CODIGO)
      references ATENDIMENTO (ATE_CODIGO) on delete restrict on update restrict;

alter table ORDEMSERVICO add constraint FK_RELATIONSHIP_88 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table ORDEMSERVICO add constraint FK_RELATIONSHIP_96 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;
	  
alter table ORDEMSERVICO add constraint FK_RELATIONSHIP_189 foreign key (PEJ_CODIGO)
      references PESSOAJURIDICA (PEJ_CODIGO) on delete restrict on update restrict;
	  
alter table PACOTESERVICO add constraint FK_RELATIONSHIP_174 foreign key (PEJ_CODIGO)
      references PESSOAJURIDICA (PEJ_CODIGO) on delete restrict on update restrict;  
	  
alter table PERFIL_CONDOMINIO add constraint FK_RELATIONSHIP_53 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table PERFIL_CONDOMINIO add constraint FK_RELATIONSHIP_7 foreign key (PER_CODIGO)
      references PERFIL (PER_CODIGO) on delete restrict on update restrict;

alter table PERFIL_CONDOMINIO_RECURSO add constraint FK_RELATIONSHIP_72 foreign key (REC_CODIGO)
      references RECURSO (REC_CODIGO) on delete restrict on update restrict;

alter table PERFIL_CONDOMINIO_RECURSO add constraint FK_RELATIONSHIP_73 foreign key (PCO_CODIGO)
      references PERFIL_CONDOMINIO (PCO_CODIGO) on delete restrict on update restrict;

alter table PERGUNTAENQUETE add constraint FK_RELATIONSHIP_27 foreign key (ENQ_CODIGO)
      references ENQUETE (ENQ_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAJURIDICA add constraint FK_RELATIONSHIP_175 foreign key (AAT_CODIGO)
      references AREAATUACAO (AAT_CODIGO) on delete restrict on update restrict;	  	  
	  
alter table PESSOAFISICA add constraint FK_RELATIONSHIP_69 foreign key (SEX_CODIGO)
      references SEXO (SEX_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_CONDOMINIO add constraint FK_RELATIONSHIP_5 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_CONDOMINIO add constraint FK_RELATIONSHIP_6 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_CONDOMINIO add constraint FK_RELATIONSHIP_138 foreign key (TIF_CODIGO)
      references TIPOFUNCIONARIO (TIF_CODIGO) on delete restrict on update restrict;
	  
ALTER TABLE PESSOAFISICA_CONDOMINIO ADD CONSTRAINT FK_RELATIONSHIP_146   FOREIGN KEY (REG_CODIGO)
      REFERENCES REGIMETRABALHO (REG_CODIGO) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  
alter table PESSOAFISICA_CONDOMINIO add constraint FK_RELATIONSHIP_176 foreign key (TPC_CODIGO)
   references TIPOPESSOAFISICACONDOMINIO (TPC_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_PERFIL_CONDOMINIO add constraint FK_RELATIONSHIP_48 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_PERFIL_CONDOMINIO add constraint FK_RELATIONSHIP_71 foreign key (PCO_CODIGO)
      references PERFIL_CONDOMINIO (PCO_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_PERGUNTA add constraint FK_RELATIONSHIP_28 foreign key (PEE_CODIGO)
      references PERGUNTAENQUETE (PEE_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_PERGUNTA add constraint FK_RELATIONSHIP_29 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAJURIDICA_PESSOAFISICA add constraint FK_RELATIONSHIP_177 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table PESSOAJURIDICA_PESSOAFISICA add constraint FK_RELATIONSHIP_178 foreign key (PEJ_CODIGO)
      references PESSOAJURIDICA (PEJ_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAJURIDICA_PESSOAFISICA add constraint FK_RELATIONSHIP_186 foreign key (FPJ_CODIGO)
      references FUNCAOPESSOAJURIDICA (FPJ_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAFISICA_ORDEMSERVICO add constraint FK_RELATIONSHIP_187 foreign key (ORD_CODIGO)
      references ORDEMSERVICO (ORD_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_ORDEMSERVICO add constraint FK_RELATIONSHIP_188 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_UNIDADE add constraint FK_RELATIONSHIP_12 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_UNIDADE add constraint FK_RELATIONSHIP_13 foreign key (UNI_CODIGO)
      references UNIDADE (UNI_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_UNIDADE add constraint FK_RELATIONSHIP_130 foreign key (TPF_CODIGO)
      references TIPOPESSOAFISICAUNIDADE (TPF_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAFISICA_UNIDADE add constraint FK_RELATIONSHIP_106 foreign key (PAR_CODIGO)
      references PARENTESCO (PAR_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAFISICA_UNIDADE add constraint FK_RELATIONSHIP_131 foreign key (PEF_CODIGOCADASTRANTE)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAFISICA_UNIDADE add constraint FK_RELATIONSHIP_136 foreign key (TIF_CODIGO)
      references TIPOFUNCIONARIO (TIF_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAFISICA_UNIDADE_DIASEMANA add constraint FK_RELATIONSHIP_118 foreign key (DSS_CODIGO)
      references DIASEMANA (DSS_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_UNIDADE_DIASEMANA add constraint FK_RELATIONSHIP_119 foreign key (PFU_CODIGO)
      references PESSOAFISICA_UNIDADE (PFU_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAFISICA_VEICULO add constraint FK_RELATIONSHIP_107 foreign key (TPF_CODIGO)
      references TIPOPESSOAFISICAUNIDADE (TPF_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_VEICULO add constraint FK_RELATIONSHIP_98 foreign key (VEI_CODIGO)
      references VEICULO (VEI_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_VEICULO add constraint FK_RELATIONSHIP_99 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
ALTER TABLE PESSOAFISICA_VEICULO ADD CONSTRAINT FK_RELATIONSHIP_141 FOREIGN KEY (`PFU_CODIGO`)
      REFERENCES PESSOAFISICA_UNIDADE (PFU_CODIGO) ON DELETE NO ACTION ON UPDATE NO ACTION;
	  
alter table PESSOAFISICA add constraint FK_RELATIONSHIP_117 foreign key (PEF_CODIGOPAI)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAFISICA_UNIDADE_DIASEMANA add constraint FK_RELATIONSHIP_137 foreign key (TUR_CODIGO)
      references TURNO (TUR_CODIGO) on delete restrict on update restrict;
	  
alter table PESSOAFISICA_PROFISSAO add constraint FK_RELATIONSHIP_163 foreign key (PRO_CODIGO)
      references PROFISSAO (PRO_CODIGO) on delete restrict on update restrict;

alter table PESSOAFISICA_PROFISSAO add constraint FK_RELATIONSHIP_164 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table PESSOAJURIDICA_CONDOMINIO add constraint FK_RELATIONSHIP_160 foreign key (PEJ_CODIGO)
      references PESSOAJURIDICA (PEJ_CODIGO) on delete restrict on update restrict;

alter table PESSOAJURIDICA_CONDOMINIO add constraint FK_RELATIONSHIP_161 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table PESSOAJURIDICA_CONDOMINIO add constraint FK_RELATIONSHIP_162 foreign key (PEF_CODIGORESPONSAVEL)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table RACA add constraint FK_RELATIONSHIP_20 foreign key (TIA_CODIGO)
      references TIPOANIMAL (TIA_CODIGO) on delete restrict on update restrict;
      
alter table RECURSO_PESSOAFISICA_UNIDADE add constraint FK_RELATIONSHIP_110 foreign key (REC_CODIGO)
      references RECURSO (REC_CODIGO) on delete restrict on update restrict;

alter table RECURSO_PESSOAFISICA_UNIDADE add constraint FK_RELATIONSHIP_111 foreign key (PFU_CODIGO)
      references PESSOAFISICA_UNIDADE (PFU_CODIGO) on delete restrict on update restrict;
	  
alter table RECURSO_PESSOAJURIDICA_PESSOAFISICA add constraint FK_RELATIONSHIP_181 foreign key (REC_CODIGO)
      references RECURSO (REC_CODIGO) on delete restrict on update restrict;

alter table RECURSO_PESSOAJURIDICA_PESSOAFISICA add constraint FK_RELATIONSHIP_180 foreign key (PJP_CODIGO)
      references PESSOAJURIDICA_PESSOAFISICA (PJP_CODIGO) on delete restrict on update restrict;
	  
alter table RESERVA add constraint FK_RELATIONSHIP_42 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table RESERVA add constraint FK_RELATIONSHIP_43 foreign key (ACF_CODIGO)
      references AREACOMUMFUNCIONAMENTO (ACF_CODIGO) on delete restrict on update restrict;

alter table SENHAS add constraint FK_RELATIONSHIP_66 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;

alter table TELEFONE add constraint FK_RELATIONSHIP_55 foreign key (PEF_CODIGO)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table TIPOENDERECOUNIDADE add constraint FK_RELATIONSHIP_77 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;
	  
alter table TIPOORDEMSERVICO add constraint FK_RELATIONSHIP_183 foreign key (TOS_SUBORDINACAO)
      references TIPOORDEMSERVICO (TOS_CODIGO) on delete restrict on update restrict;
	  
alter table TIPOATENDIMENTO add constraint FK_RELATIONSHIP_92 foreign key (TAT_SUBORDINACAO)
      references TIPOATENDIMENTO (TAT_CODIGO) on delete restrict on update restrict;	

alter table UNIDADE add constraint FK_RELATIONSHIP_11 foreign key (TPU_CODIGO)
      references TIPOUNIDADE (TPU_CODIGO) on delete restrict on update restrict;

alter table UNIDADE add constraint FK_RELATIONSHIP_61 foreign key (ENU_CODIGO)
      references ENDERECOUNIDADE (ENU_CODIGO) on delete restrict on update restrict;

alter table UNIDADE add constraint FK_RELATIONSHIP_62 foreign key (TEU_CODIGO)
      references TIPOENDERECOUNIDADE (TEU_CODIGO) on delete restrict on update restrict;
	  
alter table UNIDADE add constraint FK_RELATIONSHIP_139 foreign key (CON_CODIGO)
      references CONDOMINIO (CON_CODIGO) on delete restrict on update restrict;

alter table VAGAS add constraint FK_RELATIONSHIP_76 foreign key (UNI_CODIGO)
      references UNIDADE (UNI_CODIGO) on delete restrict on update restrict;
	  
alter table VEICULO add constraint FK_RELATIONSHIP_18 foreign key (MOD_CODIGO)
      references MODELO (MOD_CODIGO) on delete restrict on update restrict;

alter table VEICULO add constraint FK_RELATIONSHIP_113 foreign key (PEF_CODIGOCADASTRANTE)
      references PESSOAFISICA (PEF_CODIGO) on delete restrict on update restrict;
	  
alter table VEICULO add constraint FK_RELATIONSHIP_114 foreign key (CRR_CODIGO)
      references COR (CRR_CODIGO) on delete restrict on update restrict;
	  
ALTER TABLE VEICULO ADD CONSTRAINT FK_RELATIONSHIP_142 FOREIGN KEY (PES_CODIGOUFPLACA)
      REFERENCES UFE (UFE_CODIGO) ON DELETE NO ACTION ON UPDATE NO ACTION;	
	  

	  
/*==============================================================*/
/* INSERINDO DADOS                                              */
/*==============================================================*/

INSERT INTO AREAATUACAO (`AAT_NOME`) VALUES ('TECNOLOGIA DA INFORMAÇÃO');
INSERT INTO AREAATUACAO (`AAT_NOME`) VALUES ('CONTRUÇÃO CIVIL');
INSERT INTO AREAATUACAO (`AAT_NOME`) VALUES ('MANUTENÇÃO PREDIAL');
	  
INSERT INTO `PERFIL` (`PER_CODIGO`, `PER_NOME`, `PER_DESCRICAO`, `PER_ATIVO`) VALUES
(1, 'Sindico', 'Sindico', 1),
(2, 'Morador', 'Morador', 1),
(3, 'Administrador do SISGECON', 'ACESSO A TODOS OS RECURSOS DE GESTÃO DO SISTEMA', 1),
(4, 'Proprietário', 'Proprietário', 1),
(5, 'Inquilino', 'Inquilino', 1),
(6, 'Conselho Fiscal', 'Conselho Fiscal', 1);


INSERT INTO `RECURSO` (`REC_CODIGO`, `REC_NOME`, `REC_DESCRICAO`, `REC_ATIVO`) VALUES
(1, 'CADASTRO_PERFIL', 'Cadastro de perfil de sistema', 1),
(2, 'CADASTRO_RECURSO', 'Cadastro de recurso no sistema', 1),
(3, 'CADASTRO_ANIMAIS', 'Cadastro de animais', 1),
(4, 'CADASTRO_MORADOR', 'CADASTRO_MORADOR', 1),
(5, 'CADASTRO_INQUILINO', 'CADASTRO_INQUILINO', 1),
(6, 'UTILIZAR_SISTEMA ', 'UTILIZAR_SISTEMA', 1),
(7, 'CADASTRAR_VISITANTES', 'CADASTRAR_VISITANTES', 1),
(8, 'RESERVAR_ESPACOS', 'RESERVAR_ESPACOS', 1),
(9, 'PUBLICAR_CLASSIFICADOS', 'PUBLICAR_CLASSIFICADOS', 1),
(11, 'CADASTRAR_VEICULOS', 'CADASTRAR_VEICULOS', 1),
(10, 'SOLICITAR_SERVICOS', 'SOLICITAR_SERVICOS', 1);


INSERT INTO TIPOUNIDADE (TPU_NOME,TPU_SIGLA) VALUES ('CASA','CS');
INSERT INTO TIPOUNIDADE (TPU_NOME,TPU_SIGLA) VALUES ('APARTAMENTO','AP');
INSERT INTO TIPOUNIDADE (TPU_NOME,TPU_SIGLA) VALUES ('LOJA','LJ');
INSERT INTO TIPOUNIDADE (TPU_NOME,TPU_SIGLA) VALUES ('LOTE/TERRENO','LOTE');
INSERT INTO TIPOUNIDADE (TPU_NOME,TPU_SIGLA) VALUES ('COMERCIO', 'COM.');

INSERT INTO TIPOPESSOAFISICAUNIDADE (`TPF_NOME`) VALUES ('PROPRIETÁRIO');
INSERT INTO TIPOPESSOAFISICAUNIDADE (`TPF_NOME`) VALUES ('MORADOR');
INSERT INTO TIPOPESSOAFISICAUNIDADE (`TPF_NOME`) VALUES ('INQUILINO');
INSERT INTO TIPOPESSOAFISICAUNIDADE (`TPF_NOME`) VALUES ('VISITANTE');
INSERT INTO TIPOPESSOAFISICAUNIDADE (`TPF_NOME`) VALUES ('COMODATO');
INSERT INTO TIPOPESSOAFISICAUNIDADE (`TPF_NOME`) VALUES ('PROPRIETÁRIO DE PARTE');
INSERT INTO TIPOPESSOAFISICAUNIDADE (`TPF_NOME`) VALUES ('FUNCIONÁRIO');


INSERT INTO SEXO (`SEX_NOME`,SEX_SIGLA) VALUES ('MASCULINO','M');
INSERT INTO SEXO (`SEX_NOME`,SEX_SIGLA) VALUES ('FEMININO','F');


INSERT INTO `PESSOAFISICA` (`PEF_CPF`, `SEX_CODIGO`, `PEF_NOME`, `PEF_DTNASCIMENTO`,`PEF_NOMEMAE`,`PEF_NOMEPAI`) 
VALUES ('52948371880', 1, 'Gestor TushTech', '2017-08-30','Maria da Silva','Jose da Silva');

INSERT INTO `PESSOAFISICA` (`PEF_CPF`, `SEX_CODIGO`, `PEF_NOME`, `PEF_DTNASCIMENTO`,`PEF_NOMEMAE`,`PEF_NOMEPAI`) 
VALUES ('66632525504', 1, 'Usuário JCDIEHL', '2018-06-12','Joana Silva','Mario da Silva');

INSERT INTO `PESSOAFISICA` (`PEF_CPF`, `SEX_CODIGO`, `PEF_NOME`, `PEF_DTNASCIMENTO`,`PEF_NOMEMAE`,`PEF_NOMEPAI`) 
VALUES ('92726296084', 1, 'Usuário JCDIEHL TESTE', '2018-06-12','Joana Silva JCDIEHL','Mario da Silva JCDIEHL');

INSERT INTO `PESSOAFISICA` (`PEF_CPF`, `SEX_CODIGO`, `PEF_NOME`, `PEF_DTNASCIMENTO`,`PEF_NOMEMAE`,`PEF_NOMEPAI`) 
VALUES ('71864822104', 2, 'Síndico Teste 1', '2010-06-12','Gonçalina Octaviana Teste','Octávio Mesquita');

INSERT INTO `PESSOAJURIDICA` (`PEJ_NOMEFANTASIA`, `PEJ_CNPJ`, `PEJ_RAZAOSOCIAL`) VALUES ('TushTech Tecnologia da Informação', '53586990000158', 'TushTech TI');
INSERT INTO `PESSOAJURIDICA` (`PEJ_NOMEFANTASIA`, `PEJ_CNPJ`, `PEJ_RAZAOSOCIAL`) VALUES ('JCDIEHL II', '03232440000162', 'ENGENHARIA E SERVIÇOS');
INSERT INTO `PESSOAJURIDICA` (`PEJ_NOMEFANTASIA`, `PEJ_CNPJ`, `PEJ_RAZAOSOCIAL`) VALUES ('JCDIEHL', '36654767000181', 'ENGENHARIA E SERVIÇOS');





INSERT INTO `CONDOMINIO` (PEJ_CODIGO,CON_ENDERECO,CON_EMAIL_1) VALUES ('3','QS 1 Rua 210, Lote 34 e 36 - Águas Claras - Brasília / DF | Cep 71950-770','leonardobomtempo@yahoo.com.br');

INSERT INTO `EMAIL` (`PEF_CODIGO`, `EMA_DESCRICAO`) VALUES (1, 'tushtech.ti@gmail.com');
INSERT INTO `EMAIL` (`PEF_CODIGO`, `EMA_DESCRICAO`) VALUES (2, 'jcdiehl@jcdiehl.com');
INSERT INTO `EMAIL` (`PEF_CODIGO`, `EMA_DESCRICAO`) VALUES (3, 'jcdiehl2@jcdiehl.com');
INSERT INTO `EMAIL` (`PEF_CODIGO`, `EMA_DESCRICAO`) VALUES (4, 'teste@teste.com');


INSERT INTO `SENHAS` (`PEF_CODIGO`, `SEN_SENHA`) VALUES (1, '$2a$10$5Uo2sEj8.jOxPtKK3K3nPu370AoqNCMhgPiiKOWvHsB9wDFLHeit.');
INSERT INTO `SENHAS` (`PEF_CODIGO`, `SEN_SENHA`) VALUES (2, '$2a$10$5Uo2sEj8.jOxPtKK3K3nPu370AoqNCMhgPiiKOWvHsB9wDFLHeit.');
INSERT INTO `SENHAS` (`PEF_CODIGO`, `SEN_SENHA`) VALUES (3, '$2a$10$5Uo2sEj8.jOxPtKK3K3nPu370AoqNCMhgPiiKOWvHsB9wDFLHeit.');
INSERT INTO `SENHAS` (`PEF_CODIGO`, `SEN_SENHA`) VALUES (4, '$2a$10$5Uo2sEj8.jOxPtKK3K3nPu370AoqNCMhgPiiKOWvHsB9wDFLHeit.');


insert into PERFIL_CONDOMINIO (CON_CODIGO, PER_CODIGO) values (1, 1);
insert into PERFIL_CONDOMINIO (CON_CODIGO, PER_CODIGO) values (1, 2);
insert into PERFIL_CONDOMINIO (CON_CODIGO, PER_CODIGO) values (1, 4);
insert into PERFIL_CONDOMINIO (CON_CODIGO, PER_CODIGO) values (1, 5);


INSERT INTO PESSOAFISICA_PERFIL_CONDOMINIO (PEF_CODIGO,PCO_CODIGO)VALUES (1,1);
INSERT INTO PESSOAFISICA_PERFIL_CONDOMINIO (PEF_CODIGO,PCO_CODIGO)VALUES (4,1);

-- Usuaário 1
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('1', '1');
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('2', '1');
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('3', '1');
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('4', '1');
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('5', '1');

-- Usuaário 4
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('1', '4');
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('2', '4');
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('3', '4');
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('4', '4');
INSERT INTO PERFIL_CONDOMINIO_RECURSO (`REC_CODIGO`, `PCO_CODIGO`) VALUES ('5', '4');


INSERT INTO STATUSATENDIMENTO (`STA_NOME`, `STA_ATIVO`) VALUES ('ABERTO', '1');

INSERT INTO STATUSATENDIMENTO (`STA_NOME`, `STA_ATIVO`) VALUES ('EM ANDAMENTO', '1');

INSERT INTO STATUSATENDIMENTO (`STA_NOME`, `STA_ATIVO`) VALUES ('CANCELADO', '1');

INSERT INTO STATUSATENDIMENTO (`STA_NOME`, `STA_ATIVO`) VALUES ('FECHADO', '1');



INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('ABERTA', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('VISITA EXECUTADA', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('COTAÇÃO DE MATERIAL', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('SERVIÇO AUTORIZADO', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('COMPRA DE MATERIAL', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('EM EXECUÇÃO', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('SERVIÇO EXECUTADO', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('ACEITE DO SERVIÇO', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('PESQUISA DE SATISFAÇÃO', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('CANCELADA', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('FINALIZADA', '1');
INSERT INTO ORDEMSERVICOSTATUS (OSS_NOME, OSS_ATIVO) VALUES ('GARANTIA', '1');

INSERT INTO TIPOATENDIMENTO (TAT_NOME, TAT_SUBORDINACAO, TAT_ATIVO) VALUES
('Emergência', NULL, 1),
('Animal solto (cachorro, gato, coelho, etc)', 1, 1),
('Animal silvestre solto (cobras, pássaros, etc)', 1, 1),
('Financeiro', NULL, 1),
('Cadastro', NULL, 1),
('Serviços', NULL, 1),
('Reservas', NULL, 1),
('Informações', NULL, 1),
('Reclamações', NULL, 1),
('Sugestões', NULL, 1),
('Cabo de energia partido', 1, 1),
('Acidentes', 1, 1),
('2a via boleto', 4, 1),
('Nada Consta', 4, 1),
('Planilha de débitos', 4, 1),
('Parcelamento de débitos', 4, 1),
('Cadastro de Animais', 5, 1),
('Cadastro de Veículo para TAG', 5, 1),
('Cadastro de Veículo para CARTÃO', 5, 1),
('Cadastro de Passageiro do Ônibus', 5, 1),
('Cadastro de Funcionários da Unidade', 5, 1),
('Cadastro de Visitantes para liberação na portaria', 5, 1),
('Cadastro de Inquilino', 5, 1),
('Cadastro de Novo Proprietário', 5, 1),
('Cadastro de Morador da Unidade', 5, 1);



INSERT INTO PARENTESCO (`PAR_NOME`) VALUES ('CÔNJUGE');
INSERT INTO PARENTESCO (`PAR_NOME`) VALUES ('FILHO(A)');
INSERT INTO PARENTESCO (`PAR_NOME`) VALUES ('PAI');
INSERT INTO PARENTESCO (`PAR_NOME`) VALUES ('MÃE');
INSERT INTO PARENTESCO (`PAR_NOME`) VALUES ('AVÔ(Ó)');
INSERT INTO PARENTESCO (`PAR_NOME`) VALUES ('PRIMO(A)');
INSERT INTO PARENTESCO (`PAR_NOME`) VALUES ('TIO(A)');
INSERT INTO PARENTESCO (`PAR_NOME`) VALUES ('CUNHADO(A)');

INSERT INTO TIPOANIMAL (`TIA_NOME`) VALUES ('CACHORRO');
INSERT INTO TIPOANIMAL (`TIA_NOME`) VALUES ('GATO');
INSERT INTO TIPOANIMAL (`TIA_NOME`) VALUES ('PEIXE');
INSERT INTO TIPOANIMAL (`TIA_NOME`) VALUES ('TARTARUGA');
INSERT INTO TIPOANIMAL (`TIA_NOME`) VALUES ('PÁSSARO');
INSERT INTO TIPOANIMAL (`TIA_NOME`) VALUES ('LAGARTO');
INSERT INTO TIPOANIMAL (`TIA_NOME`) VALUES ('COBRA');
INSERT INTO TIPOANIMAL (`TIA_NOME`) VALUES ('ROEDOR');
INSERT INTO TIPOANIMAL (`TIA_NOME`) VALUES ('COELHO');

INSERT INTO COR (`CRR_NOME`) VALUES ('AZUL');
INSERT INTO COR (`CRR_NOME`) VALUES ('BRANCA');
INSERT INTO COR (`CRR_NOME`) VALUES ('PRETA');
INSERT INTO COR (`CRR_NOME`) VALUES ('PRATA');
INSERT INTO COR (`CRR_NOME`) VALUES ('VERMELHA');
INSERT INTO COR (`CRR_NOME`) VALUES ('VERDE');

INSERT INTO TIPOVEICULO (`TIV_NOME`) VALUES ('CARRO');
INSERT INTO TIPOVEICULO (`TIV_NOME`) VALUES ('MOTO');

-- INSERTS NA TABELA TIPO DOCUMENTOS

INSERT INTO TIPODOCUMENTO (`TID_NOME`) VALUES ('IDENTIDADE'),('ESCRITURA'),('CESSÃO DE DIREITOS'),('CONTRATO DE LOCAÇÃO'),('CONTRATO DE COMODATO');

INSERT INTO STATUSDOCUMENTO (`STD_NOME`) VALUES ('NÃO VALIDADO');
INSERT INTO STATUSDOCUMENTO (`STD_NOME`) VALUES ('VALIDADO');
INSERT INTO STATUSDOCUMENTO (`STD_NOME`) VALUES ('ARQUIVO ILEGÍVEL');
INSERT INTO STATUSDOCUMENTO (`STD_NOME`) VALUES ('ARQUIVO CORROMPIDO');
INSERT INTO STATUSDOCUMENTO (`STD_NOME`) VALUES ('ARQUIVO INCOMPATÍVEL');

INSERT INTO TIPOPESSOAFISICACONDOMINIO (`TPC_NOME`) VALUES ('ADMINISTRADOR SISTEMA');
INSERT INTO TIPOPESSOAFISICACONDOMINIO (`TPC_NOME`) VALUES ('SÍNDICO');
INSERT INTO TIPOPESSOAFISICACONDOMINIO (`TPC_NOME`) VALUES ('SUBSÍNDICO');
INSERT INTO TIPOPESSOAFISICACONDOMINIO (`TPC_NOME`) VALUES ('FUNCIONÁRIO');
INSERT INTO TIPOPESSOAFISICACONDOMINIO (`TPC_NOME`) VALUES ('VISITANTE');
INSERT INTO TIPOPESSOAFISICACONDOMINIO (`TPC_NOME`) VALUES ('MORADOR');
INSERT INTO TIPOPESSOAFISICACONDOMINIO (`TPC_NOME`) VALUES ('MEMBRO DE CONSELHO');
INSERT INTO TIPOPESSOAFISICACONDOMINIO (`TPC_NOME`) VALUES ('FUNCIONARIO UNIDADE');


-- INSERINDO UMA PESSOA A UM CONDOMINIO
INSERT INTO PESSOAFISICA_CONDOMINIO (PEF_CODIGO,CON_CODIGO,PFC_DTINICIO,TPC_CODIGO) VALUES (4,1,now(),2);



INSERT INTO PERGUNTAAVALIACAO (`PAV_NOME`, `PAV_ATIVO`,`PAV_OBJETIVA`) VALUES ('Foi atendido no prazo?', '1','1');
INSERT INTO PERGUNTAAVALIACAO (`PAV_NOME`, `PAV_ATIVO`,`PAV_OBJETIVA`) VALUES ('O problema foi resolvido?', '1','1');
INSERT INTO PERGUNTAAVALIACAO (`PAV_NOME`, `PAV_ATIVO`,`PAV_OBJETIVA`) VALUES ('Qualidade do serviço.', '1','0');
INSERT INTO PERGUNTAAVALIACAO (`PAV_NOME`, `PAV_ATIVO`,`PAV_OBJETIVA`) VALUES ('Profissionalismo do atendente.', '1','0');

-- INSERTS NA TABELA FUNCAO PESSOA JURIDICA

INSERT INTO FUNCAOPESSOAJURIDICA (FPJ_NOME,FPJ_DESCRICAO,PEJ_CODIGO) VALUES ('ADMINISTRADOR SISTEMA','Administrador de todo o sistema',1);

INSERT INTO FUNCAOPESSOAJURIDICA (FPJ_NOME,FPJ_DESCRICAO,PEJ_CODIGO) VALUES ('ADMINISTRADOR','ADMINISTRADOR',2);

INSERT INTO FUNCAOPESSOAJURIDICA (FPJ_NOME,FPJ_DESCRICAO,PEJ_CODIGO) VALUES ('ASSISTENTE','ASSISTENTE',2);

INSERT INTO FUNCAOPESSOAJURIDICA (FPJ_NOME,FPJ_DESCRICAO,PEJ_CODIGO) VALUES ('FUNCIONARIO','FUNCIONARIO',2);

INSERT INTO PESSOAJURIDICA_PESSOAFISICA (`PEF_CODIGO`, `PEJ_CODIGO`, `PJP_DTINICIO`,`FPJ_CODIGO`) VALUES ('1', '1', '2018-06-12',1);

INSERT INTO PESSOAJURIDICA_PESSOAFISICA (`PEF_CODIGO`, `PEJ_CODIGO`, `PJP_DTINICIO`,`FPJ_CODIGO`) VALUES ('2', '2', '2018-06-12',2);



INSERT INTO TIPOORDEMSERVICO (TOS_NOME,TOS_ATIVO,TOS_PRIORIDADE,TOS_SUBORDINACAO)VALUES 
									('Elétrica',1,0,NULL),
									('Hidráulica',1,0,NULL), 
									('Ar Condicionado',1,0,NULL),
									('Exaustão',1,0,NULL),
									('Incêndio',1,0,NULL),
									('Civil',1,0,NULL),
									('Marcenaria',1,0,NULL),
									('Bombas e Motores',1,0,NULL),
									('Alarme',1,0,NULL),
									('Iluminação',1,0,1),
									('Quadros elétricos',1,0,1),
									('Tomadas',1,0,1),
									('Interruptores',1,0,1),
									('Rabichos',1,0,2),
									('Sifões',1,0,2),
									('Torneiras',1,0,2),
									('Registros',1,0,2),
									('Ralos',1,0,2),
									('Caixas de gordura',1,0,2),
									('Válvulas',1,0,2),
									('Splits',1,0,3),
									('Fancolis',1,0,3),
									('Selfs',1,0,3),
									('Limpeza de dutos',1,0,3),
									('Grelhas',1,0,3),
									('Tubulações',1,0,3),
									('Chillers',1,0,3),
									('Motores (ar condicionado)',1,0,3),
									('Limpeza de coifas',1,0,4),
									('Dutos',1,0,4),
									('Filtros',1,0,4),
									('Motores',1,0,4),
									('Correias',1,0,4),
									('Sistema de detecção',1,0,5),
									('SPK',1,0,5),
									('Alvenaria',1,0,6),
									('Reboco',1,0,6),
									('Piso',1,0,6),
									('Pintura',1,0,6),
									('Teto',1,0,6),
									('Gesso',1,0,6),
									('Portas',1,0,7),
									('Armários',1,0,7),
									('Móveis',1,0,7);
									
INSERT INTO PESSOAJURIDICA_CONDOMINIO (`PEJ_CODIGO`, `CON_CODIGO`, `PEF_CODIGORESPONSAVEL`) VALUES ('2', '1', '3');
/*INSERT INTO PESSOAJURIDICA_CONDOMINIO (`PEJ_CODIGO`, `CON_CODIGO`, `PEF_CODIGORESPONSAVEL`) VALUES ('2', '2', '3');*/

INSERT INTO PACOTESERVICO (`PCS_NOME`, `PCS_DESCRICAO`, `PCS_VALOR`, `PCS_QTDVISITAS`, `PEJ_CODIGO`, `PCS_VALORVISITAEXCEDENTE`) VALUES ('Econômico I', 'Econômico I - Mensal', '680', '4', '3', '250');
INSERT INTO PACOTESERVICO (`PCS_NOME`, `PCS_DESCRICAO`, `PCS_VALOR`, `PCS_QTDVISITAS`, `PEJ_CODIGO`, `PCS_VALORVISITAEXCEDENTE`) VALUES ('Econômico II', 'Econômico II - Mensal', '900', '6', '3', '235');
INSERT INTO PACOTESERVICO (`PCS_NOME`, `PCS_DESCRICAO`, `PCS_VALOR`, `PCS_QTDVISITAS`, `PEJ_CODIGO`, `PCS_VALORVISITAEXCEDENTE`) VALUES ('Econômico III', 'Econômico III - Mensal', '1300', '10', '3', '200');
INSERT INTO PACOTESERVICO (`PCS_NOME`, `PCS_DESCRICAO`, `PCS_VALOR`, `PCS_QTDVISITAS`, `PEJ_CODIGO`, `PCS_VALORVISITAEXCEDENTE`) VALUES ('Pacote', 'Pacote - Mensal (Exclusivo)', '5200', '999', '3','0');
INSERT INTO PACOTESERVICO (`PCS_NOME`, `PCS_DESCRICAO`, `PCS_QTDVISITAS`,`PEJ_CODIGO`, `PCS_VALORVISITAEXCEDENTE`) VALUES ('Manutenção Via Demanda', 'Por Demanda',1,'3', '350');

-- INSERÇÃO DE MARCA E MODELOS

INSERT INTO MARCA (MAR_NOME,TIV_CODIGO) VALUES
	('CHEVROLET',1),
	('VOLKSWAGEN',1),
	('FIAT',1),
	('MERCEDES-BENZ',1),
	('CITROEN',1),
	('CHANA',1),
	('HONDA',1),
	('SUBARU',1),
	('FERRARI',1),
	('BUGATTI',1),
	('LAMBORGHINI',1),
	('FORD',1),
	('HYUNDAI',1),
	('JAC',1),
	('KIA',1),
	('GURGEL',1),
	('DODGE',1),
	('CHRYSLER',1),
	('BENTLEY',1),
	('SSANGYONG',1),
	('PEUGEOT',1),
	('TOYOTA',1),
	('RENAULT',1),
	('ACURA',1),
	('ADAMO',1),
	('AGRALE',1),
	('ALFA ROMEO',1),
	('AMERICAR',1),
	('ASTON MARTIN',1),
	('AUDI',1),
	('BEACH',1),
	('BIANCO',1),
	('BMW',1),
	('BORGWARD',1),
	('BRILLIANCE',1),
	('BUICK',1),
	('CBT',1),
	('NISSAN',1),
	('CHAMONIX',1),
	('CHEDA',1),
	('CHERY',1),
	('CORD',1),
	('COYOTE',1),
	('CROSS LANDER',1),
	('DAEWOO',1),
	('DAIHATSU',1),
	('VOLVO',1),
	('DE SOTO',1),
	('DETOMAZO',1),
	('DELOREAN',1),
	('DKW-VEMAG',1),
	('SUZUKI',1),
	('EAGLE',1),
	('EFFA',1),
	('ENGESA',1),
	('ENVEMO',1),
	('FARUS',1),
	('FERCAR',1),
	('FNM',1),
	('PONTIAC',1),
	('PORSCHE',1),
	('GEO',1),
	('GRANCAR',1),
	('GREAT WALL',1),
	('HAFEI',1),
	('HOFSTETTER',1),
	('HUDSON',1),
	('HUMMER',1),
	('INFINITI',1),
	('INTERNATIONAL',1),
	('JAGUAR',1),
	('JEEP',1),
	('JINBEI',1),
	('JPX',1),
	('KAISER',1),
	('KOENIGSEGG',1),
	('LAUTOMOBILE',1),
	('LAUTOCRAFT',1),
	('LADA',1),
	('LANCIA',1),
	('LAND ROVER',1),
	('LEXUS',1),
	('LIFAN',1),
	('LINCOLN',1),
	('LOBINI',1),
	('LOTUS',1),
	('MAHINDRA',1),
	('MASERATI',1),
	('MATRA',1),
	('MAYBACH',1),
	('MAZDA',1),
	('MENON',1),
	('MERCURY',1),
	('MITSUBISHI',1),
	('MG',1),
	('MINI',1),
	('MIURA',1),
	('MORRIS',1),
	('MP LAFER',1),
	('MPLM',1),
	('NEWTRACK',1),
	('NISSIN',1),
	('OLDSMOBILE',1),
	('PAG',1),
	('PAGANI',1),
	('PLYMOUTH',1),
	('PUMA',1),
	('RENO',1),
	('REVA-I',1),
	('ROLLS-ROYCE',1),
	('ROMI',1),
	('SEAT',1),
	('UTILITARIOS AGRICOLAS',1),
	('SHINERAY',1),
	('SAAB',1),
	('SHORT',1),
	('SIMCA',1),
	('SMART',1),
	('SPYKER',1),
	('STANDARD',1),
	('STUDEBAKER',1),
	('TAC',1),
	('TANGER',1),
	('TRIUMPH',1),
	('TROLLER',1),
	('UNIMOG',1),
	('WIESMANN',1),
	('CADILLAC',1),
	('AM GEN',1),
	('BUGGY',1),
	('WILLYS OVERLAND',1),
	('KASEA',1),
	('SATURN',1),
	('SWELL MINI',1),
	('SKODA',1),
	('KARMANN GHIA',1),
	('KART',1),
	('HANOMAG',1),
	('OUTROS',1),
	('HILLMAN',1),
	('HRG',1),
	('GAIOLA',1),
	('TATA',1),
	('DITALLY',1),
	('RELY',1),
	('MCLAREN',1),
	('GEELY',1);

	
INSERT INTO MODELO (`MAR_CODIGO`, `MOD_NOME`) VALUES
	(24, 'INTEGRA'),
	(24, 'LEGEND'),
	(24, 'NSX'),
	(26, 'MARRUA'),
	(27, '145'),
	(27, '147'),
	(27, '155'),
	(27, '156'),
	(27, '164'),
	(27, '166'),
	(27, '2300'),
	(27, 'SPIDER'),
	(131, 'RURAL'),
	(15, 'AM-825'),
	(15, 'HI-TOPIC'),
	(15, 'ROCSTA'),
	(15, 'TOPIC'),
	(15, 'TOWNER'),
	(30, '100'),
	(30, '80'),
	(30, 'A1'),
	(30, 'A3'),
	(30, 'A4 SEDAN'),
	(30, 'A5 COUPE'),
	(30, 'A6 SEDAN'),
	(30, 'A7'),
	(30, 'A8'),
	(30, 'Q3'),
	(30, 'Q5'),
	(30, 'Q7'),
	(30, 'R8'),
	(30, 'RS3'),
	(30, 'RS4'),
	(30, 'RS5'),
	(30, 'RS6'),
	(30, 'S3'),
	(30, 'S4 SEDAN'),
	(30, 'S6 SEDAN'),
	(30, 'S8'),
	(30, 'TT'),
	(30, 'TTS'),
	(131, 'ITAMARATY'),
	(128, 'SRX4'),
	(128, 'ESCALADE'),
	(129, 'HUMMER'),
	(37, 'JAVALI'),
	(6, 'MINI STAR FAMILY'),
	(6, 'MINI STAR UTILITY'),
	(41, 'CIELO'),
	(41, 'FACE'),
	(41, 'QQ'),
	(41, 'S-18'),
	(41, 'TIGGO'),
	(18, '300C'),
	(18, 'CARAVAN'),
	(18, 'CIRRUS'),
	(18, 'GRAN CARAVAN'),
	(18, 'LE BARON'),
	(18, 'NEON'),
	(18, 'PT CRUISER'),
	(18, 'SEBRING'),
	(18, 'STRATUS'),
	(18, 'TOWN E COUNTRY'),
	(18, 'VISION'),
	(5, 'AIRCROSS'),
	(5, 'AX'),
	(5, 'BERLINGO'),
	(5, 'BX'),
	(5, 'C3'),
	(5, 'C4'),
	(5, 'C5'),
	(5, 'C6'),
	(5, 'C8'),
	(5, 'DS3'),
	(5, 'EVASION'),
	(5, 'JUMPER'),
	(5, 'XANTIA'),
	(5, 'XM'),
	(5, 'XSARA'),
	(5, 'ZX'),
	(44, 'CL-244'),
	(44, 'CL-330'),
	(45, 'ESPERO'),
	(45, 'LANOS'),
	(45, 'LEGANZA'),
	(45, 'NUBIRA'),
	(45, 'PRINCE'),
	(45, 'RACER'),
	(45, 'SUPER SALON'),
	(45, 'TICO'),
	(46, 'APPLAUSE'),
	(46, 'CHARADE'),
	(46, 'CUORE'),
	(46, 'FEROZA'),
	(46, 'GRAN MOVE'),
	(46, 'MOVE VAN'),
	(46, 'TERIOS'),
	(17, 'AVENGER'),
	(17, 'DAKOTA'),
	(17, 'JOURNEY'),
	(17, 'RAM'),
	(17, 'STEALTH'),
	(54, 'M-100'),
	(54, 'PLUTUS'),
	(54, 'START'),
	(54, 'ULC'),
	(55, 'ENGESA'),
	(56, 'CAMPER'),
	(56, 'MASTER'),
	(9, '348'),
	(9, '355'),
	(9, '360'),
	(9, '456'),
	(9, '550'),
	(9, '575M'),
	(9, '612'),
	(9, 'CALIFORNIA'),
	(9, 'F430'),
	(9, 'F599'),
	(3, '147'),
	(3, '500'),
	(3, 'BRAVA'),
	(3, 'BRAVO'),
	(3, 'COUPE'),
	(3, 'DOBLO'),
	(3, 'DUCATO CARGO'),
	(3, 'DUNA'),
	(3, 'ELBA'),
	(3, 'FIORINO'),
	(3, 'FREEMONT'),
	(3, 'GRAND SIENA'),
	(3, 'IDEA'),
	(3, 'LINEA'),
	(3, 'MAREA'),
	(3, 'OGGI'),
	(3, 'PALIO'),
	(3, 'PANORAMA'),
	(3, 'PREMIO'),
	(3, 'PUNTO'),
	(3, 'SIENA'),
	(3, 'STILO'),
	(3, 'STRADA'),
	(3, 'TEMPRA'),
	(3, 'TIPO'),
	(3, 'UNO'),
	(12, 'AEROSTAR'),
	(12, 'ASPIRE'),
	(12, 'BELINA'),
	(12, 'CLUB WAGON'),
	(12, 'CONTOUR'),
	(12, 'CORCEL II'),
	(12, 'COURIER'),
	(12, 'CROWN VICTORIA'),
	(12, 'DEL REY'),
	(12, 'ECOSPORT'),
	(12, 'EDGE'),
	(12, 'ESCORT'),
	(12, 'EXPEDITION'),
	(12, 'EXPLORER'),
	(12, 'F-100'),
	(12, 'F-1000'),
	(12, 'F-150'),
	(12, 'F-250'),
	(12, 'FIESTA'),
	(12, 'FOCUS'),
	(12, 'FURGLAINE'),
	(12, 'FUSION'),
	(12, 'IBIZA'),
	(12, 'KA'),
	(12, 'MONDEO'),
	(12, 'MUSTANG'),
	(12, 'PAMPA'),
	(12, 'PROBE'),
	(12, 'RANGER'),
	(12, 'VERSAILLES ROYALE'),
	(12, 'TAURUS'),
	(12, 'THUNDERBIRD'),
	(12, 'TRANSIT'),
	(12, 'VERONA'),
	(12, 'VERSAILLES'),
	(12, 'WINDSTAR'),
	(1, 'A-10'),
	(1, 'A-20'),
	(1, 'AGILE'),
	(1, 'ASTRA'),
	(1, 'BLAZER'),
	(1, 'BONANZA'),
	(1, 'C-10'),
	(1, 'C-20'),
	(1, 'CALIBRA'),
	(1, 'CAMARO'),
	(1, 'CAPRICE'),
	(1, 'CAPTIVA'),
	(1, 'CARAVAN'),
	(1, 'CAVALIER'),
	(1, 'CELTA'),
	(1, 'CHEVY'),
	(1, 'CHEYNNE'),
	(1, 'COBALT'),
	(1, 'CORSA'),
	(1, 'CORVETTE'),
	(1, 'CRUZE'),
	(1, 'D-10'),
	(1, 'D-20'),
	(1, 'IPANEMA'),
	(1, 'KADETT'),
	(1, 'LUMINA'),
	(1, 'MALIBU'),
	(1, 'MERIVA'),
	(1, 'MONTANA'),
	(1, 'OMEGA'),
	(1, 'OPALA'),
	(1, 'PRISMA'),
	(1, 'S10'),
	(1, 'SILVERADO'),
	(1, 'SONIC'),
	(1, 'SONOMA'),
	(1, 'SPACEVAN'),
	(1, 'SS10'),
	(1, 'SUBURBAN'),
	(1, 'SYCLONE'),
	(1, 'TIGRA'),
	(1, 'TRACKER'),
	(1, 'TRAFIC'),
	(1, 'VECTRA'),
	(1, 'VERANEIO'),
	(1, 'ZAFIRA'),
	(64, 'HOVER'),
	(16, 'BR-800'),
	(16, 'CARAJAS'),
	(16, 'TOCANTINS'),
	(16, 'XAVANTE'),
	(16, 'VIP'),
	(65, 'TOWNER'),
	(7, 'ACCORD'),
	(7, 'CITY'),
	(7, 'CIVIC'),
	(7, 'CR-V'),
	(7, 'FIT'),
	(7, 'ODYSSEY'),
	(7, 'PASSPORT'),
	(7, 'PRELUDE'),
	(13, 'ACCENT'),
	(13, 'ATOS'),
	(13, 'AZERA'),
	(13, 'COUPE'),
	(13, 'ELANTRA'),
	(13, 'EXCEL'),
	(13, 'GALLOPER'),
	(13, 'GENESIS'),
	(13, 'H1'),
	(13, 'H100'),
	(13, 'I30'),
	(13, 'IX35'),
	(13, 'MATRIX'),
	(13, 'PORTER'),
	(13, 'SANTA FE'),
	(13, 'SCOUPE'),
	(13, 'SONATA'),
	(13, 'TERRACAN'),
	(13, 'TRAJET'),
	(13, 'TUCSON'),
	(13, 'VELOSTER'),
	(13, 'VERACRUZ'),
	(71, 'DAIMLER'),
	(71, 'S-TYPE'),
	(71, 'X-TYPE'),
	(14, 'J3'),
	(14, 'J5'),
	(14, 'J6'),
	(71, 'MODELOS XJ'),
	(71, 'MODELOS XK'),
	(71, 'MARK V'),
	(71, 'MODELOS XF'),
	(71, 'F-TYPE'),
	(72, 'GRAND CHEROKEE'),
	(72, 'WRANGLER'),
	(72, 'JEEP'),
	(72, 'CJ5'),
	(72, 'CJ3'),
	(74, 'JIPE MONTEZ'),
	(79, 'LAIKA'),
	(79, 'NIVA'),
	(15, 'BESTA'),
	(15, 'BONGO'),
	(15, 'CADENZA'),
	(15, 'CARENS'),
	(15, 'CARNIVAL'),
	(15, 'CERATO'),
	(15, 'CERES'),
	(15, 'CLARUS'),
	(15, 'MAGENTIS'),
	(15, 'MOHAVE'),
	(15, 'OPIRUS'),
	(15, 'OPTIMA'),
	(15, 'PICANTO'),
	(15, 'SEPHIA'),
	(15, 'SHUMA'),
	(15, 'SORENTO'),
	(15, 'SOUL'),
	(15, 'SPORTAGE'),
	(81, 'DEFENDER'),
	(81, 'DISCOVERY'),
	(81, 'FREELANDER'),
	(11, 'GALLARDO'),
	(11, 'MURCIELAGO'),
	(81, 'NEW RANGE'),
	(81, 'RANGE ROVER'),
	(82, 'ES'),
	(82, 'GS'),
	(82, 'IS-300'),
	(82, 'LS'),
	(82, 'RX'),
	(82, 'SC'),
	(83, '320'),
	(83, '620'),
	(83, 'X60'),
	(83, '530'),
	(84, 'AVIATOR'),
	(87, 'SCORPIO'),
	(87, 'XUV 500'),
	(87, 'XYLO'),
	(87, 'BOLERO'),
	(88, '430'),
	(88, 'COUPE'),
	(88, 'GHIBLI'),
	(88, 'GRANCABRIO'),
	(88, 'GRANSPORT'),
	(88, 'GRANTURISMO'),
	(88, 'QUATTROPORTE'),
	(88, 'SHAMAL'),
	(88, 'SPIDER'),
	(89, 'PICK-UP'),
	(91, '323'),
	(91, '626'),
	(91, '929'),
	(91, 'B-2500'),
	(91, 'B2200'),
	(91, 'MILLENIA'),
	(91, 'MPV'),
	(91, 'MX-3'),
	(91, 'MX-5'),
	(91, 'NAVAJO'),
	(91, 'PROTEGE'),
	(91, 'RX'),
	(91, 'CX-7'),
	(91, 'MX-6'),
	(93, 'MYSTIQUE'),
	(4, 'CLASSE A'),
	(4, 'CLASSE B'),
	(4, 'CLASSE R'),
	(4, 'CLASSE GLK'),
	(4, 'SPRINTER'),
	(93, 'COUGAR'),
	(94, '3000'),
	(96, 'ONE'),
	(96, 'CABRIO'),
	(96, 'COUPE'),
	(96, 'ROADSTER'),
	(94, 'COLT'),
	(94, 'DIAMANT'),
	(94, 'ECLIPSE'),
	(94, 'EXPO'),
	(94, 'GALANT'),
	(94, 'GRANDIS'),
	(94, 'L200'),
	(94, 'L300'),
	(94, 'LANCER'),
	(94, 'MIRAGE'),
	(94, 'MONTERO'),
	(94, 'OUTLANDER'),
	(94, 'PAJERO'),
	(94, 'SPACE WAGON'),
	(94, 'PAJERO FULL'),
	(94, 'PAJERO DAKAR'),
	(94, 'PAJERO TR4'),
	(97, 'X8'),
	(38, '350Z'),
	(38, 'ALTIMA'),
	(38, 'AX'),
	(38, 'D-21'),
	(38, 'FRONTIER'),
	(38, 'KING-CAB'),
	(38, 'LIVINA'),
	(38, 'MARCH'),
	(38, 'MAXIMA'),
	(38, 'MURANO'),
	(38, 'NX'),
	(38, 'PATHFINDER'),
	(38, 'PRIMERA'),
	(38, 'QUEST'),
	(38, 'SENTRA'),
	(38, 'STANZA'),
	(38, '180SX'),
	(38, 'TERRANO'),
	(38, 'TIIDA'),
	(38, 'VERSA'),
	(38, 'X-TRAIL'),
	(38, 'XTERRA'),
	(38, 'ZX'),
	(21, '106'),
	(21, '205'),
	(21, '206'),
	(21, '207'),
	(21, '3008'),
	(21, '306'),
	(21, '307'),
	(21, '308'),
	(21, '405'),
	(21, '406'),
	(21, '407'),
	(21, '408'),
	(21, '504'),
	(21, '505'),
	(21, '508'),
	(21, '605'),
	(21, '607'),
	(21, '806'),
	(21, '807'),
	(21, 'BOXER'),
	(21, 'HOGGAR'),
	(21, 'PARTNER'),
	(21, 'RCZ'),
	(106, 'FURY'),
	(106, 'SPECIAL'),
	(60, 'TRANS-AM'),
	(60, 'TRANS-SPORT'),
	(61, '911'),
	(61, 'BOXSTER'),
	(61, 'CAYENNE'),
	(61, 'CAYMAN'),
	(61, 'PANAMERA'),
	(23, '21 SEDAN'),
	(23, 'CLIO'),
	(23, 'DUSTER'),
	(23, 'EXPRESS'),
	(23, 'FLUENCE'),
	(23, 'KANGOO'),
	(23, 'LAGUNA'),
	(23, 'LOGAN'),
	(23, 'MASTER'),
	(23, 'MEGANE'),
	(23, 'SAFRANE'),
	(23, 'SANDERO'),
	(23, 'SCENIC'),
	(23, 'SYMBOL'),
	(23, 'TRAFIC'),
	(23, 'TWINGO'),
	(117, 'PROFISSIONAL'),
	(136, 'TC'),
	(113, 'COLHEITADEIRA'),
	(114, 'A7'),
	(114, 'A9'),
	(122, 'STARK'),
	(19, 'ACTYON SPORTS'),
	(20, 'CHAIRMAN'),
	(20, 'ISTANA'),
	(20, 'KORANDO'),
	(20, 'KYRON'),
	(20, 'MUSSO'),
	(20, 'REXTON'),
	(8, 'FORESTER'),
	(8, 'IMPREZA'),
	(8, 'LEGACY'),
	(8, 'OUTBACK'),
	(8, 'SVX'),
	(8, 'TRIBECA'),
	(8, 'VIVIO'),
	(52, 'BALENO'),
	(52, 'GRAND VITARA'),
	(52, 'IGNIS'),
	(52, 'JIMNY'),
	(52, 'SUPER CARRY'),
	(52, 'SWIFT'),
	(52, 'SX4'),
	(52, 'VITARA'),
	(52, 'WAGON R'),
	(123, 'SEVETSE'),
	(22, 'AVALON'),
	(22, 'BANDEIRANTE'),
	(22, 'CAMRY'),
	(22, 'CELICA'),
	(22, 'COROLLA'),
	(22, 'CORONA'),
	(22, 'HILUX'),
	(22, 'LAND CRUISER'),
	(22, 'MR-2'),
	(22, 'PASEO'),
	(22, 'PREVIA'),
	(22, 'RAV4'),
	(22, 'SUPRA'),
	(128, 'DEVILLE'),
	(128, 'ELDORADO'),
	(47, '400 SERIES'),
	(47, '850'),
	(47, '900 SERIES'),
	(2, 'AMAROK'),
	(2, 'APOLLO'),
	(2, 'BORA'),
	(2, 'CARAVELLE'),
	(2, 'CORRADO'),
	(2, 'EOS'),
	(2, 'EUROVAN'),
	(2, 'FOX'),
	(2, 'FUSCA'),
	(2, 'GOL'),
	(2, 'GOLF'),
	(2, 'JETTA'),
	(2, 'KOMBI'),
	(2, 'LOGUS'),
	(2, 'PARATI'),
	(2, 'PASSAT'),
	(2, 'POINTER'),
	(2, 'POLO'),
	(2, 'SANTANA'),
	(2, 'SAVEIRO'),
	(2, 'SPACEFOX'),
	(2, 'TIGUAN'),
	(2, 'TOUAREG'),
	(2, 'VOYAGE'),
	(24, 'ZDX'),
	(3, '140'),
	(2, 'BRASILIA'),
	(12, 'BRASILVAN'),
	(12, 'CORCEL'),
	(3, 'PALIO WEEKEND'),
	(12, 'FOCUS SEDAN'),
	(12, 'FIESTA SEDAN'),
	(12, 'FIESTA TRAIL'),
	(21, '207 SW'),
	(12, 'ESCORT SW'),
	(21, '307 SEDAN'),
	(21, '307 SW'),
	(5, 'C4 PALLAS'),
	(5, 'C4 PICASSO'),
	(5, 'C4 VTR'),
	(23, 'CLIO SEDAN'),
	(22, 'COROLLA FIELDER'),
	(22, 'HILUX SW4'),
	(23, 'MEGANE GRAND TOUR'),
	(23, 'SANDERO STEPWAY'),
	(5, 'XSARA PICASSO'),
	(114, 'A9 CARGO'),
	(131, 'INTERLAGOS'),
	(16, 'X12'),
	(1, 'BEL AIR'),
	(33, 'RX'),
	(1, 'C-14'),
	(130, 'BUGGY'),
	(1, 'C-15'),
	(1, 'BRASIL'),
	(17, 'POLARA'),
	(3, '600'),
	(12, 'F-01'),
	(12, 'FALCON'),
	(12, 'GALAXIE'),
	(12, 'MAVERICK'),
	(12, 'MODELO A'),
	(12, 'NEW FIESTA'),
	(69, 'LINHA FX'),
	(107, 'GTE'),
	(68, 'H3'),
	(13, 'PRIME'),
	(13, 'TIBURON'),
	(72, 'RENEGADE'),
	(73, 'TOPIC VAN'),
	(4, 'CLASSE CLC'),
	(4, 'CLASSE CLS'),
	(94, 'AIRTREK'),
	(97, 'SPORT'),
	(97, 'MTS'),
	(97, 'SPIDER'),
	(38, '370Z'),
	(107, 'GT'),
	(107, 'GT4R'),
	(107, 'SPYDER'),
	(103, 'EIGHT'),
	(23, '7TL'),
	(23, '19'),
	(138, 'RECORB'),
	(16, 'SUPERMINI'),
	(2, 'TL'),
	(3, 'TOPOLINO'),
	(22, 'SR5'),
	(22, 'VITZ'),
	(2, 'VARIANT'),
	(51, 'CANDANGO'),
	(2, 'SP2'),
	(2, 'POLAUTO'),
	(23, 'GORDINI'),
	(22, 'ETIOS'),
	(1, 'ONIX'),
	(13, 'HB20'),
	(33, '330'),
	(33, '520'),
	(33, '730'),
	(33, 'M1'),
	(33, 'SERIE Z'),
	(4, 'CLASSE SLK'),
	(4, 'CLASSE C'),
	(4, 'CLASSE E'),
	(4, 'CLASSE CL'),
	(4, 'CLASSE CLK'),
	(4, 'CLASSE S'),
	(4, 'CLASSE SL'),
	(4, 'CLASSE SLS'),
	(4, 'CLASSE G'),
	(4, 'CLASSE GL'),
	(4, 'CLASSE M'),
	(13, 'EQUUS'),
	(11, '350 GT'),
	(11, '400 GT'),
	(11, 'MIURA'),
	(11, 'ISLERO'),
	(11, 'ESPADA'),
	(11, 'COUNTACH'),
	(11, 'DIABLO'),
	(11, 'ZAGATO'),
	(11, 'ALAR'),
	(11, 'LM002'),
	(11, 'REVENTON'),
	(11, 'ANKONIAN'),
	(11, 'AVENTADOR'),
	(11, 'SESTO ELEMENTO'),
	(14, 'J3 TURIN'),
	(14, 'J2'),
	(23, 'SANDERO GT'),
	(1, 'SPIN'),
	(1, 'TRAILBLAZER'),
	(5, 'C3 PICASSO'),
	(5, 'GRAND C4 PICASSO'),
	(5, 'JUMPER MINIBUS'),
	(5, 'JUMPER VETRATO'),
	(21, '207 SEDAN'),
	(21, '207 QUIKSILVER'),
	(21, '207 ESCAPADE'),
	(21, '308 CC'),
	(21, 'BOXER PASSAGEIRO'),
	(12, 'NEW FIESTA SEDAN'),
	(12, 'TRANSIT PASSAGEIRO'),
	(12, 'TRANSIT CHASSI'),
	(30, 'A4 AVANT'),
	(30, 'S4 AVANT'),
	(30, 'A5 SPORTBACK'),
	(30, 'A5 CABRIOLET'),
	(30, 'S5 COUPE'),
	(30, 'S5 SPORTBACK'),
	(30, 'S5 CABRIOLET'),
	(30, 'A6 AVANT'),
	(30, 'A6 ALLROAD'),
	(30, 'S6 AVANT'),
	(30, 'S7'),
	(30, 'TT ROADSTER'),
	(30, 'TT RS'),
	(30, 'TT RS ROADSTER'),
	(30, 'TTS ROADSTER'),
	(30, 'R8 SPYDER'),
	(30, 'R8 GT'),
	(30, 'R8 GT SPYDER'),
	(9, 'F12'),
	(9, '458 SPIDER'),
	(9, '458 ITALIA'),
	(9, 'FF'),
	(9, '599'),
	(9, 'SA'),
	(9, 'CHALLENGE'),
	(9, 'SUPERAMERICA'),
	(9, 'F430 SPIDER'),
	(9, '430'),
	(9, '612 SESSANTA'),
	(9, '599 GTB'),
	(9, 'SCUDERIA SPIDER'),
	(9, '512'),
	(9, '456 GT'),
	(9, '348 GTS'),
	(9, '348 SPIDER'),
	(9, 'F355'),
	(9, 'F355 SPIDER'),
	(9, 'F50'),
	(9, '355 SPIDER'),
	(9, '360 MODENA'),
	(94, 'LANCER SPORTBACK'),
	(94, 'LANCER EVOLUTION'),
	(94, 'L200 TRITON SAVANA'),
	(94, 'L200 TRITON'),
	(94, 'PAJERO FULL 3D'),
	(94, 'PAJERO SPORT'),
	(95, '550'),
	(38, 'LIVINA X-GEAR'),
	(38, 'GRAND LIVINA'),
	(20, 'NEW ACTYON SPORTS'),
	(22, 'PRIUS'),
	(97, 'KABRIO'),
	(97, 'SAGA'),
	(97, 'SAGA II'),
	(97, '787'),
	(97, 'X11'),
	(98, 'AUSTIN'),
	(103, 'TORONADO'),
	(103, 'SIX'),
	(17, 'NITRO'),
	(17, 'CHALLENGER'),
	(17, 'DART'),
	(17, 'LE BARON'),
	(17, 'CORDOBA'),
	(17, 'CHARGER'),
	(18, 'WINDSOR'),
	(18, 'CROSSFIRE'),
	(18, 'CORDOBA'),
	(131, 'PICKUP F75'),
	(36, 'RIVIERA'),
	(36, 'COUPE'),
	(36, 'CENTURY'),
	(36, 'APOLLO'),
	(36, 'CENTURION'),
	(36, 'EIGHT'),
	(36, 'ELECTRA'),
	(36, 'ESTATE WAGON'),
	(36, 'GRAN SPORT'),
	(36, 'GSX'),
	(36, 'INVICTA'),
	(36, 'LESABRE'),
	(36, 'LIMITED'),
	(36, 'PARK AVENUE'),
	(36, 'RAINIER'),
	(36, 'REATTA'),
	(36, 'REGAL'),
	(36, 'RENDEZVOUS'),
	(36, 'ROADMASTER'),
	(36, 'ROYAUM'),
	(36, 'SKYHAWK'),
	(36, 'SKYLARK'),
	(36, 'SOMERSET'),
	(36, 'SPECIAL'),
	(36, 'SPORT WAGON'),
	(36, 'SUPER'),
	(36, 'TERRAZA'),
	(36, 'WILDCAT'),
	(36, 'LACROSSE'),
	(36, 'ENCLAVE'),
	(36, 'GL8'),
	(36, 'HRV'),
	(36, 'LUCERNE'),
	(12, 'SIERRA'),
	(45, 'BROUGHAM'),
	(45, 'CHAIRMAN'),
	(45, 'DAMAS'),
	(45, 'GENTRA'),
	(45, 'MAEPSY'),
	(45, 'ISTANA'),
	(45, 'KALOS'),
	(45, 'KORANDO'),
	(45, 'LACETTI'),
	(45, 'LEMANS'),
	(45, 'MATIZ'),
	(45, 'MUSSO'),
	(45, 'NEXIA'),
	(45, 'REZZO'),
	(45, 'ROYALE PRINCE'),
	(45, 'ROYALE SALON'),
	(45, 'STATESMAN'),
	(45, 'TOSCA'),
	(45, 'WINSTORM'),
	(131, 'JEEP CJ'),
	(17, 'D100'),
	(4, '170'),
	(17, 'CUSTOM ROYAL'),
	(1, 'CLUB COUPE'),
	(17, 'MAGNUM'),
	(1, 'GMC 100'),
	(60, 'SOLSTICE'),
	(133, 'SL-2'),
	(71, 'MARK VII'),
	(107, 'GTI'),
	(123, 'LUCENA'),
	(3, 'BALILLA'),
	(135, 'CONVERS?VEL'),
	(16, 'X15'),
	(12, 'F-85'),
	(61, 'SPEEDSTER 356'),
	(74, 'PICAPE MONTEZ'),
	(75, 'M715'),
	(4, '300D'),
	(4, 'CLASSE TE'),
	(22, 'T-100'),
	(23, 'MEGANE SEDAN'),
	(30, 'A4 CABRIOLET'),
	(69, 'LINHA G'),
	(69, 'LINHA G COUPE'),
	(69, 'LINHA G CONVERSIVEL'),
	(69, 'LINHA M'),
	(69, 'LINHA EX'),
	(69, 'LINHA JX'),
	(69, 'LINHA QX'),
	(71, 'MARK VIII'),
	(71, 'MARK IX'),
	(71, 'MARK X'),
	(71, 'E-TYPE'),
	(71, 'C-TYPE'),
	(71, 'D-TYPE'),
	(71, 'MARK I'),
	(71, 'MARK II'),
	(72, 'CHEROKEE'),
	(72, 'COMMANDER'),
	(72, 'COMPASS'),
	(107, 'AM1'),
	(107, 'AM2'),
	(107, 'AM3'),
	(107, 'AM4'),
	(107, 'AMV'),
	(112, 'CORDOBA'),
	(112, 'IBIZA'),
	(112, 'INCA'),
	(7, 'ACTY'),
	(7, 'AIRWAVE'),
	(7, 'ASCOT'),
	(7, 'BALLADE'),
	(7, 'BEAT'),
	(7, 'CR-X'),
	(7, 'CONCERTO'),
	(7, 'CR-Z'),
	(7, 'DOMANI'),
	(7, 'EDIX'),
	(7, 'ELEMENT'),
	(7, 'EV PLUS'),
	(7, 'FCX'),
	(7, 'FR-V'),
	(7, 'HR-V'),
	(7, 'HSC'),
	(7, 'INSIGHT'),
	(24, 'TL'),
	(7, 'LIFE DUNK'),
	(7, 'LOGO'),
	(7, 'MOBILIO'),
	(24, 'MDX'),
	(7, 'ORTHIA'),
	(7, 'PARTNER VAN'),
	(7, 'PILOT'),
	(7, 'RIDGELINE'),
	(7, 'S2000'),
	(7, 'S600'),
	(7, 'S500'),
	(7, 'S800'),
	(7, 'STEPWGN'),
	(7, 'STREAM'),
	(7, 'THATS'),
	(7, 'VAMOZ'),
	(7, 'Z'),
	(7, 'ZEST'),
	(52, 'AERIO'),
	(52, 'ALTO'),
	(52, 'APV'),
	(52, 'KEI'),
	(52, 'LAPIN'),
	(52, 'MR WAGON'),
	(52, 'XL-7'),
	(52, 'VERONA'),
	(21, '306 CABRIOLET'),
	(51, 'BELCAR'),
	(79, 'SAMARA'),
	(21, '407 SW'),
	(21, '307 CC'),
	(1, 'STYLELINE'),
	(12, 'ANGLIA'),
	(25, 'GT2'),
	(1, 'YUKON'),
	(48, 'SPORTSMAN'),
	(23, '21 NEVADA'),
	(10, 'VEYRON'),
	(9, 'ENZO'),
	(21, '306 SW'),
	(27, 'TI 80'),
	(61, 'SPYDER 550'),
	(9, '380 GTB'),
	(128, 'SEVILLE'),
	(17, 'KINGSWAY'),
	(1, 'SSR'),
	(1, 'IMPALA'),
	(21, '208'),
	(1, 'GRAND BLAZER'),
	(47, '100 SERIES'),
	(47, '200 SERIES'),
	(47, '300 SERIES'),
	(47, '66'),
	(47, '700 SERIES'),
	(47, 'AMAZON'),
	(47, 'C303'),
	(47, 'DUETT'),
	(47, 'L3314'),
	(47, 'OV 4'),
	(47, 'P1800'),
	(47, 'SUGGA'),
	(12, 'TT'),
	(5, 'ONCE'),
	(12, 'DE LUXE'),
	(12, 'CUSTOM'),
	(12, 'T-BUCKET'),
	(16, 'G15'),
	(95, 'MG6'),
	(96, 'COOPER'),
	(33, '120 CABRIO'),
	(33, '320 TOURING'),
	(33, '330 CABRIO'),
	(33, 'SERIE 5 TOURING'),
	(33, 'SERIE 6 CABRIO'),
	(33, 'SERIE M CONVERSIVEL'),
	(33, 'M5 TOURING'),
	(33, 'SERIE Z ROADSTER'),
	(12, 'KA SPORT'),
	(2, 'CC'),
	(15, 'CERATO KOUP'),
	(1, 'ASTRO'),
	(22, 'COROLLA XRS'),
	(22, 'ETIOS SEDAN'),
	(12, 'FREESTYLE'),
	(94, 'ASX'),
	(87, 'THAR'),
	(87, 'AXE'),
	(87, 'LEGEND'),
	(87, 'ARMADA'),
	(87, 'CHASSI'),
	(87, 'SCORPIO PICK-UP'),
	(73, 'TOPIC FURGAO'),
	(88, '222'),
	(88, '228'),
	(88, '3200'),
	(6, 'STAR TRUCK'),
	(6, 'STAR'),
	(6, 'STAR VAN CARGO'),
	(6, 'STAR VAN PASSAGEIROS'),
	(117, 'VEDETTE'),
	(117, 'ARONDE'),
	(117, '1200S'),
	(117, '1000'),
	(118, 'FORTWO'),
	(118, 'FORTWO CABRIO'),
	(121, 'CHAMPION'),
	(13, 'HB20X'),
	(13, 'HB20S'),
	(1, 'MONZA'),
	(1, 'CHEVETTE'),
	(84, 'BLACKWOOD'),
	(1, 'TRAX'),
	(33, '118'),
	(33, '120'),
	(33, '130'),
	(33, 'BAVARIA'),
	(33, 'C-2800'),
	(33, '318'),
	(33, '320'),
	(33, '318 CABRIO'),
	(33, '325 CABRIO'),
	(33, '530'),
	(33, '540'),
	(33, '550'),
	(33, '740'),
	(33, '750'),
	(33, '760'),
	(146, 'MP4'),
	(114, 'T20'),
	(114, 'T20 BAU'),
	(114, 'T22'),
	(115, '9000'),
	(117, 'ALVORADA'),
	(117, 'CHAMBORD'),
	(56, 'SUPER 90 COUPE'),
	(16, 'X20'),
	(16, 'ITAIPU'),
	(16, 'G800'),
	(16, 'XEF'),
	(16, 'MOTOMACHINE'),
	(16, 'BUGATO'),
	(16, 'QT'),
	(51, 'CAICARA'),
	(51, 'CARCARA'),
	(51, 'FISSORE'),
	(51, 'MALZONI'),
	(51, 'VEMAGUET'),
	(5, 'C4 LOUNGE'),
	(93, 'SABLE'),
	(123, 'RAGGE'),
	(125, 'PANTANAL'),
	(125, 'T-4'),
	(125, 'T-5'),
	(47, 'C70'),
	(47, 'C30'),
	(47, '544'),
	(47, 'S40'),
	(47, 'S60'),
	(47, 'S70'),
	(47, 'S80'),
	(47, 'V40'),
	(47, 'V50'),
	(47, 'V60'),
	(47, 'V70'),
	(47, 'S90'),
	(47, 'XC60'),
	(47, 'XC70'),
	(47, 'XC90'),
	(47, 'P1900'),
	(47, 'PV36'),
	(47, 'PV444'),
	(47, 'PV544'),
	(47, 'PV51'),
	(47, 'PV654'),
	(47, 'C50'),
	(4, '190'),
	(4, 'CLASSE CLA'),
	(4, 'CLASSE V'),
	(4, 'VANEO'),
	(4, 'CITAN'),
	(4, 'VARIO'),
	(4, 'CLASSE S CLASSICO'),
	(14, 'J3S'),
	(146, 'F1'),
	(147, 'GC2'),
	(5, 'C3 SOLARIS'),
	(5, 'C3 XTR'),
	(5, 'C4 SOLARIS'),
	(5, 'C5 BREAK/TOURER'),
	(5, 'XSARA BREAK'),
	(5, 'XSARA VTS'),
	(5, 'XANTIA BREAK'),
	(5, 'XM BREAK'),
	(5, 'C15'),
	(5, 'NEMO'),
	(5, 'VISA'),
	(5, 'C1'),
	(5, 'C2'),
	(5, 'C3 PLURIEL'),
	(5, 'DS4'),
	(5, 'DS5'),
	(5, 'JUMPY'),
	(5, 'C-CROSSER'),
	(5, 'C35'),
	(5, 'C25'),
	(5, 'CX'),
	(5, 'CX BREAK'),
	(5, 'AXEL'),
	(5, 'DYANE'),
	(5, 'GS/GSA'),
	(5, 'GS/GSA BREAK'),
	(5, 'MEHARI'),
	(5, 'SAXO'),
	(5, 'SM'),
	(5, 'ELYSEE'),
	(23, 'MASTER MINIBUS'),
	(41, 'CELER'),
	(41, 'CELER SEDAN'),
	(41, 'CIELO SEDAN'),
	(30, 'A1 SPORTBACK'),
	(30, 'A1 QUATTRO'),
	(30, 'A3 SPORTBACK'),
	(30, 'RS4 AVANT'),
	(30, 'A8L W12'),
	(30, 'R8 V10'),
	(12, 'RANGER CD'),
	(14, 'T140'),
	(33, 'X1'),
	(33, 'X3'),
	(33, 'X5'),
	(33, 'X6'),
	(33, '840'),
	(33, '850'),
	(33, '645'),
	(33, '650'),
	(7, 'FIT TWIST'),
	(147, 'EC7'),
	(12, 'MONDEO SW'),
	(12, 'ESCORT SEDAN'),
	(12, 'ESCORT CONVERSIVEL'),
	(93, 'MONTEREY'),
	(1, 'CORISCO'),
	(1, 'CHEVELLE'),
	(12, 'EXCURSION'),
	(2, 'TOURAN'),
	(12, 'F-10000'),
	(21, 'HOGGAR ESCAPADE'),
	(12, 'PHAETON'),
	(2, 'TRANSPORTER'),
	(15, 'GRAND BESTA'),
	(38, '200SX'),
	(38, '240SX'),
	(18, '300M'),
	(18, '300C TOURING'),
	(12, 'TORINO'),
	(1, 'VENTURE'),
	(1, 'FLEETLINE'),
	(1, 'FLEETMASTER'),
	(1, 'DELUXE'),
	(12, 'ESCORT XR3'),
	(1, 'MASTER'),
	(103, 'DELUXE'),
	(103, 'SERIES 60'),
	(103, 'SERIES 70'),
	(103, 'SERIES 80'),
	(103, 'SERIES 90'),
	(103, 'STARFIRE'),
	(103, '442'),
	(103, 'CUTLASS'),
	(103, 'CUTLASS SUPREME'),
	(103, 'CUTLASS SALON'),
	(103, 'CUTLASS CALAIS'),
	(103, 'CUTLASS CIERA'),
	(103, 'CUSTOM CRUISER'),
	(103, 'VISTA CRUISER'),
	(103, 'F-85'),
	(103, 'FIRENZA'),
	(103, 'ACHIEVA'),
	(103, 'ALERO'),
	(103, 'AURORA'),
	(103, 'BRAVADA'),
	(103, 'INTRIGUE'),
	(103, 'SILHOUETTE'),
	(105, 'ZONDA'),
	(106, 'GRAN VOYAGER'),
	(106, 'SUNDANCE'),
	(106, 'PROWLER'),
	(106, 'TRAIL DUSTER'),
	(106, 'VOYAGER'),
	(106, 'SCAMP'),
	(106, 'ARROW'),
	(106, 'PT50'),
	(106, 'PT57'),
	(106, 'PT81'),
	(106, 'PT105'),
	(106, 'PT125'),
	(106, 'EXPRESS'),
	(106, 'VOYAGER EXPRESSO'),
	(106, 'NEON'),
	(106, 'LASER'),
	(106, 'CARAVELLE'),
	(106, 'STATION WAGON'),
	(106, 'MODEL Q'),
	(106, 'MODEL P6'),
	(107, 'GTS'),
	(107, 'GTB'),
	(107, 'GTC'),
	(29, 'DB9 COUPE'),
	(29, 'DB9 VOLANTE'),
	(29, 'VIRAGE COUPE'),
	(29, 'RAPIDE S'),
	(29, 'V12 VANTAGE'),
	(29, 'V8 VANTAGE COUPE'),
	(29, 'V8 VANTAGE ROADSTER'),
	(29, 'V8 VANTAGE S COUPE'),
	(29, 'V8 VANTAGE S ROADSTER'),
	(29, 'VANQUISH COUPE'),
	(29, 'VANQUISH VOLANTE'),
	(29, 'V12 ZAGATO'),
	(29, 'DB5'),
	(29, 'DBS'),
	(29, 'DBS VOLANTE'),
	(29, 'CYGNET'),
	(29, 'ONE-77'),
	(29, 'DBR9'),
	(33, 'M3'),
	(33, 'M5'),
	(33, 'M6'),
	(33, 'X6 M'),
	(96, 'COUNTRYMAN'),
	(96, 'PACEMAN'),
	(96, 'JOHN COOPER WORKS'),
	(97, 'BG-TRUCK'),
	(97, 'TOPSPORT'),
	(97, 'TARGA'),
	(106, 'SUPERBIRD'),
	(8, 'NEW XV'),
	(8, 'IMPREZA WRX HATCH'),
	(8, 'IMPREZA WRX STI HATCH'),
	(8, 'IMPREZA WRX STI SEDAN'),
	(8, 'IMPREZA WRX SEDAN'),
	(22, 'ETIOS CROSS'),
	(11, 'HURACAN'),
	(2, 'UP'),
	(140, 'MINX'),
	(123, 'TR'),
	(25, 'GT'),
	(25, 'GTL'),
	(25, 'GTM'),
	(25, 'C2'),
	(25, 'CRX'),
	(25, 'AC 2000'),
	(84, 'CONTINENTAL'),
	(84, 'LS'),
	(84, 'MARK'),
	(84, 'MARK LT'),
	(84, 'MKR'),
	(84, 'MKS'),
	(84, 'MKX'),
	(84, 'MKZ'),
	(84, 'NAVIGATOR'),
	(84, 'PREMIERE'),
	(84, 'TOWN CAR'),
	(84, 'VERSAILLES'),
	(84, 'ZEPHYR'),
	(85, 'H1'),
	(86, 'ELAN'),
	(86, 'ESPRIT'),
	(1, 'CLASSIC'),
	(20, 'ACTYON'),
	(1, 'MARAJO'),
	(1, 'SUPREMA'),
	(2, 'NEW BEETLE'),
	(2, 'QUANTUM'),
	(2, 'CROSSFOX'),
	(3, 'MILLE'),
	(84, 'CAPRI'),
	(3, 'MOBI'),
	(3, 'TORO'),
	(73, 'TOPIC ESCOLAR'),
	(23, 'DUSTER OROCH'),
	(23, 'SANDERO RS'),
	(13, 'HB20R'),
	(13, 'GRAND SANTA FE'),
	(2, 'GOLF VARIANT'),
	(2, 'SPACE CROSS'),
	(21, '2008'),
	(15, 'QUORIS'),
	(15, 'GRAND CARNIVAL'),
	(14, 'T8'),
	(14, 'T6'),
	(14, 'T5'),
	(12, 'KA SEDAN'),
	(12, 'FOCUS FASTBACK');
	
INSERT INTO MARCA (`MAR_NOME`,`TIV_CODIGO`) VALUES
('HONDA',2),
('AGRALE',2),
('BMW',2),
('SUZUKI',2),
('LIFAN',2),
('MAHINDRA',2),
('SHINERAY',2),
('KASINSKI',2),
('YAMAHA',2),
('GARINNI',2),
('SUNDOWN',2),
('KAWASAKI',2),
('POLARIS',2),
('ADLY',2),
('AMAZONAS',2),
('APRILIA',2),
('ATALA',2),
('BAJAJ',2),
('BENELLI',2),
('BETA',2),
('BIMOTA',2),
('BRANDY',2),
('BRAVA',2),
('BRP',2),
('BUELL',2),
('BUENO',2),
('CAGIVA',2),
('MOBILETE',2),
('DAELIM',2),
('DAFRA',2),
('DAYANG',2),
('DAYUN',2),
('DERBI',2),
('DUCATI',2),
('EMME',2),
('FYM',2),
('GAS GAS',2),
('GREEN',2),
('HAOBAO',2),
('HARLEY-DAVIDSON',2),
('HARTFORD',2),
('HERO',2),
('HUSABERG',2),
('HUSQVARNA',2),
('IROS',2),
('JIAPENG VOLCANO',2),
('JOHNNYPAG',2),
('JONNY',2),
('KAHENA',2),
('KIMCO',2),
('LAQUILA',2),
('LANDUM',2),
('LAVRALE',2),
('LERIVO',2),
('LON-V',2),
('TRICICLO',2),
('MALAGUTI',2),
('MIZA',2),
('MOTO GUZZI',2),
('MRX',2),
('MV AUGUSTA',2),
('MVK',2),
('ORCA',2),
('PEGASSI',2),
('PIAGGIO',2),
('REGAL RAPTOR',2),
('SANYANG',2),
('SIAMOTO',2),
('TARGOS',2),
('TRAXX',2),
('VENTO',2),
('WUYANG',2),
('GARRA',2),
('X MOTOS',2),
('TRICKER',2),
('LAMBRETA',2),
('OUTROS',2),
('SCOOTER',2),
('ZONGSHEN',2),
('BIRELLI',2),
('WALK MACHINE',2),
('FBM',2),
('ARIEL',2),
('DUCAR',2),
('DITALLY',2),
('MARVA',2),
('WOLVER',2),
('KTM',2),
('LEOPARD',2),
('JAWA',2),
('BULL',2),
('CAN-AM',2),
('ACELLERA',2),
('VICTORY',2),
('INDIAN',2),
('BRAVAX',2),
('GARELLI',2),
('TRIUMPH',2),
('PEUGEOT MOTO',2);
 
 
 INSERT INTO MODELO (`MAR_CODIGO`, `MOD_NOME`) VALUES
 (148,'BIZ'),
(148,'CB 300R'),
(148,'CB 400'),
(148,'CB 500'),
(148,'CB 600 HORNET'),
(148,'CBR 1000F'),
(148,'FIREBLADE CBR'),
(148,'CBR 450'),
(148,'CBR 600'),
(148,'TWISTER CBX 250'),
(148,'CBX 750F'),
(148,'CG FAN'),
(148,'CG TITAN'),
(148,'AMERICA'),
(148,'CB 1000R'),
(148,'CB 1300'),
(148,'SUPERBLACKBIRD CBR 1100XX'),
(148,'CBR 250'),
(148,'AERO CBX 150'),
(148,'STRADA CBX 200'),
(148,'CH 125R'),
(148,'CR 85'),
(148,'GL GOLD WING'),
(148,'LEAD'),
(148,'MAGNA VF 750C'),
(148,'NX 200/250'),
(148,'NX 350 SAHARA'),
(148,'NX 400 FALCON'),
(148,'NXR BROS'),
(148,'POP 100'),
(148,'SUPER HAWK'),
(148,'TRX'),
(148,'VALKYRIE'),
(148,'VFR 1200'),
(148,'VTX'),
(148,'VARADERO XL 1000V'),
(148,'XL'),
(148,'XL 700V TRANSALP'),
(148,'XLR'),
(148,'XLX'),
(148,'VLR 350'),
(148,'XR'),
(148,'TORNADO XR 250'),
(148,'XRE 300'),
(148,'SHADOW'),
(148,'DREAM'),
(148,'HERO'),
(148,'NC 700X'),
(148,'CRF 110/150'),
(148,'FURY'),
(148,'STUNNER'),
(148,'CB 50'),
(148,'BOLDOR CB 900'),
(148,'CBF 600'),
(148,'CBR 400RR'),
(148,'CBX 1000'),
(148,'CD 125'),
(148,'ML 125'),
(148,'NSR 500'),
(148,'RC'),
(148,'PAN-EUROPEAN ST'),
(148,'TURUNA 125'),
(148,'VTR'),
(148,'AFRICA TWIN XRV 750'),
(148,'CG CARGO/JOB'),
(148,'PC 50'),
(148,'CG 125'),
(148,'PACIFIC COAST'),
(148,'CG TODAY'),
(148,'CB 125'),
(148,'DUTY'),
(148,'PCX'),
(148,'CRF 230/250'),
(148,'CRF 450'),
(148,'NX 650 DOMINATOR'),
(148,'CR 250'),
(148,'CR 125'),
(246,'BUXY'),
(246,'ELYSEO'),
(246,'SCOOTELEC'),
(246,'SPEEDAKE'),
(246,'SPEEDFIGHT'),
(246,'SQUAB'),
(246,'TREKKER'),
(246,'VIVACITY'),
(246,'ZENITH'),
(149,'ELEFANTRE'),
(149,'CITY'),
(149,'DAKAR'),
(149,'ELEFANT'),
(149,'FORCE'),
(149,'JUNIOR'),
(149,'SST'),
(149,'SUPER CITY'),
(149,'SXT'),
(149,'TCHAU'),
(149,'EXPLORER'),
(150,'G650GS'),
(150,'S1000RR'),
(150,'AIRHEAD'),
(150,'C1'),
(150,'F650GS'),
(150,'F650CS'),
(150,'F800GS'),
(150,'K75'),
(150,'K100'),
(150,'K1200R'),
(150,'K1300R'),
(150,'K1200GT'),
(150,'K1300GT'),
(150,'K1200GS'),
(150,'K1200S'),
(150,'R32'),
(150,'R51/3'),
(150,'R27'),
(150,'R60'),
(150,'R75'),
(150,'R75/5'),
(150,'R90S'),
(150,'R1200C'),
(150,'R1150GS'),
(150,'R1200RT'),
(151,'V-STROM DL'),
(151,'GSX'),
(151,'GSX-R GIXXER/SRAD'),
(151,'ADDRESS'),
(151,'BANDIT 250'),
(151,'BOULEVARD'),
(151,'BURGMAN AN'),
(151,'DR'),
(151,'FREEWIND 650'),
(151,'GS 500'),
(151,'GSR'),
(151,'INTRUDER'),
(151,'KATANA'),
(151,'LETS II'),
(151,'LT'),
(151,'MARAUDER GZ'),
(151,'RF'),
(151,'RM 250'),
(151,'SAVAGE'),
(151,'TL'),
(151,'VX 800'),
(151,'YES EN 125'),
(151,'B-KING GSX 1300'),
(151,'BANDIT 400'),
(151,'BANDIT 600'),
(151,'BANDIT GSF 750'),
(151,'BANDIT 1200'),
(151,'HAYABUSA'),
(151,'INTRUDER CUSTOM'),
(151,'RV'),
(151,'SV'),
(151,'MARAUDER VZ'),
(151,'GT'),
(151,'GSI'),
(151,'JOB'),
(151,'AE 50'),
(152,'CARGO'),
(152,'COOL STAR 125'),
(152,'X3'),
(152,'JOJO 110'),
(152,'CC125'),
(152,'CC150'),
(152,'KP150F'),
(152,'LF100'),
(152,'PK125'),
(152,'POWER KING'),
(152,'CROSS'),
(152,'GLOW 110S'),
(152,'KP150'),
(152,'POWER MAN 250'),
(152,'HIKER'),
(152,'IV'),
(152,'STEED 250'),
(152,'TRAVELLER'),
(152,'TB125'),
(152,'PK110F'),
(152,'LF125'),
(152,'LF125'),
(153,'RODEO'),
(153,'DURO'),
(154,'RACING 200'),
(154,'PHOENIX 50'),
(154,'INDIANAPOLIS 200'),
(154,'F35/F40'),
(154,'X2 250'),
(154,'MAX 150'),
(154,'NEW WAVE 125'),
(154,'EXPLORER 150'),
(154,'XY 250'),
(154,'JET 50'),
(154,'RETRO 50'),
(154,'NEW 50'),
(154,'EAGLE 50'),
(154,'SUPER SMART 50'),
(154,'NEW 200'),
(154,'CARGO 200'),
(154,'BRAVO 200'),
(154,'STRONG 250'),
(154,'FUTURE 150'),
(154,'VENICE 50'),
(154,'LIBERTY 50'),
(154,'BIKE 50'),
(154,'JET 125'),
(154,'CUSTOM 250'),
(154,'DISCOVER 250'),
(155,'CRZ'),
(155,'COMET GT 650'),
(155,'COMET GT-R 650'),
(155,'CRUISE'),
(155,'ERO'),
(155,'FLASH'),
(155,'FURIA'),
(155,'GF'),
(155,'MAGIK'),
(155,'MIDAS'),
(155,'MIRAGE'),
(155,'MOTOKAR'),
(155,'PRIMA'),
(155,'RX'),
(155,'SETA'),
(155,'SOFT'),
(155,'SUPER CAB'),
(155,'TN'),
(155,'WAY'),
(155,'WIN'),
(155,'COMET GT 250'),
(155,'COMET GT-R 250'),
(156,'CRYPTON T115'),
(156,'DT 200'),
(156,'FAZER YS250'),
(156,'FACTOR YBR125'),
(156,'YZF R1'),
(156,'VIRAGO 250'),
(156,'AXIS'),
(156,'BWS'),
(156,'JOG'),
(156,'MAJESTY'),
(156,'MT-03'),
(156,'NEO AT'),
(156,'RD 135'),
(156,'ROYAL STAR'),
(156,'TDM'),
(156,'TDR 180'),
(156,'TRX'),
(156,'TT-R'),
(156,'V-MAX 1200'),
(156,'WR 250F'),
(156,'XJR'),
(156,'XT 660R'),
(156,'XTZ 125'),
(156,'MIDNIGHT STAR XVS'),
(156,'YFS'),
(156,'RX'),
(156,'DRAG STAR'),
(156,'XJ6'),
(156,'LANDER XTZ 250'),
(156,'ALBA'),
(156,'FROG'),
(156,'LIBERO'),
(156,'GLADIATOR'),
(156,'MEST'),
(156,'ECCY'),
(156,'PASSOL'),
(156,'EC-02'),
(156,'YZ 85LW'),
(156,'V-STAR'),
(156,'PAS'),
(156,'AEROX'),
(156,'MORPHOUS'),
(156,'XF50X'),
(156,'LAGEND'),
(156,'TZR'),
(156,'QT50'),
(156,'FS1'),
(156,'FZR1000'),
(156,'XS400'),
(156,'RD 350 VIUVA NEGRA'),
(156,'XTZ 250 TENERE'),
(156,'XT 225'),
(156,'XT 660Z TENERE'),
(156,'XT 1200Z SUPER TENERE'),
(156,'FAZER FZ6'),
(156,'FAZER YS150'),
(156,'FAZER FZ1'),
(156,'YZF 600R'),
(156,'YZF R6'),
(156,'TMAX'),
(156,'YFM 700R'),
(156,'GRIZZLY'),
(156,'RD 50'),
(156,'DT 50'),
(156,'MT-01'),
(156,'FZ-09'),
(156,'MT-07'),
(156,'FZR600'),
(156,'FZR400'),
(156,'FZR250'),
(156,'XS500'),
(156,'XS650'),
(156,'XS750'),
(156,'XS850'),
(156,'XS1100'),
(156,'SR500'),
(156,'SRX 400'),
(156,'SRX 600'),
(156,'V-MAX 1680'),
(156,'FJR1300'),
(156,'XTZ 750 SUPER TENERE'),
(156,'XT 600'),
(156,'VIRAGO 400'),
(156,'VIRAGO 535'),
(156,'VIRAGO 750'),
(156,'VIRAGO 1400'),
(156,'XZ 550'),
(156,'YZR 500'),
(156,'WR 450F'),
(156,'TZ 250'),
(156,'YZ 125'),
(156,'YZ 250'),
(156,'YZ 450'),
(156,'TX500'),
(156,'XR 180'),
(156,'TT 125'),
(157,'GR 500'),
(157,'GRI 50'),
(157,'GR08T4'),
(157,'GR 125S/ST'),
(157,'GR125T3'),
(157,'GR125U'),
(157,'GR 125Z'),
(157,'GR 150P/PI'),
(157,'GR150ST'),
(157,'GR150T3'),
(157,'GR150TI'),
(157,'GR150U'),
(157,'GR250T3'),
(158,'FUTURE'),
(158,'AKROS'),
(158,'ERGON'),
(158,'FIFTY'),
(158,'HUNTER'),
(158,'MAX'),
(158,'PALIO'),
(158,'PGO'),
(158,'STX'),
(158,'STX MOTARD'),
(158,'SUPER FIFTY'),
(158,'VBLADE'),
(158,'WEB'),
(158,'WEB EVO'),
(245,'ADVENTURER'),
(245,'BONNEVILLE'),
(245,'DAYTONA'),
(245,'LEGEND'),
(245,'ROCKET'),
(245,'SCRAMBLER'),
(245,'SPEED TRIPLE'),
(245,'SPRINT'),
(245,'STREET TRIPLE'),
(245,'THRUXTON'),
(245,'THUNDERBIRD'),
(245,'TIGER'),
(245,'TRIDENT'),
(245,'TROPHY'),
(245,'TT-600'),
(159,'AVAJET'),
(159,'CONCOURS'),
(159,'D-TRACKER'),
(159,'ER-6N'),
(159,'KLX 110/140'),
(159,'KX 100/125'),
(159,'KZ'),
(159,'MAXI'),
(159,'NINJA 250'),
(159,'VERSYS'),
(159,'VOYAGER'),
(159,'VULCAN'),
(159,'ZZR'),
(159,'BAIO'),
(159,'KDX'),
(159,'ELIMINATOR'),
(159,'GTR'),
(159,'KLE'),
(159,'KLR'),
(159,'SUPER SHERPA'),
(159,'ZRX'),
(159,'ZR'),
(159,'NINJA 1400'),
(159,'NINJA 1000'),
(159,'NINJA 600'),
(159,'NINJA 300'),
(159,'NINJA 1100'),
(159,'NINJA 900'),
(159,'NINJA 700'),
(159,'NINJA 1200'),
(159,'Z1000'),
(159,'Z750'),
(159,'Z800'),
(159,'VERSYS CITY'),
(159,'VERSYS GRAND TOURER'),
(159,'VERSYS TOURER'),
(159,'VERSYS 1000'),
(159,'KX 450/500'),
(159,'KX 250'),
(159,'KX 65/85'),
(159,'KLX 400/450'),
(159,'KLX 250/300'),
(159,'ER-5'),
(160,'RZR 570'),
(160,'RZR 800'),
(160,'RZR 800 XC'),
(160,'RZR S 800'),
(160,'RZR 900'),
(160,'RZR XP 1000'),
(160,'RZR 4 800'),
(160,'RZR 4 900'),
(160,'RZR 4 1000'),
(160,'RANGER 6X6'),
(160,'RANGER 800'),
(160,'RANGER DIESEL'),
(160,'RANGER XP 900'),
(160,'RANGER 400'),
(160,'RANGER 570'),
(160,'RANGER 800 MIDSIZE'),
(160,'RANGER EV'),
(160,'RANGER CREW 570'),
(160,'RANGER CREW 800'),
(160,'RANGER CREW DIESEL'),
(160,'RANGER CREW 900'),
(160,'SPORTSMAN 550 EPS'),
(160,'SPORTSMAN 570'),
(160,'SPORTSMAN 400 HO'),
(160,'SPORTSMAN WV850 HO'),
(160,'SPORTSMAN XP 850'),
(160,'SPORTSMAN 800 EFI'),
(160,'SPORTSMAN BIG BOSS 6X6 800'),
(160,'SCRAMBLER XP 850 HO'),
(160,'SCRAMBLER XP 1000 EPS'),
(160,'SPORTSMAN TOURING 850'),
(160,'SPORTSMAN TOURING 570'),
(160,'SPORTSMAN TOURING 550'),
(160,'SPORTSMAN X2 550'),
(160,'PHOENIX 200'),
(160,'RZR 170'),
(160,'OUTLAW 90'),
(160,'SPORTSMAN 90'),
(160,'OUTLAW 50'),
(160,'BRUTUS'),
(161,'ATV'),
(161,'JAGUAR'),
(161,'RT'),
(162,'AME-110'),
(162,'AME-150'),
(162,'AME-250'),
(162,'AME-LX'),
(163,'AREA-51'),
(163,'CLASSIC'),
(163,'LEONARDO'),
(163,'MOTO'),
(163,'PEGASO'),
(163,'RALLY'),
(163,'RS'),
(163,'RSV MILLE'),
(163,'RX'),
(163,'SCARABEO'),
(163,'SONIC'),
(163,'SR RACING'),
(163,'SR WWW'),
(163,'SR'),
(164,'CALIFFONE'),
(164,'MASTER'),
(164,'SKEGIA'),
(165,'CHAMPION'),
(165,'CLASSIC'),
(166,'TNT'),
(166,'TRE'),
(167,'MX-50'),
(168,'DB4'),
(168,'DB5R'),
(168,'DB6'),
(168,'DB6R'),
(168,'DB7'),
(168,'MANTRA'),
(168,'SBR8'),
(168,'TESI'),
(169,'ELEGANT'),
(169,'FOSTI'),
(169,'PISTA'),
(169,'TURBO'),
(169,'ZANELLA'),
(170,'YB'),
(170,'ALPINA'),
(170,'APOLO'),
(170,'NEVADA'),
(170,'WINSTAR'),
(170,'ELEKTRA'),
(170,'TEXANA'),
(170,'ALTINO'),
(170,'AQUILA'),
(170,'DAYSTAR ROUTIER'),
(171,'CAN-AM'),
(172,'1125'),
(172,'FIREBOLT'),
(172,'LIGHTNING'),
(172,'ULYSSES'),
(173,'JBR'),
(174,'CANYON'),
(174,'ELEFANT'),
(174,'GRAN CANYON'),
(174,'MITO'),
(174,'NAVIGATOR'),
(174,'PLANET'),
(174,'ROADSTER'),
(174,'V-RAPTOR'),
(174,'W-16'),
(175,'MOBILETE'),
(176,'ALTINO'),
(176,'MESSAGE'),
(176,'VC'),
(176,'VF'),
(176,'VS'),
(176,'VT'),
(176,'VX'),
(177,'APACHE'),
(177,'CITYCOM'),
(177,'KANSAS'),
(177,'LASER'),
(177,'NEXT'),
(177,'RIVA'),
(177,'ROADWIN'),
(177,'SMART'),
(177,'SPEED'),
(177,'SUPER'),
(177,'ZIG'),
(178,'DY100-31'),
(178,'DY110-20'),
(178,'DY125-18'),
(178,'DY125-36A'),
(178,'DY125-37'),
(178,'DY125-5'),
(178,'DY125-52'),
(178,'DY150-12'),
(178,'DY200'),
(178,'DY50'),
(179,'SUMMER'),
(179,'DY125-8'),
(179,'SPACE'),
(179,'PHANTON'),
(179,'CITY'),
(179,'DY150GY'),
(179,'CARGO 150'),
(179,'CARGO 200'),
(179,'DY250-2'),
(180,'ATLANTIS'),
(180,'GPR'),
(180,'PREDATOR'),
(180,'RED BULLET'),
(180,'REPLICAS'),
(180,'SENDA'),
(181,'1098'),
(181,'1198'),
(181,'749'),
(181,'848'),
(181,'996'),
(181,'998'),
(181,'999'),
(181,'999R'),
(181,'DESMOSEDICI'),
(181,'DIAVEL'),
(181,'HYPERMOTARD'),
(181,'MONSTER'),
(181,'MULTISTRADA'),
(181,'SPORTCLASSIC'),
(181,'SS'),
(181,'ST-2'),
(181,'ST-4'),
(181,'STREETFIGHTER'),
(182,'MIRAGE'),
(182,'ONE'),
(182,'ONE RACING'),
(183,'FY100-10A'),
(183,'FY125-19'),
(183,'FY125-20'),
(183,'FY125EY-2'),
(183,'FY125Y-3'),
(183,'FY150-3'),
(183,'FY150T-18'),
(183,'FY250'),
(184,'ENDUCROSS'),
(184,'ROOKIE ENDURO'),
(184,'BOY'),
(184,'MC'),
(184,'PAMPERA'),
(184,'TX BOY'),
(184,'TXT'),
(184,'TXT ROOKIE'),
(185,'EASY'),
(185,'RUNNER'),
(185,'SAFARI'),
(185,'SPORT'),
(186,'HB-110'),
(186,'HB-125'),
(186,'HB-150'),
(186,'HB-50'),
(187,'SOFTAIL'),
(187,'TOURING ROAD KING'),
(187,'TOURING ELECTRA GLIDE'),
(187,'DYNA'),
(187,'V-ROD'),
(187,'SPORTSTER'),
(187,'TOURING STREET GLIDE'),
(187,'CVO'),
(188,'LEGION'),
(189,'ANKUR'),
(189,'PUCH'),
(189,'STREAM'),
(190,'FE'),
(191,'CR'),
(191,'HUSQY'),
(191,'SM'),
(191,'SMR'),
(191,'TC'),
(191,'TE'),
(191,'WR'),
(191,'WRE'),
(192,'ACTION'),
(192,'BRAVE'),
(192,'MATRIX'),
(192,'MOVING'),
(192,'ONE'),
(192,'VINTAGE'),
(192,'WARRIOR'),
(192,'IZY 50'),
(193,'JP'),
(194,'BARHOG'),
(194,'PROSTREET'),
(194,'SPYDER'),
(195,'ATLANTIC'),
(195,'HYPE'),
(195,'JONNY'),
(195,'ORBIT'),
(195,'PEGASUS'),
(195,'QUICK'),
(195,'TR'),
(195,'NAKED'),
(195,'RACER'),
(195,'TEXAS'),
(196,'TOP'),
(196,'DUAL'),
(196,'AMAZONAS'),
(197,'DJW'),
(197,'MBOY'),
(197,'PEOPLE'),
(197,'ZING'),
(198,'AKROS'),
(198,'ERGON'),
(198,'NIX'),
(199,'MOTO TRUCK'),
(200,'QUATTOR'),
(201,'FORMIGAO'),
(202,'DE LUXE'),
(202,'E RETRO'),
(202,'LY'),
(203,'TRICICLO'),
(204,'CIAK'),
(204,'SPIDER'),
(205,'DRAGO'),
(205,'EASY'),
(205,'FAST'),
(205,'SKEMA'),
(205,'VITE'),
(206,'CALIFORNIA'),
(206,'QUOTA'),
(206,'V11'),
(206,'LE MANS'),
(207,'230R'),
(208,'BRUTALE'),
(208,'F4'),
(209,'BIG FORCE'),
(209,'BLACK STAR'),
(209,'BRX'),
(209,'DUAL'),
(209,'FENIX GOLD'),
(209,'FOX'),
(209,'GO'),
(209,'HALLEY'),
(209,'FENIX'),
(209,'JURASIC'),
(209,'MA'),
(209,'SIMBA'),
(209,'SPORT'),
(209,'SPYDER'),
(209,'STREET'),
(209,'SUPER'),
(209,'XRT'),
(210,'AX'),
(210,'JC'),
(210,'QM'),
(211,'BR III'),
(212,'BEVERLY'),
(212,'LIBERTY'),
(212,'MP3'),
(212,'NRG'),
(212,'RUNNER'),
(212,'VESPA'),
(212,'ZIP'),
(213,'BLACK JACK'),
(213,'FENIX GOLD'),
(213,'GHOST'),
(213,'SPYDER'),
(214,'ENJOY'),
(214,'HUSKY'),
(214,'PASSING'),
(215,'SCROSS'),
(216,'TRIMOTO'),
(217,'CJ'),
(217,'JH'),
(217,'JL'),
(217,'STAR 50'),
(217,'WORK 125'),
(217,'SKY 125'),
(217,'FLY 135'),
(217,'BEST'),
(217,'SHARK'),
(217,'MOBY 50'),
(217,'JOTO 135'),
(217,'MONTEZ 250'),
(217,'DUNNA 600'),
(217,'VICO'),
(218,'REBELLIAN'),
(218,'TRITON'),
(218,'VTHUNDER'),
(219,'WY'),
(221,'XM'),
(223,'LAMBRETA'),
(225,'SCOOTER'),
(226,'ZS 200'),
(226,'125 ZS'),
(227,'BW 125'),
(228,'WALK MACHINE'),
(229,'MZ 250'),
(229,'KAPRA'),
(229,'RALLYE'),
(229,'PASSEIO'),
(230,'RED HUNTER'),
(230,'W/NG'),
(230,'M2F'),
(230,'GOLDEN ARROW'),
(231,'150'),
(232,'WAKE 500'),
(232,'OUTCROSS'),
(232,'JOY PLUS'),
(232,'PASSION'),
(232,'TREND'),
(232,'PIT BULL'),
(232,'BRUTTUS'),
(233,'UFO 50'),
(233,'LF250ST'),
(233,'FOX 150R'),
(233,'LF400ST'),
(233,'VR 150 WIND'),
(233,'HERCULES 200'),
(233,'ONIX 50R'),
(233,'HS 150 FIRE'),
(234,'KN 150'),
(234,'KN 125 S'),
(234,'KN 50 S'),
(235,'50 SX'),
(235,'65 SX'),
(235,'85 SX'),
(235,'125 SX'),
(235,'150 SX'),
(235,'250 SX'),
(235,'350 SX'),
(235,'450 SX-F'),
(235,'350 EXC'),
(235,'500 EXC'),
(235,'150 XC'),
(235,'250 XC'),
(235,'300 XC'),
(235,'450 XC-F/W'),
(235,'200 XC'),
(235,'690 ENDURO R'),
(235,'1190 ADVENTURE'),
(235,'990 SM T'),
(235,'990 ADVENTURE BAJA'),
(235,'690 DUKE'),
(235,'1190 RC8 R'),
(235,'525 XC'),
(235,'505 SX'),
(235,'450 XC'),
(235,'450 SX'),
(238,'HB'),
(239,'OUTLANDER 400'),
(239,'OUTLANDER 500'),
(239,'OUTLANDER 650'),
(239,'OUTLANDER 800R'),
(239,'OUTLANDER MAX 400'),
(239,'OUTLANDER MAX 500'),
(239,'OUTLANDER MAX 650'),
(239,'OUTLANDER MAX 800R'),
(239,'OUTLANDER MAX LTD 800R'),
(239,'RENEGADE 500'),
(239,'RENEGADE 800R'),
(239,'DS 250'),
(239,'DS 450'),
(239,'DS 90'),
(239,'RT'),
(239,'RS'),
(240,'ACX 250F'),
(240,'HOTZOO SPORT 90'),
(240,'SPORTLANDER 150R'),
(240,'SPORTLANDER 250XR'),
(240,'SPORTLANDER 350ZX'),
(240,'SPORTLANDER 450TR'),
(240,'QUADRILANDER 300'),
(240,'QUADRILANDER 400'),
(240,'QUADRILANDER 600'),
(240,'FRONTLANDER 500'),
(240,'FRONTLANDER 800 EFI'),
(241,'CROSS ROADS 8-BALL'),
(241,'CROSS ROADS CLASSIC'),
(241,'CROSS COUNTRY 8-BALL'),
(241,'CROSS COUNTRY'),
(241,'NESS CROSS COUNTRY'),
(241,'VISION TOUR'),
(241,'CROSS COUNTRY TOUR'),
(241,'CROSS COUNTRY TOUR ANNIVERSARY'),
(241,'VEGAS 8-BALL'),
(241,'HIGH-BALL'),
(241,'HAMMER 8-BALL'),
(241,'BOARDWALK'),
(241,'JACKPOT'),
(241,'JUDGE'),
(241,'ZACH NESS CROSS COUNTRY'),
(241,'HARD-BALL'),
(241,'NESS VISION'),
(241,'CORY NESS CROSS COUNTRY TOUR'),
(242,'CHIEF CLASSIC'),
(242,'CHIEF VINTAGE'),
(242,'CHIEFTAIN'),
(243,'F2 PLUS'),
(243,'SUPER F2'),
(243,'SUPER F1'),
(243,'SUPER F3'),
(243,'BVX 125'),
(243,'BVX 130 STREET'),
(243,'BVX 150 S1'),
(243,'TR 150 D6'),
(243,'AX 200'),
(243,'BVX 200'),
(243,'BX 260'),
(243,'BR 250 D1'),
(243,'CARGO 260');	

INSERT INTO UFE (`UFE_CODIGO`, `UFE_NOME`, `UFE_SIGLA`, `UFE_ATIVO`) VALUES
	(1, 'ACRE', 'AC', 1),
	(2, 'ALAGOAS', 'AL', 1),
	(3, 'AMAZONAS', 'AM', 1),
	(4, 'AMAPÁ', 'AP', 1),
	(5, 'BAHIA', 'BA', 1),
	(6, 'CEARÁ', 'CE', 1),
	(7, 'DISTRITO FEDERAL', 'DF', 1),
	(8, 'ESPÍRITO SANTO', 'ES', 1),
	(9, 'GOIÁS', 'GO', 1),
	(10, 'MARANHÃO', 'MA', 1),
	(11, 'MINAS GERAIS', 'MG', 1),
	(12, 'MATO GROSSO DO SUL', 'MS', 1),
	(13, 'MATO GROSSO', 'MT', 1),
	(14, 'PARÁ', 'PA', 1),
	(15, 'PARAÍBA', 'PB', 1),
	(16, 'PERNAMBUCO', 'PE', 1),
	(17, 'PIAUÍ', 'PI', 1),
	(18, 'PARANÁ', 'PR', 1),
	(19, 'RIO DE JANEIRO', 'RJ', 1),
	(20, 'RIO GRANDE DO NORTE', 'RN', 1),
	(21, 'RONDÔNIA', 'RO', 1),
	(22, 'RORAIMA', 'RR', 1),
	(23, 'RIO GRANDE DO SUL', 'RS', 1),
	(24, 'SANTA CATARINA', 'SC', 1),
	(25, 'SERGIPE', 'SE', 1),
	(26, 'SÃO PAULO', 'SP', 1),
	(27, 'TOCANTINS', 'TO', 1);