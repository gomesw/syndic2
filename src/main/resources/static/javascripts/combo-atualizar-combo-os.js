var Sgp = Sgp || {};

Sgp.ComboOs = (function(){
	//metodo construtor de estado
	
	function ComboOs(){
		this.combo = $("#tipo");
		//this.atualizar = this.combo.data('atualizar');//qual combo a combo estado vai atualizar
		this.comboParaAtualizar = $("#subtipoordemservico");
		
	}
	
	ComboOs.prototype.iniciar = function(){
		
		this.combo.on('change',onEstadoAlterado.bind(this));
	}
	
	function onEstadoAlterado(event){
		event.preventDefault();
		var comboAlterada = $(event.currentTarget);
		
		ComboCidade(comboAlterada);
		
	}
	
	function ComboCidade(comboAlterada){
		
		this.combo = comboAlterada;
		this.imgLoading = $(".js-img-loading");
		console.log($("#subtipoordemservico").attr('url'));
		atualizarComboReferencia(comboAlterada.val());
	}
	
	
	function atualizarComboReferencia(codigoEstado){
		if(codigoEstado){
			var resposta = $.ajax({
				url : $("#subtipoordemservico").attr('url'),
				method:'GET',
				contentType:'application/json',
				data:{'id':codigoEstado},
				beforeSend:iniciarRequisicao.bind(this),
				complete:finalizarRequisicao.bind(this)
			});
			
			resposta.done(onBuscarCidadesFinalizado.bind(this));
		}
		else{
			reset.call(this);
		}
	}
	
	
	//carrega o options de cidade	
	function onBuscarCidadesFinalizado(cidades){
		var options = [];
		
		options.push('<option disabled selected >SELECIONE O TIPO DE ORDEM DE SERVIÇO *</option>');
		
		cidades.forEach(function(cidade){
			
			options.push('<option value="'+cidade.id+'" >'+cidade.nome+' </option>');
		});
		
		
		$("#subtipoordemservico").html(options.join('')).selectpicker('refresh');
	}
	
	
	//reseta a combo para estágio inicial
	function reset(){
		$('.selectpicker').selectpicker('refresh');
		
	}
	
	//quando inicia a requisição
	function iniciarRequisicao(){
		reset.call(this);
		this.imgLoading.show();
	}
	
	
	//quando finaliza a requisição
	function finalizarRequisicao(){
		this.imgLoading.hide();
	}
	
	
	
	return ComboOs;
	
}());


//Inicializando objetos
$(function(){
	
	var comboOs = new Sgp.ComboOs();
	comboOs.iniciar();
	
	
	
});