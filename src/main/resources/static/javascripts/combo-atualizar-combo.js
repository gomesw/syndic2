var Sgp = Sgp || {};

Sgp.ComboEstado = (function(){
	//metodo construtor de estado
	
	function ComboEstado(){
		this.combo = $(".atualizar-auto");
		//this.atualizar = this.combo.data('atualizar');//qual combo a combo estado vai atualizar
		this.comboParaAtualizar ;
		
	}
	
	ComboEstado.prototype.iniciar = function(){
		this.combo.on('change',onEstadoAlterado.bind(this));
	}
	
	function onEstadoAlterado(event){
		event.preventDefault();
		var comboAlterada = $(event.currentTarget);
		
		ComboCidade(comboAlterada);
		
	}
	
	function ComboCidade(comboAlterada){
		
		this.combo = comboAlterada;
		this.comboParaAtualizar = $("."+comboAlterada.attr('atualizar'));
		
		this.imgLoading = $(".js-img-loading");
		
		atualizarComboReferencia(comboAlterada.val());
	}
	
	
	function atualizarComboReferencia(codigoEstado){
		
		if(codigoEstado){
			var resposta = $.ajax({
				url :this.comboParaAtualizar.attr('url'),
				method:'GET',
				contentType:'application/json',
				data:{'id':codigoEstado},
				beforeSend:iniciarRequisicao.bind(this),
				complete:finalizarRequisicao.bind(this)
			});
			
			resposta.done(onBuscarCidadesFinalizado.bind(this));
		}
		else{
			reset.call(this);
		}
	}
	
	
	//carrega o options de cidade	
	function onBuscarCidadesFinalizado(cidades){
		var options = [];
		
		options.push('<option value="">----------</option>');
		
		cidades.forEach(function(cidade){
			
			options.push('<option value="'+cidade.id+'" >'+cidade.nome+' </option>');
		});
		
		
		this.comboParaAtualizar.html(options.join(''));
		this.comboParaAtualizar.removeAttr('disabled');
	}
	
	
	//reseta a combo para estágio inicial
	function reset(){
		this.comboParaAtualizar.html('<option value="">----------</option>');
		this.comboParaAtualizar.val('');
		this.comboParaAtualizar.attr('disabled','disabled');
		this.comboParaAtualizar.attr('aria-required','false');
		
	}
	
	//quando inicia a requisição
	function iniciarRequisicao(){
		reset.call(this);
		this.imgLoading.show();
	}
	
	
	//quando finaliza a requisição
	function finalizarRequisicao(){
		this.imgLoading.hide();
	}
	
	
	
	return ComboEstado;
	
}());


//Inicializando objetos
$(function(){
	
	var comboEstado = new Sgp.ComboEstado();
	comboEstado.iniciar();
	
	
	
});