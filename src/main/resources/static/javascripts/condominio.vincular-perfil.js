var Syndic = Syndic || {};


Syndic.VicularPerfil = (function(){

	function VicularPerfil(){

		this.chkAlterada = $(".chk-selecionado");

	}

	VicularPerfil.prototype.iniciar = function (){

		$("input[type=checkbox]").click(function(e){
			if($(this).is(':checked')){
				onChkInserir(e);
				return;
			}
			else{
				onChkDeletar(e);

				return;
			}
		});

	}



	function onChkInserir(event){
		event.preventDefault();
		chkAlterada = $(event.currentTarget);



		var perfilRecurso = {
				perfil:chkAlterada.attr("id"),
				id:chkAlterada.data("condominioperfil"),
				condominio:chkAlterada.data('condominio')
		};


		$.ajax({
			url: "/perfil/inserirperfilcondominio",
			method: 'POST',
			beforeSend: adicionarCsrfToken,
			contentType: 'application/json',
			data: JSON.stringify(perfilRecurso),
			error: onErroInserindo.bind(this),
			success: onSalvoSucesso.bind(this)
		});
		return;
	}

	function onChkDeletar(event){
		event.preventDefault();

		chkAlterada = $(event.currentTarget);


		var perfilRecurso = {
				perfil:chkAlterada.attr("id"),
				id:chkAlterada.data("condominioperfil"),
				condominio:chkAlterada.data('condominio')
		};

		$.ajax({
			url: "/perfil/excluirperfilcondominio",
			method: 'POST',
			beforeSend: adicionarCsrfToken,
			contentType: 'application/json',
			data: JSON.stringify(perfilRecurso),
			error: onErroExcluindo.bind(this),
			success: onExcluidoSucesso.bind(this)

		});
		return;
	}

	function onExcluidoSucesso(obj) {

		chkAlterada.attr('data-condominioperfil',0);
		chkAlterada.removeAttr('checked');
		return;
	}

	function onErroExcluindo(obj) {
		console.log("Erro Ao excluir",obj);
		return;
	}

	function onSalvoSucesso(obj) {
		this.chkAlterada.prop('checked',true);

		chkAlterada.attr('data-condominioperfil',obj.id);

		return;
	}

	function onErroInserindo(obj) {

		return;
	}

	function adicionarCsrfToken(xhr) {
		
		var token = $('input[name=_csrf]').val();
		xhr.setRequestHeader('x-csrf-token', token);
	}

	return VicularPerfil;

}());




$(function(){
	var vicularPerfil = new Syndic.VicularPerfil();
	vicularPerfil.iniciar();
});