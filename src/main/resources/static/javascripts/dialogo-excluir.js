var Sgpol = Sgpol || {};

Sgpol.DialogoExcluir = (function() {
	
	function DialogoExcluir() {
		this.exclusaoBtn = $('.btn-excluir')
	}
	
	DialogoExcluir.prototype.iniciar = function() {
		this.exclusaoBtn.on('click', onExcluirClicado.bind(this));
		
		if (window.location.search.indexOf('excluido') > -1) {
			swal('Pronto!', 'Excluído com sucesso!', 'success');
		}
	}
	
	function onExcluirClicado(evento) {
		evento.preventDefault();
		var botaoClicado = $(evento.currentTarget);
		var url = botaoClicado.data('url');
		var id = botaoClicado.data('id');
		var retorno = botaoClicado.data('retorno');
		var object = JSON.stringify({ id : id });
		console.log(url);
		
		swal({
			title : "Voce deseja excluir esse registro?",
			text : "Você não será mais capaz de recuperar esse arquivo!",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#DD6B55",
			confirmButtonText : "Sim, Delete!",
			cancelButtonText : "Não, Cancele",
			closeOnConfirm : false,
			showLoaderOnConfirm: true,
			closeOnCancel: true
		}, onExcluirConfirmado.bind(this, url, retorno, object));
	}
	
	function onExcluirConfirmado(url, retorno, object) {
		console.log(url);
		$.ajax({
			url: url,
			method: 'DELETE',
			beforeSend:adicionarCsrfToken,
			dataType: 'json',
			contentType: 'application/json',
			data: object,
			success: onExcluidoSucesso(retorno),
			error: onErroExcluir.bind(this)
		});
	}
	
	function onExcluidoSucesso(retorno) {
			
			swal({
					  title:"Deletado!",
					  text:"Registro - Deletado com sucesso.",
					  type :"success",
					  timer:1000,
					  showConfirmButton:false
			  	},
			  	function(){
			  		window.location = retorno;
			  
				});
						 
	}
	
	function onErroExcluir(e) {
		swal('Oops!', e.responseText, 'error');
	}
	
	function adicionarCsrfToken(xhr) {
		var token = $('input[name=_csrf]').val();
		xhr.setRequestHeader('x-csrf-token', token);
	}
	
	return DialogoExcluir;
	
}());

$(function() {
	var dialogo = new Sgpol.DialogoExcluir();
	dialogo.iniciar();
});

