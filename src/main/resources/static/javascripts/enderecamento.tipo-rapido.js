var Syndic = Syndic || {};


Syndic.TipoEndereco = (function(){
	
	function TipoEndereco(){
		this.modal = $('#ModalEndereco');
		this.botaoSalvar = this.modal.find('.js-modal-endereco-btn-salvar');
		this.form = this.modal.find('form');
		this.url = this.form.attr('action');
		this.inputDescricaoDesligamento = $('.js-input-descricao-endereco');
		this.containerMensagemErro = $('.js-mensagem-endereco');	
		this.inputCodigoCondominio = $('.js-input-codigo-condominio')
		this.btnCloseModal = $('.js-close-modal-endereco');
		
		
	}
	
	TipoEndereco.prototype.iniciar = function (){
		
		this.form.on('submit', function(event) { event.preventDefault() });
		this.modal.on('shown.bs.modal', onModalShow.bind(this));
		this.botaoSalvar.on('click', onBotaoSalvarClick.bind(this));
		this.btnCloseModal.on('click', onModalClose.bind(this));
		
	}
	
	
	function onModalShow() {
		this.inputDescricaoDesligamento.focus();
	}

	function onModalClose() {
		
		this.inputDescricaoDesligamento.val('');
		this.containerMensagemErro.addClass('hidden');
		this.form.find('.form-group').removeClass('has-error');
		
	}


	function onBotaoSalvarClick() {
		
		
		var descricaoEndereco = this.inputDescricaoDesligamento.val();
		var codigo = this.inputCodigoCondominio.val();
		
		
		 var endereco = {
		            condominio: codigo,
		            id: 0,
		            descricao: descricaoEndereco,
		  };
		
		$.ajax({
			url: this.url,
			method: 'POST',
			contentType: 'application/json',
			beforeSend:adicionarCsrfToken,
			data: JSON.stringify(endereco),
			error: onErroSalvando.bind(this),
			success: onSalvo.bind(this)
		});
	}


	function onErroSalvando(obj) {
		var mensagemErro = obj.responseText;
		this.containerMensagemErro.removeClass('hidden');
		this.containerMensagemErro.html('<span>' + mensagemErro + '</span>');
		this.form.find('.form-group').addClass('has-error');
	}

	function onSalvo(endereco) {
		$(window.document.location).attr('href','../incluir/'+this.inputCodigoCondominio.val());
	}

	
	function adicionarCsrfToken(xhr) {
		var token = $('input[name=_csrf]').val();
		xhr.setRequestHeader('x-csrf-token', token);
	}
	
	return TipoEndereco;
	
}());




$(function(){
	var tipoEndereco = new Syndic.TipoEndereco();
	tipoEndereco.iniciar();
});