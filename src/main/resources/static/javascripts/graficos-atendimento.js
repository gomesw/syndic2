var Syndic = Syndic || {};

Syndic.AtendimentoMesLinha = (function(){

	function AtendimentoMesLinha(){

		this.ctx = $('#graficoServicoPorMes')[0].getContext('2d');

	}

	AtendimentoMesLinha.prototype.iniciar = function(){

		$.ajax({
			url: 'pormes',
			method: 'GET', 
			success: onDadosRecebidos.bind(this)
		});


		function onDadosRecebidos(atendimentoMes) {
			var meses = [];
			var valores = [];

			atendimentoMes.forEach(function(obj) {
				meses.unshift(obj.mes);
				valores.unshift(obj.total);
			});

			var graficoAtendimentosPorMes = new Chart(this.ctx, {
				type: 'line',
				data: {
					labels: meses,
					datasets: [{
						label: 'Atendimentos',
						backgroundColor: "rgba(26,179,148,0.5)",
						pointBorderColor: "rgba(26,179,148,1)",
						pointBackgroundColor: "#fff",
						data: valores
					}]
				},
			});
		}

	}

	return AtendimentoMesLinha;

}());




Syndic.AtendimentoMesPizza = (function(){

	function AtendimentoMesPizza(){

		this.ctx =  $('#graficoServicoPorMesPizza')[0].getContext('2d'); //document.getElementById("graficoServicoPorMesPizza");

	}

	AtendimentoMesPizza.prototype.iniciar = function(){

		$.ajax({
			url: 'pormespizza',
			method: 'GET', 
			success: onDadosRecebidosPizza.bind(this)
		});


		function onDadosRecebidosPizza(atendimentoMes) {
			var status = [];
			var valores = [];
			var cores = [];

			
			var chartColors = window.chartColors;
		    var color = Chart.helpers.color;
			
			atendimentoMes.forEach(function(obj) {
				status.unshift(obj.status);
				valores.unshift(obj.total);
				cores.unshift(color(obj.cor).alpha(0.5).rgbString());
			});
			
			
			
			var config = {
			        data: {
			            datasets: [{
			                data: valores,
			                backgroundColor: cores,
			                label: 'My dataset' // for legend
			            }],
			            labels: status
			        },
			        options: {
			            responsive: true,
			            legend: {
			                position: 'right',
			            },
			            title: {
			                display: false,
			                text: 'Atendimentos dos Últimos 06 Meses'
			            },
			            scale: {
			              ticks: {
			                beginAtZero: true
			              },
			              reverse: false
			            },
			            animation: {
			                animateRotate: true,
			                animateScale: true
			            }
			        }
			    };
		
				var graficoAtendimentosPorMesPizza = Chart.PolarArea(this.ctx, config);
			
		}

	}

	return AtendimentoMesPizza;

}());






Syndic.GraficoAtendimentoPorCategoria = (function() {
	
	function GraficoAtendimentoPorCategoria() {
		this.ctx = $('#graficoServicoPorCategoria')[0].getContext('2d');
	}
	
	GraficoAtendimentoPorCategoria.prototype.iniciar = function() {
		$.ajax({
			url: 'pormestipo',
			method: 'GET', 
			success: onDadosRecebidos.bind(this)
		});
	}
	
	function onDadosRecebidos(atendimentoTipos) {
		var meses = [];
		var tipos = [];
		var cores = [];
		
		var valores = [];
		
		atendimentoTipos.forEach(function(obj) {
			tipos.unshift(obj.tipo);
			cores.unshift(obj.cor);
			valores.unshift(obj.quantidade);
			meses.unshift(obj.mes);
		});
		
		cores.forEach(function(ti) {
			console.log(ti);
		});
		
		
		
		var chartColors = window.chartColors;
	    var color = Chart.helpers.color;
		
	    
	    var arradatasets = new Array();
	    
	    var objConfig = new Object();
	    
	    
	    for (i = 0; i < tipos.length; i++) {
	    	
	    	var objConfig = new Object();
	    	objConfig.label = tipos[i];
		    objConfig.backgroundColor = color(cores[i]).alpha(0.5).rgbString();
		    objConfig.data = valores[i];
		   
		    arradatasets.unshift(objConfig);
	    	
	    }

	    
	    var config = new Chart(this.ctx, {
		    type: 'bar',
		    data: {
		    	labels: meses[0],
		    	datasets: arradatasets
		    },
		});
		
		  
		
	}
	
	return GraficoAtendimentoPorCategoria;
	
}());



$(function(){
	var atendimentoMes = new Syndic.AtendimentoMesLinha();
	atendimentoMes.iniciar();
	
	var graficoAtendimentoPorCategoria = new Syndic.GraficoAtendimentoPorCategoria();
	graficoAtendimentoPorCategoria.iniciar();
	
	

	var atendimentoMesPizza = new Syndic.AtendimentoMesPizza();
	atendimentoMesPizza.iniciar();

});