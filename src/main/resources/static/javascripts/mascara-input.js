var Syndic = Syndic || {};


Syndic.MascaraAno = (function(){
	
	function MascaraAno(){
		
		this.inputAno = $(".ano-mask");
		this.inputAno.mask('0000');
		
	}
	
	MascaraAno.prototype.iniciar = function(){
		
	}
	
	return MascaraAno;
	
}());

Syndic.MascaraPlaca = (function(){
	
	function MascaraPlaca(){
		
		this.inputPlaca = $(".placa-mask");
		this.inputPlaca.mask('AAA-0000');
		
	}
	
	MascaraPlaca.prototype.iniciar = function(){
		
	}
	
	return MascaraPlaca;
	
}());

Syndic.MascaraCpf = (function(){
	
	function MascaraCpf(){
		
		this.inputCpf = $(".cpf-mask");
		this.inputCpf.mask('000.000.000-00');
		
	}
	
	MascaraCpf.prototype.iniciar = function(){
		
	}
	
	return MascaraCpf;
	
}());


Syndic.MascaraCNPJ = (function(){
	
	function MascaraCNPJ(){
		
		this.inputCNPJ = $(".cnpj-mask");
		this.inputCNPJ.mask('00.000.000/0000-00');
		
	}
	
	MascaraCNPJ.prototype.iniciar = function(){
		
	}
	
	return MascaraCNPJ;
	
}());

Syndic.MascaraData = (function(){
	
	function MascaraData(){
		
		this.inputData = $(".data-mask");
		this.inputData.mask('00/00/0000');
		
	}
	
	MascaraData.prototype.iniciar = function(){
		
	}
	
	return MascaraData;
	
}());


Syndic.MascaraTelefone = (function(){
	
	function MascaraTelefone(){
		
		this.inputTelefone = $(".telefone-mask");
		
	}
	
	MascaraTelefone.prototype.iniciar = function(){
		
		var maskBehavior = function (val) {
		  return val.replace(/\D/g, '').length === 9 ? '00000-0000' : '0000-00009';
		};
		
		var options = {
		  onKeyPress: function(val, e, field, options) {
		      field.mask(maskBehavior.apply({}, arguments), options);
		    }
		};
		
		this.inputTelefone.mask(maskBehavior, options);
		
	}
	
	return MascaraTelefone;
	
}());


Syndic.MascaraTelefoneCompleto = (function(){
	
	function MascaraTelefoneCompleto(){
		
		this.inputTelefone = $(".telefonecompleto-mask");
		
	}
	
	MascaraTelefoneCompleto.prototype.iniciar = function(){
		
		var maskBehavior = function (val) {
		  return val.replace(/\D/g, '').length === 9 ? '(00) 00000-0000' : '(00) 90000-0000';
		};
		
		var options = {
		  onKeyPress: function(val, e, field, options) {
		      field.mask(maskBehavior.apply({}, arguments), options);
		    }
		};
		
		this.inputTelefone.mask(maskBehavior, options);
		
	}
	
	return MascaraTelefoneCompleto;
	
}());

Syndic.MascaraCep = (function(){
	
	function MascaraCep(){
		
		this.inputCep = $(".cep-mask");
		this.inputCep.mask('00000-000');
		
	}
	
	MascaraCep.prototype.iniciar = function(){
		
	}
	
	return MascaraCep;
	
}());


Syndic.MascaraAltura = (function(){
	
	function MascaraAltura(){
		
		this.inputAltura = $(".altura-mask");
		this.inputAltura.mask('0.00');

	}
	
	MascaraAltura.prototype.iniciar = function(){
		
	}
	
	return MascaraAltura;
	
}());


Syndic.MascaraPercentual = (function(){
	
	function MascaraPercentual(){
		
		this.inputPercentual = $(".percentual-mask");
		this.inputPercentual.mask('000.00', {reverse: true});

	}
	
	MascaraPercentual.prototype.iniciar = function(){
		
	}
	
	return MascaraPercentual;
	
}());

Syndic.MaskMoney = (function(){
	
	function MaskMoney(){
		this.decimal = $('.js-decimal');
		this.plain = $('.js-plain');
	}
	
	MaskMoney.prototype.iniciar = function(){
		this.decimal.maskMoney({ decimal: ',', thousands: '.' });
		this.plain.maskMoney({ precision: 0, thousands: '.' });
	}
	
	return MaskMoney;
	
}());



$(function(){
	
	var mascaraAno = new Syndic.MascaraAno();
	mascaraAno.iniciar();
	
	var mascaraCpf = new Syndic.MascaraCpf();
	mascaraCpf.iniciar();
	
	var mascaraCNPJ = new Syndic.MascaraCNPJ();
	mascaraCNPJ.iniciar();	
	
	var mascaraTelefone = new Syndic.MascaraTelefone();
	mascaraTelefone.iniciar();
	
	var mascaraTelefoneCompleto = new Syndic.MascaraTelefoneCompleto();
	mascaraTelefoneCompleto.iniciar();
	
	
	var mascaraCep = new Syndic.MascaraCep();
	mascaraCep.iniciar();
	
	var mascaraData = new Syndic.MascaraData();
	mascaraData.iniciar();
	
	var mascaraPlaca = new Syndic.MascaraPlaca();
	mascaraPlaca.iniciar();
	
	
	var mascaraAltura = new Syndic.MascaraAltura();
	mascaraAltura.iniciar();
	
	var mascaraPercentual = new Syndic.MascaraPercentual();
	mascaraPercentual.iniciar();
	
	var maskMoney = new Syndic.MaskMoney();
	maskMoney.iniciar();
	
});