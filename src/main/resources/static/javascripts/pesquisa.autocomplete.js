var Syndic = Syndic || {};

Syndic.Autocomplete = (function() {

	function Autocomplete() {
		this.parametro = $('.js-parametro-input');
		var htmlTemplateAutocomplete = $('#template-autocomplete-visitante').html();
		this.url = this.parametro.data('url');
		this.template = Handlebars.compile(htmlTemplateAutocomplete);
		this.inputunidade =$('.js-unidade');
		this.inputnome=$('.js-nome');
		this.inputdocumento=$('.js-documento');
		this.inputveiculo=$('.js-veiculo');
		this.inputpermitido=$('.js-permitido');
		
		this.idcadastrante=$('.js-id-cadastrante');
		this.idunidade=$('.js-id-unidade');
		this.idcondominio=$('.js-id-condominio');
		this.idpessoa=$('.js-id-pessoa');
		this.idveiculo=$('.js-id-veiculo');
		
		
		
		
	}

	Autocomplete.prototype.iniciar = function() {
		var options = {
				url: function(parametro) {
					return  '../../../syndic/pessoafisica?parametro=' + parametro;
				}.bind(this),

				getValue: 'nome',
				minCharNumber: 2,
				requestDelay: 300,
				ajaxSettings: {
					contentType: 'application/json'
				},
				template: {
					type: 'custom',
					method: template.bind(this)
				},
				list: {
					onChooseEvent: onItemSelecionado.bind(this)
				}
				
				
				
				
		};
	
		this.parametro.easyAutocomplete(options);

	}


	function onItemSelecionado() {
		
		var visitante = this.parametro.getSelectedItemData();
		
		console.log(visitante);
		
		this.inputnome.val(visitante.nome);
		this.inputunidade.val(visitante.unidade);
		this.inputdocumento.val(visitante.documento);
		this.inputveiculo.val("sem veiculo");
		this.inputpermitido.val("pessoa que permitiu");
		
		
		
		this.idunidade.val(visitante.codigoUnidade);
		this.idpessoa.val(visitante.codigoPessoaFisicaUnidade);
		this.idveiculo.val(0);
		
		
		this.parametro.val('');
		this.parametro.focus();
	}



	function template(nome, pessoafisica) {
		return this.template(pessoafisica);
	}




	return Autocomplete

}());













$(function() {

	var autocomplete = new Syndic.Autocomplete();
	autocomplete.iniciar();
	
	

});