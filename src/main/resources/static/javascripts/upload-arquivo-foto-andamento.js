var Synd = Synd || {};


	Synd.UploadFotoAndamento = (function() {

		function UploadFotoAndamento() {
			this.inputNomeArquivo1 = $('.js-nomeArquivo1');
			this.inputContentType1 = $('.js-contentType1');
			this.inputNomeArquivo2 = $('.js-nomeArquivo2');
			this.inputContentType2 = $('.js-contentType2');
			this.inputNomeArquivo2 = $('.js-nome3'); 	
			this.inputContentType2 = $('.js-contentType3');
			this.inputNomeArquivo2 = $('.js-nomeArquivo4'); 	
			this.inputContentType2 = $('.js-contentType4');
			this.diretorio = $(".js-diretorioOs");
			this.urlExcluir = $('js-url-excluir-foto-andamento').attr("data-url");
			this.id = $(".js-id");
		}


		UploadFotoAndamento.prototype.iniciar = function () {
			
//				$(".js-upload1").addClass("js-sumir");
//				$(".js-upload1").removeClass("js-mostrar");
//				$(".js-upload2").addClass("js-sumir");
//				$(".js-upload2").removeClass("js-mostrar");
//				$(".js-upload3").addClass("js-sumir");
//				$(".js-upload3").removeClass("js-mostrar");
//			
//				
//				$(".js-arquivo-foto1").addClass("js-sumir");
//				$(".js-arquivo-foto1").removeClass("js-mostrar");
//				$(".js-arquivo-foto2").addClass("js-sumir");
//				$(".js-arquivo-foto2").removeClass("js-mostrar");
//				$(".js-arquivo-foto3").addClass("js-sumir");
//				$(".js-arquivo-foto3").removeClass("js-mostrar");
//				
//				
//			
//				$(".js-upload4").addClass("js-sumir");
//				$(".js-upload4").removeClass("js-mostrar");
				
			

			for(i=1;i<=$('.js-total-permitido-foto-andamento').val();i++){
				
				//Dropzone.autoDiscover = true;
				Dropzone.autoDiscover = false;
		         var myDropzone = new Dropzone("#arquivoUploadFotoAndamento"+i, {
		            paramName: "file", // The name that will be used to transfer the file
		            maxFilesize: 20, // MB
		            dictResponseError: 'Erro ao fazer o upload !',
		          //  autoProcessQueue: false, //FALSE PARA ENVIAR DEPOIS DE CLICAR EM OK
		            maxFiles: 1,
		           // acceptedFiles: 'image/*',
		            beforeSend:adicionarCsrfToken,
		            accept: function(file, done) {
					    if (file.size > 20971520) {
					      done("Limite de Tamanho Excedeu 20MB");
					    }
					    else { 
					    	done(); 
					    	}
					  },
		              headers: {
					        'x-csrf-token': $('input[name=_csrf]').val()
					    },
					    params:{
							'diretorio': this.diretorio.val(),
							'id': this.id.val(),
						},
						success: function(file, response){
							  onUploadCompleto(response);
							  
							  if(this.files.length != 0){
						            for(i=0; i<this.files.length; i++){
						                this.files[i].previewElement.remove();
						            }
						            this.files.length = 0;
						        }
							  
				            },
			           
			            
			            error: function(file, response){
							  $(".js-erros-i").html(parseInt($(".js-erros-i").html())+1);
							  $(".js-erros").html(parseInt($(".js-erros").html())+1);
							 // $(".js-lista-erros").show();
							  //$(".js-lista-erros").html( $(".js-lista-erros").html()+"<p>"+file.name+"<p/>");
							  
							  $(".relatorio-erro-table").show();
							//  adicionarLinha(file.name);
							  
							  if(parseInt($(".js-enviados").html())+parseInt($(".js-erros").html()) ==(parseInt($(".js-tentativas").html())-1) ){
								  $(".js-aguarde").fadeOut(4000);  
							  }
							  
							  
				            },
				            complete: function(file,response){
				            	$(".js-iniciando-upload").val(parseInt($(".js-iniciando-upload").val())+1);
								  
							  },
							  init: function() {
				                    this.on("addedfile", function(file) {
				                       
				                    	total = $(".js-total-permitido").val();
				                    	
				                    	if (this.files[total]!=null){
				                            this.removeFile(this.files[total]);
				                            $(".js-limite-excedido").show();
				                        }
				                    	else{
				                    		$(".js-limite-excedido").hide();
				                    	}
				                    	
				                       // var removeButton = Dropzone.createElement("<button class='btn btn-xs red btn-danger'>Excluir</button>");
				                        
				                        // Capture the Dropzone instance as closure.
				                       // var _this = this;

				                        // Listen to the click event
				                        //removeButton.addEventListener("click", function(e) {
				                          // Make sure the button click doesn't submit the form:
				                         // e.preventDefault();
				                         // e.stopPropagation();
				                          
				                          //excluirArquivo($(".js-id").val());
				                          
				                          // Remove the file preview.
				                          //_this.removeFile(file);
				                          // If you want to the delete the file on the server as well,
				                          // you can do the AJAX request here.
				                        //});

				                        // Add the button to the file preview element.
				                       // file.previewElement.appendChild(removeButton);
				                    });
				                    
				                    
				                } ,
		            
//		            clickable: ".fileinput-button",
		            addRemoveLinks: true, //BOTÃO PARA REMOVER O ARQUIVO
		            dictDefaultMessage: //MENSAGEM PADRÃO
	                    '<span class="bigger-100 bolder" style=" font-size: 12px;"> Foto</span>\
	                       <br /> \
	                       <i class="fa fa-plus fa-4x"></i>',
//		            dictResponseError: 'Error while uploading file!',
//		                    //change the previewTemplate to use Bootstrap progress bars
//		                    previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-details\">\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n    <div class=\"dz-size\" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class=\"progress progress-small progress-striped active\"><div class=\"progress-bar progress-bar-success\" data-dz-uploadprogress></div></div>\n  <div class=\"dz-success-mark\"><span></span></div>\n  <div class=\"dz-error-mark\"><span></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>"
		         });
		         
			}   

		}
		
		function resetDropZone(files) {
			if(files.length != 0){
	            for(i=0; i<files.length; i++){
	                files[i].previewElement.remove();
	            }
	            files.length = 0;
	        }
		}
		
		function excluirArquivo(id) {
			
			var parametro = (id!=0?id:$('.js-nome').val());
			var acao = (id!=0?"DELETE":"PUT");
				$.ajax({
				    url: $('.js-url-excluir').attr("data-url")+parametro,
				    type: acao,
				    beforeSend:adicionarCsrfToken,
				    success: function(result) {
				    	window.location.reload();
				    }
				});
		}
		
		
		function onUploadCompleto(resposta) {
			
			
			
		
					if($('.js-nomeArquivo1').val()==''){
						$('.js-contentType1').val(resposta.contentType);
						$('.js-nomeArquivo1').val(resposta.nome);			
						$('.js-imagem-arquivo1').html("<img class=\'img\'  src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
						$('.js-btn-anexo').attr("data-nome",resposta.nome);
						$('.js-arquivo-foto1').show();
						$('.js-upload1').hide();
					}
					else if($('.js-nomeArquivo2').val()==''){
						$('.js-contentType2').val(resposta.contentType);
						$('.js-nomeArquivo2').val(resposta.nome);
						$('.js-imagem-arquivo2').html("<img class=\'img\'  src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
						$('.js-arquivo-foto2').show();
						$('.js-upload2').hide();
					}
					
					else if($('.js-nomeArquivo3').val()==''){
						$('.js-contentType3').val(resposta.contentType);
						$('.js-nomeArquivo3').val(resposta.nome);
						$('.js-imagem-arquivo3').html("<img class=\'img\' src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
						$('.js-arquivo-foto3').show();
						$('.js-upload3').hide();
					}
					
					else if($('.js-nomeArquivo4').val()==''){
						$('.js-contentType4').val(resposta.contentType);
						$('.js-nomeArquivo4').val(resposta.nome);
						$('.js-imagem-arquivo4').html("<img class=\'img\'  src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
						$('.js-arquivo-foto4').show();
						$('.js-upload4').hide();
					}
			
//					cont = parseInt($(".js-iniciando-upload").val())+1
//					
//					$(".js-upload"+cont).addClass("js-sumir");
//					$(".js-upload"+cont).removeClass("js-mostrar");
//					
//					$(".js-arquivo-foto"+cont).addClass("js-mostrar");
//					$(".js-arquivo-foto"+cont).removeClass("js-sumir");
					
					$(".dz-message").show();
			
//			if($('.js-nomeArquivo1').val()=='' || $('.js-nomeArquivo2').val()==''  || $('.js-nomeArquivo3').val()==''){
//
//				cont = parseInt($(".js-iniciando-upload").val())+1
//				
//				$(".js-upload"+cont).addClass("js-sumir");
//				$(".js-upload"+cont).removeClass("js-mostrar");
//				$('.js-imagem-arquivo'+cont).html("<img class=\'img\' src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
//				$(".js-arquivo-foto"+cont).addClass("js-mostrar");
//				$(".js-arquivo-foto"+cont).removeClass("js-sumir");
//			}
				
		}
		return UploadFotoAndamento;
	})();

$(function() {
	var uploadFotoAndamento = new Synd.UploadFotoAndamento();
	uploadFotoAndamento.iniciar();
});