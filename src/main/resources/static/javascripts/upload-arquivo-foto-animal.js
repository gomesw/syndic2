var Syndic = Syndic || {};

Syndic.UploadArquivo = (function() {
	
	function UploadArquivo() {
		this.inputNomeArquivo1 = $('.js-nomeArquivo1');
		this.inputContentType1 = $('.js-contentType1');
		this.inputNomeArquivo2 = $('.js-nomeArquivo2');
		this.inputContentType2 = $('.js-contentType2');
		this.inputNomeArquivo3 = $('.js-nomeArquivo3');
		this.inputContentType3 = $('.js-contentType3');
		this.diretorio = $(".js-diretorio");
		this.urlExcluir = $('js-url-excluir').attr("data-url");
		this.id = $(".js-id");
		this.id1 = $(".js-id1");
		this.id2 = $(".js-id2");
		this.id3 = $(".js-id3");
		this.idcondominio = $(".js-idcondominio");
		
	}
	
	UploadArquivo.prototype.iniciar = function () {
		this.valortotal = 0;
		//Dropzone.autoDiscover = true;
		Dropzone.options.arquivoUpload = {
				  paramName: "file", // The name that will be used to transfer the file
				  parallelUploads: 1,
				  maxFilesize: 3, // MB
				  acceptedFiles: "image/jpeg,image/png,image/gif",
				//  dictDefaultMessage: "<strong>Arraste seus arquivos para cá!</strong>",
				  
				  dictDefaultMessage: //MENSAGEM PADRÃO
	                    ' <span class="bigger-200 bolder"><i class="ace-icon fa fa-caret-right red"></i>  Fotos </span>\
	                       <br /> \
	                       <i class="fa fa-plus fa-4x"></i>',
					 // maxFiles: 1,
				  headers: {
				        'x-csrf-token': $('input[name=_csrf]').val()
				    },
				    params:{
						'diretorio': this.diretorio.val(),
						'id': this.id.val(),
						'idcondominio': this.idcondominio.val()
					},
				  accept: function(file, done) {
					  if($(".js-total-gerado").val()>3){
						  done("Limite Máximo de 3 arquivos.");
					  }
					  else{
				    	done();
					  }
				  },
				  success: function(file, response){
					  $(".js-total-gerado").val(parseInt($(".js-total-gerado").val())+1);
					
					  onUploadCompleto(response);

					  
		            },
				  complete: function(file,response){
//					  if($(".js-id").val()!=0){
//							window.location.reload();
//						}
					  
				  },
				  
				  init: function() {
	                    this.on("addedfile", function(file) {
	                    	var corrente =   $(".js-total-gerado-antes").val();
	                    	$(".js-total-gerado-antes").val(parseInt($(".js-total-gerado-antes").val())+1);
	                    	
	                    	total = $(".js-total-permitido").val();
	                    	
	                    	if (this.files[total]!=null){
	                            this.removeFile(this.files[total]);
	                            $(".js-limite-excedido").show();
	                        }
	                    	else{
	                    		$(".js-limite-excedido").hide();
	                    	}
	                    	
	                        var removeButton = 
	                        	Dropzone.createElement("<button data-foto=\"\"  class='btn btn-xs red btn-danger js-url-excluir"+corrente+"'>Excluir</button>");
	                        
	                        
	                        
	                        
	                        // Capture the Dropzone instance as closure.
	                        var _this = this;

	                        // Listen to the click event
	                        removeButton.addEventListener("click", function(e) {
	                          // Make sure the button click doesn't submit the form:
	                          e.preventDefault();
	                          e.stopPropagation();
	                         
	                          
	                          excluirArquivo($(".js-id").val(),$(this).attr("data-foto"));
	                          
	                          // Remove the file preview.
	                          _this.removeFile(file);
	                          // If you want to the delete the file on the server as well,
	                          // you can do the AJAX request here.
	                        });

	                        // Add the button to the file preview element.
	                        file.previewElement.appendChild(removeButton);
	                        
	                    });
	                } 
				  
				  
				};
		
		
	}
	
	function adicionarCsrfToken(xhr) {
		var token = $('input[name=_csrf]').val();
		xhr.setRequestHeader('x-csrf-token', token);
	}
	
	function excluirArquivo(id,nome) {
		var acao = ("PUT");
		
		
			$.ajax({
			    url: $('.js-url-excluir').attr("data-url-excluir")+"/"+$(".js-idcondominio").val()+"/"+nome,
			    type: acao,
			    beforeSend:adicionarCsrfToken,
			    success: function(result) {
			    	
			    	
			    	$(".js-total-gerado-antes").val(parseInt($(".js-total-gerado-antes").val())-1);
			    	$(".js-total-gerado").val(parseInt($(".js-total-gerado").val())-1);
			    	
			    	$('.js-contentType'+parseInt($(".js-total-gerado").val())).val('');
					$('.js-nomeArquivo'+parseInt($(".js-total-gerado").val())).val('');
			    	
			    	
			    	
			    	
			    	
			    }
			});
	}
	
	
	function onUploadCompleto(resposta) {
		
		var index = parseInt($(".js-total-gerado").val())-1;
		
			$('.js-contentType'+index).val(resposta.contentType);
			$('.js-nomeArquivo'+index).val(resposta.nome);
			$('.js-url-excluir'+index).attr("data-foto",resposta.nome)
			

		

		
		
		
	}
	
	
	return UploadArquivo;
	
})();


$(function() {
	var uploadArquivo = new Syndic.UploadArquivo();
	uploadArquivo.iniciar();
});