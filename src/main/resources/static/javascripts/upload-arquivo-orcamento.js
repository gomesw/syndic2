var Synd = Synd || {};


	Synd.UploadArquivoOrcamento = (function() {

		function UploadArquivoOrcamento() {
			this.inputNomeArquivo1 = $('.js-nomeArquivoOrcamento1');
			this.inputContentType1 = $('.js-contentTypeOrcamento1');
			this.inputNomeArquivo2 = $('.js-nomeArquivoOrcamento2');
			this.inputContentType2 = $('.js-contentTypeOrcamento2');
			this.inputNomeArquivo2 = $('.js-nomeArquivoOrcamento3'); 	
			this.inputContentType2 = $('.js-contentTypeOrcamento3');
			this.inputNomeArquivo2 = $('.js-nomeArquivoOrcamento4'); 	
			this.inputContentType2 = $('.js-contentTypeOrcamento4');
			this.diretorio = $(".js-diretorioOrcamento");
			this.urlExcluir = $('js-url-excluirOrcamento').attr("data-url");
			this.id = $(".js-idOrcamento");
		}


		UploadArquivoOrcamento.prototype.iniciar = function () {
			
//			alert("iniciar");
			if($("#tipopessoa").val()==2){//sindico
				$(".js-upload1").addClass("js-sumir");
				$(".js-upload1").removeClass("js-mostrar");
				$(".js-upload2").addClass("js-sumir");
				$(".js-upload2").removeClass("js-mostrar");
				$(".js-upload3").addClass("js-sumir");
				$(".js-upload3").removeClass("js-mostrar");
			
				
				$(".js-arquivo-Orcamento1").addClass("js-sumir");
				$(".js-arquivo-Orcamento1").removeClass("js-mostrar");
				$(".js-arquivo-Orcamento2").addClass("js-sumir");
				$(".js-arquivo-Orcamento2").removeClass("js-mostrar");
				$(".js-arquivo-Orcamento3").addClass("js-sumir");
				$(".js-arquivo-Orcamento3").removeClass("js-mostrar");
				
				
			}
			else{
				
				$(".js-upload4").addClass("js-sumir");
				$(".js-upload4").removeClass("js-mostrar");
				
			}
			
			

			for(i=1;i<=$('.js-total-permitido-orcameto').val();i++){
				
				//Dropzone.autoDiscover = true;
				
				console.log('>>>>'+i);
				Dropzone.autoDiscover = false;
		         var myDropzone = new Dropzone("#arquivoUploadOrcamento"+i, {
		            paramName: "file", // The name that will be used to transfer the file
		            maxFilesize: 20, // MB
		            dictResponseError: 'Erro ao fazer o upload !',
		          //  autoProcessQueue: false, //FALSE PARA ENVIAR DEPOIS DE CLICAR EM OK
		            maxFiles: 1,
		           // acceptedFiles: 'image/*',
		            beforeSend:adicionarCsrfToken,
		            accept: function(file, done) {
					    if (file.size > 20971520) {
					      done("Limite de Tamanho Excedeu 20MB");
					    }
					    else { 
					    	done(); 
					    	}
					  },
		              headers: {
					        'x-csrf-token': $('input[name=_csrf]').val()
					    },
					    params:{
							'diretorio': this.diretorio.val(),
							'id': this.id.val(),
						},
						success: function(file, response){
							 console.log(response);
							  onUploadCompleto(response);
							  
							  if(this.files.length != 0){
						            for(i=0; i<this.files.length; i++){
						                this.files[i].previewElement.remove();
						            }
						            this.files.length = 0;
						        }
							  
				            },
			           
			            
			            error: function(file, response){
							  $(".js-erros-i").html(parseInt($(".js-erros-i").html())+1);
							  $(".js-erros").html(parseInt($(".js-erros").html())+1);
							 // $(".js-lista-erros").show();
							  //$(".js-lista-erros").html( $(".js-lista-erros").html()+"<p>"+file.name+"<p/>");
							  
							  $(".relatorio-erro-table").show();
							//  adicionarLinha(file.name);
							  
							  if(parseInt($(".js-enviados").html())+parseInt($(".js-erros").html()) ==(parseInt($(".js-tentativas").html())-1) ){
								  $(".js-aguarde").fadeOut(4000);  
							  }
							  
							  
				            },
				            complete: function(file,response){
				            	$(".js-iniciando-upload").val(parseInt($(".js-iniciando-upload").val())+1);
								  
							  },
							  init: function() {
				                    this.on("addedfile", function(file) {
				                       
				                    	total = $(".js-total-permitido").val();
				                    	
				                    	if (this.files[total]!=null){
				                            this.removeFile(this.files[total]);
				                            $(".js-limite-excedido").show();
				                        }
				                    	else{
				                    		$(".js-limite-excedido").hide();
				                    	}
				                    	
				                       // var removeButton = Dropzone.createElement("<button class='btn btn-xs red btn-danger'>Excluir</button>");
				                        
				                        // Capture the Dropzone instance as closure.
				                       // var _this = this;

				                        // Listen to the click event
				                        //removeButton.addEventListener("click", function(e) {
				                          // Make sure the button click doesn't submit the form:
				                         // e.preventDefault();
				                         // e.stopPropagation();
				                          
				                          //excluirArquivo($(".js-id").val());
				                          
				                          // Remove the file preview.
				                          //_this.removeFile(file);
				                          // If you want to the delete the file on the server as well,
				                          // you can do the AJAX request here.
				                        //});

				                        // Add the button to the file preview element.
				                       // file.previewElement.appendChild(removeButton);
				                    });
				                    
				                    
				                } ,
		            
//		            clickable: ".fileinput-button",
		            addRemoveLinks: true, //BOTÃO PARA REMOVER O ARQUIVO
		            dictDefaultMessage: //MENSAGEM PADRÃO
	                    '<span class="bigger-100 bolder" style=" font-size: 12px;"> '+(i==4?'Seu Orcamento':'Orcamento ' + i )+'</span>\
	                       <br /> \
	                       <i class="fa fa-plus fa-4x"></i>',
//		            dictResponseError: 'Error while uploading file!',
//		                    //change the previewTemplate to use Bootstrap progress bars
//		                    previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-details\">\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n    <div class=\"dz-size\" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class=\"progress progress-small progress-striped active\"><div class=\"progress-bar progress-bar-success\" data-dz-uploadprogress></div></div>\n  <div class=\"dz-success-mark\"><span></span></div>\n  <div class=\"dz-error-mark\"><span></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>"
		         });
		         
			}   

		}
		
		function resetDropZone(files) {
			if(files.length != 0){
	            for(i=0; i<files.length; i++){
	                files[i].previewElement.remove();
	            }
	            files.length = 0;
	        }
		}
		
		function excluirArquivo(id) {
			
			var parametro = (id!=0?id:$('.js-nomeArquivoOrcamento').val());
			var acao = (id!=0?"DELETE":"PUT");
				$.ajax({
				    url: $('.js-url-excluir').attr("data-url")+parametro,
				    type: acao,
				    beforeSend:adicionarCsrfToken,
				    success: function(result) {
				    	window.location.reload();
				    }
				});
		}
		
		
		function onUploadCompleto(resposta) {
			
			
			
			if($("#tipopessoa").val()==2){
				$('.js-contentTypeOrcamento4').val(resposta.contentType);
				$('.js-nomeArquivoOrcamento4').val(resposta.nome);
				$('.js-imagem-arquivoOrcamento4').html("<img src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
				$('.js-arquivo-Orcamento4').show();
				$('.js-upload4').hide();
				
			}else{
					if($('.js-nomeArquivoOrcamento1').val()==''){
						$('.js-contentTypeOrcamento1').val(resposta.contentType);
						$('.js-nomeArquivoOrcamento1').val(resposta.nome);			
						$('.js-imagem-arquivoOrcamento1').html("<img src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
						$('.js-btn-anexoOrcamento').attr("data-nome",resposta.nome);
						$('.js-arquivo-Orcamento1').show();
						$('.js-upload1').hide();
					}
					else if($('.js-nomeArquivoOrcamento2').val()==''){
						$('.js-contentTypeOrcamento2').val(resposta.contentType);
						$('.js-nomeArquivoOrcamento2').val(resposta.nome);
						$('.js-imagem-arquivoOrcamento2').html("<img src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
						$('.js-arquivo-Orcamento2').show();
						$('.js-upload2').hide();
					}
					
					else if($('.js-nomeArquivoOrcamento3').val()==''){
						$('.js-contentTypeOrcamento3').val(resposta.contentType);
						$('.js-nomeArquivoOrcamento3').val(resposta.nome);
						$('.js-imagem-arquivoOrcamento3').html("<img src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
						$('.js-arquivo-Orcamento3').show();
						$('.js-upload3').hide();
					}
			}
			
			
//			if($('.js-nomeArquivoOrcamento1').val()=='' || $('.js-nomeArquivoOrcamento2').val()==''  || $('.js-nomeArquivoOrcamento3').val()==''){

				cont = parseInt($(".js-iniciando-upload").val())+1
				
				$(".js-upload"+cont).addClass("js-sumir");
				$(".js-upload"+cont).removeClass("js-mostrar");
				
				$(".js-arquivo-Orcamento"+cont).addClass("js-mostrar");
				$(".js-arquivo-Orcamento"+cont).removeClass("js-sumir");
				
				$(".dz-message").show();
				
		}
		return UploadArquivoOrcamento;
	})();

$(function() {
	var uploadArquivoOrcamento = new Synd.UploadArquivoOrcamento();
	uploadArquivoOrcamento.iniciar();
});