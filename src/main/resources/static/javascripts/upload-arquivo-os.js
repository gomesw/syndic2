var Seta = Seta || {};

Seta.UploadArquivo = (function() {
	
	function UploadArquivo() {
		this.inputNomeArquivo1 = $('.js-nomeArquivo1');
		this.inputContentType1 = $('.js-contentType1');
		this.inputNomeArquivo2 = $('.js-nomeArquivo2');
		this.inputContentType2 = $('.js-contentType2');
		this.diretorio = $(".js-diretorio");
		this.urlExcluir = $('js-url-excluir').attr("data-url");
		this.id = $(".js-id");
	}
	
	UploadArquivo.prototype.iniciar = function () {
		Dropzone.autoDiscover = true;
		
		Dropzone.options.arquivoUpload = {
				  paramName: "file", // The name that will be used to transfer the file
				  maxFilesize: 20, // MB
				  acceptedFiles: ".jpeg,.jpg,.png",
//				  allow: "*.(pdf|jpg|jpeg|png)",
//				  dictDefaultMessage: "<strong>Arraste seus arquivos para cá!</strong>",
				  
				  dictDefaultMessage: //MENSAGEM PADRÃO
	                    '<span class="bigger-50 bolder">&nbsp; </span>\
	                       \
	                       <i class="fa fa-camera fa-3x"></i>',
					  maxFiles: 1,
				  headers: {
				        'x-csrf-token': $('input[name=_csrf]').val()
				    },
				    params:{
						'diretorio': this.diretorio.val(),
						'id': this.id.val(),
					},
				  accept: function(file, done) {
					  console.log(file.size);
				    if (file.size > 20971520) {
				      done("Limite de Tamanho Excedeu 20MB");
				    }
				    else { 
				    	done(); 
				    	}
				  },
				  success: function(file, response){
					  onUploadCompleto(response);
					  
					  if(this.files.length != 0){
				            for(i=0; i<this.files.length; i++){
				                this.files[i].previewElement.remove();
				            }
				            this.files.length = 0;
				        }
					  
		            },
				  complete: function(file,response){
					  if($(".js-id").val()!=0){
							window.location.reload();
						}
					  
				  },
				  init: function() {
	                    this.on("addedfile", function(file) {
	                       
	                    	total = $(".js-total-permitido").val();
	                    	
	                    	if (this.files[total]!=null){
	                            this.removeFile(this.files[total]);
	                            $(".js-limite-excedido").show();
	                        }
	                    	else{
	                    		$(".js-limite-excedido").hide();
	                    	}
	                    	
	                       // var removeButton = Dropzone.createElement("<button class='btn btn-xs red btn-danger'>Excluir</button>");
	                        
	                        // Capture the Dropzone instance as closure.
	                       // var _this = this;

	                        // Listen to the click event
	                        //removeButton.addEventListener("click", function(e) {
	                          // Make sure the button click doesn't submit the form:
	                         // e.preventDefault();
	                         // e.stopPropagation();
	                          
	                          //excluirArquivo($(".js-id").val());
	                          
	                          // Remove the file preview.
	                          //_this.removeFile(file);
	                          // If you want to the delete the file on the server as well,
	                          // you can do the AJAX request here.
	                        //});

	                        // Add the button to the file preview element.
	                       // file.previewElement.appendChild(removeButton);
	                    });
	                    
	                    
	                }    
				};
		
		
	}
	
	function resetDropZone(files) {
		if(files.length != 0){
            for(i=0; i<files.length; i++){
                files[i].previewElement.remove();
            }
            files.length = 0;
        }
	}
	
	function excluirArquivo(id) {
		
		var parametro = (id!=0?id:$('.js-nomeArquivo').val());
		var acao = (id!=0?"DELETE":"PUT");
			$.ajax({
			    url: $('.js-url-excluir').attr("data-url")+parametro,
			    type: acao,
			    beforeSend:adicionarCsrfToken,
			    success: function(result) {
			    	window.location.reload();
			    }
			});
	}
	
	
	function onUploadCompleto(resposta) {
		
		
		
		if($('.js-nomeArquivo1').val()==''){
			$('.js-contentType1').val(resposta.contentType);
			$('.js-nomeArquivo1').val(resposta.nome);			
			$('.js-imagem-arquivo1').html("<img class=\'img img-circle\' style=\'width: 70px; height: 70px;\' src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
			$('.js-btn-anexo').attr("data-nome",resposta.nome);
			$('.js-arquivo-1').show();			
		}
		else if($('.js-nomeArquivo2').val()==''){
			$('.js-contentType2').val(resposta.contentType);
			$('.js-nomeArquivo2').val(resposta.nome);
			$('.js-imagem-arquivo2').html("<img class=\'img img-circle\' style=\'width: 70px; height: 70px;\' src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
			$('.js-arquivo-2').show();
		}
		
		else if($('.js-nomeArquivo3').val()==''){
			$('.js-contentType3').val(resposta.contentType);
			$('.js-nomeArquivo3').val(resposta.nome);
			$('.js-imagem-arquivo3').html("<img class=\'img img-circle\' style=\'width: 70px; height: 70px;\' src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
			$('.js-arquivo-3').show();
		}
		
		
		else if($('.js-nomeArquivo4').val()==''){
			$('.js-contentType4').val(resposta.contentType);
			$('.js-nomeArquivo4').val(resposta.nome);
			$('.js-imagem-arquivo4').html("<img class=\'img img-circle\' style=\'width: 70px; height: 70px;\' src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
			$('.js-arquivo-4').show();
		}
		
		else {
			$('.js-contentType4').val(resposta.contentType);
			$('.js-nomeArquivo4').val(resposta.nome);
			$('.js-imagem-arquivo4').html("<img class=\'img img-circle\' style=\'width: 70px; height: 70px;\' src=\'/arquivo/arquivoos/nome/thumbnail." + resposta.nome + "\' >");
			$('.js-arquivo-4').show();
		}
		
		if($('.js-nomeArquivo1').val()=='' || $('.js-nomeArquivo2').val()=='' || $('.js-nomeArquivo3').val()=='' || $('.js-nomeArquivo4').val()==''){
			$(".dz-message").html(
				"<span class=\"bigger-100 bolder\">&nbsp; </span>\
                \
                <i class=\"fa fa-plus fa-3x\"></i>");
			$(".dz-message").show();
			
			
		}
		else{
			
			$(".js-upload").hide();
		}
		
		
		
	}
	
	
	return UploadArquivo;
	
})();


$(function() {
	var uploadArquivo = new Seta.UploadArquivo();
	uploadArquivo.iniciar();
});