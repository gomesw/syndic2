var Syndic = Syndic || {};

Syndic.UploadArquivo = (function() {
	
	function UploadArquivo() {
		this.inputNomeArquivo = $('.js-nomeArquivo');
		this.inputContentType = $('.js-contentType');
		this.novoArquivo = $('.js-novoArquivo');
		this.inputUrlArquivo = $('js-urlArquivo');
		this.idcondominio = $('.js-idcondominio');
		this.diretorio = $(".js-diretorio");
		this.urlExcluir = $('js-url-excluir').attr("data-url");
		this.id = $(".js-id");
		this.idunidade = $(".js-idunidade");
	}
	
	UploadArquivo.prototype.iniciar = function () {
		
		Dropzone.autoDiscover = true;
		Dropzone.options.arquivoUpload = {
				  paramName: "file", // The name that will be used to transfer the file
				  maxFilesize: 10, // MB
				//  dictDefaultMessage: "<strong>Arraste seus arquivos para cá!</strong>",
				  
				  dictDefaultMessage: //MENSAGEM PADRÃO
	                    '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i>  Mova seus arquivos </span> para fazer upload \
	                       <span class="smaller-80 grey">(ou clique)</span> <br /> \
	                       <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
					 // maxFiles: 1,
				  headers: {
				        'x-csrf-token': $('input[name=_csrf]').val()
				    },
				    params:{
						'diretorio': this.diretorio.val(),
						'id': this.id.val()+","+this.idunidade.val(),
					},
				  accept: function(file, done) {
				    if (file.name == "download.png") {
				      done("Arquivo Inválido.");
				    }
				    else { 
				    	done(); 
				    	}
				  },
				  success: function(file, response){
					  //console.log(file);
					  onUploadCompleto(response);
					  
					  $(".js-total-enviado").val(parseInt($(".js-total-enviado").val())+1);
					  
					  
		            },
				  complete: function(file,response){
					  //console.log(response);
					  
				  },
				  init: function() {
	                    this.on("addedfile", function(file) {
	                       
	                    	total = $(".js-total-permitido").val();
	                    	
	                    	if (this.files[total]!=null){
	                            this.removeFile(this.files[total]);
	                            $(".js-limite-excedido").show();
	                        }
	                    	else{
	                    		$(".js-limite-excedido").hide();
	                    	}
	                    	
	                        var removeButton = Dropzone.createElement("<button class='btn btn-xs red btn-danger'>Excluir</button>");
	                        
	                        // Capture the Dropzone instance as closure.
	                        var _this = this;

	                        // Listen to the click event
	                        removeButton.addEventListener("click", function(e) {
	                          // Make sure the button click doesn't submit the form:
	                          e.preventDefault();
	                          e.stopPropagation();
	                          
	                          excluirArquivo($(".js-id").val());
	                          
	                          // Remove the file preview.
	                          _this.removeFile(file);
	                          // If you want to the delete the file on the server as well,
	                          // you can do the AJAX request here.
	                        });

	                        // Add the button to the file preview element.
	                        file.previewElement.appendChild(removeButton);
	                    });
	                }     
				};
		
		
	}
	
	function excluirArquivo(id) {
		
		var parametro = (id!=0?id:$('.js-nomeArquivo').val());
		var acao = (id!=0?"DELETE":"PUT");
			$.ajax({
			    url: $('.js-url-excluir').attr("data-url")+parametro,
			    type: acao,
			    beforeSend:adicionarCsrfToken,
			    success: function(result) {
			    	window.location.reload();
			    }
			});
	}
	
	
	function onUploadCompleto(resposta) {
		$('.js-novoArquivo').val('true');
		$('.js-contentType').val(resposta.contentType);
		$('.js-nomeArquivo').val(resposta.nome);
		
	}
	
	
	return UploadArquivo;
	
})();


$(function() {
	var uploadArquivo = new Syndic.UploadArquivo();
	uploadArquivo.iniciar();
});