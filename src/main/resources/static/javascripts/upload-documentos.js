var Syndic = Syndic || {};

Syndic.UploadArquivo = (function() {
	
	function UploadArquivo() {
		this.novoArquivo = $('.js-novoArquivo');
		this.inputUrlArquivo = $('js-urlArquivo');
		
		this.urlExcluir = $('js-url-excluir').attr("data-url");
		this.diretorio = $(".js-diretorio");
		
		this.id = $(".js-id");
	}
	
	UploadArquivo.prototype.iniciar = function () {
		
		Dropzone.autoDiscover = true;
		Dropzone.options.arquivoUpload = {
				  paramName: "file", // The name that will be used to transfer the file
				  maxFilesize: 10, // MB
				//  dictDefaultMessage: "<strong>Arraste seus arquivos para cá!</strong>",
				  
				  dictDefaultMessage: //MENSAGEM PADRÃO
	                    '<span class="bigger-75 bolder"><i class="ace-icon fa fa-caret-right red"></i>  Mova seus arquivos </span> para fazer upload \
	                       <span class="smaller-40 grey">(ou clique)</span> <br /> \
	                       <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
					 // maxFiles: 1,
				  headers: {
				        'x-csrf-token': $('input[name=_csrf]').val()
				    },
//				    params:{
//						'tipo': $('#tipo').val(),
//						'tipodocumento': $('#tipodocumento').val()
//					},
				  accept: function(file, done) {
					  
					var erro = false;
					  
					 $(".js-msg-forma").hide();
					 $(".js-msg-tipo").hide();
					
				    if ($(".js-input-tipo").val() == null || $(".js-input-tipo").val() == '') {
				    	 $(".js-msg-forma").show();
				    	erro = true;
				    }
				    
				    if($(".js-select-documento").val()==null || $(".js-select-documento").val()==0){
				    	$(".js-msg-tipo").show();
				    	erro = true;
				    }
				    
				    
				    
				    if(erro){
				    	done("Erro na tentativa de envio deste arquivo");
				    	
				    	
//				    	$(".dz-message").html(
//				    			'<span class="bigger-75 bolder"><i class="ace-icon fa fa-caret-right red"></i>  Mova seus arquivos </span> para fazer upload \
//			                       <span class="smaller-40 grey">(ou clique)</span> <br /> \
//			                       <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>');
//							$(".dz-message").show();
				    }
				    else{ 
				    	done(); 
				    }
				    
				  },
				  success: function(file, response){
					  
					  
					  if(response.nome!=0){
					  
						  $(".js-enviados-i").html(parseInt($(".js-enviados-i").html())+1);
						  $(".js-enviados").html(parseInt($(".js-enviados").html())+1);
						  
						  if(parseInt($(".js-enviados").html())+parseInt($(".js-erros").html()) ==(parseInt($(".js-tentativas").html())-1) ){
							  $(".js-aguarde").fadeOut(4000);  
						  }
					  }
					  else{
						  $(".js-erros-i").html(parseInt($(".js-erros-i").html())+1);
						  $(".js-erros").html(parseInt($(".js-erros").html())+1);
						  
						  $(".relatorio-erro-table").show();
						  adicionarLinha(file.name);
						  
						  //$(".js-lista-erros").html( $(".js-lista-erros").html()+"<p>"+file.name+"<p/>");
						  
						  if(parseInt($(".js-enviados").html())+parseInt($(".js-erros").html()) ==(parseInt($(".js-tentativas").html())-1) ){
							  $(".js-aguarde").fadeOut(4000);  
						  }
						  
					  }
					  
		            },
		            
		            error: function(file, response){
						  $(".js-erros-i").html(parseInt($(".js-erros-i").html())+1);
						  $(".js-erros").html(parseInt($(".js-erros").html())+1);
						 // $(".js-lista-erros").show();
						  //$(".js-lista-erros").html( $(".js-lista-erros").html()+"<p>"+file.name+"<p/>");
						  
						  $(".relatorio-erro-table").show();
						  adicionarLinha(file.name);
						  
						  if(parseInt($(".js-enviados").html())+parseInt($(".js-erros").html()) ==(parseInt($(".js-tentativas").html())-1) ){
							  $(".js-aguarde").fadeOut(4000);  
						  }
						  
						  
			            },
				  complete: function(file,response){
					  
					  
				  },
				  init: function() {
	                    this.on("addedfile", function(file) {
	                    	
	                    	if(parseInt($(".js-tentativas").html())>=3){
						  		$(".js-anexos").fadeOut(1000);
						  		$(".js-relatorio").fadeIn(4000);
					  	  	}
	                    	else{
	                    		$(".js-relatorio-i").fadeIn(2000);
	                    	}
	                    	$(".js-tentativas").html(parseInt($(".js-tentativas").html())+1);
	                    	$(".js-tentativas-i").html(parseInt($(".js-tentativas-i").html())+1);
	                    	
	                    	total = $(".js-total-permitido").val();
	                    	
	                        // Capture the Dropzone instance as closure.
	                        var _this = this;

	                        // Listen to the click event

	                    });
	                }     
				};
		
		
	}
	
	return UploadArquivo;
	
})();


$(function() {
	var uploadArquivo = new Syndic.UploadArquivo();
	uploadArquivo.iniciar();
});


function adicionarLinha(valor){
	var newRow = $("<tr>");	    var cols = "";	
	cols += '<td>'+valor+'</td>';	  
	newRow.append(cols);	    
	$(".relatorio-erro-table").append(newRow);
}