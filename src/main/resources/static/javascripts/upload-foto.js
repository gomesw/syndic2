var Syndic = Syndic || {};

Syndic.UploadFoto = (function() {
	
	function UploadFoto() {
		this.inputNomeBrasao = $('.js-nomeFoto');
		this.inputContentType = $('.js-contentType');
		this.novoBrasao = $('.js-novaFoto');
		this.inputUrlBrasao = $('js-urlFoto');
		this.cnpj = $('.js-cnpj');
		this.idcondominio = $('.js-idcondominio');
		this.diretorio = $(".js-diretorio");
		
//		this.htmlFotoTemplate = $('#foto').html();
//		this.template = Handlebars.compile(this.htmlFotoTemplate);
//		
//		this.htmlSemFotoTemplate = $('#sem-foto').html();
//		this.templatesemfoto = Handlebars.compile(this.htmlSemFotoTemplate);
		
		
		this.containerFoto = $('.js-container-foto');
		this.removerFoto = $('.js-remove-brasao');
		this.uploadDrop = $('#upload-drop');
		this.divfoto = $('#foto');
	}
	
	UploadFoto.prototype.iniciar = function () {
		
		
		
		var settings = {
			type: 'json',
			params:{
				'diretorio': this.diretorio.val(),
				'id': this.idcondominio.val()+","+$(".js-id-pessoa").val(),
				'idunidade': this.idcondominio.val()+","+$(".js-id-pessoa").val()
			},
			filelimit: 1,
			allow: '*.(jpg|jpeg|png)',
			action: this.containerFoto.data('url'),
			complete: onUploadCompleto.bind(this),
			beforeSend: adicionarCsrfToken
		}
		
		UIkit.uploadSelect($('#upload-select'), settings);
		UIkit.uploadDrop(this.uploadDrop, settings);
		
		if (this.inputNomeBrasao.val()) {
			
			renderizarBrasao.call(this, { 
				nome:  this.inputNomeBrasao.val(), 
				contentType: this.inputContentType.val(),
				diretorio: this.diretorio.val(),
				url: this.inputUrlBrasao.val()});
			
		}
		
		
	}
	
	
	
	
	function onUploadCompleto(resposta) {
		this.novoBrasao.val('true');
	this.inputUrlBrasao.val(resposta.url);
		renderizarBrasao.call(this, resposta);
		
	}
	

	
	
	
	
	function renderizarBrasao(resposta) {
		
		this.inputNomeBrasao.val(resposta.nome);
		this.inputContentType.val(resposta.contentType);
		 $.ajax({
			url :this.containerFoto.data('url')+"/comfoto/"+this.diretorio.val()+"/"+this.idcondominio.val()+"/"+resposta.nome,
			method:'GET',
			contentType:'application/html',
			beforeSend:adicionarCsrfToken,
			success:function(resposta){
				$('#foto').html("&nbsp;");
				$('#foto').html(resposta);
			}
		 
		});
		
		
	}
	
	function adicionarCsrfToken(xhr) {
		var token = $('input[name=_csrf]').val();
		xhr.setRequestHeader('x-csrf-token', token);
	}
	
	return UploadFoto;
	
})();


$(function() {
	var uploadFoto = new Syndic.UploadFoto();
	uploadFoto.iniciar();
});