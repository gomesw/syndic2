function loadFilter() {
	'use strict';
	// indents all options depending on "i" CSS class
	function add_nbsp() {
		var opt = document.getElementsByTagName("option");
		for (var i = 0; i < opt.length; i++) {
			if (opt[i].className[0] === 'i') {
				opt[i].innerHTML = Array(3*opt[i].className[1]+1).join("&nbsp;") + opt[i].innerHTML;      // this means "&nbsp;" x (3*$indent)
			}
		}
	}
	// detects browser
	navigator.sayswho= (function() {
		var ua= navigator.userAgent, tem,
		M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
		if(/trident/i.test(M[1])){
			tem=  /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
			return 'IE '+(tem[1] || '');
		}
		M= M[2]? [M[1], M[2]]:[navigator.appName, navigator.appVersion, '-?'];
		if((tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
		return M.join(' ');
	})();
	// quick detection if browser is firefox
	function isFirefox() {
		var ua= navigator.userAgent,
		M= ua.match(/firefox\//i);  
		return M;
	}
	// indented select options support for non-firefox browsers
	if (!isFirefox()) {
		add_nbsp();
	}
}  